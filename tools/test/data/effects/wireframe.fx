# system material for drawing wireframe geometry

matrix modelview : WORLDVIEWMATRIX;
matrix proj : PROJMATRIX;
technique AMBIENT00
{
	pass p0
	{
		modelviewtransform <modelview>;
		projectiontransform <proj>;
		polygonmode line;
	}
}
