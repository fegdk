// X11: light00 wrong on vanta with nv driver
// WIN32: no valid techniques on ms opengl

// this effect demonstrates lightmapping and texcoord transformation
// supports cg shaders and ffp
// it uses GL_ARB_texture_env_combine and GL_ARB_multitexture extensions, in addition to extensions used by Cg runtime
// it's opengl only effect

// used in 'test' application, which must evolve to full-featured engine reference application soon


// define textures

texture lightmap : LIGHTMAP;
texture basemap;
float4 ambient_color 0.1 0.1 0.1 1;
float4 material_color 1 1 1 1;

matrix tcmod;	// texture transformation matrix, generated and set by material

// transformation matrices used by fixed function pipeline

matrix modelview : WORLDVIEWMATRIX;
matrix proj : PROJMATRIX;

// WVP matrix used by programmable pipe
matrix modelviewproj : WORLDVIEWPROJMATRIX;

technique AMBIENT00 // programmable pipeline
{
	pass p0
	{
		vertexprogram {
			type cgprogram;
			file cg/lightmap.cg;
			entry main;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
			bind tcmod <tcmod>;
			bind color <ambient_color>;
		}

		texture[0] <basemap>;

		// setup transforms
		modelviewtransform <modelview>;
		projectiontransform <proj>;
		texturetransform[0] <tcmod>;

		// it relies on some default values, but it works for that demo
		combine_rgb[0] modulate;
		source1_rgb[0] primary_color;
	}
}

technique LIGHT00	// programmable pipeline
{
	pass p0
	{
		// we use nothing but pure Cg here
		// vertex program transforms vertices and texcoords and passes them along with color to fragment program

		vertexprogram {
			type cgprogram;
			file cg/lightmap.cg;
			entry main;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
			bind tcmod <tcmod>;
			bind color <material_color>;
		}

		// fragment program does standard base*light*2 and outputs result
		
		fragmentprogram {
			type cgprogram;
			file cg/lightmap.cg;
			entry frag_main;
			profile bestfp;
			bind basemap <basemap>;
			bind lightmap <lightmap>;
		}
	}
}



technique AMBIENT01 // programmable pipeline
{
	pass p0
	{
		texture[0] <basemap>;

		// setup transforms
		modelviewtransform <modelview>;
		projectiontransform <proj>;
		texturetransform[0] <tcmod>;

		// it relies on some default values, but it works for that demo
		combine_rgb[0] modulate;
		source1_rgb[0] primary_color;
	}
}

technique LIGHT01	// fixed-function pipeline
{
	pass p0
	{
		// setup textures
		texture[0] <basemap>;
//		texture[1] <lightmap>;

		// setup transforms
		modelviewtransform <modelview>;
		projectiontransform <proj>;
		texturetransform[0] <tcmod>;

		// setup color equation (t0*t1*2)
		// it relies on some default values, but it works for that demo
		combine_rgb[1] modulate;
		rgb_scale[1] 2;
	}
}

