# simple material implementation
# just ambient + lighting using vertex program

texture map;
texture normalmap;
matrix modelviewproj : WORLDVIEWPROJMATRIX;
matrix world2model : INVWORLDMATRIX;
matrix view2world : INVVIEWMATRIX;
matrix view2model : VIEWTOMODELMATRIX;
matrix modelview : WORLDVIEWMATRIX;
float4 ambient_color : AMBIENTCOLOR;
float4 light_dir : LIGHTDIR0;
float4 light_pos : LIGHTPOS0;
float4 light_diffuse : LIGHTDIFFUSE0;
float4 light_specular : LIGHTSPECULAR0;

technique AMBIENT00
{
	pass p0
	{
		texture[0] <map>;
		vertexprogram {
			type cgprogram;
			file cg/vertexlighting.cg;
			entry ambient_simple;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
			bind ambientcolor <ambient_color>;
		}
	}
}

technique DIRECTLIGHT00
{
	pass p0
	{
		vertexprogram {
			type cgprogram;
			file cg/ppl.cg;
			entry directlight_vert;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
			bind world2model <world2model>;
			bind view2model <view2model>;
			bind lightdir <light_dir>;
			bind modelview <modelview>;
		}
		fragmentprogram {
			type cgprogram;
			file cg/ppl.cg;
			entry directlight_frag;
			profile bestfp;
			bind map <map>;
			bind nmap <normalmap>;
			bind light_diffuse <light_diffuse>;
		}
	}
}

