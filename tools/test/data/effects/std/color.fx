matrix worldviewproj : WORLDVIEWPROJMATRIX;
float4 color : AMBIENTCOLOR;

technique AMBIENT00
{
	pass p0
	{
		vertexprogram {
			type cgprogram;
			file cg/std/color.cg;
			entry main;
			profile bestvp;
			bind worldviewproj <worldviewproj>;
			bind color <color>;
		}
	}
}
