texture map;
texture lightmap;
matrix worldviewproj : WORLDVIEWPROJMATRIX;

# lightmap is drawn in on pass and doesn't need dynamic light contribution,
# so we don't have lighting techniques
# we don't draw ambient color either, since it's encoded in lightmap
technique AMBIENT00
{
	pass p0
	{
		vertexprogram {
			type cgprogram;
			file cg/std/lightmap.cg;
			entry lightmap_vert;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
		}
		fragmentprogram {
			type cgprogram;
			file cg/std/lightmap.cg
			entry lightmap_frag;
			profile bestfp;
			bind map <map>;
			bind lightmap <lightmap>;
		}
	}
}

technique AMBIENT01
{
	pass p0
	{
		texture[0] <map>;
		texture[1] <lightmap>;
	}
}



