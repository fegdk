// no effects

texture basemap;
texture normalmap;
texture specularmap;

float4 ambient_color 0.1 0.1 0.1 1;
float4 material_color 1 1 1 1;

matrix modelview : WORLDVIEWMATRIX;
matrix proj : PROJMATRIX;
matrix modelviewproj : WORLDVIEWPROJMATRIX;

technique AMBIENT00 {	// programmable
	pass p0 {
		texture[0] <basemap>;
				
		cg_vertexprogram {
			source cg/std/vert.cg trans_clr best;
			parm worldviewproj <modelviewproj>;
			parm color <ambient_color>;
		}
	}
}

technique AMBIENT01 { // ffp
	pass p0 {
		texture[0] <basemap>;
		ffp_primary_color <ambient_color>;
		modelviewtransform <modelview>;
		projectiontransform <proj>;
	}
}

technique LIGHT00 { // full-featured programmable vs.2.0/ps.2.0
}

