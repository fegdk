texture map;
matrix worldviewproj : WORLDVIEWPROJMATRIX;

technique AMBIENT00
{
	pass p0
	{
		vertexprogram {
			type cgprogram;
			file cg/std/misc.cg;
			entry simple_vert;
			profile bestvp;
			bind worldviewproj <worldviewproj>;
		}
		fragmentprogram {
			type cgprogram;
			file cg/std/misc.cg;
			entry simple_frag;
			profile bestfp;
			bind map <map>;
		}
	}
}

technique AMBIENT01
{
	pass p0
	{
		texture[0] <map>;
	}
}


