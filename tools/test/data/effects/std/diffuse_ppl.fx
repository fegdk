# simple material implementation
# just ambient + lighting using vertex program

texture map;
texture normalmap;
matrix modelviewproj : WORLDVIEWPROJMATRIX;
matrix world2model : INVWORLDMATRIX;
matrix model2world : WORLDMATRIX;
float4 ambient_color : AMBIENTCOLOR;
float4 light_dir : LIGHTDIR0;
float4 light_pos : LIGHTPOS0;
float4 light_diffuse : LIGHTDIFFUSE0;
float4 light_specular : LIGHTSPECULAR0;
matrix modelview : WORLDVIEWMATRIX;
matrix proj : PROJMATRIX;

technique AMBIENT00
{
	pass p0
	{
		fragmentprogram {
			type cgprogram;
			file cg/std/ambient.cg;
			entry ambient_frag;
			profile bestfp;
			bind map <map>;
			bind ambient_color <ambient_color>;
		}
		vertexprogram {
			type cgprogram;
			file cg/std/ambient.cg;
			entry ambient_vert;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
		}
	}
}

technique AMBIENT01
{
	pass p0
	{
		texture[0] <map>;
		modelviewtransform <modelview>;
		projectiontransform <proj>;
	}
}


technique DIRECTLIGHT00
{
	pass p0
	{
		vertexprogram {
			type cgprogram;
			file cg/std/ppl_diffuse.cg;
			entry directlight_vert;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
			bind world2model <world2model>;
			bind model2world <model2world>;
			bind light_dir <light_dir>;
		}
		fragmentprogram {
			type cgprogram;
			file cg/std/ppl_diffuse.cg;
			entry directlight_frag;
			profile bestfp;
			bind map <map>;
			bind nmap <normalmap>;
			bind light_diffuse <light_diffuse>;
		}
	}
}
