# simplest FFP single-pass setup

texture basemap;
matrix modelview : WORLDVIEWMATRIX;
matrix proj : PROJMATRIX;

technique AMBIENT00
{
	pass p0
	{
		texture[0] <basemap>;
		modelviewtransform <modelview>;
		projectiontransform <proj>;
	}
}

# don't draw light contribution, this setup for simple full-bright texture
technique DIRECTLIGHT00
{
	pass p0
	{
		texture[0] <basemap>;
		modelviewtransform <modelview>;
		projectiontransform <proj>;
	}
}

