# simple material implementation
# just ambient + lighting using vertex program

texture map;
matrix modelviewproj : WORLDVIEWPROJMATRIX;
matrix model2world : WORLDMATRIX;
float4 ambient_color : AMBIENTCOLOR;
float4 light_dir : LIGHTDIR0;
float4 light_pos : LIGHTPOS0;
float4 light_diffuse : LIGHTDIFFUSE0;
float4 light_specular : LIGHTSPECULAR0;

technique AMBIENT00
{
	pass p0
	{
		texture[0] <map>;
		vertexprogram {
			type cgprogram;
			file cg/vertexlighting.cg;
			entry ambient_simple;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
			bind ambientcolor <ambient_color>;
		}
	}
}

technique DIRECTLIGHT00
{
	pass p0
	{
		texture[0] <map>;
		vertexprogram {
			type cgprogram;
			file cg/vertexlighting.cg;
			entry directlight_simple;
			profile bestvp;
			bind worldviewproj <modelviewproj>;
			bind model2world <model2world>;
			bind lightdir <light_dir>;
			bind lightdiffuse <light_diffuse>;
			bind lightspecular <light_specular>;
		}
	}
}

