// this tool is a reference application for all FEGDK features.
// it is also a good "howto" guide, cause it's documented relatively good.
// just use grep and read code comments.
//
// the application is forked off of a years-old "mdlview" app, which is bundled too
//
// tool is supposed to draw some appropriate model (which can change anytime), play/stop music and sound effects, draw debugging information, showcase rendering features, let the user change rendering modes, use console, etc.

#include <fegdk/config.h>
#ifndef SOUND_DISABLED
#define ENABLE_MUSIC	1
#else
#define ENABLE_MUSIC	0
#endif

#define ENABLE_FONT		1

#include <fegdk/pch.h> // we don't use precompiled headers most of the time. i must change it to something apropriate someday...

#include <fegdk/f_application.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_model.h>

#if ENABLE_FONT
#include <fegdk/f_fontft.h>
#endif

#if ENABLE_MUSIC
#include <fegdk/f_oggvorbis.h>
#include <fegdk/f_modmusic.h>
#include <fegdk/wavfile.h>
#endif

#include <fegdk/f_console.h>

#include <fegdk/f_input.h>
#include <fegdk/f_baseviewport.h>
#include <fegdk/f_animation.h>
#include <fegdk/f_timer.h>
#include <fegdk/f_baserenderer.h>
#include <fegdk/f_effectparms.h>
#include <fegdk/f_resourcemgr.h>
#include <fegdk/f_drawutil.h>
#include <fegdk/f_scenemanager.h>

#include <fegdk/f_lightsource.h>

#include <GL/gl.h>	// gl is STILL required, cause we need it to draw some stuff. it will be removed when the engine will be more feature-complete.

#include <fegdk/f_parser.h>
#include <fegdk/f_types.h>

#include <fegdk/netmanager.h>
#include <fegdk/animsequencer.h>

//#include <fegdk/physx.h>
#include <fegdk/entitymanager.h>
#include <fegdk/entclasses.h>

#include <vector>

using namespace fe;

class TestApp : public application
{

private:

	smartPtr <animContainer>	mpAnimContainer;
	smartPtr <animation>	mpAnimation;
	std::vector <smartPtr <sceneObject> >	mpModels;
	int					mCurrentModel;
	matrix4			mMatrix;
	float				mAngleX;
	float				mAngleY;
	float				mDist;
	fe::ulong				mFrameTime;
	fe::ulong				mPrevTime;
#if ENABLE_MUSIC
//	vorbisFile*		mpMusic;
	smartPtr <modMusic>			mpMod;
	smartPtr <vorbisFile>		mpVorbis;
	smartPtr <wavFile>			mpWav;
#endif

#if ENABLE_FONT
	smartPtr< fontFT >	mpFont;
	smartPtr< fontFT >	mpBoldFont;
#endif

	smartPtr <netManager> mpNetManager;
	smartPtr <netSocket> mpRSocket;

	bool		mbDrawHelp;
	bool		mbPlayMusic;
	bool		mbDrawModel;
	bool		mbWireframe;
	bool		mbShadowVolume;
	bool		mbShadows;
	bool		mbTangents;

	enum { MAX_POINTS_ON_WINDING = 64 };
	struct winding_t {
		vector3 points[MAX_POINTS_ON_WINDING];
		int npoints;
		winding_t *next;
	};

	struct cluster_t;

	struct portal_t {
		vector3 points[MAX_POINTS_ON_WINDING];
		int npoints;
		portal_t *next[2];
		cluster_t *clusters[2];

	};

	struct cluster_t {
		portal_t *portals;
		winding_t *faces;
		cluster_t *next;
	};

	cluster_t *mpClusters;
	cluster_t *mpCurrentCluster;

	void free_prt (void)
	{
		cluster_t *next;
		for (cluster_t *c = mpClusters; c; c = next)
		{
			next = c->next;
			portal_t *nextp;
			for (portal_t *p = c->portals; p; p = nextp)
			{
				int side = p->clusters[0] == c ? 0 : 1;
				nextp = p->next[side];
				delete p;
			}
			winding_t *nextf;
			for (winding_t *f = c->faces; f; f = nextf)
			{
				nextf = f->next;
				delete f;
			}
			delete c;
		}
	}

	void load_prt (void)
	{
		charParser p (NULL, "test.prt", true);
		// magic, numclusters, numportals, numfaces
		p.getToken ();
		p.getToken ();
		int nclusters = atoi (p.token ());
		p.getToken ();
		int nportals = atoi (p.token ());
		p.getToken ();
		int nfaces = atoi (p.token ());
		int i;

		// create clusters
		for (i = 0; i < nclusters; i++)
		{
			cluster_t *clust = new cluster_t;
			memset (clust, 0, sizeof (cluster_t));
			clust->next = mpClusters;
			mpClusters = clust;
		}

		for (i = 0; i < nportals; i++)
		{
			int nclust;
			int k;
			cluster_t *c;
			portal_t *port = new portal_t;
			memset (port, 0, sizeof (portal_t));
			p.getToken (); // npoints
			port->npoints = atoi (p.token ());
			p.getToken (); // clust1
			nclust = atoi (p.token ());
			for (k = 0, c = mpClusters; k < nclust && c; k++, c = c->next);
			port->next[0] = c->portals;
			c->portals = port;
			port->clusters[0] = c;
			p.getToken (); // clust2
			nclust = atoi (p.token ());
			for (k = 0, c = mpClusters; k < nclust && c; k++, c = c->next);
			port->next[1] = c->portals;
			c->portals = port;
			port->clusters[1] = c;
			p.getToken (); // hint
			// points
			for (k = 0; k < port->npoints; k++)
			{
				p.matchToken ("(");
				p.getToken ();
				port->points[k][0] = atof (p.token ());
				p.getToken ();
				port->points[k][1] = atof (p.token ());
				p.getToken ();
				port->points[k][2] = atof (p.token ());
				p.matchToken (")");
			}
		}
		
		for (i = 0; i < nfaces; i++)
		{
			winding_t *face = new winding_t;
			memset (face, 0, sizeof (winding_t));
			p.errMode (0);
			if (p.getToken ()) // numpoints
				break;
			p.errMode (1);
			face->npoints = atoi (p.token ());
			p.getToken (); // cluster
			int nclust = atoi (p.token ());
			int k;
			cluster_t *c;
			for (k = 0, c = mpClusters; k < nclust && c; k++, c = c->next);
			face->next = c->faces;
			c->faces = face;
			// points
			for (k = 0; k < face->npoints; k++)
			{
				p.matchToken ("(");
				p.getToken ();
				face->points[k][0] = atof (p.token ());
				p.getToken ();
				face->points[k][1] = atof (p.token ());
				p.getToken ();
				face->points[k][2] = atof (p.token ());
				p.matchToken (")");
			}

		}
	}

public:

    TestApp (void)
	{
		mbDrawHelp = false;
		mbPlayMusic = false;
		mbDrawModel = true;
		mbWireframe = false;
		mbShadowVolume = false;
		mbShadows = false;
		mbTangents = false;
	
		mDist = 30.f;
		mpClusters = NULL;
	}

	bool isFullscreen (void)
	{
		return false;
	}
	
	void init ()
	{
//		mMatrix.identity ();
		mMatrix.camera (vector3 (0, -300, 0), vector3::zero, vector3 (0, 0, 1));
//		(vector3&)mMatrix._11 = -(vector3&)mMatrix._11;
//		(vector3&)mMatrix._22 = (vector3&)mMatrix._22;
//		(vector3&)mMatrix._33 = (vector3&)mMatrix._33;

		sceneObject *mdl;
		sceneObject *root = new sceneObject (NULL);
		for (int j = 0; j < 10; j++)
		{
			for (int i = 0; i < 10; i++)
			{
				mdl = g_engine->getResourceMgr ()->createModel ("models/lmtest.bmdl");
				matrix4 m = mdl->getRelativeMatrix ();
				m._41 += (i-5) * 200;
				m._43 += (j-5) * 200;
				mdl->setRelativeMatrix (m);
				mdl->update (sceneObject::F_RECALC_MATRIX);
				root->addChild (mdl);
			}
		}

		mpModels.push_back (root);
		mpModels.push_back (g_engine->getResourceMgr ()->createModel ("models/crate.bmdl"));
		//mpModels.push_back (g_engine->getResourceMgr ()->createModel ("models/hierarchy.bmdl"));
		mpAnimContainer = new animContainer;
		mpAnimation = g_engine->getResourceMgr ()->createAnimation ("animations/baking.anm");
		mpAnimContainer->addAnim (mpAnimation);
		mpModels.push_back (g_engine->getResourceMgr ()->createModel ("models/baking.bmdl", mpAnimContainer));
		mCurrentModel = 0;
#if ENABLE_MUSIC
		mpVorbis = new vorbisFile ("audio/test.ogg");
		mpMod = new modMusic ("audio/dreamfish-sanxion.mod");
		mpWav = new wavFile ("audio/test.wav");
#endif

#if ENABLE_FONT
		mpFont = g_engine->getResourceMgr ()->createFontFT ("ter-116n.pcf");
		mpBoldFont = g_engine->getResourceMgr ()->createFontFT ("ter-116b.pcf");
#endif
		if (mpModels[mCurrentModel])
			g_engine->getSceneManager ()->add (mpModels[mCurrentModel]);

		mpNetManager = new netManager ();
		mpRSocket = mpNetManager->createReceivingSocket (6666);

		load_prt ();
		mpCurrentCluster = mpClusters;
	}
	
	void free ()
	{
#if ENABLE_MUSIC
		mpVorbis = NULL;
		mpMod = NULL;
		mpWav = NULL;
#endif

		mpModels.clear ();
#if ENABLE_FONT
		mpFont = NULL;
#endif
	}
	
	void update ()
	{
		application::update ();
//		mpModel->update ();
		g_engine->getSceneManager ()->update ();

//		if (g_engine->getConsole ()->isVisible ())
//			return;

		mPrevTime = mFrameTime;
		mFrameTime = (int) (g_engine->getTimer ()->getTime () * 1000);

		const float speed = 100;

		smartPtr <inputDevice>	inp = g_engine->getInputDevice ();
	
// FIXME: had to be done through key bindings
//		if (inp->getState ()->isPressed (Key_UpArrow))
//			mDist += speed * (mFrameTime - mPrevTime) * 0.001;
//		if (inp->getState ()->isPressed (Key_DownArrow))
//			mDist -= speed * (mFrameTime - mPrevTime) * 0.001;

		mMatrix._43 = 0;

		static point prevPt;
		point pt = inp->getCursorPos ();

		int x = (int)pt.x - (int)prevPt.x;
		int y = (int)pt.y - (int)prevPt.y;

		prevPt = pt;

		if (inp->getState ()->isPressed (Key_Mouse1))
		{
			// rotate
			matrix4 r;
			r.rotateX (-y * 0.01);
			mMatrix = mMatrix * r;
			r.rotateY (-x * 0.01);
			mMatrix = mMatrix * r;
		}
		else if (inp->getState ()->isPressed (Key_Mouse2))
		{
			mDist -= y * 2;
		}

		if (mDist < 0)
			mDist = 0;

		mMatrix._43 = mDist;

		char message[256];
		int len = mpRSocket->recive (message, sizeof (message));
		if (len > 0)
		{
			printf ("message:\n%s\n", message);
		}
	}

#if 0
	void renderShadowVolume (const modelPtr &mdl, const lightSource &ls) const
	{
		// build shadow volume from provided ls
		mdl->buildShadowVolume (ls);

//		const modelTriangles_t &tris = model->getTriangles ();

		// set states to make shadow volume visible in a way we want to
//		glEnable (GL_BLEND);
//		glBlendFunc (GL_ONE, GL_ONE);
		glDepthMask (GL_FALSE);

		// set transforms
		glMatrixMode (GL_MODELVIEW);
		const matrix4 &m = mdl->getWorldMatrix ();
		matrix4 modelview = m * g_engine->getEffectParms ()->getMatrix (effectParm_ViewMatrix);
		glLoadMatrixf ( (GLfloat*)&modelview);
		
		// draw
		glDepthFunc (GL_LESS);
		glDepthMask (GL_FALSE);
		g_engine->getDrawUtil ()->setCullMode (drawUtil::cull_front);
		glColor3f (1, 0, 0);
//		model->renderShadowVolume ();
		g_engine->getDrawUtil ()->setCullMode (drawUtil::cull_back);
		glColor3f (0, 1, 0);
//		mdl->renderShadowVolume ();
		glDepthMask (GL_TRUE);
		glDepthFunc (GL_LEQUAL);

		// cancel states
		glDisable (GL_BLEND);
//		glDepthMask (GL_TRUE);

		// recurse. remember - renderShadowVolume doesn't recurse, cause it is requirement of rendering queue implementation
/*		for (size_t i = 0; i < mdl->numChildren (); i++)
		{
			if (mdl->getChild (i)->isTypeOf (model::TypeId))
			{
				renderShadowVolume (mdl->getChild (i).dynamicCast <model> (), ls);
			}
		}*/
	}
	#endif
	
	void render () const
	{
		application::render ();
		g_engine->getRenderer ()->clear (0, NULL, baseRenderer::clear_target | baseRenderer::clear_z, float4 (.2f, .8f, .8f, 1), 1, 0);
		
		// setup view and projection transforms for the scene
		matrix4 worldview = mMatrix;
		matrix4 viewproj;
		smartPtr <baseViewport> vp = g_engine->getViewport ();
		viewproj.projection (60.f*3.14f/180.f, vp->getHeight () / (float)vp->getWidth (), .1f, 10000.f);
		g_engine->getEffectParms ()->setMatrix (effectParm_ViewMatrix, worldview);
		g_engine->getEffectParms ()->setMatrix (effectParm_ProjMatrix, viewproj);
		g_engine->getSceneManager ()->draw ();

#if 1
		// draw current leaf
		if (mpClusters)
		{
			matrix4 mtx = worldview;;
			glMatrixMode (GL_PROJECTION);
			glLoadMatrixf ((GLfloat*)&viewproj);
			glMatrixMode (GL_MODELVIEW);
			glLoadMatrixf ((GLfloat*)&mtx);
			glDisable (GL_TEXTURE_2D);
			glColor4f (0,0,1,1);
			cluster_t *c;
			for (c = mpClusters; c; c = c->next)
			{
				if (c == mpCurrentCluster)
					glColor4f (0, 1, 0, 1);
				else
					glColor4f (0, 0, 1, 1);
				winding_t *f;
				for (f = c->faces; f; f = f->next)
				{
					glBegin (GL_LINE_LOOP);
					for (int p = 0; p < f->npoints; p++)
					{
						glVertex3fv (&f->points[p].x);
					}
					glEnd ();
				}
			}
			c = mpCurrentCluster;
			int side = 0;
			for (portal_t *port = c->portals; port; port = port->next[side])
			{
				side = port->clusters[0] == c ? 0 : 1;
				vector3 origin(0,0,0);
				glColor4f (1,1,1,1);
				glBegin (GL_LINE_LOOP);
				for (int p = 0; p < port->npoints; p++)
				{
					glVertex3fv (&port->points[p].x);
				}
				glEnd ();
				origin /= port->npoints;
				vector3 v1, v2;
				v1 = port->points[2] - port->points[1];
				v2 = port->points[0] - port->points[1];
				vector3 norm = v1.cross (v2);
				norm.normalize ();
				glColor4f (1, 0, 0, 1);
				glBegin (GL_LINES);
				glVertex3fv (&origin.x);
				origin += norm*2;
				glVertex3fv (&origin.x);
				glEnd ();
			}
		}
#endif
		
		lightSource ls;
		ls.setType (lightSource::spot);
		ls.setPosition (vector3 (110, 0, 200));
		ls.setDirection (vector3 (-1, 0, 0));

/*		if (mbShadowVolume)
		{
			glMatrixMode (GL_PROJECTION);
			glLoadMatrixf ( (GLfloat*)&viewproj);
			if (mpModels[mCurrentModel])
				renderShadowVolume (mpModels[mCurrentModel], ls);
		}*/

#if ENABLE_FONT
		cStr s;
#if 0
		s.printf ("leaf faces: %d", leafs[currentLeaf_].windings.size ());
		mpFont->drawTextString (s.c_str (), 0, 100, 0xffffffff);
		s.printf ("%s", leafs[currentLeaf_].opaque ? "opaque" : "translucent");
		mpFont->drawTextString (s.c_str (), 0, 110, 0xffffffff);
#endif

		if (mbDrawHelp)
		{
			int h = mpFont->getTextHeight ();
			int y = mpFont->getTextHeight () * 2;
			mpFont->drawTextString ("'m' - toggle mod playback", 0, y, 0xffffffff);
			y+=h;
			mpFont->drawTextString ("'o' - toggle ogg-vorbis playback", 0, y, 0xffffffff);
			y+=h;
			mpFont->drawTextString ("'w' - toggle wav playback", 0, y, 0xffffffff);
			y+=h;
			mpFont->drawTextString ("'t' - toggle 3d model", 0, y, 0xffffffff);
			y+=h;
			mpFont->drawTextString ("'v' - toggle shadowvolume", 0, y, 0xffffffff);
			y+=h;
			mpFont->drawTextString ("'s' - toggle shadows", 0, y, 0xffffffff);
			y+=h;
			mpFont->drawTextString ("'p' - toggle tangents", 0, y, 0xffffffff);
			y+=h;
			mpFont->drawTextString ("'a' - restart animation of current scene", 0, y, 0xffffffff);
			y+=h;
		}
		else
		{
			mpFont->drawTextString ("press 'h' for help", 0, mpFont->getTextHeight () * 2, 0xffffffff);
		}
		
		// draw text
		mpFont->drawTextString ("Test [$Revision$]", 0, 0, 0xffffffff);

		// draw fps
		static float fFPS      = 0.0f;
		static float fLastTime = 0.0f;
		static unsigned long dwFrames  = 0L;
		float fTime;
		fTime = g_engine->getTimer ()->getTime ();
		dwFrames++;
		if (fTime - fLastTime > 0.5f)
		{
			fFPS      = dwFrames / (fTime - fLastTime);
			fLastTime = fTime;
			dwFrames  = 0L;
		}
		s.printf ("fps: %.2f", fFPS);
		mpBoldFont->drawTextString (s.c_str (), 0, mpBoldFont->getTextHeight (), float4 (1,1,0,1));
#endif

	}

	void keyDown (int keycode)
	{
		static bool pause = false;
		if (keycode == Key_Escape)
			g_engine->quit ();
		else if (keycode == '[')
		{
			if (mpCurrentCluster == mpClusters)
				mpCurrentCluster = NULL;
			for (cluster_t *c = mpClusters; c; c = c->next)
			{
				if (c->next == mpCurrentCluster)
				{
					mpCurrentCluster = c;
					break;
				}
			}
		}
		else if (keycode == ']')
		{
			mpCurrentCluster = mpCurrentCluster->next;
			if (!mpCurrentCluster)
				mpCurrentCluster = mpClusters;
		}
		else if (keycode == 'b') {
			// physics stuff
			entityManager *em = g_engine->getEntManager ();
			entPhysXObj *en = (entPhysXObj *)em->createEntity (ECLASS_PHYSXOBJ);
			fprintf (stderr, "entity is %x\n", en);
			sceneObject *m = g_engine->getResourceMgr()->createModel ("models/crate.bmdl");
			fprintf (stderr, "crate is %x\n",m);
			en->setModel (m);
			en->setInitPos (vector3 (0,0,100));
//			physx_add_entity (en);
			g_engine->getSceneManager ()->add (m);
		}
		else if (keycode == 'a')
		{
			g_engine->getAnimSequencer ()->startAnimation (mpModels.back (), mpAnimation);
		}
		else if (keycode == 'h')
		{
			mbDrawHelp = !mbDrawHelp;
		}
		else if (keycode == 't')
		{
			mbDrawModel = !mbDrawModel;
			if (!mbDrawModel)
			{
				if (mpModels[mCurrentModel])
					g_engine->getSceneManager ()->remove (mpModels[mCurrentModel]);
			}
			else
			{
				if (mpModels[mCurrentModel])
					g_engine->getSceneManager ()->add (mpModels[mCurrentModel]);
			}
		}
#if ENABLE_MUSIC
		else if (keycode == 'm')
		{
			mbPlayMusic = !mbPlayMusic;
			if (mbPlayMusic)
				mpMod->play (true);
			else
				mpMod->stop ();
		}
		else if (keycode == 'o')
		{
			if (mpVorbis->isPlaying ())
			{
				mpVorbis->stop ();
			}
			else
			{
				mpVorbis->play (true);
			}
		}
		else if (keycode == 'w')
		{
			if (mpWav->isPlaying ())
			{
				mpWav->stop ();
			}
			else
			{
				mpWav->play (true);
			}
		}
#endif
		else if (keycode == 'v')
		{
			mbShadowVolume = !mbShadowVolume;
		}
		else if (keycode == Key_Tab)
		{
			g_engine->getSceneManager ()->remove (mpModels[mCurrentModel]);
			mCurrentModel++;
			mCurrentModel %= mpModels.size ();
			g_engine->getSceneManager ()->add (mpModels[mCurrentModel]);
		}
		else if (keycode == Key_Space)
		{
/*			// next sequence
			animationSequencer *s = mpModel->getAnimationSequencer ();
			if (s)
			{
				int n = s->numSequences ();
				int c = s->getSequence ();
				c++;
				if (c >= n)
					c = 0;
				s->setSequence (c, g_engine->getTimer ()->getTime ());
			}*/
		}
		else if ( (keycode &~ Key_CharFlag) == 'p')
		{
//			pause = !pause;
//			mpMusic->pause (pause);
		}
	}

	const char*		dataPath ()
	{
		return "data";
	}
};

int
main (int argc, char *argv[])
{
	// init objects
	engine* e = new engine;
	smartPtr <application> app = new TestApp;

	// run application through the engine
	e->run (app, argc, argv);

	app = NULL;
	delete e;

#if defined (_DEBUG) && defined (_MSC_VER)
	_CrtDumpMemoryLeaks ();
#endif
	fprintf (stderr, "exit.\n");
	return 0;
}

// eof

