// $Header$

// this tool takes quake3 shader scripts and generates fe material files which "look like" q3 shaders
// used to load q3 maps into feditor and see it really texture mapped and all that kind of stuff

#include "pch.h"

#define FE_NO_FILESYSTEM

#include "f_parser.h"
#include <direct.h>

// $Log$
// Revision 1.1  2005/01/18 19:11:36  waker
// Initial revision
//
// Revision 1.1.1.1  2005/01/03 15:21:08  waker
// import
//
// Revision 1.9  2003/06/18 10:01:15  waker
// effect file names are now compliant to the engine (less than or equal to 32 chars), but seems to skip some flags. needs further debugging
//
// Revision 1.8  2003/06/11 07:05:15  waker
// started doing material support in editor. lot's of modifications throught the entire engine (finding bugs, leaks, fixing)
//
// Revision 1.7  2003/04/17 14:22:37  waker
// filetool was tested on a large dataset (sabotain), fixed critical bugs, added timings/progress indication
//
// Revision 1.6  2003/04/16 15:07:54  waker
// fixed minor flag-bits overflow bug, added flags2 for alphatest
//
// Revision 1.5  2003/04/16 15:06:07  waker
// added support for more blendfuncs, and more to come
//
// Revision 1.4  2003/04/16 14:42:11  waker
// SHADER_F_TEXTRANSFORMx flags removed from source code
//
// Revision 1.3  2003/04/16 14:41:04  waker
// comments changed, cvs revision tag removed
//
// Revision 1.2  2003/04/16 14:39:39  waker
// first working version of q3shader. should test it's output with editor or some other app
//

// min-spec hw is supported (everything done through multipass)
// no hi-end optimizations
//      (it's for test purposes only, but generated effects can be hand-optimized if needed)

// 1-4 passes supported (effect-side)
// alphatest is predefined, can be disabled or GE128 (effect-side)
// surfaceparms are partially supported (only important stuff, such as origin, etc) (material-side)
// blendfunc is to be fully supported (just a matter of time)

// all other stuff is ignored through feCharParser::nextLine or some other way

// blendfuncs for each pass
#define SHADER_F_BLEND_DSTCOLOR_ONE1		0x00000001
#define SHADER_F_BLEND_DSTCOLOR_ONE2		0x00000002
#define SHADER_F_BLEND_DSTCOLOR_ONE3		0x00000004
#define SHADER_F_BLEND_DSTCOLOR_ONE4		0x00000008
#define SHADER_F_BLEND_ZERO_SRCALPHA1		0x00000010
#define SHADER_F_BLEND_ZERO_SRCALPHA2		0x00000020
#define SHADER_F_BLEND_ZERO_SRCALPHA3		0x00000040
#define SHADER_F_BLEND_ZERO_SRCALPHA4		0x00000080
#define SHADER_F_BLEND_BLEND1				0x00000100
#define SHADER_F_BLEND_BLEND2				0x00000200
#define SHADER_F_BLEND_BLEND3				0x00000400
#define SHADER_F_BLEND_BLEND4				0x00000800
#define SHADER_F_BLEND_ADD1					0x00001000
#define SHADER_F_BLEND_ADD2					0x00002000
#define SHADER_F_BLEND_ADD3					0x00004000
#define SHADER_F_BLEND_ADD4					0x00008000
#define SHADER_F_BLEND_FILTER1				0x00010000
#define SHADER_F_BLEND_FILTER2				0x00020000
#define SHADER_F_BLEND_FILTER3				0x00040000
#define SHADER_F_BLEND_FILTER4				0x00080000
#define SHADER_F_BLEND_FILTER_ALPHA1		0x00100000
#define SHADER_F_BLEND_FILTER_ALPHA2		0x00200000
#define SHADER_F_BLEND_FILTER_ALPHA3		0x00400000
#define SHADER_F_BLEND_FILTER_ALPHA4		0x00800000
#define SHADER_F_BLEND_INVBLEND1			0x01000000
#define SHADER_F_BLEND_INVBLEND2			0x02000000
#define SHADER_F_BLEND_INVBLEND3			0x04000000
#define SHADER_F_BLEND_INVBLEND4			0x08000000
#define SHADER_F_BLEND_ONE_SRCCOLOR1		0x10000000
#define SHADER_F_BLEND_ONE_SRCCOLOR2		0x20000000
#define SHADER_F_BLEND_ONE_SRCCOLOR3		0x40000000
#define SHADER_F_BLEND_ONE_SRCCOLOR4		0x80000000

// alphatest for each pass
#define SHADER_F_ALPHATEST1			0x00000001
#define SHADER_F_ALPHATEST2			0x00000002
#define SHADER_F_ALPHATEST3			0x00000004
#define SHADER_F_ALPHATEST4			0x00000008

const char effectstart[] =
	"texture tex0;\n"
	"texture tex1;\n"
	"texture tex2;\n"
	"texture tex3;\n"
	"technique t0 {\n";

const char passstart[] =
	"\tpass p%d {\n"
	"\t\ttexture[0] <tex%d>;\n"
	"\t\tvertexshader xyz | diffuse | tex1;\n"
	"\t\tColorOp[0]   selectarg2;\n"
	"\t\tColorArg1[0] Diffuse;\n"
	"\t\tColorArg2[0] Texture;\n"
	"\t\tAlphaOp[0]   selectarg2;\n"
	"\t\tAlphaArg1[0] Diffuse;\n"
	"\t\tAlphaArg2[0] Texture;\n"
	"\t\tMinFilter[0] linear;\n"
	"\t\tMagFilter[0] linear;\n"
	"\t\tMipFilter[0] linear;\n"
	"\t\taddressu[0] wrap;\n"
	"\t\taddressv[0] wrap;\n"
	"\t\twrap0 0;\n"
	"\t\tColorOp[1] Disable;\n"
	"\t\tAlphaOp[1] Disable;\n"
	"\t\tlighting false;\n"
	"\t\tspecularenable false;\n";

// there goes blendfunc and alphafunc
const char passblend[] =
	"\t\talphablendenable true;\n"
	"\t\tsrcblend srcalpha;\n"
	"\t\tdestblend invsrcalpha;\n";

const char passadd[] =
	"\t\talphablendenable true;\n"
	"\t\tsrcblend one;\n"
	"\t\tdestblend one;\n";

const char passfilter[] =
	"\t\talphablendenable true;\n"
	"\t\tsrcblend destcolor;\n"
	"\t\tdestblend zero;\n";

const char passfilteralpha[] =
	"\t\talphablendenable true;\n"
	"\t\tsrcblend destcolor;\n"
	"\t\tdestblend srcalpha;\n";

const char passinvblend[] =
	"\t\talphablendenable true;\n"
	"\t\tsrcblend invsrcalpha;\n"
	"\t\tdestblend srcalpha;\n";

const char passonesrccolor[] =
	"\t\talphablendenable true;\n"
	"\t\tsrcblend one;\n"
	"\t\tdestblend srccolor;\n";

const char passdstcolorone[] =
	"\t\talphablendenable true;\n"
	"\t\tsrcblend destcolor;\n"
	"\t\tdestblend one;\n";

const char passzerosrcalpha[] =
	"\t\talphablendenable true;\n"
	"\t\tsrcblend zero;\n"
	"\t\tdestblend srcalpha;\n";

const char passalphatest[] =
	"\t\talphatestenable true;\n"
	"\t\talphafunc greaterequal;\n"
	"\t\talpharef 128;\n";

const char passend[] =
	"\t}\n";

const char effectend[] =
	"}\n";

void ProcessFile( const TCHAR *fname )
{
	std::vector< feCStr > shadernames;
	try {
		feCharParser p( feCStr( fname ), true );
        for ( ;; )
		{
			feCStr mtlname;
			ulong flags=0;
			ulong flags2 = 0;
			int numpasses = 0;
			p.getToken(); // name
			mtlname = p.token();
			_tprintf( _T( "loading '%s'...\n" ), feStr( mtlname ).c_str() );
			p.matchToken( "{" );
			feCStr maps[4];
			for ( ;; )
			{
				p.getToken();
				// load shader
				if ( !p.cmptoken( "}" ) )
				{
					// write effect and material

					// construct effect fname
					feCStr fname = "q3shaders/";
					__int64 fname_int = 0;

					// numpasses
					fname_int = numpasses;
					if ( numpasses > 4 )
						continue;

#if 0
					// blendfuncs for each pass
					for ( int i = 0; i < numpasses; i++ )
					{
						if ( flags & ( SHADER_F_BLEND_BLEND1 << i ) )
						{
							fname += "_bb";
							continue;
						}
						if ( flags & ( SHADER_F_BLEND_ADD1 << i ) )
						{
							fname += "_ba";
							continue;
						}
						if ( flags & ( SHADER_F_BLEND_FILTER1 << i ) )
						{
							fname += "_bf";
							continue;
						}
						if ( flags & ( SHADER_F_BLEND_FILTER_ALPHA1 << i ) )
						{
							fname += "_bfa";
							continue;
						}
						if ( flags & ( SHADER_F_BLEND_INVBLEND1 << i ) )
						{
							fname += "_bib";
							continue;
						}
						if ( flags & ( SHADER_F_BLEND_ONE_SRCCOLOR1 << i ) )
						{
							fname += "_b1srccolor";
							continue;
						}
						if ( flags & ( SHADER_F_BLEND_DSTCOLOR_ONE1 << i ) )
						{
							fname += "_bdstcolor1";
							continue;
						}
						if ( flags & ( SHADER_F_BLEND_ZERO_SRCALPHA1 << i ) )
						{
							fname += "_b0srcalpha";
							continue;
						}
						
						fname += "_br";
						continue;
					}

					// alphatest for each pass
					for ( int i = 0; i < numpasses; i++ )
					{
						if ( flags2 & ( SHADER_F_ALPHATEST1 << i ) )
						{
							fname += "_at1";
							continue;
						}
						fname += "_at0";
						continue;
					}
#endif
					fname_int |= ( ( __int64 )flags ) << 3;

					__int64 ff2 = ( __int64 )flags2 << ( 3 + 32 );

					fname_int |= ( ( __int64 )flags2 ) << (3+32);

					char str[256];
					sprintf( str, "%0.8X", (ulong)flags );
					fname += str;
					sprintf( str, "%0.8X", (ulong)(flags>>32) );
					fname += str;
					fname += ".fx";

					FILE *fp = fopen( fname, "w+t" );
					if ( fp )
					{
						fprintf( fp, effectstart );
						if ( !numpasses )
						{
							fprintf( fp, passstart, 0, 0 );
							fprintf( fp, passblend );
							fprintf( fp, passend );
						}
						else
						{
							for ( int i = 0; i < numpasses; i++ )
							{
								fprintf( fp, passstart, i, i );

								if ( flags & ( SHADER_F_BLEND_BLEND1 << i ) )
								{
									fprintf( fp, passblend );
								}
								else if ( flags & ( SHADER_F_BLEND_ADD1 << i ) )
								{
									fprintf( fp, passadd );
								}
								else if ( flags & ( SHADER_F_BLEND_FILTER1 << i ) )
								{
									fprintf( fp, passfilter );
								}
								else if ( flags & ( SHADER_F_BLEND_FILTER_ALPHA1 << i ) )
								{
									fprintf( fp, passfilteralpha );
								}
								else if ( flags & ( SHADER_F_BLEND_INVBLEND1 << i ) )
								{
									fprintf( fp, passinvblend );
								}
								else if ( flags & ( SHADER_F_BLEND_ONE_SRCCOLOR1 << i ) )
								{
									fprintf( fp, passonesrccolor );
								}
								else if ( flags & ( SHADER_F_BLEND_DSTCOLOR_ONE1 << i ) )
								{
									fprintf( fp, passdstcolorone );
								}

								if ( flags2 & ( SHADER_F_ALPHATEST1 << i ) )
								{
									fprintf( fp, passalphatest );
								}

								fprintf( fp, passend );
							}
						}
						fprintf( fp, effectend );
						fclose( fp );
					}

					// build mtl fname
					feCStr mtlfname = mtlname;
					// replace all slashes to underscores
					char *ptr = (char*)mtlfname.c_str();
					for ( ; *ptr; ptr++ )
					{
						if ( *ptr == '/' )
							*ptr = '_';
					}
					feCStr mtldir = "q3materials/" +  mtlname;
					{
						char fname[MAX_PATH];
						char *ptr = fname;
						strcpy( fname, mtldir );

						while (1)
						{
							while (*ptr && *ptr!='/') ptr++;
							if (*ptr!='/') break;

							// create if not exists
							*ptr=0;
							_mkdir(fname);
							*ptr='/';
							ptr++;
						}
					}

					mtlfname = mtldir + ".fmtl";
					shadernames.push_back( mtlfname );
					fp = fopen( mtlfname, "w+t" );
					if ( fp )
					{
						fprintf( fp, "%s {\n", mtlname.c_str() );
						fprintf( fp, "\teffectfile %s\n", fname.c_str() );
						for ( int i = 0; i < numpasses; i++ )
						{
							fprintf( fp, "\ttexinput tex%d %s\n", i, maps[i].c_str() );
						}
						fprintf( fp, "}\n" );
						fclose( fp );
					}
					break;
				}
				else if ( !p.cmptoken( "surfaceparm" ) )
				{
					p.getToken();
					// TODO: handle useful surfaceparms
				}
				else if ( !p.cmptoken( "qer_editorimage" ) )
				{
					p.getToken();
					// TODO: do i need qer_editorimage?
				}
				else if ( !p.cmptoken( "{" ) )
				{
					if ( numpasses == 4 )
					{
						p.ignoreBlock( "{", "}" );
						continue;
					}
					for ( ;; )
					{
						// load pass
						p.getToken();
						// ignore stuff
						if ( !p.cmptoken( "}" ) )
						{
							numpasses++;
							break;
						}
						else if ( !p.cmptoken( "map" ) || !p.cmptoken( "clampmap" ) )
						{
							p.getToken();
							// evil! carmack doesn't like to inform artists about their shaders being buggy!
							// just look for "$lightmapt" in "hell.shader"!
							if ( !p.cmptoken( "$lightmap" ) || !p.cmptoken( "$lightmapt" ) )
							{
								p.ignoreBlock( "{", "}" );
								break;
							}
							maps[numpasses] = p.token();
						}
						else if ( !p.cmptoken( "animmap" ) )
						{
							p.getToken();	// skip rate
							p.getToken();
							maps[numpasses] = p.token();
							// skip to eol
							p.nextLine();
						}
						else if ( !p.cmptoken( "alphaFunc" ) )
						{
							p.getToken();
							flags2 |= ( SHADER_F_ALPHATEST1 << numpasses );
						}
						else if ( !p.cmptoken( "blendFunc" ) )
						{
							p.getToken();
							if ( !p.cmptoken( "add" ) )
							{
								flags |= ( SHADER_F_BLEND_ADD1 << numpasses );
							}
							else if ( !p.cmptoken( "blend" ) )
							{
								flags |= ( SHADER_F_BLEND_BLEND1 << numpasses );
							}
							else if ( !p.cmptoken( "filter" ) )
							{
								flags |= ( SHADER_F_BLEND_FILTER1 << numpasses );
							}
							else if ( !p.cmptoken( "GL_ONE" ) )
							{
								p.getToken();
								if ( !p.cmptoken( "GL_ONE" ) )
									flags |= ( SHADER_F_BLEND_ADD1 << numpasses );
								else if ( !p.cmptoken( "GL_SRC_COLOR" ) )
									flags |= ( SHADER_F_BLEND_ONE_SRCCOLOR1 << numpasses );
							}
							else if ( !p.cmptoken( "GL_SRC_ALPHA" ) )
							{
								p.getToken();
								if ( !p.cmptoken( "GL_ONE_MINUS_SRC_ALPHA" ) )
									flags |= ( SHADER_F_BLEND_BLEND1 << numpasses );
							}
							else if ( !p.cmptoken( "GL_DST_COLOR" ) )
							{
								p.getToken();
								if ( !p.cmptoken( "GL_ZERO" ) )
									flags |= ( SHADER_F_BLEND_FILTER1 << numpasses );
								else if ( !p.cmptoken( "GL_SRC_ALPHA" ) )
									flags |= ( SHADER_F_BLEND_FILTER_ALPHA1 << numpasses );
								else if ( !p.cmptoken( "GL_SRC_ALPHA" ) )
									flags |= ( SHADER_F_BLEND_DSTCOLOR_ONE1 << numpasses );
							}
							else if ( !p.cmptoken( "GL_ONE_MINUS_SRC_ALPHA" ) )
							{
								p.getToken();
								if ( !p.cmptoken( "GL_SRC_ALPHA" ) )
									flags |= ( SHADER_F_BLEND_INVBLEND1 << numpasses );
							}
							else if ( !p.cmptoken( "GL_ZERO" ) )
							{
								p.getToken();
								if ( !p.cmptoken( "GL_SRC_ALPHA" ) )
									flags |= ( SHADER_F_BLEND_ZERO_SRCALPHA1 << numpasses );
							}
							else 
								p.nextLine();
						}
						else
							p.nextLine();
					}
				}
				else
					p.nextLine();
			}
		}
	}
	catch ( feParserException e )
	{
		if ( e.mErrCode != feParserException::END_OF_FILE )
			_tprintf( _T( "error while parsing %s:\n   %s\n" ), e.mOutput.c_str() );
	}
	// write out shadernames
	char fn[_MAX_FNAME];
	_splitpath( feCStr( fname ), NULL, NULL, fn, NULL );
	FILE *fp = fopen( feCStr( "q3materials/" ) + feCStr( fn ) + ".lst", "w+t" );
	if ( fp )
	{
		for ( size_t i = 0; i < shadernames.size(); i++ )
		{
			fprintf( fp, "%s\n", shadernames[i].c_str() );
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	if ( argc <= 1 )
	{
		_tprintf( _T( "q3shader, tool for converting quake3 shaders to FE materials.\n" ) );
		_tprintf( _T( "usage: q3shader <shader_file(s)>\n" ) );
		_tprintf( _T( "     e.g.: q3shader c:/games/quake3/baseq3/shaders/*.shader\n" ) );
		return -1;
	}


	_tmkdir( _T( "q3shaders" ) );
	_tmkdir( _T( "q3materials" ) );

	TCHAR drive[_MAX_PATH];
	TCHAR dir[_MAX_PATH];
	_tsplitpath( argv[1], drive, dir, NULL, NULL );
	WIN32_FIND_DATA wfd;
	memset( &wfd, 0, sizeof( wfd ) );
	HANDLE hs = FindFirstFile( argv[1], &wfd );
	if ( hs != INVALID_HANDLE_VALUE )
	{
		do {
			TCHAR fullpath[MAX_PATH];
			_stprintf( fullpath, _T( "%s%s%s" ), drive, dir, wfd.cFileName );
			ProcessFile( fullpath );
		} while ( FindNextFile( hs, &wfd ) );
	}
	return 0;
}

