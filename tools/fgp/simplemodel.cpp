#include <fegdk/pch.h>
#include "fgp.h"
#include "simplemodel.h"

// simple model is an indexed list of triangles, with a separate list of material (texture) indices

using fe::uchar;

#	define handle_dword(dst) {\
		uchar tmp;\
			tmp = ( (uchar*)&dst)[0];\
			 ((uchar*)&dst)[0] = ( (uchar*)&dst)[3];\
			 ((uchar*)&dst)[3] = tmp;\
			tmp = ( (uchar*)&dst)[1];\
			 ((uchar*)&dst)[1] = ( (uchar*)&dst)[2];\
			 ((uchar*)&dst)[2] = tmp;\
	}

struct simpleModel
{
	struct vertex
	{
		bool operator == (const vertex &v) const
		{
			return pos == v.pos && uv == v.uv;
		}
		fe::vector3 pos;
		fe::vector2 uv;
	};
	struct face
	{
		int v[3];
	};
	std::vector <fe::vector3> mverts;
	std::vector <fe::vector2> muvs;
	std::vector <face> mfaces;
	std::vector <fe::uchar> mtextures;

	void convertFrom (fe::smartPtr <fe::expScene> s, fe::smartPtr <fe::expObject> oo)
	{

		if (s->getRootNode () == oo && oo->children ().size () == 1)
		{
			// discard rootnode, convert from child #1 instead
			oo = oo->children ().front ();
		}

		// fill vb
		if (oo->getClassID () == fe::TRI_OBJECT_CLASS_ID)
		{
			fe::smartPtr <fe::expTriObject> o = oo.dynamicCast <fe::expTriObject> ();
			// matrix
			fe::matrix4 worldMatrix = oo->getWorldMatrix ();

			int mtl_i;
			int face_i;
			int vertex_i;

			for (mtl_i = 0; mtl_i <= (int)s->mtlNameList ().size (); mtl_i++)
			{
				std::vector< vertex > verts;
				std::vector< face > faces;
				if (mtl_i == (int)s->mtlNameList ().size ())
					mtl_i = -1;
				for (face_i = 0; face_i < (int)o->faces ().size (); face_i++)
				{
					if (o->mtlIndex ().empty () || (!o->mtlIndex ().empty () && o->mtlIndex ()[face_i] == mtl_i))
					{
						// add verts
						fe::expFace *f = &o->faces ()[face_i];
						fe::expFace *tf = &o->texCoordFaces ()[face_i];

						face outface;

						for (vertex_i = 0; vertex_i < 3; vertex_i++)
						{
							vertex v;
							v.pos = o->verts ()[f->v[vertex_i]];
							v.uv = o->texCoords ().empty () ? fe::vector2 (0, 0) : o->texCoords ()[tf->v[vertex_i]];
							std::vector< vertex >::iterator it;
							it = std::find (verts.begin (), verts.end (), v);
							if (it != verts.end ())
								outface.v[vertex_i] = it - verts.begin ();
							else
							{
								outface.v[vertex_i] = (int)verts.size ();
								verts.push_back (v);
							}
						}
						faces.push_back (outface);
					}
				}
				if (false == verts.empty ())
				{
					// add subset
//					subset s;
					int firstVertex = (int)mverts.size ();
					int firstFace = (int)mfaces.size ();
					int numVerts = (int)mverts.size ();
					int numFaces = (int)mfaces.size ();
					int mtl = mtl_i;
					// add verts
					for (vertex_i = 0; vertex_i < (int)verts.size (); vertex_i++)
					{
						mverts.push_back (verts[vertex_i].pos * worldMatrix);
						muvs.push_back (verts[vertex_i].uv);
					}
					// add faces
					for (face_i = 0; face_i < (int)faces.size (); face_i++)
					{
						face ff;
						ff.v[0] = faces[face_i].v[0] + firstVertex;
						ff.v[1] = faces[face_i].v[1] + firstVertex;
						ff.v[2] = faces[face_i].v[2] + firstVertex;
						mfaces.push_back (ff);
						mtextures.push_back (mtl);
					}
				}

				if (mtl_i == -1)
					break;
			}
		}


#if 0
		// animation
		//	mpAnimationController = new fe::animationController;
		//	mpAnimationController->create (o->mFrameRate, o->mAnimation);
		mpAnimationController = fe::animationController::createFromMemory (oo->frameRate (), oo->animation ());
#endif

		// create d3dxmesh and optimize
		// ...

		for (int i = 0; i < (int)oo->children ().size (); i++)
		{
			convertFrom (s, oo->children ()[i]);
		}
	}

};

void
ConvertSimpleModel (const char *fname, int flags)
{
	_tprintf (_T ("loading model...\n"));

	fe::expScene *model = new fe::expScene ();
	model->load (fname);

	_tprintf (_T ("model ready, converting...\n"));

	simpleModel m;
	m.convertFrom (model, model->getRootNode ());

	// write
	char fn [200];
	char *dot = strchr (fname, '.');
	if (dot)
	{
		strncpy (fn, fname, dot - fname);
		fn[dot-fname] = 0;
	}
	else
		strcpy (fn, fname);
	strcat (fn, ".smd");

/*	std::vector <fe::vector3> mverts;
	std::vector <fe::vector2> muvs;
	std::vector <face> mfaces;
	std::vector <int> mtextures;*/

	FILE *fp = fopen (fn, "w+b");
	size_t i;

	i = m.mverts.size ();
	if (flags & SMF_BIGENDIAN)
		handle_dword (i);
	fwrite (&i, sizeof (i), 1, fp);
	for (i = 0; i < m.mverts.size (); i++)
	{
		if (flags & SMF_FIXED)
		{
			int xyz[3] = { (int) (m.mverts[i][0] * (1<<16)), (int) (m.mverts[i][1] * (1<<16)), (int) (m.mverts[i][2] * (1<<16))};

			if (flags & SMF_BIGENDIAN)
			{
				// swap bytes
				handle_dword (xyz[0]);
				handle_dword (xyz[1]);
				handle_dword (xyz[2]);
			}
			
			fwrite (xyz, sizeof (xyz), 1, fp);
		}
		else
		{
			float xyz[3] = {m.mverts[i][0], m.mverts[i][1], m.mverts[2][0]};

			if (flags & SMF_BIGENDIAN)
			{
				// swap bytes
				handle_dword (xyz[0]);
				handle_dword (xyz[1]);
				handle_dword (xyz[2]);
			}
			
			fwrite (xyz, sizeof (xyz), 1, fp);
		}
	}


	for (i = 0; i < m.muvs.size (); i++)
	{
		if (flags & SMF_FIXED)
		{
			int uv[2] = { (int) (m.muvs[i][0] * (1<<16)), (int) (m.muvs[i][1] * (1<<16))};

			if (flags & SMF_BIGENDIAN)
			{
				// swap bytes
				handle_dword (uv[0]);
				handle_dword (uv[1]);
			}
			
			fwrite (uv, sizeof (uv), 1, fp);
		}
		else
		{
			float uv[2] = {m.muvs[i][0], m.muvs[i][1]};

			if (flags & SMF_BIGENDIAN)
			{
				// swap bytes
				handle_dword (uv[0]);
				handle_dword (uv[1]);
			}
			
			fwrite (uv, sizeof (uv), 1, fp);
		}
	}

	i = m.mfaces.size ();
	if (flags & SMF_BIGENDIAN)
		handle_dword (i);
	fwrite (&i, sizeof (i), 1, fp);
	for (i = 0; i < m.mfaces.size (); i++)
	{
		int face[3] = {m.mfaces[i].v[0], m.mfaces[i].v[1], m.mfaces[i].v[2]};
		if (flags & SMF_BIGENDIAN)
		{
			// swap bytes
			handle_dword (face[0]);
			handle_dword (face[1]);
			handle_dword (face[2]);
		}
			
		fwrite (face, sizeof (face), 1, fp);
	}
	
	fwrite (&m.mtextures[0], m.mfaces.size (), 1, fp);

	// write texture names
	i = model->mtlList ().size ();
	handle_dword (i);
	fwrite (&i, sizeof (i), 1, fp);
	for (i = 0; i < model->mtlList ().size (); i++)
	{
		const fe::baseTexture *tex = model->mtlList ()[i]->getTexture (0);
		fe::cStr name = tex ? tex->name () : "";
		char fn[100];
		char *dot = strchr (name.c_str (), '.');
		if (dot)
		{
			strncpy (fn, name.c_str (), dot - name.c_str ());
			fn[dot - name.c_str ()] = 0;
		}
		else
			strcpy (fn, name.c_str ());

		fwrite (fn, strlen (fn) + 1, 1, fp);
	}
	fclose (fp);

	model->release ();
}
