#include <fegdk/pch.h>
#include "fgp.h"
#include <fegdk/f_error.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_application.h>
#include <fegdk/f_console.h>
#include "simplemodel.h"
#include "config.h"

// Log: main.cpp,v
//
// Revision 1.10  2004/11/10 01:22:32  waker
// newver
//
// Revision 1.9  2003/11/09 10:56:47  waker
// changed copyright message, added engine support (due to memleaks)
//
// Revision 1.8  2003/06/10 10:02:00  waker
// fixed fgp to recompile under new engine rev
//
// Revision 1.7  2002/12/02 17:10:45  waker
// help fixed a bit, played with tgs data
//
// Revision 1.6  2002/12/02 12:05:03  waker
// help updated, tiny code cleanup took place
//

void nextarg (int &i, int &argc)
{
	i++;
	if (i >= argc)
		throw fe::genericError ("invalid command line");
}

void printCommandsHelp (void)
{
	printf ("fgp commands are:\n");
	printf ("\tmcvt\tconverts model from text .mdl format to binary .bmdl format\n");
	printf ("\tsmd\tconverts model from text .mdl format to binary .smd format (simple model)\n");
	printf ("\tnmap\tbuilds normalmap texture from pair of hi and low poly models\n");
	printf (" (specify the --help global option for a list of other help options)\n");
}

void printOptionsHelp (void)
{
	printf ("No options available.\n");
}

void printCommandHelp (const char *cmd)
{
	if (!strcmp (cmd, "mcvt"))
	{
		printf ("Usage: fgp mcvt file.mdl\n");
	}
	else if (!strcmp (cmd, "nmap"))
	{
		printf ("Usage: fgp nmap texwidth texheight hipoly poly\n");
	}
	else if (!strcmp (cmd, "smd"))
	{
		printf ("Usage: fgp smd [-f [-be]] file.mdl\n");
		printf ("  -f    fixed-point format\n");
		printf ("  -be   big-endian byte order\n");
	}
	else
	{
		printCommandsHelp ();
	}
}

void printUsage ()
{
	printf ("usage: fgp.exe [options] command [command-options-and-arguments]\n");
	printf ("  where options are [...], etc.\n");
	printf ("    (specify --help-options for a list of options)\n");
	printf ("  where command are nmap, mcvt, etc.\n");
	printf ("    (specify --help-commands for a list of commands)\n");
	printf ("  where command-options-and-arguments depend on the specific command\n");
	printf ("    (specify --help followed by a command name for command-specific help)\n");
	printf ("  specify --help to recieve this message\n");
	printf ("\nfgp-%s (FE Geometry Processor)\n"), VERSION;
	printf ("written by waker (c) 2001-2005.\n");
	printf ("licensed under the terms of GNU General Public License.\n");
}

int execute (int argc, char *argv[])
{
	if (argc <= 1)
	{
		printUsage ();
		return -1;
	}

	// options

	// normalmap
	bool normalmap = false;
	int nmapw;
	int nmaph;
	fe::cStr hipolymdl;
	fe::cStr lopolymdl;

	// model conversion
	bool convertmodel = false;
	fe::cStr mdlname;

	// smd conversion
	bool convert_smd = false;
	int smd_flags = 0;

	// parse arguments
	for (int i = 1; i < argc; i++)
	{
		if (!strcmp (argv[i], "nmap"))
		{
			// create normal map
			nextarg (i, argc);
			nmapw = atoi (argv[i]);
			nextarg (i, argc);
			nmaph = atoi (argv[i]);
			nextarg (i, argc);
			hipolymdl = argv[i];
			nextarg (i, argc);
			lopolymdl = argv[i];

			normalmap = true;
		}
		else if (!strcmp (argv[i], "mcvt"))
		{
			// convert model to binary fmt
			nextarg (i, argc);
			printf ("setting mdlname to %s...\n", argv[i]);
			mdlname = argv[i];

			convertmodel = true;
		}
		else if (!strcmp (argv[i], "smd"))
		{
			// convert model to binary fmt

			smd_flags = 0;

			for (;;)
			{
				nextarg (i, argc);

				if (!strcmp (argv[i], "-f"))
					smd_flags |= SMF_FIXED;
				else if (!strcmp (argv[i], "-be"))
					smd_flags |= SMF_BIGENDIAN;
				else
					break;
			}

			mdlname = argv[i];

			convert_smd = true;
		}
		else if (!strcmp (argv[i], "--help"))
		{
			// print help msg
			if (i == argc-1)
				printUsage ();
			else
			{
				i++;
				printCommandHelp (argv[i]);
			}
		}
		else if (!strcmp (argv[i], "--help-commands"))
		{
			printCommandsHelp ();
		}
		else if (!strcmp (argv[i], "--help-options"))
		{
			printOptionsHelp ();
		}
	}

	if (normalmap)
	{
		printf ("normalmap generation is temporarily disabled\n");
	//	ExportNormalMap (nmapw, nmaph, hipolymdl, lopolymdl);
	}

	if (convertmodel)
		ConvertModel (mdlname);

	if (convert_smd)
		ConvertSimpleModel (mdlname, smd_flags);

//	MakeLevelFile (argc, argv);
	return 0;
}

class DummyApp : public fe::application
{
public:
	DummyApp (void)
	{
	}
	~DummyApp (void)
	{
	}
	void configure (void)
	{
		fe::g_engine->getConsole ()->command ("sys_int_mainloop 0");
		fe::g_engine->getConsole ()->command ("sys_profile null");
	}
	void init (void)
	{
	}
	void free (void)
	{
	}
	const char* appName (void) const
	{
		return PACKAGE " " VERSION;
	}
	const char* dataPath (void) const
	{
		return "./";
	}

};

int main (int argc, char *argv[])
{
	fe::engine *eng = new fe::engine ();
	DummyApp *app = new DummyApp ();
	eng->run (app, argc, argv);
	int res = execute (argc, argv);
	eng->release ();
#if defined(_DEBUG) && defined(_WIN32)
	_CrtDumpMemoryLeaks ();
#endif
	return res;
}
