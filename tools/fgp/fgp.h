#ifndef __FGP_H
#define __FGP_H

#include <fegdk/f_expmodel.h>
#include <fegdk/f_sceneobject.h>
#include <fegdk/f_effect.h>
#include <fegdk/f_basetexture.h>

//-------------------------------------------------
// normalmap
//-------------------------------------------------

void ExportNormalMap( int nmapw, int nmaph, const char *hipolymdl, const char *lopolymdl );

//-------------------------------------------------
// model
//-------------------------------------------------

void ConvertModel( const char *modelname );

//-------------------------------------------------
// level
//-------------------------------------------------

void MakeLevelFile( int argc, TCHAR *argv[] );

//-------------------------------------------------
// util
//-------------------------------------------------

bool Vector2_EQ( const fe::vector2 &v1, const fe::vector2 &v2 );
bool Vector3_EQ( const fe::vector3 &v1, const fe::vector3 &v2 );
bool Vector4_EQ( const float *v1, const float *v2 );

#endif

