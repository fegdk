#include <fegdk/pch.h>
#include "fgp.h"
#include "normalmapper.h"

void ExportNormalMap( int nmapw, int nmaph, const char *hipolymdl, const char *lowpolymdl )
{
	// load models
	printf( "loading hipoly model '%s'...\n", hipolymdl );
	feExpScene *modelHi = feExpScene::create();
	modelHi->load( hipolymdl );
	printf( "loading lowpoly model '%s'...\n", lowpolymdl );
	feExpScene *modelLow = feExpScene::create();
	modelLow->load( lowpolymdl );

	fgpNormalMapper nmapper( lowpolymdl, modelHi, modelLow, nmapw, nmaph );
	nmapper.run();

	// free models
	modelHi->release();
	modelLow->release();
}

