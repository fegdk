// $Id$

#ifndef __SIMPLEMODEL_H
#define __SIMPLEMODEL_H

// flags
#define SMF_FIXED		0x00000001
#define SMF_BIGENDIAN	0x00000002

void
ConvertSimpleModel (const char *modelname, int flags);

#endif
