// $Id$
#include <fegdk/pch.h>
#include "fgp.h"
#include <fegdk/f_error.h>
#include "normalmapper.h"
#include <fegdk/f_helpers.h>
#include "NmFileIO.h"

#define INT_ROUND_TEXCOORD_U(X)  (int)(((X)*(float)(mXRes-1))+0.5f)
#define INT_ROUND_TEXCOORD_V(X)  (int)(((X)*(float)(mYRes-1))+0.5f)

typedef union
{
   struct { float x, y; } xy;
   struct { float v[2]; } vv;
} Sample;

//////////////////////////////////////////////////////////////////////////////
// Get the sample offsets for supersampling.
//////////////////////////////////////////////////////////////////////////////
Sample*
GetSamples (int numSamples)
{
   static Sample s1[] = { {0.0f, 0.0f} };
   switch (numSamples)
   {
      case 1:
         {
            return s1;
         }
      case 2:
         {
            static Sample s2[2] = { {0.5f, 0.5f},
                                    {-0.5f, -0.5f} };
            return s2;
         }
      case 3:
         {
            static Sample s3[3] = { {0.5f, 0.5f},
                                    {0.0f, 0.0f},
                                    {-0.5f, -0.5f} };
            return s3;
         }
      case 4:
         {
            static Sample s4[4] = { {0.5f, 0.5f}, 
                                    {-0.5f, 0.5f},
                                    {0.5f, -0.5f},
                                    {-0.5f, -0.5f} };
            return s4;
         }
      case 5:
         {
            static Sample s5[5] = { {0.5f, 0.5f},
                                    {-0.5f, 0.5f},
                                    {0.0f, 0.0f},
                                    {0.5f, -0.5f},
                                    {-0.5f, -0.5f} };
            return s5;
         }
      case 9:
         {
            static Sample s9[9] = { {0.5f, 0.5f},
                                    {0.0f, 0.5f},
                                    {-0.5f, 0.5f},
                                    {-0.5f, 0.0f},
                                    {0.0f, 0.0f},
                                    {0.5f, 0.0f},
                                    {0.5f, -0.5f},
                                    {0.0f, -0.5f},
                                    {-0.5f, -0.5f} };
            return s9;
         }
      case 13:
         {
            static Sample s13[13] = { { 0.33f, 0.33f},
                                      { 0.0f,  0.33f},
                                      {-0.33f, 0.33f},
                                      {-0.33f, 0.0f},
                                      { 0.0f,  0.0f},
                                      { 0.33f, 0.0f},
                                      { 0.33f,-0.33f},
                                      { 0.0f, -0.33f},
                                      {-0.33f,-0.33f},
                                      { 0.66f, 0.00f},
                                      {-0.66f, 0.00f},
                                      { 0.00f, 0.66f},
                                      { 0.00f, 0.66f}};
            return s13;
         }
      case 21:
         {
            static Sample s21[21] = { { 0.33f, 0.33f},
                                      { 0.0f,  0.33f},
                                      {-0.33f, 0.33f},
                                      {-0.33f, 0.0f},
                                      { 0.0f,  0.0f},
                                      { 0.33f, 0.0f},
                                      { 0.33f,-0.33f},
                                      { 0.0f, -0.33f},
                                      {-0.33f,-0.33f},
                                      { 0.66f, 0.00f},
                                      { 0.66f, 0.33f},
                                      { 0.66f,-0.33f},
                                      {-0.66f, 0.00f},
                                      {-0.66f, 0.33f},
                                      {-0.66f,-0.33f},
                                      { 0.00f, 0.66f},
                                      { 0.33f, 0.66f},
                                      {-0.33f, 0.66f},
                                      { 0.33f,-0.66f},
                                      {-0.33f,-0.66f},
                                      { 0.00f,-0.66f}};
            return s21;
         }
      case 37:
         {
            static Sample s37[37] = { { 0.25f, 0.25f},
                                      { 0.0f,  0.25f},
                                      {-0.25f, 0.25f},
                                      {-0.25f, 0.0f},
                                      { 0.0f,  0.0f},
                                      { 0.25f, 0.0f},
                                      { 0.25f,-0.25f},
                                      { 0.0f, -0.25f},
                                      {-0.25f,-0.25f},
                                      { 0.50f, 0.00f},
                                      { 0.50f, 0.25f},
                                      { 0.50f,-0.25f},
                                      {-0.50f, 0.00f},
                                      {-0.50f, 0.25f},
                                      {-0.50f,-0.25f},
                                      { 0.00f, 0.50f},
                                      { 0.25f, 0.50f},
                                      {-0.25f, 0.50f},
                                      { 0.25f,-0.50f},
                                      {-0.25f,-0.50f},
                                      { 0.00f,-0.50f},
                                      { 0.75f, 0.25f},
                                      { 0.75f, 0.00f},
                                      { 0.75f,-0.25f},
                                      { 0.50f, 0.50f},
                                      { 0.50f,-0.50f},
                                      { 0.25f, 0.75f},
                                      { 0.25f,-0.75f},
                                      { 0.00f, 0.75f},
                                      { 0.00f,-0.75f},
                                      {-0.25f, 0.75f},
                                      {-0.25f,-0.75f},
                                      {-0.50f, 0.50f},
                                      {-0.50f,-0.50f},
                                      {-0.75f, 0.25f},
                                      {-0.75f, 0.00f},
                                      {-0.75f,-0.25f}};
            return s37;
         }
      case 57:
         {
            static Sample s57[57] = { {-0.8f, 0.2f},
                                      {-0.8f, 0.0f},
                                      {-0.8f,-0.2f},
                                      {-0.6f, 0.4f},
                                      {-0.6f, 0.2f},
                                      {-0.6f, 0.0f},
                                      {-0.6f,-0.2f},
                                      {-0.6f,-0.4f},
                                      {-0.4f, 0.6f},
                                      {-0.4f, 0.4f},
                                      {-0.4f, 0.2f},
                                      {-0.4f, 0.0f},
                                      {-0.4f,-0.2f},
                                      {-0.4f,-0.4f},
                                      {-0.4f,-0.6f},
                                      {-0.2f, 0.8f},
                                      {-0.2f, 0.6f},
                                      {-0.2f, 0.4f},
                                      {-0.2f, 0.2f},
                                      {-0.2f, 0.0f},
                                      {-0.2f,-0.2f},
                                      {-0.2f,-0.4f},
                                      {-0.2f,-0.6f},
                                      {-0.2f,-0.8f},
                                      {-0.0f, 0.8f},
                                      {-0.0f, 0.6f},
                                      {-0.0f, 0.4f},
                                      {-0.0f, 0.2f},
                                      {-0.0f, 0.0f},
                                      {-0.0f,-0.2f},
                                      {-0.0f,-0.4f},
                                      {-0.0f,-0.6f},
                                      {-0.0f,-0.8f},
                                      { 0.2f, 0.8f},
                                      { 0.2f, 0.6f},
                                      { 0.2f, 0.4f},
                                      { 0.2f, 0.2f},
                                      { 0.2f, 0.0f},
                                      { 0.2f,-0.2f},
                                      { 0.2f,-0.4f},
                                      { 0.2f,-0.6f},
                                      { 0.2f,-0.8f},
                                      { 0.4f, 0.6f},
                                      { 0.4f, 0.4f},
                                      { 0.4f, 0.2f},
                                      { 0.4f, 0.0f},
                                      { 0.4f,-0.2f},
                                      { 0.4f,-0.4f},
                                      { 0.4f,-0.6f},
                                      { 0.6f, 0.4f},
                                      { 0.6f, 0.2f},
                                      { 0.6f, 0.0f},
                                      { 0.6f,-0.2f},
                                      { 0.6f,-0.4f},
                                      { 0.8f, 0.2f},
                                      { 0.8f, 0.0f},
                                      { 0.8f,-0.2f}};
            return s57;
         }
   }
   return s1;
}

fgpNormalMapper::fgpNormalMapper( const feStr &name, feExpScene *hi, feExpScene *low, int xres, int yres )
{
	mModelName = name;
	mpMdlHi = hi;
	mpMdlLow = low;
	mXRes = xres;
	mYRes = yres;
}

fgpNormalMapper::~fgpNormalMapper( void )
{
	std::map< feCStr, texture_t * >::iterator it;
	for ( it = mTextures.begin(); it != mTextures.end(); it++ )
	{
		delete ( *it ).second;
	}
	mTextures.clear();
}

void fgpNormalMapper::run( void )
{
	// recurse
	run( mpMdlHi->getRootNode(), mpMdlLow->getRootNode() );
	expandTexels();
	boxFilter();
	saveMaps();
//	TCHAR fn[_MAX_FNAME];
//	_tsplitpath( mModelName.c_str(), NULL, NULL, fn, NULL );
//	saveTangentSpace( mpMdlLow->getRootNode(), fn );
}

bool fgpNormalMapper::check( feExpObject *_hi, feExpObject *_low )
{
	if ( _hi->getClassID() == _low->getClassID() && _hi->getClassID() == TRI_OBJECT_CLASS_ID )
	{
		feExpTriObject *low = checked_cast< feExpTriObject * >( _low );
		feExpTriObject *hi = checked_cast< feExpTriObject * >( _hi );
		if ( low->verts().empty() && !hi->verts().empty() )
		{
			printf( "warning: hipoly version contains verts why lowpoly not (%s).\n", hi->name() );
		}
		if ( !low->verts().empty() && hi->verts().empty() )
		{
			printf( "warning: lowpoly version contains verts why hipoly not (%s).\n", hi->name() );
		}

		// check texcoords to be in range 0..1
		size_t i;
		for ( i = 0; i < low->texCoords().size(); i++ )
		{
			if ( low->texCoords()[i].x < 0 || low->texCoords()[i].x > 1 )
			{
				printf( "warning: lowpoly contains out-of-range texcoords. must be in [0..1] range.\n" );
				break;
			}
		}
		if ( i < low->texCoords().size() )
			return false;

		return ( !low->verts().empty() && !hi->verts().empty() );
	}
	return false;
}

// NOTE: WORKS ONLY WITH UNIQUE TEXTURING
void fgpNormalMapper::run( feExpObject *hi, feExpObject *low )
{
	if ( check( hi, low ) )
	{
		checked_cast< feExpTriObject * >( hi )->transformIntoWorldSpace();
		checked_cast< feExpTriObject * >( low )->transformIntoWorldSpace();

		std::vector< feVector3 > &hiverts = checked_cast< feExpTriObject * >( hi )->verts();
		std::vector< feVector3 > &lowverts = checked_cast< feExpTriObject * >( low )->verts();

		// show mins/maxs
		feVector3 mins, maxs;
		size_t i;
		mins = feVector3( 1.0e+5f, 1.0e+5f, 1.0e+5f );
		maxs = feVector3( -1.0e+5f, -1.0e+5f, -1.0e+5f );
		for ( i = 0; i < hiverts.size(); i++ )
		{
			if ( hiverts[i].x > maxs.x ) maxs.x = hiverts[i].x;
			if ( hiverts[i].y > maxs.y ) maxs.y = hiverts[i].y;
			if ( hiverts[i].z > maxs.z ) maxs.z = hiverts[i].z;
			if ( hiverts[i].x < mins.x ) mins.x = hiverts[i].x;
			if ( hiverts[i].y < mins.y ) mins.y = hiverts[i].y;
			if ( hiverts[i].z < mins.z ) mins.z = hiverts[i].z;
		}
		_tprintf( _T( "hipoly bounds are: (%.2f %.2f %.2f) - (%.2f %.2f %.2f)\n" ), mins.x, mins.y, mins.z, maxs.x, maxs.y, maxs.z );
		mins = feVector3( 1.0e+5f, 1.0e+5f, 1.0e+5f );
		maxs = feVector3( -1.0e+5f, -1.0e+5f, -1.0e+5f );
		for ( i = 0; i < lowverts.size(); i++ )
		{
			if ( lowverts[i].x > maxs.x ) maxs.x = lowverts[i].x;
			if ( lowverts[i].y > maxs.y ) maxs.y = lowverts[i].y;
			if ( lowverts[i].z > maxs.z ) maxs.z = lowverts[i].z;
			if ( lowverts[i].x < mins.x ) mins.x = lowverts[i].x;
			if ( lowverts[i].y < mins.y ) mins.y = lowverts[i].y;
			if ( lowverts[i].z < mins.z ) mins.z = lowverts[i].z;
		}
		_tprintf( _T( "lowpoly bounds are: (%.2f %.2f %.2f) - (%.2f %.2f %.2f)\n" ), mins.x, mins.y, mins.z, maxs.x, maxs.y, maxs.z );

		computeTangents( checked_cast< feExpTriObject * >( hi ), checked_cast< feExpTriObject * >( low ) );
		computePVS( checked_cast< feExpTriObject * >( hi ), checked_cast< feExpTriObject * >( low ) );
		updateMaps( checked_cast< feExpTriObject * >( hi ), checked_cast< feExpTriObject * >( low ) );
	}

	// recurse
	for ( size_t i = 0; i < hi->children().size(); i++ )
	{
		feExpTriObject *hic = ( feExpTriObject * )hi->children()[i];
		for ( size_t j = 0; j < low->children().size(); j++ )
		{
			feExpTriObject *lowc = ( feExpTriObject * )low->children()[j];
			if ( !strcmp( hic->name(), lowc->name() ) )
			{
				// recurse
				run( hic, lowc );
				break;
			}
		}
	}
}

void fgpNormalMapper::computePlaneEq( fePlane &plane, const feVector3 &v0, const feVector3 &v1, const feVector3 &norm )
{
	plane.normal() = ( v1 - v0 ).cross( norm );
	float l = plane.normal().length();
	if ( l < 1.0e-7f )
	{
		plane.normal() = feVector3( 0, 0, 1 );
	}
	else
	{
		plane.normal() /= l;
	}
	plane.constant() = plane.normal().dot( v0 );
}

bool fgpNormalMapper::faceVisible( const feVector3 pos[3], const fePlane &plane )
{
	if ( pos[0].dot( plane.normal() ) >= plane.constant()  )
		return true;
	if ( pos[1].dot( plane.normal() ) >= plane.constant()  )
		return true;
	if ( pos[2].dot( plane.normal() ) >= plane.constant()  )
		return true;
	return false;
}

void fgpNormalMapper::computePVS( feExpTriObject *hi, feExpTriObject *low )
{
	printf( "computing pvs for mesh '%s'...\n", low->name() );
	mPVS.clear();
	mPVS.resize( low->faces().size() );
	for ( size_t face_i = 0; face_i < low->faces().size(); face_i++ )
	{
		printf( "face %d/%d\r", face_i+1, low->faces().size() );
		// compute frustum & check agains hipoly faces
		feVector3 pos[3] = { low->verts()[low->faces()[face_i].v[0]], low->verts()[low->faces()[face_i].v[1]], low->verts()[low->faces()[face_i].v[2]] };
		feVector3 norm[3] = { low->normals()[low->normalFaces()[face_i].v[0]], low->normals()[low->normalFaces()[face_i].v[1]], low->normals()[low->normalFaces()[face_i].v[2]] };
		fePlane planes[6];
		computePlaneEq( planes[0], pos[1], pos[0], norm[0] );
		computePlaneEq( planes[1], pos[1], pos[0], norm[1] );
		computePlaneEq( planes[2], pos[2], pos[1], norm[1] );
		computePlaneEq( planes[3], pos[2], pos[1], norm[2] );
		computePlaneEq( planes[4], pos[0], pos[2], norm[2] );
		computePlaneEq( planes[5], pos[0], pos[2], norm[0] );

		for ( size_t faceh_i = 0; faceh_i < hi->faces().size(); faceh_i++ )
		{
			feVector3 pos[3] = { hi->verts()[hi->faces()[faceh_i].v[0]], hi->verts()[hi->faces()[faceh_i].v[1]], hi->verts()[hi->faces()[faceh_i].v[2]] };

			if ( (faceVisible (pos, planes[0]) || faceVisible (pos, planes[1])) &&
				(faceVisible (pos, planes[2]) || faceVisible (pos, planes[3])) &&
				(faceVisible (pos, planes[4]) || faceVisible (pos, planes[5])) )
			{
				mPVS[face_i].tris.push_back( (int)faceh_i );
			}
		}
//		if ( mPVS[face_i].tris.empty() )
//		{
//			throw feGenericError( _T( "\nno visible faces for face %d\n" ), face_i );
//		}
//		else
//			_tprintf( _T( "\npvs for face %d is %d\n" ), face_i, mPVS[face_i].tris.size() );
	}
	_tprintf( _T( "\r                \r" ) );
}

/*float fgpNormalMapper::intRoundTexCoordU( float u )
{
	return u * ( float )( mXRes - 1 ) + .5f;
}

float fgpNormalMapper::intRoundTexCoordV( float v )
{
	return v * ( float )( mYRes - 1 ) + .5f;
}
*/
void fgpNormalMapper::getEdge( edge_t &edge, feVector2 tri[3], int idx0, int idx1 )
{
	// Based on the Y/v coordinate fill in the structure.
	if ( tri[idx0].y <= tri[idx1].y )
	{
		edge.idx0 = idx0;
		edge.idx1 = idx1;
		edge.yMin = INT_ROUND_TEXCOORD_V( tri[idx0].y );
		edge.yMax = INT_ROUND_TEXCOORD_V( tri[idx1].y );
		edge.x = INT_ROUND_TEXCOORD_U( tri[idx0].x );
		edge.x1 = INT_ROUND_TEXCOORD_U( tri[idx1].x );
		edge.increment = edge.denominator = edge.yMax - edge.yMin;
		edge.numerator = edge.x1 - edge.x;
	}
	else
	{
		edge.idx0 = idx1;
		edge.idx1 = idx0;
		edge.yMin = INT_ROUND_TEXCOORD_V( tri[idx1].y );
		edge.yMax = INT_ROUND_TEXCOORD_V( tri[idx0].y );
		edge.x = INT_ROUND_TEXCOORD_U( tri[idx1].x );
		edge.x1 = INT_ROUND_TEXCOORD_U( tri[idx0].x );
		edge.increment = edge.denominator = edge.yMax - edge.yMin;
		edge.numerator = edge.x1 - edge.x;
	}
}

bool fgpNormalMapper::intersectTri( const FVector3 &orig, const FVector3 &dir,
				  const FVector3 tri[3], float &t, float &u, float &v )
{
	FVector3 edge1, edge2, tvec, pvec, qvec;
	float det, inv_det;

	// find vectors for two edges sharing vert0
	edge1 = tri[1] - tri[0];
	edge2 = tri[2] - tri[0];

	// begin calculating determinant - also used to calculate U parameter
	pvec = dir.cross( edge2 );

	// if determinant is near zero, ray lies in plane of triangle
	det = edge1.dot( pvec );
	if ( det == 0.0 )
	{
		return false;
	}
	inv_det = 1.0 / det;

	// calculate distance from vert0 to ray origin
	tvec = orig - tri[0];

	// calculate U parameter and test bounds
	u = tvec.dot( pvec ) * inv_det;
	if ( u < -0 || u > (1.0 + 0.0))
	{
		return false;
	}

	// prepare to test V parameter
	qvec = tvec.cross( edge1 );

	// calculate V parameter and test bounds
	v = dir.dot( qvec ) * inv_det;
	if ( v < -0.0 || ( u + v ) > ( 1.0 + 0.0 ) )
	{
		return false;
	}

	// calculate t, ray intersects triangle
	t = edge2.dot( qvec ) * inv_det;

	return true;
}

void fgpNormalMapper::updateMaps( feExpTriObject *hi, feExpTriObject *low )
{
	int overdraw = 0;
	int numpixels = 0;
	// Get samples for supersampling
	int numsamples = 1;
	Sample* samples = GetSamples( numsamples );

	float minNorm[3] = { 1.0e+5f, 1.0e+5f, 1.0e+5f };
	float maxNorm[3] = { -1.0e+5f, -1.0e+5f, -1.0e+5f };

	// face-by-face
	printf( "computing normals for mesh '%s'...\n", low->name() );
	for ( size_t face_i = 0; face_i < low->faces().size(); face_i++ )
//	for ( int face_i = (int)low->faces().size()-1; face_i >= 0 ; face_i-- )
	{
		_tprintf( _T( "face %d/%d\r" ), face_i+1, low->faces().size() );
		const char *name = mpMdlLow->mtlNameList()[low->mtlIndex()[face_i]];
		std::map< feCStr, texture_t * >::iterator it = mTextures.find( name );
		texture_t *tex = NULL;
		if ( it == mTextures.end() )
		{
			// alloc new
			tex = new texture_t;
			tex->normals.resize( mXRes * mYRes );
			memset( &tex->normals.front(), 0, tex->normals.size() * sizeof( FVector3 ) );
			mTextures[ mpMdlLow->mtlNameList()[low->mtlIndex()[face_i]] ] = tex;
		}
		else
			tex = ( *it ).second;

		feVector3 tpos[3] = { low->verts()[low->faces()[face_i].v[0]], low->verts()[low->faces()[face_i].v[1]], low->verts()[low->faces()[face_i].v[2]] };
		feVector3 tnorm[3] = { low->normals()[low->normalFaces()[face_i].v[0]], low->normals()[low->normalFaces()[face_i].v[1]], low->normals()[low->normalFaces()[face_i].v[2]] };
		feVector2 tuv[3] = { low->texCoords()[low->texCoordFaces()[face_i].v[0]], low->texCoords()[low->texCoordFaces()[face_i].v[1]], low->texCoords()[low->texCoordFaces()[face_i].v[2]] };

		// interpolate pos & norm

		// calc texture-space coords of tri vertices

/*		feVector2	texpos[3] = {
			feVector2( intRoundTexCoordU( tuv[0].x ), intRoundTexCoordV( tuv[0].y ) ),
			feVector2( intRoundTexCoordU( tuv[1].x ), intRoundTexCoordV( tuv[1].y ) ),
			feVector2( intRoundTexCoordU( tuv[2].x ), intRoundTexCoordV( tuv[2].y ) )
		};*/

		// get edges and sort by min y
		edge_t edges[3];
		edge_t tmp;
		getEdge( edges[0], tuv, 0, 1 );
		getEdge( edges[1], tuv, 0, 2 );
		getEdge( edges[2], tuv, 1, 2 );
		if ( edges[2].yMin < edges[1].yMin )
		{
			tmp = edges[1];
			edges[1] = edges[2];
			edges[2] = tmp;
		}
		if ( edges[1].yMin < edges[0].yMin )
		{
			tmp = edges[0];
			edges[0] = edges[1];
			edges[1] = tmp;
		}

		// find initial barycentric parameter
		float x1 = (float)INT_ROUND_TEXCOORD_U( tuv[0].x );
		float x2 = (float)INT_ROUND_TEXCOORD_U( tuv[1].x );
		float x3 = (float)INT_ROUND_TEXCOORD_U( tuv[2].x );
		float y1 = (float)INT_ROUND_TEXCOORD_V( tuv[0].y );
		float y2 = (float)INT_ROUND_TEXCOORD_V( tuv[1].y );
		float y3 = (float)INT_ROUND_TEXCOORD_V( tuv[2].y );
		float b0 = ((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1)); 

		// find min/max y
		int minY = edges[0].yMin;
		int maxY = edges[0].yMax;
		if ( edges[1].yMax > maxY )
			maxY = edges[1].yMax;
		if ( edges[2].yMax > maxY )
			maxY = edges[2].yMax;

		// Now loop over Ys doing each "scanline"
		for ( int y = minY; y <= maxY; y++ )
		{
			int minX = 32767;
			int maxX = 0;
			for (int e = 0; e < 3; e++)
			{
				// See if this edge is active.
				if ((edges[e].yMin <= y) && (edges[e].yMax >= y))
				{
					// Check it's X values to see if they are min or max.
					if (edges[e].x < minX)
					{
						minX = edges[e].x;
					}
					if (edges[e].x > maxX)
					{
						maxX = edges[e].x;
					}

					// update x for next scanline
					edges[e].increment += edges[e].numerator;
					if (edges[e].denominator != 0)
					{
						if (edges[e].numerator < 0)
						{
							while (edges[e].increment <= 0)
							{
								edges[e].x--;
								edges[e].increment += edges[e].denominator;
							}
						}
						else
						{
							while (edges[e].increment > edges[e].denominator)
							{
								edges[e].x++;
								edges[e].increment -= edges[e].denominator;
							}
						}
					}
				} // end if edge is active
			} // end for number of edges

			// Loop over the Xs filling in each pixel of the normal map
			for (int x = minX; x <= maxX; x++)
			{
				// Find Barycentric coordinates.
				float b1 = ((x2-x) * (y3-y) - (x3-x) * (y2-y)) / b0;
				float b2 = ((x3-x) * (y1-y) - (x1-x) * (y3-y)) / b0;
				float b3 = ((x1-x) * (y2-y) - (x2-x) * (y1-y)) / b0;

				// Interpolate tangent space.
				feExpFace &tsf = low->tsFaces()[face_i];

				FVector3 ts_[3];
				ts_[0] = low->tsVerts()[tsf.v[0]].tangent * b1 + low->tsVerts()[tsf.v[1]].tangent * b2 + low->tsVerts()[tsf.v[2]].tangent * b3;
				ts_[1] = low->tsVerts()[tsf.v[0]].binormal * b1 + low->tsVerts()[tsf.v[1]].binormal * b2 + low->tsVerts()[tsf.v[2]].binormal * b3;
				ts_[2] = ts_[0].cross( ts_[1] );

/*				FVector3 ts[3];

				// normals will be in objects space

				ts[0][0] = ts_[0][0];
				ts[1][0] = ts_[0][1];
				ts[2][0] = ts_[0][2];
				ts[0][1] = ts_[1][0];
				ts[1][1] = ts_[1][1];
				ts[2][1] = ts_[1][2];
				ts[0][2] = ts_[2][0];
				ts[1][2] = ts_[2][1];
				ts[2][2] = ts_[2][2];*/

				feMatrix4 ts( true );
				ts._11 = ts_[0][0];
				ts._12 = ts_[0][1];
				ts._13 = ts_[0][2];
				ts._21 = ts_[1][0];
				ts._22 = ts_[1][1];
				ts._23 = ts_[1][2];
				ts._31 = ts_[2][0];
				ts._32 = ts_[2][1];
				ts._33 = ts_[2][2];

				// For each sample accumulate the normal
				FVector3 sNorm( 0, 0, 0 );
				int sFound = 0;
				for (int s = 0; s < numsamples; s++)
				{
					// Compute new x & y
					float sX = (float)x + samples[s].xy.x;
					float sY = (float)y + samples[s].xy.y;
//					float sX = ( float )x;
//					float sY = ( float )y;

					// Find Barycentric coordinates.
					float b1 = ((x2-sX) * (y3-sY) - (x3-sX) * (y2-sY)) / b0;
					float b2 = ((x3-sX) * (y1-sY) - (x1-sX) * (y3-sY)) / b0;
					float b3 = ((x1-sX) * (y2-sY) - (x2-sX) * (y1-sY)) / b0;

					// Compute position
					FVector3 pos;
					pos.x = (tpos[0].x * b1) + (tpos[1].x * b2) +
						(tpos[2].x * b3);
					pos.y = (tpos[0].y * b1) + (tpos[1].y * b2) +
						(tpos[2].y * b3);
					pos.z = (tpos[0].z * b1) + (tpos[1].z * b2) +
						(tpos[2].z * b3);

					// Compute normal
					FVector3 norm;
					norm.x = (tnorm[0].x * b1) + (tnorm[1].x * b2) +
						(tnorm[2].x * b3);
					norm.y = (tnorm[0].y * b1) + (tnorm[1].y * b2) +
						(tnorm[2].y * b3);
					norm.z = (tnorm[0].z * b1) + (tnorm[1].z * b2) +
						(tnorm[2].z * b3);

					// Find intersection with high poly model.
					bool first = true;
					FVector3 intersect;
					FVector3 lastIntersect( 1.0e+5, 1.0e+5, 1.0e+5 );
					FVector3 newNorm( 0, 0, 0 );
					FVector3 lastNorm( 0, 0, 0 );

					for ( size_t h = 0; h < mPVS[face_i].tris.size(); h++ )
					{
						// Find closest intersection

						FVector3 hpos[3] = {
							hi->verts()[hi->faces()[mPVS[face_i].tris[h]].v[0]],
							hi->verts()[hi->faces()[mPVS[face_i].tris[h]].v[1]],
							hi->verts()[hi->faces()[mPVS[face_i].tris[h]].v[2]]
						};
						FVector3 hnorm[3] = {
							hi->normals()[hi->normalFaces()[mPVS[face_i].tris[h]].v[0]],
							hi->normals()[hi->normalFaces()[mPVS[face_i].tris[h]].v[1]],
							hi->normals()[hi->normalFaces()[mPVS[face_i].tris[h]].v[2]]
						};

                        if ( intersectTri( pos, norm, hpos, intersect.x, intersect.y, intersect.z) )
						{
							// Figure out new normal so we can test if it's
							//  facing away from the current normal
							float b0 = 1.0 - intersect.y - intersect.z;
							FVector3 nn;
							nn.x = ( hnorm[0].x * b0) +
								(hnorm[1].x * intersect.y) +
								(hnorm[2].x * intersect.z);
							nn.y = (hnorm[0].y * b0) +
								(hnorm[1].y * intersect.y)+
								(hnorm[2].y * intersect.z);
							nn.z = (hnorm[0].z * b0) +
								(hnorm[1].z * intersect.y) +
								(hnorm[2].z * intersect.z);
							nn.normalize();

							FVector3 intr(
								hpos[0].x * b0 + hpos[1].x * intersect[1] + hpos[2].x * intersect[2],
								hpos[0].y * b0 + hpos[1].y * intersect[1] + hpos[2].y * intersect[2],
								hpos[0].z * b0 + hpos[1].z * intersect[1] + hpos[2].z * intersect[2]
							);

							if ( nn.dot( norm ) > .0f )
							{
								float gDistance = 1.0e+5;
								if ( first )
								{
									first = false;
									newNorm = nn;
									lastIntersect = intersect;
								}
								else if ( (fabs(intersect.x) < gDistance) &&
									(fabs(intersect.x) < fabs(lastIntersect.x)) )
								{
									newNorm = nn;
									lastIntersect = intersect;
								}
								else if ( fabs(intersect.x) < gDistance
									&& fabs(intersect.x) == fabs(lastIntersect.x)
									&& nn.dot( norm ) >	lastNorm.dot( norm ) )
								{
									newNorm = nn;
									lastIntersect = intersect;
								}
							} // end if this intersection is better
						}
					} // end for triangles in the high res model

					// Add this normal into our normal
					if ((newNorm.x != 0.0) ||
						(newNorm.y != 0.0) ||
						(newNorm.z != 0.0) )
					{
						sNorm += newNorm;
//						sNorm = norm;
						sFound++;
					}
				} // end for number of samples

				if (sFound > 0)
				{
					// convert to object space
					feMatrix4 m = low->getWorldMatrix().inverse();
					sNorm = m.transform( sNorm );

					// convert to tangent space
//					convertToTangentSpace( ts, sNorm, sNorm );
//					sNorm = ts.inverse().transform( sNorm );
					sNorm.normalize();

					// clamp
					sNorm.x = min( 1, max( -1, sNorm.x ) );
					sNorm.y = min( 1, max( -1, sNorm.y ) );
					sNorm.z = min( 1, max( -1, sNorm.z ) );

					if ( sNorm.x < minNorm[0] )
						minNorm[0] = sNorm.x;
					if ( sNorm.y < minNorm[1] )
						minNorm[1] = sNorm.y;
					if ( sNorm.z < minNorm[2] )
						minNorm[2] = sNorm.z;
					if ( sNorm.x > maxNorm[0] )
						maxNorm[0] = sNorm.x;
					if ( sNorm.y > maxNorm[1] )
						maxNorm[1] = sNorm.y;
					if ( sNorm.z > maxNorm[2] )
						maxNorm[2] = sNorm.z;

					// Save into output image.
					int idx = y * mXRes + x;
					FVector3 cmp;
					memset( &cmp, 0, sizeof( cmp ) );
					if ( !memcmp( &tex->normals[idx], &cmp, sizeof( cmp ) ) )
						tex->normals[idx] = sNorm;
					else
					{
//						_tprintf( _T( "non-unique texturing detected @ (%d;%d)!\n" ), x, y );
						overdraw++;
					}
					numpixels++;
				}
			}
		}
	} // end for lowres tris

	_tprintf( _T( "overdraw: %d %%\n" ), ( int )( 100.f / numpixels * overdraw ) );

	_tprintf( _T( "minimal normal: %lf %lf %lf\n" ), minNorm[0], minNorm[1], minNorm[2] );
	_tprintf( _T( "maximal normal: %lf %lf %lf\n" ), maxNorm[0], maxNorm[1], maxNorm[2] );
}

void fgpNormalMapper::saveMaps( void )
{
	std::map< feCStr, texture_t * >::iterator it;
	int i = 0;
	for ( it = mTextures.begin(); it != mTextures.end(); it++, i++ )
	{
		texture_t *tex = (*it).second;
		// convert to rgb
		uchar *rgb = new uchar[mXRes*mYRes*3];
		for ( int x = 0; x < mXRes; x++ )
		{
			for ( int y = 0; y < mYRes; y++ )
			{
//				tex->normals[y * mXRes + x].normalize();
#define PACKINTOBYTE_MINUS1TO1(X)  ((uchar)((X)*127.5+127.5))
				rgb[( ( mYRes - y - 1 ) * mXRes + x ) * 3 + 2 ] = PACKINTOBYTE_MINUS1TO1( tex->normals[y * mXRes + x].x );
				rgb[( ( mYRes - y - 1 ) * mXRes + x ) * 3 + 1 ] = PACKINTOBYTE_MINUS1TO1( tex->normals[y * mXRes + x].y );
				rgb[( ( mYRes - y - 1 ) * mXRes + x ) * 3 + 0 ] = PACKINTOBYTE_MINUS1TO1( tex->normals[y * mXRes + x].z );
#undef PACKINTOBYTE_MINUS1TO1
			}
		}

#if 0
		// write as bmp 24bpp
		BITMAPFILEHEADER bfh;
		BITMAPINFO bi;
		bfh.bfType = 0x4D42;
		bfh.bfSize = sizeof( bfh ) + sizeof( bi.bmiHeader ) + mXRes * mYRes * 3;
		bfh.bfReserved1 = bfh.bfReserved2 = 0;
		bfh.bfOffBits = sizeof( bfh ) + sizeof( bi.bmiHeader );
		bi.bmiHeader.biSize = sizeof( bi.bmiHeader );
		bi.bmiHeader.biWidth = mXRes;
		bi.bmiHeader.biHeight = mYRes;
		bi.bmiHeader.biPlanes = 1;
		bi.bmiHeader.biBitCount = 24;
		bi.bmiHeader.biCompression = BI_RGB;
		char fn[_MAX_FNAME];
		_splitpath( ( *it ).first, NULL, NULL, fn, NULL );
		feCStr s;
		s.printf( "%s_nmap.bmp", fn );
		FILE *fp = fopen( s, "w+b" );
		fwrite( &bfh, sizeof( bfh ), 1, fp );
		fwrite( &bi, sizeof( bi.bmiHeader ), 1, fp );
		fwrite( rgb, sizeof( char ) * mXRes * mYRes * 3, 1, fp );
		fclose( fp );
#endif

		delete[] rgb;
	}
}

void fgpNormalMapper::expandTexels( void )
{
	_tprintf( _T( "expanding texels...\n" ) );
	std::map< feCStr, texture_t * >::iterator it;
	for ( it = mTextures.begin(); it != mTextures.end(); it++ )
	{
		texture_t *tex = (*it).second;
		std::vector< FVector3 > newNormals;
		newNormals.resize( tex->normals.size() );
		memset( &newNormals.front(), 0, sizeof( FVector3 ) * newNormals.size() );
		for (int f = 0; f < 10; f++)
		{
			// Loop over the old image and create a new one.
			for (int y = 0; y < mYRes; y++)
			{
				//			ShowSpinner ();
				for (int x = 0; x < mXRes; x++)
				{
					// See if this pixel is empty
					int idx = y*mXRes + x;
					if ((fabs(tex->normals[idx].x) <= 1.0e-7f) &&
						(fabs(tex->normals[idx].y) <= 1.0e-7f) && 
						(fabs(tex->normals[idx].z) <= 1.0e-7f))
					{
						// Accumulate the neary filled pixels
						FVector3 nn( 0.f, 0.f, 0.f );
						int ncount = 0;
						int yv;
						int xv;
						int nidx;

						// -Y
						yv = (y - 1)%mYRes;
						if (yv < 0) 
						{
							yv = mYRes -1;
						}
						xv = (x)%mXRes;
						if (xv < 0) 
						{
							xv = mXRes -1;
						}
						nidx = yv*mXRes + xv;
						if ((fabs(tex->normals[nidx].x) > 1.0e-7f) ||
							(fabs(tex->normals[nidx].y) > 1.0e-7f) ||
							(fabs(tex->normals[nidx].z) > 1.0e-7f))
						{
							nn += tex->normals[nidx];
							ncount++;
						}

						// +Y
						yv = (y + 1)%mYRes;
						if (yv < 0) 
						{
							yv = mYRes -1;
						}
						xv = (x)%mXRes;
						if (xv < 0) 
						{
							xv = mXRes -1;
						}
						nidx = yv*mXRes + xv;
						if ((fabs(tex->normals[nidx].x) > 1.0e-7f) ||
							(fabs(tex->normals[nidx].y) > 1.0e-7f) ||
							(fabs(tex->normals[nidx].z) > 1.0e-7f))
						{
							nn += tex->normals[nidx];
							ncount++;
						}

						// -X
						yv = (y)%mYRes;
						if (yv < 0) 
						{
							yv = mYRes -1;
						}
						xv = (x-1)%mXRes;
						if (xv < 0) 
						{
							xv = mXRes -1;
						}
						nidx = yv*mXRes + xv;
						if ((fabs(tex->normals[nidx].x) > 1.0e-7f) ||
							(fabs(tex->normals[nidx].y) > 1.0e-7f) ||
							(fabs(tex->normals[nidx].z) > 1.0e-7f))
						{
							nn += tex->normals[nidx];
							ncount++;
						}

						// -X
						yv = (y)%mYRes;
						if (yv < 0) 
						{
							yv = mYRes -1;
						}
						xv = (x+1)%mXRes;
						if (xv < 0) 
						{
							xv = mXRes -1;
						}
						nidx = yv*mXRes + xv;
						if ((fabs(tex->normals[nidx].x) > 1.0e-7f) ||
							(fabs(tex->normals[nidx].y) > 1.0e-7f) ||
							(fabs(tex->normals[nidx].z) > 1.0e-7f))
						{
							nn += tex->normals[nidx];
							ncount++;
						}

						// If we found some neighbors that were filled, fill
						// this one with the average, otherwise copy
						if (ncount > 0)
						{
							nn.normalize();
							newNormals[idx] = nn;
						}
						else
						{
							newNormals[idx] = tex->normals[idx];
						}
					} // end if this pixel is empty
					else
					{
						newNormals[idx] = tex->normals[idx];
					}
				} // end for x
			} // end for y

			// copy
			tex->normals = newNormals;
			//		float* i1 = img2;
			//		img2 = img;
			//		img = i1;
		} // end for f
	}
}

void fgpNormalMapper::boxFilter( void )
{
	_tprintf( _T( "box-filtering image...\n" ) );
	std::map< feCStr, texture_t * >::iterator it;
	for ( it = mTextures.begin(); it != mTextures.end(); it++ )
	{
		texture_t *tex = (*it).second;
		std::vector< FVector3 > newNormals;
		newNormals.resize( tex->normals.size() );
		memset( &newNormals.front(), 0, sizeof( FVector3 ) * newNormals.size() );
		for (int y = 0; y < (mYRes-1); y++)
		{
			for (int x = 0; x < (mXRes-1); x++)
			{
				FVector3 norm( 0.f, 0.f, 0.f );
				int oldIdx = y*mXRes + x;
				norm += tex->normals[oldIdx];
				oldIdx = (y+1)*mXRes + x;
				norm += tex->normals[oldIdx];
				oldIdx = (y+1)*mXRes + (x+1);
				norm += tex->normals[oldIdx];
				oldIdx = y*mXRes + (x+1);
				norm += tex->normals[oldIdx];
				int idx = y*mXRes + x;
				float len = norm.length();
				if (len < 1.0e-7f)
				{
					newNormals[idx] = feVector3( 0.f, 0.f, 0.f );
				}
				else
				{
					newNormals[idx] = norm/len;
				}
			}
		}

		// copy
		tex->normals = newNormals;
	}
}

void fgpNormalMapper::convertToTangentSpace( FVector3 ts[3], const FVector3 &vec, FVector3 &result )
{
	FVector3 n;
	n.x = vec.x * ts[0][0] + vec.y * ts[1][0] + vec.z * ts[2][0];
	n.y = vec.x * ts[0][1] + vec.y * ts[1][1] + vec.z * ts[2][1];
	n.z = vec.x * ts[0][2] + vec.y * ts[1][2] + vec.z * ts[2][2];
	result = n;
}

void fgpNormalMapper::computeTangents( feExpTriObject *hi, feExpTriObject *low )
{
	std::vector< NmRawTriangle > tris;
	NmRawTangentSpace *tan;
	size_t i;
	feMatrix4 mtx = low->getWorldMatrix().inverse();
	for ( i = 0; i < low->faces().size(); i++ )
	{
		NmRawTriangle t;
		feVector3 vv[] = { low->verts()[low->faces()[i].v[0]] * mtx
			, low->verts()[low->faces()[i].v[1]] * mtx
			, low->verts()[low->faces()[i].v[2]] * mtx };
		feVector3 nn[] = { mtx.transform( low->normals()[low->normalFaces()[i].v[0]] )
			, mtx.transform( low->normals()[low->normalFaces()[i].v[1]] )
			, mtx.transform( low->normals()[low->normalFaces()[i].v[2]] ) };

		t.vert[0].x = vv[0].x;
		t.vert[0].y = vv[0].y;
		t.vert[0].z = vv[0].z;
		t.vert[1].x = vv[1].x;
		t.vert[1].y = vv[1].y;
		t.vert[1].z = vv[1].z;
		t.vert[2].x = vv[2].x;
		t.vert[2].y = vv[2].y;
		t.vert[2].z = vv[2].z;

		t.norm[0].x = nn[0].x;
		t.norm[0].y = nn[0].y;
		t.norm[0].z = nn[0].z;
		t.norm[1].x = nn[1].x;
		t.norm[1].y = nn[1].y;
		t.norm[1].z = nn[1].z;
		t.norm[2].x = nn[2].x;
		t.norm[2].y = nn[2].y;
		t.norm[2].z = nn[2].z;

		t.texCoord[0].u = low->texCoords()[low->texCoordFaces()[i].v[0]].x;
		t.texCoord[0].v = low->texCoords()[low->texCoordFaces()[i].v[0]].y;
		t.texCoord[1].u = low->texCoords()[low->texCoordFaces()[i].v[1]].x;
		t.texCoord[1].v = low->texCoords()[low->texCoordFaces()[i].v[1]].y;
		t.texCoord[2].u = low->texCoords()[low->texCoordFaces()[i].v[2]].x;
		t.texCoord[2].v = low->texCoords()[low->texCoordFaces()[i].v[2]].y;

		tris.push_back( t );
	}

	bool res = NmComputeTangents( ( int )low->faces().size(), &tris.front(), &tan );

	if ( !res )
	{
		// print error
		return;
	}


	std::vector<feTriTangentSpace> tangentSpace;
	tangentSpace.resize( low->faces().size() );

//	feMatrix4 mtx = low->getWorldMatrix().inverse();

	for ( i = 0; i < low->faces().size(); i++ )
	{
		feTriTangentSpace &tt = tangentSpace[i];
		NmRawTangentSpace &t = tan[i];

		tt.tangent[0].x = t.tangent[0].x;
		tt.tangent[0].y = t.tangent[0].y;
		tt.tangent[0].z = t.tangent[0].z;
		tt.tangent[1].x = t.tangent[1].x;
		tt.tangent[1].y = t.tangent[1].y;
		tt.tangent[1].z = t.tangent[1].z;
		tt.tangent[2].x = t.tangent[2].x;
		tt.tangent[2].y = t.tangent[2].y;
		tt.tangent[2].z = t.tangent[2].z;

		tt.binormal[0].x = t.binormal[0].x;
		tt.binormal[0].y = t.binormal[0].y;
		tt.binormal[0].z = t.binormal[0].z;
		tt.binormal[1].x = t.binormal[1].x;
		tt.binormal[1].y = t.binormal[1].y;
		tt.binormal[1].z = t.binormal[1].z;
		tt.binormal[2].x = t.binormal[2].x;
		tt.binormal[2].y = t.binormal[2].y;
		tt.binormal[2].z = t.binormal[2].z;

	}

	delete[] tan;

	std::vector< feExpFace > tsFaces( low->faces().size() );
	std::vector< feVertexTangentSpace > tsVerts;

	// remove duplicates and store to verts
	for ( size_t face_i = 0; face_i < tsFaces.size(); face_i++ )
	{
		const feTriTangentSpace &tt = tangentSpace[face_i];
		for ( int vertex_i = 0; vertex_i < 3; vertex_i++ )
		{
			feVector3 tangent( tt.tangent[vertex_i].x, tt.tangent[vertex_i].y, tt.tangent[vertex_i].z );
			feVector3 binormal( tt.binormal[vertex_i].x, tt.binormal[vertex_i].y, tt.binormal[vertex_i].z );
			// check for dupe
			size_t vv;
			for ( vv = 0; vv < tsVerts.size(); vv++ )
			{
				if ( tsVerts[vv].tangent == tangent
					&& tsVerts[vv].binormal == binormal )
				{
					break;
				}
			}
			if ( vv == tsVerts.size() )
			{
				// add
				feVertexTangentSpace vts;
				vts.tangent = tangent;
				vts.binormal = binormal;
				tsVerts.push_back( vts );
			}
			tsFaces[face_i].v[vertex_i] = (int)vv;
		}
	}

	// convert to object space
	for ( size_t vv = 0; vv < tsVerts.size(); vv++ )
	{
		feVector3 t, b, n;
//		t = mtx.transform( tsVerts[vv].tangent );
//		b = mtx.transform( tsVerts[vv].binormal );
		t = tsVerts[vv].tangent;
		b = tsVerts[vv].binormal;
		tsVerts[vv].tangent = feVector3( t.x, t.y, t.z );
		tsVerts[vv].binormal = feVector3( b.x, b.y, b.z );
	}

	low->tsVerts() = tsVerts;
	low->tsFaces() = tsFaces;


#if 0
	_tprintf( _T( "calculating tangent space...\n" ) );
	std::vector<feTriTangentSpace> tangentSpace;
	tangentSpace.resize( low->faces().size() );

	feMatrix4 mtx = low->getWorldMatrix().inverse();

	size_t face_i;

	// for each vertex, write list of faces using it
	std::vector< std::list< size_t > >	vtxFaces( low->verts().size() );
	std::vector< bool > vtxProcessed( low->verts().size(), false );
	for ( face_i = 0; face_i < low->faces().size(); face_i++ )
	{
		vtxFaces[low->faces()[face_i].v[0]].push_back( face_i );
		vtxFaces[low->faces()[face_i].v[1]].push_back( face_i );
		vtxFaces[low->faces()[face_i].v[2]].push_back( face_i );
	}

	// calc tangent and binormal for all vertices of each face
	for ( face_i = 0; face_i < low->faces().size(); face_i++ )
	{
		for ( int k = 0; k < 3; k++ )
		{
			// calc worldspace->tangetspace matrix
			calcTan( low, face_i, k, tangentSpace[face_i] );
/*			tangentSpace[face_i].normal[k] = mtx.transform( tangentSpace[face_i].normal[k] );
			tangentSpace[face_i].binormal[k] = mtx.transform( tangentSpace[face_i].binormal[k] );
			tangentSpace[face_i].tangent[k] = mtx.transform( tangentSpace[face_i].tangent[k] );*/
		}
	}

	// smooth tangents between vertices
	std::vector< feVertexTangentSpace > tsVerts;
	std::vector< feExpFace > tsFaces( low->faces().size() );

	// init
	for ( face_i = 0; face_i < tsFaces.size(); face_i++ )
	{
		tsFaces[face_i].v[0] = tsFaces[face_i].v[1] = tsFaces[face_i].v[2] = -1;
	}

	// go
	for ( size_t vtx_i = 0; vtx_i < low->verts().size(); vtx_i++ )
	{
		std::list< size_t > &faces = vtxFaces[vtx_i];
		while ( !faces.empty() )
		{
			size_t f = faces.front();
			faces.pop_front();

			for ( int k = 0; k < 3; k++ )
			{
				if ( vtx_i == low->faces()[f].v[k] )
				{
					if ( tsFaces[f].v[k] != -1 )
						break;
					// get tangent
					FVector3 tangent = tangentSpace[f].tangent[k];
					const FVector3 normal = tangentSpace[f].normal[k];
					std::vector< size_t > found;
					found.push_back( f );
					// we can do that only when tangent vector points in nearly the same direction
					for ( std::list< size_t >::iterator it = faces.begin(); it != faces.end(); it++ )
					{
						for ( int kk = 0; kk < 3; kk++ )
						{
							if ( low->faces()[*it].v[kk] == vtx_i )
							{
								// compare tangent vector direction
								const FVector3 &tt = tangentSpace[*it].tangent[kk];
								const FVector3 &nn = tangentSpace[*it].normal[kk];

								if ( fabs( 1 - tt.dot( tangentSpace[f].tangent[k] ) ) < 0.001f
									&& fabs( 1 - normal.dot( nn ) ) < 0.001f )	// this epsilon value should be adjusted
								{
									// points in same direction
									tangent += tt;
									found.push_back( *it );
								}
								break;
							}
						}
					}
					// calc new tangent and update
					tangent /= ( float )found.size();
					tangent.normalize();

					feVertexTangentSpace vts;
					vts.tangent = feVector3( tangent.x, tangent.y, tangent.z );
					vts.normal = feVector3( normal.x, normal.y, normal.z );
					vts.binormal = vts.normal.cross( vts.tangent );
					for ( size_t ff = 0; ff < found.size(); ff++ )
					{
						for ( int kk = 0; kk < 3; kk++ )
						{
							if ( low->faces()[found[ff]].v[kk] == vtx_i )
							{
								tsFaces[found[ff]].v[kk] = (int)tsVerts.size();
							}
						}
					}
					tsVerts.push_back( vts );
					break;
				}
			}
		}
	}

	// check
	for ( face_i = 0; face_i < tsFaces.size(); face_i++ )
	{
		if ( tsFaces[face_i].v[0] == -1
			|| tsFaces[face_i].v[1] == -1
			|| tsFaces[face_i].v[2] == -1
			)
		{
			_tprintf( _T( "ERROR: not all faces got tangentspace!\n" ) );
			exit( 0 );
		}
	}

	// convert basis vectors to object space
	for ( size_t i = 0; i < tsVerts.size(); i++ )
	{
		tsVerts[i].binormal = mtx.transform( tsVerts[i].binormal );
		tsVerts[i].tangent = mtx.transform( tsVerts[i].tangent );
		tsVerts[i].normal = mtx.transform( tsVerts[i].normal );
	}

	low->tsVerts() = tsVerts;
	low->tsFaces() = tsFaces;
#endif
}

void fgpNormalMapper::calcTan( feExpTriObject *low, size_t face_i, int vertex_i, feTriTangentSpace &ts )
{
	ts.tangent[vertex_i] = FVector3( 1, 0, 0 );
	ts.binormal[vertex_i] = FVector3( 0, 0, 1 );

	FVector3 norm;
	norm = low->normals()[low->normalFaces()[face_i].v[vertex_i]];

	if ( low->texCoords()[ low->texCoordFaces()[face_i].v[0] ].x == 0.f
		&& low->texCoords()[ low->texCoordFaces()[face_i].v[1] ].x == 0.f
		&& low->texCoords()[ low->texCoordFaces()[face_i].v[2] ].x == 0.f )
	{
		return;
	}

	// compute edge vectors
	FVector3 vec1, vec2;
	vec1 = low->verts()[low->faces()[face_i].v[1]] - low->verts()[low->faces()[face_i].v[0]]; // 1-0
	vec2 = low->verts()[low->faces()[face_i].v[2]] - low->verts()[low->faces()[face_i].v[0]]; // 2-0

	// calculate u vector
	feVector2 uv1, uv2;
	uv1 = low->texCoords()[low->texCoordFaces()[face_i].v[1]] - low->texCoords()[low->texCoordFaces()[face_i].v[0]]; // 1-0
	uv2 = low->texCoords()[low->texCoordFaces()[face_i].v[2]] - low->texCoords()[low->texCoordFaces()[face_i].v[0]]; // 2-0
	float a = uv1.x - uv1.y * uv2.x / uv2.y;
	if ( a != 0.f )
		a = 1.f / a;
	float b = uv2.x - uv2.y * uv1.x / uv1.y;
	if ( b != 0.f )
		b = 1.f / b;
	FVector3 duTmp = vec1 * a + vec2 * b;
	duTmp.normalize();

	// calculate v vector
	a = uv1.y - uv1.x * uv2.y / uv2.x;
	if ( a != 0.f )
		a = 1.f / a;
	b = uv2.y - uv2.x * uv1.y / uv1.x;
	if ( b != 0.f )
		b = 1.f / b;
	FVector3 dvTmp;
	dvTmp = vec1 * a + vec2 * b;
	dvTmp.normalize();

	// project the vector into the plane defined by the normal
	float tmpf;
	
	tmpf = duTmp.dot( norm );
	ts.tangent[vertex_i] = duTmp - norm * tmpf;
	ts.tangent[vertex_i].normalize();
	
	tmpf = dvTmp.dot( norm );
	ts.binormal[vertex_i] = dvTmp - norm * tmpf;
	ts.binormal[vertex_i].normalize();

	ts.normal[vertex_i] = norm;
}

void fgpNormalMapper::saveTangentSpace( feExpObject *_low, feStr n )
{
	n += _T( "_" );
	n += _low->name();

	if ( _low->getClassID() == TRI_OBJECT_CLASS_ID )
	{
		feExpTriObject *low = checked_cast< feExpTriObject * >( _low );
		if ( !low->tsVerts().empty() )
		{
			ulong sz;
			feStr s;
			s.printf( _T( "%s.tgs" ), n.c_str() );
			FILE *fp = _tfopen( s.c_str(), _T( "w+b" ) );
			char magic[] = "TGS0101";
			fwrite( magic, strlen( magic ), 1, fp );
			sz = ( ulong )low->tsVerts().size();
			fwrite( &sz, sizeof( sz ), 1, fp );
			fwrite( &low->tsVerts().front(), low->tsVerts().size(), sizeof( feVertexTangentSpace ), fp );
			sz = ( ulong )low->tsFaces().size();
			fwrite( &sz, sizeof( sz ), 1, fp );
			fwrite( &low->tsFaces().front(), low->tsFaces().size(), sizeof( feExpFace ), fp );
			fclose( fp );
		}
	}

	for ( size_t i = 0; i < _low->children().size(); i++ )
	{
		saveTangentSpace( _low->children()[i], n );
	}
}
