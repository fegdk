#pragma once

#define _WIN32_WINNT 0x0400

#ifdef _DEBUG
#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include "mstchar.h"

//-----------------------------------
// misc msvc warning disables
//-----------------------------------

#pragma warning (disable:4244)
//#pragma warning (disable:4305)
//#pragma warning (disable:4142)
//#pragma warning (disable:4018)
#pragma warning (disable:4786)

//-----------------------------------
// stl includes
//-----------------------------------

#include <vector>
#include <list>
#include <map>
#include <algorithm>

//-----------------------------------
// win32 includes
//-----------------------------------

#include <windows.h>
#include <assert.h>
#include <mmsystem.h>
#include <objbase.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>

#define DIRECT3D_VERSION         0x0800
#include <d3d8.h>
#include <d3dx8.h>