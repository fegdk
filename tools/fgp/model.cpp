//------------------------------------------------------
// model processor
// converts geometry & animation into suitable form
// reduces number of bones-per-vertex, etc
//------------------------------------------------------
#include <fegdk/pch.h>
#include "fgp.h"

void ConvertModel(const char *fname )
{
	_tprintf( _T( "loading model...\n" ), fname );

	fe::smartPtr <fe::expScene> model = new fe::expScene ();
	model->load( fname );

	_tprintf( _T( "model ready, converting...\n" ) );

	fe::smartPtr <fe::expModel> out = new fe::expModel(NULL);
	out->convertFrom( model, model->getRootNode() );

	char fn [200];
	const char *dot = fname + strlen (fname);
	while (dot != fname && *dot != '.')
		dot--;
	if (dot == fname)
		strcpy (fn, fname);
	else
	{
		strncpy (fn, fname, dot-fname);
		fn[dot-fname] = 0;
	}

	strcat (fn, ".bmdl");

	printf( "saving %s...\n", fn );
	out->save( fn, model );

	printf( "done.\n" );
}

