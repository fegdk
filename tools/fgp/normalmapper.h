#ifndef __F_NORMALMAPPER_H
#define __F_NORMALMAPPER_H

#include <fegdk/f_string.h>
#include <fegdk/f_math.h>

class fgpNormalMapper
{

protected:

	// types

	// double version of vector3
	typedef feVector3 FVector3;
#if 0
	class FVector3
	{
	public:
		double x, y, z;

		FVector3( void )
		{
		}

		FVector3( double xx, double yy, double zz )
		{
			x = xx;
			y = yy;
			z = zz;
		}

		FVector3( const feVector3 &v )
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		FVector3( const FVector3 &v )
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		double &operator [] ( int i )
		{
			return (&x)[i];
		}

		FVector3 operator - (const FVector3& v) const
		{
			return FVector3( x - v.x, y - v.y, z - v.z );
		}

		FVector3 operator + (const FVector3& v) const
		{
			return FVector3( x + v.x, y + v.y, z + v.z );
		}

		FVector3 operator * (const FVector3& v) const
		{
			return FVector3( x * v.x, y * v.y, z * v.z );
		}

		FVector3 operator * ( double d ) const
		{
			return FVector3( x * d, y * d, z * d );
		}

		FVector3 operator / ( double d ) const
		{
			return FVector3( x / d, y / d, z / d );
		}

		FVector3& operator += ( const FVector3& srcVector )
		{
			x += srcVector.x;
			y += srcVector.y;
			z += srcVector.z;
			return *this;
		}

		FVector3& operator /= ( double d )
		{
			x /= d;
			y /= d;
			z /= d;
			return *this;
		}

		FVector3 Cross( const FVector3& srcVector ) const
		{
			return FVector3( y*srcVector.z - z*srcVector.y,
				z*srcVector.x - x*srcVector.z,
				x*srcVector.y - y*srcVector.x );
		}

		double Dot( const FVector3& v ) const
		{
			return x * v.x + y * v.y + z * v.z;
		}

		double Length( void ) const
		{
			return sqrt( Dot( *this ) );
		}

		void Normalize( void )
		{
			double d = sqrt( Dot( *this ) );
			if ( d < 1.0e-7 )
			{
				x = 0;
				y = 1;
				z = 0;
			}
			else
			{
				x /= d;
				y /= d;
				z /= d;
			}
		}

	};
#endif

	// structure for storing face visibility info
	struct pvs_t
	{
		std::vector< int >		tris;
	};

	struct texture_t
	{
		std::vector< FVector3 >	normals;
	};

	struct edge_t {
		int idx0;
		int idx1;
		int yMin;
		int yMax;
		int x;
		int x1;
		int increment;
		int numerator;
		int denominator;
	};

	// represents tangent-space matrix of a single triangle
	struct feTriTangentSpace
	{
		FVector3 normal[3];
		FVector3 tangent[3];
		FVector3 binormal[3];
	};

	// data

	feStr						mModelName;
	feExpScene*					mpMdlHi;
	feExpScene*					mpMdlLow;
	int							mXRes;
	int							mYRes;
	std::vector< pvs_t >		mPVS;
	std::map< feCStr, texture_t * >	mTextures;

	void computePlaneEq( fePlane &plane, const feVector3 &v0, const feVector3 &v1, const feVector3 &norm );
	void run( feExpObject *hi, feExpObject *low );
	bool check( feExpObject *hi, feExpObject *low );
	bool faceVisible( const feVector3 pos[3], const fePlane &plane );
	void updateMaps( feExpTriObject *hi, feExpTriObject *low );
	void computePVS( feExpTriObject *hi, feExpTriObject *low );
//	float intRoundTexCoordU( float u );
//	float intRoundTexCoordV( float v );
	void getEdge( edge_t &edge, feVector2 tri[3], int idx0, int idx1 );
	void saveMaps( void );
	void saveTangentSpace( feExpObject *low, feStr n );
	bool intersectTri( const FVector3 &orig, const FVector3 &dir, const FVector3 tri[3], float &t, float &u, float &v );
	void expandTexels( void );
	void boxFilter( void );
	void convertToTangentSpace( FVector3 ts[3], const FVector3 &vec, FVector3 &result );
	void computeTangents( feExpTriObject *hi, feExpTriObject *low );
	void calcTan( feExpTriObject *low, size_t face_i, int vertex_i, feTriTangentSpace &ts );

public:

	fgpNormalMapper( const feStr &name, feExpScene *hi, feExpScene *low, int xres, int yres );
	~fgpNormalMapper( void );

	void run( void );
};

#endif // __F_NORMALMAPPER_H

