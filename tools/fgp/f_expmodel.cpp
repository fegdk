/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_types.h"
#include "f_parser.h"
#include "f_expmodel.h"
#include "f_material.h"
#include "f_error.h"
#include "f_engine.h"
#include "f_texture.h"
#include "f_effect.h"
#include "f_resourcemgr.h"

namespace fe
{
	
	//----------------------
	// expScene
	//----------------------
	
	expScene::expScene (void)
	{
	}
	
	expScene::~expScene ()
	{
	}
	
	int expScene::load (const char *fname)
	{
		const char *tmp = fname;
		const char *fn = tmp;
		while (*tmp)
		{
			if (*tmp == '/')
				fn = tmp;
			tmp++;
		}
		if (*fn == '/')
			fn++;
		
		mName = fn;
	
		fprintf (stderr, "fname is %s, modelname is %s\n", fname, fn);
#ifdef FE_NO_FILESYSTEM
		charParser p (fname, true);
#else
		// this thing always uses stdio
		charParser p (NULL, fname, true);
#endif
		load (p);
	
		return 0;
	}
	
	void expScene::load (charParser &p)
	{
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("materials"))
			{
				p.matchToken ("{");
				for (;;)
				{
					p.getToken ();
					if (p.token () == "}")
						break;
					mMtlNameList.push_back (p.token ());
					if (g_engine)
					{
						materialPtr mtl =  g_engine->getResourceMgr ()->createMaterial (p.token ());					mMtlList.push_back (mtl);
					}
				}
			}
			else if (!p.cmptoken ("bones"))
			{
				// read bones
				p.matchToken ("{");
				for (;;)
				{
					p.getToken ();
					if (!p.cmptoken ("}"))
						break;
					mBoneList.push_back (p.token ());
				}
			}
			else
			{
				mpSceneRoot = loadObject (p);
				if (!mpSceneRoot)
					p.generalSyntaxError ();
				return;
			}
		}
	}
	
	expObjectPtr expScene::loadObject (charParser &p)
	{
		expObject *o = NULL;
		if (!p.cmptoken ("triobject")) 
		{
			o = new expTriObject (NULL);
			o->load (p, name ());
		}
		else if (!p.cmptoken ("dummy"))
		{
			o = new expDummyObject (NULL);
			o->load (p, name ());
		}
		else if (!p.cmptoken ("entity"))
		{
			o = new expEntityObject (NULL);
			o->load (p, name ());
		}
		return o;
	}
	
	void expScene::render ()
	{
		if (getRootNode ())
			getRootNode ()->render (this);
	}
	
	//----------------------
	// expTriObject
	//----------------------
	
	expTriObject::expTriObject (expObject *parent)
		: expObject (parent)
	{
		mClassID = TRI_OBJECT_CLASS_ID;
	}
	
	void expTriObject::load (charParser &p, const char *filename)
	{
		p.getToken ();
		mName = p.token ();
		p.matchToken ("{");
	
		cStr fname = filename;
	
		// try to load tangentspace
		fname += "_";
		fname += mName;
	
	/*	
	 	// old code which relied on tangent-space matrix built by normalmap generator
		FILE *fp = fopen (fname + ".tgs", "rb");
		if (fp)
		{
			// read magic
			char magic[8];
			fread (magic, 7, 1, fp);
			magic[7] = 0;
			// 1.00 contained SxT too, was removed in 1.01
			if (strcmp (magic, "TGS0101"))
				throw genericError ("invalid signature inf file '%s.tgs'", fname.c_str ());
			ulong sz;
			fread (&sz, sizeof (sz), 1, fp);
			mTSVerts.resize (sz);
			fread (&mTSVerts.front (), sz, sizeof (vertexTangentSpace), fp);
			fread (&sz, sizeof (sz), 1, fp);
			mTSFaces.resize (sz);
			fread (&mTSFaces.front (), sz, sizeof (expFace), fp);
			fclose (fp);
		}*/
	
		for (;;)
		{
			p.getToken ();
			if (p.token () == "}")
				break;
			else if (baseLoad (p, fname))
			{
			}
			else if (!p.cmptoken ("verts"))
			{
				// verts
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mVerts.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("v");
					p.getToken ();
					mVerts[i].x = atof (p.token ());
					p.getToken ();
					mVerts[i].y = atof (p.token ());
					p.getToken ();
					mVerts[i].z = atof (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("faces"))
			{
				// faces
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mFaces.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("f");
					p.getToken ();
					mFaces[i].v[0] = atoi (p.token ());
					p.getToken ();
					mFaces[i].v[1] = atoi (p.token ());
					p.getToken ();
					mFaces[i].v[2] = atoi (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("colors"))
			{
				// verts
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mColors.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("v");
					p.getToken ();
					mColors[i].x = atof (p.token ());
					p.getToken ();
					mColors[i].y = atof (p.token ());
					p.getToken ();
					mColors[i].z = atof (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("ColorFaces"))
			{
				// ColorFaces
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mColorFaces.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("f");
					p.getToken ();
					mColorFaces[i].v[0] = atoi (p.token ());
					p.getToken ();
					mColorFaces[i].v[1] = atoi (p.token ());
					p.getToken ();
					mColorFaces[i].v[2] = atoi (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("normals"))
			{
				// normals
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mNormals.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("v");
					p.getToken ();
					mNormals[i].x = atof (p.token ());
					p.getToken ();
					mNormals[i].y = atof (p.token ());
					p.getToken ();
					mNormals[i].z = atof (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("normalFaces"))
			{
				// normalFaces
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mNormalFaces.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("f");
					p.getToken ();
					mNormalFaces[i].v[0] = atoi (p.token ());
					p.getToken ();
					mNormalFaces[i].v[1] = atoi (p.token ());
					p.getToken ();
					mNormalFaces[i].v[2] = atoi (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("uvVerts"))
			{
				// uvVerts
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mTexCoords.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("v");
					p.getToken ();
					mTexCoords[i].x = atof (p.token ());
					p.getToken ();
					mTexCoords[i].y = atof (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("uvFaces"))
			{
				// uvFaces
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mTexCoordFaces.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("f");
					p.getToken ();
					mTexCoordFaces[i].v[0] = atoi (p.token ());
					p.getToken ();
					mTexCoordFaces[i].v[1] = atoi (p.token ());
					p.getToken ();
					mTexCoordFaces[i].v[2] = atoi (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("uvLightmap"))
			{
				// uvVerts
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mLightmapTexCoords.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("v");
					p.getToken ();
					mLightmapTexCoords[i].x = atof (p.token ());
					p.getToken ();
					mLightmapTexCoords[i].y = atof (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("uvLightmapFaces"))
			{
				// uvFaces
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mLightmapTexCoordFaces.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("f");
					p.getToken ();
					mLightmapTexCoordFaces[i].v[0] = atoi (p.token ());
					p.getToken ();
					mLightmapTexCoordFaces[i].v[1] = atoi (p.token ());
					p.getToken ();
					mLightmapTexCoordFaces[i].v[2] = atoi (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("mtlIndex"))
			{
				// uvFaces
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mMtlIndex.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("f");
					p.getToken ();
					mMtlIndex[i] = atoi (p.token ());
				}
				p.matchToken ("}");
			}
			else if (!p.cmptoken ("vertexInfluences"))
			{
				p.matchToken ("{");
				p.matchToken ("count");
				p.getToken ();
				int cnt = atoi (p.token ());
				mVertexInfluences.resize (cnt);
				for (int i = 0; i < cnt; i++)
				{
					p.matchToken ("w");
					p.getToken ();
					int vtx = atoi (p.token ());
					p.getToken ();
					int wcnt = atoi (p.token ());
					mVertexInfluences[i].resize (wcnt);
					for (int k = 0; k < wcnt; k++)
					{
						p.getToken ();
						mVertexInfluences[vtx][wcnt].bone = atoi (p.token ());
						p.getToken ();
						mVertexInfluences[vtx][wcnt].weight = atof (p.token ());
					}
				}
			}
		}
		
		printf ("'%s' triobj stats:\n", name ());
		printf ("  %d verts\n", mVerts.size ());
		printf ("  %d colors\n", mColors.size ());
		printf ("  %d normals\n", mNormals.size ());
		printf ("  %d texcoords\n", mTexCoords.size ());
		printf ("  %d litmapcoords\n", mLightmapTexCoords.size ());
		printf ("  %d faces\n", mFaces.size ());
		printf ("  %d colorfaces\n", mColorFaces.size ());
		printf ("  %d normalfaces\n", mNormalFaces.size ());
		printf ("  %d tcfaces\n", mTexCoordFaces.size ());
		printf ("  %d lightmaptcfaces\n", mLightmapTexCoordFaces.size ());
		printf ("  %d mtlindexes\n", mMtlIndex.size ());
		printf ("  %d tangents\n", mTangents.size ());
		printf ("  %d binormals\n", mBinormals.size ());
		printf ("  %d tsfaces\n", mTSFaces.size ());
	
	
	
	#if 0
		if (!mTangentSpace.empty ())
		{
			assert (mTangentSpace.size () == mFaces.size ());
		}
	#endif
	}
	
	void	expTriObject::transformIntoWorldSpace (void)
	{
		size_t i;
		for (i = 0; i < mVerts.size (); i++)
			mVerts[i] = mVerts[i] * mWorldMatrix;
		for (i = 0; i < mNormals.size (); i++)
		{
			mNormals[i] = mWorldMatrix.transform (mNormals[i]);
			mNormals[i].normalize ();
		}
	}
	
	void expTriObject::render (expScene *scene)
	{
	#if 0
		if (mtlIndex ().empty ())
			return;
		// set transforms
		gpD3DDev->SetTransform (D3DTS_WORLD, (D3DMATRIX*)&getWorldMatrix ());
	
		// render mesh
		int prevMtl = -1;
		int firstFace = 0;
		int nFaces = 0;
	
		for (size_t face_i = 0; face_i < faces ().size (); face_i++)
		{
			if ( (!mtlIndex ().empty () && mtlIndex ()[face_i] != prevMtl) || nFaces * 3 > engine::DYNAMIC_VB_SIZE - 3)
			{
				if (nFaces)
				{
					if (prevMtl != -1)
					{
						flush (scene, prevMtl, firstFace, nFaces);
					}
					nFaces = 0;
					firstFace = face_i;
				}
			}
	
			nFaces++;
			prevMtl = mtlIndex ()[face_i];
		}
		if (nFaces)
			flush (scene, prevMtl, firstFace, nFaces);
	#endif
		expObject::render (scene);
	}
	
	void	expTriObject::flush (expScene *scene, int mtl, int firstFace, int nFaces)
	{
	#if 0
		// flush & restart vb
		dynamicVB< fFVertex > *vb = engine::get ()->dynamicVB ();
		uint start;
		fFVertex *data = vb->lock (nFaces * 3, start);
		for (int f = 0; f < nFaces; f++)
		{
			if (texCoords ().empty ())
			{
				*data = fFVertex (verts ()[faces ()[firstFace+f].v[0]], 0, vector2 (0, 0));
				data++;
				*data = fFVertex (verts ()[faces ()[firstFace+f].v[1]], 0, vector2 (0, 0));
				data++;
				*data = fFVertex (verts ()[faces ()[firstFace+f].v[2]], 0, vector2 (0, 0));
				data++;
			}
			else
			{
				*data = fFVertex (verts ()[faces ()[firstFace+f].v[0]], 0, texCoords ()[texCoordFaces ()[firstFace+f].v[0]]);
				data++;
				*data = fFVertex (verts ()[faces ()[firstFace+f].v[1]], 0, texCoords ()[texCoordFaces ()[firstFace+f].v[1]]);
				data++;
				*data = fFVertex (verts ()[faces ()[firstFace+f].v[2]], 0, texCoords ()[texCoordFaces ()[firstFace+f].v[2]]);
				data++;
			}
		}
		vb->unlock ();
	
		// setup mtl
		scene->mtlList ()[mtl]->setupEffectInputs ();
		effect *e = scene->mtlList ()[mtl]->getEffect ();
		e->setBestTechnique ();
		e->begin ();
		e->pass (0);
		gpD3DDev->DrawPrimitive (D3DPT_TRIANGLELIST, start, nFaces);
		e->end ();
	#endif
	}
	
	//----------------------
	// expModel
	//----------------------
	
	expModel::expModel (expModel *parent)
		: sceneObject (parent)
	{
		mbLightmaps = false;
		mbTangents = false;
	}
	
	expModel::~expModel ()
	{
	}
	
	void	expModel::convertFrom (const expScenePtr &s, const expObjectPtr &root)
	{
		expObjectPtr oo = root;
		if (s->getRootNode () == oo && oo->children ().size () == 1)
		{
			// discard rootnode, convert from child #1 instead
			oo = oo->children ().front ();
			fprintf (stderr, "root node is redundant, starting from 1st child (%s)...\n", oo->name ());
		}
		else if (s->getRootNode () == oo)
			fprintf (stderr, "root node will be included in resulting scene, contains %d children...\n", oo->children ().size ());
		setName (oo->name ());
	
		// fill vb
		if (oo->getClassID () == TRI_OBJECT_CLASS_ID)
		{
			fprintf (stderr, "converting mesh for triobject %s...\n", oo->name ());
			const expTriObject *o = checked_cast <const expTriObject*> ( (const expObject*)oo);
			// for now we always build tangentspace (by default)
			mbTangents = true; // o->tsVerts ().empty () ? false : true;
			mbLightmaps = o->lightmapTexCoords ().empty () ? false : true;
			if (mbLightmaps)
				mLightmapName = oo->name () + cStr ("_lm.tga");	// max uses tga by default
	
			int mtl_i;
			int face_i;
			int vertex_i;
		
			// FIXME: optimize tangentspace calculation - this is very slow unoptimized code
			
			fprintf (stderr, "calculating tangent space basis for all faces...\n");
			
			// calc tangent space for all faces
			vector3 *faceTangents = new vector3[o->faces ().size () * 2];
			for (int i = 0; i < o->faces ().size (); i++)
			{
				const vector3 &V0 = o->verts ()[o->faces ()[i].v[0]];
				const vector3 &V1 = o->verts ()[o->faces ()[i].v[1]];
				const vector3 &V2 = o->verts ()[o->faces ()[i].v[2]];
				const vector2 &v0st = o->texCoords ()[o->texCoordFaces ()[i].v[0]];
				const vector2 &v1st = o->texCoords ()[o->texCoordFaces ()[i].v[1]];
				const vector2 &v2st = o->texCoords ()[o->texCoordFaces ()[i].v[2]];
				calcTriangleBasis (V0, V1, V2,
						v0st[0], v0st[1], v1st[0], v1st[1], v2st[0], v2st[1],
						faceTangents[i*2+0], faceTangents[i*2+1]);
			}
	
			// find average
			vector3 *tangents = new vector3[o->faces ().size () * 3];
			vector3 *binormals = new vector3[o->faces ().size () * 3];
			int *cnt = new int[o->faces ().size () * 3];
			memset (tangents, 0, sizeof (vector3) * o->faces ().size () * 3);
			memset (binormals, 0, sizeof (vector3) * o->faces ().size () * 3);
			memset (cnt, 0, sizeof (int) * o->faces ().size () * 3);
	
			int i, j;
	
			for (j = 0; j < o->faces ().size () * 3; j++)
			{
				// add tangent space to vertex
				tangents[j] += faceTangents[j/3*2+0];
				binormals[j] += faceTangents[j/3*2+1];
				cnt[j]++;
	
				const vector3 &pos = o->verts ()[o->faces ()[j/3].v[j%3]];
				const vector3 &norm = o->normals ()[o->normalFaces ()[j/3].v[j%3]];
	
				// find all verts which share same position and normal (i.e. share smoothing groups)
				for (int k = 0; k < o->faces ().size ()*3; k++)
				{
					const vector3 &pos2 = o->verts ()[o->faces ()[k/3].v[k%3]];
					const vector3 &norm2 = o->normals ()[o->normalFaces ()[k/3].v[k%3]];
					if (pos == pos2 && norm == norm2)
					{
						// check if angle between tangents ate ok
						float d_t = faceTangents[j/3*2+0].dot (faceTangents[k/3*2+0]);
						float d_b = faceTangents[j/3*2+1].dot (faceTangents[k/3*2+1]);
						if (d_t > 0 && d_b > 0)
						{
							// smooth
							tangents[j] += faceTangents[k/3*2+0];
							binormals[j] += faceTangents[k/3*2+1];
							cnt[j]++;
						}
					}
				}
			}
			for (j = 0; j < o->faces ().size (); j++)
			{
				for (i = 0; i < 3; i++)
				{
					tangents[j*3+i] /= cnt[j*3+i];
					binormals[j*3+i] /= cnt[j*3+i];
					tangents[j*3+i].normalize ();
					binormals[j*3+i].normalize ();
					tangents[j*3+i] = orthogonalize (o->normals ()[o->normalFaces ()[j].v[i]], tangents[j*3+i]);
					binormals[j*3+i] = orthogonalize (o->normals ()[o->normalFaces ()[j].v[i]], binormals[j*3+i]);
				}
			}
	
			delete[] faceTangents;
			delete[] cnt;
	
			fprintf (stderr, "creating vertex and index buffer...\n");
	
			for (mtl_i = 0; mtl_i < (int)s->mtlNameList ().size (); mtl_i++)
			{
				std::vector< drawVertex_t > verts;
				std::vector< face > faces;
				fprintf (stderr, "subset %d (mtl=%s)...\n", mtl_i, s->mtlNameList ()[mtl_i].c_str ());
				for (face_i = 0; face_i < (int)o->faces ().size (); face_i++)
				{
					if (o->mtlIndex ().empty () || o->mtlIndex ()[face_i] == mtl_i)
					{
						// add verts
	//					fprintf (stderr, "fetching face %d...\n", face_i);
						const expFace &f = o->faces ()[face_i];
	//					fprintf (stderr, "fetching normface %d...\n", face_i);
						const expFace &nf = o->normalFaces ()[face_i];
	//					fprintf (stderr, "fetching tcface %d...\n", face_i);
						const expFace &tf = o->texCoordFaces ()[face_i];
	//					fprintf (stderr, "fetching lmtcface %d...\n", face_i);
						const expFace *ltf = o->lightmapTexCoordFaces ().empty () ? NULL : &o->lightmapTexCoordFaces ()[face_i];
	
						face outface;
						int remap_face[] = {0,2,1};
	
						for (vertex_i = 0; vertex_i < 3; vertex_i++)
						{
							drawVertex_t v;
	//						fprintf (stderr, "fetching vx %d...\n", f.v[vertex_i]);
							v.pos = o->verts ()[f.v[vertex_i]];
	//						fprintf (stderr, "fetching norm %d...\n", nf.v[vertex_i]);
							v.norm = o->normals ()[nf.v[vertex_i]];
	//						fprintf (stderr, "fetching uv %d...\n", tf.v[vertex_i]);
							v.uv = o->texCoords ().empty () ? vector2 (0, 0) : o->texCoords ()[tf.v[vertex_i]];
	//						fprintf (stderr, "fetching ligthmap uv %d...\n", ltf ? ltf->v[vertex_i] : -1);
							if (ltf)
								v.uvLightmap = o->lightmapTexCoords ()[ltf->v[vertex_i]];
	//						fprintf (stderr, "fetching tangent %d...\n", face_i*3+vertex_i);
	//						fprintf (stderr, "fetching binormal %d...\n", face_i*3+vertex_i);
							v.tangents[0] = tangents[face_i*3+vertex_i];
							v.tangents[1] = binormals[face_i*3+vertex_i];
	
							std::vector< drawVertex_t >::iterator it;
							it = std::find (verts.begin (), verts.end (), v);
							if (it != verts.end ())
								outface.v[remap_face[vertex_i]] = it - verts.begin ();
							else
							{
								outface.v[remap_face[vertex_i]] = (int)verts.size ();
								verts.push_back (v);
							}
						}
						faces.push_back (outface);
					}
				}
				if (!verts.empty ())
				{
					fprintf (stderr, "adding %d verts, %d faces...\n", verts.size (), faces.size ());
					// add subset
					subset s;
					s.firstVertex = (int)mVerts.size ();
					s.firstFace = (int)mFaces.size ();
					s.numVerts = (int)verts.size ();
					s.numFaces = (int)faces.size ();
					s.mtl = mtl_i;
					mSubSets.push_back (s);
					// add verts
					for (vertex_i = 0; vertex_i < (int)verts.size (); vertex_i++)
						mVerts.push_back (verts[vertex_i]);
					// add faces
					for (face_i = 0; face_i < (int)faces.size (); face_i++)
					{
						mFaces.push_back (faces[face_i]);
						mMtlIndex.push_back (s.mtl);
					}
				}
	
				if (mtl_i == -1)
					break;
			}
			delete[] tangents;
			delete[] binormals;
		}
	
		// matrix
		mRelativeMatrix = oo->getMatrix ();
		mWorldMatrix = mpParentObject ? mRelativeMatrix * mpParentObject->getWorldMatrix () : mRelativeMatrix;
	
		// ... win32 version should create d3dxmesh and optimize here ...
	
		for (int i = 0; i < (int)oo->children ().size (); i++)
		{
			expModel *m = new expModel (this);
			m->convertFrom (s, oo->children ()[i]);
			addChild (m);
		}
	}
	
	void	expModel::save (const char *fname, const expScenePtr &s) const
	{
		FILE *fp = fopen (fname, "w+b");
		if (!fp)
		{
			fprintf (stderr, "failed to open file '%s' for writing\n", fname);
			return;
		}
	
		// write magic
		// 0100 -- original
		// 0101 -- added lightmaps support
		// 0102 -- animation is now separate file
	
		char mgc[] = "FEMDL0102";
		fwrite (mgc, strlen (mgc), 1, fp);
	
		ulong sp;
		int i;
	
		// save materials
		fprintf (stderr, "writing materials...\n");
		sp = (ulong)s->mtlNameList ().size ();
		fwrite (&sp, sizeof (ulong), 1, fp);
	
		for (i = 0; i < (int)s->mtlNameList ().size (); i++)
		{
			cStr mtlName = s->mtlNameList ()[i];
			uchar l = (uchar)mtlName.size ();
			fwrite (&l, 1, 1, fp);
			fwrite (mtlName.c_str (), mtlName.size (), 1, fp);
		}
	
		fprintf (stderr, "writing scene hierarchy...\n");
		saveGeometry (fp);
	
		fclose (fp);
	}
	
	void	expModel::saveGeometry (FILE *fp) const
	{
		ulong sp;
	
		fprintf (stderr, "writing name: %s\n", name ());
		char nsz = strlen (name ());
		fwrite (&nsz, sizeof (char), 1, fp);
		if (nsz)
			fwrite (name (), nsz, 1, fp);
	
		sp = sizeof (matrix4);
		fwrite (&mRelativeMatrix, sp, 1, fp);
	
		fprintf (stderr, "writing %d subsets\n", mSubSets.size ());
		sp = (ulong)mSubSets.size ();
		fwrite (&sp, sizeof (ulong), 1, fp);
		fwrite (&mSubSets.front (), sizeof (subset), mSubSets.size (), fp);
	
		fprintf (stderr, "tangents: %s\n", mbTangents ? "yes" : "no");
		uchar tangents = mbTangents ? 1 : 0;
		fwrite (&tangents, sizeof (uchar), 1, fp);
	
		fprintf (stderr, "ligthmap coords: %s\n", mbLightmaps ? "yes" : "no");
		uchar lightmaps = mbLightmaps ? 1 : 0;
		fwrite (&lightmaps, sizeof (uchar), 1, fp);
		if (mbLightmaps)
		{
			// write lightmap texture name
			uchar sz = mLightmapName.size ();
			fwrite (&sz, 1, 1, fp);
			if (sz)
				fwrite (&mLightmapName[0], sz, 1, fp);
		}
	
		fprintf (stderr, "%d verts\n", mVerts.size ());
		sp = (ulong)mVerts.size ();
		fwrite (&sp, sizeof (ulong), 1, fp);
		fwrite (&mVerts.front (), sizeof (drawVertex_t), mVerts.size (), fp);
	
		fprintf (stderr, "%d faces\n", mFaces.size ());
		sp = (ulong)mFaces.size ();
		fwrite (&sp, sizeof (ulong), 1, fp);
		fwrite (&mFaces.front (), sizeof (face), mFaces.size (), fp);
	
	#if 0
		// animation
		uchar b = mpAnimationController->numFrames () != 0;
		fwrite (&b, sizeof (uchar), 1, fp);
		if (b)
		{
			float fr = mpAnimationController->frameRate ();
			fwrite (&fr, sizeof (float), 1, fp);
			sp = (ulong)mpAnimationController->numFrames ();
			fwrite (&sp, sizeof (ulong), 1, fp);
			for (int i = 0; i < (int)sp; i++)
			{
				const animationFrame &frm = mpAnimationController->getFrame (i);
				fwrite (&frm, sizeof (animationFrame), 1, fp);
			}
		}
		#endif
		// write children
		sp = 0;
		for (sceneObject *c = mpChildren; c; c = c->getNextChild (), sp++);
		
		fwrite (&sp, sizeof (ulong), 1, fp);
	
		for (sceneObject *c = mpChildren; c; c = c->getNextChild ())
		{
			const expModel *exp = checked_cast <const expModel *> (c);
			exp->saveGeometry (fp);
		}
	}
	
	//----------------------
	// expRefObject
	//----------------------
	expRefObject::expRefObject (expObject *parent)
		: expObject (parent)
	{
		mClassID = REF_OBJECT_CLASS_ID;
		mpScene = NULL;
	}
	
	void expRefObject::load (charParser &p, const char *fname)
	{
	
	}
	
	void expRefObject::render (expScene *scene)
	{
		if (mpScene)
			mpScene->render ();
		expObject::render (scene);
	}
	
	//----------------------
	// expEntityObject
	//----------------------
	expEntityObject::expEntityObject (expObject *parent)
		: expObject (parent)
	{
		mClassID = ENTITY_OBJECT_CLASS_ID;
	}
	
	void expEntityObject::load (charParser &p, const char *filename)
	{
		p.getToken ();
		mName = p.token ();
		p.matchToken ("{");
	
		cStr fname = filename;
	
		fname += "_";
		fname += mName;
	
		for (;;)
		{
			p.getToken ();
			if (p.token () == "}")
				break;
			else if (baseLoad (p, fname))
			{
			}
			else if (!p.cmptoken ("prop"))
			{
				p.getToken ();
				cStr key = p.token ();
				p.getToken ();
				mProps[key] = p.token ();
			}
			else
				p.generalSyntaxError ();
		}
	}
	
	void expEntityObject::render (expScene *scene)
	{
		expObject::render (scene);
	}
	
	//----------------------
	// expDummyObject
	//----------------------
	
	expDummyObject::expDummyObject (expObject *parent)
		: expObject (parent)
	{
		mClassID = DUMMY_OBJECT_CLASS_ID;
	}
	
	void expDummyObject::load (charParser &p, const char *filename)
	{
		p.getToken ();
		mName = p.token ();
		p.matchToken ("{");
	
		cStr fname = filename;
		fname += _T ("_");
		fname += mName;
	
		for (;;)
		{
			p.getToken ();
			if (p.token () == "}")
				break;
			else if (baseLoad (p, fname))
			{
			}
			else
				p.generalSyntaxError ();
		}
	}
	
	void expDummyObject::render (expScene *scene)
	{
		expObject::render (scene);
	}
	
	//----------------------
	// expObject
	//----------------------
	expObject::expObject (expObject *parent)
		: mMatrix (true)
	{
		mpParent = parent;
	}
	
	bool expObject::baseLoad (charParser &p, const char *fname)
	{
		if (!p.cmptoken ("nodeTransform"))
		{
			p.matchToken ("{");
			// matrix
			mMatrix.identity ();
			for (int i = 0; i < 4; i++)
			{
				for (int k = 0; k < 3; k++)
				{
					p.getToken ();
					mMatrix.m[i][k] = atof (p.token ());
				}
			}
			p.matchToken ("}");
	
			if (mpParent)
				mWorldMatrix = mMatrix * mpParent->getWorldMatrix ();
			else
				mWorldMatrix = mMatrix;
		}
		else if (!p.cmptoken ("dummy"))
		{
			expDummyObject *o = new expDummyObject (this);
			o->load (p, fname);
			mChildren.push_back (o);
		}
		else if (!p.cmptoken ("triobject"))
		{
			expTriObject *o = new expTriObject (this);
			o->load (p, fname);
			mChildren.push_back (o);
		}
		else if (!p.cmptoken ("entity"))
		{
			expEntityObject *o = new expEntityObject (this);
			o->load (p, fname);
			mChildren.push_back (o);
		}
		else
			return false;
	
		return true;
	}
	
	void expObject::setMatrix (const matrix4 &m)
	{
		mMatrix = m;
	
		// recalc world matrix
		mWorldMatrix = mpParent ? mMatrix * mpParent->getWorldMatrix () : mMatrix;
	
		// retransform children
		for (size_t i = 0; i < mChildren.size (); i++)
		{
			mChildren[i]->recalcWorldTransforms ();
		}
	}
	
	void expObject::setWorldMatrix (const matrix4 &m)
	{
		mWorldMatrix = m;
		
		// recalc relative matrix
		mMatrix = mpParent ? mWorldMatrix * mpParent->getWorldMatrix ().inverse () : mWorldMatrix;
	
		// retransform children
		for (size_t i = 0; i < mChildren.size (); i++)
		{
			mChildren[i]->recalcWorldTransforms ();
		}
	}
	
	void expObject::recalcWorldTransforms (void)
	{
		mWorldMatrix = mpParent ? mMatrix * mpParent->getWorldMatrix () : mMatrix;
		for (size_t i = 0; i < mChildren.size (); i++)
		{
			mChildren[i]->recalcWorldTransforms ();
		}
	}
	
	void expObject::render (const expScenePtr &scene) const
	{
		for (size_t i = 0; i < mChildren.size (); i++)
			mChildren[i]->render (scene);
	}
	
	void expObject::addChild (const expObjectPtr &child)
	{
		child->setParent (this);
	}
	
	void expObject::removeChild (const expObjectPtr &child)
	{
		assert (child);
		assert (child->getParent () == this);
		child->setParent (NULL);
	}
	
	void expObject::setParent (expObject *parent)
	{
		if (NULL != mpParent)
		{
			for (childrenList::iterator it = mpParent->mChildren.begin (); it != mpParent->mChildren.end (); it++)
			{
				if (*it == this)
				{
					mpParent->mChildren.erase (it);
					mpParent = NULL;
					break;
				}
			}
		}
	
		if (NULL != parent)
		{
			mpParent = parent;
			mpParent->mChildren.push_back (this);
		}
	}
	
}

