//------------------------------------------------
// -+- best viewed with tabsize 4
//  level.cpp: level conversion (fgpExpModel->dxlevel)
//             lightmap generation using light volumes algorithm
//  started by waker, july 2001
//------------------------------------------------
#include <fegdk/pch.h>
#include "fgp.h"

#if 0
// TODO:
// check convex hull computations on more complex surfaces (random set of points?..)
// implement lightvolume::classify

//----------------------------------
// notes
//----------------------------------

/* ----------------------------------------------------------------------------------------

[ lighting, waker, Tue 21 Aug 17:17:25 2001 ]

  light volume is a set of planes describing volume (not necessarily closed)
  used to cull other surfaces which do not affect specific surface.

  it is built using convex hull of lightsource-projected surface and light position.

  when calculating volume for directional lights - planes are calculated on the convex hull
  points moved along light direction, i.e. all planes are coplanar with light direction.

  for area lights light volume is built using rays casted from each area's point
  into nearest non-culled points of surface's convex hull.

---------------------------------------------------------------------------------------- */

//----------------------------------
// prototypes
//----------------------------------

class AdjFace {
public:

	Mgc::Plane plane;

	int faceIndex;
/*
// no need to store that
	ulong v[3];			// position indices
	ulong n[3];			// normal indices
	ulong uv[3];		// uv indices
	ulong uvLight[3];	// lightmap uv indices
*/

	bool mark;
	
	int mtl;
	std::vector< int > adjacent;

	// NOTE: big doesn't necessary slow :)
	AdjFace( fgpExpModel::Mesh *mesh_p, int nFace, std::vector<int> *sortIndex ) {
		// init
		mark = false;

		// fill with data
		faceIndex = nFace;
		ulong v[3];
		ulong n[3];
		ulong uv[3];
		ulong uvLight[3];
		v[0] = mesh_p->faces[nFace].v1;
		v[1] = mesh_p->faces[nFace].v2;
		v[2] = mesh_p->faces[nFace].v3;
		mtl = mesh_p->faces[nFace].material;
		n[0] = mesh_p->normalFaces[nFace].v1;
		n[1] = mesh_p->normalFaces[nFace].v2;
		n[2] = mesh_p->normalFaces[nFace].v3;
		uv[0] = mesh_p->tFaces[nFace].v1;
		uv[1] = mesh_p->tFaces[nFace].v2;
		uv[2] = mesh_p->tFaces[nFace].v3;
		uvLight[0] = 0;
		uvLight[1] = 0;
		uvLight[2] = 0;

		Mgc::Vector3 v1, v2;
		v1 = mesh_p->verts[v[1]].pos - mesh_p->verts[v[0]].pos;
		v2 = mesh_p->verts[v[2]].pos - mesh_p->verts[v[1]].pos;
		plane.Normal() = v2.Cross( v1 );
		plane.Normal().Unitize();
#if 0
		if ( mFaces[face].mPlane.Normal().Unitize() < 0.1f )
			return false;
#endif
		plane.Constant() = mesh_p->verts[v[0]].pos.Dot( plane.Normal() );

		// build adjacency info
		std::vector<int> &a = sortIndex[v[0]];
		std::vector<int> &b = sortIndex[v[1]];
		std::vector<int> &c = sortIndex[v[2]];

		bool valid[3] = { true, true, true };
		int ai = 0, bi = 0, ci = 0;

		int num;

		while ( 1 )
		{
			num = 0;

			if ( ai >= a.size() )
			{
				valid[0]=false;
				num++;
			}

			if ( bi >= b.size() )
			{
				valid[1]=false;
				num++;
			}
			
			if ( ci >= c.size() )
			{
				valid[2]=false;
				num++;
			}

			if ( num>=2 ) // only one index left
				break;

			if ( valid[0] && a[ai] != nFace )
			{
				// ab
				if ( valid[1] && a[ai] == b[bi])
				{
					adjacent.push_back( a[ai] );
					ai++; bi++;
					continue;
				}
				// ac
				if ( valid[2] && a[ai] == c[ci] )
				{
					adjacent.push_back( a[ai] );
					ai++; ci++;
					continue;
				}
			}
			if ( valid[1] && valid[2] && b[bi] != nFace )
			{
				// bc
				if ( b[bi] == c[ci] )
				{
					adjacent.push_back( b[bi] );
					bi++; ci++;
					continue;
				}
			}
			// find min and increment
			int min_i = -1;
			if ( valid[0] )
				min_i = 0;
			if ( valid[1] )
			{
				if ( min_i == -1 || b[bi] < a[ai] )
					min_i = 1;
			}
			if ( valid[2] )
			{
				if ( min_i == -1 || ( min_i == 0 && c[ci] < a[ai] ) || ( min_i == 1 && c[ci] < b[bi] ) )
					min_i = 2;
			}

			assert( min_i != -1 );
			if ( min_i == 0 )
				ai++;
			else if ( min_i == 1 )
				bi++;
			else
				ci++;
		}
	}
};

class LightVolume;
class LightSource;
class Surface;

#define THRESHOLD 0.9

class Surface {
public:

	class Face {
	public:
		ushort v[3];
	};

	Surface( fgpExpModel::Mesh *Mesh, AdjFace *face_p, std::vector< AdjFace * > &faceList ) {

		mesh = Mesh;

		build( face_p, faceList );

		// find projection plane as average of face planes

		int face_i;

		projPlane.Normal() = Mgc::Vector3( 0, 0, 0 );
		projPlane.Constant() = 0;
		for ( face_i = 0; face_i < faces.size(); face_i++ )
		{
			projPlane.Normal() += faces[face_i]->plane.Normal();
			projPlane.Constant() += faces[face_i]->plane.Constant();
		}
		projPlane.Normal().Unitize();
		projPlane.Constant() /= faces.size();

		std::map< ushort, ushort > indexMap;

		// project surface vertices on plane
		for ( face_i = 0; face_i < faces.size(); face_i++ )
		{
			std::map< ushort, ushort >::iterator f;

			Face newFace;
			for ( int k = 0; k < 3; k++ )
			{
				ushort face = mesh->faces[face_i].v[k];
				std::map< ushort, ushort >::iterator f = indexMap.find( face );
				if ( f == indexMap.end() )
				{
					// project new vertex
					indexMap[face] = projVerts.size();
					float d = projPlane.Normal().Dot( mesh->verts[face].pos ) - projPlane.Constant();
					Mgc::Vector3 v = mesh->verts[face].pos - projPlane.Normal() * d;
					newFace.v[k] = projVerts.size();
					projVerts.push_back( v );
					verts.push_back( face );
				}
				else
				{
					newFace.v[k] = (*f).second;
				}
			}
			projFaces.push_back( newFace );
		}
		
		// find center
		center = Mgc::Vector3( 0, 0, 0 );
		for ( int vertex_i = 0; vertex_i < projVerts.size(); vertex_i++ )
			center += projVerts[vertex_i];
		center /= projVerts.size();
	}

	void build( AdjFace *face_p, std::vector< AdjFace * > &faceList )
	{
		int face_i;

		if ( face_p->mark != false )
			return;

		if ( faces.size() == 0 )
		{
			// base face
			face_p->mark = true;
			faces.push_back( face_p );

			// recurse with all adjacents
			for ( face_i = 0; face_i < face_p->adjacent.size(); face_i++ )
			{
				build( faceList[face_p->adjacent[face_i]], faceList );
			}
		}
		else
		{
			// compare face normal with all previous faces
			for ( face_i = 0; face_i < faces.size(); face_i++ )
			{
				float dot = faces[face_i]->plane.Normal().Dot( face_p->plane.Normal() );
				if ( dot < THRESHOLD )
					break;
			}

			if ( face_i == faces.size() )
			{
				face_p->mark = true;
				faces.push_back( face_p );

				// recurse with all adjacents
				for ( int face_i = 0; face_i < face_p->adjacent.size(); face_i++ )
				{
					build( faceList[face_p->adjacent[face_i]], faceList );
				}
			}
		}
	}

	fgpExpModel::Mesh *mesh;

	Mgc::Plane projPlane;
	Mgc::Vector3 center;
	std::vector< AdjFace * > faces;

	std::vector< ushort > verts; // indices into original (untransformed) vertices
	std::vector< Mgc::Vector3 > projVerts;
	std::vector< Face > projFaces;

	int lightmap;
};

#undef THRESHOLD

#define THRESHOLD 0.0001f

class LightVolume {
public:

	std::vector<Mgc::Plane> volume;

	struct chPoint_s {
		ushort index;
		Mgc::Vector3 pos;
		float cosAngle;
	};

	LightVolume( Surface *surface, const Mgc::Vector3 &light ) {

		Mgc::Vector3 lightDir = surface->center - light;
//		lightDir = Mgc::Vector3( 0, 0, 1 );
		lightDir.Unitize();

		float d = surface->projPlane.Normal().Dot( lightDir );
		if ( d >= 0 )
			return;

		// prepare storage
		chPoint_s *points = new chPoint_s[surface->projVerts.size()];

		memset( points, 0, sizeof( chPoint_s ) * surface->projVerts.size() );

		// project as 2d
		feMatrix4 proj;

#if 0 // does quake's method work better?..
		// calculate up vector ( it can be random perpendicular to center-light vector )
		Mgc::Vector3 up = surface->center - light;
		up.Unitize();
		Mgc::Vector3 dominant;
		
		// get minimal axis and select it as dominant

		float x = fabs( up.x );
		float y = fabs( up.y );
		float z = fabs( up.z );

		if ( x < y )
		{
			if ( x < z )	dominant = Mgc::Vector3( 1, 0, 0 ); // x
			else			dominant = Mgc::Vector3( 0, 0, 1 ); // z
		}
		else
		{
			if ( y < z )	dominant = Mgc::Vector3( 0, 1, 0 ); // y
			else			dominant = Mgc::Vector3( 0, 0, 1 ); // z
		}

		// get perpendicular
		up = up ^ dominant;

		proj.camera( light, surface->center, up );
#else
		Mgc::Vector3 front, right, up;
		front = surface->center - light;
		front.Unitize();
		MakeNormalVectors( front, right, up );

		// make view matrix
		proj._11 = right.x;	proj._12 = right.y; proj._13 = right.z;	proj._14 = 0;
		proj._21 = up.x;	proj._22 = up.y;	proj._23 = up.z;	proj._24 = 0;
		proj._31 = front.x;	proj._32 = front.y;	proj._33 = front.z;	proj._34 = 0;
#if 0 // do not move projection - no need to do it
		proj._41 = surface->center.x;
		proj._42 = surface->center.y;
		proj._43 = surface->center.z;
#else
		proj._41 = 0;		proj._42 = 0;		proj._43 = 0;
#endif
		proj._44 = 1;

		// do i really need to invert
		proj = proj.inverse();
#endif

		Mgc::Vector3 pt = Mgc::Vector3( 1, 0, 0 );
		
		int p0 = 0;
		int point_i;

		// transform (project) points
		for ( point_i = 0; point_i < surface->projVerts.size(); point_i++ )
		{
			points[point_i].index = point_i;
			// using original (untransformed) vertices to build 2d-projected convex hull
			points[point_i].pos = surface->mesh->verts[surface->verts[point_i]].pos * proj;
			points[point_i].pos.z = 0;

			if ( point_i > 0 )
			{
				// check for equal ordinate
				if ( fabs( points[point_i].pos.y - points[p0].pos.y ) < THRESHOLD )
				{
					if ( points[point_i].pos.x < points[p0].pos.x )
						p0 = point_i;
				}
				else if ( points[point_i].pos.y < points[p0].pos.y )
					p0 = point_i;
			}
		}

		// insert p0 into beginning
		chPoint_s p = points[0];
		points[0] = points[p0];
		points[p0]=p;

		// calculate angles
		for ( point_i = 1; point_i < surface->projVerts.size(); point_i++ )
		{
			float l = (points[point_i].pos - points[0].pos).Length();
			points[point_i].cosAngle = points[point_i].pos.x / l;
		}

		// sort by cosine descending
		qsort( points+1, surface->projVerts.size()-1, sizeof( chPoint_s ),
			reinterpret_cast< int ( __cdecl * )( const void *, const void * ) > ( comparePoints ) );

		// build convex hull of projected surface points looking from light position
		// not really an optimal CH, 'cause can contain duplicated planes


		ushort *ch = new ushort[surface->projVerts.size()];
		// add first 3 pts
		ch[0] = 0;
		ch[1] = 1;
		ch[2] = 2;
		int numChPoints = 3;

		for ( point_i = 3; point_i < surface->projVerts.size(); point_i++ )
		{
			while ( numChPoints > 0 )
			{
				Mgc::Vector3 *p, *p1, *p2;
				p = &points[point_i].pos;
				p1 = &points[ch[numChPoints-2]].pos;
				p2 = &points[ch[numChPoints-1]].pos;
				float d = ( ( p->x - p1->x ) / ( p2->x - p1->x ) ) - ( (p->y - p1->y ) / ( p2->y - p1->y ) );

				if ( d < 0 )
					break;

				numChPoints--;
			}

			// add new point
			ch[numChPoints] = point_i;
			numChPoints++;
		}

		// build volume
		if ( numChPoints >= 3 )
		{
			volume.reserve(numChPoints);
			for ( point_i = 0; point_i < numChPoints; point_i++ )
			{
				// add plane
				Mgc::Plane p;
				const Mgc::Vector3
					&a = light,
					&b = surface->projVerts[ch[point_i]],
					&c = surface->projVerts[( ch[point_i]+1 ) % numChPoints];
				Mgc::Vector3 v1, v2;
				v1 = b - a;
				v2 = c - b;
				p.Normal() = v2.Cross( v1 );
				p.Normal().Unitize();
				p.Constant() = a.Dot( p.Normal() );
				volume.push_back( p );
			}
		}

		delete[] ch;
		delete[] points;
	}

	static int __cdecl comparePoints( const chPoint_s *a, const chPoint_s *b ) {
		if ( a->cosAngle < b->cosAngle )
			return 1;
		else if ( a->cosAngle > b->cosAngle )
			return -1;
		else
			return 0;
	}

	// 0 - not intersects
	// 1 - intersects
	// 2 - splits
	int classifySurface( Surface *surf );
};

#undef THRESHOLD

typedef std::vector< Surface * > surfaceList_t;

surfaceList_t *findSurfaces( fgpExpModel::Mesh *mesh_p )
{
	surfaceList_t *surfaces = new surfaceList_t;

	std::vector< AdjFace * > faces;

	int face_i;

	printf( "generating adjacency info...\n" );

	faces.reserve( mesh_p->numFaces );

	// create sort index
	std::vector<int> *sortIndex = new std::vector<int>[mesh_p->numVerts];

	// add faces
	for ( face_i = 0; face_i < mesh_p->numFaces; face_i++ )
	{
		fgpExpModel::Face *face_p = &mesh_p->faces[face_i];
		sortIndex[face_p->v1].push_back( face_i );
		sortIndex[face_p->v2].push_back( face_i );
		sortIndex[face_p->v3].push_back( face_i );
	}

	for ( face_i = 0; face_i < mesh_p->numFaces; face_i++ )
	{
		AdjFace *face = new AdjFace( mesh_p, face_i, sortIndex );
		int v0 = mesh_p->faces[face->faceIndex].v1;
		int v1 = mesh_p->faces[face->faceIndex].v2;
		int v2 = mesh_p->faces[face->faceIndex].v3;
		faces.push_back( face );
		printf( "%d%% done   \r", 100 * ( face_i + 1 ) / mesh_p->numFaces );
	}

	printf( "\n" );

	delete[] sortIndex;

	printf( "generating surfaces...\n" );

#if 0 // algo of surface generator
	for each face {
		surface = new surface(face);
		for each adj {
			if ( dot( face->norm, adj->norm ) < threshold &&
				 dot( surface->norm, adj->norm ) < threshold )
			{
				surface->add( adj );
				facelist->markAsRead( adj );
			}
		}
		faceList->removeMarked();
	}
#endif

	int size = 0;
	while ( size != faces.size() )
	{
		// create new surface
		for ( face_i = 0; face_i < faces.size(); face_i++ )
		{
			if ( faces[face_i]->mark == false )
			{
				Surface *surf = new Surface( mesh_p, faces[face_i], faces );
				LightVolume *volume = new LightVolume( surf, Mgc::Vector3( 0, 0, -100 ) );
				printf( "surface created, %d faces", surf->faces.size() );
				if ( volume->volume.size() == 0 )
					printf( " ( invisible from the light source )" );
				printf( ".\n" );
				delete volume;
				size += surf->faces.size();
				surfaces->push_back( surf );
			}
		}
	}

	printf( "done.\n" );

	for ( face_i = 0; face_i < faces.size(); face_i++ )
		delete faces[face_i];

	faces.clear();

	return surfaces;
}

void MakeLevelFile( int argc, char *argv[] )
{
#if 0 // this is an algo
	// [+] means "implemented"
	findSurfaces( mesh ); // [+]
	for each surface {
		plane_t plane = projectOnPlane( surface );
		matrix_t orientation = findBestOrientation( surface, plane );
		transformSurface( surface, orienation );
		rect_t rect = getSurfaceBoundingRect( surface );
		texture_t texture = createTexture( rect );
		renderSurfaceToTexture( texture, surface );
		calculateTextureCoordinates( surface );

		texture_t lightmap = findLightmapPageHavingEnoughFreeSpace( rect );
		if ( lightmap == NULL )
			lightmap = createNewLightmapPage();

		rect_t lightmapRect = getLightmapRect( lightmap, rect );
		copyTexture( texture, lightmap, rect, lightmapRect );
		delete texture;

		transformTextureCoordinates( surface, rect, lightmapRect );

		surface->lightmap = lightmap->index;
	}
	saveSurfaces();
#endif

	char *fname = argv[1];
	printf( "loading model...\n" );

	fgpExpModel *model = new fgpExpModel;
	model->load( fname );

	printf( "model ready, converting...\n" );

	fgpExpModel::MeshSystem *meshSystem_p;
	fgpExpModel::Mesh *mesh_p;
	int meshSystem_i;
	int mesh_i;

	for ( meshSystem_i = 0; meshSystem_i < model->meshSystems.size(); meshSystem_i++ )
	{
		meshSystem_p = model->meshSystems[meshSystem_i];

		for ( mesh_i = 0; mesh_i < meshSystem_p->meshes.size(); mesh_i++ )
		{
			mesh_p = meshSystem_p->meshes[mesh_i];

			surfaceList_t *surfaces = findSurfaces( mesh_p );
			
			surfaceList_t::iterator it;
			for ( it = surfaces->begin(); it != surfaces->end(); it++ )
				delete *it;

			delete surfaces;
		}
	}

	model->release();
}

#endif
