//-------------------------------------------
// some simple funcs
//-------------------------------------------
#include <fegdk/pch.h>
#include "fgp.h"

#define EPS 0.001

bool Vector2_EQ( const fe::vector2 &v1, const fe::vector2 &v2 )
{
	if (
		fabs( v1.x - v2.x ) >= EPS ||
		fabs( v1.y - v2.y ) >= EPS
		)
	{
		return false;
	}

	return true;
}

bool Vector3_EQ( const fe::vector3 &v1, const fe::vector3 &v2 )
{
	if (
		fabs( v1.x - v2.x ) >= EPS ||
		fabs( v1.y - v2.y ) >= EPS ||
		fabs( v1.z - v2.z ) >= EPS
		)
	{
		return false;
	}

	return true;
}

bool Vector4_EQ( const float *v1, const float *v2 )
{
	if (
		fabs( v1[0] - v2[0] ) >= EPS ||
		fabs( v1[1] - v2[1] ) >= EPS ||
		fabs( v1[2] - v2[2] ) >= EPS ||
		fabs( v1[3] - v2[3] ) >= EPS
		)
	{
		return false;
	}

	return true;
}

#undef EPS

