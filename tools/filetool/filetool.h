#pragma once

#include "f_string.h"
#include "f_filesystem.h"
#include "../lib/re_lib/regexp.h"

class feFileToolErr
{
	feStr mMsg;

public:
	
	feFileToolErr( const feStr &msg )
	{
		mMsg = msg;
	}

	const feStr& msg( void ) const
	{
		return mMsg;
	}

};

class feRegexp
{
	
	regexp *mpRegexp;

public:

	feRegexp( const feCStr &s )
	{
		feCStr val = s;
		val.tolower();
		char *v = new char[val.size()+1];
		strcpy( v, val.c_str() );
		mpRegexp = regcomp( v );
		delete[] v;

		if ( !mpRegexp )
		{
			feStr msg;
			msg.printf( _T( "invalid regexp: %s" ), feStr( s ).c_str() );
			throw feFileToolErr( msg );
		}
	}

	~feRegexp()
	{
		regfree( mpRegexp );
	}

	regexp *getRegexp( void )
	{
		return mpRegexp;
	}

};

class feFileTool
{

protected:

	enum command_e
	{
		cmdRebuild	=	1,
	};

	struct file
	{
		file()
		{
			memset( name, 0, 32 );
			blocktype = feFSBlockType_BinaryCompressed;
			size = 0;
			comprsize = 0;
			offset = 0;
			parent = NULL;
		}
		~file()
		{
			for ( size_t i = 0; i < children.size(); i++ )
				delete children[i];
		}
		char					name[32];
		feFSBlockType			blocktype;
		ulong					size;
		ulong					comprsize;
		ulong					offset;
		file*					parent;
		std::vector< file* >	children;
	};

	std::vector< feRegexp * >		mIgnoreList;
	std::map< feRegexp *, feCStr >	mRulesList;
	file*							mpRoot;
	ulong							mTableOffset;
	int								mArgc;
	TCHAR**							mArgv;

	int								mCurrentFile;
	int								mNumFiles;
	double							mStartTime;
	unsigned int					mTotalSize;

	// params
	feStr							mOutFile;
	command_e						mCommand;

	// commands impl
	void rebuild( void );

	// misc
	void printUsage( void );
	bool parseArgs( void );			// returns false in no commands collected, true otherwise
	void printOptionsHelp( void );
	void printCommandsHelp( void );
	void printCommandHelp( int argIndex );
	void loadIgnoreList( void );
	void loadRulesList( void );
	void createFSStructure( file *f );
	void processFiles( FILE *fp, file *f, const feCStr &dir );
	void writeTable( FILE *fp, file *f );

	bool testIgnoreList( const feStr &s );
	feCStr findRule( const feStr &s );

	void dbgPrintTree( file *f, int indent = 0 );

public:

	feFileTool( int argc, TCHAR *argv[] );
	~feFileTool();
	int	run();
};

