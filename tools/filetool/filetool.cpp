#include "../common/pch.h"
#include "filetool.h"

// tool used to pack and compress all game data into big .pak file(s)

int _tmain( int argc, TCHAR *argv[] )
{
	feFileTool *ft = new feFileTool( argc, argv );
	int res = ft->run();
	delete ft;
#ifdef _DEBUG
	_CrtDumpMemoryLeaks();
#endif
	return res;
}
