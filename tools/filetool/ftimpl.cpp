#include "../common/pch.h"
#include "filetool.h"
#include "f_parser.h"
#include "zlib.h"

feFileTool::feFileTool( int argc, TCHAR *argv[] )
{
	mArgc = argc;
	mArgv = argv;
	mpRoot = NULL;
	mTableOffset = 0;
	mNumFiles = 0;
	mCurrentFile = 0;
}

feFileTool::~feFileTool()
{
	std::vector< feRegexp * >::iterator itIgnore;
	for ( itIgnore = mIgnoreList.begin(); itIgnore != mIgnoreList.end(); itIgnore++ )
		delete ( *itIgnore );
	std::map< feRegexp *, feCStr >::iterator itRules;
	for ( itRules = mRulesList.begin(); itRules != mRulesList.end(); itRules++ )
		delete ( *itRules ).first;
	if ( mpRoot )
		delete mpRoot;
}

int	feFileTool::run()
{
	try {

		// parse command line
		if ( mArgc <= 1 )
		{
			printUsage();
			return 0;
		}

		if ( !parseArgs() )
			return false;

		if ( mCommand == cmdRebuild )
			rebuild();
		// else
		// no commands were specified
	}
	catch ( feFileToolErr e )
	{
		_tprintf( _T( "%s\n" ), e.msg().c_str() );
		return -1;
	}
	return 0;
}

void feFileTool::rebuild( void )
{
	if ( mOutFile.empty() )
	{
		_tprintf( _T( "rebuild command requires output filename. use -o option.\n" ) );
		return;
	}
	loadIgnoreList();
	loadRulesList();

	mpRoot = new file;
	mpRoot->blocktype = feFSBlockType_Folder;
	createFSStructure( mpRoot );
	_tprintf( _T( "total number of files: %d\n" ), mNumFiles );

#ifdef _DEBUG
	dbgPrintTree( mpRoot );
#endif

	_tprintf( _T( "opening '%s' for writing...\n" ), mOutFile.c_str() );
	FILE *fp = _tfopen( mOutFile.c_str(), _T( "w+b" ) );
	if ( !fp )
	{
		_tprintf( _T( "cannot open file. exiting.\n" ) );
		return;
	}

	_tprintf( _T( "writing magic...\n" ) );
	char magic[] = "FEPAK";
	fwrite( magic, strlen( magic ), 1, fp );
	fwrite( &mTableOffset, sizeof( mTableOffset ), 1, fp );

	mStartTime = (double)clock() / CLOCKS_PER_SEC;

	mTotalSize = 0;
	processFiles( fp, mpRoot, "" );
	_tprintf( _T( "\n" ) );

	_tprintf( _T( "approximated filesize = %.3lf mb\n" ), mTotalSize / ( 1024.0 * 1024.0 ) );

	mTableOffset = ftell( fp );
	writeTable( fp, mpRoot );
	fseek( fp, (long)strlen( magic ), SEEK_SET );
	fwrite( &mTableOffset, sizeof( mTableOffset ), 1, fp );
	_tprintf( _T( "closing output file...\n" ) );
	fclose( fp );
	_tprintf( _T( "done!\n" ) );
}

void feFileTool::printUsage( void )
{
	_tprintf( _T( "usage: filetool.exe [options] command [command-options-and-arguments]\n" ) );
	_tprintf( _T( "  where options are -o, -c, etc.\n" ) );
	_tprintf( _T( "    (specify --help-options for a list of options)\n" ) );
	_tprintf( _T( "  where command are add, update, remove, etc.\n" ) );
	_tprintf( _T( "    (specify --help-commands for a list of commands)\n" ) );
	_tprintf( _T( "  where command-options-and-arguments depend on the specific command\n" ) );
	_tprintf( _T( "    (specify --help followed by a command name for command-specific help)\n" ) );
	_tprintf( _T( "  specify --help to recieve this message\n" ) );
	_tprintf( _T( "\nfe file processing tool v.0.1\n" ) );
}

bool feFileTool::parseArgs( void )
{
	for ( int i = 1; i < mArgc; i++ )
	{
		// help
		if ( !_tcsicmp( mArgv[i], _T( "--help" ) ) )
		{
			if ( i == mArgc-1 )
			{
				printUsage();
				return false;
			}
			else
			{
				i++;
				printCommandHelp( i );
				return false;
			}
		}
		else if ( !_tcsicmp( mArgv[i], _T( "--help-options" ) ) )
		{
			printOptionsHelp();
			return false;
		}
		else if ( !_tcsicmp( mArgv[i], _T( "--help-commands" ) ) )
		{
			printCommandsHelp();
			return false;
		}
		// options
		else if ( !_tcsnicmp( mArgv[i], _T( "-o" ), 2 ) )		// outfile
		{
			mOutFile = mArgv[i]+2;
			if ( mOutFile.empty() )
			{
				_tprintf( _T( "empty filename specified with -o\n" ) );
			}
		}
		// commands
		else if ( !_tcsicmp( mArgv[i], _T( "rebuild" ) ) )		// rebuild all
		{
			mCommand = cmdRebuild;
			return true;
		}
		// error
		else
		{
			_tprintf( _T( "unknown keyword: %s\n" ), mArgv[i] );
			return false;
		}
	}
	return true;
}

void feFileTool::printOptionsHelp( void )
{
	_tprintf( _T( "filetool global options (specified before command name) are:\n" ) );
	_tprintf( _T( "\t-o filename\t\tuse 'filename' as output file.\n" ) );
	_tprintf( _T( "\n(specify the --help global option for a list of other help options)\n" ) );
}

void feFileTool::printCommandsHelp( void )
{
	_tprintf( _T( "filetool commands are:\n" ) );
	_tprintf( _T( "\trebuild\t\trebuilds entire package from scratch.\n" ) );
	_tprintf( _T( "\n(specify the --help global option for a list of other help options)\n" ) );
}

void feFileTool::printCommandHelp( int argIndex )
{
	if ( !_tcsicmp( mArgv[argIndex], _T( "rebuild" ) ) )
	{
		_tprintf( _T( "usage: filetool rebuild\n" ) );
	}
	_tprintf( _T( "(specify the --help global option for a list of other help options)\n" ) );
}

void feFileTool::loadIgnoreList()
{
	_tprintf( _T( "reading '.ftignore'...\n" ) );
	bool	expEof;
	try {
		feCharParser p( ".ftignore", true );
		for ( ;; )
		{
			expEof = true;
			p.getToken();
			expEof = false;
			feRegexp *r = new feRegexp( p.token() );
			mIgnoreList.push_back( r );
			p.matchToken( ";" );
		}
	}
	catch ( feParserException e )
	{
		if ( e == feParserException::FILE_NOT_FOUND )
		{
			_tprintf( _T( "'.ftignore' does not exist. all contents will be processed.\n" ) );
		}
		else if ( e == feParserException::END_OF_FILE && false == expEof )
		{
			throw feFileToolErr( _T( "unexpected eof while reading '.ftignore'" ) );
		}
		else if ( e != feParserException::END_OF_FILE )
			_tprintf( _T( "parser error: %s" ), feStr (e) );
	}

	// add always-ignored items
	mIgnoreList.push_back( new feRegexp( "\\.ftignore$" ) );
	mIgnoreList.push_back( new feRegexp( "\\.ftrules$" ) );
	mIgnoreList.push_back( new feRegexp( "filetool\\.exe$" ) );
}

void feFileTool::loadRulesList()
{
	_tprintf( _T( "reading '.ftrules'...\n" ) );
	bool	expEof;
	try {
		feCharParser p( ".ftrules", true );
		for ( ;; )
		{
			expEof = true;
			p.getToken();
			feCStr re = p.token();
			expEof = false;
			p.getToken();
			feRegexp *r = new feRegexp( re );
			mRulesList[r] = p.token();
			p.matchToken( ";" );
		}
	}
	catch ( feParserException e )
	{
		if ( e == feParserException::FILE_NOT_FOUND )
		{
			_tprintf( _T( "'.ftrules' does not exist. all files will be converted to compressed binaries ( feFSBlockType_BinaryCompressed ).\n" ) );
			return;
		}
		else if ( e == feParserException::END_OF_FILE && false == expEof )
		{
			throw feFileToolErr( _T( "unexpected eof while reading '.ftrules'" ) );
		}
		else if ( e != feParserException::END_OF_FILE )
			_tprintf( _T( "parser error: %s" ), feStr (e));
	}
}

void feFileTool::createFSStructure( file *f )
{
	if ( f == mpRoot )
		_tprintf( _T( "creating filesystem structure...\n" ) );
    WIN32_FIND_DATA fd;
	memset( &fd, 0, sizeof( fd ) );
	HANDLE hFind = ::FindFirstFile( _T( "*.*" ), &fd );
	if ( hFind != INVALID_HANDLE_VALUE )
	{
		do
		{
			feStr fname = fd.cFileName;
			fname.tolower();
			if ( !testIgnoreList( fname ) )
			{
				if ( fname != _T( ".." ) && fname != _T( "." ) && _tcslen( fname ) <= 32 )
				{
					// add file
					mNumFiles++;
					file *nf = new file;
					strcpy( nf->name, feCStr( fname ).c_str() );
					nf->parent = f;

					if ( fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY )
					{
						// recurse
						TCHAR rmbFolder[_MAX_PATH];
						_tgetcwd( rmbFolder, _MAX_PATH );
						_tchdir( fname );
						createFSStructure( nf );
						_tchdir( rmbFolder );
						nf->blocktype = feFSBlockType_Folder;
					}
					else
					{
						// find rule, default is 'c'
						feCStr rule = findRule( fname );
						if ( rule == "c" )
							nf->blocktype = feFSBlockType_BinaryCompressed;
						else if ( rule == "b" )
							nf->blocktype = feFSBlockType_BinaryRawData;
						else
						{
							feStr s;
							s.printf( _T( "invalid rule: '%s'" ), rule.c_str() );
							throw feFileToolErr( s );
						}
						// write filesize
						if ( fd.nFileSizeHigh != 0 )
						{
							feStr s;
							s.printf( _T( "file size is too big: '%s'" ), fname.c_str() );
							throw feFileToolErr( s );
						}

						nf->size = fd.nFileSizeLow;
					}
					f->children.push_back( nf );
				}
			}
		} while ( FindNextFile( hFind, &fd ) );
	}
}

bool feFileTool::testIgnoreList( const feStr &s )
{
	feCStr cs = s;
	std::vector< feRegexp * >::iterator it;
	for ( it = mIgnoreList.begin(); it != mIgnoreList.end(); it++ )
	{
		char *str = new char[cs.size() + 1];
		strcpy( str, cs.c_str() );
		int r = regexec( ( *it )->getRegexp(), str );
		delete[] str;
		if ( r )
			return true;
	}
	return false;
}

feCStr feFileTool::findRule( const feStr &s )
{
	feCStr cs = s;
	std::map< feRegexp *, feCStr >::iterator it;
	for ( it = mRulesList.begin(); it != mRulesList.end(); it++ )
	{
		char *str = new char[cs.size() + 1];
		strcpy( str, cs.c_str() );
		int r = regexec( ( *it ).first->getRegexp(), str );
		delete[] str;
		if ( r )
			return ( *it ).second;
	}

	return _T( "c" );
}

void feFileTool::dbgPrintTree( file *f, int indent )
{
	int i;
	for ( i = 0; i < indent; i++ )
		_tprintf( _T( "--" ) );
	if ( false == f->children.empty() )
		_tprintf( _T( "[" ) );
	_tprintf( feStr( f->name ).c_str() );
	if ( false == f->children.empty() )
		_tprintf( _T( "]" ) );
	_tprintf( _T( "\n" ) );
	for ( i = 0; i < (int)f->children.size(); i++ )
		dbgPrintTree( f->children[i], indent+1 );
}

void feFileTool::processFiles( FILE *fp, file *f, const feCStr &dir )
{
	if ( f == mpRoot )
		_tprintf( _T( "writing files...\n" ) );
	if ( f->blocktype != feFSBlockType_Folder )	// folder is dummy
	{
		f->offset = ftell( fp );

		uchar *buffer = new uchar[f->size];
		feCStr s;
		// maxname is 32 chars, terminating 0 is not included
		// copy string and append with 0
		char str[33];
		memset( str, 0, 33 );
		memcpy( str, f->name, 32 );
		s.printf( "%s%s", dir.c_str(), str );
		FILE *in = fopen( s.c_str(), "rb" );
		if ( !in )
		{
			_tprintf( _T( "*** warning: cannot open file '%s', filling with zeros.\n" ), feStr( s.c_str() ).c_str() );
			_tprintf( _T( "*** hint: file name may contain non-OEM characters.\n" ) );
			memset( buffer, 0, f->size );
		}
		else
		{
			memset( buffer, 0, f->size );
			fread( buffer, f->size, 1, in );
			fclose( in );
		}

		if ( f->blocktype == feFSBlockType_BinaryRawData )
		{
			fwrite( buffer, f->size, 1, fp );
			mTotalSize += f->size;
			f->comprsize = f->size;
		}
		else if ( f->blocktype == feFSBlockType_BinaryCompressed )
		{
			f->comprsize = (ulong)(f->size * 1.3);
			uchar *compr = new uchar[f->comprsize];
			compress2( compr, &f->comprsize, buffer, f->size, 9 );
			fwrite( compr, f->comprsize, 1, fp );
			mTotalSize += f->comprsize;
			delete[] compr;
		}
		else
			__asm int 3;	// break execution: something wrong

		delete[] buffer;

		double elapsedtime = (double)clock() / CLOCKS_PER_SEC - mStartTime;
		double timeperfile = elapsedtime / ( mCurrentFile ? mCurrentFile : 1 );
		double totaltime = timeperfile * mNumFiles;
		timeperfile /= 60.0;
		elapsedtime /= 60.0;
		totaltime /= 60.0;
		printf( "%d%% (%d/%d); elapsed time %.2lfm, estimated time left: %.2lfm, avg time per file: %.5lfm    \r"
			, (int)( 100.0 * ( mCurrentFile + 1 ) / mNumFiles )
			, mCurrentFile+1, mNumFiles
			, elapsedtime
			, totaltime - elapsedtime
			, timeperfile );
		mCurrentFile++;
	}
	else
	{
		if ( f != mpRoot )
			mCurrentFile++;
		for ( size_t i = 0; i < f->children.size(); i++ )
		{
			feCStr newDir = f == mpRoot ? "" : dir + feCStr(f->name) + feCStr("/");
			processFiles( fp, f->children[i], newDir );
		}
	}
}

void feFileTool::writeTable( FILE *fp, file *f )
{
	if ( f == mpRoot )
		_tprintf( _T( "writing file table...\n" ) );
	fwrite( f, 48, 1, fp );
	if ( f->blocktype == feFSBlockType_Folder )
	{
		ulong nc = ( ulong )f->children.size();
		fwrite( &nc, sizeof( nc ), 1, fp );
		for ( ulong i = 0; i < nc; i++  )
			writeTable( fp, f->children[i] );
	}
}

