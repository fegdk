// $Id$

// this tool is model viewer which supports animation too
// press space to see animation

#include <fegdk/pch.h>
#include <fegdk/f_application.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_model.h>
#include <fegdk/f_fontft.h>
#include <fegdk/f_oggvorbis.h>
#include <fegdk/f_console.h>
#include <fegdk/f_input.h>
#include <fegdk/f_viewport.h>
#include <fegdk/f_animation.h>
#include <fegdk/f_timer.h>
#include <fegdk/f_texture.h>
#include <fegdk/f_nullrenderer.h>
#include <fegdk/f_effectparms.h>
#include <fegdk/f_resourcemgr.h>
#include <fegdk/f_types.h>
#include <fegdk/f_drawutil.h>
#include <fegdk/f_modmusic.h>
#include <fegdk/f_renderablesceneobject.h>
#include <fegdk/f_renderqueue.h>

#define ENABLE_MUSIC 0

class MdlViewApp : public feApplication
{

private:

	feModel*			mpTestTriObject;
	feMatrix4			mMatrix;
	float				mAngleX;
	float				mAngleY;
	float				mDist;
	ulong				mFrameTime;
	ulong				mPrevTime;
#if ENABLE_MUSIC
	feVorbisFile*		mpMusic;
	feModMusic*			mpMod;
#endif
	feSmartPtr< feFontFT >	mpFont;
	feTexture*			mpTexture;
	feCStr	mTriObjFName;

public:

    MdlViewApp(const char *fname, feEngine *engine)
		: feApplication (engine)
	{
		mpTestTriObject = NULL;
		mTriObjFName = fname;
		mDist = 30.f;
		feVector3 f;
#if ENABLE_MUSIC
		mpMusic = NULL;
		mpMod = NULL;
#endif
	}

/*	bool		confirmDevice( D3DCAPS8 *caps, ulong behavior, D3DFORMAT f )
	{
		if ( behavior != D3DCREATE_SOFTWARE_VERTEXPROCESSING )
			return false;
		return true;
	}*/

	bool isFullscreen( void )
	{
		return false;
	}
	
	void init()
	{
		FE_EPTR_FROM_OWNER;
		mMatrix.identity();
		mpTestTriObject = (feModel *)engine->resourceMgr()->createObject ("model", mTriObjFName);
#if ENABLE_MUSIC
		mpMusic = new feVorbisFile(mpOwner, "music/transmutator.ogg" );
//		mpMusic->play( true );
		mpMod = new feModMusic (mpOwner, "starpaws.mod");
		mpMod->play (true);
#endif

		feFontFT *f = checked_cast <feFontFT *> (engine->resourceMgr ()->createObject ("font", "tahoma:14"));
		mpFont = f;
		f->release();

		mpTexture = checked_cast <feTexture *> (engine->resourceMgr ()->createObject ("texture2d", "textures_b/mtlplat2.jpg"));
	}
	
	void free()
	{
		mpTexture->release ();
#if ENABLE_MUSIC
		if ( mpMusic )
		{
			mpMusic->release();
			mpMusic = NULL;
		}
		if (mpMod)
		{
			mpMod->release ();
			mpMod = NULL;
		}
#endif

		if ( mpTestTriObject )
		{
			mpTestTriObject->release();
			mpTestTriObject = NULL;
		}
	}
	
	void update()
	{
		FE_EPTR_FROM_OWNER;
/*		if ( mpMusic )
		{
			if ( !mpMusic->isPlaying() )
			{
				mpMusic->release();
				mpMusic = NULL;
			}
		}*/
		feApplication::update();
		mpTestTriObject->update();

		if (engine->console()->isVisible() )
			return;

		mPrevTime = mFrameTime;
		mFrameTime = (int)(engine->timer()->getTime() * 1000);

		const float speed = 100;

		feKeybInputDevice*				kb = (feKeybInputDevice*)( engine->inputDevice( 0 ) );
		feMouseInputDevice*			mouse = (feMouseInputDevice*)( engine->inputDevice( 1 ) );

		if ( kb->getState ()->isPressed( Key_UpArrow ) )
			mDist += speed * ( mFrameTime - mPrevTime ) * 0.001;
		if ( mouse->getState ()->isPressed( Key_DownArrow ) )
			mDist -= speed * ( mFrameTime - mPrevTime ) * 0.001;

		mMatrix._43 = 0;

		static fePoint prevPt;
		fePoint pt = mouse->getCursorPos();

		int x = (int)pt.x - (int)prevPt.x;
		int y = (int)pt.y - (int)prevPt.y;

		prevPt = pt;

		if (mouse->getState ()->isPressed (Key_Mouse1))
		{
			// rotate
			feMatrix4 r;
			r.rotateX( -y * 0.01 );
			mMatrix = mMatrix * r;
			r.rotateY( -x * 0.01 );
			mMatrix = mMatrix * r;
		}
		else if (mouse->getState ()->isPressed (Key_Mouse2))
		{
			mDist -= y * 2;
		}

		if ( mDist < 0 )
			mDist = 0;

		mMatrix._43 = mDist;
	}

	void render() const
	{
		FE_EPTR_FROM_OWNER;
		feApplication::render();
		engine->renderer()->clear(0, NULL, feNullRenderer::clear_target | feNullRenderer::clear_z, float4(.5f, .5f, .5f, 1), 1, 0 );

		feMatrix4 worldview = mMatrix;

		feMatrix4 viewproj;
		feViewport *vp = engine->viewport ();
		viewproj.projection( 60.f*3.14f/180.f, vp->getHeight() / (float)vp->getWidth(), .1f, 1000.f );

		engine->effectParms()->setMatrix( feEffectParm_ViewMatrix, worldview );
		engine->effectParms()->setMatrix( feEffectParm_ProjMatrix, viewproj );
		worldview = worldview.inverse();
		engine->effectParms()->setVector( feEffectParm_LightPos0, feVector3( worldview._41, worldview._42, worldview._43 ) );

//		mpTexture->bind (0);
//		mpTestTriObject->render();
		engine->renderQueue ()->add (mpTestTriObject);
		engine->renderQueue ()->flush ();

//		mpTexture->bind (0);
//		engine->drawUtil ()->drawPic (0, 0, 200, 200, 0, 0, 1, 1, 0xfffffffful, false);
		// draw text
		mpFont->drawTextString( L"mdlview [$Revision$]", 0, 0, 0xffffffff );

		// draw fps
		static float fFPS      = 0.0f;
		static float fLastTime = 0.0f;
		static unsigned long dwFrames  = 0L;
		float fTime;
		fTime = engine->timer ()->getTime ();
		dwFrames++;
		if( fTime - fLastTime > 0.5f )
		{
			fFPS      = dwFrames / (fTime - fLastTime);
			fLastTime = fTime;
			dwFrames  = 0L;
		}
		feWStr s;
		s.printf( L"fps: %.2f", fFPS );
		mpFont->drawTextString( s.c_str(), 0, mpFont->getTextHeight(), 0xffffff00 );

	}

	void keyDown( int keycode )
	{
		static bool pause = false;
		feEngine *engine = checked_cast <feEngine *> (mpOwner);
		if ( keycode == Key_Escape )
			engine->quit();
		else if ( keycode == Key_Space )
		{
/*			// next sequence
			feAnimationSequencer *s = mpTestTriObject->getAnimationSequencer();
			if ( s )
			{
				int n = s->numSequences();
				int c = s->getSequence();
				c++;
				if ( c >= n )
					c = 0;
				s->setSequence( c, engine->timer()->getTime() );
			}*/
		}
		else if ( (keycode &~ Key_CharFlag) == 'p' )
		{
//			pause = !pause;
//			mpMusic->pause( pause );
		}
	}

	TCHAR*		getDataPath()
	{
		return _T( "data" );
	}
};

#ifdef _WIN32
int __stdcall _tWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
#else
int main (int argc, char *argv[])
{
	int __argc = argc;
	char **__argv = argv;
#endif
	printf ("winmain\n");
	
//	if ( __argc <= 1 )
//	{
//		::MessageBox( NULL, _T( "usage: mdlview.exe <fname.bmdl>" ), _T( "mdlview" ), MB_OK );
//		return -1;
//	}
	// init objects
	feEngine *engine = new feEngine;
	MdlViewApp *app = new MdlViewApp(__targv[1], engine);

	// run application through the engine
	engine->run (app);

	// free objects
	app->release ();
	engine->release ();

#ifdef _DEBUG
	_CrtDumpMemoryLeaks();
#endif
	return 0;
}

// eof

