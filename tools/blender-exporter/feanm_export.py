#!BPY

## $Header$

##
##  fegdk: FE Game Development Kit
##  Copyright (C) 2001-2008 Alexey "waker" Yakovenko
##
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Library General Public
##  License as published by the Free Software Foundation; either
##  version 2 of the License, or (at your option) any later version.
##
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Library General Public License for more details.
##
##  You should have received a copy of the GNU Library General Public
##  License along with this library; if not, write to the Free
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##
##  Alexey Yakovenko
##  waker@users.sourceforge.net
##

""" Registration info for Blender menus:
Name: 'FE animation (.anm)...'
Blender: 241
Group: 'Export'
Submenu: 'Export to FEGDK anm file format' export
Tip: 'Export to FEGDK anm file format'
"""

__author__ = "Alexey (waker) Yakovenko"
__url__ = ("Author's site, http://fegdk.sourceforge.net")
__version__ = "1.1"

# format history:
# 1.0 initial format
# 1.1 added "count" field to animation keyframes chunks

__bpydoc__ = """\
	Animation exporter for FEGDK
	http://fegdk.sf.net
"""


# $Id$

import Blender
from Blender import Types, Object, NMesh, Material, Armature, Mathutils
from Blender.Mathutils import *

def event(evt, val):
	if evt == Blender.Draw.ESCKEY and not val: Blender.Draw.Exit()

def bevent(evt):
	
	if evt == 1: Blender.Draw.Exit()
	
		

#***********************************************
#***********************************************
#                EXPORTER
#***********************************************
#***********************************************

class feExport:

	def __init__ (self):
		self.indent = 0


	#------------------------------------------------
	# we don't do it like we did w/ 3dsmax plugin, just write on the fly

	def writeString (self, s):
		for i in range (0, self.indent):
			self.file.write ("\t")
		self.file.write ("%s\n" % s)

	def writeMatrix (self, m):
		self.writeString ("\t\t%s %s %s" % (m[0][0], m[0][1], m[0][2]))
		self.writeString ("\t\t%s %s %s" % (m[1][0], m[1][1], m[1][2]))
		self.writeString ("\t\t%s %s %s" % (m[2][0], m[2][1], m[2][2]))
		self.writeString ("\t\t%s %s %s" % (m[3][0], m[3][1], m[3][2]))

	def countIpoFrames (self, curve):
		cnt = 0;
		triples = curve.getPoints ()
		for c in triples:
			if (c.vec[1][0] >= self.anim_start) and (c.vec[1][0] <= self.anim_end):
				h1, p, h2 = c.vec
				name = "unknown"
				mult = 1.0
				if curve.getName () == "LocX": name = "x"
				if curve.getName () == "LocY": name = "y"
				if curve.getName () == "LocZ": name = "z"
				if curve.getName () == "SizeX": name = "sx"
				if curve.getName () == "SizeY": name = "sy"
				if curve.getName () == "SizeZ": name = "sz"
	
				# write out scale and position as ipo curves;
				if name != "unknown":
					cnt = cnt+1
		return cnt
		
	def exportIpoCurve (self, curve):
		triples = curve.getPoints ()
		for c in triples:
			if (c.vec[1][0] >= self.anim_start) and (c.vec[1][0] <= self.anim_end):
				h1, p, h2 = c.vec
				name = "unknown"
				mult = 1.0
				if curve.getName () == "LocX": name = "x"
				if curve.getName () == "LocY": name = "y"
				if curve.getName () == "LocZ": name = "z"
				if curve.getName () == "SizeX": name = "sx"
				if curve.getName () == "SizeY": name = "sy"
				if curve.getName () == "SizeZ": name = "sz"
	
				# write out scale and position as ipo curves;
				if name != "unknown":
					self.writeString ("\t%s { %f %f %f %f %f %f }" % (name, (h1[0]-1)/25.0, h1[1]*mult, (p[0]-1)/25.0, p[1]*mult, (h2[0]-1)/25.0, h2[1]*mult))


	def collectRotationKeyFrames (self, curve, rotFrames):
		triples = curve.getPoints ()
		for c in triples:
			t = int (c.vec[1][0])
			if (t not in rotFrames) and (t >= self.anim_start) and (t <= self.anim_end):
				rotFrames.append (t)
			
	def writeRotationQuats (self, obj, rotFrames):
		if len (rotFrames):
			for t in rotFrames:
				if t > 0:
					Blender.Set('curframe',t)
					mtx = obj.getMatrix ('worldspace')
					par = obj.getParent ()
					if par:
						mtx = mtx * par.getInverseMatrix ()
					quat = mtx.toQuat ()
					self.writeString ("\trot { %f %f %f %f %f }" % ((t-1) / float (self.anim_fps), quat.w, quat.x, quat.y, quat.z))

	def exportAnimation (self, filename):
		curframe = Blender.Get('curframe')
		self.file = open (filename, "w")
		self.writeString ("# FEANM%s" % __version__);
		self.writeString ("animation \"%s\" {" % self.sceneName)

		self.anim_start = Blender.Get ('staframe')
		self.anim_end = Blender.Get ('endframe')
		
		scn = Blender.Scene.GetCurrent ()
		rc = scn.getRenderingContext ()
		self.anim_fps = rc.framesPerSec ()

		# calc number of animated bones
		numbones = 0
		objs = Object.Get ()
		for ob in objs:
			ipo = ob.getIpo ()
			if (ipo):
				numbones = numbones + 1

		self.writeString ("\tduration %f\n" % ((self.anim_end - self.anim_start - 1) / float (self.anim_fps)))
		self.writeString ("\tnumbones %d\n" % (numbones))
		self.indent = self.indent + 1
		objs = Object.Get ()
		for ob in objs:
			ipo = ob.getIpo ()
			if (ipo):
				self.writeString ("ipo_quat_keyframes \"%s\" {" % ob.name)
				rotFrames = []
				for curve in ipo.getCurves ():
					if curve:
						self.exportIpoCurve (curve)
						nm = curve.getName ()
						if nm == "RotX" or nm == "RotY" or nm == "RotZ":
							self.collectRotationKeyFrames (curve, rotFrames)
				rotFrames.sort ()
				self.writeString ("count %d" % len(rotFrames))
				self.writeRotationQuats (ob, rotFrames)
				self.writeString ("}")
		actions = Armature.NLA.GetActions ()

		for a in actions.values ():
			for bone, ipo in a.getAllChannelIpos().iteritems ():
				self.writeString ("ipo_quat_keyframes \"%s\" {" % bone)
				cnt = 0
				for curve in ipo.getCurves ():
					if curve:
						cnt += self.countIpoFrames (curve)
				self.writeString ("count %d {" % cnt)
				for curve in ipo.getCurves ():
					if curve:
						self.exportIpoCurve (curve)
				self.writeString ("}")
		self.indent = self.indent - 1
		self.writeString ("}")
		self.file.close ()
		Blender.Set('curframe',curframe)


#***********************************************
# MAIN
#***********************************************

def my_callback(filename):
	fname=filename
	if fname.find('.anm', -4) > 0: fname = fname[:-4] 
	fname_anm = fname+'.anm'

	exporter = feExport ()
	exporter.sceneName = fname
	exporter.exportAnimation(fname_anm)
	print ("done exporting")

fname = Blender.sys.makename(ext = ".anm")
Blender.Window.FileSelector(my_callback, "Export FE .anm", fname)	

