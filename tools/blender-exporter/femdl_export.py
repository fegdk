#!BPY

## $Header$

##
##  fegdk: FE Game Development Kit
##  Copyright (C) 2001-2008 Alexey "waker" Yakovenko
##
##  This library is free software; you can redistribute it and/or
##  modify it under the terms of the GNU Library General Public
##  License as published by the Free Software Foundation; either
##  version 2 of the License, or (at your option) any later version.
##
##  This library is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
##  Library General Public License for more details.
##
##  You should have received a copy of the GNU Library General Public
##  License along with this library; if not, write to the Free
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##
##  Alexey Yakovenko
##  waker@users.sourceforge.net
##

# all vertex/index data seems very unoptimized
# using of vcache optimizer and dup removal tools (like fgp) is assumed

""" Registration info for Blender menus:
Name: 'FE model (.mdl)...'
Blender: 241
Group: 'Export'
Submenu: 'Export to FEGDK mdl file format' export
Tip: 'Export to FEGDK mdl file format'
"""

__author__ = "Alexey (waker) Yakovenko"
__url__ = ("blender", "elysiun", "Author's site, http://fegdk.sourceforge.net")
__version__ = "1.0"

__bpydoc__ = """\
"""


# $Id$

import Blender
from Blender import Types, Object, NMesh, Material, Armature, Mathutils
from Blender.Mathutils import *

def event(evt, val):
	if evt == Blender.Draw.ESCKEY and not val: Blender.Draw.Exit()

def bevent(evt):
	
	if evt == 1: Blender.Draw.Exit()
	
		

#***********************************************
#***********************************************
#                EXPORTER
#***********************************************
#***********************************************

class feExport:

	def __init__ (self):
		self.indent = 0


	#------------------------------------------------
	# we don't do it like we did w/ 3dsmax plugin, just write on the fly

	def writeString (self, s):
		for i in range (0, self.indent):
			self.file.write ("\t")
		self.file.write ("%s\n" % s)

	def writeMatrix (self, m):
		self.writeString ("\t\t%s %s %s" % (m[0][0], m[0][1], m[0][2]))
		self.writeString ("\t\t%s %s %s" % (m[1][0], m[1][1], m[1][2]))
		self.writeString ("\t\t%s %s %s" % (m[2][0], m[2][1], m[2][2]))
		self.writeString ("\t\t%s %s %s" % (m[3][0], m[3][1], m[3][2]))

	def exportObject (self, name):
		obj = name.getData ()
		typename="dummy"

		if type(obj) == Types.NMeshType :
			typename="triobject"
		
		self.writeString ("%s \"%s\" {" % (typename, name.getName ()))
		self.writeString ("\tnodeTransform {")

		mtx = name.getMatrix ('worldspace')
		par = name.getParent ()
		if par:
			mtx = mtx * par.getInverseMatrix ()
		self.writeMatrix (mtx)
		self.writeString ("\t}")

		if type(obj) == Types.NMeshType :
			# verts
			self.writeString ("\tverts {")
			self.writeString ("\t\tcount %d" % (len (obj.verts)))
			for v in obj.verts :
				self.writeString ("\t\tv %f %f %f" % (v[0], v[1], v[2]))
			self.writeString ("\t}")

			# calc numfaces
			numfaces = len (obj.faces)
			for f in obj.faces :
				if (len (f.v) == 4) :
					numfaces += 1

			# faces
			self.writeString ("\tfaces {")
			self.writeString ("\t\tcount %d" % (numfaces))
			for f in obj.faces:
				self.writeString ("\t\tf %d %d %d" % (f.v[0].index, f.v[1].index, f.v[2].index))
				if len (f) == 4:
					self.writeString ("\t\tf %d %d %d" % (f.v[2].index, f.v[3].index, f.v[0].index))
			self.writeString ("\t}")

			# normals
			self.writeString ("\tnormals {")
			self.writeString ("\t\tcount %d" % (numfaces*3))
			for f in obj.faces:
				if f.smooth:
					v = obj.verts[f.v[0].index]
					self.writeString ("\t\tv %f %f %f" % (v.no.x, v.no.y, v.no.z))
					v = obj.verts[f.v[1].index]
					self.writeString ("\t\tv %f %f %f" % (v.no.x, v.no.y, v.no.z))
					v = obj.verts[f.v[2].index]
					self.writeString ("\t\tv %f %f %f" % (v.no.x, v.no.y, v.no.z))
				else:
					self.writeString ("\t\tv %f %f %f" % (f.no[0], f.no[1], f.no[2]))
					self.writeString ("\t\tv %f %f %f" % (f.no[0], f.no[1], f.no[2]))
					self.writeString ("\t\tv %f %f %f" % (f.no[0], f.no[1], f.no[2]))
				if len(f) == 4:
					if f.smooth:
						v = obj.verts[f.v[2].index]
						self.writeString ("\t\tv %f %f %f" % (v.no.x, v.no.y, v.no.z))
						v = obj.verts[f.v[3].index]
						self.writeString ("\t\tv %f %f %f" % (v.no.x, v.no.y, v.no.z))
						v = obj.verts[f.v[0].index]
						self.writeString ("\t\tv %f %f %f" % (v.no.x, v.no.y, v.no.z))
					else:
						self.writeString ("\t\tv %f %f %f" % (f.no[0], f.no[1], f.no[2]))
						self.writeString ("\t\tv %f %f %f" % (f.no[0], f.no[1], f.no[2]))
						self.writeString ("\t\tv %f %f %f" % (f.no[0], f.no[1], f.no[2]))
					
			self.writeString ("\t}")
			
			# normalfaces
			self.writeString ("\tnormalFaces {")
			self.writeString ("\t\tcount %d" % (numfaces))
			idx = 0
			for i in range (0, numfaces):
				self.writeString ("\t\tf %d %d %d" % (idx, idx+1, idx+2))
				idx = idx + 3
			self.writeString ("\t}")

			# uvVerts
			self.writeString ("\tuvVerts {")
			self.writeString ("\t\tcount %d" % (numfaces * 3))
			for f in obj.faces :
				if f.uv:
					self.writeString ("\t\tv %f %f" % (f.uv[0][0], 1-f.uv[0][1]))
					self.writeString ("\t\tv %f %f" % (f.uv[1][0], 1-f.uv[1][1]))
					self.writeString ("\t\tv %f %f" % (f.uv[2][0], 1-f.uv[2][1]))
					if (len (f) == 4):
						self.writeString ("\t\tv %f %f" % (f.uv[2][0], 1-f.uv[2][1]))
						self.writeString ("\t\tv %f %f" % (f.uv[3][0], 1-f.uv[3][1]))
						self.writeString ("\t\tv %f %f" % (f.uv[0][0], 1-f.uv[0][1]))
				else:
					self.writeString ("\t\tv 0 0")
					self.writeString ("\t\tv 0 0")
					self.writeString ("\t\tv 0 0")
					if (len (f) == 4):
						self.writeString ("\t\tv 0 0")
						self.writeString ("\t\tv 0 0")
						self.writeString ("\t\tv 0 0")
			self.writeString ("\t}")
			
			# uvfaces
			self.writeString ("\tuvFaces {")
			self.writeString ("\t\tcount %d" % (numfaces))
			counter = 0
			for f in obj.faces :
				self.writeString ("\t\tf %d %d %d" % (counter, counter+1, counter+2))
				counter += 3
				if (len (f) == 4) :
					self.writeString ("\t\tf %d %d %d" % (counter, counter+1, counter+2))
					counter += 3
					
			self.writeString ("\t}")

			# mtlIndex
			self.writeString ("\tmtlIndex {")
			self.writeString ("\t\tcount %d" % (numfaces))
			for face in obj.faces:
				if (face.image):
					idx = self.texList.index (face.image.name)
				else:
					idx = self.texList.index ("untextured")
				self.writeString ("\t\tf %d" % idx)
				if (len (face.v) == 4) :
					self.writeString ("\t\tf %d" % idx)
			self.writeString ("\t}")

			# skinmesh weights
			if self.bonelist:
				self.writeString ("\tvertexInfluences {")
				self.writeString ("\t\tcount %d" % len (obj.verts))
				for i in range (0, len(obj.verts)):
					# indent
					for ind in range (0, self.indent):
						self.file.write ("\t")
					infl_list = obj.getVertexInfluences (i)
					self.file.write ("\t\tw %d %d" % (i, len(infl_list)))
					for infl in infl_list:
						bone_idx = self.bonelist.index (infl[0])
						self.file.write (" %d %f" % (bone_idx, infl[1]))
					self.file.write ("\n")
				self.writeString ("\t}")


		elif type (obj) == Types.ArmatureType:
			self.exportArmature (obj)

		self.indent = self.indent+1
			
		children = Blender.Object.Get ()
		for o in children :
			if o.getParent () == name:
				self.exportObject (o)

		self.indent = self.indent-1

		self.writeString ("}")
			

	def exportBone (self, bone):
		self.writeString ("dummy \"%s\" {" % bone.name)
		self.writeString ("\tnodeTransform {")
		matrix = bone.matrix['ARMATURESPACE']
		self.writeMatrix (matrix)
		self.writeString ("\t}")
		self.indent = self.indent+1
		if bone.children:
			for b in bone.children:
				self.exportBone (b)
		self.indent = self.indent-1
		self.writeString ("}")
		
	def exportArmature (self, armature):
		self.indent = self.indent+1
		# export root bones
		for bone in armature.bones.values ():
			if not bone.hasParent ():
				self.exportBone (bone)
		self.indent = self.indent-1
		
	def exportScene (self, filename):
		self.file = open (filename, "w")

		curframe = Blender.Get('curframe')
		Blender.Set('curframe',1)
		objects = Blender.Object.Get ()
		self.texList = []
		for name in objects :
			obj = name.getData ()
			if type(obj) == Types.NMeshType :
				self.collectSceneTextures (obj, self.texList)
		numMtls = 0
		self.writeString ("materials {")
		for m in self.texList :
			self.writeString ("\t\"%s\"" % (m))
			numMtls = numMtls+1
		self.writeString ("}\n")

		# bone list
		self.bonelist = []
		armatures = Blender.Armature.Get ()
		for a in armatures.values ():
			for b in a.bones.values ():
				self.bonelist.append (b.name)
		
		if self.bonelist:
			self.writeString ("bones {")
			for m in self.bonelist :
				self.writeString ("\t\"%s\"" % (m))
			self.writeString ("}\n")
		
		self.writeString ("dummy sceneroot {")
		self.writeString ("\tnodeTransform {")
		self.writeString ("\t\t-1 0 0")
		self.writeString ("\t\t0 1 0")
		self.writeString ("\t\t0 0 1")
		self.writeString ("\t\t0 0 0")
		self.writeString ("\t}")

		# export all scene objects
		for obj in objects :
			parent = obj.getParent ()
			self.indent = self.indent+1
			if not parent:
				self.exportObject (obj)
			self.indent = self.indent-1

		self.writeString ("}")
		
		Blender.Set('curframe', curframe)
		self.file.close ()

	def collectSceneTextures (self, mesh, lst):
		for face in mesh.faces:
			if face.image and (face.image.name not in lst):
				lst.append (face.image.name)
			elif not face.image and ("untextured" not in lst):
				lst.append ("untextured")

#***********************************************
# MAIN
#***********************************************

def my_callback(filename):
	fname=filename
	if fname.find('.mdl', -4) > 0: fname = fname[:-4] 
	fname_mdl = fname+'.mdl'

	exporter = feExport ()
	exporter.sceneName = fname
	exporter.exportScene(fname_mdl)
	print ("done exporting")

# exporter logic:
#	be as simple as possible
#   export material names
#   for each node export transforms, verts, faces, uvVerts, uvFaces, normalVerts, normalFaces, face material indices, weights
#	export animation for entire scene
#   DON'T do any dupe detection
#   PRESERVE proper hierarchy




#arg = __script__['arg']
#if arg == 'help':
#else:
fname = Blender.sys.makename(ext = ".mdl")
Blender.Window.FileSelector(my_callback, "Export FE .mdl", fname)	

