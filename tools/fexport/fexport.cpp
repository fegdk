#include "pch.h"
#include "fexport.h"
#include "resource.h"
#include <algorithm>

// $Log$
// Revision 1.1  2005/01/18 19:11:00  waker
// Initial revision
//
// Revision 1.1.1.1  2005/01/03 15:21:07  waker
// import
//
// Revision 1.15  2004/11/10 01:22:10  waker
// added lightmap support
//
// Revision 1.14  2003/12/19 20:09:18  waker
// added progress bar (really useless on hi-poly models not split up into separate objects)
//
// Revision 1.13  2003/06/09 13:10:43  waker
// exports to right fmt (additional parenthesis w/ mtl name)
//
// Revision 1.12  2002/12/02 11:05:26  waker
// added parent check in animation controller export
//
// Revision 1.11  2002/12/02 10:57:47  waker
// INode::GetObjTMAfterWSM is used instead of INode::GetNodeTM
//
// Revision 1.10  2002/11/23 16:07:54  waker
// going home, saving changes
//

#define FEXPORT_CLASS_ID	Class_ID(0x770130b9, 0x5a8d06cc)

class FExport : public SceneExport {
public:
	static HWND hParams;
	static BOOL CALLBACK ParmsDlgProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );

	//Constructor/Destructor
	FExport( void ) {}
	~FExport( void ) {}
	BOOL SupportsOptions( int ext, DWORD options ) { return ( options & SCENE_EXPORT_SELECTED ) ? TRUE : FALSE; }
	int ExtCount( void ) { return 1; }
	const TCHAR *Ext( int i ) { return _T( "mdl" ); }
	const TCHAR *LongDesc( void ) { return _T( "fe model and animation exporter" ); }
	const TCHAR *ShortDesc( void ) { return _T( "fe model" ); }
	const TCHAR *AuthorName( void ) { return _T( "[_WaKeR_]" ); }
	const TCHAR *CopyrightMessage( void ) { return _T( "" ); }
	const TCHAR *OtherMessage1( void ) { return _T( "" ); }
	const TCHAR *OtherMessage2( void ) { return _T( "" ); }
	unsigned int Version( void ) { return 1; }
	void ShowAbout( HWND hWnd ) {}

	// exported stuff
	struct ExpMtl
	{
		TSTR name;
		TSTR textureName;
		int index;
	};
	typedef std::map< Mtl *, int > MtlMap;
	MtlMap mtlIndex;
	std::vector< ExpMtl > mtlList;

	typedef std::vector< INode * >	boneList;

	int numberOfNodes;

	enum nodeType { none, tri, light, dummy };

	struct ExpNode
	{
		ExpNode()
		{
			node = NULL;
			export = true;
			type = none;
		}
		~ExpNode()
		{
			node = NULL;
			for ( size_t i = 0; i < children.size(); i++ )
				delete children[i];
		}

		struct entity_t
		{
			std::map< std::string, std::string >	props;
		};

		nodeType		type;
		INode*			node;
		bool			export;
		entity_t		lightentity;

		struct Face
		{
			int v[3];
		};

		std::vector< Point3 >		verts;				// indexed by faces
		std::vector< Point3 >		colors;				// indexed by colorFaces
		std::vector< boneList >		boneAssignments;	// indexed by faces if present
		std::vector< Point3 >		normals;			// indexed by normalFaces
		std::vector< Point2 >		uvVerts;			// indexed by uvFaces
		std::vector< Point2 >		uvLightmap;			// indexed by uvLightmapFaces if present

		std::vector< Face >			faces;
		std::vector< Face >			colorFaces;
		std::vector< Face >			normalFaces;
		std::vector< Face >			uvFaces;
		std::vector< Face >			uvLightmapFaces;
		
		std::vector< int >			mtlIndex;

		std::vector< ExpNode * >	children;

		float						animSpeed;			// frames per second (or matrices per second, or whatever..)
		std::vector< Matrix3 >		animFrames;
	};

	ExpNode*		rootExpNode;
	Interface*		pInterface;

	int DoExport( const TCHAR *name, ExpInterface *ei, Interface *i, BOOL suppressPrompts, DWORD options )
	{
		TSTR fname = name;
		pInterface = i;
		// tolower
		fname.toLower();
		// get params
//		BOOL res = DialogBox( hInstance, MAKEINTRESOURCE( IDD_PARMS ), i->GetMAXHWnd(), ParmsDlgProc );
//		if ( res )
			ExportScene( i, fname, options );
		return 1;
	}

	// dummy progress func
	static DWORD WINAPI fn(LPVOID arg)	{		return 0;	}
	void ExportScene( Interface *ip, const TSTR &fname, DWORD options )
	{
		TCHAR dp[_MAX_PATH];
		TCHAR df[_MAX_FNAME];
		TCHAR de[_MAX_EXT];
		BMMSplitFilename( fname.data(), dp, df, de );

		numberOfNodes = 0;
		ExportMaterials( ip->GetRootNode(), df, options, numberOfNodes );
		rootExpNode = NULL;

		// Startup the progress bar.
		ip->ProgressStart( _T( "Exporting FE model file..." ), TRUE, fn, NULL );

		if ( ExportGeometry( rootExpNode, ip->GetRootNode(), df, options, 0 ) )
		{
			Save( fname );
		}
		if ( rootExpNode )
		{
			delete rootExpNode;
			rootExpNode = NULL;
		}
	
		ip->ProgressEnd();
	}

	void Save( const TSTR &fname )
	{
		FILE *fp;
		fp = _tfopen( fname.data(), _T( "w+t" ) );
		assert( fp );
		if ( !fp )
			return;

		SaveMaterials( fp );
        SaveNode( fp, rootExpNode );

		fclose( fp );
	}

	void SaveMaterials( FILE *fp )
	{
		if ( mtlList.size() )
		{
			for ( int i = 0; i < (int)mtlList.size(); i++ )
			{
				FILE *fp = _tfopen( ( mtlList[i].name + _T( ".fmtl" ) ).data(), _T( "w+t" ) );
				assert( fp );
				if ( !fp )
					return;

				_ftprintf( fp, _T( "\"%s\" {\n" ), mtlList[i].name );
				_ftprintf( fp, _T( "\teffectfile effects/simple.fx\n" ) );
				if ( mtlList[i].textureName.Length() )
					_ftprintf( fp, _T( "\ttexinput basetexture \"%s\"\n" ), mtlList[i].textureName.data() );
				_ftprintf( fp, _T( "}\n" ) );

				fclose( fp );
			}

			// write materials to textfile
			_ftprintf( fp, _T( "materials {\n" ) );
			for ( int i = 0; i < (int)mtlList.size(); i++ )
			{
				_ftprintf( fp, _T( "\t\"materials/%s\"\n" ), mtlList[i].name );
			}
			_ftprintf( fp, _T( "}\n\n" ) );
		}
	}

	void SaveNode( FILE *fp, ExpNode *node, const TSTR &indent = _T( "" ) )
	{
		if ( !node->export )
		{
			assert( node->children.empty() );
			return;	// shouldn't have children
		}

		TSTR ind = indent + _T( "\t" );
		int i;

		if ( node->type == tri )
			_ftprintf( fp, _T( "%striobject \"%s\" {\n" ), indent.data(), node->node->GetName() );
		else if ( node->type == light )
			_ftprintf( fp, _T( "%sentity \"%s\" {\n" ), indent.data(), node->node->GetName() );
		else if ( node->type == dummy )
			_ftprintf( fp, _T( "%sdummy \"%s\" {\n" ), indent.data(), node->node->GetName() );
		else
		{
			TCHAR str[256];
			_stprintf( str, _T( "node '%s' has no correct type assignment. will be written as dummy." ), node->node->GetName() );
			warning( str );
			_ftprintf( fp, _T( "%sdummy \"%s\" {\n" ), indent.data(), node->node->GetName() );
		}

		// matrix
		float m[4][4];
		Matrix3 nodeTM = node->node->GetObjTMAfterWSM( 0 );
		if ( node->node->GetParentNode() )
			nodeTM = nodeTM * Inverse( node->node->GetParentNode()->GetObjTMAfterWSM( 0 ) );
		Point3 x = nodeTM.GetRow( 0 );
		Point3 y = nodeTM.GetRow( 1 );
		Point3 z = nodeTM.GetRow( 2 );
		m[0][0] = x.x; m[0][1] = x.z; m[0][2] = x.y; m[0][3] = 0;
		m[1][0] = z.x; m[1][1] = z.z; m[1][2] = z.y; m[1][3] = 0;
		m[2][0] = y.x; m[2][1] = y.z; m[2][2] = y.y; m[2][3] = 0;
		m[3][0] = nodeTM.GetTrans().x; m[3][1] = nodeTM.GetTrans().z; m[3][2] = nodeTM.GetTrans().y; m[3][3] = 1;

		_ftprintf( fp, _T( "%snodeTransform {\n" ), ind.data() );
		_ftprintf( fp, _T( "%s\t%f %f %f\n" ), ind.data(), m[0][0], m[0][1], m[0][2] );
		_ftprintf( fp, _T( "%s\t%f %f %f\n" ), ind.data(), m[1][0], m[1][1], m[1][2] );
		_ftprintf( fp, _T( "%s\t%f %f %f\n" ), ind.data(), m[2][0], m[2][1], m[2][2] );
		_ftprintf( fp, _T( "%s\t%f %f %f\n" ), ind.data(), m[3][0], m[3][1], m[3][2] );
		_ftprintf( fp, _T( "%s}\n" ), ind.data() );

		if ( node->type == tri && node->verts.size() )
		{
			// verts
			_ftprintf( fp, _T( "%sverts {\n" ), ind.data() );
			_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->verts.size() );
			for ( i = 0; i < (int)node->verts.size(); i++ )
			{
				_ftprintf( fp, _T( "%s\tv %f %f %f\n" ), ind.data(), node->verts[i].x, node->verts[i].y, node->verts[i].z );
			}
			_ftprintf( fp, _T( "%s}\n" ), ind.data() );

			// faces
			_ftprintf( fp, _T( "%sfaces {\n" ), ind.data() );
			_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->faces.size() );
			for ( i = 0; i < (int)node->faces.size(); i++ )
			{
				_ftprintf( fp, _T( "%s\tf %d %d %d\n" ), ind.data(), node->faces[i].v[0], node->faces[i].v[1], node->faces[i].v[2] );
			}
			_ftprintf( fp, _T( "%s}\n" ), ind.data() );

			// colors
			_ftprintf( fp, _T( "%scolors {\n" ), ind.data() );
			_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->colors.size() );
			for ( i = 0; i < (int)node->colors.size(); i++ )
			{
				_ftprintf( fp, _T( "%s\tv %f %f %f\n" ), ind.data(), node->colors[i].x, node->colors[i].y, node->colors[i].z );
			}
			_ftprintf( fp, _T( "%s}\n" ), ind.data() );

			// colorfaces
			_ftprintf( fp, _T( "%scolorFaces {\n" ), ind.data() );
			_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->colorFaces.size() );
			for ( i = 0; i < (int)node->colorFaces.size(); i++ )
			{
				_ftprintf( fp, _T( "%s\tf %d %d %d\n" ), ind.data(), node->colorFaces[i].v[0], node->colorFaces[i].v[1], node->colorFaces[i].v[2] );
			}
			_ftprintf( fp, _T( "%s}\n" ), ind.data() );

			// normals
			_ftprintf( fp, _T( "%snormals {\n" ), ind.data() );
			_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->normals.size() );
			for ( i = 0; i < (int)node->normals.size(); i++ )
			{
				_ftprintf( fp, _T( "%s\tv %f %f %f\n" ), ind.data(), node->normals[i].x, node->normals[i].y, node->normals[i].z );
			}
			_ftprintf( fp, _T( "%s}\n" ), ind.data() );

			// normalFaces
			_ftprintf( fp, _T( "%snormalFaces {\n" ), ind.data() );
			_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->normalFaces.size() );
			for ( i = 0; i < (int)node->normalFaces.size(); i++ )
			{
				_ftprintf( fp, _T( "%s\tf %d %d %d\n" ), ind.data(), node->normalFaces[i].v[0], node->normalFaces[i].v[1], node->normalFaces[i].v[2] );
			}
			_ftprintf( fp, _T( "%s}\n" ), ind.data() );

			if ( node->uvVerts.size() )
			{
				// uvVerts
				_ftprintf( fp, _T( "%suvVerts {\n" ), ind.data() );
				_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->uvVerts.size() );
				for ( i = 0; i < (int)node->uvVerts.size(); i++ )
				{
					_ftprintf( fp, _T( "%s\tv %f %f\n" ), ind.data(), node->uvVerts[i].x, node->uvVerts[i].y );
				}
				_ftprintf( fp, _T( "%s}\n" ), ind.data() );

				// uvFaces
				_ftprintf( fp, _T( "%suvFaces {\n" ), ind.data() );
				_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->uvFaces.size() );
				for ( i = 0; i < (int)node->uvFaces.size(); i++ )
				{
					_ftprintf( fp, _T( "%s\tf %d %d %d\n" ), ind.data(), node->uvFaces[i].v[0], node->uvFaces[i].v[1], node->uvFaces[i].v[2] );
				}
				_ftprintf( fp, _T( "%s}\n" ), ind.data() );
			}

			if ( node->uvLightmap.size() )
			{
				// uvLightmap
				_ftprintf( fp, _T( "%suvLightmap {\n" ), ind.data() );
				_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->uvLightmap.size() );
				for ( i = 0; i < (int)node->uvLightmap.size(); i++ )
				{
					_ftprintf( fp, _T( "%s\tv %f %f\n" ), ind.data(), node->uvLightmap[i].x, node->uvLightmap[i].y );
				}
				_ftprintf( fp, _T( "%s}\n" ), ind.data() );

				// uvLightmapFaces
				_ftprintf( fp, _T( "%suvLightmapFaces {\n" ), ind.data() );
				_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->uvLightmapFaces.size() );
				for ( i = 0; i < (int)node->uvLightmapFaces.size(); i++ )
				{
					_ftprintf( fp, _T( "%s\tf %d %d %d\n" ), ind.data(), node->uvLightmapFaces[i].v[0], node->uvLightmapFaces[i].v[1], node->uvLightmapFaces[i].v[2] );
				}
				_ftprintf( fp, _T( "%s}\n" ), ind.data() );
			}

			if ( node->mtlIndex.size() )
			{
				// mtl assignments
				_ftprintf( fp, _T( "%smtlIndex {\n" ), ind.data() );
				_ftprintf( fp, _T( "%s\tcount %d\n" ), ind.data(), (int)node->mtlIndex.size() );
				for ( i = 0; i < (int)node->mtlIndex.size(); i++ )
				{
					_ftprintf( fp, _T( "%s\tf %d\n" ), ind.data(), node->mtlIndex[i] );
				}
				_ftprintf( fp, _T( "%s}\n" ), ind.data() );
			}

			if ( false == node->animFrames.empty() )
			{
				// write animation
				_ftprintf( fp, _T( "%sanimation {\n" ), ind.data() );
				_ftprintf( fp, _T( "%s\tframeRate %f\n" ), ind.data(), node->animSpeed );
				_ftprintf( fp, _T( "%s\tnumFrames %d\n" ), ind.data(), (int)node->animFrames.size() );
				for ( i = 0; i < (int)node->animFrames.size(); i++ )
				{
					// frame time
					_ftprintf( fp, _T( "%s\ttime %f transform " ), ind.data(), i * TicksToSec( GetTicksPerFrame() ) );
					float m[4][4];
					Matrix3 &nodeTM = node->animFrames[i];
					Point3 x = nodeTM.GetRow( 0 );
					Point3 y = nodeTM.GetRow( 1 );
					Point3 z = nodeTM.GetRow( 2 );
					m[0][0] = x.x; m[0][1] = x.z; m[0][2] = x.y; m[0][3] = 0;
					m[1][0] = z.x; m[1][1] = z.z; m[1][2] = z.y; m[1][3] = 0;
					m[2][0] = y.x; m[2][1] = y.z; m[2][2] = y.y; m[2][3] = 0;
					m[3][0] = nodeTM.GetTrans().x; m[3][1] = nodeTM.GetTrans().z; m[3][2] = nodeTM.GetTrans().y; m[3][3] = 1;
					_ftprintf( fp, _T( "%f %f %f " ), m[0][0], m[0][1], m[0][2] );
					_ftprintf( fp, _T( "%f %f %f " ), m[1][0], m[1][1], m[1][2] );
					_ftprintf( fp, _T( "%f %f %f " ), m[2][0], m[2][1], m[2][2] );
					_ftprintf( fp, _T( "%f %f %f\n" ), m[3][0], m[3][1], m[3][2] );
				}
				_ftprintf( fp, _T( "%s}\n" ), ind.data() );
			}
		}
		else if ( node->type == light )
		{
			std::map< std::string, std::string >::iterator it;
			for ( it = node->lightentity.props.begin(); it != node->lightentity.props.end(); it++ )
			{
				_ftprintf( fp, _T( "%sprop \"%s\" \"%s\"\n" ), ind.data(), (*it).first.c_str(), (*it).second.c_str() );
			}
		}

		// children
		for ( i = 0; i < (int)node->children.size(); i++ )
		{
			SaveNode( fp, node->children[i], ind );
		}

		_ftprintf( fp, _T( "%s}\n" ), indent.data() );
	}

	void ExportMaterials( INode *node, const TSTR &parentName, DWORD options, int &numNodes )
	{
		numNodes++;
		if ( !( options & SCENE_EXPORT_SELECTED ) || ( node->Selected() ) )
		{
			if ( node->GetMtl() )
				ExportMaterial( node->GetMtl(), parentName );
		}
		for ( int i = 0; i < node->NumberOfChildren(); i++ )
			ExportMaterials( node->GetChildNode( i ), parentName, options, numNodes );
	}

	void ExportMaterial( Mtl *mtl, const TSTR &parentName )
	{
		if ( mtl->ClassID() == Class_ID( MULTI_CLASS_ID, 0 ) )
		{
			TSTR name = parentName + _T( "_" ) + mtl->GetName();
			for ( int i = 0; i < mtl->NumSubMtls(); i++ )
				ExportMaterial( mtl->GetSubMtl( i ), name );
		}
		else if ( mtl->ClassID() == Class_ID( DMTL_CLASS_ID, 0 ) )
		{
			ExportStdMat( mtl, parentName );
		}
	}

	void ExportStdMat( Mtl *mtl, const TSTR &parentName )
	{
		MtlMap::iterator it = mtlIndex.find( mtl );
		if ( it != mtlIndex.end() )
			return;

		StdMat *stdMtl = ( StdMat * )mtl;
		ExpMtl m;

		m.name = parentName + _T( "_" ) + mtl->GetName();

		Texmap *tmap = stdMtl->GetSubTexmap( ID_DI );

//		if ( !tmap )
//			return; // do not export texture-less materials

		if ( !tmap || tmap->ClassID() != Class_ID( BMTEX_CLASS_ID, 0 ) )
			tmap = NULL;
//			return; // unsupported texmap class

		TSTR texname;
		if ( tmap )
		{
			BitmapTex *bmt = ( BitmapTex * ) tmap;
			TextureOutput *tout = bmt->GetTexout();
			texname = bmt->GetMapName();
			TCHAR dp[_MAX_PATH];
			TCHAR df[_MAX_FNAME];
			TCHAR de[_MAX_EXT];
			BMMSplitFilename( texname, dp, df, de );
			m.textureName = TSTR( df ) + TSTR( de );
		}

		mtlIndex[mtl] = (int)mtlList.size();
		mtlList.push_back( m );
	}

	bool ExportGeometry( ExpNode *parentNode, INode *node, const TSTR &parentName, DWORD options, int numNodes, bool parentDiscard = false )
	{
		if ( GetCOREInterface()->GetCancel() )
			return false;

		numNodes++;

		bool del;
		ExpNode *n = new ExpNode;
		if ( !parentNode )
			rootExpNode = n;
		else
		{
			if ( parentDiscard )
				rootExpNode->children.push_back( n );
			else
				parentNode->children.push_back( n );
		}
		n->node = node;

		TriObject *o = GetTriObjectFromNode( node, 0, del );
		if ( o )
		{
			n->type = tri;
			if ( ( options & SCENE_EXPORT_SELECTED ) && ( !node->Selected() ) )
			{
				n->export = false;
				parentDiscard = true;	// all selected children will be attached to the root node
			}

			if ( n->export )
			{
				Matrix3 nodeTM = node->GetObjTMAfterWSM(0);
				int order[3];
				if (!nodeTM.Parity())
				{ order[0]=0; order[1]=1; order[2]=2; }
				else
				{ order[0]=2; order[1]=1; order[2]=0; }

				// export geometry info (verts, normals, etc)
				Mesh &mesh = o->GetMesh();

				size_t i;

				// verts
				n->verts.resize( mesh.getNumVerts() );
				for ( i = 0; i < ( int )n->verts.size(); i++ )
				{
					n->verts[i].x = mesh.verts[i].x;
					n->verts[i].y = mesh.verts[i].z;
					n->verts[i].z = mesh.verts[i].y;
				}
				n->faces.resize( mesh.getNumFaces() );
				//			memcpy( &n->faces.front(), mesh.faces, n->faces.size() * sizeof( Face ) );
				for ( i = 0; i < n->faces.size(); i++ )
				{
					n->faces[i].v[0] = mesh.faces[i].v[order[2]];
					n->faces[i].v[1] = mesh.faces[i].v[order[1]];
					n->faces[i].v[2] = mesh.faces[i].v[order[0]];
				}

				// colors
				if ( mesh.vertCol )
				{
					n->colors.resize( mesh.getNumVertCol() );
					for ( i = 0; i < (size_t)mesh.getNumVertCol(); i++ )
					{
						n->colors[i] = mesh.vertCol[i];
					}
					n->colorFaces.resize( mesh.getNumFaces() );
					for ( i = 0; i < (size_t)mesh.getNumFaces(); i++ )
					{
						n->colorFaces[i].v[0] = mesh.vcFace[i].t[order[2]];
						n->colorFaces[i].v[1] = mesh.vcFace[i].t[order[1]];
						n->colorFaces[i].v[2] = mesh.vcFace[i].t[order[0]];
					}
				}

				// uv
				if ( 0 != mesh.getNumTVerts() )
				{
					n->uvVerts.resize( mesh.getNumTVerts() );
					for ( i = 0; i < ( int )n->uvVerts.size(); i++ )
					{
						n->uvVerts[i].x = mesh.tVerts[i].x;
						n->uvVerts[i].y = 1 - mesh.tVerts[i].y;
					}
					n->uvFaces.resize( mesh.getNumFaces() );
					//				memcpy( &n->uvFaces.front(), mesh.tvFace, n->faces.size() * sizeof( TVFace ) );
					for ( i = 0; i < n->uvFaces.size(); i++ )
					{
						n->uvFaces[i].v[0] = mesh.tvFace[i].t[order[2]];
						n->uvFaces[i].v[1] = mesh.tvFace[i].t[order[1]];
						n->uvFaces[i].v[2] = mesh.tvFace[i].t[order[0]];
					}
				}

				// lightmap uv
				for (i = 2; i < MAX_MESHMAPS; i++)
				{
					if (mesh.mapSupport (i))
					{
						// fill uvLightmap
						size_t nverts = mesh.getNumMapVerts (i);
						n->uvLightmap.resize (nverts);
						for (size_t vx = 0; vx < nverts; vx++)
						{
							n->uvLightmap[vx].x = mesh.mapVerts (i)[vx].x;
							n->uvLightmap[vx].y = 1 - mesh.mapVerts (i)[vx].y;
						}

						n->uvLightmapFaces.resize( mesh.getNumFaces() );
						//				memcpy( &n->uvFaces.front(), mesh.tvFace, n->faces.size() * sizeof( TVFace ) );
						for (size_t f = 0; f < n->uvLightmapFaces.size(); f++)
						{
							n->uvLightmapFaces[f].v[0] = mesh.mapFaces (i)[f].t[order[2]];
							n->uvLightmapFaces[f].v[1] = mesh.mapFaces (i)[f].t[order[1]];
							n->uvLightmapFaces[f].v[2] = mesh.mapFaces (i)[f].t[order[0]];
						}
						break;
					}
				}

				// face normals
				std::vector< Point3 > faceNormals;
				faceNormals.resize( mesh.getNumFaces() );
				ExpNode::Face *face = &n->faces.front();
				for ( i = 0; i < ( int )faceNormals.size(); i++, face++ )
				{
					Point3 v0 = n->verts[face->v[order[0]]];
					Point3 v1 = n->verts[face->v[order[1]]];
					Point3 v2 = n->verts[face->v[order[2]]];
					Point3 norm =  ( v1 - v0 ) ^ ( v2 - v1 );
					faceNormals[i] = norm;
				}

				// indexing
				std::vector< std::vector<int> > indexing;
				indexing.resize( n->verts.size() );
				face = &n->faces.front();
				for ( i = 0; i < n->faces.size(); i++, face++ )
				{
					indexing[face->v[order[0]]].push_back( (int)i );
					indexing[face->v[order[1]]].push_back( (int)i );
					indexing[face->v[order[2]]].push_back( (int)i );
				}

				// vertex normals
				face = &n->faces.front();
				n->normalFaces.resize( n->faces.size() );
				for ( i = 0; i < n->faces.size(); i++, face++ )
				{
					for ( int k = 0; k < 3; k++ )
					{
						// calc normal for face's vertex
						Point3 norm = faceNormals[i];
						std::vector<int> &ind = indexing[face->v[order[k]]];
						for ( int j = 0; j < (int)ind.size(); j++ )
						{
							if ( ind[j] != i && ( mesh.faces[i].getSmGroup() & mesh.faces[ind[j]].getSmGroup() ) != 0 )
								norm += faceNormals[ind[j]];
						}
						norm = norm.Normalize();

						// check for duplicate
						std::vector< Point3 >::iterator it;
						it = std::find( n->normals.begin(), n->normals.end(), norm );
						int index = (int)n->normals.size();
						if ( it != n->normals.end() )
							index = it - n->normals.begin();
						else
							n->normals.push_back( norm );
						n->normalFaces[i].v[order[k]] = index;
					}
				}

				// material assignments
				Mtl *mtl = node->GetMtl();
				if ( mtl )
				{
					n->mtlIndex.resize( mesh.getNumFaces() );
					for ( i = 0; i < (size_t)mesh.getNumFaces(); i++ )
					{
						MtlMap::iterator it;
						if ( mtl->IsMultiMtl() )
							it = mtlIndex.find( mtl->GetSubMtl( mesh.faces[i].getMatID() % mtl->NumSubMtls() ) );
						else
							it = mtlIndex.find( mtl );

						if ( it == mtlIndex.end() )
							n->mtlIndex[i] = -1;
						else
							n->mtlIndex[i] = ( *it ).second;
					}
				}
			}

			if ( del )
				o->DeleteThis();
		}
		else
		{
			// can be light or dummy
			ObjectState state = node->EvalWorldState(0);
			if ( state.obj && state.obj->SuperClassID() == LIGHT_CLASS_ID )
			{
				n->lightentity.props[_T( "classname" )] = _T( "light" );
				// export light
				TCHAR str[256];	// required to do some formatting. unsafe!
				n->type = light;
				GenLight *lightObj=(GenLight *)state.obj;
				if ( lightObj->GetUseLight() == FALSE )
					n->lightentity.props[_T( "disabled" )] = _T( "1" );
				ObjLightDesc *ld = lightObj->CreateLightDesc( node );

				// export diffuse color
				// TODO: export color animation controller
				Color rgb=lightObj->GetRGBColor(0);
				_stprintf( str, _T( "%.2f %.2f %.2f" ), rgb.r, rgb.g, rgb.b );
				n->lightentity.props[ _T( "diffusecolor" ) ] = str;

				// export multiplier
//				multiplier = lightObj->GetIntensity( 0 );
                
				// export light type
				int type=lightObj->Type();
				switch ( type )
				{
				case OMNI_LIGHT:
					n->lightentity.props[ _T( "type" ) ] = _T( "omni" );
					break;
				case TSPOT_LIGHT:
				case FSPOT_LIGHT:
					n->lightentity.props[ _T( "type" ) ] = _T( "spot" );
					break;
				case DIR_LIGHT:
				case TDIR_LIGHT:
					n->lightentity.props[ _T( "type" ) ] = _T( "dir" );
					break;
				default:
					_stprintf( str, _T( "light '%s' has unknown type and will be treated as dummy object." ), node->GetName() );
					warning( str );
					n->type = dummy;
				}

				// export atten and range
				float atten[3] = { 1, 0, 0 };
				float range = 1.0e+5f;
				if ( lightObj->GetUseAtten() )
				{
					range = lightObj->GetAtten(0, ATTEN_END);
					atten[0] = 1.f;
					atten[1] = 1.f / ( lightObj->GetAtten( 0, ATTEN_END ) - lightObj->GetAtten( 0, ATTEN_START ) );
					atten[2] = atten[1] * atten[1];
				}
				_stprintf( str, _T( "%.2f" ), range );
				n->lightentity.props[ _T( "range" ) ] = str;
				_stprintf( str, _T( "%.2f %.2f %.2f" ), atten[0], atten[1], atten[2] );
				n->lightentity.props[ _T( "atten" ) ] = str;

				// get shadowcasting parms
				n->lightentity.props[ _T( "castshadows" ) ] = lightObj->GetShadow() ? _T( "1" ) : _T( "0" );

#if 0	// FIXME: uncomment this if matrix stuff doesn't apply
				// get direction (from matrix or directly from light)
                if ( type != OMNI_LIGHT )
				{
					Point3 pos=node->GetObjTMAfterWSM(0).GetTrans();
					position = Point3( pos.x, pos.z, pos.y );
			    	INode *target=node->GetTarget();
					Point3 direction;
					if ( target )
					{
						Point3 t=target->GetObjTMAfterWSM(0).GetTrans();
						direction = Point3( t.x, t.z, t.y );
						direction -= position;
						direction = Normalize( direction );
					}
					else
					{
						Point4 at = node->GetObjTMAfterWSM(0).GetColumn( 1 );
						direction = Point3( at.x, at.z, at.y );
						direction = Normalize( direction );
					}
					_stprintf( str, _T( "%.2f %.2f %.2f" ), direction[0], direction[1], direction[2] );
					n->lightentity.props[ _T( "direction" ) ] = str;
				}
#endif
				if ( type == TSPOT_LIGHT || type == FSPOT_LIGHT )
				{
					float theta = lightObj->GetHotspot(0) * 3.14159f / 360.f;
					float phi = lightObj->GetFallsize(0) * 3.14159f / 360.f;
					float falloff = 1.f; // FIXME: does 3dsmax has an analogue?
					_stprintf( str, _T( "%.2f" ), theta );
					n->lightentity.props[ _T( "theta" ) ] = str;
					_stprintf( str, _T( "%.2f" ), theta );
					n->lightentity.props[ _T( "phi" ) ] = str;
					_stprintf( str, _T( "%.2f" ), falloff );
					n->lightentity.props[ _T( "falloff" ) ] = str;
				}
			}
			else
			{
				n->type = dummy;
				// dummy has no additional attributes
			}
		}

		// export animation
		if ( !ExportAnim( node, n->animFrames ) )
			n->animFrames.clear();
		else
		{
			n->animSpeed = GetFrameRate();
		}

		GetCOREInterface()->ProgressUpdate((int)((float)numNodes/numberOfNodes*100.0f)); 

		for ( int i = 0; i < node->NumberOfChildren(); i++ )
		{
			if ( !ExportGeometry( n, node->GetChildNode( i ), parentName, options, numNodes, parentDiscard ) )
				return false;
		}

		return true;
	}

	#define ALMOST_ZERO 1.0e-3f
	// Not truly the correct way to compare floats of arbitary magnitude...
	bool EqualPoint3( const Point3 &p1, const Point3 &p2)
	{
		if (fabs(p1.x - p2.x) > ALMOST_ZERO)
			return false;
		if (fabs(p1.y - p2.y) > ALMOST_ZERO)
			return false;
		if (fabs(p1.z - p2.z) > ALMOST_ZERO)
			return false;

		return true;
	}

	bool		ExportAnim( INode *node, std::vector<Matrix3> &frames )
	{
		TimeValue start = pInterface->GetAnimRange().Start();
		TimeValue end = pInterface->GetAnimRange().End();
		TimeValue t;
		int delta = GetTicksPerFrame();
		Matrix3 startTM;
		Matrix3 tm;
		bool animated = false;

		for ( t = start; t <= end; t += delta )
		{
			tm = node->GetParentNode() ? node->GetObjTMAfterWSM( t ) * Inverse( node->GetParentNode()->GetObjTMAfterWSM( t ) ) : node->GetObjTMAfterWSM( t );
			if ( t != start )
			{
				if ( !EqualPoint3( tm.GetRow( 0 ), startTM.GetRow( 0 ) ) )
					animated = true;
				if ( !EqualPoint3( tm.GetRow( 1 ), startTM.GetRow( 1 ) ) )
					animated = true;
				if ( !EqualPoint3( tm.GetRow( 2 ), startTM.GetRow( 2 ) ) )
					animated = true;
				if ( !EqualPoint3( tm.GetTrans(), startTM.GetTrans() ) )
					animated = true;
			}
			else
				startTM = tm;
			frames.push_back( tm );
		}
		return animated;
	}

	TriObject* GetTriObjectFromNode( INode *pNode, TimeValue t, bool &bDeleteIt )
	{
		bDeleteIt = false;
		Object *obj = pNode->EvalWorldState( t ).obj;
		if ( obj && obj->CanConvertToType( Class_ID( TRIOBJ_CLASS_ID, 0 ) ) ) { 
			TriObject *tri = ( TriObject * )obj->ConvertToType( t, Class_ID( TRIOBJ_CLASS_ID, 0 ) );
			if ( obj != tri ) bDeleteIt = true;
			return tri;
		}
		return NULL;
	}

	void warning( TCHAR *str )
	{
		::MessageBox( pInterface->GetMAXHWnd(), str, "fexport warning", MB_OK | MB_ICONINFORMATION );
	}
};

BOOL CALLBACK FExport::ParmsDlgProc( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch ( msg )
	{
	case WM_INITDIALOG:
		break;
	case WM_COMMAND:
		switch ( LOWORD( wParam ) )
		{
		case IDOK:
			EndDialog( hwnd, TRUE );
			break;
		case IDCANCEL:
			EndDialog( hwnd, FALSE );
			break;
		}
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

class FExportClassDesc:public ClassDesc2 {
public:
	int 			IsPublic() {return 1;}
	void *			Create(BOOL loading = FALSE) {return new FExport();}
	const TCHAR *	ClassName() {return GetString(IDS_CLASS_NAME);}
	SClass_ID		SuperClassID() {return SCENE_EXPORT_CLASS_ID;}
	Class_ID		ClassID() {return FEXPORT_CLASS_ID;}
	const TCHAR* 	Category() {return GetString(IDS_CATEGORY);}
	const TCHAR*	InternalName() { return _T("FExport"); }	// returns fixed parsable name (scripter-visible name)
	HINSTANCE		HInstance() { return hInstance; }			// returns owning module handle
};

static FExportClassDesc FExportDesc;
ClassDesc2* GetFExportDesc() {return &FExportDesc;}

