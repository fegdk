#pragma once

#pragma warning ( disable : 4786 )
#pragma warning ( disable : 4244 )
#pragma warning ( disable : 4503 )

// stl
#include <strstream>
#include <vector>
#include <list>
#include <string>
#include <map>

// 3dsmax
#include "Max.h"
#include "resource.h"
#include "istdplug.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include "stdmat.h"
#include "modstack.h"

