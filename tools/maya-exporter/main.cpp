#include <maya/MPxFileTranslator.h>
#include <maya/MFnPlugin.h>
#include <maya/MItDag.h>
#include <maya/MGlobal.h>
#include <maya/MItSelectionList.h>
#include <maya/MPlug.h>
#include <maya/MFnSet.h>
#include <maya/MItDependencyGraph.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>
#include <maya/MFloatPointArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFn.h>
#include <maya/MItDependencyNodes.h>
#include <stdio.h>
#include <map>
#include <string>

class feMdlTranslator : public MPxFileTranslator
{
public:
	feMdlTranslator () : mpFile(NULL) {}
	virtual ~feMdlTranslator ()
	{
		if (mpFile)
		{
			fclose (mpFile);
		}
	}

	virtual MStatus writer (const MFileObject &file, const MString &optionsString, MPxFileTranslator::FileAccessMode mode);

	virtual bool haveWriteMethod () const { return true; }
	virtual bool haveReadMethod () const { return false; }
	virtual bool canBeOpened () const { return false; }

	virtual MString defaultExtension () const { return "mdl"; }

	static void *create () { return new feMdlTranslator (); }

protected:
	MStatus exportAll ();
	MStatus exportSelected ();

	void exportMesh (MDagPath path);

	void buildMaterialsMap ();

protected:
	FILE *mpFile;
	std::map<std::string, std::string> mMaterials;
	std::map<std::string, int> mMaterialMap;
};


MStatus feMdlTranslator::writer (const MFileObject &file, const MString &optionsString, MPxFileTranslator::FileAccessMode mode)
{
	mpFile = fopen (file.fullName ().asChar (), "w");
	if (!mpFile)
	{
		fprintf (stderr, "feMdlTranslator::writer error : unable to create/open for writing mpFile %s\n", file.fullName ().asChar ());
		return MStatus::kFailure;
	}

	MStatus status;
	if (mode == MPxFileTranslator::kExportAccessMode)
	{
		status = exportAll ();
	}
	else if (mode == MPxFileTranslator::kExportActiveAccessMode)
	{
		status = exportSelected ();
	}

	fprintf (mpFile, "}\n");
	fclose (mpFile);
	mpFile = NULL;
	return status;
}

MStatus feMdlTranslator::exportAll ()
{
	buildMaterialsMap ();

	fprintf (mpFile, "%s", "dummy sceneroot {\n"
			"\tnodeTransform {\n"
			"\t\t-1 0 0\n"
			"\t\t0 1 0\n"
			"\t\t0 0 1\n"
			"\t\t0 0 0\n"
			"\t}\n");

	MStatus status;
	MItDag itDag (MItDag::kDepthFirst, MFn::kMesh, &status);
	if (status == MStatus::kFailure)
	{
		MGlobal::displayError ("MItDag::MItDag");
		return MStatus::kFailure;
	}

	for (; !itDag.isDone (); itDag.next ())
	{
		MDagPath dagPath;
		itDag.getPath(dagPath);

		MFnDagNode dagNode (dagPath);
		if (dagNode.isIntermediateObject ())
		{
			continue;
		}
		if (!dagPath.hasFn (MFn::kMesh))
		{
			continue;
		}

		exportMesh (dagPath);
	}

	return MStatus::kSuccess;
}

MStatus feMdlTranslator::exportSelected ()
{
	buildMaterialsMap ();

	fprintf (mpFile, "%s", "dummy sceneroot {\n"
			"\tnodeTransform {\n"
			"\t\t-1 0 0\n"
			"\t\t0 1 0\n"
			"\t\t0 0 1\n"
			"\t\t0 0 0\n"
			"\t}\n");

	MSelectionList selection;
	MGlobal::getActiveSelectionList (selection);
	MItSelectionList itList (selection);

	if (itList.isDone ())
	{
		MGlobal::displayError ("nothing is selected");
		return MStatus::kFailure;
	}

	MItDag itDag (MItDag::kDepthFirst, MFn::kMesh);
	for (; !itList.isDone (); itList.next ())
	{
		MDagPath dagPath;
		itList.getDagPath (dagPath);

		itDag.reset (dagPath.node (), MItDag::kDepthFirst, MFn::kMesh);
		for (; !itDag.isDone (); itDag.next ())
		{
			MDagPath path;
			itDag.getPath (path);

			MFnDagNode dagNode (dagPath);
			if (dagNode.isIntermediateObject ())
			{
				continue;
			}
			if (!dagPath.hasFn (MFn::kMesh))
			{
				continue;
			}

			exportMesh (dagPath);
		}
	}

	return MStatus::kSuccess;
}

void feMdlTranslator::exportMesh (MDagPath dagPath)
{
	MFnDagNode dagNode (dagPath);
	MFnMesh mesh (dagPath);
	MItMeshPolygon itPoly (dagPath);

	// name
	fprintf (mpFile, "\ttriobject \"%s\" {\n", mesh.name ().asChar ());

	// transformation matrix
	fprintf (mpFile, "\t\tnodeTransform {\n");
	MMatrix m = dagNode.transformationMatrix ();
	fprintf (mpFile, "\t\t\t%f %f %f %f\n"
			"\t\t\t%f %f %f %f\n"
			"\t\t\t%f %f %f %f\n"
			"\t\t\t%f %f %f %f\n",
			m (0, 0), m (0, 1), m (0, 2), m (0, 3),
			m (1, 0), m (1, 1), m (1, 2), m (1, 3),
			m (2, 0), m (2, 1), m (2, 2), m (2, 3),
			m (3, 0), m (3, 1), m (3, 2), m (3, 3));
	fprintf (mpFile, "\t\t}\n");

	// vertices
	fprintf (mpFile, "\t\tverts {\n");
	fprintf (mpFile, "\t\t\tcount %d\n", mesh.numVertices ());
	MFloatPointArray varray;
	mesh.getPoints (varray, MSpace::kObject);
	for (int i = 0; i != (int)varray.length (); ++i)
	{
		MFloatPoint &v = varray[i];
		fprintf (mpFile, "\t\t\tv %f %f %f\n", v.x, v.y, v.z);
	}
	fprintf (mpFile, "\t\t}\n");

	// faces
	fprintf (mpFile, "\t\tfaces {\n");
	fprintf (mpFile, "\t\t\tcount %d\n", mesh.numPolygons ());
	for (; !itPoly.isDone (); itPoly.next ())
	{
		fprintf (mpFile, "\t\t\tf");
		for (int i = 0; i != (int)itPoly.polygonVertexCount (); ++i)
		{
			fprintf (mpFile, " %d", itPoly.vertexIndex (i));
		}
		fprintf (mpFile, "\n");
	}
	itPoly.reset ();
	fprintf (mpFile, "\t\t}\n");

	// normals
	fprintf (mpFile, "\t\tnormals {\n");
	fprintf (mpFile, "\t\t\tcount %d\n", mesh.numNormals ());
	MFloatVectorArray narray;
	mesh.getNormals (narray, MSpace::kObject);
	for (int i = 0; i != (int)narray.length (); ++i)
	{
		MFloatVector &v = narray[i];
		fprintf (mpFile, "\t\t\tv %f %f %f\n", v.x, v.y, v.z);
	}
	fprintf (mpFile, "\t\t}\n");

	// normalFaces
	fprintf (mpFile, "\t\tnormalFaces {\n");
	fprintf (mpFile, "\t\t\tcount %d\n", mesh.numPolygons ());
	for (; !itPoly.isDone (); itPoly.next ())
	{
		fprintf (mpFile, "\t\t\tf");
		for (int i = 0; i != (int)itPoly.polygonVertexCount (); ++i)
		{
			fprintf (mpFile, " %d", itPoly.normalIndex (i));
		}
		fprintf (mpFile, "\n");
	}
	itPoly.reset ();
	fprintf (mpFile, "\t\t}\n");

	// uvVerts
	fprintf (mpFile, "\t\tuvVerts {\n");
	fprintf (mpFile, "\t\t\tcount %d\n", mesh.numPolygons () * 3);
	for (; !itPoly.isDone (); itPoly.next ())
	{
		if (itPoly.hasUVs ())
		{
			float2 uv;
			for (int i = 0; i != (int)itPoly.polygonVertexCount (); ++i)
			{
				itPoly.getUV (i, uv);
				fprintf (mpFile, "\t\t\tv %f %f\n", uv[0], uv[1]);
			}
		}
		else
		{
			for (int i = 0; i != (int)itPoly.polygonVertexCount (); ++i)
			{
				fprintf (mpFile, "\t\t\tv 0.0 0.0\n");
			}
		}
	}
	itPoly.reset ();
	fprintf (mpFile, "\t\t}\n");

	// uvFaces
	fprintf (mpFile, "\t\tuvFaces {\n");
	fprintf (mpFile, "\t\t\tcount %d\n", mesh.numPolygons ());
	int cntr = 0;
	for (; !itPoly.isDone (); itPoly.next ())
	{
		fprintf (mpFile, "\t\t\tf %d %d %d\n", cntr, cntr + 1, cntr + 2);
		cntr += 3;
	}
	fprintf (mpFile, "\t\t}\n");
	itPoly.reset ();

	// mtlIndex
	fprintf (mpFile, "\t\tmtlIndex {\n");
	fprintf (mpFile, "\t\t\tcount %d\n", mesh.numPolygons ());
	std::string mtlname;
	MObjectArray shaders;
	MIntArray faceIndices;
	mesh.getConnectedShaders (0, shaders, faceIndices);
	for(unsigned int i = 0; i != faceIndices.length (); ++i)
	{
		if (faceIndices[i] != -1) {
			MFnDependencyNode fnShader (shaders[faceIndices[i]]);
			MPlug ssader = fnShader.findPlug ("surfaceShader");
			MPlugArray materials;
			ssader.connectedTo (materials, true, true);
			if (materials.length ())
			{
				MFnDependencyNode fnMat (materials[0].node ());
				mtlname = fnMat.name ().asChar ();
			} else
			{
				mtlname = "untextured";
			}
		} else
		{
			mtlname = "untextured";
		}
		fprintf (mpFile, "\t\t\tf %d\n", mMaterialMap[mtlname]);
	}

	fprintf (mpFile, "\t}\n");
}

void feMdlTranslator::buildMaterialsMap ()
{
	MStatus status;

	MItDependencyNodes itDep (MFn::kLambert);
	while (!itDep.isDone ())
	{
		MObject material = itDep.item ();
		MFnDependencyNode fnDependNode (material);
		MPlug plug = fnDependNode.findPlug ("color", &status);
		if (status == MS::kSuccess)
		{
			MPlugArray cc;
			plug.connectedTo (cc, true, false);
			if (cc.length() > 0)
			{
				MObject src = cc[0].node ();
				if (src.hasFn (MFn::kFileTexture))
				{
					MFnDependencyNode fnFile (src);
					MPlug ftnPlug = fnFile.findPlug ("fileTextureName", &status);
					if (status == MS::kSuccess)
					{
						MString fileTextureName;
						ftnPlug.getValue (fileTextureName);
						std::string fname = fileTextureName.asChar ();
						std::string::size_type idx = fname.find_last_of ('/');
						if (idx == std::string::npos)
						{
							idx = 0;
						}
						mMaterials[fnDependNode.name ().asChar ()] = fname.c_str () + idx + 1;
					}
				}
			}
		}

		itDep.next ();
	}
	MItDag itDag (MItDag::kDepthFirst, MFn::kMesh, &status);
	if (status == MStatus::kFailure)
	{
		MGlobal::displayError ("MItDag::MItDag");
		return;
	}

	fprintf (mpFile, "materials {\n");
	int i = 0;
	for (; !itDag.isDone (); itDag.next ())
	{
		MDagPath dagPath;
		itDag.getPath(dagPath);

		MFnDagNode dagNode (dagPath);
		if (dagNode.isIntermediateObject ())
		{
			continue;
		}
		if (!dagPath.hasFn (MFn::kMesh))
		{
			continue;
		}

		MFnMesh mesh (dagPath);
		MObjectArray shaders;
		MIntArray faceIndices;
		mesh.getConnectedShaders (0, shaders, faceIndices);
		for (unsigned int k = 0; k != shaders.length (); ++k)
		{
			MFnDependencyNode fnShader (shaders[k]);
			MPlug ssader = fnShader.findPlug ("surfaceShader");
			MPlugArray materials;
			ssader.connectedTo (materials, true, true);
			for (unsigned int l = 0; l != materials.length (); ++l)
			{
				MFnDependencyNode fnMat (materials[l].node ());
				std::map<std::string, std::string>::iterator it = mMaterials.find (fnMat.name ().asChar ());
				if (it == mMaterials.end ())
				{
					fprintf (mpFile, "\t\"untextured\"\n");
				} else
				{
					fprintf (mpFile, "\t\"%s\"\n", it->second.c_str ());
				}
				mMaterialMap[fnMat.name ().asChar ()] = i++;
			}
		}
	}
	fprintf (mpFile, "}\n\n");
}


MStatus initializePlugin (MObject o)
{
	MFnPlugin plugin (o, "fegdk", "0.01");
	return plugin.registerFileTranslator ("fegdk .mdl export", "none", feMdlTranslator::create);
}

MStatus uninitializePlugin (MObject o)
{
	MFnPlugin plugin (o);
	return plugin.deregisterFileTranslator ("fegdk .mdl export");
}
