// $Id$

// +waker 20050118: this tool is intended to convert x files taken from zmodeler to mdl format
// i used it to rip gta3 models into my demos
	
// +waker 20060707: adding support for linux, animation, skinmesh, X-files and recent version of .mdl
// exported by panda exporter (3dsmax)

#include <stdio.h>
#include <vector>
#include <string>
#include <locale.h>
#include <stdarg.h>

typedef struct
{
	float x, y, z;
} vec3;

typedef struct
{
	float x, y, z;
} vec2;

typedef struct
{
	float r, g, b, a;
} rgba;

struct node_t
{
	std::string name;

	float transform[4][4];

	std::vector<vec3> verts;
	std::vector<int> faces;
	std::vector<vec3> normals;
	std::vector<int> normalFaces;
	std::vector<vec2> uvVerts;
	std::vector<int> faceMtls;
	std::vector<int> duplicationIndices;

	node_t *next;
	node_t *children;
};

std::vector<std::string> materials;
std::vector<rgba> material_colors;

#define MAX_NODES	1000
node_t nodes[MAX_NODES];
int numNodes = 0;

node_t *allocNode (void)
{
	if (numNodes == MAX_NODES)
	{
		fprintf (stderr, "ERROR: too many nodes!\n");
		return NULL;
	}
	node_t *n = &nodes[numNodes++];
	n->next = n->children = NULL;
	memset (n->transform, 0, sizeof (n->transform));
	return n;
}

bool is_zmodeler = false;
bool is_panda = false;

std::string
get_chunk_name (FILE *fp)
{
	char name[100] = "";
	char *ptr = name;
	for (;;)
	{
		*ptr = fgetc (fp);
		if (*ptr == '{')
			break;
		ptr++;
	}
	*(ptr-1) = 0;

	ptr = name;
	while (*ptr <= 0x20)
		ptr++;

	return std::string (ptr);
}

void
skip_whitespaces (FILE *fp)
{
	while (fgetc (fp) <= 32);
}

int
loadNode (node_t *node, FILE *fp)
{
	node->name = get_chunk_name (fp);
	fprintf (stderr, "INFO: got node %s\n", node->name.c_str ());
	char token[100]="";

	for (;;)
	{
		// next token
		fscanf (fp, "%s", token);
		if (!strcmp (token, "}"))
			break;
		else if (!strcmp (token, "FrameTransformMatrix"))
		{
			fprintf (stderr, "INFO: got FrameTransformMatrix\n");
			fscanf (fp, "%s", token);	// {
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					if (j == 3 && i == 3)
						fscanf (fp, "%f;;", &node->transform[i][j]);
					else
						fscanf (fp, "%f,", &node->transform[i][j]);
				}
			}
			fscanf (fp, "%s", token);	// }
		}
		else if (!strcmp (token, "Frame"))
		{
			node_t *n = allocNode ();
			n->next = node->children;
			node->children = n;
			if (-1 == loadNode (n, fp))
				return -1;
		}
		else if (!strcmp (token, "Mesh"))
		{
			fprintf (stderr, "INFO: got Mesh\n");
			fscanf (fp, "%s", token);	// {

			if (is_zmodeler)
				std::string mesh_name = get_chunk_name (fp);	// ignored
			float x = 0.0f;
			float y = 1.1f;
			float z = x + y;

			int numverts;

			// get numverts
			fscanf (fp, "%d;", &numverts);
			fprintf (stderr, "INFO: got %d verts\n", numverts);

			node->verts.reserve (numverts);

			// get verts
			for (int i = 0; i < numverts; i++)
			{
				vec3 v;
				if (is_zmodeler)
					fscanf (fp, "%f; %f; %f;", &v.x, &v.y, &v.z);
				else
				{
					if (i != numverts-1)
						fscanf (fp, "%f;%f;%f;,", &v.x, &v.y, &v.z);
					else
						fscanf (fp, "%f;%f;%f;;", &v.x, &v.y, &v.z);
				}
				node->verts.push_back (v);
				fgetc (fp); // skip delimiter
			}

			int numfaces;
			// get numfaces
			fscanf (fp, "%d;", &numfaces);
			fprintf (stderr, "INFO: got %d faces\n", numfaces);

			node->faces.reserve (numfaces*3);

			// get faces
			for (int i = 0; i < numfaces; i++)
			{
				int three, a, b, c;
				if (i != numfaces-1)
					fscanf (fp, "%d;%d,%d,%d;,", &three, &a, &c, &b);
				else
					fscanf (fp, "%d;%d,%d,%d;;", &three, &a, &c, &b);
				node->faces.push_back (a);
				node->faces.push_back (b);
				node->faces.push_back (c);
				fgetc (fp); // skip delimiter
			}

			for (;;)	// read optional mesh counterparts
			{
				// next token
				fscanf (fp, "%s ", token);

				if (!strcmp (token, "}"))
				{
					// end of mesh
					break;
				}
				else if (!strcmp (token, "MeshNormals"))
				{
					while (fgetc (fp) != '{');
					int numnormals;
					// get numverts
					fscanf (fp, "%d;", &numnormals);
					fprintf (stderr, "INFO: got %d normals\n", numnormals);

					node->normals.reserve (numnormals);

					// get verts
					for (int i = 0; i < numnormals; i++)
					{
						vec3 v;
						if (is_zmodeler)
							fscanf (fp, "%f; %f; %f;", &v.x, &v.y, &v.z);
						else
						{
							if (i != numnormals-1)
								fscanf (fp, "%f;%f;%f;,", &v.x, &v.y, &v.z);
							else
								fscanf (fp, "%f;%f;%f;;", &v.x, &v.y, &v.z);
						}
						node->normals.push_back (v);
						fgetc (fp); // skip delimiter
					}
					int numnormalFaces;
					// get numfaces
					fscanf (fp, "%d;", &numnormalFaces);
					fprintf (stderr, "INFO: got %d normalFaces\n", numnormalFaces);

					node->normalFaces.reserve (numnormalFaces*3);

					// get faces
					for (int i = 0; i < numnormalFaces; i++)
					{
						int three, a, b, c;
						if (is_zmodeler)
							fscanf (fp, "%d;%d,%d,%d;", &three, &a, &b, &c);
						else
						{
							if (i != numnormalFaces-1)
								fscanf (fp, "%d;%d,%d,%d;,", &three, &a, &c, &b);
							else
								fscanf (fp, "%d;%d,%d,%d;;", &three, &a, &c, &b);
						}
						node->normalFaces.push_back (a);
						node->normalFaces.push_back (b);
						node->normalFaces.push_back (c);
						fgetc (fp); // skip delimiter
					}
					while (fgetc (fp) != '}');
					fprintf (stderr, "INFO: done with normals!\n");
				}
				else if (!strcmp (token, "MeshMaterialList"))
				{
					while (fgetc (fp) != '{');
					int num_mtls;
					int num_faces;
					fscanf (fp, "%d;", &num_mtls);
					fscanf (fp, "%d;", &num_faces);
					fprintf (stderr, "INFO: got %d/%d meshmateriallist\n", num_mtls, num_faces);
					node->faceMtls.reserve (num_faces);

					for (int i = 0; i < num_faces; i++)
					{
						int mtl;
						if (is_zmodeler)
							fscanf (fp, "%d", &mtl);
						else
						{
							if (i != num_faces-1)
								fscanf (fp, "%d,", &mtl);
							else
								fscanf (fp, "%d;", &mtl);
						}
						fgetc (fp);
						mtl += materials.size ();
						node->faceMtls.push_back (mtl);
					}

					if (is_zmodeler)
					{
						// read material names
						std::vector<std::string> names;
						for (int i = 0; i < num_mtls; i++)
						{
							while (fgetc (fp) != '{');
							char name[100] = "";
							char *ptr = name;
							for (;;)
							{
								*ptr = fgetc (fp);
								if (*ptr == '}')
									break;
								ptr++;
							}
							*ptr = 0;
							names.push_back (name);
						}

						// remap
						std::vector <int> mapping;
						mapping.resize (num_mtls);
						for (int i = 0; i < num_mtls; i++)
						{
							for (size_t j = 0; j < materials.size (); j++)
							{
								if (materials[j] == names[i])
								{
									mapping[i] = (int)j;
									break;
								}
							}
						}
						for (size_t i = 0; i < node->faceMtls.size(); i++)
						{
							node->faceMtls[i] = mapping[node->faceMtls[i]];
						}
						while (fgetc (fp) != '}');
					}
					else
					{
						for (;;)
						{
							fscanf (fp, "%s", token);
							if (!strcmp (token, "}"))
								break;
							else if (!strcmp (token, "Material"))
							{
								fprintf (stderr, "INFO: got material!\n");
								fscanf (fp, "%s", token); // {
								// skip until "TextureFilename"
								for (;;)
								{
									fscanf (fp, "%s", token);
									if (!strcmp (token, "}"))
									{
										break;
									}
									else if (!strcmp (token, "TextureFilename"))
									{
										fscanf (fp, "%s\n", token);	// {
										// get name in ""
										fgets (token, 100, fp);
										// parse
										char *ptr = token + strlen (token)-1;
										while (ptr != token && *ptr != '/' && *ptr != '\\')
											ptr--;
										if (ptr != token)
											ptr++;
										if (*ptr == '\"')
											ptr++;
										char *quote = ptr;
										while (*quote)
										{
											if (*quote=='\"' || *quote < 0x20)
											{
												*quote = 0;
												break;
											}
											quote++;
										}

										fprintf (stderr, "INFO: trimmed name = %s\n", ptr);
										materials.push_back (std::string ("textures/") + ptr);
										fscanf (fp, "%s", token);	// }
									}
								}
							}
							else
							{
								fprintf (stderr, "ERROR: loadMesh: MeshMaterialList has unknown material specification\n");
								return -1;
							}
						}
					}
				}
				else if (!strcmp (token, "MeshTextureCoords"))
				{
					while (fgetc (fp) != '{');
					int num_verts;
					fscanf (fp, "%d;", &num_verts);
					fprintf (stderr, "INFO: got %d texcoords\n", num_verts);
					node->uvVerts.reserve (num_verts);

					for (int i = 0; i < num_verts; i++)
					{
						vec2 uv;
						if (is_zmodeler)
							fscanf (fp, "%f;%f;", &uv.x, &uv.y);
						else
						{
							if (i != num_verts-1)
								fscanf (fp, "%f;%f;,", &uv.x, &uv.y);
							else
								fscanf (fp, "%f;%f;;", &uv.x, &uv.y);
						}
						node->uvVerts.push_back (uv);
						fgetc (fp);
					}
					while (fgetc (fp) != '}');
				}
				else if (!strcmp (token, "VertexDuplicationIndices"))	// zmodeler doesn't handle this
				{
					fscanf (fp, "%s", token);
					int num_verts;
					int num_orig_verts;
					fscanf (fp, "%d;", &num_verts);
					fscanf (fp, "%d;", &num_orig_verts);
					fprintf (stderr, "INFO: got %d/%d duplication index\n", num_verts, num_orig_verts);
					for (int i = 0; i < num_verts; i++)
					{
						int idx;
						if (i != num_verts-1)
							fscanf (fp, "%d,", &idx);
						else
							fscanf (fp, "%d;", &idx);
						node->duplicationIndices.push_back (idx);
					}
					while (fgetc (fp) != '}');
				}
				else
				{
					fprintf (stderr, "ERROR: loadMesh: nothing known about %s\n", token);
					return -1;
				}
			}
			
#if 0
			// write into output file

			if (!have_materials)
			{
				// write all mtls and root node stuff
				fprintf (out, "materials {\n");
				for (size_t i = 0; i < materials.size(); i++)
				{
					fprintf (out, "\t\"%d.png\"\n", i);
				}
				fprintf (out, "}\n\n");

				fprintf (out, "dummy root {\n");
				fprintf (out, "\tnodeTransform {\n");
				fprintf (out, "\t\t1.000000 0.000000 0.000000\n");
				fprintf (out, "\t\t0.000000 1.000000 0.000000\n");
				fprintf (out, "\t\t0.000000 0.000000 1.000000\n");
				fprintf (out, "\t\t0.000000 0.000000 0.000000\n\t}\n");
				have_materials = true;
			}

			// write current mesh
			fprintf (out, "\ttriobject \"%s\" {\n", mesh_name.c_str ());

			// transform
			fprintf (out, "\t\tnodeTransform {\n");
			fprintf (out, "\t\t\t1.000000 0.000000 0.000000\n");
			fprintf (out, "\t\t\t0.000000 1.000000 0.000000\n");
			fprintf (out, "\t\t\t0.000000 0.000000 1.000000\n");
			fprintf (out, "\t\t\t0.000000 0.000000 0.000000\n\t\t}\n");

			// verts
			fprintf (out, "\t\tverts {\n");
			fprintf (out, "\t\t\tcount %d\n", verts.size());
			for (size_t i = 0; i < verts.size(); i++)
			{
				fprintf (out, "\t\t\tv %f %f %f\n", -verts[i].x, verts[i].y, verts[i].z);
			}
			fprintf (out, "\t\t}\n");

			// faces
			fprintf (out, "\t\tfaces {\n");
			fprintf (out, "\t\t\tcount %d\n", faces.size()/3);
			for (size_t i = 0; i < faces.size()/3; i++)
			{
				fprintf (out, "\t\t\tf %d %d %d\n", faces[i*3+0], faces[i*3+1], faces[i*3+2]);
			}
			fprintf (out, "\t\t}\n");

			// normals
			fprintf (out, "\t\tnormals {\n");
			fprintf (out, "\t\t\tcount %d\n", normals.size());
			for (size_t i = 0; i < normals.size(); i++)
			{
				fprintf (out, "\t\t\tv %f %f %f\n", normals[i].x, normals[i].y, normals[i].z);
			}
			fprintf (out, "\t\t}\n");

			// normalFaces
			fprintf (out, "\t\tnormalFaces {\n");
			fprintf (out, "\t\t\tcount %d\n", normalFaces.size()/3);
			for (size_t i = 0; i < normalFaces.size()/3; i++)
			{
				fprintf (out, "\t\t\tf %d %d %d\n", normalFaces[i*3+0], normalFaces[i*3+1], normalFaces[i*3+2]);
			}
			fprintf (out, "\t\t}\n");

			// uvVerts
			fprintf (out, "\t\tuvVerts {\n");
			fprintf (out, "\t\t\tcount %d\n", uvVerts.size());
			for (size_t i = 0; i < uvVerts.size(); i++)
			{
				fprintf (out, "\t\t\tv %f %f\n", uvVerts[i].x, 1-uvVerts[i].y);
			}
			fprintf (out, "\t\t}\n");

			// uvFaces
			fprintf (out, "\t\tuvFaces {\n");
			fprintf (out, "\t\t\tcount %d\n", faces.size()/3);
			for (size_t i = 0; i < faces.size()/3; i++)
			{
				fprintf (out, "\t\t\tf %d %d %d\n", faces[i*3+0], faces[i*3+1], faces[i*3+2]);
			}
			fprintf (out, "\t\t}\n");

			// mtlIndex
			fprintf (out, "\t\tmtlIndex {\n");
			fprintf (out, "\t\t\tcount %d\n", faceMtls.size());
			for (size_t i = 0; i < faceMtls.size(); i++)
			{
				fprintf (out, "\t\t\tf %d\n", faceMtls[i]);
			}
			fprintf (out, "\t\t}\n");

			fprintf (out, "\t}\n");
#endif
		}
		else
		{
			fprintf (stderr, "ERROR: loadNode: nothing known about %s\n", token);
			return -1;
		}
	}
	return 0;
}

void
writeMaterials (FILE *fp)
{
	fprintf (fp, "materials {\n");
	for (int i = 0; i < materials.size (); i++)
		fprintf (fp, "\t\"%s\"\n", materials[i].c_str ());
	fprintf (fp, "}\n\n");
}

void
writeNodeStr (FILE *fp, int indent, const char *fmt, ...)
{
	char p[1000];
	va_list ap;

	va_start(ap, fmt);
	vsnprintf (p, 1000, fmt, ap);
	va_end(ap);

	for (int i = 0; i < indent; i++)
		fprintf (fp, "\t");
	fprintf (fp, p);
}

void
writeNode (FILE *fp, node_t *node, int ind)
{
	writeNodeStr (fp, ind, "%s \"%s\" {\n", node->verts.empty () ? "dummy" : "triobject", node->name.c_str ());
	ind++;
	writeNodeStr (fp, ind, "nodeTransform {\n");
	ind++;
	for (int i = 0; i < 4; i++)
	{
		writeNodeStr (fp, ind, "%f %f %f\n", node->transform[i][0], node->transform[i][1], node->transform[i][2]);
	}
	ind--;
	writeNodeStr (fp, ind, "}\n");

	// mesh stuff
	if (!node->verts.empty ())
	{
		writeNodeStr (fp, ind, "verts {\n");
		ind++;
		writeNodeStr (fp, ind, "count %d\n", node->verts.size ());
		for (int i = 0; i < node->verts.size (); i++)
		{
			writeNodeStr (fp, ind, "v %f %f %f\n", node->verts[i].x, node->verts[i].y, node->verts[i].z);
		}
		ind--;
		writeNodeStr (fp, ind, "}\n");

		if (!node->faces.empty ())
		{
			writeNodeStr (fp, ind, "faces {\n");
			ind++;
			writeNodeStr (fp, ind, "count %d\n", node->faces.size ()/3);
			for (int i = 0; i < node->faces.size ()/3; i++)
			{
				writeNodeStr (fp, ind, "f %d %d %d\n", node->faces[i*3+0], node->faces[i*3+1], node->faces[i*3+2]);
			}
			ind--;
			writeNodeStr (fp, ind, "}\n");
		}
		if (!node->normals.empty ())
		{
			writeNodeStr (fp, ind, "normals {\n");
			ind++;
			writeNodeStr (fp, ind, "count %d\n", node->normals.size ());
			for (int i = 0; i < node->normals.size (); i++)
			{
				writeNodeStr (fp, ind, "v %f %f %f\n", node->normals[i].x, node->normals[i].y, node->normals[i].z);
			}
			ind--;
			writeNodeStr (fp, ind, "}\n");
		}

		if (!node->normalFaces.empty ())
		{
			writeNodeStr (fp, ind, "normalFaces {\n");
			ind++;
			writeNodeStr (fp, ind, "count %d\n", node->normalFaces.size ()/3);
			for (int i = 0; i < node->normalFaces.size ()/3; i++)
			{
				writeNodeStr (fp, ind, "f %d %d %d\n", node->normalFaces[i*3+0], node->normalFaces[i*3+1], node->normalFaces[i*3+2]);
			}
			ind--;
			writeNodeStr (fp, ind, "}\n");
		}
		if (!node->uvVerts.empty ())
		{
			writeNodeStr (fp, ind, "uvVerts {\n");
			ind++;
			writeNodeStr (fp, ind, "count %d\n", node->uvVerts.size ());
			for (int i = 0; i < node->uvVerts.size (); i++)
			{
				writeNodeStr (fp, ind, "v %f %f\n", node->uvVerts[i].x, node->uvVerts[i].y);
			}
			ind--;
			writeNodeStr (fp, ind, "}\n");
		}

		if (!node->faces.empty ())
		{
			writeNodeStr (fp, ind, "uvFaces {\n");
			ind++;
			writeNodeStr (fp, ind, "count %d\n", node->faces.size ()/3);
			for (int i = 0; i < node->faces.size ()/3; i++)
			{
				writeNodeStr (fp, ind, "f %d %d %d\n", node->faces[i*3+0], node->faces[i*3+1], node->faces[i*3+2]);
			}
			ind--;
			writeNodeStr (fp, ind, "}\n");
		}

		if (!node->faceMtls.empty ())
		{
			writeNodeStr (fp, ind, "mtlIndex {\n");
			ind++;
			writeNodeStr (fp, ind, "count %d\n", node->faceMtls.size ());
			for (int i = 0; i < node->faceMtls.size (); i++)
			{
				writeNodeStr (fp, ind, "f %d\n", node->faceMtls[i]);
			}
			ind--;
			writeNodeStr (fp, ind, "}\n");
		}
	}
	for (node_t *n = node->children; n; n = n->next)
		writeNode (fp, n, ind);
	ind--;
	writeNodeStr (fp, ind, "}\n");
}

int
main (int argc, char *argv[])
{
	setlocale (LC_ALL, "C");
	if (argc <= 1)
	{
		printf ("x2mdl\n");
		printf ("(c) waker 2004-2006\n");
		printf ("usage: x2mdl fname.x\n");
		return -1;
	}


	// old info:
	// parse x file by hand
	// it must be guaranteed that the X file was taken from zmodeler
	// this file format is not standart since texturing info is stored just like in the gta itself

	// there's no templates, but we have a standart header here

	// format seems following

	// materials[]
	// frames[] (only one frame, it seems..)

	// material fmt:
	// Material <name> {
	//	ColorRGBA faceColor;
	//	FLOAT power;
	//	ColorRGB specularColor;
	//	ColorRGB emissiveColor;
	// }

	// name encodes texture also, and info about layer order i think

	// each frame consist of the following:
	// frame <name> {
	//	mesh <name> {
	//	 <num_verts>;
	//   <x; y; z;,>
	//   .... repeat num_verts times ...
	//
	//   <num_polygons>
	//	 <number_of_points;v0,v1,v2;,>
	//   .... repeat num_verts times ...
	//
	//	 [MeshNormals {
	//    <num_normals>
	//    <x;y;z;,>
	//    .... repeat num_normals times ...
	//    <num_normal_polygons>
	//	  <number_of_points;v0,v1,v2;,>
	//   }]
	//
	//   [MeshMaterialList {
	//	  <number_of_materials>;
	//    <number_of_polygons>;
	//    <mtl,>
	//    .... repeat number_of_polygons times ...
	//    {<mtlname>}
	//    .... repeat number_of_materials times ...
	//	 }]
	//
	//   [MeshTextureCoords {
	//    <num_points;>
	//    <u;v;,>
	//    .... repeat num_points times ...
	//   }]
	//	}
	//  .... next mesh ....
	// }

	// there's no any transformation matrices, seems everything is in shared space

	FILE *fp = fopen (argv[1], "rt");
	if (!fp)
	{
		printf ("failed to open file '%s'\n", argv[1]);
		return -1;
	}

	char *ptr = argv[1] + strlen (argv[1])-1;
	// result should written in current dir, so we strip path and ext leaving fname
	while (ptr > argv[1] && *ptr != '/' && *ptr != '\\')
		ptr--;
	if (ptr != argv[1])
		ptr++;

	// got last slash
	// now strip ext
	char *ext = ptr;
	while (*ext && *ext != '.')
		ext++;

	char new_name[200];
	strncpy (new_name, ptr, ext-ptr);
	new_name[ext-ptr] = 0;
	strcat (new_name, ".mdl");

	FILE *out = fopen (new_name, "w+t");

	bool have_materials = false;

	node_t *rootNode = NULL;

	// read header
	for (;;)
	{
		char hdr[100];
		fgets (hdr, 100, fp);
		if (!strncmp (hdr, "xof 0302txt 0032", 16))	// zmodeler x file (0302)
		{
			is_zmodeler = true;
		}
		else if (!strncmp (hdr, "xof 0303txt 0032", 16))
		{
			is_panda = true;
		}
		else
		{
			fprintf (stderr, "ERROR: wrong x file header in file %s\nmust be \"xof 0302txt 0032\" or \"xof 0303txt 0032\"\n", argv[1]);
			break;
		}
		for (;;)
		{
			char token[100]="";
			if (fscanf (fp, "%s", token) <= 0)
			{
				break;
			}
			if (is_panda && !strcmp (token, "template"))
			{
				fprintf (stderr, "INFO: skipping template\n");
				// skip template
				for (;;)
				{
					if (fscanf (fp, "%s", token) <= 0)
						break;
					if (!strcmp (token, "}"))
						break;
				}
			}
			else if (is_zmodeler && !strcmp (token, "Material"))	// zmodeler mtls (panda stores them in MeshMaterialList)
			{
				materials.push_back (get_chunk_name (fp));
				// read diffuse color
				rgba c;
				fscanf (fp, "%f, %f, %f, %f;;", &c.r, &c.g, &c.b, &c.a);
				material_colors.push_back (c);

				// skip all parameters
				while (fgetc (fp) != '}');

				// write output

			}
			else if (!strcmp (token, "Frame"))
			{
				rootNode = allocNode ();
				if (-1 == loadNode (rootNode, fp))
					break;
			}
			else
			{
				fprintf (stderr, "ERROR: main: nothing known about %s\n", token);
				return -1;
			}
		}

		break;
	}

	// write out
	writeMaterials (out);
	writeNode (out, rootNode, 0);

	fclose (out);
	fclose (fp);

	return 0;
}
