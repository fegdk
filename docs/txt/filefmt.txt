// $Header$

fast cd/dvd reads with minimized memory allocations and disk accesses.
----------------------------------------------------------------------

memory usage is increased by only small amount of bytes,
memory footprint is filesize + 4 bytes per chunk (for offsets).

e.g. we have a model.
model has chunks:

	- verts
	- faces
	- bounds
	- additional info

store all chunks altogether in linear buffer, and precede each chunk with 4 bytes of memory.
buffer size must be stored separately somewhere in header near the filename.
then load procedure (for 32-bit integer platforms) will look like following:

ubyte *buffer = new ubyte[buffersize];
ubyte *tmp = buffer;
stream->read( buffer, buffersize );

vertsptr = (vertex_t*)(tmp+4);
numverts = *((ulong*)tmp) / sizeof( vertex );
tmp += *((ulong*)tmp) + 4;
facesptr = (face_t*)(tmp+4);
numfaces = *((ulong*)tmp) / sizeof( face );
tmp += *((ulong*)tmp) + 4;
boundsptr = (bounds_t*)(tmp+4);
tmp += *((ulong*)tmp) + 4;
addinfoptr = (addinfo_t*)(tmp+4);

so, we have 1 diskread, 1 memalloc, and will need only 1 memfree on destruction.

