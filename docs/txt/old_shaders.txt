//------------------------------------------------------

[shader] shader_name {

	// map compiler directives
	#{directive_name} {directive_parms}
	
	[pass [pass_name]] {
		
		t0 texture_name
		[ t1 texture_name ]  // = NULL
		[ t2 texture_name ]  // = NULL
		[ t3 texture_name ]  // = NULL

		[ c{0..95} p1 p2 p3 p4 ]  // see notes below

		[ fvf { combination of fvf flags separated with "|" sign } ]
	
		rs rs_name rs_value
	
		vs vso_name
		ps pso_name
	}
}

const notes (in order of priority):

1. defaults are 0 0 0 0 vectors.
2. can be specified by shader writer ( read "artist" ).
3. can be changed by the engine without notify ( for predefined shaders only )

specification by the artist:

1. can be specified directly, e.g. "c0 x y z w"
2. can be specified with engine-defined macro command:

	"time"              value of time for the frame
	"bone{X}"           where X is in range 0..3 (4 bones total)
	"eyePos"            position of the camera

	full list of macros will be available a bit later.

    macro can use 1 constant (4d vector) or can set up to 4 constants (for matrices).

    e.g. "viewTM" and "worldTM" use 4 vectors (constants) as opposed to 4x4 matrix representation.

engine-reserved shaders use following names:

engineVS{X}
enginePS{X}

where X is a zero-based number.

artists cannot create their own shaders with names beginning with engineVS and enginePS.
those shaders CAN be specified by artists in their code,
however this is not the recommended behavior. use aliases instead.

one of the simplest example of aliased predefined pixel and vertex shaders are:
ps replace_t0_flt_t1_add_t2_blend_t3 // equal to enginePS03
vs envmap_2ndpass_01 // equal to engineVS01

that means (quake3-style shader):

{
	map t0 // e.g. $lightmap
	// no blendfunc which means just "replace"
}
{
	map t1 // e.g. textures/some_map.tga
	blendfunc filter // modulate2x
}
{
	map t2 // e.g. textures/fx/enmap01.tga
	blendfunc add // add
	tcgen environment // generate fake cam-space envmap texcoords
}
{
	map t3 // e.g. textures/fx/bloody_text.tga
	blendfunc blend // use texture alpha-channel to blend it on top of the stuff
}

each vertex shader (including all predefined shaders) requires
specific FVF codes/ textures/ constants to be set.
in the case of FVF mismatch the rendering behavior is undefined.
pixel shader requires constants and textures to be correct.
vertex shader requires constants and FVF to be correct.
there is no way to check that in any run-time way
so it is only programmer's and artist's errors.


parser can contain some bugs ( incorrect comment handler, etc ).
be patient - it will be fixed ASAP.
