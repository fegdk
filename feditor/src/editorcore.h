#ifndef __F_CORE_H
#define __F_CORE_H

#include <list>
#include <vector>
#include <gtk/gtk.h>
#include <fegdk/f_math.h>
#include <fegdk/f_parmmgr.h>
#include <fegdk/f_keybinder.h>
#include "editortypes.h"

// core interface

namespace fe
{
	class fontFT;
	class material;
	class texture;
}

namespace feditor
{
	
	class editorViewport;
	class editorCmd;
	class brush;
	class entity;
	class mainFrm;
	class glView;
	class eClass;
	class editorMap;


	class editorCore : public fe::baseObject
	{
	
	public:
	
		typedef std::vector <glView *> viewList;
		
	private:
	
		bool	mbAllowRendering;
		mainFrm*		mpMainFrm;
	
		viewList		mViewList;
		std::map <cStr, cStr> mSettings;
	
		// mouse stuff
		bool			mbSelectingArea;
		bool			mbSelectingFaces;
		bool			mbSelectingBrushes;
		bool			mbMovingSelection;
		point			mLastMousePt;
		point			mMoveStartMousePt;
		vector3		mMoveVec;
		vector3		mMovedVec;
		brushList		mMovingBrushes;
		vector3		mSnappedFaceDragDir;
		vector3		mFaceDragDir;
		bool			mbDraggingFaces;
		std::vector < std::vector <int> >		mDragFaces;		// faces that are in drag mode now. each element contains face indices of each selected brush
		matrix4		mCameraMatrix;
		bool	mbCamWireFrame;
		smartPtr <fe::material>	mpEntMtl;
		smartPtr <fe::material> mpSelectionMtl;
		smartPtr <fe::fontFT>	mpFont;
		bool	mbDrawingSelectionOutlines;
	
		bool mbDrawGrid;
	
		// clipper
		bool			mbClipperMode;
		vector3		mClipperPoints[3];
		int				mClipperNumPoints;
		int				mClipperMovingPoint;
	
		// rotate
		bool			mbRotateMode;
	    bool            mbRotatingSelection;
	    point         mRotateStartMousePt;
	    int             mRotationAmount;   // in degrees
		vector3       mRotationOrigin;
		bool			mbRotationMovingOrigin;
	
		// python
		void	initPython (void);
	
		void*							mpPyMainModule;
		void*							mpPyMainDictionary;
		void*							mpPyLogFile;
		void*							mpPyFEModule;
		void*							mpPyFEDictionary;
	
		std::vector <cStr>			mPyCmdHistory;
		int								mPyHistoryCursor;
	
		// scene
		smartPtr <editorMap>	mpMap;
		bool		mbSnapToGrid;
		smartPtr <brush>	mpActiveBrush;
	
		// commands
		int			mCurrentCommand;
		typedef std::vector <editorCmd*>	cmdList;
		cmdList mCommands;
	
		smartPtr <editorViewport>	mpCurrentViewport;
	
	public:
		
		editorCore (void);
		~editorCore (void);
		
		// config
		void		loadConfig (const char *fname);
	
		smartPtr <editorMap>	getMap (void) const;
	
		// resets current map to scratch
		void			reset (void);
	
		// returns mainfrm object
		mainFrm*		getMainFrm (void) const;
		
		// returns nearest brush and it's face
		smartPtr <brush>		pickNearest(const ray3 &ray, int *face = NULL);
	
		void handleClipperMouseEvent (GdkEventType type, guint button, guint state, gdouble x, gdouble y);
	
		void handleRotationMouseEvent (GdkEventType type, guint button, guint state, gdouble x, gdouble y);
	
		void handleDefaultMouseEvent (GdkEventType type, guint button, guint state, gdouble x, gdouble y);
	
		// handles mouse event for current viewport
		// type, button, state, x, y are all members of GdkEventButton or GdkEventMotion
		// valid types are GDK_BUTTON_PRESS, GDK_BUTTON_RELEASE, GDK_MOTION_NOTIFY
		void			mouseEvent (GdkEventType type, guint button, guint state, gdouble x, gdouble y);
	
		// returns last mouse position
		point			getLastMousePt (void) const;
	
		// sets new value for last mouse position
		void			setLastMousePt (const point &pt);
	
		// scene rendering
		void			render (void);
	
		// commands
		void			addCommand (editorCmd *cmd);
		void undo (void);
		void redo (void);
	
		// selects/deselects brush face using mouse pointer
		void			selectFace (const point &pt);
	
		// selects/deselects brush using mouse pointer
		void			selectBrush (const point &pt);
	
		// starts face drag mode
		void			dragBegin (int x, int y, const ray3 &ray, bool perspective);
	
		// updates drag process
		void			dragUpdate (const vector3 &dir);
		
		// ends face drag mode
		void			dragEnd (void);
		
		// allow/disallow rendering
		void			allowRendering (bool onoff);
		bool			isRenderingAllowed (void) const;
		
		// redraws all views
		void			setViewport (smartPtr <editorViewport> vp);
		smartPtr <editorViewport>		getViewport (void) const;
		void			registerView (glView *view);
		void			unregisterView (glView *view);
		void			redrawAllViews (void) const;

		// area selection
		void selectBegin (int x, int y);
		void selectEnd (int x, int y);
		void selectUpdate (int x, int y);
	
		// snap point
		void			snapPt (vector3 &pt, bool force = false) const;
	
		// draw grid for specified viewport
		void			drawGrid (void) const;
		
		// some script access
		void			pyRunString (const cStr &string);
		cStr			pyHistoryPrev (void);
		cStr			pyHistoryNext (void);
	
		void			pyRunScript (const char *filename);
		
		// clipper
		void			clipperToggle (void);
		bool			clipperGetMode (void) const;
		void			clipperSplitSelection (void);
		void			clipperClipSelection (void);
		void			clipperFlipNormal (void);
	
		// rotation
		void			rotateToggle (void);
		bool			rotateGetMode (void) const;
	
		// camera access
		const matrix4& getCameraMatrix (void) const;
		void setCameraMatrix (const matrix4 &mtx);
	
		smartPtr <fe::material>	getEntMtl (void) const;
		
		bool canCreateEntityFromSelection (const eClass *ec) const;
		smartPtr <brush> getEntBrush (const entity *ent) const;
	
		void toggleGrid (void);

		const char *getValueForKey (const char *key) const;
		bool isDrawingSelectionOutlines (void) const;
	
	};
	
	extern editorCore *g_editor;

}

#endif // __F_CORE_H

