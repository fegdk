#ifndef __F_PYOBJECT_H
#define __F_PYOBJECT_H

#ifdef _POSIX_C_SOURCE	// shut up pyconfig.h complaints
#undef _POSIX_C_SOURCE
#endif
#include <Python.h>
#include "feditor.h"

namespace feditor
{

	template <typename T> class pyObjectHolder {
		PyObject_HEAD;
		
		typedef pyObjectHolder <T> objType;
		smartPtr <T> obj_;
		cStr typeName_;
		bool decreffed_;
	
		static void destroy (PyObject *self)
		{
			fprintf (stderr, "python destroyed the object!\n");
			// this should be called when python destroys object
			objType *myobj = (objType*)self;
			myobj->obj_ = NULL;
			myobj->decreffed_ = false;
	
			// python object doesn't need deinitialization except "delete" on a whole object
		}
	
	public:
		pyObjectHolder (const char *typeName)
		{
			typeName_ = typeName;
			decreffed_ = false;
		}
	
		PyObject* getForObject (smartPtr <T> obj)
		{
			if (!obj_)
			{
				obj_ = obj;
				// python is definitely NOT initialized here
				static PyTypeObject typeDef = {
					PyObject_HEAD_INIT(NULL)
						0,                     /*ob_size*/
					(char*)typeName_.c_str (),        /*tp_name*/
					sizeof(*this),             /*tp_basicsize*/
					0,                         /*tp_itemsize*/
					this->destroy,             /*tp_dealloc*/
					0,                         /*tp_print*/
					0,                         /*tp_getattr*/
					0,                         /*tp_setattr*/
					0,                         /*tp_compare*/
					0,                         /*tp_repr*/
					0,                         /*tp_as_number*/
					0,                         /*tp_as_sequence*/
					0,                         /*tp_as_mapping*/
					0,                         /*tp_hash */
					0,                         /*tp_call*/
					0,                         /*tp_str*/
					0,                         /*tp_getattro*/
					0,                         /*tp_setattro*/
					0,                         /*tp_as_buffer*/
					Py_TPFLAGS_DEFAULT,        /*tp_flags*/
					(char*)typeName_.c_str (),        /* tp_doc */
				};
	
				PyObject_Init ((PyObject*)this, &typeDef);
			}
			return (PyObject *)this;
		}
	
		void decref (void)
		{
			assert (obj_);
			if (!decreffed_)
			{
				Py_DECREF ((PyObject*)this);
				decreffed_ = true;
			}
		}
	
		smartPtr <T>	getObject (void)
		{
			return obj_;
		}
	
	};
	
}

#endif // __F_PYOBJECT_H

