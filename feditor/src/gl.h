#ifndef __F_GL_H
#define __F_GL_H

namespace feditor
{

	class gl
	{
	public:
	#define DYNAMIC_BIND 1
	#define GL_EXT(_GL) static bool SUPPORTS##_GL;
	#define GL_PROC(_GL,type,name,args) static type (*name)args;
	#include "opengl.h"
	#undef DYNAMIC_BIND
	#undef GL_EXT
	#undef GL_PROC
	
		static void*	mhGLDLL;
	
		static void init (void);
		static void free (void);
		static void* bindProc (const char *name);
			
	};

}

#endif // __F_GL_H

