#include "pch.h"
#include "editorwidget.h"
#include "zview.h"
#include "editorview.h"

namespace feditor
{

	editorWidget::editorWidget (void)
	{
		GtkWidget *frame;
	
		mpSplitter = gtk_hpaned_new ();
		
		frame = gtk_frame_new (NULL);
		gtk_paned_pack1 (GTK_PANED (mpSplitter), frame, FALSE, FALSE);
	
		mpZView = new zView;
		gtk_widget_set_size_request (GTK_WIDGET (mpZView->getWidget ()), 40, 0);
		gtk_container_add (GTK_CONTAINER (frame), mpZView->getWidget ());
	
		frame = gtk_frame_new (NULL);
		gtk_paned_pack2 (GTK_PANED (mpSplitter), frame, TRUE, TRUE);
		mpEditorView = new editorView;
		gtk_container_add (GTK_CONTAINER (frame), mpEditorView->getWidget ());
		
	}
	
	editorWidget::~editorWidget (void)
	{
		delete mpEditorView;
		delete mpZView;
	}
	
	editorView *editorWidget::getEditorView (void) const
	{
		return mpEditorView;
	}
	
	zView *editorWidget::getZView (void) const
	{
		return mpZView;
	}
	
	GtkWidget *editorWidget::getContainer (void) const
	{
		return mpSplitter;
	}
	
}
