#include "pch.h"
#include <stdlib.h>
#include <stdio.h>
#ifdef __linux__
#include <dlfcn.h>
#endif
#include "gl.h"

namespace feditor
{

	//define static variables
	
	#define DYNAMIC_BIND 1
	#define GL_EXT(ext) bool gl::SUPPORTS##ext = false;
	#define GL_PROC(ext,type,name,args) type (*gl::name)args = NULL;
	#include "opengl.h"
	#undef DYNAMIC_BIND
	#undef GL_EXT
	#undef GL_PROC
	
	void*	gl::mhGLDLL = NULL;
			
	void gl::init (void)
	{
		// load opengl library
		const char *dllname = NULL;
	#ifdef __linux__
		dllname = "libGL.so.1";
	#else
	#	error "this platform isn't supported yet"
	#endif
	
		mhGLDLL = dlopen (dllname, RTLD_LAZY | RTLD_GLOBAL);
		const char *err = dlerror ();
		if (err)
			printf ("error loading opengl dll\n%s\n", err);
		if (mhGLDLL == NULL)
			return;
	
		printf ("loading opengl...\n");
	
	#define DYNAMIC_BIND 1
	#define GL_EXT(ext) SUPPORTS##ext = true;
	#define GL_PROC(ext,type,name,args) if (&SUPPORTS##ext == &SUPPORTS_GL || SUPPORTS##ext) { \
			printf ("loading ext %s proc %s .. ", #ext, #name); \
			name = (type(*)args)bindProc (#name); \
			if (!name) SUPPORTS##ext = false; \
			printf (name ? "ok\n" : "failed\n"); \
	}
	#include "opengl.h"
	#undef DYNAMIC_BIND
	#undef GL_EXT
	#undef GL_PROC
	}
	
	void gl::free (void)
	{
		printf ("freeing opengl...\n");
		if (mhGLDLL)
		{
			dlclose (mhGLDLL);
			mhGLDLL = NULL;
		}
	}
	
	void* gl::bindProc (const char *name)
	{
		return dlsym (mhGLDLL, name);
	}
	
}
