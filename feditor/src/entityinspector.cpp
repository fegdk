#include "pch.h"
#include "entityinspector.h"
#include "editorcore.h"
#include "editorcmd.h"
#include "mainfrm.h"
#include "logview.h"

namespace feditor
{

	// parm wrappers
	
	entParmWidget::entParmWidget (void)
	{
	}
	
	GtkWidget *entParmWidget::getWidget (void) const
	{
		return mpWidget;
	}
	
	// string
	
	entParmString::entParmString (void)
		: entParmWidget ()
	{
		mpWidget = gtk_entry_new ();
		gtk_widget_show (mpWidget);
	}
	
	cStr entParmString::getValue (void) const
	{
		return gtk_entry_get_text (GTK_ENTRY (mpWidget));
	}
	
	void entParmString::setValue (const char *value)
	{
		gtk_entry_set_text (GTK_ENTRY (mpWidget), value);
	}
	
	entParmBool::entParmBool (void)
		: entParmWidget ()
	{
		mpWidget = gtk_check_button_new ();
		gtk_widget_show (mpWidget);
	}
	
	cStr entParmBool::getValue (void) const
	{
		gboolean b = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (mpWidget));
		return b ? "1" : "0";
	}
	
	void entParmBool::setValue (const char *value)
	{
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mpWidget), atoi (value) ? TRUE : FALSE);
	}
	
	// parmbox
	
	entParmsBox::entParmsBox (GtkWidget *parent)
	{
		mpScroller = NULL;
		mpTable = NULL;
		mpParent = parent;
	}
	
	entParmsBox::~entParmsBox (void)
	{
	}
	
	void entParmsBox::build (const eClass *eclass)
	{
		if (mpScroller)
		{
			// this should destroy all widgets
			gtk_container_remove (GTK_CONTAINER (mpParent), mpScroller);
			mpScroller = NULL;
			mpTable = NULL;
			mParms.clear ();
		}
		if (!eclass)
			return;
		if (!mpScroller)
		{
			mpScroller = gtk_scrolled_window_new (NULL, NULL);
		    gtk_widget_show (mpScroller);
		    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (mpScroller), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		    gtk_box_pack_start (GTK_BOX (mpParent), mpScroller, TRUE, TRUE, 0);
			mpTable = gtk_table_new (1, 2, FALSE);
			gtk_widget_show (mpTable);
			gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (mpScroller), mpTable);
		}
		const eClass::eParmsMap &eParms = eclass->getEParms ();
		int sz = 0;
		for (eClass::eParmsMap::const_iterator i = eParms.begin (); i != eParms.end (); i++)
		{
			GtkWidget *value = NULL;
			entParmWidget *val;
			switch ((*i).second.type)
			{
			case eClass::string:
				val = new entParmString ();
				value = val->getWidget ();
				mParms[(*i).first] = val;
				break;
			case eClass::boolean:
				val = new entParmBool ();
				value = val->getWidget ();
				mParms[(*i).first] = val;
				break;
			}
			if (value)
			{
				if (sz >= 1)
					gtk_table_resize (GTK_TABLE (mpTable), sz+1, 2);
				GtkWidget *key = gtk_label_new ((*i).first);
				gtk_label_set_justify (GTK_LABEL (key), GTK_JUSTIFY_RIGHT);
				gtk_widget_show (key);
				gtk_misc_set_alignment (GTK_MISC (key), 1, 0);
				gtk_table_attach (GTK_TABLE (mpTable), key, 0, 1, sz, sz+1,
	                              (GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
	                              (GtkAttachOptions)(0), 2, 0);
				gtk_table_attach (GTK_TABLE (mpTable), value, 1, 2, sz, sz+1,
	                              (GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
	                              (GtkAttachOptions)(0), 2, 0);
				sz++;
			}
		}
	}
	
	void entParmsBox::fill (smartPtr <entity> ent)
	{
		const eClass *eclass = ent->getEClass ();
		const eClass::eParmsMap &eParms = eclass->getEParms ();
		for (eClass::eParmsMap::const_iterator i = eParms.begin (); i != eParms.end (); i++)
		{
			parmsMap::iterator it = mParms.find ((*i).first);
			if (it != mParms.end ())
			{
				const char *val = ent->getAttrValue ((*i).first);
				(*it).second->setValue (val ? val : "");
			}
		}
	}
	
	// ent inspector
	
	entityInspector::entityInspector (void)
	{
		GtkWidget *scroller;
		GtkWidget *vpaned[3];
	
		for (int i = 0; i < 3; i++)
		{
			vpaned[i] = gtk_vpaned_new ();
			gtk_widget_show (vpaned[i]);
		}
		mpWindow = new editorWnd (vpaned[0], "Entity");
		gtk_paned_pack1 (GTK_PANED (vpaned[0]), vpaned[1], TRUE, TRUE);
		gtk_paned_pack2 (GTK_PANED (vpaned[0]), vpaned[2], TRUE, FALSE);
	
		GtkCellRenderer *renderer;
		GtkTreeViewColumn *column;
		GtkTreeSelection *select;
		// eclass list
		scroller = gtk_scrolled_window_new (NULL, NULL);
		gtk_widget_show (scroller);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		gtk_paned_pack1 (GTK_PANED (vpaned[1]), scroller, TRUE, TRUE);
	    
		mpEClasses = gtk_list_store_new (1, G_TYPE_STRING);
	    GtkWidget *treeview = gtk_tree_view_new_with_model (GTK_TREE_MODEL (mpEClasses));
		gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (treeview), FALSE);
	    gtk_widget_show (treeview);
	    gtk_tree_view_set_enable_search (GTK_TREE_VIEW (treeview), TRUE);
	    renderer = gtk_cell_renderer_text_new ();
	    column = gtk_tree_view_column_new_with_attributes ("eclass", renderer, "text", 0, NULL);
	    gtk_tree_view_append_column (GTK_TREE_VIEW (treeview), column);
	    g_object_unref (G_OBJECT (mpEClasses));
	    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scroller), treeview);
		const eClass::eClassList& lst = eClass::getEClass ();
		for (eClass::eClassList::const_iterator i = lst.begin (); i != lst.end (); i++)
		{
			GtkTreeIter   iter;
			gtk_list_store_append (mpEClasses, &iter);
			gtk_list_store_set (mpEClasses, &iter, 0, (*i).getName ().c_str (), -1);
		}
		mpEClassesWidget = treeview;
		select = gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
		gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
		g_signal_connect_after (G_OBJECT (treeview), "row-activated", G_CALLBACK (setEClass), this);
	
		// description
		scroller = gtk_scrolled_window_new (NULL, NULL);
		gtk_widget_show (scroller);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		GtkWidget *view;
		view = gtk_text_view_new ();
		mpDescription = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));
		gtk_text_view_set_editable (GTK_TEXT_VIEW (view), FALSE);
	    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scroller), view);
		gtk_paned_pack2 (GTK_PANED (vpaned[1]), scroller, TRUE, TRUE);
	
		GtkWidget *vbox;
		// epairs box
		vbox = gtk_vbox_new (FALSE, 0);
		GtkWidget *list;
		GtkListStore *store;
	
		vbox = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox);
		scroller = gtk_scrolled_window_new (NULL, NULL);
		gtk_widget_show (scroller);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	    list = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
		gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (list), FALSE);
	    gtk_widget_show (list);
	    gtk_tree_view_set_enable_search (GTK_TREE_VIEW (list), TRUE);
	    renderer = gtk_cell_renderer_text_new ();
	    column = gtk_tree_view_column_new_with_attributes ("key", renderer, "text", 0, NULL);
	    gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
	    renderer = gtk_cell_renderer_text_new ();
	    column = gtk_tree_view_column_new_with_attributes ("value", renderer, "text", 1, NULL);
	    gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
	    g_object_unref (G_OBJECT (store));
	    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scroller), list);
		mpEPairsWidget = list;
		mpEPairs = store;
		select = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
		gtk_tree_selection_set_mode (select, GTK_SELECTION_SINGLE);
		gtk_box_pack_start (GTK_BOX (vbox), scroller, TRUE, TRUE, 0);
		g_signal_connect_after (G_OBJECT (list), "cursor-changed", G_CALLBACK (ePairsListCursorChanged), this);
	
		GtkWidget *label, *entry, *table;
		table = gtk_table_new (2, 2, FALSE);
		gtk_widget_show (table);
	
		label = gtk_label_new ("Key");
		gtk_widget_show (label);
		entry = gtk_entry_new ();
		gtk_widget_show (entry);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 0, 1,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		mpKeyEntry = entry;
		
		label = gtk_label_new ("Value");
		gtk_widget_show (label);
		entry = gtk_entry_new ();
		gtk_widget_show (entry);
		
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 1, 2,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		mpValueEntry = entry;
		g_signal_connect_after (G_OBJECT (entry), "activate", G_CALLBACK (setEntityAttr), this);
		
		gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
		
		GtkWidget *hbox = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (hbox);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
		GtkWidget *btn;
		btn = gtk_button_new_with_label ("Delete key");
		gtk_widget_show (btn);
		g_signal_connect_after (G_OBJECT (btn), "clicked", G_CALLBACK (deleteKey), this);
		gtk_box_pack_start (GTK_BOX (hbox), btn, TRUE, TRUE, 2);
		btn = gtk_button_new_with_label ("Clear all");
		gtk_widget_show (btn);
		g_signal_connect_after (G_OBJECT (btn), "clicked", G_CALLBACK (clearAll), this);
		gtk_box_pack_start (GTK_BOX (hbox), btn, TRUE, TRUE, 2);
	
		gtk_paned_pack1 (GTK_PANED (vpaned[2]), vbox, FALSE, FALSE);
	
		vbox = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox);
		gtk_paned_pack2 (GTK_PANED (vpaned[2]), vbox, FALSE, FALSE);
		// parameters
		mpParmsBox = new entParmsBox (vbox);
	}
	
	entityInspector::~entityInspector (void)
	{
	}
	
	void entityInspector::setEntity (smartPtr <entity> ent)
	{
		if (mpEntity != ent)
		{
			mpEntity = ent;
			mpParmsBox->build (ent ? ent->getEClass () : NULL);
		}
		gtk_list_store_clear (mpEPairs);
		if (ent)
		{
			mpParmsBox->fill (ent);
			gtk_text_buffer_set_text (GTK_TEXT_BUFFER (mpDescription), ent->getEClass ()->getDescription (), -1);
	
			cStr s;
			s.printf ("%d", eClass::getEClassIdx (ent->getEClass ()->getName ()));
			GtkTreePath *path = gtk_tree_path_new_from_string (s.c_str ());
			gtk_tree_view_set_cursor (GTK_TREE_VIEW (mpEClassesWidget), path, NULL, FALSE);
			g_free (path);
	
			const entity::ePairList &epairs = ent->getEPairs ();
			for (entity::ePairList::const_iterator it = epairs.begin (); it != epairs.end (); it++)
			{
				GtkTreeIter   iter;
				gtk_list_store_append (mpEPairs, &iter);
				gtk_list_store_set (mpEPairs, &iter, 0, (*it).getName (), 1, (*it).getValue (), -1);
				
			}
		}
		else
		{
			gtk_text_buffer_set_text (GTK_TEXT_BUFFER (mpDescription), "", -1);
		}
	}
	
	void entityInspector::toggle (void)
	{
		mpWindow->toggle ();
	}
	
	bool entityInspector::isVisible (void) const
	{
		return mpWindow->isVisible ();
	}
	
	void entityInspector::update (void)
	{
		setEntity (mpEntity);
	}
	
	void entityInspector::setEClass (GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, entityInspector *insp)
	{
		gchar *str = gtk_tree_path_to_string (path);
		const eClass *ec = &eClass::getEClass ()[atoi (str)];
		if (insp->mpEntity && g_editor->canCreateEntityFromSelection (ec))
		{
			g_editor->addCommand (new CmdCreateEntity (ec));
		}
		g_free (str);
	}
	
	void entityInspector::deleteKey (GtkButton *button, entityInspector *insp)
	{
		GtkTreePath *path;
		gtk_tree_view_get_cursor (GTK_TREE_VIEW (insp->mpEPairsWidget), &path, NULL);
		if (!path)
			return;
		gchar *str = gtk_tree_path_to_string (path);
		const entity::ePairList& epairs = insp->mpEntity->getEPairs ();
		g_editor->addCommand (new CmdSetEntityAttribute (insp->mpEntity, epairs[atoi (str)].getName (), NULL));
		g_free (str);
		g_editor->redrawAllViews ();
	}
	
	void entityInspector::clearAll (GtkButton *button, entityInspector *insp)
	{
		g_editor->addCommand (new CmdClearAllEntAttributes (insp->mpEntity));
		g_editor->redrawAllViews ();
	}
	
	void entityInspector::setEntityAttr (GtkEntry *entry, entityInspector *insp)
	{
		if (insp->mpEntity)
		{
			const gchar *key = gtk_entry_get_text (GTK_ENTRY (insp->mpKeyEntry));
			const gchar *value = gtk_entry_get_text (GTK_ENTRY (entry));
			if (value[0] == 0)
				value = 0;
	
			if (!strcmp (key, "classname"))
			{
				const eClass *ec = key ? eClass::getEClassByName (value) : NULL;
				if (!ec)
				{
					g_editor->getMainFrm ()->getLogView ()->print ("invalid eclass!\n");
				}
				else if (insp->mpEntity && g_editor->canCreateEntityFromSelection (ec))
				{
					g_editor->addCommand (new CmdCreateEntity (ec));
				}
			}
			else
				g_editor->addCommand (new CmdSetEntityAttribute (insp->mpEntity, key, value));
			g_editor->redrawAllViews ();
		}
	}
	
	void entityInspector::ePairsListCursorChanged (GtkTreeView *list, entityInspector *insp)
	{
		if (!insp->mpEntity)
			return;
		GtkTreePath *path;
		gtk_tree_view_get_cursor (list, &path, NULL);
		if (!path)
			return;
		gchar *str = gtk_tree_path_to_string (path);
		int idx = atoi (str);
		const entity::ePairList& epairs = insp->mpEntity->getEPairs ();
		gtk_entry_set_text (GTK_ENTRY (insp->mpKeyEntry), epairs[idx].getName ());
		gtk_entry_set_text (GTK_ENTRY (insp->mpValueEntry), epairs[idx].getValue ());
		g_free (str);
	}
	
}
