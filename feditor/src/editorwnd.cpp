#include "pch.h"
#include "editorwnd.h"

namespace feditor
{

	editorWnd::editorWnd (GtkWidget *widget, const char *name)
	{
		mpWidget = widget;
		mpWindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		gtk_window_set_title (GTK_WINDOW (mpWindow), name);
		gtk_window_set_resizable (GTK_WINDOW (mpWindow), TRUE);
		gtk_window_set_default_size (GTK_WINDOW (mpWindow), 300, 200);
		g_signal_connect (G_OBJECT (mpWindow), "delete_event", G_CALLBACK (gtk_widget_hide_on_delete), this);
		gtk_container_add (GTK_CONTAINER (mpWindow), widget);
	}
	
	editorWnd::~editorWnd (void)
	{
	}
	
	GtkWidget *editorWnd::getWidget (void) const
	{
		return mpWidget;
	}
	
	GtkWidget *editorWnd::getWindow (void) const
	{
		return mpWindow;
	}
	
	void editorWnd::toggle (void)
	{
		bool vis = (mpWindow->object.flags & GTK_VISIBLE) ? true : false;
		if (!vis)
			gtk_widget_show_all (mpWindow);
		else
			gtk_widget_hide (mpWindow);
	}
	
	bool editorWnd::isVisible (void) const
	{
		return (mpWindow->object.flags & GTK_VISIBLE) ? true : false;
	}
	
}
