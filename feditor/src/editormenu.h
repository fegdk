#ifndef __F_EDITORMENU_H
#define __F_EDITORMENU_H

#include <fegdk/f_baseobject.h>
#include <gtk/gtk.h>
#include "feditor.h"

namespace feditor
{

	class editorMenu : public fe::baseObject
	{
		GtkWidget *mpWidget;
		std::map <GtkMenuItem *, cStr> mItemNames;
		static void activateMenu (GtkMenuItem *menuitem, gpointer user_data);
		static void unlinkSelected (GtkMenuItem *menuitem, gpointer user_data);
	public:
		editorMenu (const char *name);
		~editorMenu (void);
		GtkWidget *getWidget (void) const;
	};
	
}

#endif // __F_EDITORMENU_H

