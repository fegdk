#ifndef __F_EDITORVIEW_H
#define __F_EDITORVIEW_H

#include <fegdk/f_fontft.h>
#include "glview.h"

namespace feditor
{

	class editorView : public glView
	{
			
	private:
	
		fe::fontFTPtr	mpFont;	// font for drawing current state
	
		virtual void init (void);
		virtual void reshape (void);
		virtual void draw (void);
		
		bool		mbMovingCamera;
		bool mbAllowContextMenu;
	
		static gboolean gtkButtonPress (GtkWidget *widget, GdkEventButton *event, gpointer user_data);
		static gboolean gtkButtonRelease (GtkWidget *widget, GdkEventButton *event, gpointer user_data);
		static gboolean gtkMotionNotify (GtkWidget *widget, GdkEventMotion *event, gpointer user_data);
		static gboolean gtkScroll (GtkWidget *widget, GdkEventScroll *event, gpointer user_data);
	
	public:
	
		editorView (void);
		~editorView (void);
		
	};
	
}

#endif // __F_EDITORVIEW_H

