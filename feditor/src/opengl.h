/*-----------------------------------------------------------------------------
	Standard OpenGL functions.
-----------------------------------------------------------------------------*/
#if DYNAMIC_BIND
GL_EXT(_GL)

// OpenGL.
GL_PROC(_GL,void,glAccum,(unsigned int,float))
GL_PROC(_GL,void,glAlphaFunc,(unsigned int,float))
GL_PROC(_GL,unsigned char,glAreTexturesResident,(int,const unsigned int*,unsigned char*))
GL_PROC(_GL,void,glArrayElement,(int))
GL_PROC(_GL,void,glBegin,(unsigned int))
GL_PROC(_GL,void,glBindTexture,(unsigned int,unsigned int))
GL_PROC(_GL,void,glBitmap,(int,int,float,float,float,float,const unsigned char*))
GL_PROC(_GL,void,glBlendFunc,(unsigned int,unsigned int))
GL_PROC(_GL,void,glCallList,(unsigned int))
GL_PROC(_GL,void,glCallLists,(int,unsigned int,const void*))
GL_PROC(_GL,void,glClear,(unsigned int))
GL_PROC(_GL,void,glClearAccum,(float,float,float,float))
GL_PROC(_GL,void,glClearColor,(float,float,float,float))
GL_PROC(_GL,void,glClearDepth,(double))
GL_PROC(_GL,void,glClearIndex,(float))
GL_PROC(_GL,void,glClearStencil,(int))
GL_PROC(_GL,void,glClipPlane,(unsigned int,const double*))
GL_PROC(_GL,void,glColor3b,(signed char,signed char,signed char))
GL_PROC(_GL,void,glColor3bv,(const signed char*))
GL_PROC(_GL,void,glColor3d,(double,double,double))
GL_PROC(_GL,void,glColor3dv,(const double*))
GL_PROC(_GL,void,glColor3f,(float,float,float))
GL_PROC(_GL,void,glColor3fv,(const float*))
GL_PROC(_GL,void,glColor3i,(int,int,int))
GL_PROC(_GL,void,glColor3iv,(const int*))
GL_PROC(_GL,void,glColor3s,(short,short,short))
GL_PROC(_GL,void,glColor3sv,(const short*))
GL_PROC(_GL,void,glColor3ub,(unsigned char,unsigned char,unsigned char))
GL_PROC(_GL,void,glColor3ubv,(const unsigned char*))
GL_PROC(_GL,void,glColor3ui,(unsigned int,unsigned int,unsigned int))
GL_PROC(_GL,void,glColor3uiv,(const unsigned int*))
GL_PROC(_GL,void,glColor3us,(unsigned short,unsigned short,unsigned short))
GL_PROC(_GL,void,glColor3usv,(const unsigned short*))
GL_PROC(_GL,void,glColor4b,(signed char,signed char,signed char,signed char))
GL_PROC(_GL,void,glColor4bv,(const signed char*))
GL_PROC(_GL,void,glColor4d,(double,double,double,double))
GL_PROC(_GL,void,glColor4dv,(const double*))
GL_PROC(_GL,void,glColor4f,(float,float,float,float))
GL_PROC(_GL,void,glColor4fv,(const float*))
GL_PROC(_GL,void,glColor4i,(int,int,int,int))
GL_PROC(_GL,void,glColor4iv,(const int*))
GL_PROC(_GL,void,glColor4s,(short,short,short,short))
GL_PROC(_GL,void,glColor4sv,(const short*))
GL_PROC(_GL,void,glColor4ub,(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha))
GL_PROC(_GL,void,glColor4ubv,(const unsigned char *v))
GL_PROC(_GL,void,glColor4ui,(unsigned int red, unsigned int green, unsigned int blue, unsigned int alpha))
GL_PROC(_GL,void,glColor4uiv,(const unsigned int *v))
GL_PROC(_GL,void,glColor4us,(unsigned short red, unsigned short green, unsigned short blue, unsigned short alpha))
GL_PROC(_GL,void,glColor4usv,(const unsigned short *v))
GL_PROC(_GL,void,glColorMask,(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha))
GL_PROC(_GL,void,glColorMaterial,(unsigned int face, unsigned int mode))
GL_PROC(_GL,void,glColorPointer,(int size, unsigned int type, int stride, const void *pointer))
GL_PROC(_GL,void,glCopyPixels,(int x, int y, int width, int height, unsigned int type))
GL_PROC(_GL,void,glCopyTexImage1D,(unsigned int target, int level, unsigned int internalFormat, int x, int y, int width, int border))
GL_PROC(_GL,void,glCopyTexImage2D,(unsigned int target, int level, unsigned int internalFormat, int x, int y, int width, int height, int border))
GL_PROC(_GL,void,glCopyTexSubImage1D,(unsigned int target, int level, int xoffset, int x, int y, int width))
GL_PROC(_GL,void,glCopyTexSubImage2D,(unsigned int target, int level, int xoffset, int yoffset, int x, int y, int width, int height))
GL_PROC(_GL,void,glCullFace,(unsigned int mode))
GL_PROC(_GL,void,glDeleteLists,(unsigned int list, int range))
GL_PROC(_GL,void,glDeleteTextures,(int n, const unsigned int *textures))
GL_PROC(_GL,void,glDepthFunc,(unsigned int func))
GL_PROC(_GL,void,glDepthMask,(unsigned char flag))
GL_PROC(_GL,void,glDepthRange,(double zNear, double zFar))
GL_PROC(_GL,void,glDisable,(unsigned int cap))
GL_PROC(_GL,void,glDisableClientState,(unsigned int array))
GL_PROC(_GL,void,glDrawArrays,(unsigned int mode, int first, int count))
GL_PROC(_GL,void,glDrawBuffer,(unsigned int mode))
GL_PROC(_GL,void,glDrawElements,(unsigned int mode, int count, unsigned int type, const void *indices))
GL_PROC(_GL,void,glDrawPixels,(int width, int height, unsigned int format, unsigned int type, const void *pixels))
GL_PROC(_GL,void,glEdgeFlag,(unsigned char flag))
GL_PROC(_GL,void,glEdgeFlagPointer,(int stride, const void *pointer))
GL_PROC(_GL,void,glEdgeFlagv,(const unsigned char *flag))
GL_PROC(_GL,void,glEnable,(unsigned int cap))
GL_PROC(_GL,void,glEnableClientState,(unsigned int array))
GL_PROC(_GL,void,glEnd,(void))
GL_PROC(_GL,void,glEndList,(void))
GL_PROC(_GL,void,glEvalCoord1d,(double u))
GL_PROC(_GL,void,glEvalCoord1dv,(const double *u))
GL_PROC(_GL,void,glEvalCoord1f,(float u))
GL_PROC(_GL,void,glEvalCoord1fv,(const float *u))
GL_PROC(_GL,void,glEvalCoord2d,(double u, double v))
GL_PROC(_GL,void,glEvalCoord2dv,(const double *u))
GL_PROC(_GL,void,glEvalCoord2f,(float u, float v))
GL_PROC(_GL,void,glEvalCoord2fv,(const float *u))
GL_PROC(_GL,void,glEvalMesh1,(unsigned int mode, int i1, int i2))
GL_PROC(_GL,void,glEvalMesh2,(unsigned int mode, int i1, int i2, int j1, int j2))
GL_PROC(_GL,void,glEvalPoint1,(int i))
GL_PROC(_GL,void,glEvalPoint2,(int i, int j))
GL_PROC(_GL,void,glFeedbackBuffer,(int size, unsigned int type, float *buffer))
GL_PROC(_GL,void,glFinish,(void))
GL_PROC(_GL,void,glFlush,(void))
GL_PROC(_GL,void,glFogf,(unsigned int pname, float param))
GL_PROC(_GL,void,glFogfv,(unsigned int pname, const float *params))
GL_PROC(_GL,void,glFogi,(unsigned int pname, int param))
GL_PROC(_GL,void,glFogiv,(unsigned int pname, const int *params))
GL_PROC(_GL,void,glFrontFace,(unsigned int mode))
GL_PROC(_GL,void,glFrustum,(double left, double right, double bottom, double top, double zNear, double zFar))
GL_PROC(_GL,unsigned int,glGenLists,(int range))
GL_PROC(_GL,void,glGenTextures,(int n, unsigned int *textures))
GL_PROC(_GL,void,glGetBooleanv,(unsigned int pname, unsigned char *params))
GL_PROC(_GL,void,glGetClipPlane,(unsigned int plane, double *equation))
GL_PROC(_GL,void,glGetDoublev,(unsigned int pname, double *params))
GL_PROC(_GL,unsigned int,glGetError,(void))
GL_PROC(_GL,void,glGetFloatv,(unsigned int pname, float *params))
GL_PROC(_GL,void,glGetIntegerv,(unsigned int pname, int *params))
GL_PROC(_GL,void,glGetLightfv,(unsigned int light, unsigned int pname, float *params))
GL_PROC(_GL,void,glGetLightiv,(unsigned int light, unsigned int pname, int *params))
GL_PROC(_GL,void,glGetMapdv,(unsigned int target, unsigned int query, double *v))
GL_PROC(_GL,void,glGetMapfv,(unsigned int target, unsigned int query, float *v))
GL_PROC(_GL,void,glGetMapiv,(unsigned int target, unsigned int query, int *v))
GL_PROC(_GL,void,glGetMaterialfv,(unsigned int face, unsigned int pname, float *params))
GL_PROC(_GL,void,glGetMaterialiv,(unsigned int face, unsigned int pname, int *params))
GL_PROC(_GL,void,glGetPixelMapfv,(unsigned int map, float *values))
GL_PROC(_GL,void,glGetPixelMapuiv,(unsigned int map, unsigned int *values))
GL_PROC(_GL,void,glGetPixelMapusv,(unsigned int map, unsigned short *values))
GL_PROC(_GL,void,glGetPointerv,(unsigned int pname, void* *params))
GL_PROC(_GL,void,glGetPolygonStipple,(unsigned char *mask))
GL_PROC(_GL,const unsigned char *,glGetString,(unsigned int name))
GL_PROC(_GL,void,glGetTexEnvfv,(unsigned int target, unsigned int pname, float *params))
GL_PROC(_GL,void,glGetTexEnviv,(unsigned int target, unsigned int pname, int *params))
GL_PROC(_GL,void,glGetTexGendv,(unsigned int coord, unsigned int pname, double *params))
GL_PROC(_GL,void,glGetTexGenfv,(unsigned int coord, unsigned int pname, float *params))
GL_PROC(_GL,void,glGetTexGeniv,(unsigned int coord, unsigned int pname, int *params))
GL_PROC(_GL,void,glGetTexImage,(unsigned int target, int level, unsigned int format, unsigned int type, void *pixels))
GL_PROC(_GL,void,glGetTexLevelParameterfv,(unsigned int target, int level, unsigned int pname, float *params))
GL_PROC(_GL,void,glGetTexLevelParameteriv,(unsigned int target, int level, unsigned int pname, int *params))
GL_PROC(_GL,void,glGetTexParameterfv,(unsigned int target, unsigned int pname, float *params))
GL_PROC(_GL,void,glGetTexParameteriv,(unsigned int target, unsigned int pname, int *params))
GL_PROC(_GL,void,glHint,(unsigned int target, unsigned int mode))
GL_PROC(_GL,void,glIndexMask,(unsigned int mask))
GL_PROC(_GL,void,glIndexPointer,(unsigned int type, int stride, const void *pointer))
GL_PROC(_GL,void,glIndexd,(double c))
GL_PROC(_GL,void,glIndexdv,(const double *c))
GL_PROC(_GL,void,glIndexf,(float c))
GL_PROC(_GL,void,glIndexfv,(const float *c))
GL_PROC(_GL,void,glIndexi,(int c))
GL_PROC(_GL,void,glIndexiv,(const int *c))
GL_PROC(_GL,void,glIndexs,(short c))
GL_PROC(_GL,void,glIndexsv,(const short *c))
GL_PROC(_GL,void,glIndexub,(unsigned char c))
GL_PROC(_GL,void,glIndexubv,(const unsigned char *c))
GL_PROC(_GL,void,glInitNames,(void))
GL_PROC(_GL,void,glInterleavedArrays,(unsigned int format, int stride, const void *pointer))
GL_PROC(_GL,unsigned char,glIsEnabled,(unsigned int cap))
GL_PROC(_GL,unsigned char,glIsList,(unsigned int list))
GL_PROC(_GL,unsigned char,glIsTexture,(unsigned int texture))
GL_PROC(_GL,void,glLightModelf,(unsigned int pname, float param))
GL_PROC(_GL,void,glLightModelfv,(unsigned int pname, const float *params))
GL_PROC(_GL,void,glLightModeli,(unsigned int pname, int param))
GL_PROC(_GL,void,glLightModeliv,(unsigned int pname, const int *params))
GL_PROC(_GL,void,glLightf,(unsigned int light, unsigned int pname, float param))
GL_PROC(_GL,void,glLightfv,(unsigned int light, unsigned int pname, const float *params))
GL_PROC(_GL,void,glLighti,(unsigned int light, unsigned int pname, int param))
GL_PROC(_GL,void,glLightiv,(unsigned int light, unsigned int pname, const int *params))
GL_PROC(_GL,void,glLineStipple,(int factor, unsigned short pattern))
GL_PROC(_GL,void,glLineWidth,(float width))
GL_PROC(_GL,void,glListBase,(unsigned int base))
GL_PROC(_GL,void,glLoadIdentity,(void))
GL_PROC(_GL,void,glLoadMatrixd,(const double *m))
GL_PROC(_GL,void,glLoadMatrixf,(const float *m))
GL_PROC(_GL,void,glLoadName,(unsigned int name))
GL_PROC(_GL,void,glLogicOp,(unsigned int opcode))
GL_PROC(_GL,void,glMap1d,(unsigned int target, double u1, double u2, int stride, int order, const double *points))
GL_PROC(_GL,void,glMap1f,(unsigned int target, float u1, float u2, int stride, int order, const float *points))
GL_PROC(_GL,void,glMap2d,(unsigned int target, double u1, double u2, int ustride, int uorder, double v1, double v2, int vstride, int vorder, const double *points))
GL_PROC(_GL,void,glMap2f,(unsigned int target, float u1, float u2, int ustride, int uorder, float v1, float v2, int vstride, int vorder, const float *points))
GL_PROC(_GL,void,glMapGrid1d,(int un, double u1, double u2))
GL_PROC(_GL,void,glMapGrid1f,(int un, float u1, float u2))
GL_PROC(_GL,void,glMapGrid2d,(int un, double u1, double u2, int vn, double v1, double v2))
GL_PROC(_GL,void,glMapGrid2f,(int un, float u1, float u2, int vn, float v1, float v2))
GL_PROC(_GL,void,glMaterialf,(unsigned int face, unsigned int pname, float param))
GL_PROC(_GL,void,glMaterialfv,(unsigned int face, unsigned int pname, const float *params))
GL_PROC(_GL,void,glMateriali,(unsigned int face, unsigned int pname, int param))
GL_PROC(_GL,void,glMaterialiv,(unsigned int face, unsigned int pname, const int *params))
GL_PROC(_GL,void,glMatrixMode,(unsigned int mode))
GL_PROC(_GL,void,glMultMatrixd,(const double *m))
GL_PROC(_GL,void,glMultMatrixf,(const float *m))
GL_PROC(_GL,void,glNewList,(unsigned int list, unsigned int mode))
GL_PROC(_GL,void,glNormal3b,(signed char nx, signed char ny, signed char nz))
GL_PROC(_GL,void,glNormal3bv,(const signed char *v))
GL_PROC(_GL,void,glNormal3d,(double nx, double ny, double nz))
GL_PROC(_GL,void,glNormal3dv,(const double *v))
GL_PROC(_GL,void,glNormal3f,(float nx, float ny, float nz))
GL_PROC(_GL,void,glNormal3fv,(const float *v))
GL_PROC(_GL,void,glNormal3i,(int nx, int ny, int nz))
GL_PROC(_GL,void,glNormal3iv,(const int *v))
GL_PROC(_GL,void,glNormal3s,(short nx, short ny, short nz))
GL_PROC(_GL,void,glNormal3sv,(const short *v))
GL_PROC(_GL,void,glNormalPointer,(unsigned int type, int stride, const void *pointer))
GL_PROC(_GL,void,glOrtho,(double left, double right, double bottom, double top, double zNear, double zFar))
GL_PROC(_GL,void,glPassThrough,(float token))
GL_PROC(_GL,void,glPixelMapfv,(unsigned int map, int mapsize, const float *values))
GL_PROC(_GL,void,glPixelMapuiv,(unsigned int map, int mapsize, const unsigned int *values))
GL_PROC(_GL,void,glPixelMapusv,(unsigned int map, int mapsize, const unsigned short *values))
GL_PROC(_GL,void,glPixelStoref,(unsigned int pname, float param))
GL_PROC(_GL,void,glPixelStorei,(unsigned int pname, int param))
GL_PROC(_GL,void,glPixelTransferf,(unsigned int pname, float param))
GL_PROC(_GL,void,glPixelTransferi,(unsigned int pname, int param))
GL_PROC(_GL,void,glPixelZoom,(float xfactor, float yfactor))
GL_PROC(_GL,void,glPointSize,(float size))
GL_PROC(_GL,void,glPolygonMode,(unsigned int face, unsigned int mode))
GL_PROC(_GL,void,glPolygonOffset,(float factor, float units))
GL_PROC(_GL,void,glPolygonStipple,(const unsigned char *mask))
GL_PROC(_GL,void,glPopAttrib,(void))
GL_PROC(_GL,void,glPopClientAttrib,(void))
GL_PROC(_GL,void,glPopMatrix,(void))
GL_PROC(_GL,void,glPopName,(void))
GL_PROC(_GL,void,glPrioritizeTextures,(int n, const unsigned int *textures, const float *priorities))
GL_PROC(_GL,void,glPushAttrib,(unsigned int mask))
GL_PROC(_GL,void,glPushClientAttrib,(unsigned int mask))
GL_PROC(_GL,void,glPushMatrix,(void))
GL_PROC(_GL,void,glPushName,(unsigned int name))
GL_PROC(_GL,void,glRasterPos2d,(double x, double y))
GL_PROC(_GL,void,glRasterPos2dv,(const double *v))
GL_PROC(_GL,void,glRasterPos2f,(float x, float y))
GL_PROC(_GL,void,glRasterPos2fv,(const float *v))
GL_PROC(_GL,void,glRasterPos2i,(int x, int y))
GL_PROC(_GL,void,glRasterPos2iv,(const int *v))
GL_PROC(_GL,void,glRasterPos2s,(short x, short y))
GL_PROC(_GL,void,glRasterPos2sv,(const short *v))
GL_PROC(_GL,void,glRasterPos3d,(double x, double y, double z))
GL_PROC(_GL,void,glRasterPos3dv,(const double *v))
GL_PROC(_GL,void,glRasterPos3f,(float x, float y, float z))
GL_PROC(_GL,void,glRasterPos3fv,(const float *v))
GL_PROC(_GL,void,glRasterPos3i,(int x, int y, int z))
GL_PROC(_GL,void,glRasterPos3iv,(const int *v))
GL_PROC(_GL,void,glRasterPos3s,(short x, short y, short z))
GL_PROC(_GL,void,glRasterPos3sv,(const short *v))
GL_PROC(_GL,void,glRasterPos4d,(double x, double y, double z, double w))
GL_PROC(_GL,void,glRasterPos4dv,(const double *v))
GL_PROC(_GL,void,glRasterPos4f,(float x, float y, float z, float w))
GL_PROC(_GL,void,glRasterPos4fv,(const float *v))
GL_PROC(_GL,void,glRasterPos4i,(int x, int y, int z, int w))
GL_PROC(_GL,void,glRasterPos4iv,(const int *v))
GL_PROC(_GL,void,glRasterPos4s,(short x, short y, short z, short w))
GL_PROC(_GL,void,glRasterPos4sv,(const short *v))
GL_PROC(_GL,void,glReadBuffer,(unsigned int mode))
GL_PROC(_GL,void,glReadPixels,(int x, int y, int width, int height, unsigned int format, unsigned int type, void *pixels))
GL_PROC(_GL,void,glRectd,(double x1, double y1, double x2, double y2))
GL_PROC(_GL,void,glRectdv,(const double *v1, const double *v2))
GL_PROC(_GL,void,glRectf,(float x1, float y1, float x2, float y2))
GL_PROC(_GL,void,glRectfv,(const float *v1, const float *v2))
GL_PROC(_GL,void,glRecti,(int x1, int y1, int x2, int y2))
GL_PROC(_GL,void,glRectiv,(const int *v1, const int *v2))
GL_PROC(_GL,void,glRects,(short x1, short y1, short x2, short y2))
GL_PROC(_GL,void,glRectsv,(const short *v1, const short *v2))
GL_PROC(_GL,int,glRenderMode,(unsigned int mode))
GL_PROC(_GL,void,glRotated,(double angle, double x, double y, double z))
GL_PROC(_GL,void,glRotatef,(float angle, float x, float y, float z))
GL_PROC(_GL,void,glScaled,(double x, double y, double z))
GL_PROC(_GL,void,glScalef,(float x, float y, float z))
GL_PROC(_GL,void,glScissor,(int x, int y, int width, int height))
GL_PROC(_GL,void,glSelectBuffer,(int size, unsigned int *buffer))
GL_PROC(_GL,void,glShadeModel,(unsigned int mode))
GL_PROC(_GL,void,glStencilFunc,(unsigned int func, int ref, unsigned int mask))
GL_PROC(_GL,void,glStencilMask,(unsigned int mask))
GL_PROC(_GL,void,glStencilOp,(unsigned int fail, unsigned int zfail, unsigned int zpass))
GL_PROC(_GL,void,glTexCoord1d,(double s))
GL_PROC(_GL,void,glTexCoord1dv,(const double *v))
GL_PROC(_GL,void,glTexCoord1f,(float s))
GL_PROC(_GL,void,glTexCoord1fv,(const float *v))
GL_PROC(_GL,void,glTexCoord1i,(int s))
GL_PROC(_GL,void,glTexCoord1iv,(const int *v))
GL_PROC(_GL,void,glTexCoord1s,(short s))
GL_PROC(_GL,void,glTexCoord1sv,(const short *v))
GL_PROC(_GL,void,glTexCoord2d,(double s, double t))
GL_PROC(_GL,void,glTexCoord2dv,(const double *v))
GL_PROC(_GL,void,glTexCoord2f,(float s, float t))
GL_PROC(_GL,void,glTexCoord2fv,(const float *v))
GL_PROC(_GL,void,glTexCoord2i,(int s, int t))
GL_PROC(_GL,void,glTexCoord2iv,(const int *v))
GL_PROC(_GL,void,glTexCoord2s,(short s, short t))
GL_PROC(_GL,void,glTexCoord2sv,(const short *v))
GL_PROC(_GL,void,glTexCoord3d,(double s, double t, double r))
GL_PROC(_GL,void,glTexCoord3dv,(const double *v))
GL_PROC(_GL,void,glTexCoord3f,(float s, float t, float r))
GL_PROC(_GL,void,glTexCoord3fv,(const float *v))
GL_PROC(_GL,void,glTexCoord3i,(int s, int t, int r))
GL_PROC(_GL,void,glTexCoord3iv,(const int *v))
GL_PROC(_GL,void,glTexCoord3s,(short s, short t, short r))
GL_PROC(_GL,void,glTexCoord3sv,(const short *v))
GL_PROC(_GL,void,glTexCoord4d,(double s, double t, double r, double q))
GL_PROC(_GL,void,glTexCoord4dv,(const double *v))
GL_PROC(_GL,void,glTexCoord4f,(float s, float t, float r, float q))
GL_PROC(_GL,void,glTexCoord4fv,(const float *v))
GL_PROC(_GL,void,glTexCoord4i,(int s, int t, int r, int q))
GL_PROC(_GL,void,glTexCoord4iv,(const int *v))
GL_PROC(_GL,void,glTexCoord4s,(short s, short t, short r, short q))
GL_PROC(_GL,void,glTexCoord4sv,(const short *v))
GL_PROC(_GL,void,glTexCoordPointer,(int size, unsigned int type, int stride, const void *pointer))
GL_PROC(_GL,void,glTexEnvf,(unsigned int target, unsigned int pname, float param))
GL_PROC(_GL,void,glTexEnvfv,(unsigned int target, unsigned int pname, const float *params))
GL_PROC(_GL,void,glTexEnvi,(unsigned int target, unsigned int pname, int param))
GL_PROC(_GL,void,glTexEnviv,(unsigned int target, unsigned int pname, const int *params))
GL_PROC(_GL,void,glTexGend,(unsigned int coord, unsigned int pname, double param))
GL_PROC(_GL,void,glTexGendv,(unsigned int coord, unsigned int pname, const double *params))
GL_PROC(_GL,void,glTexGenf,(unsigned int coord, unsigned int pname, float param))
GL_PROC(_GL,void,glTexGenfv,(unsigned int coord, unsigned int pname, const float *params))
GL_PROC(_GL,void,glTexGeni,(unsigned int coord, unsigned int pname, int param))
GL_PROC(_GL,void,glTexGeniv,(unsigned int coord, unsigned int pname, const int *params))
GL_PROC(_GL,void,glTexImage1D,(unsigned int target, int level, int internalformat, int width, int border, unsigned int format, unsigned int type, const void *pixels))
GL_PROC(_GL,void,glTexImage2D,(unsigned int target, int level, int internalformat, int width, int height, int border, unsigned int format, unsigned int type, const void *pixels))
GL_PROC(_GL,void,glTexParameterf,(unsigned int target, unsigned int pname, float param))
GL_PROC(_GL,void,glTexParameterfv,(unsigned int target, unsigned int pname, const float *params))
GL_PROC(_GL,void,glTexParameteri,(unsigned int target, unsigned int pname, int param))
GL_PROC(_GL,void,glTexParameteriv,(unsigned int target, unsigned int pname, const int *params))
GL_PROC(_GL,void,glTexSubImage1D,(unsigned int target, int level, int xoffset, int width, unsigned int format, unsigned int type, const void *pixels))
GL_PROC(_GL,void,glTexSubImage2D,(unsigned int target, int level, int xoffset, int yoffset, int width, int height, unsigned int format, unsigned int type, const void *pixels))
GL_PROC(_GL,void,glTranslated,(double x, double y, double z))
GL_PROC(_GL,void,glTranslatef,(float x, float y, float z))
GL_PROC(_GL,void,glVertex2d,(double x, double y))
GL_PROC(_GL,void,glVertex2dv,(const double *v))
GL_PROC(_GL,void,glVertex2f,(float x, float y))
GL_PROC(_GL,void,glVertex2fv,(const float *v))
GL_PROC(_GL,void,glVertex2i,(int x, int y))
GL_PROC(_GL,void,glVertex2iv,(const int *v))
GL_PROC(_GL,void,glVertex2s,(short x, short y))
GL_PROC(_GL,void,glVertex2sv,(const short *v))
GL_PROC(_GL,void,glVertex3d,(double x, double y, double z))
GL_PROC(_GL,void,glVertex3dv,(const double *v))
GL_PROC(_GL,void,glVertex3f,(float x, float y, float z))
GL_PROC(_GL,void,glVertex3fv,(const float *v))
GL_PROC(_GL,void,glVertex3i,(int x, int y, int z))
GL_PROC(_GL,void,glVertex3iv,(const int *v))
GL_PROC(_GL,void,glVertex3s,(short x, short y, short z))
GL_PROC(_GL,void,glVertex3sv,(const short *v))
GL_PROC(_GL,void,glVertex4d,(double x, double y, double z, double w))
GL_PROC(_GL,void,glVertex4dv,(const double *v))
GL_PROC(_GL,void,glVertex4f,(float x, float y, float z, float w))
GL_PROC(_GL,void,glVertex4fv,(const float *v))
GL_PROC(_GL,void,glVertex4i,(int x, int y, int z, int w))
GL_PROC(_GL,void,glVertex4iv,(const int *v))
GL_PROC(_GL,void,glVertex4s,(short x, short y, short z, short w))
GL_PROC(_GL,void,glVertex4sv,(const short *v))
GL_PROC(_GL,void,glVertexPointer,(int size, unsigned int type, int stride, const void *pointer))
GL_PROC(_GL,void,glViewport,(int x, int y, int width, int height))

#ifdef _WIN32
// WGL functions.
GL_PROC(_GL,BOOL,wglCopyContext,(HGLRC,HGLRC,UINT))
GL_PROC(_GL,HGLRC,wglCreateContext,(HDC))
GL_PROC(_GL,HGLRC,wglCreateLayerContext,(HGLRC))
GL_PROC(_GL,BOOL,wglDeleteContext,(HGLRC))
GL_PROC(_GL,HGLRC,wglGetCurrentContext,(VOID))
GL_PROC(_GL,HDC,wglGetCurrentDC,(VOID))
GL_PROC(_GL,PROC,wglGetProcAddress,(LPCSTR))
GL_PROC(_GL,BOOL,wglMakeCurrent,(HDC, HGLRC))
GL_PROC(_GL,BOOL,wglShareLists,(HGLRC,HGLRC))

// GDI functions.
GL_PROC(_GL,INT,ChoosePixelFormat,(HDC hDC,CONST PIXELFORMATDESCRIPTOR*))
GL_PROC(_GL,INT,DescribePixelFormat,(HDC,INT,UINT,PIXELFORMATDESCRIPTOR*))
GL_PROC(_GL,BOOL,GetPixelFormat,(HDC))
GL_PROC(_GL,BOOL,SetPixelFormat,(HDC,INT,CONST PIXELFORMATDESCRIPTOR*))
GL_PROC(_GL,BOOL,SwapBuffers,(HDC hDC))
#endif // _WIN32

#endif


/*-----------------------------------------------------------------------------
	OpenGL extensions.
-----------------------------------------------------------------------------*/

// BGRA textures.
GL_EXT(_GL_EXT_bgra)

// Paletted textures.
GL_EXT(_GL_EXT_paletted_texture)
GL_PROC(_GL_EXT_paletted_texture,void,glColorTableEXT,(unsigned int target,unsigned int internalFormat,int width,unsigned int format,unsigned int type,const void *data))
GL_PROC(_GL_EXT_paletted_texture,void,glColorSubTableEXT,(unsigned int target,int start,int count,unsigned int format,unsigned int type,const void *data))
GL_PROC(_GL_EXT_paletted_texture,void,glGetColorTableEXT,(unsigned int target,unsigned int format,unsigned int type,void *data))
GL_PROC(_GL_EXT_paletted_texture,void,glGetColorTableParameterivEXT,(unsigned int target,unsigned int pname,int *params))
GL_PROC(_GL_EXT_paletted_texture,void,glGetColorTableParameterfvEXT,(unsigned int target,unsigned int pname,float *params))

// S3TC texture compression.
GL_EXT(_GL_S3_s3tc)

#ifdef _WIN32
// Swap interval control.
GL_EXT(_WGL_EXT_swap_control)
GL_PROC(_WGL_EXT_swap_control,BOOL,wglSwapIntervalEXT,(int))
#endif // _WIN32

// Windows swap control.
GL_EXT(_GL_WIN_swap_hint)

// ARB multitexture.
GL_EXT(_GL_ARB_multitexture)
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord1fARB,(unsigned int target,float))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord2fARB,(unsigned int target,float,float))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord3fARB,(unsigned int target,float,float,float))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord4fARB,(unsigned int target,float,float,float,float))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord1fvARB,(unsigned int target,float))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord2fvARB,(unsigned int target,float,float))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord3fvARB,(unsigned int target,float,float,float))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord4fvARB,(unsigned int target,float,float,float,float))
GL_PROC(_GL_ARB_multitexture,void,glActiveTextureARB,(unsigned int target))
GL_PROC(_GL_ARB_multitexture,void,glClientActiveTextureARB,(unsigned int target))
#define GL_ACTIVE_TEXTURE_ARB               0x84E0
#define GL_CLIENT_ACTIVE_TEXTURE_ARB        0x84E1
#define GL_MAX_TEXTURES_UNITS_ARB           0x84E2
#define GL_TEXTURE0_ARB                     0x84C0
#define GL_TEXTURE1_ARB                     0x84C1
#define GL_TEXTURE2_ARB                     0x84C2
#define GL_TEXTURE3_ARB                     0x84C3

GL_EXT(_GL_ARB_texture_env_combine)
#define GL_COMBINE_ARB			0x8570

#define GL_COMBINE_RGB_ARB		0x8571
#define GL_COMBINE_ALPHA_ARB	0x8572

#define GL_SOURCE0_RGB_ARB		0x8580
#define GL_SOURCE1_RGB_ARB		0x8581
#define GL_SOURCE2_RGB_ARB		0x8582
#define GL_SOURCE0_ALPHA_ARB	0x8588
#define GL_SOURCE1_ALPHA_ARB	0x8589
#define GL_SOURCE2_ALPHA_ARB	0x858A

#define GL_OPERAND0_RGB_ARB		0x8590
#define GL_OPERAND1_RGB_ARB		0x8591
#define GL_OPERAND2_RGB_ARB		0x8592
#define GL_OPERAND0_ALPHA_ARB	0x8598
#define GL_OPERAND1_ALPHA_ARB	0x8599
#define GL_OPERAND2_ALPHA_ARB	0x859A

#define GL_RGB_SCALE_ARB		0x8573
#define GL_ADD_SIGNED_ARB		0x8574
#define GL_INTERPOLATE_ARB		0x8575
#define GL_SUBTRACT_ARB			0x84E7
#define GL_CONSTANT_ARB			0x8576
#define GL_PRIMARY_COLOR_ARB	0x8577
#define GL_PREVIOUS_ARB			0x8578

GL_EXT(_GL_EXT_texture_env_combine)
#define GL_COMBINE_EXT			0x8570
#define GL_COMBINE_RGB_EXT		0x8571
#define GL_COMBINE_ALPHA_EXT	0x8572
#define GL_SOURCE0_RGB_EXT		0x8580
#define GL_SOURCE1_RGB_EXT		0x8581
#define GL_SOURCE2_RGB_EXT		0x8582
#define GL_SOURCE0_ALPHA_EXT	0x8588
#define GL_SOURCE1_ALPHA_EXT	0x8589
#define GL_SOURCE2_ALPHA_EXT	0x858A
#define GL_OPERAND0_RGB_EXT		0x8590
#define GL_OPERAND1_RGB_EXT		0x8591
#define GL_OPERAND2_RGB_EXT		0x8592
#define GL_OPERAND0_ALPHA_EXT	0x8598
#define GL_OPERAND1_ALPHA_EXT	0x8599
#define GL_OPERAND2_ALPHA_EXT	0x859A
#define GL_RGB_SCALE_EXT		0x8573
#define GL_ADD_SIGNED_EXT		0x8574
#define GL_INTERPOLATE_EXT		0x8575
#define GL_CONSTANT_EXT			0x8576
#define GL_PRIMARY_COLOR_EXT	0x8577
#define GL_PREVIOUS_EXT			0x8578

GL_EXT(_GL_NV_texture_env_combine4)
#define GL_COMBINE4_NV 0x8503
#define GL_SOURCE3_RGB_NV 0x8583
#define GL_SOURCE3_ALPHA_NV 0x858B
#define GL_OPERAND3_RGB_NV 0x8593
#define GL_OPERAND3_ALPHA_NV 0x859B

GL_EXT(_GL_ARB_texture_env_dot3)
#define GL_DOT3_RGB_ARB			0x86AE
#define GL_DOT3_RGBA_ARB		0x86AF

GL_EXT(_GL_ARB_texture_env_add)

GL_EXT(_GL_ARB_texture_compression)
GL_PROC(_GL_ARB_texture_compression,void,glCompressedTexImage3DARB,(unsigned int,int,unsigned int,int,int,int,int,int,const void *))
GL_PROC(_GL_ARB_texture_compression,void,glCompressedTexImage2DARB,(unsigned int,int,unsigned int,int,int,int,int,const void *))
GL_PROC(_GL_ARB_texture_compression,void,glCompressedTexImage1DARB,(unsigned int,int,unsigned int,int,int,int,const void *))
GL_PROC(_GL_ARB_texture_compression,void,glCompressedTexSubImage3DARB,(unsigned int,int,int,int,int,int,int,int,unsigned int,int,const void *))
GL_PROC(_GL_ARB_texture_compression,void,glCompressedTexSubImage2DARB,(unsigned int,int,int,int,int,int,unsigned int,int,const void *))
GL_PROC(_GL_ARB_texture_compression,void,glCompressedTexSubImage1DARB,(unsigned int,int,int,int,unsigned int,int,const void *))
GL_PROC(_GL_ARB_texture_compression,void,glGetCompressedTexImageARB,(unsigned int,int,void *))
#define GL_COMPRESSED_ALPHA_ARB					0x84E9
#define GL_COMPRESSED_LUMINANCE_ARB				0x84EA
#define GL_COMPRESSED_LUMINANCE_ALPHA_ARB		0x84EB
#define GL_COMPRESSED_INTENSITY_ARB				0x84EC
#define GL_COMPRESSED_RGB_ARB					0x84ED
#define GL_COMPRESSED_RGBA_ARB					0x84EE
#define GL_TEXTURE_COMPRESSION_HINT_ARB			0x84EF
#define GL_TEXTURE_COMPRESSED_IMAGE_SIZE_ARB	0x86A0
#define GL_TEXTURE_COMPRESSED_ARB				0x86A1
#define GL_NUM_COMPRESSED_TEXTURE_FORMATS_ARB	0x86A2
#define GL_COMPRESSED_TEXTURE_FORMATS_ARB		0x86A3

GL_EXT(_GL_EXT_texture_compression_s3tc)
#define GL_COMPRESSED_RGB_S3TC_DXT1_EXT 0x83F0
#define GL_COMPRESSED_RGBA_S3TC_DXT1_EXT 0x83F1
#define GL_COMPRESSED_RGBA_S3TC_DXT3_EXT 0x83F2
#define GL_COMPRESSED_RGBA_S3TC_DXT5_EXT 0x83F3

GL_EXT(_GL_EXT_compiled_vertex_array)
GL_PROC(_GL_EXT_compiled_vertex_array,void,glLockArraysEXT,(int,int))
GL_PROC(_GL_EXT_compiled_vertex_array,void,glUnlockArraysEXT,(void))
#define GL_ARRAY_ELEMENT_LOCK_FIRST_EXT		0x81A8
#define GL_ARRAY_ELEMENT_LOCK_COUNT_EXT		0x81A9

GL_EXT(_GL_EXT_packed_pixels)
#define GL_UNSIGNED_BYTE_3_3_2_EXT			0x8032
#define GL_UNSIGNED_SHORT_4_4_4_4_EXT		0x8033
#define GL_UNSIGNED_SHORT_5_5_5_1_EXT		0x8034
#define GL_UNSIGNED_INT_8_8_8_8_EXT			0x8035
#define GL_UNSIGNED_INT_10_10_10_2_EXT		0x8036

GL_EXT(_GL_NV_fence)
GL_PROC(_GL_NV_fence,void,glGenFencesNV,(int,unsigned int *))
GL_PROC(_GL_NV_fence,void,glDeleteFencesNV,(int,const unsigned int *))
GL_PROC(_GL_NV_fence,void,glSetFenceNV,(unsigned int, unsigned int))
GL_PROC(_GL_NV_fence,unsigned char,glTestFenceNV,(unsigned int fence))
GL_PROC(_GL_NV_fence,void,glFinishFenceNV,(unsigned int))
GL_PROC(_GL_NV_fence,unsigned char,glIsFenceNV,(unsigned int))
GL_PROC(_GL_NV_fence,void,glGetFenceivNV,(unsigned int,unsigned int,int *))
#define GL_ALL_COMPLETED_NV			0x84F2
#define GL_FENCE_STATUS_NV			0x84F3
#define GL_FENCE_CONDITION_NV		0x84F4

GL_EXT(_GL_NV_packed_depth_stencil)
#define GL_DEPTH_STENCIL_NV			0x84F9
#define GL_UNSIGNED_INT_24_8_NV		0x84FA

GL_EXT(_GL_NV_register_combiners)
GL_PROC(_GL_NV_register_combiners,void,glCombinerParameterfvNV,(unsigned int,const float *))
GL_PROC(_GL_NV_register_combiners,void,glCombinerParameterivNV,(unsigned int pname,const int *params))
GL_PROC(_GL_NV_register_combiners,void,glCombinerParameterfNV,(unsigned int pname,float param))
GL_PROC(_GL_NV_register_combiners,void,glCombinerParameteriNV,(unsigned int pname,int param))
GL_PROC(_GL_NV_register_combiners,void,glCombinerInputNV,(unsigned int stage,unsigned int portion,unsigned int variable,unsigned int input,unsigned int mapping,unsigned int componentUsage))
GL_PROC(_GL_NV_register_combiners,void,glCombinerOutputNV,(unsigned int stage,unsigned int portion,unsigned int abOutput,unsigned int cdOutput,unsigned int sumOutput,unsigned int scale,unsigned int bias,unsigned char abDotProduct,unsigned char cdDotProduct,unsigned char muxSum))
GL_PROC(_GL_NV_register_combiners,void,glFinalCombinerInputNV,(unsigned int variable,unsigned int input,unsigned int mapping,unsigned int componentUsage))
GL_PROC(_GL_NV_register_combiners,void,glGetCombinerInputParameterfvNV,(unsigned int stage,unsigned int portion,unsigned int variable,unsigned int pname,float *params))
GL_PROC(_GL_NV_register_combiners,void,glGetCombinerInputParameterivNV,(unsigned int stage,unsigned int portion,unsigned int variable,unsigned int pname,int *params))
GL_PROC(_GL_NV_register_combiners,void,glGetCombinerOutputParameterfvNV,(unsigned int stage,unsigned int portion,unsigned int pname,float *params))
GL_PROC(_GL_NV_register_combiners,void,glGetCombinerOutputParameterivNV,(unsigned int stage,unsigned int portion,unsigned int pname,int *params))
GL_PROC(_GL_NV_register_combiners,void,glGetFinalCombinerInputParameterfvNV,(unsigned int variable,unsigned int pname,float *params))
GL_PROC(_GL_NV_register_combiners,void,glGetFinalCombinerInputParameterivNV,(unsigned int variable,unsigned int pname,float *params))

#define GL_REGISTER_COMBINERS_NV		0x8522
#define GL_COMBINER0_NV					0x8550
#define GL_COMBINER1_NV					0x8551
#define GL_COMBINER2_NV					0x8552
#define GL_COMBINER3_NV					0x8553
#define GL_COMBINER4_NV					0x8554
#define GL_COMBINER5_NV					0x8555
#define GL_COMBINER6_NV					0x8556
#define GL_COMBINER7_NV					0x8557
#define GL_VARIABLE_A_NV				0x8523
#define GL_VARIABLE_B_NV				0x8524
#define GL_VARIABLE_C_NV				0x8525
#define GL_VARIABLE_D_NV				0x8526
#define GL_VARIABLE_E_NV				0x8527
#define GL_VARIABLE_F_NV				0x8528
#define GL_VARIABLE_G_NV				0x8529
#define GL_CONSTANT_COLOR0_NV 0x852A
#define GL_CONSTANT_COLOR1_NV 0x852B
#define GL_PRIMARY_COLOR_NV 0x852C
#define GL_SECONDARY_COLOR_NV 0x852D
#define GL_SPARE0_NV 0x852E
#define GL_SPARE1_NV 0x852F
#define GL_UNSIGNED_IDENTITY_NV 0x8536
#define GL_UNSIGNED_INVERT_NV 0x8537
#define GL_EXPAND_NORMAL_NV 0x8538
#define GL_EXPAND_NEGATE_NV 0x8539
#define GL_HALF_BIAS_NORMAL_NV 0x853A
#define GL_HALF_BIAS_NEGATE_NV 0x853B
#define GL_SIGNED_IDENTITY_NV 0x853C
#define GL_SIGNED_NEGATE_NV 0x853D
#define GL_E_TIMES_F_NV 0x8531
#define GL_SPARE0_PLUS_SECONDARY_COLOR_NV 0x8532
#define GL_SCALE_BY_TWO_NV 0x853E
#define GL_SCALE_BY_FOUR_NV 0x853F
#define GL_SCALE_BY_ONE_HALF_NV 0x8540
#define GL_BIAS_BY_NEGATIVE_ONE_HALF_NV 0x8541
#define GL_DISCARD_NV 0x8530
#define GL_COMBINER_INPUT_NV 0x8542
#define GL_COMBINER_MAPPING_NV 0x8543
#define GL_COMBINER_COMPONENT_USAGE_NV 0x8544
#define GL_COMBINER_AB_DOT_PRODUCT_NV 0x8545
#define GL_COMBINER_CD_DOT_PRODUCT_NV 0x8546
#define GL_COMBINER_MUX_SUM_NV 0x8547
#define GL_COMBINER_SCALE_NV 0x8548
#define GL_COMBINER_BIAS_NV 0x8549
#define GL_COMBINER_AB_OUTPUT_NV 0x854A
#define GL_COMBINER_CD_OUTPUT_NV 0x854B
#define GL_COMBINER_SUM_OUTPUT_NV 0x854C
#define GL_NUM_GENERAL_COMBINERS_NV 0x854E
#define GL_COLOR_SUM_CLAMP_NV 0x854F
#define GL_MAX_GENERAL_COMBINERS_NV 0x854D

GL_EXT(_GL_NV_register_combiners2)
GL_PROC(_GL_NV_register_combiners2,void,glCombinerStageParameterfvNV,(unsigned int stage,unsigned int pname,const float *params))
GL_PROC(_GL_NV_register_combiners2,void,glGetCombinerStageParameterfvNV,(unsigned int stage,unsigned int pname,float *params))
#define GL_PER_STAGE_CONSTANTS_NV 0x8535

GL_EXT(_GL_NV_texture_shader)
#define GL_TEXTURE_SHADER_NV 0x86DE
#define GL_RGBA_UNSIGNED_DOT_PRODUCT_MAPPING_NV 0x86D9
#define GL_SHADER_OPERATION_NV 0x86DF
#define GL_CULL_MODES_NV 0x86E0
#define GL_OFFSET_TEXTURE_MATRIX_NV 0x86E1
#define GL_OFFSET_TEXTURE_SCALE_NV 0x86E2
#define GL_OFFSET_TEXTURE_BIAS_NV 0x86E3
#define GL_OFFSET_TEXTURE_2D_MATRIX_NV OFFSET_TEXTURE_MATRIX_NV
#define GL_OFFSET_TEXTURE_2D_SCALE_NV OFFSET_TEXTURE_SCALE_NV
#define GL_OFFSET_TEXTURE_2D_BIAS_NV OFFSET_TEXTURE_BIAS_NV
#define GL_PREVIOUS_TEXTURE_INPUT_NV 0x86E4
#define GL_CONST_EYE_NV 0x86E5
#define GL_SHADER_CONSISTENT_NV 0x86DD
#define GL_PASS_THROUGH_NV 0x86E6
#define GL_CULL_FRAGMENT_NV 0x86E7
#define GL_OFFSET_TEXTURE_2D_NV 0x86E8
#define GL_OFFSET_TEXTURE_RECTANGLE_NV 0x864C
#define GL_OFFSET_TEXTURE_RECTANGLE_SCALE_NV 0x864D
#define GL_DEPENDENT_AR_TEXTURE_2D_NV 0x86E9
#define GL_DEPENDENT_GB_TEXTURE_2D_NV 0x86EA
#define GL_DOT_PRODUCT_NV 0x86EC
#define GL_DOT_PRODUCT_DEPTH_REPLACE_NV 0x86ED
#define GL_DOT_PRODUCT_TEXTURE_2D_NV 0x86EE
#define GL_DOT_PRODUCT_TEXTURE_RECTANGLE_NV 0x864E
#define GL_DOT_PRODUCT_TEXTURE_CUBE_MAP_NV 0x86F0
#define GL_DOT_PRODUCT_DIFFUSE_CUBE_MAP_NV 0x86F1
#define GL_DOT_PRODUCT_REFLECT_CUBE_MAP_NV 0x86F2
#define GL_DOT_PRODUCT_CONST_EYE_REFLECT_CUBE_MAP_NV 0x86F3
#define GL_HILO_NV 0x86F4
#define GL_DSDT_NV 0x86F5
#define GL_DSDT_MAG_NV 0x86F6
#define GL_DSDT_MAG_VIB_NV 0x86F7
#define GL_UNSIGNED_INT_S8_S8_8_8_NV 0x86DA
#define GL_UNSIGNED_INT_8_8_S8_S8_REV_NV 0x86DB
#define GL_SIGNED_RGBA_NV 0x86FB
#define GL_SIGNED_RGBA8_NV 0x86FC
#define GL_SIGNED_RGB_NV 0x86FE
#define GL_SIGNED_RGB8_NV 0x86FF
#define GL_SIGNED_LUMINANCE_NV 0x8701
#define GL_SIGNED_LUMINANCE8_NV 0x8702
#define GL_SIGNED_LUMINANCE_ALPHA_NV 0x8703
#define GL_SIGNED_LUMINANCE8_ALPHA8_NV 0x8704
#define GL_SIGNED_ALPHA_NV 0x8705
#define GL_SIGNED_ALPHA8_NV 0x8706
#define GL_SIGNED_INTENSITY_NV 0x8707
#define GL_SIGNED_INTENSITY8_NV 0x8708
#define GL_SIGNED_RGB_UNSIGNED_ALPHA_NV 0x870C
#define GL_SIGNED_RGB8_UNSIGNED_ALPHA8_NV 0x870D
#define GL_HILO16_NV 0x86F8
#define GL_SIGNED_HILO_NV 0x86F9
#define GL_SIGNED_HILO16_NV 0x86FA
#define GL_DSDT8_NV 0x8709
#define GL_DSDT8_MAG8_NV 0x870A
#define GL_DSDT_MAG_INTENSITY_NV 0x86DC
#define GL_DSDT8_MAG8_INTENSITY8_NV 0x870B
#define GL_HI_SCALE_NV 0x870E
#define GL_LO_SCALE_NV 0x870F
#define GL_DS_SCALE_NV 0x8710
#define GL_DT_SCALE_NV 0x8711
#define GL_MAGNITUDE_SCALE_NV 0x8712
#define GL_VIBRANCE_SCALE_NV 0x8713
#define GL_HI_BIAS_NV 0x8714
#define GL_LO_BIAS_NV 0x8715
#define GL_DS_BIAS_NV 0x8716
#define GL_DT_BIAS_NV 0x8717
#define GL_MAGNITUDE_BIAS_NV 0x8718
#define GL_VIBRANCE_BIAS_NV 0x8719
#define GL_TEXTURE_BORDER_VALUES_NV 0x871A
#define GL_TEXTURE_HI_SIZE_NV 0x871B
#define GL_TEXTURE_LO_SIZE_NV 0x871C
#define GL_TEXTURE_DS_SIZE_NV 0x871D
#define GL_TEXTURE_DT_SIZE_NV 0x871E
#define GL_TEXTURE_MAG_SIZE_NV 0x871F

GL_EXT(_GL_NV_texture_shader2)
#define GL_DOT_PRODUCT_TEXTURE_3D_NV 0x86EF
#define GL_HILO_NV 0x86F4
#define GL_DSDT_NV 0x86F5
#define GL_DSDT_MAG_NV 0x86F6
#define GL_DSDT_MAG_VIB_NV 0x86F7
#define GL_UNSIGNED_INT_S8_S8_8_8_NV 0x86DA
#define GL_UNSIGNED_INT_8_8_S8_S8_REV_NV 0x86DB
#define GL_SIGNED_RGBA_NV 0x86FB
#define GL_SIGNED_RGBA8_NV 0x86FC
#define GL_SIGNED_RGB_NV 0x86FE
#define GL_SIGNED_RGB8_NV 0x86FF
#define GL_SIGNED_LUMINANCE_NV 0x8701
#define GL_SIGNED_LUMINANCE8_NV 0x8702
#define GL_SIGNED_LUMINANCE_ALPHA_NV 0x8703
#define GL_SIGNED_LUMINANCE8_ALPHA8_NV 0x8704
#define GL_SIGNED_ALPHA_NV 0x8705
#define GL_SIGNED_ALPHA8_NV 0x8706
#define GL_SIGNED_INTENSITY_NV 0x8707
#define GL_SIGNED_INTENSITY8_NV 0x8708
#define GL_SIGNED_RGB_UNSIGNED_ALPHA_NV 0x870C
#define GL_SIGNED_RGB8_UNSIGNED_ALPHA8_NV 0x870D
#define GL_HILO16_NV 0x86F8
#define GL_SIGNED_HILO_NV 0x86F9
#define GL_SIGNED_HILO16_NV 0x86FA
#define GL_DSDT8_NV 0x8709
#define GL_DSDT8_MAG8_NV 0x870A
#define GL_DSDT_MAG_INTENSITY_NV 0x86DC
#define GL_DSDT8_MAG8_INTENSITY8_NV 0x870B

#ifdef _WIN32
GL_EXT(_GL_NV_vertex_array_range)
GL_PROC(_GL_NV_vertex_array_range,void,glVertexArrayRangeNV,(int length, void *pointer))
GL_PROC(_GL_NV_vertex_array_range,void,glFlushVertexArrayRangeNV,(void))
GL_PROC(_GL_NV_vertex_array_range,void*,wglAllocateMemoryNV,(int size, float readfreq, float writefreq, float priority))
GL_PROC(_GL_NV_vertex_array_range,void,wglFreeMemoryNV,(void *pointer))
#define GL_VERTEX_ARRAY_RANGE_NV 0x851D
#define GL_VERTEX_ARRAY_RANGE_LENGTH_NV 0x851E
#define GL_VERTEX_ARRAY_RANGE_VALID_NV 0x851F
#define GL_MAX_VERTEX_ARRAY_RANGE_ELEMENT_NV 0x8520
#define GL_VERTEX_ARRAY_RANGE_POINTER_NV 0x8521
#endif // WIN32

GL_EXT(_GL_NV_vertex_array_range2)
#define GL_VERTEX_ARRAY_RANGE_WITHOUT_FLUSH_NV 0x8533

GL_EXT(_GL_NV_vertex_program)
GL_PROC(_GL_NV_vertex_program,void,glBindProgramNV,(unsigned int target, unsigned int id))
GL_PROC(_GL_NV_vertex_program,void,glDeleteProgramsNV,(int n, const unsigned int *ids))
GL_PROC(_GL_NV_vertex_program,void,glExecuteProgramNV,(unsigned int target, unsigned int id, const float *params))
GL_PROC(_GL_NV_vertex_program,void,glGenProgramsNV,(int n, unsigned int *ids))
GL_PROC(_GL_NV_vertex_program,unsigned char,glAreProgramsResidentNV,(int n, const unsigned int *ids,unsigned char *residences))
GL_PROC(_GL_NV_vertex_program,void,glRequestResidentProgramsNV,(int n, unsigned int *ids))
GL_PROC(_GL_NV_vertex_program,void,glGetProgramParameterfvNV,(unsigned int target, unsigned int index,unsigned int pname, float *params))
GL_PROC(_GL_NV_vertex_program,void,glGetProgramParameterdvNV,(unsigned int target, unsigned int index,unsigned int pname, double *params))
GL_PROC(_GL_NV_vertex_program,void,glGetProgramivNV,(unsigned int id, unsigned int pname, int *params))
GL_PROC(_GL_NV_vertex_program,void,glGetProgramStringNV,(unsigned int id, unsigned int pname, unsigned char *program))
GL_PROC(_GL_NV_vertex_program,void,glGetTrackMatrixivNV,(unsigned int target, unsigned int address,unsigned int pname, int *params))
GL_PROC(_GL_NV_vertex_program,void,glGetVertexAttribdvNV,(unsigned int index, unsigned int pname, double *params))
GL_PROC(_GL_NV_vertex_program,void,glGetVertexAttribfvNV,(unsigned int index, unsigned int pname, float *params))
GL_PROC(_GL_NV_vertex_program,void,glGetVertexAttribivNV,(unsigned int index, unsigned int pname, int *params))
GL_PROC(_GL_NV_vertex_program,void,glGetVertexAttribPointervNV,(unsigned int index, unsigned int pname, void **pointer))
GL_PROC(_GL_NV_vertex_program,unsigned char,glIsProgramNV,(unsigned int id))
GL_PROC(_GL_NV_vertex_program,void,glLoadProgramNV,(unsigned int target, unsigned int id, int len,const unsigned char *program))
GL_PROC(_GL_NV_vertex_program,void,glProgramParameter4fNV,(unsigned int target, unsigned int index,float x, float y, float z, float w))
GL_PROC(_GL_NV_vertex_program,void,glProgramParameter4dNV,(unsigned int target, unsigned int index,double x, double y, double z, double w))
GL_PROC(_GL_NV_vertex_program,void,glProgramParameter4dvNV,(unsigned int target, unsigned int index,const double *params))
GL_PROC(_GL_NV_vertex_program,void,glProgramParameter4fvNV,(unsigned int target, unsigned int index,const float *params))
GL_PROC(_GL_NV_vertex_program,void,glProgramParameters4dvNV,(unsigned int target, unsigned int index,unsigned int num, const double *params))
GL_PROC(_GL_NV_vertex_program,void,glProgramParameters4fvNV,(unsigned int target, unsigned int index,unsigned int num, const float *params))
GL_PROC(_GL_NV_vertex_program,void,glTrackMatrixNV,(unsigned int target, unsigned int address,unsigned int matrix, unsigned int transform))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribPointerNV,(unsigned int index, int size, unsigned int type, int stride,const void **pointer))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib1sNV,(unsigned int index, short x))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib1fNV,(unsigned int index, float x))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib1dNV,(unsigned int index, double x))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib2sNV,(unsigned int index, short x, short y))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib2fNV,(unsigned int index, float x, float y))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib2dNV,(unsigned int index, double x, double y))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib3sNV,(unsigned int index, short x, short y, short z))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib3fNV,(unsigned int index, float x, float y, float z))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib3dNV,(unsigned int index, double x, double y, double z))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib4sNV,(unsigned int index, short x, short y, short z, short w))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib4fNV,(unsigned int index, float x, float y, float z, float w))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib4dNV,(unsigned int index, double x, double y, double z, double w))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib4ubNV,(unsigned int index, unsigned char x, unsigned char y, unsigned char z, unsigned char w))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib1svNV,(unsigned int index, const short *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib1fvNV,(unsigned int index, const float *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib1dvNV,(unsigned int index, const double *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib2svNV,(unsigned int index, const short *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib2fvNV,(unsigned int index, const float *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib2dvNV,(unsigned int index, const double *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib3svNV,(unsigned int index, const short *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib3fvNV,(unsigned int index, const float *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib3dvNV,(unsigned int index, const double *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib4svNV,(unsigned int index, const short *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib4fvNV,(unsigned int index, const float *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttrib4ubvNV,(unsigned int index, const unsigned char *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs1svNV,(unsigned int index, int n, const short *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs1fvNV,(unsigned int index, int n, const float *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs1dvNV,(unsigned int index, int n, const double *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs2svNV,(unsigned int index, int n, const short *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs2fvNV,(unsigned int index, int n, const float *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs2dvNV,(unsigned int index, int n, const double *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs3svNV,(unsigned int index, int n, const short *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs3fvNV,(unsigned int index, int n, const float *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs3dvNV,(unsigned int index, int n, const double *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs4svNV,(unsigned int index, int n, const short *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs4fvNV,(unsigned int index, int n, const float *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs4dvNV,(unsigned int index, int n, const double *v))
GL_PROC(_GL_NV_vertex_program,void,glVertexAttribs4ubvNV,(unsigned int index, int n, const unsigned char *v))


GL_EXT(_GL_EXT_draw_range_elements)
#define GL_MAX_ELEMENTS_VERTICES_EXT	0x80E8
#define GL_MAX_ELEMENTS_INDICES_EXT		0x80E9
GL_PROC(_GL_EXT_draw_range_elements,void,glDrawRangeElementsEXT,(unsigned int mode,unsigned int start,unsigned int end,int count,unsigned int type,const void *indices))

GL_EXT(_GL_EXT_multi_draw_arrays)
GL_PROC(_GL_EXT_multi_draw_arrays,void,glMultiDrawArraysEXT,(unsigned int mode,int *first,int *count,int primcount))
GL_PROC(_GL_EXT_multi_draw_arrays,void,glMultiDrawElementsEXT,(unsigned int mode,int *count,unsigned int type,const void **indices,int primcount))

#ifdef _WIN32
GL_EXT(_WGL_ARB_pbuffer)
#define WGL_DRAW_TO_PBUFFER_ARB		0x202D
#define WGL_DRAW_TO_PBUFFER_ARB		0x202D
#define WGL_MAX_PBUFFER_PIXELS_ARB	0x202E
#define WGL_MAX_PBUFFER_WIDTH_ARB	0x202F
#define WGL_MAX_PBUFFER_HEIGHT_ARB	0x2030
#define WGL_PBUFFER_LARGEST_ARB		0x2033
#define WGL_PBUFFER_WIDTH_ARB		0x2034
#define WGL_PBUFFER_HEIGHT_ARB		0x2035
#define WGL_PBUFFER_LOST_ARB		0x2036
GL_PROC(_WGL_ARB_pbuffer,HPBUFFERARB,wglCreatePbufferARB,(HDC hDC,int iPixelFormat,int iWidth,int iHeight,const int *piAttribList))
GL_PROC(_WGL_ARB_pbuffer,HDC,wglGetPbufferDCARB,(HPBUFFERARB hPbuffer))
GL_PROC(_WGL_ARB_pbuffer,int,wglReleasePbufferDCARB,(HPBUFFERARB hPbuffer,HDC hDC))
GL_PROC(_WGL_ARB_pbuffer,BOOL,wglDestroyPbufferARB,(HPBUFFERARB hPbuffer))
GL_PROC(_WGL_ARB_pbuffer,BOOL,wglQueryPbufferARB,(HPBUFFERARB hPbuffer,int iAttribute,int *piValue))

GL_EXT(_WGL_ARB_render_texture)
#define WGL_BIND_TO_TEXTURE_RGB_ARB			0x2070
#define WGL_BIND_TO_TEXTURE_RGBA_ARB		0x2071
#define WGL_TEXTURE_FORMAT_ARB				0x2072
#define WGL_TEXTURE_TARGET_ARB				0x2073
#define WGL_MIPMAP_TEXTURE_ARB				0x2074
#define WGL_TEXTURE_RGB_ARB					0x2075
#define WGL_TEXTURE_RGBA_ARB				0x2076
#define WGL_NO_TEXTURE_ARB					0x2077
#define WGL_TEXTURE_CUBE_MAP_ARB			0x2078
#define WGL_TEXTURE_1D_ARB					0x2079
#define WGL_TEXTURE_2D_ARB					0x207A
#define WGL_NO_TEXTURE_ARB					0x2077
#define WGL_MIPMAP_LEVEL_ARB				0x207B
#define WGL_CUBE_MAP_FACE_ARB				0x207C
#define WGL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB	0x207D
#define WGL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB	0x207E
#define WGL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB	0x207F
#define WGL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB	0x2080
#define WGL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB	0x2081
#define WGL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB	0x2082
#define WGL_FRONT_LEFT_ARB					0x2083
#define WGL_FRONT_RIGHT_ARB					0x2084
#define WGL_BACK_LEFT_ARB					0x2085
#define WGL_BACK_RIGHT_ARB					0x2086
#define WGL_AUX0_ARB						0x2087
#define WGL_AUX1_ARB						0x2088
#define WGL_AUX2_ARB						0x2089
#define WGL_AUX3_ARB						0x208A
#define WGL_AUX4_ARB						0x208B
#define WGL_AUX5_ARB						0x208C
#define WGL_AUX6_ARB						0x208D
#define WGL_AUX7_ARB						0x208E
#define WGL_AUX8_ARB						0x208F
#define WGL_AUX9_ARB						0x2090
GL_PROC(_WGL_ARB_render_texture,BOOL,wglBindTexImageARB,(HPBUFFERARB hPbuffer,int iBuffer))
GL_PROC(_WGL_ARB_render_texture,BOOL,wglReleaseTexImageARB,(HPBUFFERARB hPbuffer,int iBuffer))
GL_PROC(_WGL_ARB_render_texture,BOOL,wglSetPbufferAttribARB,(HPBUFFERARB hPbuffer,const int *piAttribList))

GL_EXT(_WGL_ARB_pixel_format)
GL_PROC(_WGL_ARB_pixel_format,BOOL,wglGetPixelFormatAttribivARB,(HDC hdc,int iPixelFormat,int iLayerPlane,UINT nAttributes,const int *piAttributes,int *piValues))
GL_PROC(_WGL_ARB_pixel_format,BOOL,wglGetPixelFormatAttribfvARB,(HDC hdc,int iPixelFormat,int iLayerPlane,UINT nAttributes,const int *piAttributes,FLOAT *pfValues))
GL_PROC(_WGL_ARB_pixel_format,BOOL,wglChoosePixelFormatARB,(HDC hdc,const int *piAttribIList,const FLOAT *pfAttribFList,UINT nMaxFormats,int *piFormats,UINT *nNumFormats))
#define WGL_NUMBER_PIXEL_FORMATS_ARB 0x2000
#define WGL_DRAW_TO_WINDOW_ARB 0x2001
#define WGL_DRAW_TO_BITMAP_ARB 0x2002
#define WGL_ACCELERATION_ARB 0x2003
#define WGL_NEED_PALETTE_ARB 0x2004
#define WGL_NEED_SYSTEM_PALETTE_ARB 0x2005
#define WGL_SWAP_LAYER_BUFFERS_ARB 0x2006
#define WGL_SWAP_METHOD_ARB 0x2007
#define WGL_NUMBER_OVERLAYS_ARB 0x2008
#define WGL_NUMBER_UNDERLAYS_ARB 0x2009
#define WGL_TRANSPARENT_ARB 0x200A
#define WGL_TRANSPARENT_RED_VALUE_ARB 0x2037
#define WGL_TRANSPARENT_GREEN_VALUE_ARB 0x2038
#define WGL_TRANSPARENT_BLUE_VALUE_ARB 0x2039
#define WGL_TRANSPARENT_ALPHA_VALUE_ARB 0x203A
#define WGL_TRANSPARENT_INDEX_VALUE_ARB 0x203B
#define WGL_SHARE_DEPTH_ARB 0x200C
#define WGL_SHARE_STENCIL_ARB 0x200D
#define WGL_SHARE_ACCUM_ARB 0x200E
#define WGL_SUPPORT_GDI_ARB 0x200F
#define WGL_SUPPORT_OPENGL_ARB 0x2010
#define WGL_DOUBLE_BUFFER_ARB 0x2011
#define WGL_STEREO_ARB 0x2012
#define WGL_PIXEL_TYPE_ARB 0x2013
#define WGL_COLOR_BITS_ARB 0x2014
#define WGL_RED_BITS_ARB 0x2015
#define WGL_RED_SHIFT_ARB 0x2016
#define WGL_GREEN_BITS_ARB 0x2017
#define WGL_GREEN_SHIFT_ARB 0x2018
#define WGL_BLUE_BITS_ARB 0x2019
#define WGL_BLUE_SHIFT_ARB 0x201A
#define WGL_ALPHA_BITS_ARB 0x201B
#define WGL_ALPHA_SHIFT_ARB 0x201C
#define WGL_ACCUM_BITS_ARB 0x201D
#define WGL_ACCUM_RED_BITS_ARB 0x201E
#define WGL_ACCUM_GREEN_BITS_ARB 0x201F
#define WGL_ACCUM_BLUE_BITS_ARB 0x2020
#define WGL_ACCUM_ALPHA_BITS_ARB 0x2021
#define WGL_DEPTH_BITS_ARB 0x2022
#define WGL_STENCIL_BITS_ARB 0x2023
#define WGL_AUX_BUFFERS_ARB 0x2024
#define WGL_NO_ACCELERATION_ARB 0x2025
#define WGL_GENERIC_ACCELERATION_ARB 0x2026
#define WGL_FULL_ACCELERATION_ARB 0x2027
#define WGL_SWAP_EXCHANGE_ARB 0x2028
#define WGL_SWAP_COPY_ARB 0x2029
#define WGL_SWAP_UNDEFINED_ARB 0x202A
#define WGL_TYPE_RGBA_ARB 0x202B
#define WGL_TYPE_COLORINDEX_ARB 0x202C
#endif // _WIN32

GL_EXT(_GL_SGIS_generate_mipmap)
#define GL_GENERATE_MIPMAP_SGIS			0x8191
#define GL_GENERATE_MIPMAP_HINT_SGIS	0x8192

// some ATI exts
GL_EXT(_GL_ATI_vertex_array_object)
GL_PROC(_GL_ATI_vertex_array_object,unsigned int,glNewObjectBufferATI,(int size, const void *pointer, unsigned int usage))
GL_PROC(_GL_ATI_vertex_array_object,unsigned char,glIsObjectBufferATI,(unsigned int buffer))
GL_PROC(_GL_ATI_vertex_array_object,void,glUpdateObjectBufferATI,(unsigned int buffer, unsigned int offset, int size, const void *pointer, unsigned int preserve))
GL_PROC(_GL_ATI_vertex_array_object,void,glGetObjectBufferfvATI,(unsigned int buffer, unsigned int pname, float *params))
GL_PROC(_GL_ATI_vertex_array_object,void,glGetObjectBufferivATI,(unsigned int buffer, unsigned int pname, int *params))
GL_PROC(_GL_ATI_vertex_array_object,void,glFreeObjectBufferATI,(unsigned int buffer))
GL_PROC(_GL_ATI_vertex_array_object,void,glArrayObjectATI,(unsigned int array, int size, unsigned int type, int stride, unsigned int buffer, unsigned int offset))
GL_PROC(_GL_ATI_vertex_array_object,void,glGetArrayObjectfvATI,(unsigned int array, unsigned int pname, float *params))
GL_PROC(_GL_ATI_vertex_array_object,void,glGetArrayObjectivATI,(unsigned int array, unsigned int pname, int *params))
GL_PROC(_GL_ATI_vertex_array_object,void,glVariantArrayObjectATI,(unsigned int id, unsigned int type, int stride, unsigned int buffer, unsigned int offset))
GL_PROC(_GL_ATI_vertex_array_object,void,glGetVariantArrayObjectfvATI,(unsigned int id, unsigned int pname, float *params))
GL_PROC(_GL_ATI_vertex_array_object,void,glGetVariantArrayObjectivATI,(unsigned int id, unsigned int pname, int *params))
#define        GL_STATIC_ATI                      0x8760
#define        GL_DYNAMIC_ATI                     0x8761
#define        GL_PRESERVE_ATI                    0x8762
#define        GL_DISCARD_ATI                     0x8763
#define        GL_OBJECT_BUFFER_SIZE_ATI          0x8764
#define        GL_OBJECT_BUFFER_USAGE_ATI         0x8765
#define        GL_ARRAY_OBJECT_BUFFER_ATI         0x8766
#define        GL_ARRAY_OBJECT_OFFSET_ATI         0x8767

GL_EXT(_GL_ATI_element_array)
GL_PROC(_GL_ATI_element_array,void,glElementPointerATI,(unsigned int type, const void *pointer))
GL_PROC(_GL_ATI_element_array,void,glDrawElementArrayATI,(unsigned int mode, int count))
GL_PROC(_GL_ATI_element_array,void,glDrawRangeElementArrayATI,(unsigned int mode, unsigned int start, unsigned int end, int count))
#define GL_ELEMENT_ARRAY_ATI					0x8768
#define GL_ELEMENT_ARRAY_TYPE_ATI				0x8769
#define GL_ELEMENT_ARRAY_POINTER_ATI			0x876A
	
GL_EXT(_GL_EXT_texture_edge_clamp)
#define GL_CLAMP_TO_EDGE_EXT					0x812F
		
/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

