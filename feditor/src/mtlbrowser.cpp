#include "pch.h"
#include <gtk/gtk.h>
#include <GL/gl.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_resourcemgr.h>
#include <fegdk/f_drawutil.h>
#include <fegdk/f_material.h>
#include <fegdk/f_basetexture.h>
#include <fegdk/f_fontft.h>
#include <GL/glu.h>
#include "mtlbrowser.h"
#include "editorcore.h"
#include "brush.h"
#include "editorcmd.h"
#include "editormap.h"

namespace feditor
{

	enum
	{
		THUMB_SZ=64,
		THUMB_BORDER=4,
		THUMB_VSTEP=(THUMB_SZ + THUMB_BORDER*2),
	};

	const char *
	get_shortened_mtl_name (const char *fullname)
	{
		// trim "textures/" from beginning
		if (strncmp (fullname, "textures/", 9))
			return fullname;
		return fullname + 9;
	}
//
	mtlView::mtlView (void)
	{
		mPos = 0;
		mSelection = -1;
	}
	
	void mtlView::init (void)
	{
		beginGL ();
		endGL (false);
	}
	void mtlView::reshape (void)
	{
		beginGL ();
		glViewport (0, 0, mpWidget->allocation.width, mpWidget->allocation.height);
		endGL (false);
	}
	
	void mtlView::draw (void)
	{
		fe::g_engine->setViewport (mpViewport.dynamicCast <fe::baseViewport> ());
		beginGL ();
		if (!mpFont)
			mpFont = fe::g_engine->getResourceMgr ()->createFontFT ("ter-116n.pcf");
		glClearColor (0.3, 0.3, 0.3, 1);
		glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		int w = mpWidget->allocation.width;
		int h = mpWidget->allocation.height;
	
		fe::resourceMgr *mgr = fe::g_engine->getResourceMgr ();
		int num = mgr->getCount (fe::RES_MATERIAL);
		int x = 0;
		int y = -mPos;
	
		glEnable (GL_TEXTURE_2D);
		for (int i = 0; i < num; i++)
		{
			if (y > -THUMB_VSTEP)
			{
				if (i == mSelection)
				{
					glDisable (GL_TEXTURE_2D);
					fe::g_engine->getDrawUtil ()->drawPic (x, y, w, THUMB_SZ+THUMB_BORDER*2, 0, 0, 1, 1, float4(.29f, .63f, .72f, 1.f), true);
	
					glEnable (GL_TEXTURE_2D);
				}
				fe::material *mtl = fe::checked_cast<fe::material*>(mgr->getObject (fe::RES_MATERIAL, i));
				fe::baseTexturePtr tex = mtl->getStdMap ();
				if (tex)
				{
					tex->bind (0);
					fe::g_engine->getDrawUtil ()->drawPic (x+THUMB_BORDER, y+THUMB_BORDER, THUMB_SZ, THUMB_SZ, 0, 0, 1, 1, 0xfffffffful, true);
				}
				mpFont->drawTextString (get_shortened_mtl_name (mtl->name ()), x + THUMB_SZ + THUMB_BORDER, y, 0xfffffffful);
			}
			y += THUMB_VSTEP;//THUMB_BORDER*2+THUMB_SZ;
		}
	
		endGL ();
	}
	
	void mtlView::setPos (int pos)
	{
		mPos = pos;
		draw ();
	}
	
	int mtlView::getPos (void) const
	{
		return mPos;
	}
	
	void mtlView::setSelected (int sel)
	{
		mSelection = sel;
		if (g_editor->getMap ())
		{
			// set selected mtl to selected brushes
			brushList &lst = g_editor->getMap ()->getSelection ();
			fe::resourceMgr *mgr = fe::g_engine->getResourceMgr ();
			fe::material* mtl = fe::checked_cast<fe::material*>(mgr->getObject (fe::RES_MATERIAL, sel));
			g_editor->addCommand (new CmdSetMaterial (mtl->name ()));
			g_editor->redrawAllViews ();
		}
	}
	
	int mtlView::getSelected (void) const
	{
		return mSelection;
	}
	
	mtlBrowser::mtlBrowser (GtkWidget *widget)
	{
		mSelection = -1;
		mOldParms[0] = mOldParms[1] = mOldParms[2];
		// "widget" must be a raw container
		GtkWidget *mainhbox;
		GtkWidget *vbox;
		vbox = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox);
		mpTextureEntry = gtk_entry_new ();
		gtk_widget_show (mpTextureEntry);
		gtk_box_pack_start (GTK_BOX (vbox), mpTextureEntry, FALSE, TRUE, 0);
		mainhbox = gtk_hbox_new (FALSE, 0);
		gtk_widget_show (mainhbox);
		gtk_container_add(GTK_CONTAINER (widget), vbox);
		gtk_box_pack_start (GTK_BOX (vbox), mainhbox, TRUE, TRUE, 0);
		mpTexBrowser = new mtlView;
		mpTexBrowser->setSelected (0);
		gtk_box_pack_start (GTK_BOX (mainhbox), mpTexBrowser->getWidget (), TRUE, TRUE, 0);
		mpScroll = GTK_VSCROLLBAR (gtk_vscrollbar_new (NULL));
		gtk_box_pack_start (GTK_BOX (mainhbox), GTK_WIDGET (mpScroll), FALSE, FALSE, 0);
		g_signal_connect_after (G_OBJECT (mpTexBrowser->getWidget ()), "button-press-event", G_CALLBACK (click), this);
		g_signal_connect_after (G_OBJECT (mpTexBrowser->getWidget ()), "scroll-event", G_CALLBACK (scroll), this);
		g_signal_connect_after (G_OBJECT (mpScroll), "expose_event", G_CALLBACK (draw_view), this);
		g_signal_connect_after (G_OBJECT (mpScroll), "change-value", G_CALLBACK (scroll_changed), this);
		g_signal_connect_after (G_OBJECT (mpTextureEntry), "activate", G_CALLBACK (set_texture_from_entry), this);
	}

	void mtlBrowser::set_texture_from_entry (GtkEntry *entry, mtlBrowser *browser)
	{
		const char *tex = gtk_entry_get_text (entry);
		fe::g_engine->getResourceMgr ()->createMaterial (tex);
		// now get by index
		int it;
		fe::resourceMgr *mgr = fe::g_engine->getResourceMgr ();
		int num = mgr->getCount (fe::RES_MATERIAL);
		for (it = 0; it < num; it++)
		{
			fe::baseObject *m = mgr->getObject (fe::RES_MATERIAL, it);
			if (!strcmp (m->name (), tex))
				break;
		}
		if (it != num)
		{
			browser->mpTexBrowser->setSelected (it);	
			browser->mpTexBrowser->draw ();
		}
	}
	
	gboolean mtlBrowser::scroll (GtkWidget *widget, GdkEventScroll *event, mtlBrowser *browser)
	{
		switch (event->direction)
		{
		case GDK_SCROLL_DOWN:
			gtk_range_set_value (GTK_RANGE (browser->mpScroll), gtk_range_get_value (GTK_RANGE (browser->mpScroll)) + THUMB_VSTEP/3);
			break;
		case GDK_SCROLL_UP:
			gtk_range_set_value (GTK_RANGE (browser->mpScroll), gtk_range_get_value (GTK_RANGE (browser->mpScroll)) - THUMB_VSTEP/3);
			break;
		}
		int value = (int)gtk_range_get_value (GTK_RANGE (browser->mpScroll));
		if (value < 0)
			value = 0;
		browser->mpTexBrowser->setPos ((int)value);
		return TRUE;
	}
	
	void mtlBrowser::scroll_changed (GtkRange *range, GtkScrollType scroll, gdouble value, gpointer data)
	{
		mtlBrowser *browser = (mtlBrowser*)data;
		if (value < 0)
			value = 0;
		if (value > range->adjustment->upper)
			value = range->adjustment->upper;
		browser->mpTexBrowser->setPos ((int)value);
	}
	
	void mtlBrowser::draw_view (GtkWidget *widget, GdkEventExpose *event, gpointer data)
	{
		mtlBrowser *browser = (mtlBrowser*)data;
		browser->init ();
	}
	
	void mtlBrowser::init (void)
	{
		int w = mpTexBrowser->getWidget ()->allocation.width;
		int h = mpTexBrowser->getWidget ()->allocation.height;
		fe::resourceMgr *mgr = fe::g_engine->getResourceMgr ();
		int num = mgr->getCount (fe::RES_MATERIAL);
	
		if (w != mOldParms[0] || h != mOldParms[1] || num != mOldParms[2])
		{
			mOldParms[0] = w;
			mOldParms[1] = h;
			mOldParms[2] = num;
			int nrow = num;
			if (nrow == 0)
				nrow = 1;
	
			nrow = nrow * (THUMB_SZ + THUMB_BORDER*2);
			nrow -= h;
			if (nrow < 1)
				nrow = 1;
	
			gtk_range_set_range (GTK_RANGE (mpScroll), 0, nrow + THUMB_BORDER);
			gtk_range_set_increments (GTK_RANGE (mpScroll), THUMB_VSTEP/3, THUMB_VSTEP);
			int value = (int)gtk_range_get_value (GTK_RANGE (mpScroll));
			if (value < 0)
				value = 0;
			mpTexBrowser->setPos ((int)value);
		}
	}
	
	int mtlBrowser::getSelection (void) const
	{
		return mpTexBrowser->getSelected ();
	}
	
	gboolean mtlBrowser::click (GtkWidget *widget, GdkEventButton *event, gpointer data)
	{
		mtlBrowser *browser = (mtlBrowser*)data;
		int it = ((int)event->y + browser->mpTexBrowser->getPos ()) / THUMB_VSTEP;
		fe::resourceMgr *mgr = fe::g_engine->getResourceMgr ();
		int num = mgr->getCount (fe::RES_MATERIAL);
		if (it >= num)
			return TRUE;
		browser->mpTexBrowser->setSelected (it);	
		browser->mpTexBrowser->draw ();
	
		return TRUE;
	}
	
	mtlBrowser::~mtlBrowser (void)
	{
		delete mpTexBrowser;
	}
	
	mtlBrowserWnd::mtlBrowserWnd (void)
	{
		mpWindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		gtk_window_set_title (GTK_WINDOW (mpWindow), "Material browser");
		gtk_window_set_resizable (GTK_WINDOW (mpWindow), TRUE);
		gtk_window_set_default_size (GTK_WINDOW (mpWindow), 200, 300);
		mpBrowser = new mtlBrowser (mpWindow);
		gtk_widget_hide (mpWindow);
		g_signal_connect (G_OBJECT (mpWindow), "delete_event", G_CALLBACK (gtk_widget_hide_on_delete), this);
	}
	
	mtlBrowserWnd::~mtlBrowserWnd (void)
	{
		delete mpBrowser;
	}
	
	void mtlBrowserWnd::toggle (void)
	{
		bool vis = (mpWindow->object.flags & GTK_VISIBLE) ? true : false;
		if (!vis)
			gtk_widget_show_all (mpWindow);
		else
			gtk_widget_hide (mpWindow);
	}
	
	bool mtlBrowserWnd::isVisible (void) const
	{
		return (mpWindow->object.flags & GTK_VISIBLE) ? true : false;
	}
	
	GtkWidget *mtlBrowserWnd::getWindow (void) const
	{
		return mpWindow;
	}
	
	fe::material* mtlBrowserWnd::getSelectedMtl (void) const
	{
		fe::resourceMgr *mgr = fe::g_engine->getResourceMgr ();
		int sel = mpBrowser->getSelection ();
		if (sel != -1)
			return fe::checked_cast<fe::material*>(mgr->getObject (fe::RES_MATERIAL, sel));
		return NULL;
	}
	
	cStr mtlBrowserWnd::getSelectedMtlName (void) const
	{
		fe::materialPtr mtl = getSelectedMtl ();
		if (mtl)
			return mtl->name ();
		return "";
	}
	
}
