#ifndef __F_EDITORVP_H
#define __F_EDITORVP_H

#include <fegdk/f_baseviewport.h>
#include <fegdk/f_math.h>
#include "feditor.h"

namespace feditor
{
	
	class glView;
	
	class editorViewport : public fe::baseViewport
	{
	
	public:
	
		enum viewMode { ortho, perspective };
		enum orthoMode { xy, xz, yz };
	
	private:
	
		glView*		mpGLView;
		
		mutable matrix4			mViewMatrix;
		matrix4			mInvViewMatrix;
		matrix4			mProjectionMatrix;
		vector3			mEyePos;
		bool				mbInvalidated;
		viewMode			mViewMode;
		float				mOrthoScale;
		orthoMode			mOrthoMode;
		float				mYaw, mPitch;
		vector3			mPerspCameraPos;
	
	public:
	
		editorViewport (glView *view);
		~editorViewport (void);
		
		glView*	getView (void);
		const matrix4&	getViewMatrix (void) const;
		void			setProjectionMatrix (const matrix4 &m);
		const matrix4&	getProjectionMatrix (void) const;
		vector3		getEyePos (void) const;
		float			getOrthoScale (void) const;
		void			setOrthoScale (float scale);
		void			nextOrthoMode (void);
		void			setOrthoMode (orthoMode mode);
		orthoMode		getOrthoMode (void) const;
		viewMode		getViewMode (void) const;
		void			setViewMode(viewMode mode);
		void			move(const vector3 &dir);
		void			rotate(const vector3 &axis, float angle);
		void			yaw (float angle);
		void			pitch (float angle);
		
		void	setPosition (int x, int y);
		void	setSize (int w, int h);
		int		getLeft (void) const;
		int		getTop (void) const;
		int		getWidth (void) const;
		int		getHeight (void) const;
		void	close (ulong flags);
		
		// dummy unused mainloop
		virtual void mainLoop (void) {}
	};

}

#endif // __F_EDITORVP_H

