#include "pch.h"
#include <string.h>
#include "logview.h"

namespace feditor
{

	logView::logView (void)
	{
		mpWidget = gtk_text_view_new ();
		gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (mpWidget), GTK_WRAP_WORD);
		gtk_text_view_set_editable (GTK_TEXT_VIEW (mpWidget), false);
	}
	
	logView::~logView (void)
	{
	}
	
	void logView::print (const char *text)
	{
		GtkTextBuffer *buf = gtk_text_view_get_buffer (GTK_TEXT_VIEW (mpWidget));
		GtkTextIter iter;
		gtk_text_buffer_get_end_iter (GTK_TEXT_BUFFER (buf), &iter);
		gtk_text_buffer_insert (GTK_TEXT_BUFFER (buf), &iter, text, (gint)strlen (text));
		
	}
	
	void logView::setLogFile (const char *fname)
	{
	}
	
	GtkWidget*	logView::getWidget (void)
	{
		return mpWidget;
	}
	
}
