// $Header$

#ifndef __F_RESOURCETABLE_H
#define __F_RESOURCETABLE_H

#include <map>
#include "f_string.h"

template <typename T> class feResource
{
protected:
	typedef std::map< feCStr, T* > resourceMap;
	static resourceMap *mpResourceList;
	static T* getResource( const char *name )
	{
		if ( !mpResourceList )
			return NULL;
		typename resourceMap::iterator it;
		it = mpResourceList->find( name );
		if ( it != mpResourceList->end() )
			return ( *it ).second;
		return NULL;
	}
	static void addResource( const char *name, T *r )
	{
		if ( !mpResourceList )
			mpResourceList = new resourceMap;
		( *mpResourceList )[name] = r;
	}
	static void removeResource( const char *name, T *r )
	{
		typename resourceMap::iterator it;

// FIXME:
// unfortunately, i have a bug here, where Material is derived from feMaterial
// and still has it's own feResource parent:
//	class feMaterial : public feResource< feMaterial > {};
//	class Material : public feMaterial, public feResource< Material > {};
// so there is 2 resource tables in one class

// waker 2004-12-14: it seems it's not an issue anymore since there's no more engine<->editor cooperation;
//                   they're completely separate projects now.

		if ( NULL != mpResourceList )
		{
			it = mpResourceList->find( name );
			if ( it != mpResourceList->end() )
				mpResourceList->erase( it );
			if ( mpResourceList->empty() )
			{
				delete mpResourceList;
				mpResourceList = NULL;
			}
		}
	}
};

#endif // __F_RESOURCETABLE_H

