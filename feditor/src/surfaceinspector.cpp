#include "pch.h"
#include "surfaceinspector.h"
#include "editorwnd.h"
#include "brush.h"
#include "editorcore.h"
#include "editormap.h"
#include "editorcmd.h"

namespace feditor
{

	surfaceInspector::surfaceInspector (void)
	{
		GtkWidget *vbox = gtk_vbox_new (FALSE, 0);
		gtk_widget_show (vbox);
		mpWindow = new editorWnd (vbox, "Surface inspector");
		
		GtkWidget *label, *entry, *table;
		table = gtk_table_new (6, 2, FALSE);
		gtk_widget_show (table);
		gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
	
		label = gtk_label_new ("material");
		gtk_widget_show (label);
		entry = gtk_entry_new ();
		gtk_widget_show (entry);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 0, 1,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		mpMtlEntry = entry;
	
	
		label = gtk_label_new ("shift x");
		gtk_widget_show (label);
		entry = gtk_spin_button_new_with_range (-8192, 8192, 8);
		gtk_spin_button_set_digits (GTK_SPIN_BUTTON (entry), 0);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (entry), 0);
		gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (entry), TRUE);
		gtk_widget_show (entry);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 1, 2,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		mpShiftEntry[0] = entry;
	
	
		label = gtk_label_new ("shift y");
		gtk_widget_show (label);
		entry = gtk_spin_button_new_with_range (-8192, 8192, 8);
		gtk_spin_button_set_digits (GTK_SPIN_BUTTON (entry), 0);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (entry), 0);
		gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (entry), TRUE);
		gtk_widget_show (entry);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 2, 3,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		mpShiftEntry[1] = entry;
		
		label = gtk_label_new ("scale x");
		gtk_widget_show (label);
		entry = gtk_spin_button_new_with_range (-1000, 1000, 0.01);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (entry), 1);
		gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (entry), TRUE);
		gtk_widget_show (entry);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 3, 4,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 3, 4,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		mpScaleEntry[0] = entry;
		
		label = gtk_label_new ("scale y");
		gtk_widget_show (label);
		entry = gtk_spin_button_new_with_range (-1000, 1000, 0.01);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (entry), 1);
		gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (entry), TRUE);
		gtk_widget_show (entry);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 4, 5,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 4, 5,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		mpScaleEntry[1] = entry;
		
		label = gtk_label_new ("rotate");
		gtk_widget_show (label);
		entry = gtk_spin_button_new_with_range (-360, 360, 45);
		gtk_spin_button_set_digits (GTK_SPIN_BUTTON (entry), 0);
		gtk_spin_button_set_value (GTK_SPIN_BUTTON (entry), 0);
		gtk_spin_button_set_wrap (GTK_SPIN_BUTTON (entry), TRUE);
		gtk_widget_show (entry);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 5, 6,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 5, 6,
				(GtkAttachOptions)(GTK_EXPAND | GTK_FILL),
				(GtkAttachOptions)(0), 2, 0);
		mpRotateEntry = entry;
	
		GtkWidget *btn = gtk_button_new_from_stock (GTK_STOCK_APPLY);
		gtk_widget_show (btn);
		gtk_box_pack_start (GTK_BOX (vbox), btn, FALSE, FALSE, 0);
		g_signal_connect_after (G_OBJECT (btn), "clicked", G_CALLBACK (texdefChanged), this);
	
	/*	g_signal_connect_after (G_OBJECT (mpMtlEntry), "activate", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpRotateEntry), "activate", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpRotateEntry), "value-changhed", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpShiftEntry[0]), "activate", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpShiftEntry[0]), "value-changhed", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpShiftEntry[1]), "activate", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpShiftEntry[1]), "value-changhed", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpScaleEntry[0]), "activate", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpScaleEntry[0]), "value-changhed", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpScaleEntry[1]), "activate", G_CALLBACK (texdefChanged), this);
		g_signal_connect_after (G_OBJECT (mpScaleEntry[1]), "value-changhed", G_CALLBACK (texdefChanged), this);*/
	}
	
	surfaceInspector::~surfaceInspector (void)
	{
	}
	
	void surfaceInspector::update (void)
	{
		brushList &lst = g_editor->getMap ()->getSelection ();
		if (!lst.empty ())
		{
			smartPtr <brush> b = g_editor->getMap ()->getSelection ().front ();
			for (int f = 0; f < b->numFaces (); f++)
			{
				if (b->isFaceSelected (f))
				{
					const brushFaceTexdef &t = b->faces (f).texdef ();
					gtk_entry_set_text (GTK_ENTRY (mpMtlEntry), t.mtl);
					gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpRotateEntry), t.rotate);
					gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpShiftEntry[0]), t.shift[0]);
					gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpShiftEntry[1]), t.shift[1]);
					gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpScaleEntry[0]), t.scale[0]);
					gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpScaleEntry[1]), t.scale[1]);
					break;
				}
			}
		}
		else
		{
			gtk_entry_set_text (GTK_ENTRY (mpMtlEntry), "");
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpRotateEntry), 0);
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpShiftEntry[0]), 0);
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpShiftEntry[1]), 0);
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpScaleEntry[0]), 1);
			gtk_spin_button_set_value (GTK_SPIN_BUTTON (mpScaleEntry[1]), 1);
		}
	}
	
	void surfaceInspector::toggle (void)
	{
		mpWindow->toggle ();
	}
	
	bool surfaceInspector::isVisible (void) const
	{
		return mpWindow->isVisible ();
	}
	
	void surfaceInspector::texdefChanged (GtkWidget *widg, surfaceInspector *insp)
	{
		const gchar *mtl = gtk_entry_get_text (GTK_ENTRY (insp->mpMtlEntry));
		int rot = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (insp->mpRotateEntry));
		int shift[2];
		shift[0] = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (insp->mpShiftEntry[0]));
		shift[1] = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (insp->mpShiftEntry[1]));
		float scale[2];
		scale[0] = gtk_spin_button_get_value_as_float (GTK_SPIN_BUTTON (insp->mpScaleEntry[0]));
		scale[1] = gtk_spin_button_get_value_as_float (GTK_SPIN_BUTTON (insp->mpScaleEntry[1]));
	
		fprintf (stderr, "applying texdef: %d %d %d %f %f\n", rot, shift[0], shift[1], scale[0], scale[1]);
	
		g_editor->addCommand (new CmdSetSelectionTexdef (mtl, shift, scale, rot));
	}
	
}

