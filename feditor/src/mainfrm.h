#ifndef __F_MAINFRM_H
#define __F_MAINFRM_H

#include <gtk/gtk.h>
#include <fegdk/f_baseobject.h>
#include "feditor.h"

namespace feditor
{

	class logView;
	class mtlBrowserWnd;
	class editorWidget;
	class editorWnd;
	class editorMenu;
	class entityInspector;
	class surfaceInspector;
	
	class mainFrm : public fe::baseObject
	{
	private:
	
		GtkWidget*		mpWidget;
		GtkToolbar*		mpToolbar;
		GtkStatusbar*	mpStatusbar;
	
		editorWidget*	mpEditorWidget;
		editorWidget* mpCameraWidget;
		logView*		mpLogView;
		logView*		mpScriptView;
		mtlBrowserWnd*	mpMtlBrowser;
	
		smartPtr <editorWnd> 	mpCameraView;
		smartPtr <editorMenu> mpEditorMenu;
		smartPtr <entityInspector> mpEntityInspector;
		smartPtr <surfaceInspector> mpSurfaceInspector;
	
		void initMenubar (GtkWidget* menubar);
		void initToolbar (GtkWidget* toolbar);
		GtkWidget* createEntityInspector (void);
		
		static void toggleToolbar (GtkCheckMenuItem *menuitem, gpointer user_data);
		static void toggleStatusbar (GtkCheckMenuItem *menuitem, gpointer user_data);
	
		void initGdkKeymap (void);
		static int	gtkKeycodeToFKey (int keycode);
		static gboolean gtkKeyPress (GtkWidget *widget, GdkEventKey *event, gpointer user_data);
		static gboolean gtkKeyRelease (GtkWidget *widget, GdkEventKey *event, gpointer user_data);
		static gboolean gtkFocusLost (GtkWidget *widget, GdkEventFocus *event, gpointer user_data);
		static void connectKeyHandler (GtkWidget *widget);
		static void clickedNew (GtkMenuItem *menuitem, mainFrm *frm);
		static void clickedOpen (GtkMenuItem *menuitem, mainFrm *frm);
		static void clickedSave (GtkMenuItem *menuitem, mainFrm *frm);
		static void clickedSaveAs (GtkMenuItem *menuitem, mainFrm *frm);
		static void clickedExportAsRaw (GtkMenuItem *menuitem, mainFrm *frm);
		static void clickedSaveAsPrefab (GtkMenuItem *menuitem, mainFrm *frm);
		static void clickedLoadPrefab (GtkMenuItem *menuitem, mainFrm *frm);
		
	public:
		mainFrm (void);
		~mainFrm (void);
	
		GtkWidget*		getWidget (void) const;
		logView*		getLogView (void) const;
		logView*		getScriptView (void) const;
		mtlBrowserWnd*	getMtlBrowser (void) const;
		smartPtr <editorWnd> getCameraWnd (void) const;
		smartPtr <editorMenu> getEditorMenu (void) const;
		smartPtr <entityInspector> getEntityInspector (void) const;
		smartPtr <surfaceInspector> getSurfaceInspector (void) const;
	};
	
}

#endif //__F_MAINFRM_H

