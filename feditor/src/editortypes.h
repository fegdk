#ifndef __F_EDITORTYPES_H
#define __F_EDITORTYPES_H

#include <fegdk/f_helpers.h>
#include "feditor.h"
#include <list>
#include <vector>

namespace feditor
{

class brush;
class entity;
typedef std::list < smartPtr <brush> > brushList;
typedef std::vector < smartPtr <entity> > entList;

}

#endif // __F_EDITORTYPES_H
