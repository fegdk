#ifndef __F_EDITORMAP_H
#define __F_EDITORMAP_H

#include <fegdk/f_baseobject.h>
#include <fegdk/f_helpers.h>
#include "editortypes.h"
#include <fegdk/f_parser.h>

namespace feditor
{

	class brush;
	class entity;
	
	class editorMap : public fe::baseObject
	{
	private:
		brushList	mBrushList;
		entList		mEntList;
		brushList	mSelection;
		bool		mbIsModified;
		mutable cStr	mFileName;
		
		smartPtr <brush> loadBrush (fe::charParser &p);
		void loadEntity (fe::charParser &p, brushList &brushes, entList &ents);
		template <class T> void saveBrush (T &file, smartPtr <brush> b) const;
		template <class T> void saveEntity (T &file, smartPtr <entity> e, bool selected = false) const;
	
	public:
		editorMap (void);
		~editorMap (void);
	
		// resets map
		void reset (void);
	
		// inserts brush into scene
		void			insertBrush (smartPtr <brush> b, bool select = false);
		
		// removes brush from scene
		void			removeBrush (smartPtr <brush> b);
	
		// returns list of scene brushes
		const brushList&		getBrushList (void) const;
		brushList&		getBrushList (void);
		
		// returns list of selected brushes
		const brushList&		getSelection (void) const;
		brushList&		getSelection (void);
		
		// returns list of entities
		const entList&	getEntities (void) const;
	
		// returns worldspawn entity
		smartPtr <entity>		getWorldEntity (void) const;
	
		// inserts entity into scene
		void			insertEntity (smartPtr <entity> e);
		
		// removes entity from scene
		void			removeEntity (smartPtr <entity> e);
		
		// selects specified brush
		void			addSelection (smartPtr <brush> brush);
	
		// unselects specified brush
		void			removeSelection (smartPtr <brush> brush);
	
		// sets "modified" flag
		void			setModifiedFlag (bool isModified);
	
		// returns "modified" flag
		bool			getModifiedFlag (void) const;
	
		// clears selection
		void			clearSelection (void);
	
		bool isEmptyEnt (smartPtr <entity> e) const;
	
		void mergeFromFile (const char *filename, bool select = false, bool undoable = false);
	
		void save (void) const;
		void saveAs (void) const;
		void exportAsRaw (void) const;
		void saveSelectionAsPrefab (void) const;
		void load (void);
		void loadPrefab (void);
	
		void save (std::stringstream &s, bool selected) const;
		void load (const char *s, brushList &brushes, entList &ents);
	};
	
}

#endif // __F_EDITORMAP_H
