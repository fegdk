#include "pch.h"
#include <fegdk/f_keycodes.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_input.h>
#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <GL/gl.h>
#include <gdk/gdkkeysyms.h>
#include "config.h"
#include "mainfrm.h"
#include "zview.h"
#include "editorview.h"
#include "editorcore.h"
#include "logview.h"
#include "mtlbrowser.h"
#include "editorwidget.h"
#include "editorwnd.h"
#include "editormenu.h"
#include "entityinspector.h"
#include "surfaceinspector.h"
#include "editormap.h"

namespace feditor
{

	void python_activate (GtkEntry *entry, gpointer user_data)
	{
		mainFrm *frm = (mainFrm*)user_data;
		g_editor->pyRunString (gtk_entry_get_text (entry));
		gtk_entry_set_text (entry, "");
	}
	
	gboolean python_keypress (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
	{
		GtkEntry *entry = GTK_ENTRY (widget);
		if (event->keyval == GDK_Up)
			gtk_entry_set_text (entry, g_editor->pyHistoryPrev ());
		else if (event->keyval = GDK_Down)
			gtk_entry_set_text (entry, g_editor->pyHistoryNext ());
		return TRUE;
	}
	
	mainFrm::mainFrm (void)
	{
	//	gl::init ();
		mpToolbar = NULL;
		mpStatusbar = NULL;
		mpLogView = NULL;
		mpScriptView = NULL;
		mpEditorWidget = NULL;
	
		gtk_window_set_default_icon_from_file (PKGDATADIR"/icons/icon.png", NULL);
		// create frame window
		mpWidget = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		gtk_window_set_title (GTK_WINDOW (mpWidget), PACKAGE " " VERSION);
		gtk_window_set_default_size (GTK_WINDOW (mpWidget), 800, 600);
		gtk_widget_show_all (mpWidget);
	
	
		g_signal_connect (G_OBJECT (mpWidget), "destroy", G_CALLBACK (gtk_main_quit), NULL);
	
		// create top-level vbox
		
		GtkWidget *mainvbox;
	
		mainvbox = gtk_vbox_new (FALSE, 0);
		gtk_container_add(GTK_CONTAINER (mpWidget), mainvbox);
	
		// add menubar
		
		GtkWidget *menubar;
	
		menubar = gtk_menu_bar_new ();
		gtk_box_pack_start (GTK_BOX (mainvbox), menubar, FALSE, FALSE, 0);
		initMenubar (menubar);
	
		// add toolbar
		
		GtkWidget *toolbar;
	
		toolbar = gtk_toolbar_new ();
		gtk_box_pack_start (GTK_BOX (mainvbox), toolbar, FALSE, FALSE, 0);
		initToolbar (toolbar);
	
		// add splitter for log-view and other viewports
		GtkWidget *vpaned;
		GtkWidget *frame1, *frame2;
		vpaned = gtk_vpaned_new ();
		gtk_container_add (GTK_CONTAINER (mainvbox), vpaned);
		
		// add vertical splitter for log and script
		GtkWidget *hpaned;
		hpaned = gtk_hpaned_new ();
		gtk_paned_pack2 (GTK_PANED (vpaned), hpaned, FALSE, FALSE);
		frame1 = gtk_frame_new (NULL);
		gtk_frame_set_shadow_type (GTK_FRAME (frame1), GTK_SHADOW_IN);
		frame2 = gtk_frame_new (NULL);
		gtk_frame_set_shadow_type (GTK_FRAME (frame2), GTK_SHADOW_IN);
		gtk_paned_pack1 (GTK_PANED (hpaned), frame1, TRUE, TRUE);
		gtk_paned_pack2 (GTK_PANED (hpaned), frame2, TRUE, TRUE);
	
		// add editor view to the leftmost part (log)
		GtkWidget *vbox, *label;
		GtkWidget *textview, *scroll;
		
		mpLogView = new logView ();
		
		vbox = gtk_vbox_new (FALSE, 0);
		gtk_container_add (GTK_CONTAINER (frame1), vbox);
		scroll = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 1);
		textview = mpLogView->getWidget ();
		gtk_container_add (GTK_CONTAINER (scroll), textview);
		GtkWidget *hbox = gtk_hbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 1);
	/*	label = gtk_label_new ("console:");
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		textview = gtk_entry_new ();
		gtk_box_pack_start (GTK_BOX (hbox), textview, TRUE, TRUE, 0);*/
		
		mpScriptView = new logView ();
		vbox = gtk_vbox_new (FALSE, 0);
		gtk_container_add (GTK_CONTAINER (frame2), vbox);
		scroll = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
		gtk_box_pack_start (GTK_BOX (vbox), scroll, TRUE, TRUE, 0);
		textview = mpScriptView->getWidget ();
		gtk_container_add (GTK_CONTAINER (scroll), textview);
		hbox = gtk_hbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 1);
	/*	label = gtk_label_new ("python:");
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		textview = gtk_entry_new ();
		gtk_box_pack_start (GTK_BOX (hbox), textview, TRUE, TRUE, 0);
		g_signal_connect (G_OBJECT (textview), "activate", G_CALLBACK (python_activate), this);
		g_signal_connect_after (G_OBJECT (textview), "key-press-event", G_CALLBACK (python_keypress), this);*/
	
		mpEditorWidget = new editorWidget;
		gtk_paned_pack1 (GTK_PANED (vpaned), mpEditorWidget->getContainer (), TRUE, TRUE);
	
	/*
		// add vertical splitter for zview/editorpane
		hpaned = gtk_hpaned_new ();
		gtk_paned_pack1 (GTK_PANED (vpaned), hpaned, TRUE, TRUE);
	
		frame1 = gtk_frame_new (NULL);
		gtk_paned_pack1 (GTK_PANED (hpaned), frame1, FALSE, FALSE);
		mpZView = new zView (mpOwner);
		gtk_widget_set_size_request (GTK_WIDGET (mpZView->getWidget ()), 40, 0);
		gtk_container_add (GTK_CONTAINER (frame1), mpZView->getWidget ());
	
		frame2 = gtk_frame_new (NULL);
		gtk_paned_pack2 (GTK_PANED (hpaned), frame2, TRUE, TRUE);
	
		mpEditorView = new editorView (mpOwner);
		gtk_container_add (GTK_CONTAINER (frame2), mpEditorView->getWidget ());*/
	
		
		// add statusbar
		GtkWidget *statusbar;
		statusbar = gtk_statusbar_new ();
		gtk_box_pack_start (GTK_BOX (mainvbox), statusbar, FALSE, FALSE, 0);
		mpStatusbar = GTK_STATUSBAR (statusbar);
	
		// bind keyboard signals
		// note that while mouse events are bound to specific views, keyboard is handled globally
		// btw, we'll override all of gdk keyboard handling
	
		initGdkKeymap ();
	
		connectKeyHandler (mpWidget);
	
		gtk_widget_show_all (mpWidget);
	
		mpMtlBrowser = new mtlBrowserWnd;
//		connectKeyHandler (mpMtlBrowser->getWindow ());
	
		mpCameraWidget = new editorWidget;
		mpCameraView = new editorWnd (mpCameraWidget->getContainer (), "Camera view");
		mpCameraView->toggle ();
		connectKeyHandler (mpCameraView->getWindow ());
	
		mpEntityInspector = new entityInspector ();
		mpSurfaceInspector = new surfaceInspector ();
	
		mpEditorWidget->getEditorView ()->getViewport ()->setViewMode (editorViewport::ortho);
		mpCameraWidget->getEditorView ()->getViewport ()->setViewMode (editorViewport::perspective);
	
		// entity creation menu:
		// read/parse entities.def
		// fill in menu
		// bind menu to rightclick
		mpEditorMenu = new editorMenu ("createentitymenu");
	}
	
	void mainFrm::connectKeyHandler (GtkWidget *widget)
	{
		g_signal_connect (G_OBJECT (widget), "key-press-event", G_CALLBACK (gtkKeyPress), NULL);
		g_signal_connect (G_OBJECT (widget), "key-release-event", G_CALLBACK (gtkKeyRelease), NULL);
		g_signal_connect (G_OBJECT (widget), "focus-out-event", G_CALLBACK (gtkFocusLost), NULL);
	}
	
	gboolean mainFrm::gtkFocusLost (GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
	{
		fe::g_engine->getInputDevice ()->getState ()->reset ();
	}
	
	std::map <int, int> gdk_keymap;
	
	void mainFrm::initGdkKeymap (void)
	{
		gdk_keymap [GDK_BackSpace] = fe::Key_BackSpace;
		gdk_keymap [GDK_Tab] = fe::Key_Tab;
		gdk_keymap [GDK_Return] = fe::Key_Return;
		gdk_keymap [GDK_Pause] = fe::Key_Pause;
		gdk_keymap [GDK_Scroll_Lock] = fe::Key_ScrollLock;
	//	gdk_keymap [GDK_Sys_Req] = fe::Key_SysReq;
		gdk_keymap [GDK_Escape] = fe::Key_Escape;
		gdk_keymap [GDK_Delete] = fe::Key_Del;
		gdk_keymap [GDK_Home] = fe::Key_Home;
		gdk_keymap [GDK_Left] = fe::Key_LeftArrow;
		gdk_keymap [GDK_Up] = fe::Key_UpArrow;
		gdk_keymap [GDK_Right] = fe::Key_RightArrow;
		gdk_keymap [GDK_Down] = fe::Key_DownArrow;
		gdk_keymap [GDK_Page_Up] = fe::Key_PgUp;
		gdk_keymap [GDK_Page_Down] = fe::Key_PgDn;
		gdk_keymap [GDK_End] = fe::Key_End;
		gdk_keymap [GDK_Insert] = fe::Key_Ins;
		gdk_keymap [GDK_Num_Lock] = fe::Key_Kp_NumLock;
		gdk_keymap [GDK_KP_Enter] = fe::Key_Kp_Enter;
		gdk_keymap [GDK_KP_Home] = fe::Key_Kp_Home;
		gdk_keymap [GDK_KP_Left] = fe::Key_Kp_LeftArrow;
		gdk_keymap [GDK_KP_Up] = fe::Key_Kp_UpArrow;
		gdk_keymap [GDK_KP_Right] = fe::Key_Kp_RightArrow;
		gdk_keymap [GDK_KP_Down] = fe::Key_Kp_DownArrow;
		gdk_keymap [GDK_KP_Page_Up] = fe::Key_Kp_PgUp;
		gdk_keymap [GDK_KP_Page_Down] = fe::Key_Kp_PgDn;
		gdk_keymap [GDK_KP_End] = fe::Key_Kp_End;
		gdk_keymap [GDK_KP_Insert] = fe::Key_Kp_Ins;
		gdk_keymap [GDK_KP_Delete] = fe::Key_Kp_Del;
		gdk_keymap [GDK_KP_Equal] = fe::Key_Kp_Equals;
		gdk_keymap [GDK_KP_Multiply] = fe::Key_Kp_Star;
		gdk_keymap [GDK_KP_Add] = fe::Key_Kp_Plus;
		gdk_keymap [GDK_KP_Subtract] = fe::Key_Kp_Minus;
		gdk_keymap [GDK_KP_Divide] = fe::Key_Kp_Slash;
		gdk_keymap [GDK_KP_5] = fe::Key_Kp_5;
		gdk_keymap [GDK_F1] = fe::Key_F1;
		gdk_keymap [GDK_F2] = fe::Key_F2;
		gdk_keymap [GDK_F3] = fe::Key_F3;
		gdk_keymap [GDK_F4] = fe::Key_F4;
		gdk_keymap [GDK_F5] = fe::Key_F5;
		gdk_keymap [GDK_F6] = fe::Key_F6;
		gdk_keymap [GDK_F7] = fe::Key_F7;
		gdk_keymap [GDK_F8] = fe::Key_F8;
		gdk_keymap [GDK_F9] = fe::Key_F9;
		gdk_keymap [GDK_F10] = fe::Key_F10;
		gdk_keymap [GDK_F11] = fe::Key_F11;
		gdk_keymap [GDK_F12] = fe::Key_F12;
		gdk_keymap [GDK_F13] = fe::Key_F13;
		gdk_keymap [GDK_F14] = fe::Key_F14;
		gdk_keymap [GDK_F15] = fe::Key_F15;
		gdk_keymap [GDK_Shift_L] = fe::Key_LShift;
		gdk_keymap [GDK_Shift_R] = fe::Key_RShift;
		gdk_keymap [GDK_Control_L] = fe::Key_LCtrl;
		gdk_keymap [GDK_Control_R] = fe::Key_RCtrl;
		gdk_keymap [GDK_Caps_Lock] = fe::Key_CapsLock;
		gdk_keymap [GDK_Alt_L] = fe::Key_LAlt;
		gdk_keymap [GDK_Alt_R] = fe::Key_RAlt;
		gdk_keymap [GDK_Super_L] = fe::Key_LSuper;
		gdk_keymap [GDK_Super_R] = fe::Key_RSuper;
	}
	
	int	mainFrm::gtkKeycodeToFKey (int keycode)
	{
		std::map<int,int>::const_iterator it = gdk_keymap.find (keycode);
		if (it != gdk_keymap.end ())
		{
			// normal key
			return (*it).second;
		}
		else
		{
			if (isascii (keycode))
				return tolower (keycode);
			else
				return -1;
		}
		return -1;
	}
	
	gboolean    mainFrm::gtkKeyPress (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
	{
		// get keycode for fegdk input system (keybindings)
		int code = event->keyval;
	
		for (int npass = 0; npass < 2; npass++)
		{
			int keycode = gtkKeycodeToFKey (code);
			if (keycode != -1)
			{
				// pass to input sys
				//fprintf (stderr, "keycode is %d translated to %d(%c)\n", code, keycode, keycode);
				fe::g_engine->getInputDevice ()->keyDown (keycode);
				break;
			}
			else
			{
				GdkKeymapKey *keys;
				guint *keyvals;
				gint nentries;
				gdk_keymap_get_entries_for_keycode (
					gdk_keymap_get_default (),
					event->hardware_keycode,
					&keys,
					&keyvals,
					&nentries);
	
				if (nentries != 0 && keyvals[0] < 0x7f)
				{
					code = keyvals[0];
				}
				else
					break;
			}
		}
		return TRUE;
	}
	
	gboolean    mainFrm::gtkKeyRelease (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
	{
		// get keycode for fegdk input system (keybindings)
		int code = event->keyval;
	
		for (int npass = 0; npass < 2; npass++)
		{
			int keycode = gtkKeycodeToFKey (code);
			if (keycode != -1)
			{
				// pass to input sys
	//			fprintf (stderr, "keycode is %d translated to %d(%c)\n", code, keycode, keycode);
				fe::g_engine->getInputDevice ()->keyUp (keycode);
				break;
			}
			else
			{
				GdkKeymapKey *keys;
				guint *keyvals;
				gint nentries;
				gdk_keymap_get_entries_for_keycode (
					gdk_keymap_get_default (),
					event->hardware_keycode,
					&keys,
					&keyvals,
					&nentries);
	
				if (nentries != 0 && keyvals[0] < 0x7f)
				{
					code = keyvals[0];
				}
				else
					break;
			}
		}
		return TRUE;
	}
	
	mainFrm::~mainFrm (void)
	{
		if (mpMtlBrowser)
			delete mpMtlBrowser;
		if (mpEditorWidget)
			delete mpEditorWidget;
		if (mpCameraWidget)
			delete mpCameraWidget;
	//	gl::free();
	}
	
	void mainFrm::initMenubar (GtkWidget* menubar)
	{
		GtkWidget *item, *submenu, *subitem;
	
		// create 'file' submenu
		item = gtk_menu_item_new_with_mnemonic ("_File");
		gtk_menu_bar_append (menubar, item);
		
		submenu = gtk_menu_new ();
		gtk_menu_set_title (GTK_MENU (submenu), "File");
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
	
		subitem = gtk_tearoff_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
	
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_NEW, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect_after (G_OBJECT (subitem), "activate", G_CALLBACK (clickedNew), this);
	
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_OPEN, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect_after (G_OBJECT (subitem), "activate", G_CALLBACK (clickedOpen), this);
	
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_SAVE, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect_after (G_OBJECT (subitem), "activate", G_CALLBACK (clickedSave), this);
	
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_SAVE_AS, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect_after (G_OBJECT (subitem), "activate", G_CALLBACK (clickedSaveAs), this);

		subitem = gtk_menu_item_new_with_label ("Export as .raw (early engine testing)");
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect_after (G_OBJECT (subitem), "activate", G_CALLBACK (clickedExportAsRaw), this);
	
		subitem = gtk_separator_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_separator_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect (G_OBJECT (subitem), "activate", G_CALLBACK (gtk_main_quit), NULL);
	
		// create 'edit' submenu
		item = gtk_menu_item_new_with_label ("Edit");
		gtk_menu_bar_append (menubar, item);
		
		submenu = gtk_menu_new ();
		gtk_menu_set_title (GTK_MENU (submenu), "Edit");
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
	
		subitem = gtk_tearoff_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
	
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_UNDO, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_REDO, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_separator_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_CUT, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
	
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_COPY, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_PASTE, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_separator_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_DELETE, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
	
		subitem = gtk_separator_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_image_menu_item_new_with_label ("Load prefab");
		gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (subitem), gtk_image_new_from_stock (GTK_STOCK_OPEN, GTK_ICON_SIZE_MENU));
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect_after (G_OBJECT (subitem), "activate", G_CALLBACK (clickedLoadPrefab), this);
	
		subitem = gtk_image_menu_item_new_with_label ("Save selection as prefab");
		gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (subitem), gtk_image_new_from_stock (GTK_STOCK_SAVE, GTK_ICON_SIZE_MENU));
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect_after (G_OBJECT (subitem), "activate", G_CALLBACK (clickedSaveAsPrefab), this);
	
		subitem = gtk_separator_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		subitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_PREFERENCES, NULL);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		
		// create 'selection' submenu
		item = gtk_menu_item_new_with_label ("Selection");
		gtk_menu_bar_append (menubar, item);
		
		submenu = gtk_menu_new ();
		gtk_menu_set_title (GTK_MENU (submenu), "Selection");
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
	
		subitem = gtk_tearoff_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
	
		subitem = gtk_image_menu_item_new_with_mnemonic ("_Select All");
		gtk_menu_append (GTK_MENU (submenu), subitem);
		
		// create 'grid' submenu
		item = gtk_menu_item_new_with_label ("Grid");
		gtk_menu_bar_append (menubar, item);
		
		submenu = gtk_menu_new ();
		gtk_menu_set_title (GTK_MENU (submenu), "Grid");
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
	
		// create 'view' submenu
		item = gtk_menu_item_new_with_label ("View");
		gtk_menu_bar_append (menubar, item);
		
		submenu = gtk_menu_new ();
		gtk_menu_set_title (GTK_MENU (submenu), "View");
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
	
		subitem = gtk_tearoff_menu_item_new ();
		gtk_menu_append (GTK_MENU (submenu), subitem);
	
		subitem = gtk_check_menu_item_new_with_mnemonic ("_Toolbar");
		gtk_check_menu_item_set_state (GTK_CHECK_MENU_ITEM (subitem), TRUE);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect (G_OBJECT (subitem), "toggled", G_CALLBACK (toggleToolbar), this);
	
		subitem = gtk_check_menu_item_new_with_mnemonic ("_Statusbar");
		gtk_check_menu_item_set_state (GTK_CHECK_MENU_ITEM (subitem), TRUE);
		gtk_menu_append (GTK_MENU (submenu), subitem);
		g_signal_connect (G_OBJECT (subitem), "toggled", G_CALLBACK (toggleStatusbar), this);
		
		// create 'brush' submenu
		item = gtk_menu_item_new_with_label ("Brush");
		gtk_menu_bar_append (menubar, item);
		
		submenu = gtk_menu_new ();
		gtk_menu_set_title (GTK_MENU (submenu), "Brush");
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
	
		// create 'script' submenu
		item = gtk_menu_item_new_with_label ("Script");
		gtk_menu_bar_append (menubar, item);
		
		submenu = gtk_menu_new ();
		gtk_menu_set_title (GTK_MENU (submenu), "Script");
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
	
		// create 'help' submenu
		item = gtk_menu_item_new_with_label ("Help");
		gtk_menu_bar_append (menubar, item);
		
		submenu = gtk_menu_new ();
		gtk_menu_set_title (GTK_MENU (submenu), "Help");
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), submenu);
	}
	
	void mainFrm::initToolbar (GtkWidget *toolbar)
	{
		mpToolbar = GTK_TOOLBAR (toolbar);
		gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
		gtk_toolbar_set_icon_size (GTK_TOOLBAR (toolbar), GTK_ICON_SIZE_SMALL_TOOLBAR);
			
	/*
		GtkWidget *img;
	
		// sample code for inserting bitmapped buttons
		// this one is just the same as inserting stock new
		img = gtk_image_new_from_stock (GTK_STOCK_NEW, GTK_ICON_SIZE_LARGE_TOOLBAR);
		gtk_toolbar_append_item (GTK_TOOLBAR (toolbar), "New", "Start from scratch"
						, NULL, img, NULL, NULL);
	*/
	
		
		// file section
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_NEW
						, "Start from scratch", NULL, NULL, NULL, 0);
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_OPEN
						, "Open another map file", NULL, NULL, NULL, 1);
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_SAVE
						, "Save your work", NULL, NULL, NULL, 2);
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_SAVE_AS
						, "Save work to another file", NULL, NULL, NULL, 3);
		gtk_toolbar_insert_space (GTK_TOOLBAR (toolbar), 4);
	
		// edit section
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_CUT
						, "Cut selection", NULL, NULL, NULL, 5);
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_COPY
						, "Yank selection to clipboard", NULL, NULL, NULL, 6);
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_PASTE
						, "Paste selection from clipboard", NULL, NULL, NULL, 7);
		gtk_toolbar_insert_space (GTK_TOOLBAR (toolbar), 8);
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_UNDO
						, "Undo last change", NULL, NULL, NULL, 9);
		gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_REDO
						, "Redo it back", NULL, NULL, NULL, 10);
		gtk_toolbar_insert_space (GTK_TOOLBAR (toolbar), 11);
	
		// clipper section
		GtkWidget *icon = NULL;
		icon = gtk_image_new_from_file (PKGDATADIR"/icons/clipper.png");
		gtk_toolbar_insert_item (GTK_TOOLBAR (toolbar), "Toggle clipper"
						, "Toggle this cutting thing on or off", NULL, icon
						, NULL, NULL, 12);
		icon = gtk_image_new_from_file (PKGDATADIR"/icons/clipper-split.png");
		gtk_toolbar_insert_item (GTK_TOOLBAR (toolbar), "Split selection"
						, "Cut everything apart", NULL, icon
						, NULL, NULL, 13);
		icon = gtk_image_new_from_file (PKGDATADIR"/icons/clipper-clip.png");
		gtk_toolbar_insert_item (GTK_TOOLBAR (toolbar), "Clip selection"
						, "There can be only One", NULL, icon
						, NULL, NULL, 14);
	
		// rotation section
		icon = gtk_image_new_from_file (PKGDATADIR"/icons/rotate.png");
		gtk_toolbar_insert_space (GTK_TOOLBAR (toolbar), 15);
		gtk_toolbar_insert_item (GTK_TOOLBAR (toolbar), "Toggle rotation"
						, "Rotate selection a bit", NULL, icon
						, NULL, NULL, 16);
	
	}
	
	void mainFrm::toggleToolbar (GtkCheckMenuItem *menuitem, gpointer user_data)
	{
		mainFrm *frm = reinterpret_cast <mainFrm*> (user_data);
		if (!menuitem->active)
			gtk_widget_hide (GTK_WIDGET (frm->mpToolbar));
		else
			gtk_widget_show (GTK_WIDGET (frm->mpToolbar));
	}
	
	void mainFrm::toggleStatusbar (GtkCheckMenuItem *menuitem, gpointer user_data)
	{
		mainFrm *frm = reinterpret_cast <mainFrm*> (user_data);
		if (!menuitem->active)
			gtk_widget_hide (GTK_WIDGET (frm->mpStatusbar));
		else
			gtk_widget_show (GTK_WIDGET (frm->mpStatusbar));
	}
	
	logView* mainFrm::getLogView (void) const
	{
		return mpLogView;
	}
	
	logView* mainFrm::getScriptView (void) const
	{
		return mpScriptView;
	}
	
	GtkWidget*	mainFrm::getWidget (void) const
	{
		return mpWidget;
	}
	
	mtlBrowserWnd*	mainFrm::getMtlBrowser (void) const
	{
		return mpMtlBrowser;
	}
	
	smartPtr <editorWnd>	mainFrm::getCameraWnd (void) const
	{
		return mpCameraView;
	}
	
	smartPtr <editorMenu> mainFrm::getEditorMenu (void) const
	{
		return mpEditorMenu;
	}
	
	smartPtr <entityInspector> mainFrm::getEntityInspector (void) const
	{
		return mpEntityInspector;
	}
	
	void mainFrm::clickedNew (GtkMenuItem *menuitem, mainFrm *frm)
	{
		GtkWidget *dlg = gtk_message_dialog_new (GTK_WINDOW (frm->mpWidget), GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_NONE, "Save changes?");
		gtk_dialog_add_buttons (GTK_DIALOG (dlg), GTK_STOCK_SAVE, GTK_RESPONSE_YES);
		gtk_dialog_add_buttons (GTK_DIALOG (dlg), "Don't save", GTK_RESPONSE_NO);
		gtk_dialog_add_buttons (GTK_DIALOG (dlg), GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
		gint response = gtk_dialog_run (GTK_DIALOG (dlg));
		gtk_widget_destroy (dlg);
	
		if (response == GTK_RESPONSE_YES)
			g_editor->getMap ()->save ();
		else if (response == GTK_RESPONSE_CANCEL)
			return;
		
		g_editor->reset ();
	}
	
	void mainFrm::clickedOpen (GtkMenuItem *menuitem, mainFrm *frm)
	{
		g_editor->getMap ()->load ();
	}
	
	void mainFrm::clickedSave (GtkMenuItem *menuitem, mainFrm *frm)
	{
		g_editor->getMap ()->save ();
	}
	
	void mainFrm::clickedSaveAs (GtkMenuItem *menuitem, mainFrm *frm)
	{
		g_editor->getMap ()->saveAs ();
	}
	
	void mainFrm::clickedExportAsRaw (GtkMenuItem *menuitem, mainFrm *frm)
	{
		g_editor->getMap ()->exportAsRaw ();
	}
	
	void mainFrm::clickedSaveAsPrefab (GtkMenuItem *menuitem, mainFrm *frm)
	{
		g_editor->getMap ()->saveSelectionAsPrefab ();
	}
	
	void mainFrm::clickedLoadPrefab (GtkMenuItem *menuitem, mainFrm *frm)
	{
		g_editor->getMap ()->loadPrefab ();
	}
	
	smartPtr <surfaceInspector> mainFrm::getSurfaceInspector (void) const
	{
		return mpSurfaceInspector;
	}
	
}
