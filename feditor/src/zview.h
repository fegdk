#ifndef __F_ZVIEW_H
#define __F_ZVIEW_H

#include <fegdk/f_types.h>
#include <fegdk/f_fontft.h>
#include "feditor.h"
#include "glview.h"

namespace feditor
{

	class glView;
	
	class zView : public glView
	{
	
	private:
	
		fe::fontFTPtr	mpFont;
		long		mScroll;
		bool		mbScroll;
		point		mLastMousePt;
	
		virtual void init (void);
		virtual void draw (void);
		virtual void reshape (void);
		
		static gboolean gtkButtonPress (GtkWidget *widget, GdkEventButton *event, gpointer user_data);
		static gboolean gtkButtonRelease (GtkWidget *widget, GdkEventButton *event, gpointer user_data);
		static gboolean gtkMotionNotify (GtkWidget *widget, GdkEventMotion *event, gpointer user_data);
			
	public:
	
		zView (void);
		~zView (void);
	
	};
	
}

#endif // __F_ZVIEW_H

