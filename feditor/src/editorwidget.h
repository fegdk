#ifndef __F_EDITORWIDGET
#define __F_EDITORWIDGET

#include <gtk/gtk.h>

namespace feditor
{

	// definition of editor widget which contains hpaned that splits editorview and zview
	
	class editorView;
	class zView;
	
	class editorWidget
	{
	private:
		editorView *mpEditorView;
		zView *mpZView;
		GtkWidget *mpSplitter;
	
	public:
		editorWidget (void);
		~editorWidget (void);
		editorView *getEditorView (void) const;
		zView *getZView (void) const;
		GtkWidget *getContainer (void) const;
	};

}

#endif // __F_EDITORWIDGET

