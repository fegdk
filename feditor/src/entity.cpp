#include "pch.h"
#include <fegdk/f_helpers.h>
#include <algorithm>
#include "entity.h"
#include "brush.h"
#include "editorcore.h"
#include "mainfrm.h"
#include "entityinspector.h"

namespace feditor
{

	entity::entity (void)
	{
		mpEClass = NULL;
	}
	
	entity::~entity (void)
	{
	}
	
	void		entity::setAttrValue (const char *key, const char *value, uint32 flags)
	{
		vector3 oldPos;
		if (!(flags&attr_nomovebrush))
		{
			if (!strcmp (key, "origin") && mpEClass->isFixedSize ())
				oldPos = getOrigin ();
		}
	
		std::vector< ePair >::iterator it;
		for (it = mEPairs.begin (); it != mEPairs.end (); it++)
		{
			if (!strcmp ((*it).getName (), key))
			{
				 (*it).setValue (value);
				break;
			}
		}
		if (it == mEPairs.end ())
			mEPairs.push_back (ePair (key, value));
	
		if (!strcmp (key, "classname"))
		{
			mpEClass = eClass::getEClassByName (value);
			if (!mpEClass)
				fprintf (stderr, "invalid eclass: %s\n", value);
		}
		else if (!(flags&attr_nomovebrush) && !strcmp (key, "origin") && mpEClass->isFixedSize ())
		{
			// get pos and recreate brush
			vector3 newPos = getOrigin ();
			smartPtr <brush> b = g_editor->getEntBrush (this);
			if (b)
				b->move (newPos - oldPos);
		}
	}
	
	void		entity::eraseAttr (const char *key)
	{
		vector3 oldPos;
		if (!strcmp (key, "origin") && mpEClass->isFixedSize ())
			oldPos = getOrigin ();
		// don't allow to delete classname
		if (!strcmp (key, "classname"))
			return;
		std::vector< ePair >::iterator it;
		for (it = mEPairs.begin (); it != mEPairs.end (); it++)
		{
			if (!strcmp ((*it).getName (), key))
			{
				mEPairs.erase (it);
				break;
			}
		}
		if (!strcmp (key, "origin") && mpEClass->isFixedSize ())
		{
			// get pos and recreate brush
			vector3 newPos = getOrigin ();
			smartPtr <brush> b = g_editor->getEntBrush (this);
			if (b)
				b->move (newPos - oldPos);
		}
	}
	
	const entity::ePairList&	entity::getEPairs (void) const
	{
		return mEPairs;
	}
	
	/*void		entity::setEClass (const eClass *ec)
	{
		mpEClass = ec;
		mEPairs = ePairList ();
		insertEPair (ePair ("classname", ec->getName ()));
	}*/
	
	const eClass*	entity::getEClass (void) const
	{
		return mpEClass;
	}
	
	const char *		entity::getAttrValue (const char *key) const
	{
		std::vector< ePair >::const_iterator it;
		for (it = mEPairs.begin (); it != mEPairs.end (); it++)
		{
			if (!strcmp ((*it).getName (), key))
				return (*it).getValue ();
		}
		return NULL;
	}
	
	vector3	entity::getOrigin (void) const
	{
		const char *s = getAttrValue ("origin");
		if (!s)
			return vector3 (0,0,0);
		vector3 v;
		sscanf (s, "%f %f %f", &v.x, &v.y, &v.z);
	
		return v;
	}
	
	void		entity::setOrigin (const vector3& origin)
	{
		cStr s;
		s.printf ("%0.2f %0.2f %0.2f", origin.x, origin.y, origin.z);
		setAttrValue ("origin", s.c_str (), attr_nomovebrush);
	}
	
	#if 0
	void		entity::linkBrush (smartPtr <brush> brush)
	{
		brushList::iterator it = std::find (mBrushes.begin (), mBrushes.end (), brush);
		// don't link twice
		if (it == mBrushes.end ())
		{
			// unlink from previous owner
			if (brush->getOwnerEnt ())
				brush->getOwnerEnt ()->unlinkBrush (brush);
			// link to this owner
			mBrushes.push_back (brush);
			brush->setOwnerEnt (this);
	
			// set origin
	#if 0
			TexDef *texdef = brush->GetTexDef ();
			if (texdef && texdef->flags&F_ORIGIN)
			{
				mOrigin = (brush->mins () + brush->maxs ()) / 2;
			}
	#endif
		}
	}
	
	void		entity::unlinkBrush (smartPtr <brush> brush)
	{
		brushList::iterator it = std::find (mBrushes.begin (), mBrushes.end (), brush);
		if (it != mBrushes.end ())
		{
			brush->setOwnerEnt (NULL);
			mBrushes.erase (it);
		}
	}
	#endif 
	#if 0
	void		entity::render (editorViewport *pViewport)
	{
		assert (mpEClass);
		if (!mpEClass)
			return;
	
		Interface *ip = g_pGlobals->mpInterface;
		engine *engine = ip->GetEngine ();
		editorViewport *vp = checked_cast<editorViewport *> (engine->viewport ());
	
		if (vp->GetViewMode () == VM_ORTHO)
		{
			ORTHO_MODE mode = vp->GetOrthoMode ();
			int nDim1 = (mode == OM_YZ) ? 1 : 0;
			int nDim2 = (mode == OM_XY) ? 1 : 2;
			int nDim3 = (mode == OM_XZ) ? 1 : ( (mode == OM_XY) ? 2 : 0);
	
			if (mpEClass->IsFixedSize ())
			{
				const Mgc::Vector3 &mins = mpEClass->GetMins ();
				const Mgc::Vector3 &maxs = mpEClass->GetMaxs ();
				const Mgc::Vector3 &color = mpEClass->GetColor ();
	
				// FIXME: target dir should be drawn for brush entities too,
				//        but i don't have origin brush info yet, so it's kinda useless for now
	
				// draw target direction
				const char *targetname = ePair::stringByName (mEPairs, "target");
				if (targetname)
				{
					// find entity
					entity *target = g_pGlobals->mpInterface->TargetByName (targetname);
					if (target)
					{
						const Mgc::Vector3& origin = target->getOrigin ();
						uint start;
						vertex_XyzDiffuseTex1 *lockedVerts = engine->dynamicVB ()->lock (2, start);
	
						lockedVerts->pos = Mgc::Vector3 (mOrigin[nDim1], mOrigin[nDim2], 0.1f);
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
						lockedVerts->pos = Mgc::Vector3 (origin[nDim1], origin[nDim2], 0.1f);
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
	
						engine->dynamicVB ()->unlock ();
						engine->render ()->device ()->DrawPrimitive (D3DPT_LINESTRIP, start, 1);
	
						// calc 2 arrow vertices
	
						Mgc::Vector3 up (1, 1, 1);
						up[nDim1] = 0;
						up[nDim2] = 0;
						Mgc::Vector3 mult (0, 0, 0);
						mult[nDim1] = 1;
						mult[nDim2] = 1;
						Mgc::Vector3 dir = origin - mOrigin;
						dir.x *= mult.x;
						dir.y *= mult.y;
						dir.z *= mult.z;
						dir.Unitize ();
	
						// direction is in plane of viewport
	
						Mgc::Vector3 cross = up.Cross (dir);
						Mgc::Vector3 center = (origin + mOrigin) * 0.5f;
	
						Mgc::Vector3 p1 ( center - dir * 8 + cross * 8);
						Mgc::Vector3 p2 (p1 - cross * 16);
	
						lockedVerts = lockedVerts = engine->dynamicVB ()->lock (3, start);
	
						lockedVerts->pos = Mgc::Vector3 (p1[nDim1], p1[nDim2], 0.1f);
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
						lockedVerts->pos = Mgc::Vector3 (center[nDim1], center[nDim2], 0.1f);
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
						lockedVerts->pos = Mgc::Vector3 (p2[nDim1], p2[nDim2], 0.1f);
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
						lockedVerts->pos = Mgc::Vector3 (origin[nDim1], origin[nDim2], 0.1f);
	
						engine->dynamicVB ()->unlock ();
						engine->render ()->device ()->DrawPrimitive (D3DPT_LINESTRIP, start, 2);
					}
				}
			}
		}
		else
		{
			if (mpEClass->IsFixedSize ())
			{
				// FIXME: target dir should be drawn for brush entities too,
				//        but i don't have origin brush info yet, so it's kinda useless for now
	
				// draw target direction
				const char *targetname = ePair::stringByName (mEPairs, "target");
				if (targetname)
				{
					// find entity
					entity *target = g_pGlobals->mpInterface->TargetByName (targetname);
					if (target)
					{
						const Mgc::Vector3& origin = target->getOrigin ();
						uint start;
						vertex_XyzDiffuseTex1 *lockedVerts = engine->dynamicVB ()->lock (2, start);
	
						lockedVerts->pos = mOrigin;
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
						lockedVerts->pos = origin;
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
	
						engine->dynamicVB ()->unlock ();
						engine->render ()->device ()->DrawPrimitive (D3DPT_LINESTRIP, start, 1);
	
						// calc 2 arrow vertices
	
						Mgc::Vector3 up (0, 0, 1);
						Mgc::Vector3 mult (1, 1, 0);
						Mgc::Vector3 dir = origin - mOrigin;
						dir.x *= mult.x;
						dir.y *= mult.y;
						dir.z *= mult.z;
						dir.Unitize ();
	
						// direction is in plane of viewport
	
						Mgc::Vector3 cross = up.Cross (dir);
						Mgc::Vector3 center = (origin + mOrigin) * 0.5f;
	
						Mgc::Vector3 p1 ( center - dir * 8 + cross * 8);
						Mgc::Vector3 p2 (p1 - cross * 16);
	
						lockedVerts = lockedVerts = engine->dynamicVB ()->lock (3, start);
	
						lockedVerts->pos = Mgc::Vector3 (p1);
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
						lockedVerts->pos = Mgc::Vector3 (center);
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
						lockedVerts->pos = Mgc::Vector3 (p2);
						lockedVerts->color = 0xff7f7f00;
						lockedVerts++;
						lockedVerts->pos = origin;
	
						engine->dynamicVB ()->unlock ();
						engine->render ()->device ()->DrawPrimitive (D3DPT_LINESTRIP, start, 2);
					}
				}
			}
		}
	}
	#endif
	
	std::vector< eClass >	eClass::mEClass;
	bool					eClass::mbInit = true;
	
	eClass::eClass (void)
	{
		mbFixedSize = false;
		mMins = vector3 (-8, -8, -8);
		mMaxs = vector3 (8, 8, 8);
		mColor = vector3 (0, 0, 0);
		mViewFlags = 0;
	}
	
	eClass::eClass (const eClass &ec)
	{
		mName = ec.mName;
		mbFixedSize = ec.mbFixedSize;
		mMins = ec.mMins;
		mMaxs = ec.mMaxs;
		mColor = ec.mColor;
		mDescription = ec.mDescription;
		mViewFlags = ec.mViewFlags;
		mEParms = ec.mEParms;
		mEFlags = ec.mEFlags;
	}
	
	const cStr&		eClass::getName (void) const
	{
		return mName;
	}
	
	const cStr&		eClass::getDescription (void) const
	{
		return mDescription;
	}
	
	bool				eClass::isFixedSize (void) const
	{
		return mbFixedSize;
	}
	
	const vector3&	eClass::getMins (void) const
	{
		return mMins;
	}
	
	const vector3&	eClass::getMaxs (void) const
	{
		return mMaxs;
	}
	
	const vector3&	eClass::getColor (void) const
	{
		return mColor;
	}
	
	const eClass::eClassList& eClass::getEClass (void)
	{
		if (mbInit)
		{
			mbInit = false;
			fe::charParser p (NULL, PKGDATADIR"/entities.def", true);
			for (;;)
			{
				cStr c;
				p.errMode (0);
				if (p.getToken ())
					break;
				p.errMode (1);
				c = p.token ();
				eClass e;
				e.load (c, p);

				// FIXME: viewflags must be specified by the entity
				e.mViewFlags = 0;
				if (e.mName == "light")
					e.mViewFlags |= ECLASS_LIGHT;
				if ((strncmp (e.mName, "info_player", strlen ("info_player")) == 0)
					|| (strncmp (e.mName, "path_corner", strlen ("path_corner")) == 0))
				{
					e.mViewFlags |= ECLASS_ANGLE;
				}
				if (strcmp (e.mName, "path") == 0)
				{
					e.mViewFlags |= ECLASS_PATH;
				}
				if (strcmp (e.mName, "misc_model") == 0)
				{
					e.mViewFlags |= ECLASS_MISCMODEL;
				}
				mEClass.push_back (e);
			}
	
			// sort alphabetically
			std::sort (mEClass.begin (), mEClass.end ());
		}
		return mEClass;
	}
	void eClass::load (const char *cname, fe::charParser &p)
	{
		// eclass
		mbFixedSize = true;
//		p.getToken ();
//		mName = p.token ();
		mName = cname;
		p.getToken ();
		if (!p.cmptoken ("("))
		{
			// color
			p.getToken ();
			mColor[0] = (float)atof (p.token ());
			p.getToken ();
			mColor[1] = (float)atof (p.token ());
			p.getToken ();
			mColor[2] = (float)atof (p.token ());
			p.matchToken (")");
	
			p.getToken ();
			if (!p.cmptoken ("("))
			{
				// mins
				p.getToken ();
				mMins[0] = (float)atof (p.token ());
				p.getToken ();
				mMins[1] = (float)atof (p.token ());
				p.getToken ();
				mMins[2] = (float)atof (p.token ());
				p.matchToken (")");
				p.getToken ();
				if (!p.cmptoken ("("))
				{
					// maxs
					p.getToken ();
					mMaxs[0] = (float)atof (p.token ());
					p.getToken ();
					mMaxs[1] = (float)atof (p.token ());
					p.getToken ();
					mMaxs[2] = (float)atof (p.token ());
					p.matchToken (")");
					p.getToken ();
				}
				else
				{
					mbFixedSize = false;
				}
			}
			else
			{
				mbFixedSize = false;
			}
		}
	
		if (!p.cmptoken ("{"))
		{
			// definition
			for (;;)
			{
				p.getToken ();
				if (!p.cmptoken ("}"))
				{
					break; // next entity
				}
				else if (!p.cmptoken ("doc"))
				{
					p.getToken ();
					mDescription = p.token ();
				}
				else if (!p.cmptoken ("string"))
				{
					eParm parm;
					parm.type = string;
					p.getToken ();
					parm.name = p.token ();
					p.getToken ();
					mEParms[p.token ()] = parm;
				}
				else if (!p.cmptoken ("bool"))
				{
					eParm parm;
					parm.type = boolean;
					p.getToken ();
					parm.name = p.token ();
					p.getToken ();
					mEParms[p.token ()] = parm;
				}
				else if (!p.cmptoken ("model"))
				{
					eParm parm;
					parm.type = string;
					p.getToken ();
					parm.name = p.token ();
					p.getToken ();
					mEParms[p.token ()] = parm;
				}
				else if (!p.cmptoken ("bit"))
				{
					p.getToken ();
					int bit = atoi (p.token ());
					p.getToken ();
					mEFlags[p.token ()] = bit;
				}
				else
					p.generalSyntaxError ();
			}
		}
		else
			p.generalSyntaxError ();
	}
	
	const eClass*	eClass::getEClassByName (const char *name)
	{
		std::vector< eClass >::const_iterator it;
		for (it = mEClass.begin (); it != mEClass.end (); it++)
		{
			if (!strcmp ((*it).getName (), name))
				return & (*it);
		}
		return NULL;
	}
	
	const eClass::eParmsMap& eClass::getEParms (void) const
	{
		return mEParms;
	}
	
	const eClass::eFlagsMap& eClass::getEFlags (void) const
	{
		return mEFlags;
	}
	
	int eClass::getEClassIdx (const char *name)
	{
		const eClass *eclass = getEClassByName (name);
		return eclass - &mEClass.front ();
	}
	
}
