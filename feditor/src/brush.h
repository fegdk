#ifndef __F_BRUSH_H
#define __F_BRUSH_H

#include <fegdk/f_math.h>
#include <fegdk/f_string.h>
#include <fegdk/f_types.h>
#include <fegdk/f_helpers.h>
#include <fegdk/f_material.h>
#include <fegdk/f_basetexture.h>
#include <fegdk/f_sceneobject.h>
#include <list>
#include <vector>
#include "pyobject.h"

namespace fe
{
	class material;
}

namespace feditor
{

	class editorViewport;
	class entity;

	class brushWinding
	{
		friend class brushFace;
		friend class brush;
	protected:
	
		std::vector< fe::vector3 >	mPoints;
		std::vector< fe::vector2 >	mTexCoords;
	
	public:
	
		brushWinding ();
		brushWinding (const brushWinding &w);
		~brushWinding ();
	
		// properties
		fe::vector3&	point (int iPoint) { return mPoints[iPoint]; }
		const fe::vector3&	point (int iPoint) const { return mPoints[iPoint]; }
		int				numPoints (void) const { return (int)mPoints.size(); }
		fe::vector2&	texCoord (int iPoint) { return mTexCoords[iPoint]; }
		const fe::vector2&	texCoord (int iPoint) const { return mTexCoords[iPoint]; }
		void			baseForPlane (const fe::plane &p);
		bool			clip (const fe::plane &split, bool keepon);
	
	};
	
	struct brushFaceTexdef
	{
		brushFaceTexdef (void);
		cStr			mtl;
		int32			shift[2];
		int32			rotate;
		float			scale[2];
	};
	
	class brushFace
	{
		friend class brush;
	protected:
	
		fe::plane		mPlane;
		fe::vector3			mPlanePts[3];
		brushWinding	mWinding;
		brushFaceTexdef	mTexdef;
		fe::materialPtr	mpMtl;
	
	public:
	
		brushFace ();
		brushFace (const brushFace &f);
		~brushFace ();
	
		fe::plane&			plane () { return mPlane; }
		const fe::plane&		plane () const { return mPlane; }
		fe::vector3&			planePt (uint iPoint) { assert(iPoint < 3); return mPlanePts[iPoint]; }
		const fe::vector3&	planePt (uint iPoint) const { assert(iPoint < 3); return mPlanePts[iPoint]; }
		brushWinding&		winding () { return mWinding; }
		const brushWinding&	winding () const { return mWinding; }
		void				setMtl (const char *name);
		fe::materialPtr		getMtl () const;
		brushFaceTexdef&	texdef ();
		const brushFaceTexdef&	texdef () const;
		void				textureVectors (float stFromXYZ[2][4]);
		fe::vector2			emitTexCoords (const fe::vector3 &pt);
		void				applyTexdef (void);
	
	};

	class brush;

	class brushMesh : public fe::geomSource
	{
	public:
		brushMesh (void);
		~brushMesh (void);
		void update (brush *b);
	};
	
	class brush : public fe::sceneObject
	{
	private:
		pyObjectHolder <brush>	mPyObject;
		smartPtr <brushMesh> mpMesh;
	
		bool mbNeedRecache;
/*		// cg programs parameter bindings
		struct vp_binding_t
		{
			handle pos, norm, color, tex0, tex1, tangent, binormal;
		};

		typedef std::map <cStr, vp_binding_t>       vp_binding_map_t;
		mutable vp_binding_map_t        mVPBindings;*/
			
	protected:
	
		// entity the brush is linked to, defaults to worldspawn entity;
		// never leave NULL there
		smartPtr <entity>				mpOwnerEnt;
		// selected or not
		bool					mbSelected;
		std::vector< bool >		mFaceSelection;
		
		std::vector< brushFace >	mFaces;
		fe::vector3				mMins;
		fe::vector3				mMaxs;
	
		bool	makeFaceWinding (int face);
		bool	makeFacePlane (int face);
	
		bool	testIntersection (const fe::ray3 &ray) const;
		
		void	init (void);
		
	public:
		void updateRendererCache (void);
	
		brush (void);
		brush (const fe::vector3 &mins, const fe::vector3 &maxs);
		brush (const brush &b);
		brush (const std::vector< brushFace > &brushFaces);
		void create (const fe::vector3 &mins, const fe::vector3 &maxs);
	
		PyObject *getPyObject (void);
		void	decrefPyObject (void);
		
		~brush ();
		void clear (void) { mFaces.clear(); }
	
		// edit
		void		buildWindings (bool snap = true);
		void		move (const fe::vector3 &moveVec, bool snap = true);
		void		splitByFace (const brushFace &f, smartPtr <brush> &front, smartPtr <brush> &back);
		void		snapToGrid ();
		void		snapPlanePts ();
		void		rotate (const fe::vector3 &angle, const fe::vector3 &origin, bool build = true);
		void		flip (const fe::vector3 &axis, const fe::vector3 &origin);
		void		removeEmptyFaces ();
		virtual void		build (bool bSnap = true);
		void		buildTexturing (void);
	
		// primitives
		void		makeSided (int sides, int axis = 2); 	// default axis is z
		void		makeSidedCone (int sides);
		void		makeSidedSphere (int sides);
	
		// properties
		brushFace&	faces (int iFace) { return mFaces[iFace]; }
		const brushFace&	faces (int iFace) const { return mFaces[iFace]; }
		int				numFaces (void) const { return (int)mFaces.size(); }
		fe::vector3&	mins () { return mMins; }
		fe::vector3&	maxs () { return mMaxs; }
		void			addFace (const brushFace& f) { mFaces.push_back(f); }
	
		// csg
	//	void			csgAdd (std::vector<smartPtr <brush> > &outBrushList, smartPtr <brush> b);
		void			csgSubtract (std::vector<smartPtr <brush> > &outBrushList, smartPtr <brush> b);
	//	void			csgIntersection (std::vector<smartPtr <brush> > &outBrushList, smartPtr <brush> b);
	//	void			csgDifference (std::vector<smartPtr <brush> > &outBrushList, smartPtr <brush> b);
	//
	
	//	methods from editor brush
		void		setOwnerEnt (smartPtr <entity> ent);
		smartPtr <entity>	getOwnerEnt (void) const;
		void		link (smartPtr <entity> ent);
		void		unlink (void);
		void		select (bool sel);
		bool		isSelected (void) const;
		
		// selects/deselects given face
		void		selectFace (int nface, bool sel);
		
		// returns selection state of a given face
		bool		isFaceSelected (int nface) const;
	
		/**
		 * moves specified faces by specified direction vector
		 * @param faces indexes of faces to be moved
		 * @param dir offset vector
		 * @return constrained offset vector
		 */
		fe::vector3		moveFaces (const std::vector <int> &faces, const fe::vector3 &dir);
		
		// moves object by specified vector
	//	void		move (const vector3 &dir);	// see snap version
		
		// FUNCTION IS OBSOLETE. rotates object around specified axis, uses brush pivot point as origin
		void		rotate (float x, float y, float z, float angle);
		
		// rotates object around specified axis and origin
		void		rotate (const fe::vector3 &axis, const fe::vector3 &origin, float angle);
		
		// splits brush by face
		void		split (const brushFace &f, std::list<smartPtr <brush> > &results);

		// clips brush with plane, returns false in clipped away
		bool		clip (const vector3 *planePoints);
		
		// tests intersection with specified ray. REQUIRED for selection in perspective mode.
		bool		testRayIntr (const fe::ray3 &ray) const;
		
		// finds point and face of intersection of a brush with specified ray. required for selection
		int			findRayIntr (const fe::ray3 &ray, fe::vector3 &pt) const;
		
		// tests intersection with specified point. REQUIRED for selection in ortho mode.
		bool		testPointIntr (fe::point pt) const;
		
		// renders object using specified viewport parms
		void		render (const editorViewport *pViewport) const;
	
		void debug (void) const;
	
		// renderableSceneObject
		
		virtual int numSubsets (void) const;
		virtual fe::materialPtr	getMtl (int subset) const;
		virtual void prepare (int subset) const;
		virtual void render (int subset, int pass) const;
	
		void invalidateRendererCache (void);

	};
	
	typedef smartPtr <brush> brushPtr;
	
}

#endif // __F_BRUSH_H

