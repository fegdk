#include "pch.h"
#include <fegdk/f_parser.h>
#include <fegdk/f_engine.h>
#include "editormenu.h"
#include "entity.h"
#include "editorcore.h"
#include "brush.h"
#include "mainfrm.h"
#include "logview.h"
#include "editorcmd.h"
#include "editormap.h"

namespace feditor
{

	editorMenu::editorMenu (const char *name)
	{
		setName (name);
	
		/*
		 * each entity has following standard properties:
		 *  classname
		 *  color
		 *  bounding box
		 *  flags
		 *  comments
		 * written al follows:
		 * def "classname" (r g b) (x1 y1 z1) (x2 y2 z2) flag1 flag2 flag3 ... {
		 *  // comments (until '}')
		 * }
		 */
	
	#if 0
		// TODO: make own format, not quake
		try {
			charParser p (NULL, "entities.def", true);
			for (;;)
			{
			}
		}
		catch (parserException e)
		{
		}
	#endif
	
		fprintf (stderr, "creating entity menu..\n");
		const std::vector< eClass >& eclasses = eClass::getEClass ();
		fprintf (stderr, "%d eclasses loaded..\n", eclasses.size ());
		// create menu
		mpWidget = gtk_menu_new ();
	
		GtkWidget *item;
		
		item = gtk_tearoff_menu_item_new ();
		gtk_widget_show (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (mpWidget), item);
		
		item = gtk_menu_item_new_with_label ("Unlink");
		gtk_widget_show (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (mpWidget), item);
		g_signal_connect (G_OBJECT (item), "activate", G_CALLBACK (unlinkSelected), this);
	
		item = gtk_separator_menu_item_new ();
		gtk_widget_show (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (mpWidget), item);
	
		std::map <cStr, GtkWidget *> submenus;
		for (std::vector<eClass>::const_iterator it = eclasses.begin (); it != eclasses.end (); it++)
		{
			GtkWidget *target = mpWidget;
			cStr s = (*it).getName ();
			const char *ptr;
			if (ptr = strstr (s.c_str (), "_"))
			{
				// check submenus
				char nm[100];
				strncpy (nm, s.c_str (), ptr - s.c_str ());
				nm[ptr - s.c_str ()] = 0;
				std::map <cStr, GtkWidget *>::iterator it = submenus.find (nm);
				if (it == submenus.end ())
				{
					// item in root menu
					item = gtk_menu_item_new_with_label (nm);
					gtk_widget_show (item);
					gtk_menu_shell_append (GTK_MENU_SHELL (mpWidget), item);
					
					// submenu linked to item
					target = gtk_menu_new ();
					gtk_widget_show (target);
					gtk_menu_set_title (GTK_MENU (target), nm);
					gtk_menu_item_set_submenu (GTK_MENU_ITEM (item), target);
	
					submenus[nm] = target;
					
	/*				// tearoff handle
					item = gtk_tearoff_menu_item_new ();
					gtk_widget_show (item);
					gtk_menu_shell_append (GTK_MENU_SHELL (target), item);*/
				}
				else
					target = (*it).second;
			}
			item = gtk_menu_item_new_with_label (s.c_str ());
			g_signal_connect_after (G_OBJECT (item), "activate", G_CALLBACK (activateMenu), this);
			gtk_widget_show (item);
			gtk_menu_shell_append (GTK_MENU_SHELL (target), item);
			mItemNames[GTK_MENU_ITEM (item)] = s;
		}
	
	}
	
	editorMenu::~editorMenu (void)
	{
	}
	
	GtkWidget *editorMenu::getWidget (void) const
	{
		return mpWidget;
	}
	
	void editorMenu::activateMenu (GtkMenuItem *menuitem, gpointer user_data)
	{
		editorMenu *menu = (editorMenu *)user_data;
		const brushList &lst = g_editor->getMap ()->getSelection ();
		std::map <GtkMenuItem *, cStr>::iterator label = menu->mItemNames.find (menuitem);
		if (label == menu->mItemNames.end ())
			return;
		
		const eClass *eclass = eClass::getEClassByName ((*label).second.c_str ());
		if (g_editor->canCreateEntityFromSelection (eclass))
		{
			g_editor->addCommand (new CmdCreateEntity (eclass));
			g_editor->redrawAllViews ();
		}
	}
	
	void editorMenu::unlinkSelected (GtkMenuItem *menuitem, gpointer user_data)
	{
		std::set <smartPtr <entity> > ents;
	
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = *i;
			if (b->getOwnerEnt ()->getEClass ()->isFixedSize ()
				|| b->getOwnerEnt () == g_editor->getMap ()->getWorldEntity ())
			{
				continue;
			}
			ents.insert (b->getOwnerEnt ());
		}
	
		if (ents.empty ())
		{
			g_editor->getMainFrm ()->getLogView ()->print ("no entities selected\n");
		}
		else
			g_editor->addCommand (new CmdUngroupEntity (ents));
	}
	
}
