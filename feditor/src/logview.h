#ifndef __F_LOGVIEW_H
#define __F_LOGVIEW_H

#include <gtk/gtk.h>

namespace feditor
{

	class logView
	{
	
	private:
		
		GtkWidget*		mpWidget;
	
	public:
	
		logView (void);
		~logView (void);
	
		void print (const char *text);
		void setLogFile (const char *fname);
		GtkWidget*	getWidget (void);
		
	};
	
}

#endif // __F_LOGVIEW_H

