#ifndef __F_GLVIEW_H
#define __F_GLVIEW_H

#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <fegdk/f_baseobject.h>
#include "feditor.h"
#include "editorvp.h"

namespace feditor
{

	class glView
	{
	
	protected:
	
		smartPtr <editorViewport>		mpViewport;
		GtkWidget*		mpWidget;	// opengl widget
		
	public:
			
		glView (void);
		~glView (void);
		GtkWidget*		getWidget (void);
	
		virtual void init (void) = 0;
		virtual void reshape (void) = 0;
		virtual void draw (void) = 0;
	
		virtual void beginGL (void);
		virtual void endGL (bool swapbuffers = true);
		smartPtr <editorViewport>	getViewport (void) const;
	
		bool isVisible (void) const;
	
	private:
	
		static gboolean gtkReshape (GtkWidget *widget, GdkEventConfigure *event,
						gpointer data);
		
		static void gtkInit (GtkWidget *widget, gpointer data);
		
		static void gtkDraw (GtkWidget *widget, GdkEventExpose *event,
						gpointer data);
		
	};
	
}

#endif // __F_GLVIEW_H

