#include "pch.h"
#include <stdio.h>
#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include "glview.h"
#include "editorvp.h"
#include "editorcore.h"
#include "mainfrm.h"
#include "logview.h"
#include <GL/gl.h>
#include <GL/glu.h>

namespace feditor
{

	GdkGLContext *shareCtx = NULL;
	
	glView::glView (void)
	{
		mpViewport = NULL;
	
		mpWidget = gtk_drawing_area_new ();
		
		GdkGLConfig *glconfig;
		glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |
					GDK_GL_MODE_DEPTH | GDK_GL_MODE_DOUBLE));
		GdkGLContext *share = shareCtx;//mpGLBase ? gtk_widget_get_gl_context (mpGLBase) : NULL;
		gtk_widget_set_gl_capability (mpWidget, glconfig, share, TRUE,
			GDK_GL_RGBA_TYPE);
		gtk_widget_set_events (mpWidget, GDK_DESTROY | GDK_EXPOSURE_MASK
			| GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK
			| GDK_POINTER_MOTION_MASK | GDK_SCROLL_MASK);
	
		g_signal_connect_after (G_OBJECT (mpWidget), "realize",
						G_CALLBACK (gtkInit), this);
		g_signal_connect (G_OBJECT (mpWidget), "expose_event",
						G_CALLBACK (gtkDraw), this);
		g_signal_connect (G_OBJECT (mpWidget), "configure_event",
						G_CALLBACK (gtkReshape), this);
		gtk_widget_show (mpWidget);
		
		mpViewport = new editorViewport (this);
	
		g_editor->registerView (this);
	}
	
	glView::~glView (void)
	{
		g_editor->unregisterView (this);
	}
	
	GtkWidget*		glView::getWidget (void)
	{
		return mpWidget;
	}
	
	void glView::beginGL (void)
	{
		GdkGLContext *glcontext = gtk_widget_get_gl_context (mpWidget);
	   	GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (mpWidget);
		if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext))
			return /*FALSE*/;
	}
	
	void glView::endGL (bool swapbuffers)
	{
	   	GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (mpWidget);
		if (swapbuffers)
			gdk_gl_drawable_swap_buffers (gldrawable);
	//	gdk_gl_drawable_gl_end (gldrawable);
	}
	
	gboolean glView::gtkReshape (GtkWidget *widget,
						GdkEventConfigure *event, gpointer data)
	{
		glView *view = reinterpret_cast <glView *> (data);
		view->reshape();
		return TRUE;
	}
		
	void glView::gtkInit (GtkWidget *widget, gpointer data)
	{
		glView *view = reinterpret_cast <glView *> (data);
		view->beginGL ();
		view->init();
		view->endGL (false);
		if (!shareCtx)
		{
			shareCtx = gtk_widget_get_gl_context (widget);
		}
	/*	GdkGLContext *glcontext = gtk_widget_get_gl_context (view->mpWidget);
	   	GdkGLDrawable *gldrawable = gtk_widget_get_gl_drawable (view->mpWidget);
		if (gdk_gl_drawable_gl_begin (gldrawable, glcontext))
		{
			glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		}*/
	}
		
	void glView::gtkDraw (GtkWidget *widget, GdkEventExpose *event,
						gpointer data)
	{
		glView *view = reinterpret_cast <glView *> (data);
		view->draw();
	}
	
	smartPtr <editorViewport>	glView::getViewport (void) const
	{
		return mpViewport;
	}
	
	bool glView::isVisible (void) const
	{
			return (GTK_WIDGET_REALIZED (mpWidget) && (mpWidget->object.flags & GTK_VISIBLE)) ? true : false;
	}
	
}
