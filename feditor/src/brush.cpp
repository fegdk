#include "pch.h"
#include <fegdk/f_error.h>
#include <fegdk/f_texture.h>
#include <fegdk/f_material.h>
#include <fegdk/f_types.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_shader.h>
#include <fegdk/f_effect.h>
#include <fegdk/f_resourcemgr.h>
#include <fegdk/f_sceneobject.h>
#include <algorithm>
#include <GL/glew.h>
#include "brush.h"
#include "editorcore.h"
#include "entity.h"
#include "utility.h"
#include "editormap.h"

//------------------------------
// feditor brush implementation
//------------------------------

#if 0
ulong brush::BrushColors[brush::btc_max] = {
	// active
	0xffaf0000,		// dark red
	0xffff0000,		// brightred

	// subtract
	0xff00afaf,		// dark magenta
	0xff00ffff,		// bright magenta

	// add
	0xffafaf00,		// dark yellow
	0xffffff00,		// bright yellow

	// intersect
	0xff00af00,		// dark green
	0xff00ff00,		// bright green

	// deintersect
	0xff0000af,		// dark blue
	0xff0000ff,		// bright blue
};
#endif

namespace feditor
{

	void brush::init (void)
	{
		mpMesh = new brushMesh;
		fe::renderable *rend = new fe::renderable (mpMesh);
		attachRenderable (rend);
		mpOwnerEnt = NULL; // g_editor->getWorldEntity ();
		mbSelected = false;
		mbNeedRecache = true;
	}
	
	brush::brush (void)
		: fe::sceneObject (NULL)
		, mPyObject ("fe.brush")
	{
		init ();
	}
	
	brush::brush (const brush &b)
		: fe::sceneObject (NULL)
		, mPyObject ("fe.brush")
	{
		init ();
		mFaces = b.mFaces;
		mMins = b.mMins;
		mMaxs = b.mMaxs;
		mpOwnerEnt = b.mpOwnerEnt;
		mFaceSelection = b.mFaceSelection;
	}
	
	brush::brush (const vector3 &mins, const vector3 &maxs)
		: fe::sceneObject (NULL)
		, mPyObject ("fe.brush")
	{
		init ();
		create (mins, maxs);
	}
	
	brush::brush (const std::vector< brushFace > &brushFaces)
		: fe::sceneObject (NULL)
		, mPyObject ("fe.brush")
	{
		init ();
		clear ();
		mFaces = brushFaces;
		build ();
	}
	
	brush::~brush (void)
	{
	}
	
	PyObject *brush::getPyObject (void)
	{
		return mPyObject.getForObject (this);
	}
	
	void	brush::decrefPyObject (void)
	{
		mPyObject.decref ();
	}
	
	void		brush::setOwnerEnt (smartPtr <entity> ent)
	{
		mpOwnerEnt = ent;
	}
	
	smartPtr <entity>		brush::getOwnerEnt (void) const
	{
		return mpOwnerEnt;
	}
	
	void		brush::link (smartPtr <entity> ent)
	{
		mpOwnerEnt = ent;
	}
	
	void		brush::unlink (void)
	{
		mpOwnerEnt = NULL;
	}
	
	#if 0
	void		brush::move(const vector3 &dir)
	{
		move(dir);
	//	mPivotPoint += dir;
	}
	#endif
	
	void		brush::rotate (float x, float y, float z, float angle)
	{
		// move brush to origin, rotate, move back
		matrix4 m;
		m.rotateAroundAxis (vector3 (x, y, z), angle);
	
		vector3 mPivotPoint = (mMins + mMaxs) * .5f;
	
		for (int i = 0; i < numFaces (); i++)
		{
			faces (i).planePt (0) = (faces (i).planePt (0) - mPivotPoint) * m + mPivotPoint;
			faces (i).planePt (1) = (faces (i).planePt (1) - mPivotPoint) * m + mPivotPoint;
			faces (i).planePt (2) = (faces (i).planePt (2) - mPivotPoint) * m + mPivotPoint;
		}
	
		build (false);
	}
	
	// rotates object around specified axis and origin
	void		brush::rotate (const vector3 &axis, const vector3 &origin, float angle)
	{
		// move brush to origin, rotate, move back
		matrix4 m;
		m.rotateAroundAxis (axis, angle);
	
		for (int i = 0; i < numFaces (); i++)
		{
			faces (i).planePt (0) = (faces (i).planePt (0) - origin) * m + origin;
			faces (i).planePt (1) = (faces (i).planePt (1) - origin) * m + origin;
			faces (i).planePt (2) = (faces (i).planePt (2) - origin) * m + origin;
		}
	
		build (false);
	}
	
	void		brush::split (const brushFace &f, std::list<smartPtr <brush> > &results)
	{
		// print debug info about brush being slit
		smartPtr <brush> front, back;
		splitByFace(f, front, back);
		if (front)
		{
			front->removeEmptyFaces();
			bool discard = false;
			if (front->numFaces() && !discard)
			{
				smartPtr <brush> o = new brush(*front);
	
				// !EXPERIMENTAL! preserve face selection
	#if 0
				for (int i = 0; i < o->numFaces(); i++)
				{
					const plane &p = o->faces(i).plane();
					for (int j = 0; j < numFaces(); j++)
					{
						if (p.normal() == faces(j).plane().normal() && p.constant() == faces(j).plane().constant())
						{
							o->selectFace(i, isFaceSelected(j));
						}
					}
				}
	#endif
	
				results.push_back(o);
			}
		}
		if (back)
		{
			back->removeEmptyFaces();
			bool discard = false;
			if (back->numFaces() && !discard)
			{
				smartPtr <brush> o = new brush(*back);
	#if 0
				// !EXPERIMENTAL! preserve face selection
				for (int i = 0; i < o->numFaces(); i++)
				{
					const plane &p = o->faces(i).plane();
					for (int j = 0; j < numFaces(); j++)
					{
						if (p.normal() == faces(j).plane().normal() && p.constant() == faces(j).plane().constant())
						{
							o->selectFace(i, isFaceSelected(j));
						}
					}
				}
	#endif
	
				results.push_back(o);
			}
		}
	}
	
	bool		brush::testRayIntr(const ray3 &ray) const
	{
		return testIntersection (ray);
	}
	
	int		brush::findRayIntr(const ray3 &ray, vector3 &pt) const
	{
		vector3 v1 = ray.origin();
		vector3 v2 = ray.origin() + ray.direction() * 1.0e+4f*2;
//		fprintf (stderr, "intr: %0.2f %0.2f %0.2f | %0.2f %0.2f %0.2f\n", v1.x, v1.y, v1.z, v2.x, v2.y, v2.z);
	
		int minface = 0;
		float mindist = -1.0e+5f;
	
		for (int i = 0; i < numFaces(); i++)
		{
/*			if (ray.direction().dot(faces(i).plane().normal()) > 0)
			{
				fprintf (stderr, "%d is backface\n", i);
				continue;
			}*/

			if (!clipLineWithPlane(v1, v2, faces(i).plane()))
			{
//				fprintf (stderr, "clipped away by face %d\n", i);
				return -1;
			}
	
			float d1, d2;
			float d;
			d1 = (v1 - ray.origin()).length();
			d2 = (v2 - ray.origin()).length();
//			fprintf (stderr, "f=%d, d1=%0.2f, d2=%0.2f\n", i, d1, d2);

			if (d1 < d2)
				d = d1;
			else
				d = d2;

			if (d > mindist)
			{
				mindist = d;
				minface = i;
			}
		}
	
		pt = v1;
		return minface;
	}
	
	bool		brush::testPointIntr(point pt) const
	{
		smartPtr <editorViewport> vp = g_editor->getViewport ();
		vector3 origin = vp->getEyePos();
		editorViewport::orthoMode m = vp->getOrthoMode();
		int nDim1 = (m == editorViewport::yz) ? 1 : 0;
		int nDim2 = (m == editorViewport::xy) ? 1 : 2;
		vector3 pos(0, 0, 0);
		pos[nDim1] = (pt.x - vp->getWidth() / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
		pos[nDim2] = (-pt.y + vp->getHeight() / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
	
		ray3 r;
		r.direction() = vector3(1, 1, 1);
		r.direction()[nDim1] = 0;
		r.direction()[nDim2] = 0;
		r.origin() = vector3(-1.0e+5f, -1.0e+5f, -1.0e+5f);
		r.origin()[nDim1] = pos[nDim1];
		r.origin()[nDim2] = pos[nDim2];
	
		bool intr = testIntersection (r);
		return intr;
	}
	
	bool brush::testIntersection (const ray3 &ray) const
	{
		// clip ray through all planes, check what remained inside
	    
		vector3 v1 = ray.origin();
		vector3 v2 = ray.origin() + ray.direction() * 1.0e+5f * 2;
	
		for (int i = 0; i < numFaces(); i++)
		{
			if (!clipLineWithPlane(v1, v2, faces(i).plane()))
				return false;
		}
	
		return true;
	}
	
	#if 0
	struct Vx {
		const static uint fvf = D3DFVF_XYZ;
		float x, y, z;
	};
	#endif
	
	static bool aab_intr(const vector3 &mins1, const vector3 &maxs1, const vector3 &mins2, const vector3 &maxs2)
	{
		if (std::max(mins1.x, mins2.x) <= std::min(maxs1.x, maxs2.x)
			&& std::max(mins1.y, mins2.y) <= std::min(maxs1.y, maxs2.y)
			&& std::max(mins1.z, mins2.z) <= std::min(maxs1.z, maxs2.z))
			return true;
		else
			return false;
	}
	
	static float SetShadeForPlane (const fe::plane &p)
	{
		int		i;
		float	f;
		float	lightaxis[3] = { 0.6f, 0.8f, 1.0f };
	
		// axial plane
		for (i=0; i<3; i++)
			if (fabs(p.normal()[i]) > 0.9f)
			{
				f = lightaxis[i];
				return f;
			}
	
		// between two axial planes
		for (i=0; i<3; i++)
			if (fabs(p.normal()[i]) < 0.1f)
			{
				f = (lightaxis[(i + 1) % 3] + lightaxis[ (i + 2) % 3]) / 2;
				return f;
			}
	
		// other
		f = (lightaxis[0] + lightaxis[1] + lightaxis[2]) / 3;
		return f;
	}
	
	void		brush::render (const editorViewport *vp) const
	{
		vector3 drawcolor;
		vector3 seldrawcolor;
		if (mpOwnerEnt && mpOwnerEnt->getEClass ())
		{
			drawcolor = mpOwnerEnt->getEClass()->getColor ();
			if (vp->getViewMode() == editorViewport::perspective && drawcolor == vector3 (0, 0, 0))
				drawcolor = vector3 (1, 1, 1);
		}
		else
		{
			drawcolor = vector3 (1, 1, 1);
		}
		if (vp->getViewMode () == editorViewport::ortho)
		{
			editorViewport::orthoMode mode = vp->getOrthoMode();
			int nDim1 = (mode == editorViewport::yz) ? 1 : 0;
			int nDim2 = (mode == editorViewport::xy) ? 1 : 2;
			int nDim3 = (mode == editorViewport::xz) ? 1 : ((mode == editorViewport::xy) ? 2 : 0);
			
			// render brush
			for (int i = 0; i < numFaces(); i++)
			{
				const brushFace& f = faces(i);
				const brushWinding &w = f.winding();
				if (w.numPoints() == 0)
					continue;
	
				assert(w.numPoints() + 1  <= 200);
				if (w.numPoints() + 1 > 200)
					continue;
	
				vector3 n (f.plane().normal()[nDim1], f.plane().normal()[nDim2], f.plane().normal()[nDim3]);
				if (n.z >= 0)
					continue;
	
				if (isFaceSelected (i))
				{
					glLineWidth (2);
					glLineStipple (1, 0x3333);
					glEnable (GL_LINE_STIPPLE);
	
					if (g_editor->rotateGetMode ())
						glColor4f (1, 0, 1, 1);
					else
						glColor4f (1, 0, 0, 1);
				}
				else
				{
					glColor4f (drawcolor.x, drawcolor.y, drawcolor.z, 1);
				}
				vector3 origin (0, 0, 0);
				glBegin (GL_LINE_STRIP);
				for (int j = 0; j <= w.numPoints(); j++)
				{
					if (j == w.numPoints())
					{
						glVertex2f (w.point (0)[nDim1], w.point (0)[nDim2]);
	//					origin += w.point (0);
					}
					else
					{
						glVertex2f (w.point (j)[nDim1], w.point (j)[nDim2]);
	//					origin += w.point (j);
					}
				}
	//			origin /= w.numPoints () + 1;
				glEnd ();
				if (isFaceSelected (i))
				{
					glLineWidth (1);
					glDisable (GL_LINE_STIPPLE);
				}
	/*			// draw normal
				printf ("origin: %f %f %f, normal: %f %f %f\n", origin.x, origin.y, origin.z, f.plane ().normal ().x, f.plane ().normal ().y, f.plane ().normal ().z);
				glBegin (GL_LINES);
				glVertex2f (origin[nDim1], origin[nDim2]);
				origin += f.plane ().normal () * 16;
				glVertex2f (origin[nDim1], origin[nDim2]);
				glEnd ();	*/
			}
			
		}
	#if 0
		Interface *ip = g_pGlobals->mpInterface;
		engine *engine = ip->GetEngine();
		ViewportParms *vp = checked_cast<ViewportParms *>(engine->viewport());
	
		assert(mpOwnerEnt);
		vector3 drawcolor;
		vector3 seldrawcolor;
		if (mpOwnerEnt && mpOwnerEnt->GetEClass())
		{
			drawcolor = mpOwnerEnt->GetEClass()->GetColor();
			if (vp->GetViewMode() == ViewportParms::VM_PERSPECTIVE && drawcolor == vector3(0, 0, 0))
				drawcolor = vector3(1, 1, 1);
		}
		else
		{
			drawcolor = vector3(1, 1, 1);
		}
	
		if (vp->GetViewMode() == ViewportParms::VM_ORTHO)
		{
			ViewportParms::orthoMode mode = vp->GetOrthoMode();
			int nDim1 = (mode == editorViewport::yz) ? 1 : 0;
			int nDim2 = (mode == editorViewport::xy) ? 1 : 2;
			int nDim3 = (mode == editorViewport::xz) ? 1 : ((mode == editorViewport::xy) ? 2 : 0);
			// render brush
			matrix4 m;
			m.identity();
			engine::get()->effectParms()->setMatrix(effectParmWorldMatrix, m);
	
			// better way:
			vertex *buf = reinterpret_cast< vertex * >(engine::get()->tmpBuffer(200 * sizeof(vertex)));
	
	
			for (int i = 0; i < numFaces(); i++)
			{
				const brushFace& f = faces(i);
				const brushWinding &w = f.winding();
				if (w.numPoints() == 0)
					continue;
	
				assert(w.numPoints() + 1  <= 200);
				if (w.numPoints() + 1 > 200)
					continue;
	
				vector3 n(f.plane().normal()[nDim1], f.plane().normal()[nDim2], f.plane().normal()[nDim3]);
				if (n.z >= 0)
					continue;
	
				vertex *lockedVerts = buf;
				for (int j = 0; j <= w.numPoints(); j++)
				{
	
					if (j == w.numPoints())
					{
						lockedVerts->pos[0] = w.point(0)[nDim1];
						lockedVerts->pos[1] = w.point(0)[nDim2];
					}
					else
					{
						lockedVerts->pos[0] = w.point(j)[nDim1];
						lockedVerts->pos[1] = w.point(j)[nDim2];
					}
					lockedVerts->pos[2] = 0.1f;
	
					uchar *pclr = (uchar *)&lockedVerts->color;
					pclr[0] = drawcolor[2] * 0xff;
					pclr[1] = drawcolor[1] * 0xff;
					pclr[2] = drawcolor[0] * 0xff;
					pclr[3] = 0xff;
					lockedVerts++;
				}
				if (isFaceSelected(i))
				{
					std::vector< vector3 > verts;
					verts.resize(w.numPoints()+1);
					for (int j = 0; j <= w.numPoints(); j++)
					{
						if (j == w.numPoints())
						{
							verts[j][0] = w.point(0)[nDim1];
							verts[j][1] = w.point(0)[nDim2];
						}
						else
						{
							verts[j][0] = w.point(j)[nDim1];
							verts[j][1] = w.point(j)[nDim2];
						}
						verts[j][2] = 0.1f;
					}
					d3DUtil::drawLine(2, &verts.front(), w.numPoints(), 0xffff0000);
				}
				else
					d3DUtil::drawPrimitive<vertex>(NULL, NULL, D3DPT_LINESTRIP, buf, w.numPoints() + 1, w.numPoints());
			}
		}
		else
		{
			matrix4 m;
			if (mpOwnerEnt == g_pGlobals->mpInterface->GetWorldEntity())
				drawcolor = vector3(1, 1, 1);
			vector3 eye = pViewport->GetCamPosition();
			eye.x = -eye.x;
			eye.y = -eye.y;
			eye.z = -eye.z;
			// check if brush is outsided of camera box
			vector3 mins = eye + vector3(-1000, -1000, -1000);
			vector3 maxs = eye + vector3(1000, 1000, 1000);
			if (!aab_intr(mins, maxs, mMins, mMaxs))
				return;
			// check if brush is behind the camera
			vector3 points[] = {
				mMins,
				mMaxs,
				vector3(mMaxs.x, mMins.y, mMins.z),
				vector3(mMaxs.x, mMins.y, mMaxs.z),
				vector3(mMaxs.x, mMaxs.y, mMins.z),
				vector3(mMins.x, mMaxs.y, mMaxs.z),
				vector3(mMins.x, mMins.y, mMaxs.z),
				vector3(mMins.x, mMaxs.y, mMins.z),
			};
			int i;
			for (i = 0; i < 8; i++)
			{
				vector3 pt = points[i] * m;
				if (pt.z > 0)
					break;
			}
			if (i == 8)
				return;
	
			m.identity();
			engine::get()->effectParms()->setMatrix(effectParmWorldMatrix, m);
			// draw textured faces
	#if 1
			for (int i = 0; i < numFaces(); i++)
			{
				const brushFace& f = faces(i);
				float shade = SetShadeForPlane(f.plane());
				vector3 clr = drawcolor * shade;
	
				material *mtl = f.material();
	/*			if (mtl && (strstr(mtl->name(), "caulk")
					|| strstr(mtl->name(), "common")
					|| strstr(mtl->name(), "clip")
					|| strstr(mtl->name(), "trig")
					|| strstr(mtl->name(), "portal")
					|| strstr(mtl->name(), "hint")))
					continue;*/
	
				const brushWinding &w = f.winding();
				if (w.numPoints() == 0)	// freed face
					continue;
	
				// build vb
				heapBuffer<vertex> buf(w.numPoints());
				vertex *lockedVerts = buf + w.numPoints() - 1;
				for (int j = w.numPoints() - 1; j >= 0; j--)
				{
					lockedVerts->pos = w.point(j);
					uchar *pclr = (uchar *)&lockedVerts->color;
					pclr[0] = clr[2] * 0xff;
					pclr[1] = clr[1] * 0xff;
					pclr[2] = clr[0] * 0xff;
					pclr[3] = 0xff;
					lockedVerts->uv = f.winding().texCoord(j);
					lockedVerts--;
				}
				// FIXME: it was a hack anyway, should be fixed on the engine side
				engine::get()->renderer()->getRendererData()->getDevice()->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
				d3DUtil::drawPrimitive<vertex>(f.material(), NULL, D3DPT_TRIANGLEFAN, buf, w.numPoints(), w.numPoints() - 2);
			}
	#endif
			// draw selection if any
			if (isSelected())
			{
				// draw wireframe
				// move proj matrix a bit
	//			mpLineShader3d->setBestTechnique();
				for (int i = 0; i < numFaces(); i++)
				{
					if (!isFaceSelected(i))
						continue;
					const brushFace& f = faces(i);
					const brushWinding &w = f.winding();
					if (w.numPoints() == 0)
						continue;
	
					vertex *lockedVerts = reinterpret_cast< vertex * >(engine->tmpBuffer((w.numPoints()+1) * sizeof(vertex) * 2));
					vertex *lockedVertsOverlay = lockedVerts + w.numPoints() + 1;
					vertex *vertexptr = lockedVerts;
					vertex *overlayptr = lockedVertsOverlay;
	
					for (int j = 0; j <= w.numPoints(); j++)
					{
						if (j == w.numPoints())
							lockedVerts->pos = w.point(0);
						else
							lockedVerts->pos = w.point(j);
	
						lockedVerts->color = BrushColors[mType * 2 + (isFaceSelected(i) ? 1 : 0)];
						*lockedVertsOverlay = *lockedVerts;
						lockedVertsOverlay->color = 0x7fff0000;
						lockedVerts++;
						lockedVertsOverlay++;
					}
					d3DUtil::drawPrimitive(NULL, NULL, D3DPT_TRIANGLEFAN, overlayptr, w.numPoints(), w.numPoints()-2, true);
					
					m.projection(M_PI/3.f, (float)vp->getHeight() / (float)vp->getWidth(), 1.01f, 10000.01f);
					engine::get()->effectParms()->setMatrix(effectParmProjMatrix, m);
	
	/*				D3DXMatrixPerspectiveFovLH((D3DXMATRIX*)&m, M_PI / 3.f, (float)vp->getWidth() / (float)vp->getHeight(), 1.01f, 10000.01f);
					engine::get()->renderer()->device()->SetTransform(D3DTS_PROJECTION, (D3DXMATRIX*)&m);*/
					d3DUtil::drawPrimitive(NULL, NULL, D3DPT_LINESTRIP, vertexptr, w.numPoints() + 1, w.numPoints());
	
					m.projection(M_PI/3.f, (float)vp->getHeight() / (float)vp->getWidth(), 1.f, 10000.f);
					engine::get()->effectParms()->setMatrix(effectParmProjMatrix, m);
	
	//				D3DXMatrixPerspectiveFovLH((D3DXMATRIX*)&m, M_PI / 3.f, (float)vp->getWidth() / (float)vp->getHeight(), 1.0f, 10000.0f);
	//				engine::get()->renderer()->device()->SetTransform(D3DTS_PROJECTION, (D3DXMATRIX*)&m);
				}
			}
		}
	#endif
	}
	
	void		brush::select(bool sel)
	{
		mbSelected = sel;

		int sz = numFaces ();
	
		if (sel)
			mFaceSelection = std::vector< bool >(sz, true);
		else
			mFaceSelection = std::vector< bool >(sz, false);

		fe::drawSurf_t *ds = mpRenderable->getDrawSurf(0);
		if (ds)
		{
			for (int i = 0; i < sz; i++)
			{
				if (sel)
					ds[i].appFlags |= 1;
				else
					ds[i].appFlags &= ~1;
			}
		}
	}
	
	bool		brush::isSelected(void) const
	{
		return mbSelected;
	}
	
	// selects/deselects given face
	void		brush::selectFace(int nface, bool sel)
	{
		if (mFaceSelection.empty())
			mFaceSelection = std::vector< bool >(numFaces(), false);
		mFaceSelection[nface] = sel;
/*		bool brushsel = std::find(mFaceSelection.begin(), mFaceSelection.end(), true) != mFaceSelection.end();
		if (brushsel != mbSelected)
		{
			if (brushsel)
			{
				g_editor->getMap ()->addSelection (this);
			}
			else
			{
				g_editor->getMap ()->removeSelection (this);
			}
		}*/
		fe::drawSurf_t *ds = mpRenderable->getDrawSurf(0);
		if (ds)
		{
			if (sel)
				ds[nface].appFlags |= 1;
			else
				ds[nface].appFlags &= ~1;
		}
	}
	
	// returns selection state of a given face
	bool		brush::isFaceSelected(int nface) const
	{
		if (mFaceSelection.empty())
			return false;
		return mFaceSelection[nface];
	}
	
	
	void		brush::build(bool bSnap)
	{
		buildWindings(bSnap);
		if ((int)mFaceSelection.size() != numFaces())
		{
			mFaceSelection = std::vector< bool >(numFaces(), mbSelected);
		}
		mbNeedRecache = true;
	/*	for (int f = 0; f < numFaces (); f++)
		{
			fprintf (stderr, "f: %d (%f %f %f %f)\n", f, faces (f).plane ().normal ().x, faces (f).plane ().normal ().y, faces (f).plane ().normal ().z, faces (f).plane ().constant ());
		}
	
		fprintf (stderr, "f0 dist to -300 0 0: %f\n", faces(0).plane ().distanceTo (vector3 (-300, 0, 0)));*/
	}
	
	//-----------------------------------------------
	//	texdef
	//-----------------------------------------------
	brushFaceTexdef::brushFaceTexdef()
	{
		scale[0] = scale[1] = 1;
		shift[0] = shift[1] = 0;
		rotate = 0;
	}
	
	//-----------------------------------------------
	//	winding
	//-----------------------------------------------
	
	brushWinding::brushWinding()
	{
	}
	
	brushWinding::brushWinding(const brushWinding &w)
	{
		mPoints = w.mPoints;
		mTexCoords = w.mTexCoords;
	}
	
	brushWinding::~brushWinding()
	{
	}
	
	void			brushWinding::baseForPlane(const fe::plane &p)
	{
		vector3	org, vright, vup(0, 0, 0);
		float max = -1.0e+5f;
		int x = -1;
		int i;
		float v;
		const vector3 &n = p.normal();
		for (i = 0; i < 3; i++)
		{
			v = fabs(((const float *)&n)[i]);
			if (v > max)
			{
				x = i;
				max = v;
			}
		}
		switch (x)
		{
		case 0:
		case 1:
			vup[2] = 1;
			break;		
		case 2:
			vup[0] = 1;
			break;		
		}
		v = vup.dot(n);
		vup = vup - v * n;
		vup.normalize ();
	
		org = p.normal() * p.constant();
		vright = vup.cross(p.normal());
	
		vup = vup * 1.0e+4f;
		vright = vright * 1.0e+4f;
	
		// project a really big	axis aligned box onto the plane
		mPoints.resize(4);
		mPoints[0] = org - vright;
		mPoints[0] = mPoints[0] + vup;
	
		mPoints[1] = org + vright;
		mPoints[1] = mPoints[1] + vup;
	
		mPoints[2] = org + vright;
		mPoints[2] = mPoints[2] - vup;
	
		mPoints[3] = org - vright;
		mPoints[3]= mPoints[3] - vup;
	}
	
	bool			brushWinding::clip (const fe::plane &split, bool keepon)
	{
	#define MAX_POINTS_ON_WINDING	100
	#define ON_EPSILON				0.00001f
	#define	SIDE_FRONT		0
	#define	SIDE_ON			2
	#define	SIDE_BACK		1
	#define	SIDE_CROSS		-2
		float	dists[MAX_POINTS_ON_WINDING];
		int		sides[MAX_POINTS_ON_WINDING];
		int		counts[3];
		float	dot;
		int		i, j;
		float	*p1, *p2;
		vector3	mid;
		brushWinding	neww;
		int		maxpts;
		
		counts[0] = counts[1] = counts[2] = 0;
	
		// determine sides for each point
		for (i = 0; i < numPoints(); i++)
		{
			dot = mPoints[i].dot(split.normal());
			dot -= split.constant();
			dists[i] = dot;
			if (dot > ON_EPSILON)
				sides[i] = SIDE_FRONT;
			else if (dot < -ON_EPSILON)
				sides[i] = SIDE_BACK;
			else
				sides[i] = SIDE_ON;
			counts[sides[i]]++;
		}
		sides[i] = sides[0];
		dists[i] = dists[0];
		
		if (keepon && !counts[SIDE_FRONT] && !counts[SIDE_BACK])
		{
			// leave winding unclipped
			return true;
		}
			
		if (!counts[SIDE_FRONT])
		{
			// clipped away
			return false;
		}
	
		if (!counts[SIDE_BACK])
		{
			// unclipped
			return true;
		}
		
		maxpts = numPoints() + 4;	// can't use counts[0]+2 because
									// of fp grouping errors
		neww.mPoints.reserve(maxpts);
			
		for (i = 0; i < numPoints(); i++)
		{
			p1 = mPoints[i];
			
			if (sides[i] == SIDE_ON)
			{
				neww.mPoints.push_back(p1);
				continue;
			}
		
			if (sides[i] == SIDE_FRONT)
			{
				neww.mPoints.push_back(p1);
			}
			
			if (sides[i+1] == SIDE_ON || sides[i+1] == sides[i])
			// no need to split, next point will be added
				continue;
				
			// generate a split point
			p2 = mPoints[ (i + 1) % numPoints() ];
			
			for (j=0 ; j<3 ; j++)
			{	// avoid round off error when possible
				if (split.normal()[j] == 1)
					mid[j] = split.constant();
				else if (split.normal()[j] == -1)
					mid[j] = -split.constant();
				else
				{
					dot = dists[i] / (dists[i]-dists[i+1]);
					mid[j] = p1[j] + dot * (p2[j]-p1[j]);
				}
			}
				
			neww.mPoints.push_back(mid);
		}
		
		// free the original winding
		(*this) = neww;
		
		return true;
	}
	
	
	//-----------------------------------------------
	// face
	//-----------------------------------------------
	
	brushFace::brushFace()
	{
		mpMtl = NULL;
	}
	
	brushFace::brushFace(const brushFace &f)
	{
		mpMtl = f.mpMtl;
		mPlane = f.mPlane;
		mPlanePts[0] = f.mPlanePts[0];
		mPlanePts[1] = f.mPlanePts[1];
		mPlanePts[2] = f.mPlanePts[2];
		mWinding = f.mWinding;
		mTexdef = f.mTexdef;
	}
	
	brushFace::~brushFace()
	{
	}
	
	// FIXME: where to move this stuff? it shouldn't be here..
	float	baseaxis[18][3] =
	{
		{0,0,1}, {1,0,0}, {0,-1,0},			// floor
		{0,0,-1}, {1,0,0}, {0,-1,0},		// ceiling
		{1,0,0}, {0,1,0}, {0,0,-1},			// west wall
		{-1,0,0}, {0,1,0}, {0,0,-1},		// east wall
		{0,1,0}, {1,0,0}, {0,0,-1},			// south wall
		{0,-1,0}, {1,0,0}, {0,0,-1}			// north wall
	};
	
	void textureAxisFromPlane(const fe::plane &pln, vector3 &xv, vector3 &yv)
	{
		int		bestaxis;
		float	dot,best;
		int		i;
		
		best = 0;
		bestaxis = 0;
		
		for (i=0 ; i<6 ; i++)
		{
			dot = pln.normal().dot(*((vector3 *)baseaxis[i*3]));
			if (dot > best)
			{
				best = dot;
				bestaxis = i;
			}
		}
		
		xv = (*((vector3 *)baseaxis[bestaxis*3+1]));
		yv = (*((vector3 *)baseaxis[bestaxis*3+2]));
	}
	
	void	brushFace::textureVectors(float STfromXYZ[2][4])
	{
		vector3	pvecs[2];
		int			sv, tv;
		float		ang, sinv, cosv;
		float		ns, nt;
		int			i,j;
		// get basemap from material
		smartPtr <fe::baseTexture> q = getMtl () ? getMtl ()->getStdMap () : NULL;
	
		brushFaceTexdef	*td = &texdef();
	
		memset(STfromXYZ, 0, 8 * sizeof(float));
	
		if (!td->scale[0])
			td->scale[0] = 0.5f;
		if (!td->scale[1])
			td->scale[1] = 0.5f;
	
		// get natural texture axis
		textureAxisFromPlane(plane(), pvecs[0], pvecs[1]);
	
		// rotate axis
		if (td->rotate == 0)
			{ sinv = 0 ; cosv = 1; }
		else if (td->rotate == 90)
			{ sinv = 1 ; cosv = 0; }
		else if (td->rotate == 180)
			{ sinv = 0 ; cosv = -1; }
		else if (td->rotate == 270)
			{ sinv = -1 ; cosv = 0; }
		else
		{	
			ang = td->rotate / 180.f * M_PI;
			sinv = sin(ang);
			cosv = cos(ang);
		}
	
		if (pvecs[0][0])
			sv = 0;
		else if (pvecs[0][1])
			sv = 1;
		else
			sv = 2;
					
		if (pvecs[1][0])
			tv = 0;
		else if (pvecs[1][1])
			tv = 1;
		else
			tv = 2;
						
		for (i=0 ; i<2 ; i++) {
			ns = cosv * pvecs[i][sv] - sinv * pvecs[i][tv];
			nt = sinv * pvecs[i][sv] +  cosv * pvecs[i][tv];
			STfromXYZ[i][sv] = ns;
			STfromXYZ[i][tv] = nt;
		}
	
		// scale
		for (i=0 ; i<2 ; i++)
			for (j=0 ; j<3 ; j++)
				STfromXYZ[i][j] = STfromXYZ[i][j] / td->scale[i];
	
		// shift
		STfromXYZ[0][3] = td->shift[0];
		STfromXYZ[1][3] = td->shift[1];
	
		for (j=0 ; j<4 ; j++) {
			STfromXYZ[0][j] /= q ? q.dynamicCast <fe::texture> ()->getWidth () : 64;
			STfromXYZ[1][j] /= q ? q.dynamicCast <fe::texture> ()->getHeight () : 64;
		}
	}
	
	vector2		brushFace::emitTexCoords(const vector3 &pt)
	{
		float STfromXYZ[2][4];
		textureVectors (STfromXYZ);
		vector2 out;
		out.x = pt.dot(STfromXYZ[0]) + STfromXYZ[0][3];
		out.y = pt.dot(STfromXYZ[1]) + STfromXYZ[1][3];
		return out;
	}
	
	void				brushFace::applyTexdef(void)
	{
		mpMtl = fe::g_engine->getResourceMgr ()->createMaterial (mTexdef.mtl);
		for (int w = 0; w < winding().numPoints(); w++)
			winding().texCoord(w) = emitTexCoords(winding().point(w));
	}
	
	void				brushFace::setMtl(const char *name)
	{
		mTexdef.mtl = name;
		applyTexdef();
	}
	
	fe::materialPtr		brushFace::getMtl () const
	{
		return mpMtl;
	}
	
	brushFaceTexdef&	brushFace::texdef ()
	{
		return mTexdef;
	}

	const brushFaceTexdef&	brushFace::texdef () const
	{
		return mTexdef;
	}
	
	//-----------------------------------------------
	// brush
	//-----------------------------------------------
	
	void	brush::create(const vector3 &mins, const vector3 &maxs)
	{
		mMins = mins;
		mMaxs = maxs;
	
		int		i, j;
		vector3	pts[4][2];
	
		for (i = 0; i < 3; i++)
		{
			if (mMaxs[i] < mMins[i])
				throw fe::genericError("brush::create error, inadequate bounds");
		}
	
		pts[0][0][0] = mMins[0];
		pts[0][0][1] = mMins[1];
		
		pts[1][0][0] = mMins[0];
		pts[1][0][1] = mMaxs[1];
		
		pts[2][0][0] = mMaxs[0];
		pts[2][0][1] = mMaxs[1];
		
		pts[3][0][0] = mMaxs[0];
		pts[3][0][1] = mMins[1];
		
		for (i = 0; i < 4; i++)
		{
			pts[i][0][2] = mMins[2];
			pts[i][1][0] = pts[i][0][0];
			pts[i][1][1] = pts[i][0][1];
			pts[i][1][2] = mMaxs[2];
		}
	
		mFaces.resize(6);
		brushFace *f;
		for (i = 0; i < 4; i++)
		{
	//		f->texdef = *texdef;
	//		f->texdef.flags &= ~SURF_KEEP;
	//		f->texdef.contents &= ~CONTENTS_KEEP;
	//		f->next = b->brush_faces;
	//		b->brush_faces = f;
			f = &mFaces[i];
			j = (i+1)%4;
			f->planePt(0) = pts[j][1];
			f->planePt(1) = pts[i][1];
			f->planePt(2) = pts[i][0];
		}
		
		f = &mFaces[4];
	//	f->texdef = *texdef;
	//	f->texdef->flags &= ~SURF_KEEP;
	//	f->texdef->contents &= ~CONTENTS_KEEP;
	//	f->next = b->brush_faces;
	//	b->brush_faces = f;
		f->planePt(0) = pts[0][1];
		f->planePt(1) = pts[1][1];
		f->planePt(2) = pts[2][1];
	
		f = &mFaces[5];
	//	f->texdef = *texdef;
	//	f->texdef->flags &= ~SURF_KEEP;
	//	f->texdef->contents &= ~CONTENTS_KEEP;
	//	f->next = b->brush_faces;
	//	b->brush_faces = f;
		f->planePt(0) = pts[2][0];
		f->planePt(1) = pts[1][0];
		f->planePt(2) = pts[0][0];
	
		build();
	}
	
	bool	brush::makeFaceWinding(int face)
	{
		brushWinding w;
		w.baseForPlane(mFaces[face].plane());
	
		fe::plane plane;
		bool past = false;
		for (size_t clip = 0; clip < mFaces.size (); clip++)
		{
			if ((int)clip == face)
			{
				past = true;
				continue;	// do not clip by itself
			}
			const fe::plane &facePlane = mFaces[face].plane();
			const fe::plane &clipPlane = mFaces[clip].plane();
			if (facePlane.normal().dot(clipPlane.normal()) > 0.999f
				&& fabs(facePlane.constant() - clipPlane.constant()) < 0.01f)
			{
				// identical plane, use the later one
				if (past)
				{
					// no winding
					return false;
				}
				continue;
			}
			// flip the plane, because we want to keep the back side
			plane.normal() = -clipPlane.normal();
			plane.constant() = -clipPlane.constant();
			if (!w.clip(plane, false))
				return false;
		}
		if (w.numPoints() < 3)
			return false;
	
		mFaces[face].mWinding = w;
	
		return true;
	}
	
	bool		brush::makeFacePlane(int face)
	{
		vector3 v1, v2;
		v1 = mFaces[face].mPlanePts[1] - mFaces[face].mPlanePts[0];
		v2 = mFaces[face].mPlanePts[2] - mFaces[face].mPlanePts[1];
		mFaces[face].mPlane.normal() = v2.cross(v1);
	    if (mFaces[face].mPlane.normal().normalize() < 0.1f)
			return false;
		mFaces[face].mPlane.constant() = mFaces[face].mPlanePts[0].dot(mFaces[face].mPlane.normal());
		return true;
	}
	
	void		brush::buildWindings(bool snap)
	{
		size_t face;
	
		mMins = vector3(1.0e+5f, 1.0e+5f, 1.0e+5f);
		mMaxs = vector3(-1.0e+5f, -1.0e+5f, -1.0e+5f);
	
		for (face = 0; face < mFaces.size(); face++)
		{
			makeFacePlane(face);
		}
		for (face = 0; face < mFaces.size(); face++)
		{
			if (!makeFaceWinding(face))
			{
				faces(face).winding().mPoints.clear();
				faces(face).winding().mTexCoords.clear();
				mMins = mMaxs = vector3(0, 0, 0);
			}
			else
			{
				faces(face).winding().mTexCoords.resize(faces(face).winding().numPoints());
				for (int i = 0; i < faces(face).winding().numPoints(); i++)
				{
					const vector3 &pt = faces(face).winding().point(i);
					for (int k = 0; k < 3; k++)
					{
						if (pt[k] < mMins[k])
							mMins[k] = pt[k];
						if (pt[k] > mMaxs[k])
							mMaxs[k] = pt[k];
					}
					faces(face).winding().texCoord(i) = faces(face).emitTexCoords(pt);
				}
			}
		}
	
	/*	// dump brush
		for (int f = 0; f < numFaces (); f++)
		{
			brushWinding &w = faces (f).winding ();
			for (int v = 0; v < w.numPoints (); v++)
			{
				fprintf (stderr, "v: %d (%f %f %f)\n", v, w.point (v).x, w.point (v).y, w.point (v).z);
			}
		}*/
	}
	
	void		brush::move(const vector3 &moveVec, bool snap)
	{
		for (int i = 0; i < numFaces(); i++)
		{
			for (int j = 0; j < 3; j++)
				mFaces[i].planePt(j) += moveVec;
		}
		mMins += moveVec;
		mMaxs += moveVec;
		build();
	}
	
	void		brush::splitByFace(const brushFace &f, smartPtr <brush> &front, smartPtr <brush> &back)
	{
		smartPtr <brush> b;
		b = new brush(*this);
		vector3	temp;
		b->addFace(f);
		b->build();
		b->removeEmptyFaces();
	
		if (b->numFaces())
			back = b;
	
		b = new brush(*this);
	
		// swap the plane winding
		brushFace nf = f;
		temp = nf.planePt(0);
		nf.planePt(0) = nf.planePt(1);
		nf.planePt(1) = temp;
		b->addFace(nf);
		b->build();
		b->removeEmptyFaces ();
		if (b->numFaces())
			front = b;
	}

	bool		brush::clip (const vector3 *planePoints)
	{
		if (mFaces.empty ())
			return false;
		brushFace f = mFaces[0];
		f.planePt(0) = planePoints[0];
		f.planePt(1) = planePoints[1];
		f.planePt(2) = planePoints[2];
		addFace (f);
		build ();
		removeEmptyFaces ();
		if (mFaces.empty ())
		{
			fprintf (stderr, "clipped-away!\n");
			return false;
		}
		return true;
	}
	
	void		brush::snapToGrid()
	{
	}
	
	void		brush::snapPlanePts()
	{
	}
	
	void		brush::rotate(const vector3 &angle, const vector3 &origin, bool build)
	{
	}
	
	void		brush::flip(const vector3 &axis, const vector3 &origin)
	{
	//	vector3 origin = (mins() + maxs()) / 2;
		for (int ff = 0; ff < numFaces(); ff++)
		{
			brushFace &f = faces(ff);
			vector3 newpts[3];
			for (int pp = 0; pp < 3; pp++)
			{
				vector3 dir = origin - f.planePt(pp);
				float dot = dir.dot(axis);
				float sign = (dot == 0) ? 0 : ((dot < 0) ? -1 : 1);
				dir.x *= axis.x;
				dir.y *= axis.y;
				dir.z *= axis.z;
				float d = dir.length();
				newpts[pp] = f.planePt(pp) + axis * d * 2 * sign;
			}
			f.planePt(0) = newpts[2];
			f.planePt(1) = newpts[1];
			f.planePt(2) = newpts[0];
		}
		build();
	}
	
	void		brush::removeEmptyFaces()
	{
		int i;
		for (;;)
		{
			int nf = numFaces();
			for (i = 0; i < nf; i++)
			{
				brushFace &f = mFaces[i];
				brushWinding &w = f.winding();
				if (w.numPoints() == 0)
					break;
			}
			if (i == nf)
				break;
			else
			{
				// remove face #i
				std::vector<brushFace>::iterator it = mFaces.begin() + i;
				mFaces.erase(it);
			}
		}
		if (numFaces() < 3)
			mFaces.clear();
	}
	
	void		brush::buildTexturing(void)
	{
		for (int face = 0; face < numFaces(); face++)
		{
			faces(face).applyTexdef();
		}
	}
	
	void		brush::makeSided (int sides, int axis)
	{
		mFaces.clear();
		int		i;
		vector3	mins, maxs;
		//		texdef_t	*texdef;
		brushFace	f;
		vector3	mid;
		float	width;
		float	sv, cv;
	
		if (sides < 3)
			throw("Bad sides number.");
	
	
		mins = mMins;
		maxs = mMaxs;
	
	#ifdef F_EDITOR
		switch(g_pParentWnd->ActiveXY()->GetViewType())
		{
		case XY: axis = 2; break;
		case XZ: axis = 1; break;
		case YZ: axis = 0; break;
		}
	#endif
	
		// find center of brush
		width = 8;
		for (i = 0; i < 3; i++)
		{
			mid[i] = (maxs[i] + mins[i]) * 0.5f;
			if (i == axis) continue;
			if ((maxs[i] - mins[i]) * 0.5 > width)
				width = (maxs[i] - mins[i]) * 0.5f;
		}
	
		// create top face
		f.planePt(2)[(axis+1)%3] = mins[(axis+1)%3]; f.planePt(2)[(axis+2)%3] = mins[(axis+2)%3]; f.planePt(2)[axis] = maxs[axis];
		f.planePt(1)[(axis+1)%3] = maxs[(axis+1)%3]; f.planePt(1)[(axis+2)%3] = mins[(axis+2)%3]; f.planePt(1)[axis] = maxs[axis];
		f.planePt(0)[(axis+1)%3] = maxs[(axis+1)%3]; f.planePt(0)[(axis+2)%3] = maxs[(axis+2)%3]; f.planePt(0)[axis] = maxs[axis];
		addFace(f);
	
		// create bottom face
		f.planePt(0)[(axis+1)%3] = mins[(axis+1)%3]; f.planePt(0)[(axis+2)%3] = mins[(axis+2)%3]; f.planePt(0)[axis] = mins[axis];
		f.planePt(1)[(axis+1)%3] = maxs[(axis+1)%3]; f.planePt(1)[(axis+2)%3] = mins[(axis+2)%3]; f.planePt(1)[axis] = mins[axis];
		f.planePt(2)[(axis+1)%3] = maxs[(axis+1)%3]; f.planePt(2)[(axis+2)%3] = maxs[(axis+2)%3]; f.planePt(2)[axis] = mins[axis];
		addFace(f);
	
		for (i=0 ; i<sides ; i++)
		{
			sv = sin (i*3.14159265*2/sides);
			cv = cos (i*3.14159265*2/sides);
	
			f.planePt(0)[(axis+1)%3] = floor(mid[(axis+1)%3]+width*cv+0.5);
			f.planePt(0)[(axis+2)%3] = floor(mid[(axis+2)%3]+width*sv+0.5);
			f.planePt(0)[axis] = mins[axis];
	
			f.planePt(1)[(axis+1)%3] = f.planePt(0)[(axis+1)%3];
			f.planePt(1)[(axis+2)%3] = f.planePt(0)[(axis+2)%3];
			f.planePt(1)[axis] = maxs[axis];
	
			f.planePt(2)[(axis+1)%3] = floor(f.planePt(0)[(axis+1)%3] - width*sv + 0.5);
			f.planePt(2)[(axis+2)%3] = floor(f.planePt(0)[(axis+2)%3] + width*cv + 0.5);
			f.planePt(2)[axis] = maxs[axis];
	
			addFace(f);
		}
	
		build();
	}
	
	void		brush::makeSidedCone (int sides)
	{
	}
	
	void		brush::makeSidedSphere (int sides)
	{
	}
	
	// subtracts this from a
	// stores resulting fragment(s) into list
	// FIXME: change std::vector<smartPtr <brush> > to std::vector<brush>
	void		brush::csgSubtract(std::vector<smartPtr <brush> > &out, smartPtr <brush> a)
	{
		smartPtr <brush> b = this;
		smartPtr <brush> in = a;
		smartPtr <brush> front, back;
	
		for (int f = 0; in && f < b->numFaces(); f++)
		{
			// check if we're need to split by this face - it can be out of bounds
			brushWinding &w = b->faces(f).winding();
			int ff;
			for (ff = 0; ff < in->numFaces(); ff++)
			{
				const brushFace &f = in->faces(ff);
				bool positive = false;
				int pt;
				for (pt = 0; pt < w.numPoints(); pt++)
				{
					if (f.plane().distanceTo(w.point(pt)) <= 0)
					{
						positive = true;
						break;
					}
				}
				if (positive == false)
					break;
			}
			if (ff < in->numFaces())
				continue;
	
			in->splitByFace(b->faces(f), front, back);
	//		if (in != a)
	//			delete in;
			if (front)
				out.push_back(front);
			in = back;
			if (!in)
				break;	// clipped away
		}
	
		//NOTE: in != a just in case brush b has no faces
	/*	if (in && in != a)
			delete in;
		else*/
		if (!(in && in != a))
		{
			// didn't really intersect
			out.clear();
			out.push_back(a);
		}
	}
	
	void brush::debug (void) const
	{
		fprintf (stderr, "brush 0x%X has %d refc\n", this, mRefc);
	}
	
	int brush::numSubsets (void) const
	{
		return mFaces.size ();
	}
	
	fe::materialPtr	brush::getMtl (int subset) const
	{
		if (getOwnerEnt ()->getEClass ()->isFixedSize ())
			return g_editor->getEntMtl ();
		return mFaces[subset].getMtl ();
	}
	
	void brush::prepare (int subset) const
	{
		// brushes are always defined in world-space, so matrix is always identity (set by caller)
		// there's really nothing to prepare, we'll just render
	}
	
	void brush::updateRendererCache (void)
	{
		if (!mbNeedRecache)
			return;	// cache is ok
		mbNeedRecache = false;
		mpMesh->update (this);
/*		mRendererCache.verts.clear ();
		mRendererCache.normals.clear ();
		mRendererCache.uvs.clear ();
		mRendererCache.subsets.clear ();
		mRendererCache.firstv.clear ();*/
	}
	
	void brush::render (int subset, int npass) const
	{
	#if 0
		fe::materialPtr mtl = getMtl (subset);
		if (!mtl)
			return;
		fe::effectPtr fx = NULL;
		if (mtl)
			fx = mtl->getEffect ();
	
		fe::shaderPtr vertProgram = fx->getVertexProgram (npass);
		
		// we can only draw using vertex arrays, so cache it
		updateRendererCache ();
	
		handle pos, norm, color, tex0, tex1;
		const vp_binding_t *binding = NULL;
		vp_binding_t new_binding;
		// now we have all vertex/index stuff in cache, bind it
		if (vertProgram)
		{
			// find binding
			vp_binding_map_t::const_iterator it;
			it = mVPBindings.find (vertProgram->name ());
			if (it == mVPBindings.end ())
			{
				// not found, setup for new shader
				new_binding.pos = vertProgram->parmByName ("position");
				new_binding.norm = vertProgram->parmByName ("normal");
				new_binding.color = vertProgram->parmByName ("color");
				new_binding.tex0 = vertProgram->parmByName ("texcoord0");
				new_binding.tex1 = vertProgram->parmByName ("texcoord1");
				new_binding.tangent = vertProgram->parmByName ("tangent");
				new_binding.binormal = vertProgram->parmByName ("binormal");
				mVPBindings[vertProgram->name ()] = new_binding;
				binding = &new_binding;
			}
			else
				binding = &(*it).second;
	
			if (binding->pos)
			{
				vertProgram->enableStream (binding->pos);
				vertProgram->bindStreamData (binding->pos, 3, fe::shader::FLOAT, sizeof (vector3), &mRendererCache.verts[mRendererCache.firstv[subset]]);
			}
			if (binding->norm)
			{
				vertProgram->enableStream (binding->norm);
				vertProgram->bindStreamData (binding->norm, 3, fe::shader::FLOAT, sizeof (vector3), &mRendererCache.normals[mRendererCache.firstv[subset]]);
			}
			if (binding->tex0)
			{
				vertProgram->enableStream (binding->tex0);
				vertProgram->bindStreamData (binding->tex0, 2, fe::shader::FLOAT, sizeof (vector2), &mRendererCache.uvs[mRendererCache.firstv[subset]]);
			}
		}
		else
		{
			glEnableClientState (GL_VERTEX_ARRAY);
			glEnableClientState (GL_NORMAL_ARRAY);
			glEnableClientState (GL_TEXTURE_COORD_ARRAY);
			glVertexPointer (3, GL_FLOAT, sizeof (vector3), &mRendererCache.verts[mRendererCache.firstv[subset]]);
			glNormalPointer (GL_FLOAT, sizeof (vector3), &mRendererCache.normals[mRendererCache.firstv[subset]]);
			glClientActiveTextureARB (GL_TEXTURE0_ARB);
			glTexCoordPointer (2, GL_FLOAT, sizeof (vector2), &mRendererCache.uvs[mRendererCache.firstv[subset]]);
		}
		glDisable (GL_STENCIL_TEST);
		/*
		if (fe::g_engine->getRenderQueue ()->getCurrentPass () != fe::PASS_WIREFRAME && fe::g_engine->getRenderQueue ()->getCurrentPass () != fe::PASS_OUTSIDE)
		{
			const eClass *eclass = getOwnerEnt ()->getEClass ();
			if (eclass->isFixedSize ())
			{
				const vector3 &col = eclass->getColor ();
				glColor4f (col.x, col.y, col.z, 1);
			}
			else
				glColor4f (1, 1, 1, 1);
		}*/
		if (g_editor->isDrawingSelectionOutlines ())
		{
			glDrawArrays (GL_LINE_LOOP, 0, mRendererCache.subsets[subset]);
		}
		else
		{
			glDrawArrays (GL_TRIANGLE_FAN, 0, mRendererCache.subsets[subset]);
		}
		
		// unbind
		if (vertProgram)
		{
			if (binding->pos)
			{
				vertProgram->disableStream (binding->pos);
			}
			if (binding->norm)
			{
				vertProgram->disableStream (binding->norm);
			}
			if (binding->tex0)
			{
				vertProgram->disableStream (binding->tex0);
			}
		}
		else
		{
			glDisableClientState (GL_VERTEX_ARRAY);
			glDisableClientState (GL_NORMAL_ARRAY);
			glDisableClientState (GL_TEXTURE_COORD_ARRAY);
		}
		#endif
	}
	struct faceVertex {
		int face;
		int vertex;
	};
	
	struct dupVertex {
		vector3 pos;
		vector3 newPos;
		std::vector <faceVertex> dupInfo;
	};
	
	vector3		brush::moveFaces (const std::vector <int> &mfaces, const vector3 &initial_dir)
	{
		// algorithm:
		//
		//	1. get all verts from original dataset to be moved and build duplication index
		//  struct faceVertex {
		//  	int face;
		//   	int vertex;
		//  };
		//	struct dupVertex {
		//		vec3	newPos;
		//		std::vector <faceVertex>	dupInfo;
		//	};
		//	std::vector <dupVertex>	moveVerts;
		//
		//	2. [??] mark faces which don't contain moved verts as "locked"
		//	
		//	3. calc new position for all moveVerts, and store it in newPos
		//
		//	4. build new planes for all brush faces which share moved verts (taking all moved vers as a reference points)
		//
		//	5. check if angle between any 1 changed and 1 non-changed planes forms outer angle <=PI
		//
		//		5.1. yes: get distance to  moved vertex from changed face which is the most distant (along the normal) to the locked face's plane; subtract required amount from "dir"; goto [1].
		//		5.2. no: rebuild everything; exit
		
		if (mfaces.size () == numFaces ())
		{
			move (initial_dir, false);	
			return initial_dir;
		}
		
		vector3 dir = initial_dir;
		vector3 unitDir = dir;
		unitDir.normalize ();
	
		std::vector <dupVertex> moveVerts;
		std::vector <bool>	touchedFaces;
	
		touchedFaces.resize (numFaces (), false);
	
	
	//	fprintf (stderr, "building initial vlist...\n");
		// build initial unique vertex list
		for (int f = 0; f < mfaces.size (); f++)
		{
			const brushWinding &w = faces (mfaces[f]).winding ();
			for (int v = 0; v < w.numPoints (); v++)
			{
				int i;
				for (i = 0; i < moveVerts.size (); i++)
				{
					if (moveVerts[i].pos == w.point (v))
					{
						break;
					}
				}
				if (i == moveVerts.size ())
				{
					dupVertex d;
					d.pos = w.point (v);
	//				fprintf (stderr, "add f%d v%d\n", f, v);
					moveVerts.push_back (d);
				}
			}
		}
	
		// build duplication index
	//	fprintf (stderr, "building duplication index...\n");
		for (int f = 0; f < numFaces (); f++)
		{
			const brushWinding &w = faces (f).winding ();
			for (int v = 0; v < w.numPoints (); v++)
			{
				for (int i = 0; i < moveVerts.size (); i++)
				{
					const vector3 &p = w.point (v);
					#define eps 0.001f
					#define cmp(a,b) (fabs(a.x-b.x)<eps && fabs(a.y-b.y)<eps && fabs(a.z-b.z)<eps)
					bool eq = cmp (moveVerts[i].pos, p);
	//				fprintf (stderr, "comparing [%d] %f %f %f to f%d v%d (%f %f %f) (%s)\n", i, moveVerts[i].pos.x, moveVerts[i].pos.y, moveVerts[i].pos.z, f, v, p.x, p.y, p.z, eq ? "true" : "false");
					if (eq)
					{
						faceVertex d;
						d.face = f;
						d.vertex = v;
	//					fprintf (stderr, "adding vertex: f%d v%d\n", f, v);
						moveVerts[i].dupInfo.push_back (d);
						touchedFaces[f] = true;
					}
	//				#undef eps
	//				#undef cmp
				}
			}
		}
	
	//	fprintf (stderr, "moveVerts.size () = %d\n", moveVerts.size ());
	
		int cnt = 0;
		for (;;)
		{
			// move all verts
			for (int i = 0; i < moveVerts.size (); i++)
			{
				moveVerts[i].newPos = moveVerts[i].pos + dir;
			}
	
			std::vector <fe::plane> newPlanes;
	
			// calc new planes
			for (int f = 0; f < numFaces (); f++)
			{
	//			fprintf (stderr, "getting new base points for face %d...\n", f);
				int basePointsIdx[3] = {-1, -1, -1}; // index of point in original winding (to preserve CW order)
				vector3 basePoints[3];
				int numBasePoints = 0;
				// get all moved verts for this face
				for (int v = 0; v < moveVerts.size (); v++)
				{
					for (int di = 0; di < moveVerts[v].dupInfo.size (); di++)
					{
						if (moveVerts[v].dupInfo[di].face == f)
						{
							basePoints[numBasePoints] = moveVerts[v].newPos;
							basePointsIdx[numBasePoints++] = moveVerts[v].dupInfo[di].vertex;
	//						fprintf (stderr, "added (moved) f%d v%d\n", f, moveVerts[v].dupInfo[di].vertex);
							if (numBasePoints == 3)
								break;
						}
					}
					if (numBasePoints == 3)
						break;
				}
	
				// look how many points we've got, and add points from the original dataset
				if (numBasePoints < 3)
				{
					// get point from original brush face which is not equal to the already fetched
					for (int v = 0; v < faces (f).winding ().numPoints (); v++)
					{
						int i;
						for (i = 0; i < numBasePoints; i++)
						{
							if (v == basePointsIdx[i])
							{
								break;;
							}
						}
						if (i == numBasePoints)
						{
							basePoints[numBasePoints] = faces (f).winding ().point (v);
							basePointsIdx[numBasePoints++] = v;
	//						fprintf (stderr, "added (not moved) f%d v%d\n", f, v);
							if (numBasePoints == 3)
								break;
						}
					}
				}
	
				if (numBasePoints != 3)
					continue;
				
				// sort points by basePointsIdx
				if (basePointsIdx[0] > basePointsIdx[1])
				{
					int itmp = basePointsIdx[0];
					basePointsIdx[0] = basePointsIdx[1];
					basePointsIdx[1] = itmp;
					vector3 tmp = basePoints[0];
					basePoints[0] = basePoints[1];
					basePoints[1] = tmp;
				}
				if (basePointsIdx[0] > basePointsIdx[2])
				{
					int itmp = basePointsIdx[0];
					basePointsIdx[0] = basePointsIdx[2];
					basePointsIdx[2] = itmp;
					vector3 tmp = basePoints[0];
					basePoints[0] = basePoints[2];
					basePoints[2] = tmp;
				}
				if (basePointsIdx[1] > basePointsIdx[2])
				{
					int itmp = basePointsIdx[1];
					basePointsIdx[1] = basePointsIdx[2];
					basePointsIdx[2] = itmp;
					vector3 tmp = basePoints[1];
					basePoints[1] = basePoints[2];
					basePoints[2] = tmp;
				}
				fe::plane p;
				p.fromPoints (basePoints[2], basePoints[1], basePoints[0]);
				newPlanes.push_back (p);
				faces(f).planePt (0) = basePoints[0];
				faces(f).planePt (1) = basePoints[1];
				faces(f).planePt (2) = basePoints[2];
			}
	
			bool broken = false;
			// check for concavification issues
			for (int f = 0; f < numFaces (); f++)
			{
				if (!touchedFaces[f])
					continue;
				for (int p = 0; p < numFaces (); p++)
				{
					if (touchedFaces[p])
						continue;
	
	//				fprintf (stderr, "%d vs %d\n", f, p);
	
					// faces must share at least one vertex
					// check that, and build list of moved f's verts
					std::vector <int>	fverts;
					bool share = false;
					for (int v = 0; v < moveVerts.size (); v++)
					{
						for (int d = 0; d < moveVerts[v].dupInfo.size (); d++)
						{
							if (moveVerts[v].dupInfo[d].face == f)
							{
								fverts.push_back (v);
								if (!share)
								{
									// now found check face p for the same coord
									brushWinding &w1 = faces (f).winding ();
									brushWinding &w2 = faces (p).winding ();
									for (int j = 0; j < w1.numPoints (); j++)
									{
										for (int k = 0; k < w2.numPoints (); k++)
										{
											if (cmp (w1.point (j), w2.point (k)))
											{
												share = true;
												break;
											}
										}
										if (share)
											break;
									}
								}
								break;
							}
						}
					}
					if (!share)
					{
						continue;
					}
	
					int max_idx = -1;
					float max_d = 0;
					// check distances of f's verts to p's plane
					for (int v = 0; v < fverts.size (); v++)
					{
						float d = newPlanes[p].distanceTo (moveVerts[fverts[v]].newPos);
						if (d >= max_d)
						{
							max_d = d;
							max_idx = v;
						}
					}
					if (max_idx != -1)
					{
						fprintf (stderr, "broken constraint (f%d vs f%d)! d=%f\n", f, p, max_d);
						// constraint broken
						vector3 newdir = dir - unitDir * (max_d+8);
						fprintf (stderr, "prev offs: %f %f %f ; new offs: %f %f %f\n", dir.x, dir.y, dir.z, newdir.x, newdir.y, newdir.z);
						dir -= unitDir * (max_d+8);
						broken = true;
						break;
					}
				}
				if (broken)
					break;
			}
			if (!broken)
				break;
			cnt++;
			if (cnt > 1)
				break;
		}
	
		// rebuild brush
	//	build ();
		return dir;
	}
	
	void brush::invalidateRendererCache (void)
	{
		mbNeedRecache = true;
	}

	brushMesh::brushMesh (void)
	{
	}

	brushMesh::~brushMesh (void)
	{
	}

	void brushMesh::update (brush *b)
	{
		fe::renderable *rend = b->getRenderable ();
		if (!rend)
			return;

		// clean up
		fe::drawSurf_t *ds = rend->getDrawSurf(0);
		if (ds)
		{
			delete[] ds;
			rend->setDrawSurfs (NULL, 0);
			ds = NULL;
		}
		if (mpMeshData)
		{
			if (mpMeshData->verts)
				delete[] mpMeshData->verts;
			if (mpMeshData->inds)
				delete[] mpMeshData->inds;
			mpMeshData = NULL;
			mMeshDataCount = 0;
		}

		// realloc
		mMeshDataCount = b->numFaces ();
		if (mMeshDataCount)
		{
			mpMeshData = new fe::meshData_t[mMeshDataCount];
			ds = new fe::drawSurf_t[mMeshDataCount];
			memset (ds, 0, sizeof (fe::drawSurf_t) * mMeshDataCount);
			memset (mpMeshData, 0, sizeof (fe::meshData_t) * mMeshDataCount);
			rend->setDrawSurfs (ds, mMeshDataCount);
		}
		else
			return;
		
		int nv = 0;
		uchar *verts = NULL;
		int nverts = 0;
		int vertexSize = sizeof (float)*8;
		// calc numverts
		int i;
		for (i = 0; i < mMeshDataCount; i++)
		{
			nverts += b->faces (i).winding ().numPoints ();
		}
		if (nverts)
			verts = new uchar[nverts*vertexSize]; // this will go to every mesh data
		uchar *vb = verts;
		
		for (size_t i = 0; i < mMeshDataCount; i++)
		{
			const brushFace &f = b->faces (i);
			//float shade = SetShadeForPlane(f.plane());
			//vector3 clr = drawcolor * shade;
			const brushWinding &w = f.winding();
			fe::drawSurf_t *s = ds + i;
			if (w.numPoints () == 0) // freed face
				continue;
			s->obj = b;
			s->mesh = mpMeshData + i;
			s->mesh->primType = GL_POLYGON;
			s->mtl = b->faces (i).getMtl ();
//			s->lightmap = NULL;
			
//			s->mesh->vbo = 0;
			s->mesh->verts = vb;
			s->mesh->vertexSize = vertexSize;

//			s->mesh->ibo = 0;
//			s->mesh->inds = NULL;

			s->mesh->firstVertex = nv;
			s->mesh->numVerts = w.numPoints ();
//			s->mesh->firstIndex = 0;
//			s->mesh->numInds = 0;
//			s->mesh->have_colors = 0;
//			s->mesh->have_lightmaps = 0;
//			s->mesh->have_tangents = 0;
			for (int j = 0; j < w.numPoints (); j++)
			{
				memcpy (verts, &w.point(j), sizeof (vector3));
				verts += sizeof (vector3);
				memcpy (verts, &f.plane ().normal (), sizeof (vector3));
				verts += sizeof (vector3);
				memcpy (verts, &w.texCoord (j), sizeof (vector2));
				verts += sizeof (vector2);
				nv++;
			}
			if (b->isFaceSelected (i))
				s->appFlags |= 1;
		}
	}
}
