#include "pch.h"
#include <gtk/gtk.h>
#include "editorvp.h"
#include "glview.h"

namespace feditor
{

	editorViewport::editorViewport (glView *view)
	{
		mpGLView = view;
		mViewMatrix.identity ();
		mEyePos = vector3 (0, 0, 0);
		mOrthoScale = 1.f;
		mOrthoMode = xy;
		mViewMode = ortho;
	//	mViewMode = perspective;
	}
	
	editorViewport::~editorViewport (void)
	{
	}
	
	glView*	editorViewport::getView (void)
	{
		return mpGLView;
	}
	
	int		editorViewport::getLeft (void) const
	{
		return mpGLView->getWidget ()->allocation.x;
	}
	
	int		editorViewport::getTop (void) const
	{
		return mpGLView->getWidget ()->allocation.y;
	}
	
	int		editorViewport::getWidth (void) const
	{
		return mpGLView->getWidget ()->allocation.width;
	}
	
	int		editorViewport::getHeight (void) const
	{
		return mpGLView->getWidget ()->allocation.height;
	}
	
	void editorViewport::setPosition (int x, int y)
	{
	}
	
	void editorViewport::setSize (int w, int h)
	{
	}
	
	void editorViewport::close (ulong flags)
	{
	}
	
	const matrix4&	editorViewport::getViewMatrix (void) const
	{
		mViewMatrix.identity ();
		orthoMode mode = getOrthoMode ();
		int nDim1 = (mode == yz) ? 1 : 0;
		int nDim2 = (mode == xy) ? 1 : 2;
		int nDim3 = (mode == xz) ? 1 : ((mode == xy) ? 2 : 0);
		mViewMatrix._11 = mViewMatrix._22 = mViewMatrix._33 = 1.f/mOrthoScale;
		mViewMatrix._41 = mEyePos[nDim1];
		mViewMatrix._42 = mEyePos[nDim2];
		mViewMatrix._43 = -1.f;
		mViewMatrix = mViewMatrix.inverse ();
		return mViewMatrix;
	}
	
	vector3		editorViewport::getEyePos (void) const
	{
		return mEyePos;
	}
	
	float			editorViewport::getOrthoScale (void) const
	{
		return mOrthoScale;
	}
	
	void			editorViewport::setOrthoScale (float scale)
	{
		mOrthoScale = scale;
	}
	
	editorViewport::orthoMode		editorViewport::getOrthoMode (void) const
	{
		return mOrthoMode;
	}
	
	editorViewport::viewMode		editorViewport::getViewMode (void) const
	{
		return mViewMode;
	}
	
	void			editorViewport::move (const vector3 &dir)
	{
		mEyePos += dir;
	}
	
	void			editorViewport::rotate (const vector3 &axis, float angle)
	{
	}
	
	void			editorViewport::yaw (float angle)
	{
	}
	
	void			editorViewport::pitch (float angle)
	{
	}
	
	void			editorViewport::nextOrthoMode (void)
	{
	    if (getViewMode () == ortho )
	    {
	        if (mOrthoMode == xy)
	            mOrthoMode = xz;
	        else if (mOrthoMode == xz )
	            mOrthoMode = yz;
	        else if (mOrthoMode == yz)
	            mOrthoMode = xy;
	    }
	}
	
	void			editorViewport::setViewMode(viewMode mode)
	{
		mViewMode = mode;
	}
	
}
