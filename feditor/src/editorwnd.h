#ifndef __F_EDITORWND_H
#define __F_EDITORWND_H

#include <gtk/gtk.h>
#include <fegdk/f_baseobject.h>

namespace feditor
{

	// this class is simple gtk window which contains arbitrary widget
	class editorWnd : public fe::baseObject
	{
	private:
		GtkWidget *mpWindow;
		GtkWidget *mpWidget;
	public:
		editorWnd (GtkWidget *widget, const char *name);
		~editorWnd (void);
		GtkWidget *getWidget (void) const;
		GtkWidget *getWindow (void) const;
		void toggle (void);
		bool isVisible (void) const;
	};

}

#endif // __F_EDITORWND_H

