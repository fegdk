// $Id$
#include "pch.h"
#ifdef _POSIX_C_SOURCE	// shut up pyconfig.h complaints
#undef _POSIX_C_SOURCE
#endif
#include <fegdk/f_input.h>
#include <fegdk/f_drawutil.h>
#include <fegdk/f_parser.h>
#include <fegdk/f_filesystem.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_console.h>
#include <fegdk/f_resourcemgr.h>
#include <fegdk/f_scenemanager.h>
#include <fegdk/f_baserenderer.h>
#include <fegdk/f_fontft.h>
#include <fegdk/cvars.h>
#include <fegdk/backend.h>
#include <Python.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <algorithm>
#include "editorcore.h"
#include "brush.h"
#include "editorcmd.h"
#include "mainfrm.h"
#include "logview.h"
#include "editorvp.h"
#include "glview.h"
#include "utility.h"
#include "entity.h"
#include "editorcmd.h"
#include "mtlbrowser.h"
#include "editorwnd.h"
#include "entityinspector.h"
#include "surfaceinspector.h"
#include "editormap.h"
namespace fe
{
	extern cvar_t *gl_polygon_offset_units_line;
	extern cvar_t *gl_polygon_offset_factor_line;
}

namespace feditor
{
	
	editorCore *g_editor = NULL;
		
	struct portal_t {
		std::vector <vector3> points;
	};
	std::vector <portal_t> portals;
	
	//-----------------------------------------------
	// Interface implementation
	//-----------------------------------------------
	
	// python fe module methods
	
	#define PyRetVal Py_BuildValue ("i", 0)
	
	extern "C" PyObject *PyPrintMsg (PyObject *self, PyObject *args);
	extern "C" PyObject *PyPrintOwners (PyObject *self, PyObject *args);
	extern "C" PyObject *PyVid_Restart (PyObject *self, PyObject *args);
	extern "C" PyObject *PyBrush_Create (PyObject *self, PyObject *args);
	extern "C" PyObject *PyBrush_Rotate (PyObject *self, PyObject *args);
	extern "C" PyObject *PyDlg_Create (PyObject *self, PyObject *args);
	extern "C" PyObject *PyDlg_AddEditBox (PyObject *self, PyObject *args);
	extern "C" PyObject *PyDlg_Run (PyObject *self, PyObject *args);
	extern "C" PyObject *PyDlg_Free (PyObject *self, PyObject *args);
	
	template <typename T> struct python_ptr_holder
	{
		PyObject_HEAD;
		smartPtr <T> ptr_;
		python_ptr_holder (smartPtr <T> ptr)
		{
			ptr_ = ptr;
			PyTypeObject TypeDef = {
				PyObject_HEAD_INIT(0)
				0, "python_ptr_holder",
				sizeof(*this)
			};
		}
	};
	
	void		editorCore::initPython (void)
	{
		mpPyMainModule = NULL;
		mpPyMainDictionary = NULL;
		mpPyLogFile = NULL;
	
		mpPyFEModule = NULL;
		mpPyFEDictionary = NULL;
	
		Py_Initialize ();
	
		// init log file
		mpPyLogFile = static_cast<void*> (PyFile_FromString ((char*)"python.log", (char*)"w"));
		PyFile_SetBufSize (static_cast<PyObject*> (mpPyLogFile), 0);
		PySys_SetObject ((char*)"stderr", static_cast<PyObject*> (mpPyLogFile));
	
		// invoke main module
		mpPyMainModule = PyImport_AddModule ((char*)"__main__");
		assert (mpPyMainModule);
		if (!mpPyMainModule)
		{
			PyErr_Clear ();
			mpMainFrm->getScriptView ()->print ("Failed to apply __main__.\n");
		}
		else
		{
			Py_INCREF (static_cast<PyObject*> (mpPyMainModule));
			mpPyMainDictionary = static_cast<void*> (PyModule_GetDict (static_cast<PyObject*> (mpPyMainModule)));
		}
	
		if (mpPyMainModule && mpPyMainDictionary)
		{
			mpMainFrm->getScriptView ()->print ("Python interpreter started.\n");
			mpMainFrm->getScriptView ()->print ("Log file is 'python.log'.\n");
			mpMainFrm->getScriptView ()->print ("Creating 'fe' module.\n");
	
			mpPyFEModule = PyImport_AddModule ((char*)"fe");
			assert (mpPyFEModule);
			if (!mpPyFEModule)
			{
				PyErr_Clear ();
				mpMainFrm->getScriptView ()->print ("Failed to apply fe.\n");
			}
			else
			{
				Py_INCREF (static_cast<PyObject*> (mpPyFEModule));
				mpPyFEDictionary = static_cast<void*> (PyModule_GetDict (static_cast<PyObject*> (mpPyFEModule)));
			}
	
			static PyMethodDef exp[] = {
				{ (char*)"print_msg", PyPrintMsg, METH_VARARGS },
				{ (char*)"print_owners", PyPrintOwners, METH_VARARGS },
				{ (char*)"vid_restart", PyVid_Restart, METH_VARARGS },
				{ (char*)"Brush_Create", PyBrush_Create, METH_VARARGS },
				{ (char*)"Brush_Rotate", PyBrush_Rotate, METH_VARARGS },
				{ (char*)"Dlg_Create", PyDlg_Create, METH_VARARGS },
				{ (char*)"Dlg_AddEditBox", PyDlg_AddEditBox, METH_VARARGS },
				{ (char*)"Dlg_Run", PyDlg_Run, METH_VARARGS },
				{ (char*)"Dlg_Free", PyDlg_Free, METH_VARARGS },
				{ NULL, NULL, 0 }
			};
		    Py_InitModule ((char*)"fe", exp);
	
			// show some info
			mpMainFrm->getScriptView ()->print ("Version: " + cStr (Py_GetVersion ()) + "\n");
			mpMainFrm->getScriptView ()->print ("Build info: " + cStr (Py_GetBuildInfo ()) + "\n");
			mpMainFrm->getScriptView ()->print ("Platform: " + cStr (Py_GetPlatform ()) + "\n");
		}
		mpMainFrm->getLogView ()->print ("initPython ok\n");
	}
	
	void Cmd_editor_deleteselection_f (void)
	{
		g_editor->addCommand (new CmdDeleteSelection);
	}
	
	void Cmd_editor_undo_f (void)
	{
		g_editor->undo ();
		g_editor->redrawAllViews ();
	}
	
	void Cmd_editor_redo_f (void)
	{
		g_editor->redo ();
		g_editor->redrawAllViews ();
	}

	void Cmd_editor_selectall_f (void)
	{
		g_editor->addCommand (new CmdSelectAll ());
	}
	
	void Cmd_editor_cancel_f (void)
	{
		if (g_editor->clipperGetMode ())
	        g_editor->clipperToggle ();
	    else if (g_editor->rotateGetMode ())
	        g_editor->rotateToggle ();
	    else
	        g_editor->addCommand (new CmdSelectionClear ());
	}
	
	void Cmd_editor_apply_f (void)
	{
		if (g_editor->clipperGetMode ())
		{
			if (!strcmp ("clip", g_editor->getValueForKey ("editor_defaultclipaction")))
				g_editor->clipperClipSelection ();
			else
				g_editor->clipperSplitSelection ();
		}
	}
	
	void Cmd_editor_toggleclipper_f (void)
	{
		g_editor->clipperToggle ();
	}
	
	void Cmd_editor_togglerotation_f (void)
	{
		g_editor->rotateToggle ();
	}
	
	void Cmd_editor_cyclelayout_f (void) {
		smartPtr <editorViewport> vp = g_editor->getViewport ();
	    vp->nextOrthoMode ();
		g_editor->redrawAllViews ();
	}
	
	void Cmd_editor_makesidedbrush_f (void) {
		if (fe::Cmd_Argc () <= 1)
		{
			fe::Con_Printf ("usage: editor_makesidedbrush numsides\n");
		}
		if (g_editor->getMap ()->getSelection ().size () == 1)
		{
			brushPtr b = g_editor->getMap ()->getSelection ().front ();
			g_editor->addCommand (new CmdMakeSidedBrush (b, atoi (fe::Cmd_Argv (1))));
		}
	}
	
	void Cmd_editor_togglemtlmgr_f (void) {
		g_editor->getMainFrm ()->getMtlBrowser ()->toggle ();
	}
	
	void Cmd_editor_togglecamera_f (void) {
		g_editor->getMainFrm ()->getCameraWnd ()->toggle ();
	}
	
	void Cmd_editor_togglecamwireframe_f (void) {
		fe::g_engine->getCVarManager ()->set ("r_show_tris", fe::r_show_tris->ivalue ? "0" : "1", false);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_editor_csghollow_f (void) {
		g_editor->addCommand (new CmdMakeBrushHollow ());
		g_editor->redrawAllViews ();
	}
	
	void Cmd_editor_csgsubtract_f (void) {
		g_editor->addCommand (new CmdCSGSubtract ());
		g_editor->redrawAllViews ();
	}
	
	#define MOVE_SPEED	8
	#define ROTATE_SPEED	(10*M_PI/180)
	
	void Cmd_stepforward_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 translate;
		translate.identity ();
		translate._43 = -MOVE_SPEED;
		g_editor->setCameraMatrix (mtx*translate);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_stepbackward_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 translate;
		translate.identity ();
		translate._43 = MOVE_SPEED;
		g_editor->setCameraMatrix (mtx*translate);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_stepleft_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 translate;
		translate.identity ();
		translate._41 = MOVE_SPEED;
		g_editor->setCameraMatrix (mtx*translate);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_stepright_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 translate;
		translate.identity ();
		translate._41 = -MOVE_SPEED;
		g_editor->setCameraMatrix (mtx*translate);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_stepup_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 translate;
		translate.identity ();
		translate._42 = -MOVE_SPEED;
		g_editor->setCameraMatrix (mtx*translate);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_stepdown_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 translate;
		translate.identity ();
		translate._42 = MOVE_SPEED;
		g_editor->setCameraMatrix (mtx*translate);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_rotatestepleft_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 rot;
		rot.rotateY (ROTATE_SPEED);
		g_editor->setCameraMatrix (mtx*rot);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_rotatestepright_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 rot;
		rot.rotateY (-ROTATE_SPEED);
		g_editor->setCameraMatrix (mtx*rot);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_rotatestepup_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 rot;
		rot.rotateX (ROTATE_SPEED);
		g_editor->setCameraMatrix (mtx*rot);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_rotatestepdown_f (void) {
		const matrix4 &mtx = g_editor->getCameraMatrix ();
		matrix4 rot;
		rot.rotateX (-ROTATE_SPEED);
		g_editor->setCameraMatrix (mtx*rot);
		g_editor->redrawAllViews ();
	}
	
	void Cmd_testmove_f (void) {
		if (!g_editor->getMap ()->getSelection ().empty ())
		{
			smartPtr <brush> &b = g_editor->getMap ()->getSelection ().front ();
			std::vector <int>	faces;
			faces.push_back (0);
			b->moveFaces (faces, vector3 (0, 16, 0));
			b->build ();
			g_editor->redrawAllViews ();
		}
	}
	
	void Cmd_editor_toggleentityinspector_f (void) {
		g_editor->getMainFrm ()->getEntityInspector ()->toggle ();
	}
	
	void Cmd_editor_togglesurfaceinspector_f (void) {
		g_editor->getMainFrm ()->getSurfaceInspector ()->toggle ();
	}
	
	void Cmd_editor_copyselection_f (void) {
		GdkAtom clipAtom = gdk_atom_intern ("CLIPBOARD", TRUE);
		if (clipAtom)
		{
			GtkClipboard *clip = gtk_clipboard_get (clipAtom);
			if (clip)
			{
				std::stringstream str;
				g_editor->getMap ()->save (str, true);
				std::string s = str.str ();
				gtk_clipboard_set_text (clip, s.c_str (), s.size ());
			}
		}
	}
	
	void Cmd_editor_pasteselection_f (void) {
		GdkAtom clipAtom = gdk_atom_intern ("CLIPBOARD", TRUE);
		if (clipAtom)
		{
			GtkClipboard *clip = gtk_clipboard_get (clipAtom);
			if (clip)
			{
				gchar *str = gtk_clipboard_wait_for_text (clip);
				if (str)
				{
					brushList brushes;
					entList ents;
					g_editor->getMap ()->load (str, brushes, ents);
					g_free (str);
					g_editor->addCommand (new CmdInsertBrushes (ents, brushes));
					g_editor->redrawAllViews ();
				}
			}
		}
	}
	
	void Cmd_editor_cutselection_f (void) {
		GdkAtom clipAtom = gdk_atom_intern ("CLIPBOARD", TRUE);
		if (clipAtom)
		{
			GtkClipboard *clip = gtk_clipboard_get (clipAtom);
			if (clip)
			{
				std::stringstream str;
				g_editor->getMap ()->save (str, true);
				std::string s = str.str ();
				gtk_clipboard_set_text (clip, s.c_str (), s.size ());
			}
		}
		g_editor->addCommand (new CmdDeleteSelection ());
		g_editor->redrawAllViews ();
	}
	
	void Cmd_editor_togglegrid_f (void) {
		g_editor->toggleGrid ();
		g_editor->redrawAllViews ();
	}
	
	editorCore::editorCore (void)
	{
		g_editor = this;
	
		FE_REGISTER_CONSOLE_CMD (editor_deleteselection);
		FE_REGISTER_CONSOLE_CMD (editor_undo);
		FE_REGISTER_CONSOLE_CMD (editor_redo);
		FE_REGISTER_CONSOLE_CMD (editor_selectall);
		FE_REGISTER_CONSOLE_CMD (editor_cancel);
		FE_REGISTER_CONSOLE_CMD (editor_apply);
		FE_REGISTER_CONSOLE_CMD (editor_toggleclipper);
		FE_REGISTER_CONSOLE_CMD (editor_togglerotation);
		FE_REGISTER_CONSOLE_CMD (editor_cyclelayout);
		FE_REGISTER_CONSOLE_CMD (editor_makesidedbrush);
		FE_REGISTER_CONSOLE_CMD (editor_togglemtlmgr);
		FE_REGISTER_CONSOLE_CMD (editor_togglecamera);
		FE_REGISTER_CONSOLE_CMD (editor_csghollow);
		FE_REGISTER_CONSOLE_CMD (editor_csgsubtract);
		FE_REGISTER_CONSOLE_CMD (stepforward);
		FE_REGISTER_CONSOLE_CMD (stepbackward);
		FE_REGISTER_CONSOLE_CMD (stepleft);
		FE_REGISTER_CONSOLE_CMD (stepright);
		FE_REGISTER_CONSOLE_CMD (stepup);
		FE_REGISTER_CONSOLE_CMD (stepdown);
		FE_REGISTER_CONSOLE_CMD (rotatestepleft);
		FE_REGISTER_CONSOLE_CMD (rotatestepright);
		FE_REGISTER_CONSOLE_CMD (rotatestepup);
		FE_REGISTER_CONSOLE_CMD (rotatestepdown);
		FE_REGISTER_CONSOLE_CMD (editor_togglecamwireframe);
		FE_REGISTER_CONSOLE_CMD (editor_toggleentityinspector);
		FE_REGISTER_CONSOLE_CMD (editor_togglesurfaceinspector);
		FE_REGISTER_CONSOLE_CMD (testmove);
		FE_REGISTER_CONSOLE_CMD (editor_copyselection);
		FE_REGISTER_CONSOLE_CMD (editor_pasteselection);
		FE_REGISTER_CONSOLE_CMD (editor_cutselection);
		FE_REGISTER_CONSOLE_CMD (editor_togglegrid);
	
		mbAllowRendering = false;
		// filesystem
	//	fe::g_engine->getFileSystem ()->init (getKeyValue ("datapath"));
		mpMainFrm = new mainFrm ();
	
		mbClipperMode = false;
		mClipperNumPoints = 0;
		mClipperMovingPoint = -1;
	
		mbRotateMode = false;
		mbRotatingSelection = false;
		mbRotationMovingOrigin = false;
	
		mbDrawGrid = true;
		mpMap = new editorMap ();
	
		mbSelectingArea = false;
		mbSelectingFaces = false;
		mbSelectingBrushes = false;
		mpActiveBrush = NULL;
		mbMovingSelection = false;
		mbDraggingFaces = false;
	
		mbSnapToGrid = true;
		mCurrentCommand = -1;	// empty command list
	
		mCameraMatrix.camera (vector3 (0, -300, 0), vector3::zero, vector3 (0, 0, 1));
		(vector3&)mCameraMatrix._11 = -(vector3&)mCameraMatrix._11;
		mbCamWireFrame = false;

		mbDraggingFaces = false;
	
		initPython ();
	
		// write some stuff..
		mpMainFrm->getLogView ()->print ("OpenGL info:\n  Vendor: ");
		mpMainFrm->getLogView ()->print ( (const char *)glGetString (GL_VENDOR));
		mpMainFrm->getLogView ()->print ("\n  Version: ");
		mpMainFrm->getLogView ()->print ( (const char *)glGetString (GL_VERSION));
		mpMainFrm->getLogView ()->print ("\n  Extensions: ");
		mpMainFrm->getLogView ()->print ( (const char *)glGetString (GL_EXTENSIONS));
		mpMainFrm->getLogView ()->print ("\n");
	
		// load portals
		fe::charParser p (NULL, "portals.txt", true);
		p.errMode (0);
		if (!p.readNextChunk ()) {
			p.errMode (1);
			for (;;)
			{
				portal_t port;
				p.getToken ();
				if (!p.cmptoken ("{"))
				{
					for (;;)
					{
						p.getToken ();
						if (!p.cmptoken ("}"))
						{
							portals.push_back (port);
							break;
						}
						else
						{
							vector3 pt;
							for (int i = 0; i < 3; i++)
							{
								if (i != 0)
									p.getToken ();
								pt[i] = atof (p.token ());
							}
							port.points.push_back (pt);
						}
					}
				}
			}
			fprintf (stderr, "got %d portals\n", portals.size ());
		}
	}
	
	editorCore::~editorCore (void)
	{
		Py_Finalize ();
		reset ();
		mpMap = NULL;
		mpMainFrm->release ();
	}
	
	// returns mainfrm object
	mainFrm*		editorCore::getMainFrm (void) const
	{
		return mpMainFrm;
	}
	
	fe::materialPtr tmpMtl;
	
	// returns nearest brush and it's face
	smartPtr <brush>		editorCore::pickNearest (const ray3 &ray, int *face)
	{
		std::map <float, smartPtr <brush> > objects;
		std::map <float, int> faces;
		brushList::iterator it;
		for (it = mpMap->getBrushList ().begin (); it != mpMap->getBrushList ().end (); it++)
		{
			vector3 pt;
			int nface = (*it)->findRayIntr (ray, pt);
			if (nface != -1)
			{
				float d = (ray.origin () - pt).squaredLength ();
				objects[d] = (*it);
				if (face)
					faces[d] = nface;
			}
		}
		for (it = mpMap->getSelection ().begin (); it != mpMap->getSelection ().end (); it++)
		{
			vector3 pt;
			int nface = (*it)->findRayIntr (ray, pt);
			if (nface != -1)
			{
				float d = (ray.origin () - pt).squaredLength ();
				objects[d] = *it;
				if (face)
					faces[d] = nface;
			}
		}
	
		if (face)
			*face = faces.size () ? (* (faces.begin ())).second : -1;
	
		return objects.size () ? (* (objects.begin ())).second : NULL;
	}
	
	void
	editorCore::handleClipperMouseEvent (GdkEventType type, guint button, guint state, gdouble x, gdouble y)
	{
		smartPtr <editorViewport> vp = getViewport ();
		editorViewport::orthoMode m = vp->getOrthoMode ();
		int nDim1 = (m == editorViewport::yz) ? 1 : 0;
		int nDim2 = (m == editorViewport::xy) ? 1 : 2;
		if (type == GDK_BUTTON_RELEASE && button == 1)
		{
			mClipperMovingPoint = -1;
		}
		else if (type == GDK_MOTION_NOTIFY)
		{
			if (mClipperMovingPoint != -1)
			{
				vector3 mpos (0, 0, 0);
				float xx = (float) (x - vp->getWidth () / 2.f);
				float yy = (float) (y - vp->getHeight () / 2.f);
				mpos[nDim1] = xx / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
				mpos[nDim2] = -yy  / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
				snapPt (mpos);
				mClipperPoints[mClipperMovingPoint] = mpos;
				redrawAllViews ();
			}
		}
		else if (type == GDK_BUTTON_PRESS && button == 1)
		{
			// check if we hit a point
			if (mClipperNumPoints > 0)
			{
				vector3 mpos (0, 0, 0);
				float xx = (float) (x - vp->getWidth () / 2.f);
				float yy = (float) (y - vp->getHeight () / 2.f);
				mpos[nDim1] = xx / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
				mpos[nDim2] = -yy  / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
				int i;
				// get nearest clipper point (no farther than 8 pixels)
				for (i = 0; i < mClipperNumPoints; i++)
				{
					float xx = mpos[nDim1] - mClipperPoints[i][nDim1];
					float yy = mpos[nDim2] - mClipperPoints[i][nDim2];
					float dd = xx*xx + yy*yy;
					if (dd < 16)
						break;
				}
				if (i < mClipperNumPoints)
				{
					mClipperMovingPoint = i;
					return;
				}
			}
	
			// add point
			if (mClipperNumPoints >= 3)
				mClipperNumPoints--;
			vector3 pos (0, 0, 0);
	
			float xx = (float) (x - vp->getWidth () / 2.f);
			float yy = (float) (y - vp->getHeight () / 2.f);
	
			pos[nDim1] = xx / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
			pos[nDim2] = -yy  / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
	
			snapPt (pos);
			mClipperPoints[mClipperNumPoints] = pos;
			mClipperNumPoints++;
	
			redrawAllViews ();
		}
	}
	
	void
	editorCore::handleDefaultMouseEvent (GdkEventType type, guint button, guint state, gdouble x, gdouble y)
	{
		smartPtr <editorViewport> vp = getViewport ();
		editorViewport::orthoMode m = vp->getOrthoMode ();
		int nDim1 = (m == editorViewport::yz) ? 1 : 0;
		int nDim2 = (m == editorViewport::xy) ? 1 : 2;
		point pt;
		pt.x = (int)x;
		pt.y = (int)y;
		switch (type)
		{
		case GDK_BUTTON_PRESS:
			if (button != 1)
			{
				break;
			}
			if (state&GDK_SHIFT_MASK)
			{
				mbSelectingBrushes = false;
				mbSelectingFaces = false;
				if (state&GDK_CONTROL_MASK)
				{
					mbSelectingFaces = true;
//					selectFace (pt, state);
				}
				else
				{
					mbSelectingBrushes = true;
//					selectBrush (pt, state);
				}

				selectBegin ((int)x, (int)y);
			}
			else if (!mpMap->getSelection ().empty ())
			{
				// check if we hit any brush
				ray3 ray;
				if (vp->getViewMode () == editorViewport::perspective)
				{
				//	ray = rayFromMousePos (pt, vp);
					ray = rayFromMousePos (pt, vp->getWidth (), vp->getHeight (), mCameraMatrix);
				}
				else
				{
					vector3 pos (0, 0, 0);
					pos[nDim1] = (pt.x - vp->getWidth () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
					pos[nDim2] = (-pt.y + vp->getHeight () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
					ray.direction () = vector3 (1, 1, 1);
					ray.direction ()[nDim1] = 0;
					ray.direction ()[nDim2] = 0;
					ray.origin () = vector3 (-1.0e+4f, -1.0e+4f, -1.0e+4f);
					ray.origin ()[nDim1] = pos[nDim1];
					ray.origin ()[nDim2] = pos[nDim2];
				}
				dragBegin (pt.x, pt.y, ray, vp->getViewMode () == editorViewport::perspective);
			}
			else if (mpMap->getSelection ().empty ())
			{
				// start creating new brush
				smartPtr <brush> b = new brush ();
				b->link (mpMap->getWorldEntity ());
				mpMap->insertBrush (b);
				mpActiveBrush = b;
				setLastMousePt (pt);
				mMoveStartMousePt = pt;
				mpMap->addSelection (b);
			}
			break;
		case GDK_MOTION_NOTIFY:
			if (mbSelectingArea)
			{
				selectUpdate ((int)x, (int)y);
				redrawAllViews (); // FIXME: only one view needs to be repainted
			}
			else if (mpActiveBrush)
			{
				// change activebrush mins/maxs and rebuild
				vector3 mins (-8, -8, -8);
				vector3 maxs (8, 8, 8);
				mins[nDim1] = (mMoveStartMousePt.x - vp->getWidth () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
				mins[nDim2] = (-mMoveStartMousePt.y + vp->getHeight () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
				maxs[nDim1] = (pt.x - vp->getWidth () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
				maxs[nDim2] = (-pt.y + vp->getHeight () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
				snapPt (mins);
				snapPt (maxs);
				float tmp;
				if (mins.x > maxs.x) { tmp = mins.x; mins.x = maxs.x; maxs.x = tmp; }
				if (mins.y > maxs.y) { tmp = mins.y; mins.y = maxs.y; maxs.y = tmp; }
				if (mins.z > maxs.z) { tmp = mins.z; mins.z = maxs.z; maxs.z = tmp; }
				mpActiveBrush->create (mins, maxs);
	
				cStr mtl = getMainFrm ()->getMtlBrowser ()->getSelectedMtlName ();
				for (int j = 0; j < mpActiveBrush->numFaces (); j++)
					mpActiveBrush->faces (j).setMtl (mtl);
	
				redrawAllViews ();
			}
			else if (mbMovingSelection || mbDraggingFaces)
			{
				vector3 dir (0, 0, 0);
				if (vp->getViewMode () == editorViewport::ortho)
				{
					point lastpt = getLastMousePt ();
					dir[nDim1] = (pt.x - lastpt.x) / vp->getOrthoScale ();
					dir[nDim2] = (lastpt.y - pt.y) / vp->getOrthoScale ();
				}
				else
				{
					point lastpt = getLastMousePt ();
					dir[0] = (float) (pt.x - lastpt.x);
					dir[1] = (float) (lastpt.y - pt.y);
					// transform by inverse view matrix
					dir = mCameraMatrix.inverse ().transform (dir);//vp->getViewMatrix ().inverse ().transform (dir);
				}
				setLastMousePt (pt);
	
				dragUpdate (dir);
				redrawAllViews ();
			}
			break;
		case GDK_BUTTON_RELEASE:
			if (mbSelectingArea)
			{
				selectEnd ((int)x, (int)y);
				// select all in the area in current vp
				redrawAllViews ();
			}
			else if (mpActiveBrush)
			{
				mpActiveBrush->removeEmptyFaces ();
				mpActiveBrush->unlink ();
				mpMap->removeBrush (mpActiveBrush);
	
				if (0 != mpActiveBrush->numFaces ())
				{
					addCommand (new CmdCreateBrush (mpActiveBrush));
					if (!mpMap->getSelection ().empty ())
						getMainFrm ()->getEntityInspector ()->setEntity (mpMap->getSelection ().front ()->getOwnerEnt ());
				}
				else
					getMainFrm ()->getEntityInspector ()->setEntity (NULL);
				mpActiveBrush = NULL;
			}
			else if (mbMovingSelection || mbDraggingFaces)
			{
				dragEnd ();
	//			getMainFrm ()->getEntityInspector ()->setEntity (mpMap->getSelection ().front ()->getOwnerEnt ());
			}
			break;
		}
	}
	
	void
	editorCore::handleRotationMouseEvent (GdkEventType type, guint button, guint state, gdouble x, gdouble y)
	{
		smartPtr <editorViewport> vp = getViewport ();
		point pt;
		pt.x = (int)x;
		pt.y = (int)y;
		editorViewport::orthoMode m = vp->getOrthoMode ();
		int nDim1 = (m == editorViewport::yz) ? 1 : 0;
		int nDim2 = (m == editorViewport::xy) ? 1 : 2;
		if (!mbRotationMovingOrigin && !mbRotatingSelection && type == GDK_BUTTON_PRESS
			&& (button == 1 || (button == 2 && (state&GDK_CONTROL_MASK))))
		{
			if (mpMap->getSelection ().empty ())
			{
				fprintf (stderr, "You must select at least one brush to rotate.\n");
				return;
			}
			// check for origin movement
			vector3 mpos (0, 0, 0);
			float xx = (float) (x - vp->getWidth () / 2.f);
			float yy = (float) (y - vp->getHeight () / 2.f);
			mpos[nDim1] = xx / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
			mpos[nDim2] = -yy  / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
			int i;
			// get dist (no farther than 8 pixels)
			xx = mpos[nDim1] - mRotationOrigin[nDim1];
			yy = mpos[nDim2] - mRotationOrigin[nDim2];
			float dd = xx*xx + yy*yy;
			if (dd < 16)
			{
				mbRotationMovingOrigin = true;
				mMoveStartMousePt.x = (int)x;
				mMoveStartMousePt.y = (int)y;
			}
			else
			{
				// if we don't in origin rect -- start rotating selection around it
				mbRotatingSelection = true;
				mRotateStartMousePt = getLastMousePt ();
				mRotationAmount = 0;
			}
		}
		else if ((type == GDK_BUTTON_RELEASE && (button == 1 || button == 2)))
		{
			if (mbRotationMovingOrigin)
			{
				mbRotationMovingOrigin = false;
			}
			else if (mbRotatingSelection)
			{
				// restore initial rotation and run rotation command
				float axis[3] = { 1, 1, 1 };
				axis[nDim1] = axis[nDim2] = 0.f;
				// restore rotation to zero
				for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++)
					 (*i)->rotate (axis, mRotationOrigin, -mRotationAmount * 3.14f / 180.f);
				addCommand (new CmdRotateSelection (axis, mRotationOrigin, mRotationAmount));
				mbRotatingSelection = false;
			}
		}
		else if (type == GDK_MOTION_NOTIFY)
		{
			if (mbRotationMovingOrigin)
			{
				vector3 mpos (0, 0, 0);
				float xx = (float) (x - vp->getWidth () / 2.f);
				float yy = (float) (y - vp->getHeight () / 2.f);
				mpos[nDim1] = xx / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
				mpos[nDim2] = -yy  / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
				snapPt (mpos);
				mRotationOrigin = mpos;
				redrawAllViews ();
			}
			else if (mbRotatingSelection)
			{
				// 1 pixel = 1 degree, snap
				int rot = - (pt.y - mRotateStartMousePt.y);
				rot = (rot / 5) * 5;  // snap to 5 degrees
				int nDim1 = (m == editorViewport::yz) ? 1 : 0;
				int nDim2 = (m == editorViewport::xy) ? 1 : 2;
				float axis[3] = { 1, 1, 1 };
				axis[nDim1] = axis[nDim2] = 0.f;
				// restore
				for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++)
				{
					(*i)->rotate (axis, mRotationOrigin, (-mRotationAmount + rot) * 3.14f / 180.f);
				}
				mRotationAmount = rot;
				redrawAllViews ();
			}
		}
	}
	
	// handles mouse event for current viewport
	// type, button, state, x, y are all members of GdkEventButton or GdkEventMotion
	// valid types are GDK_BUTTON_PRESS, GDK_BUTTON_RELEASE, GDK_MOTION_NOTIFY
	void			editorCore::mouseEvent (GdkEventType type, guint button, guint state, gdouble x, gdouble y)
	{
		if (!mpCurrentViewport)
			return;
		point pt;
		pt.x = (int)x;
		pt.y = (int)y;
		if (type == GDK_BUTTON_PRESS)
		{
			setLastMousePt (pt);
		}
		
		if (mbClipperMode)
		{
			handleClipperMouseEvent (type, button, state, x, y);
		}
		else if (mbRotateMode)
		{
			handleRotationMouseEvent (type, button, state, x, y);
		}
		else
			handleDefaultMouseEvent (type, button, state, x, y);
		
	}
	
	// returns last mouse position
	point			editorCore::getLastMousePt (void) const
	{
		return mLastMousePt;
	}
	
	// sets new value for last mouse position
	void			editorCore::setLastMousePt (const point &pt)
	{
		mLastMousePt = pt;
	}
	
	class backend_selection_renderer : public fe::rBackendPass
	{
	public:
		void draw (fe::renderable **objects, int numobjects, fe::drawSurf_t **drawsurfs, int numds)
		{
			glDisable (GL_TEXTURE_2D);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable (GL_BLEND);
			glDepthFunc (GL_EQUAL);
			glColor4f (0.5f, 0.5f, 0.9f, 0.7f);
			int i;
			fe::sceneManager* scene = fe::g_engine->getSceneManager ();
			fe::sceneObject *prev = NULL;
			fe::effectParms* parms = fe::g_engine->getEffectParms ();
			fe::matrix4 mIdent (true);
			parms->setMatrix (fe::effectParm_WorldMatrix, mIdent);
			fe::rBackend *be = fe::g_engine->getRBackend ();

			for (i = 0; i < numds; i++)
			{
				fe::drawSurf_t *surf = drawsurfs[i];
				if (!surf->obj || !(surf->appFlags & 0x00000001))
				{
					continue;
				}
				be->setCurrentDrawSurf (surf);
				if (surf->mtl)
				{
					scene->setCurrentMtl (surf->mtl);
					fe::material::effectRef *effect = surf->mtl->getEffectRef ();
					if (effect && effect->ambient != -1 && effect->fx)
					{
						effect->fx->setTechnique (effect->ambient);

						// prepare obj-specific states
						if (surf->obj != prev)
						{
							surf->mtl->setupEffectInputs ();
							prev = surf->obj;
						}

						int np = effect->fx->begin ();
						for (int p = 0; p < np; p++)
						{
							effect->fx->pass (p, fe::PASS_NO_DL | fe::PASS_NO_FP);
							fe::backend_draw_surf (surf);
							effect->fx->finalizePass (p);
						}
						effect->fx->end ();
					}
				}
			}
			be->setCurrentDrawSurf (NULL);
			glDisable (GL_BLEND);
		}
	};
	
	class backend_selection_renderer2 : public fe::rBackendPass
	{
	public:
		void draw (fe::renderable **objects, int numobjects, fe::drawSurf_t **drawsurfs, int numds)
		{
			glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
			glDisable (GL_TEXTURE_2D);
			glDepthFunc (GL_LEQUAL);
			glPolygonOffset (fe::gl_polygon_offset_factor_line->fvalue, fe::gl_polygon_offset_units_line->fvalue);
			glEnable (GL_POLYGON_OFFSET_LINE);
			glColor4f (1.f, 1.f, 1.f, 1.f);
			int i;
			fe::sceneManager* scene = fe::g_engine->getSceneManager ();
			fe::sceneObject *prev = NULL;
			fe::effectParms* parms = fe::g_engine->getEffectParms ();
			fe::matrix4 mIdent (true);
			parms->setMatrix (fe::effectParm_WorldMatrix, mIdent);
			fe::rBackend *be = fe::g_engine->getRBackend ();

			for (i = 0; i < numds; i++)
			{
				fe::drawSurf_t *surf = drawsurfs[i];
				if (!surf->obj || !(surf->appFlags & 0x00000001))
				{
					continue;
				}
				be->setCurrentDrawSurf (surf);
				if (surf->mtl)
				{
					scene->setCurrentMtl (surf->mtl);
					fe::material::effectRef *effect = surf->mtl->getEffectRef ();
					if (effect && effect->ambient != -1 && effect->fx)
					{
						effect->fx->setTechnique (effect->ambient);

						// prepare obj-specific states
						if (surf->obj != prev)
						{
							surf->mtl->setupEffectInputs ();
							prev = surf->obj;
						}

						int np = effect->fx->begin ();
						for (int p = 0; p < np; p++)
						{
							effect->fx->pass (p, fe::PASS_NO_DL | fe::PASS_NO_FP);
							fe::backend_draw_surf (surf);
							effect->fx->finalizePass (p);
						}
						effect->fx->end ();
					}
				}
			}
			be->setCurrentDrawSurf (NULL);
			glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
			glPolygonOffset (0, 0);
			glDisable (GL_POLYGON_OFFSET_LINE);
		}
	};


	//fontFTPtr fnt;
	// scene rendering
	void			editorCore::render (void)
	{
		editorViewport *vp = fe::checked_cast <editorViewport *> (fe::g_engine->getViewport ());
	
		if (!mpFont)
			mpFont = fe::g_engine->getResourceMgr ()->createFontFT ("ter-116b.pcf");
		fe::g_engine->getRenderer ()->clear (0, NULL, fe::baseRenderer::clear_target | fe::baseRenderer::clear_z, float4 (.9f, .9f, .9f, 1), 1, 0);
	
		if (vp->getViewMode () == editorViewport::ortho)
		{
			if (mbDrawGrid)
				drawGrid ();
			editorViewport::orthoMode m = vp->getOrthoMode ();
			int nDim1 = (m == editorViewport::yz) ? 1 : 0;
			int nDim2 = (m == editorViewport::xy) ? 1 : 2;
	
			glMatrixMode (GL_PROJECTION);
			glPushMatrix ();
			glLoadIdentity ();
			gluOrtho2D (-vp->getWidth ()/2, vp->getWidth ()/2, -vp->getHeight ()/2, vp->getHeight ()/2);
			glMatrixMode (GL_MODELVIEW);
			glPushMatrix ();
			matrix4 mtx = vp->getViewMatrix ();
			glLoadMatrixf ( (GLfloat*)&mtx);
			glTranslatef (0, 0, -1);
	
			glDisable (GL_TEXTURE_2D);
	
			// render clipper
			if (mbClipperMode)
			{
				if (mClipperNumPoints)
				{
					int i;
					glDisable (GL_BLEND);
					glBegin (GL_QUADS);
					glColor4f (.0f,.0f,1.f,1.f);
					for (i = 0; i < mClipperNumPoints; i++)
					{
						glVertex2f (mClipperPoints[i][nDim1] - 3 / vp->getOrthoScale (), mClipperPoints[i][nDim2] + 3 / vp->getOrthoScale ());
						glVertex2f (mClipperPoints[i][nDim1] + 3 / vp->getOrthoScale (), mClipperPoints[i][nDim2] + 3 / vp->getOrthoScale ());
						glVertex2f (mClipperPoints[i][nDim1] + 3 / vp->getOrthoScale (), mClipperPoints[i][nDim2] - 3 / vp->getOrthoScale ());
						glVertex2f (mClipperPoints[i][nDim1] - 3 / vp->getOrthoScale (), mClipperPoints[i][nDim2] - 3 / vp->getOrthoScale ());
					}
					glEnd ();
				}
			}
			brushList::iterator it;
			// render unselected
			for (it = mpMap->getBrushList ().begin (); it != mpMap->getBrushList ().end (); it++)
			{
				(*it)->render (vp);
			}
	
			// render selected
			for (it = mpMap->getSelection ().begin (); it != mpMap->getSelection ().end (); it++)
			{
				(*it)->render (vp);
			}
	
			// rotation origin
			if (rotateGetMode ())
			{
				glColor4f (1.f,.0f,1.f,1.f);
				glBegin (GL_QUADS);
				glVertex2f (mRotationOrigin[nDim1] - 3 / vp->getOrthoScale (), mRotationOrigin[nDim2] + 3 / vp->getOrthoScale ());
				glVertex2f (mRotationOrigin[nDim1] + 3 / vp->getOrthoScale (), mRotationOrigin[nDim2] + 3 / vp->getOrthoScale ());
				glVertex2f (mRotationOrigin[nDim1] + 3 / vp->getOrthoScale (), mRotationOrigin[nDim2] - 3 / vp->getOrthoScale ());
				glVertex2f (mRotationOrigin[nDim1] - 3 / vp->getOrthoScale (), mRotationOrigin[nDim2] - 3 / vp->getOrthoScale ());
				glEnd ();
			}
	
			// world origin
			glDisable (GL_TEXTURE_2D);
			glEnable (GL_BLEND);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glColor4f (.0f,.0f,1.f,.5f);
			glBegin (GL_QUADS);
			glVertex2f (-8 / vp->getOrthoScale (), 8 / vp->getOrthoScale ());
			glVertex2f (8 / vp->getOrthoScale (), 8 / vp->getOrthoScale ());
			glVertex2f (8 / vp->getOrthoScale (), -8 / vp->getOrthoScale ());
			glVertex2f (-8 / vp->getOrthoScale (), -8 / vp->getOrthoScale ());
			glEnd ();
			glDisable (GL_BLEND);
	
			// camera position
			matrix4 view = mCameraMatrix;//vp->getViewMatrix ();
			vector3 origin = vector3::zero * view.inverse ();
			vector3 target = vector3 (0, 0, 50) * view.inverse ();
			glBegin (GL_LINES);
			glVertex2f (origin[nDim1], origin[nDim2]);
			glVertex2f (target[nDim1], target[nDim2]);
			glEnd ();
	
			// portals
	//		glLineWidth (3);
			for (size_t i = 0; i < portals.size (); i++)
			{
				glColor4f (.0f, 0.5f, .0f, 1.f);
				glBegin (GL_LINE_LOOP);
				for (size_t pt = 0; pt < portals[i].points.size (); pt++)
					glVertex2f (portals[i].points[pt][nDim1], portals[i].points[pt][nDim2]);
				glEnd ();
				glBegin (GL_LINES);
				glVertex2f (portals[i].points[0][nDim1], portals[i].points[0][nDim2]);
				glVertex2f (portals[i].points[portals[i].points.size ()-2][nDim1], portals[i].points[portals[i].points.size ()-2][nDim2]);
				glEnd ();
			}
	//		glLineWidth (1);
	
			//selection
			if (mbSelectingArea && vp == mpCurrentViewport)
			{
				vector3 eyepos = vp->getEyePos ();
				float scale = vp->getOrthoScale ();
				point p1 = {(int)(mMoveStartMousePt.x - vp->getWidth () / 2.f), (int)(mMoveStartMousePt.y - vp->getHeight () / 2)};
				point p2 = {(int)(mLastMousePt.x - vp->getWidth () / 2.f), (int)(mLastMousePt.y - vp->getHeight () / 2)};
				glColor4f (.0f, .0f, .0f, 1.f);
				glBegin (GL_LINE_STRIP);
				glVertex2f (p1.x/scale+eyepos[nDim1], -p1.y/scale+eyepos[nDim2]);
				glVertex2f (p2.x/scale+eyepos[nDim1], -p1.y/scale+eyepos[nDim2]);
				glVertex2f (p2.x/scale+eyepos[nDim1], -p2.y/scale+eyepos[nDim2]);
				glVertex2f (p1.x/scale+eyepos[nDim1], -p2.y/scale+eyepos[nDim2]);
				glVertex2f (p1.x/scale+eyepos[nDim1], -p1.y/scale+eyepos[nDim2]);
				glEnd ();
			}
	
			glEnable (GL_TEXTURE_2D);
			glMatrixMode (GL_PROJECTION);
			glPopMatrix ();
			glMatrixMode (GL_MODELVIEW);
			glPopMatrix ();
	
			switch (m)
			{
			case editorViewport::xy:
				mpFont->drawTextString ("Ortho XY", 0, 0, 0x7f000000ul);
				break;
			case editorViewport::xz:
				mpFont->drawTextString ("Ortho XZ", 0, 0, 0x7f000000ul);
				break;
			case editorViewport::yz:
				mpFont->drawTextString ("Ortho YZ", 0, 0, 0x7f000000ul);
				break;
			}
		}
		else
		{
			if (!mpEntMtl)
				mpEntMtl = fe::g_engine->getResourceMgr ()->createMaterial ("white");
			matrix4 world, view, proj;
			world.identity ();
			view = mCameraMatrix;
			proj.projection (60.f*3.14f/180.f, vp->getHeight () / (float)vp->getWidth (), .1f, 10000.f);
	        fe::g_engine->getEffectParms ()->setMatrix (fe::effectParm_WorldMatrix, world);
	        fe::g_engine->getEffectParms ()->setMatrix (fe::effectParm_ViewMatrix, view);
	        fe::g_engine->getEffectParms ()->setMatrix (fe::effectParm_ProjMatrix, proj);
			glColor4f (1.f,1.f,1.f,1.f);

			// update renderer cache for all brushes
			brushList::iterator it;
			for (it = getMap ()->getBrushList ().begin (); it != getMap ()->getBrushList ().end (); it++)
			{
				(*it)->updateRendererCache ();
			}
			for (it = getMap ()->getSelection ().begin (); it != getMap ()->getSelection ().end (); it++)
			{
				(*it)->updateRendererCache ();
			}
			fe::g_engine->getSceneManager ()->update ();
			backend_selection_renderer r;
			backend_selection_renderer2 r2;
			fe::g_engine->getRBackend ()->addPass (&r);
			fe::g_engine->getRBackend ()->addPass (&r2);
			fe::g_engine->getSceneManager ()->draw ();
			fe::g_engine->getRBackend ()->removePass (&r);
			fe::g_engine->getRBackend ()->removePass (&r2);
			
			glMatrixMode (GL_PROJECTION);
			glPushMatrix ();
			glLoadIdentity ();
			gluOrtho2D (-vp->getWidth ()/2, vp->getWidth ()/2, -vp->getHeight ()/2, vp->getHeight ()/2);
			glMatrixMode (GL_MODELVIEW);
			glPushMatrix ();
			matrix4 mtx = vp->getViewMatrix ();
			glLoadMatrixf ( (GLfloat*)&mtx);
			glTranslatef (0, 0, -1);
	
			glDisable (GL_TEXTURE_2D);
			if (mbSelectingArea && vp == mpCurrentViewport)
			{
				vector3 eyepos = vp->getEyePos ();
				float scale = vp->getOrthoScale ();
				point p1 = {(int)(mMoveStartMousePt.x - vp->getWidth () / 2.f), (int)(mMoveStartMousePt.y - vp->getHeight () / 2)};
				point p2 = {(int)(mLastMousePt.x - vp->getWidth () / 2.f), (int)(mLastMousePt.y - vp->getHeight () / 2)};
				glColor4f (.0f, .0f, .0f, 1.f);
				glBegin (GL_LINE_STRIP);
				glVertex2f (p1.x/scale, -p1.y/scale);
				glVertex2f (p2.x/scale, -p1.y/scale);
				glVertex2f (p2.x/scale, -p2.y/scale);
				glVertex2f (p1.x/scale, -p2.y/scale);
				glVertex2f (p1.x/scale, -p1.y/scale);
				glEnd ();
			}
			glEnable (GL_TEXTURE_2D);
			glMatrixMode (GL_PROJECTION);
			glPopMatrix ();
			glMatrixMode (GL_MODELVIEW);
			glPopMatrix ();


#if 0
			if (!mpSelectionMtl)
				mpSelectionMtl = fe::g_engine->getResourceMgr ()->createObject ("material", "common/color").dynamicCast <fe::material>();

			fe::g_engine->getSceneManager ()->setCurrentMtl (mpSelectionMtl);
			fe::material::effectRef *effect = mpSelectionMtl->getEffectRef ();
			fe::g_engine->getDrawUtil ()->ambientPass (true);
			// draw all selected brushes
			fe::g_engine->getRenderQueue ()->setCurrentPass (fe::PASS_OUTSIDE);
			
			fe::g_engine->getEffectParms ()->setFloat4 (fe::effectParm_AmbientColor, float4 (0.3, 0.3, 0.7, 0.3));
			glDisable (GL_TEXTURE_2D);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable (GL_BLEND);
			mpSelectionMtl->setupEffectInputs ();

			for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++)
			{
				brushPtr b = (*i);
				for (int s = 0; s < b->numSubsets (); s++)
				{
					if (effect && effect->ambient != -1 && effect->fx)
					{
						effect->fx->setTechnique (effect->ambient);
						b->prepare (s);
						int np = effect->fx->begin ();
						for (int p = 0; p < np; p++)
						{
							effect->fx->pass (p);
							b->render (s, p);
							effect->fx->finalizePass (p);
						}
						effect->fx->end ();
					}
				}
			}
			fe::g_engine->getEffectParms ()->setFloat4 (fe::effectParm_AmbientColor, float4 (1, 1, 1, 1));
			glDisable (GL_BLEND);
			mpSelectionMtl->setupEffectInputs ();
			mbDrawingSelectionOutlines = true;
			for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++)
			{
				brushPtr b = (*i);
				for (int s = 0; s < b->numSubsets (); s++)
				{
					if (effect && effect->ambient != -1 && effect->fx)
					{
						effect->fx->setTechnique (effect->ambient);
						b->prepare (s);
						int np = effect->fx->begin ();
						for (int p = 0; p < np; p++)
						{
							effect->fx->pass (p);
							b->render (s, p);
							effect->fx->finalizePass (p);
						}
						effect->fx->end ();
					}
				}
			}
			mbDrawingSelectionOutlines = false;

			fe::g_engine->getRenderQueue ()->setCurrentPass (fe::PASS_AMBIENT);
			#endif
	
	#if 0
			// draw camera axes
			glDisable (GL_BLEND);
			glDisable (GL_TEXTURE_2D);
			glBegin (GL_LINES);
			glColor3f (1, 0, 0);
			glVertex3f (0, 0, 0);
			glVertex3f (50, 0, 0);
			glColor3f (0, 1, 0);
			glVertex3f (0, 0, 0);
			glVertex3f (0, 50, 0);
			glColor3f (0, 0, 1);
			glVertex3f (0, 0, 0);
			glVertex3f (0, 0, 50);
			glEnd ();
	#endif
		}
	/*	glDisable (GL_TEXTURE_2D);
		fe::g_engine->getDrawUtil ()->setOrthoTransforms ();
		fe::g_engine->getDrawUtil ()->drawPic (0, 0, 50, 20, 0, 0, 1, 1, 0x7f003ffful, true);
		fnt->drawTextString (L"XYView", 0, 2, 0xffffffff);*/
	
	}
	
	// commands
	void			editorCore::addCommand (editorCmd *cmd)
	{
		// remove all commands above 'mCurrentCommand'
		if (!mCommands.empty ())
		{
			while ( (int)mCommands.size () > mCurrentCommand + 1)
			{
				delete mCommands.back ();
				mCommands.pop_back ();
			}
		}
		mCommands.push_back (cmd);
		cmd->doIt ();
		mCurrentCommand = (int)mCommands.size () - 1;
		mpMap->setModifiedFlag (true);
		getMainFrm ()->getSurfaceInspector ()->update ();
	}
	
	void editorCore::undo (void)
	{
		if (mCurrentCommand >= 0)
	    {
	        mCommands[mCurrentCommand]->undo ();
	        mCurrentCommand--;
	    }
	}
	
	void editorCore::redo (void)
	{
	    if (false == mCommands.empty () && mCurrentCommand < (int)mCommands.size () - 1)
	    {
	        mCurrentCommand++;
	        mCommands[mCurrentCommand]->doIt ();
	    }
	
	}
	
	// selects/deselects brush face using mouse pointer
	void			editorCore::selectFace (const point &pt)
	{
		// get ray and find intr
		ray3 ray;
		smartPtr <editorViewport> vp = getViewport ();
		smartPtr <brush> sel = NULL;
		if (vp->getViewMode () == editorViewport::perspective)
		{
			//ray = rayFromMousePos (pt, vp);
			ray = rayFromMousePos (pt, vp->getWidth (), vp->getHeight (), mCameraMatrix);
		}
		else
		{
			int nDim1 = (vp->getOrthoMode () == editorViewport::yz) ? 1 : 0;
			int nDim2 = (vp->getOrthoMode () == editorViewport::xy) ? 1 : 2;
			vector3 pos (0, 0, 0);
			pos[nDim1] = (pt.x - vp->getWidth () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
			pos[nDim2] = (-pt.y + vp->getHeight () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
			ray.direction () = vector3 (1, 1, 1);
			ray.direction ()[nDim1] = 0;
			ray.direction ()[nDim2] = 0;
			ray.origin () = vector3 (-1.0e+4f, -1.0e+4f, -1.0e+4f);
			ray.origin ()[nDim1] = pos[nDim1];
			ray.origin ()[nDim2] = pos[nDim2];
		}
		int face;
		sel = pickNearest (ray, &face);
		if (sel)
		{
			if (sel->getOwnerEnt () != mpMap->getWorldEntity ())
			{
				GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (getMainFrm ()->getWidget ()),
								GTK_DIALOG_DESTROY_WITH_PARENT,
								GTK_MESSAGE_INFO,
								GTK_BUTTONS_CLOSE,
								"You cannot select face of a brush linked to entity other than worldspawn");
				gtk_dialog_run (GTK_DIALOG (dialog));
				gtk_widget_destroy (GTK_WIDGET (dialog));
				
				return;
			}
	
			assert (face != -1);
	#if 0
			if (!sel->IsSelected ())	// will be selected now
				AddCommand (new CmdSelectBrushAdd (sel));
			sel->SelectFace (face, sel->IsFaceSelected (face) ? false : true);
			RedrawAllViews ();
	#endif
			addCommand (new CmdToggleFaceSelection (sel, face));
		}
	}
	
	// selects/deselects brush using mouse pointer
	void			editorCore::selectBrush (const point &pt)
	{
		// get ray and find intr
		ray3 ray;
		smartPtr <editorViewport> vp = getViewport ();
		smartPtr <brush> sel = NULL;
		if (vp->getViewMode () == editorViewport::perspective)
		{
		//	ray = rayFromMousePos (pt, vp);
			ray = rayFromMousePos (pt, vp->getWidth (), vp->getHeight (), mCameraMatrix);
		}
		else
		{
			int nDim1 = (vp->getOrthoMode () == editorViewport::yz) ? 1 : 0;
			int nDim2 = (vp->getOrthoMode () == editorViewport::xy) ? 1 : 2;
			vector3 pos (0, 0, 0);
			pos[nDim1] = (pt.x - vp->getWidth () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim1];
			pos[nDim2] = (-pt.y + vp->getHeight () / 2) / vp->getOrthoScale () + vp->getEyePos ()[nDim2];
			ray.direction () = vector3 (1, 1, 1);
			ray.direction ()[nDim1] = 0;
			ray.direction ()[nDim2] = 0;
			ray.origin () = vector3 (-1.0e+4f, -1.0e+4f, -1.0e+4f);
			ray.origin ()[nDim1] = pos[nDim1];
			ray.origin ()[nDim2] = pos[nDim2];
		}
		sel = pickNearest (ray);
	
		// toggle
		if (sel)
		{
			fprintf (stderr, "found brush\n");
			if ( (sel)->isSelected ())
			{
				fprintf (stderr, "deselect\n");
				addCommand (new CmdSelectBrushRemove (sel));
			}
			else
			{
				fprintf (stderr, "select\n");
				addCommand (new CmdSelectBrushAdd (sel));
			}
		}
	
#if 0	// what this was for??
		if ( (0 == (state & (GDK_SHIFT_MASK|GDK_CONTROL_MASK))) && !sel)
		{
			if (!mpMap->getSelection ().empty ())
				addCommand (new CmdSelectionClear ());
		}
#endif
	}
	
	// starts face drag mode
	void			editorCore::dragBegin (int x, int y, const ray3 &ray, bool persp)
	{
		smartPtr <brush> sel = NULL;
		point pt;
		pt.x = x;
		pt.y = y;
	
/*		int nDim1;
		int nDim2;
		if (!persp)
		{
			nDim1 = (ray.direction ().x != 0.f) ? 1 : 0;
			nDim2 = (ray.direction ().z != 0.f) ? 1 : 2;
		}*/
	
	//	if (persp)
		{
			std::map <float, smartPtr <brush> > objects;
			brushList::iterator it;
			for (it = mpMap->getBrushList ().begin (); it != mpMap->getBrushList ().end (); it++)
			{
				vector3 pt;
				if ( (*it)->isSelected () && (*it)->findRayIntr (ray, pt) != -1)
				{
					float d = (ray.origin () - pt).length ();
					objects[d] = *it;
				}
			}
			for (it = mpMap->getSelection ().begin (); it != mpMap->getSelection ().end (); it++)
			{
				vector3 pt;
				if ( (*it)->isSelected () && (*it)->findRayIntr (ray, pt) != -1)
				{
					float d = (ray.origin () - pt).length ();
					objects[d] = *it;
				}
			}
			sel = objects.size () ? (* (objects.begin ())).second : NULL;
		}
	/*	else
		{
			std::vector< Brush * >::iterator it;
			for (it = mpMap->getBrushList ().begin (); it != mpMap->getBrushList ().end (); it++)
			{
				if ( (*it)->IsSelected () && (*it)->TestPointIntr (pt))
				{
					sel = (*it);
					break;
				}
			}
	
			if (NULL == sel && mpMap->getSelection ().size () == 1 && GetSelection (0)->GetOwner ()->GetEClass ()->IsFixedSize ())
			{
				sel = GetSelection (0);
			}
		}*/
		if (sel)
		{
			// move selection
			mbMovingSelection = true;
			setLastMousePt (pt);
			mMoveStartMousePt = getLastMousePt ();
			mMoveVec = vector3 (0, 0, 0);
			mMovedVec = vector3 (0, 0, 0);
			mMovingBrushes = mpMap->getSelection ();
		}
		else
		{
			mSnappedFaceDragDir = mFaceDragDir = vector3 (0, 0, 0);
			// test against brush
			// drag faces
			mbDraggingFaces = true;
			setLastMousePt (pt);
			mMoveStartMousePt = pt;
			mDragFaces.resize (mpMap->getSelection ().size ());
			std::vector <const fe::plane*> planes;
			int ii = 0;
			for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++, ii++)
			{
				smartPtr <brush> b = *i;
	//			printf ("owner: 0x%X, eclass: 0x%X\n", b->getOwnerEnt (), b->getOwnerEnt ()->getEClass ());
				if (b->getOwnerEnt ()->getEClass ()->isFixedSize ())
					continue;
				for (int f = 0; f < b->numFaces (); f++)
				{
					vector3 p1 = ray.origin ();
					vector3 p2 = p1 + ray.direction () * 1.0e+5f * 2;
					for (int ff = 0; ff < b->numFaces (); ff++)
					{
						if (f == ff)
							continue;
						clipLineWithPlane (p1, p2, b->faces (ff).plane ());
					}
					if (p1 == ray.origin ())
						continue;
					if (clipLineWithPlane (p1, p2, b->faces (f).plane ()))
						continue;
	
					// f is our face
					mDragFaces[ii].push_back (f);
					size_t pp;
					for (pp = 0; pp < planes.size (); pp++)
					{
						if (!memcmp (planes[pp], &b->faces (f).plane (), sizeof (fe::plane)))
							break;
					}
					if (pp == planes.size ())
						planes.push_back (&b->faces (f).plane ());
				}
			}
			// add any faces with the same planes as chosen faces, but w/ opposite direction
			ii = 0;
			for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++, ii++)
			{
				smartPtr <brush> b = *i;
				if (b->getOwnerEnt ()->getEClass ()->isFixedSize ())
					continue;
				for (int f = 0; f < b->numFaces (); f++)
				{
					size_t pp;
					for (pp = 0; pp < planes.size (); pp++)
					{
						const fe::plane *plane = planes[pp];
						fe::plane plane2 (b->faces (f).plane ());
						plane2.normal () = -plane2.normal ();
						plane2.constant () = -plane2.constant ();
						if (fabs (plane->normal ().x - plane2.normal ().x) < 0.0001f
							&& fabs (plane->normal ().y - plane2.normal ().y) < 0.0001f
							&& fabs (plane->normal ().z - plane2.normal ().z) < 0.0001f
							&& fabs (plane->constant () - plane2.constant ()) < 0.0001f)
						{
							mDragFaces[ii].push_back (f);
							break;	// do not add face twice
						}
					}
				}
			}
		}
	}
	
	// updates drag process
	void			editorCore::dragUpdate (const vector3 &dir)
	{
		if (mbMovingSelection)
		{
			// movevec - snapped
			// movedvec - unsnapped
			for (brushList::iterator i = mMovingBrushes.begin (); i != mMovingBrushes.end (); i++)
			{
				 (*i)->move (-mMoveVec);
			}
	
			if (mMovingBrushes != mpMap->getSelection ())	// alt-tab or whatever changed selection
			{
				// cancel movement
				mbMovingSelection = false;
			}
			else
			{
				mMoveVec = mMovedVec + dir;
				snapPt (mMoveVec);
				for (brushList::iterator i = mMovingBrushes.begin (); i != mMovingBrushes.end (); i++)
				{
					 (*i)->move (mMoveVec);
				}
				mMovedVec += dir;
			}
		}
		else if (mbDraggingFaces)
		{
			assert (mDragFaces.size () == mpMap->getSelection ().size ());
			if (mDragFaces.size () != mpMap->getSelection ().size ())
				return;
			// restore initial
			int ii = 0;
			for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++, ii++)
			{
				smartPtr <brush> b = *i;
				size_t sz = mDragFaces[ii].size ();
				for (size_t f = 0; f < sz; f++)
				{
					b->faces ( (int)mDragFaces[ii][f]).planePt (0) -= mSnappedFaceDragDir;
					b->faces ( (int)mDragFaces[ii][f]).planePt (1) -= mSnappedFaceDragDir;
					b->faces ( (int)mDragFaces[ii][f]).planePt (2) -= mSnappedFaceDragDir;
				}
			}
	
			mFaceDragDir += dir;
			mSnappedFaceDragDir = mFaceDragDir;
			snapPt (mSnappedFaceDragDir);
	
			ii = 0;
			for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++, ii++)
			{
				smartPtr <brush> b = *i;
				for (size_t f = 0; f < mDragFaces[ii].size (); f++)
				{
					b->faces ( (int)mDragFaces[ii][f]).planePt (0) += mSnappedFaceDragDir;
					b->faces ( (int)mDragFaces[ii][f]).planePt (1) += mSnappedFaceDragDir;
					b->faces ( (int)mDragFaces[ii][f]).planePt (2) += mSnappedFaceDragDir;
				}
				b->build ();
			}
		}
	}
		
	// ends face drag mode
	void			editorCore::dragEnd (void)
	{
		if (mbMovingSelection)
		{
			snapPt (mMovedVec);
			// end moving
			// return object to it's initial pos
			for (brushList::iterator i = mMovingBrushes.begin (); i != mMovingBrushes.end (); i++)
			{
				 (*i)->move (-mMovedVec);
			}
			if (mMovingBrushes != mpMap->getSelection ()) 	// alt-tab or whatever changed selection
			{
				// cancel movement
				int i = 0;
			}
			else
			{
				CmdMoveSelection *cmd = new CmdMoveSelection (mMovedVec);
				addCommand (cmd);
			}
			mbMovingSelection = false;
		}
		else
		{
			// restore initial
			int ii = 0;
			for (brushList::iterator i= mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++, ii++)
			{
				for (size_t f = 0; f < mDragFaces[ii].size (); f++)
				{
					smartPtr <brush> b = *i;
					b->faces ( (int)mDragFaces[ii][f]).planePt (0) -= mSnappedFaceDragDir;
					b->faces ( (int)mDragFaces[ii][f]).planePt (1) -= mSnappedFaceDragDir;
					b->faces ( (int)mDragFaces[ii][f]).planePt (2) -= mSnappedFaceDragDir;
				}
			}
	
			// add command
			if (mSnappedFaceDragDir != vector3 (0, 0, 0))
				addCommand (new CmdBrushDragFaces (mDragFaces, mSnappedFaceDragDir));
	
			mDragFaces.resize (0);
			mbDraggingFaces = false;
		}
	}
		
	void	editorCore::allowRendering (bool onoff)
	{
		mbAllowRendering = onoff;
	}
	
	bool	editorCore::isRenderingAllowed (void) const
	{
		return mbAllowRendering;
	}
	
	void	editorCore::setViewport (smartPtr <editorViewport> vp)
	{
		mpCurrentViewport = vp;
	}
	
	smartPtr <editorViewport>		editorCore::getViewport (void) const
	{
		return mpCurrentViewport;
	}
	
	void	editorCore::registerView (glView *view)
	{
		if (std::find (mViewList.begin (), mViewList.end (), view) == mViewList.end ())
			mViewList.push_back (view);
	}
	
	void			editorCore::unregisterView (glView *view)
	{
		viewList::iterator it = std::find (mViewList.begin (), mViewList.end (), view);
		if (it != mViewList.end ())
			mViewList.erase (it);
	}
	
	// redraws all views
	void			editorCore::redrawAllViews (void) const
	{
		viewList::const_iterator it;
		for (it = mViewList.begin (); it != mViewList.end (); it++)
		{
			if ((*it)->isVisible ())
				(*it)->draw ();
		}
	}
	
	// snap point
	void			editorCore::snapPt (vector3 &pt, bool force) const
	{
		if (!mbSnapToGrid && !force)
			return;
		for (int i = 0; i < 3; i++)
		{
			int v = (int)pt[i];
			int rest = v % 8;
			if (rest < 0)
			{
				if (rest <= -4)
					v += -8 - rest;
				else
					v -= rest;
			}
			else
			{
				if (rest >= 4)
					v += 8 - rest;
				else
					v -= rest;
			}
			pt[i] = (float)v;
		}
	}
	
	
	// draw grid for specified viewport
	void			editorCore::drawGrid (void) const
	{
		editorViewport *viewport = fe::checked_cast <editorViewport *> (fe::g_engine->getViewport ());
		// q3radiant style colors
		float majorclr[] = {.5f, .5f, .5f, 1.f};
		float minorclr[] = {.8f, .8f, .8f, 1.f};
		float originclr[] = {.0f, .0f, .0f, 1.f};
		float orthobackclr[] = {1.f, 1.f, 1.f};
		float perspbackclr[] = {.25f, .25f, .56f, 1.f};
		float textclr[] = {.0f, .62f, .0f, 1.f};
		float textshadowclr[] = {.0f, .0f, .0f, .5f};
		{
			// assume:
			//  - scale is equal in all directions
			//  - only current orthomode axes are valid
	
			int nDim1 = (viewport->getOrthoMode () == editorViewport::yz) ? 1 : 0;
			int nDim2 = (viewport->getOrthoMode () == editorViewport::xy) ? 1 : 2;
	#if 1
			float scale = viewport->getOrthoScale ();
	//		if (scale > 0.2f)
			{
				float w, h;
				float x, y, xb, xe, yb, ye;
				int majorstep, minorstep;
	
				majorstep = 64;
				minorstep = 8;
	
				if (scale < 0.5f)
				{
					minorstep *= 8;
					majorstep *= 8;
				}
	
				if (scale < 0.05)
				{
					minorstep *= 8;
					majorstep *= 8;
				}
	
				w = viewport->getWidth () / 2 / scale;
				h = viewport->getHeight () / 2 / scale;
				vector3 origin = viewport->getEyePos ();
	
				vector3 region_mins (-128*1024, -128*1024, -128*1024);
				vector3 region_maxs (128*1024, 128*1024, 128*1024);
				xb = origin[nDim1] - w;
				if (xb < region_mins[nDim1])
					xb = region_mins[nDim1];
				xb = majorstep * floor (xb/majorstep);
	
				xe = origin[nDim1] + w;
				if (xe > region_maxs[nDim1])
					xe = region_maxs[nDim1];
				xe = majorstep * ceil (xe/majorstep);
	
				yb = origin[nDim2] - h;
				if (yb < region_mins[nDim2])
					yb = region_mins[nDim2];
				yb = majorstep * floor (yb/majorstep);
	
				ye = origin[nDim2] + h;
				if (ye > region_maxs[nDim2])
					ye = region_maxs[nDim2];
				ye = majorstep * ceil (ye/majorstep);
	
				int stepSize = majorstep;
	
				// draw lines
				float mins[2];
				float maxs[2];
				mins[0] = origin[nDim1] - w;
				maxs[0] = origin[nDim1] + w;
				mins[1] = origin[nDim2] - h;
				maxs[1] = origin[nDim2] + h;
	
				float* clr;
	
				uint numlines = 0;
	//			vertex *verts = reinterpret_cast< vertex * > (engine::get ()->tmpBuffer (2048 * sizeof (vertex)));
	//			vertex *vertexptr = verts;
	
	//			drawUtil::setOrthoTransforms ();	
				fe::g_engine->getDrawUtil ()->allowTransforms (false);
				glMatrixMode (GL_PROJECTION);
				glPushMatrix ();
				glLoadIdentity ();
				gluOrtho2D (mins[0], maxs[0], mins[1], maxs[1]);
				glMatrixMode (GL_MODELVIEW);
				glPushMatrix ();
	//			glTranslatef (viewport->getWidth ()/2, viewport->getHeight ()/2, 0);
				glLoadIdentity ();
	
				// draw minor blocks
				clr = minorclr;
				for (x = xb; x < xe; x += minorstep)
				{
					if (! ( (int)x & (majorstep-1)))
						continue;
					fe::g_engine->getDrawUtil ()->drawLine (x, yb, x, ye, clr);
					numlines++;
				}
				for (y=yb ; y<ye ; y+=minorstep)
				{
					if (! ( (int)y & (majorstep-1)))
						continue;
					fe::g_engine->getDrawUtil ()->drawLine (xb, y, xe, y, clr);
					numlines++;
				}
	
				// draw major blocks
				clr = majorclr;
				for (x=xb ; x<=xe ; x+=stepSize)
				{
					fe::g_engine->getDrawUtil ()->drawLine (x, yb, x, ye, clr);
					numlines++;
				}
				for (y=yb ; y<=ye ; y+=stepSize)
				{
					fe::g_engine->getDrawUtil ()->drawLine (xb, y, xe, y, clr);
					numlines++;
				}
	
				// draw origin
				clr = originclr;
				fe::g_engine->getDrawUtil ()->drawLine (0, yb, 0, ye, clr);
				numlines++;
				fe::g_engine->getDrawUtil ()->drawLine (xb, 0, xe, 0, clr);
				numlines++;
	
	/*			matrix4 m;
				m.orthoOffCenterLH (mins[0], maxs[0], mins[1], maxs[1], .01f, 1000.f);
				engine::get ()->effectParms ()->setMatrix (effectParmProjMatrix, m);
				m.identity ();
				engine::get ()->effectParms ()->setMatrix (effectParmWorldMatrix, m);
				engine::get ()->effectParms ()->setMatrix (effectParmViewMatrix, m);
				g_pGlobals->mpLineShader->setBestTechnique ();
	
				d3DUtil::drawPrimitive (NULL, g_pGlobals->mpLineShader, D3DPT_LINELIST, vertexptr, numlines*2, numlines);*/
				glMatrixMode (GL_PROJECTION);
				glPopMatrix ();
				glMatrixMode (GL_MODELVIEW);
				glPopMatrix ();
				fe::g_engine->getDrawUtil ()->allowTransforms (true);
			}
	#endif
	
	#if 0
			if (g_pGlobals->mpInterface->GetCurrentViewport () == viewport)
			{
				// set ortho matrix
				matrix4 m;
				m.ortho (rc.right, rc.bottom);
				engine::get ()->effectParms ()->setMatrix (effectParmProjMatrix, m);
				m.identity ();
				engine::get ()->effectParms ()->setMatrix (effectParmWorldMatrix, m);
				engine::get ()->effectParms ()->setMatrix (effectParmViewMatrix, m);
	
				// draw border
				vertex *verts = reinterpret_cast< vertex * > (engine::get ()->tmpBuffer (5 * sizeof (vertex)));
				vertex *vertexptr = verts;
				
				*verts++ = vertex (vector3 (0, rc.bottom-1, 1), 0x7f007f00, vector2 (0, 0));
				*verts++ = vertex (vector3 (rc.right-1, rc.bottom-1, 1), 0x7f007f00, vector2 (0, 0));
				*verts++ = vertex (vector3 (rc.right-1, 0, 1), 0x7f007f00, vector2 (0, 0));
				*verts++ = vertex (vector3 (0, 0, 1), 0x7f007f00, vector2 (0, 0));
				*verts++ = vertex (vector3 (0, rc.bottom-1, 1), 0x7f007f00, vector2 (0, 0));
	
				d3DUtil::drawPrimitive (NULL, g_pGlobals->mpLineShader, D3DPT_LINESTRIP, vertexptr, 5, 4);
	
				verts = reinterpret_cast< vertex * > (engine::get ()->tmpBuffer (5 * sizeof (vertex)));
				vertexptr = verts;
				
				*verts++ = vertex (vector3 (1, rc.bottom-2, 1), 0x7f007f00, vector2 (0, 0));
				*verts++ = vertex (vector3 (rc.right-2, rc.bottom-2, 1), 0x7f007f00, vector2 (0, 0));
				*verts++ = vertex (vector3 (rc.right-2, 1, 1), 0x7f007f00, vector2 (0, 0));
				*verts++ = vertex (vector3 (1, 1, 1), 0x7f007f00, vector2 (0, 0));
				*verts++ = vertex (vector3 (1, rc.bottom-2, 1), 0x7f007f00, vector2 (0, 0));
	
				d3DUtil::drawPrimitive (NULL, g_pGlobals->mpLineShader, D3DPT_LINESTRIP, vertexptr, 5, 4);
			}
	#endif
	
	//		matrix4 m;
	//		m.orthoLH (viewport->getWidth (), viewport->getHeight (), .01f, 1000.f);
	//		engine::get ()->effectParms ()->setMatrix (effectParmProjMatrix, m);
	//		engine::get ()->effectParms ()->setMatrix (effectParmViewMatrix, viewport->GetViewMatrix ());
	
	//		g_pGlobals->mpInterface->Render (viewport);
	
	//		wStr s;
	//		s.printf (L"ortho (%s)", viewport->GetOrthoMode () == ViewportParms::OM_XY ? L"XY" : (viewport->GetOrthoMode () == ViewportParms::OM_XZ ? L"XZ" : L"YZ"));
	//		mpArial8Font->drawTextString (s, 1, 1, textshadowclr);
	//		mpArial8Font->drawTextString (s, 0, 0, textclr);
		}
	}
		
	// script (python)
	
	// executes 1 string of python code, prints result message into scriptview
	void			editorCore::pyRunString (const cStr &string)
	{
		//CMainFrame *mpMainFrm = checked_cast<CMainFrame *> (AfxGetMainWnd ());
	/*	PyObject *obj = PyRun_String ( (char *) (string+"\n").c_str (), Py_eval_input, static_cast<PyObject*> (mpPyMainDictionary), static_cast<PyObject*> (mpPyMainDictionary));
		cStr res = "";
		if (!obj)
		{
			PyErr_Clear ();
			obj = PyRun_String ( (char *)string.c_str (), Py_single_input, static_cast<PyObject*> (mpPyMainDictionary), static_cast<PyObject*> (mpPyMainDictionary));
			Py_XDECREF	 (obj);
		}
		if (obj)
		{
			PyObject *str = PyObject_Repr (obj);
			if (str)
			{
				res = PyString_AsString (str);
				mpMainFrm->getScriptView ()->print (str (res).c_str ());
				return res;
			}
			Py_XDECREF (str);
			Py_XDECREF (obj);
		}*/
		mPyCmdHistory.push_back (string);
		mPyHistoryCursor = mPyCmdHistory.size ();
		int i = PyRun_SimpleString ((char *)string.c_str ());
		if (PyErr_Occurred ())
		{
			PyErr_Print ();
			PyErr_Clear ();
			mpMainFrm->getScriptView ()->print ("*** Error in expression.\r");
		}
	}
	
	cStr		editorCore::pyHistoryPrev (void)
	{
		mPyHistoryCursor--;
		if (mPyHistoryCursor < 0)
			mPyHistoryCursor = 0;
		return mPyHistoryCursor < mPyCmdHistory.size () ? mPyCmdHistory[mPyHistoryCursor] : "";
	}
	
	cStr		editorCore::pyHistoryNext (void)
	{
		mPyHistoryCursor++;
		if (mPyHistoryCursor > mPyCmdHistory.size ())
			mPyHistoryCursor = mPyCmdHistory.size ();
		if (mPyHistoryCursor < 0)
			mPyHistoryCursor = 0;
		return mPyHistoryCursor < mPyCmdHistory.size () ? mPyCmdHistory[mPyHistoryCursor] : "";
		
	}
	
	void			editorCore::pyRunScript (const char *filename)
	{
		cStr s = "execfile (r\"";
		s += filename;
		s += "\")";
		pyRunString (s);
	
	/*
		char *c;
		while (c = strchr ( (char*)filename, '\\'))
		{
			*c = '/';
		}
	
		FILE *fp = fopen (filename, "rb");
		PyRun_SimpleFile (fp, filename);
		fclose (fp);
		*/
	
	//    PyImport_ImportModule ( (char*)filename);
	}
	
	
	//--------------------------------------
	// python methods
	// FIXME: move to some class/namespace
	//--------------------------------------
	extern "C" PyObject *PyPrintMsg (PyObject *self, PyObject *args)
	{
	    char *name;
		if (!PyArg_ParseTuple (args, (char*)"s", &name))
		{
			PyErr_Print ();
			PyErr_Clear ();
			return Py_None;
		}
	
		if (name)
		{
			g_editor->getMainFrm ()->getScriptView ()->print (cStr (name) + "\n");
		}    
	
	    return PyRetVal;
	}
	
	extern "C" PyObject *PyVid_Restart (PyObject *self, PyObject *args)
	{
		fe::g_engine->getConsole ()->command ("r_restart");
		g_editor->redrawAllViews ();
	/*	if (false == g_pGlobals->mpMtlBrowser->IsHidden ())
			g_pGlobals->mpMtlBrowser->GetTexBrowser ()->Draw ();*/
	    return PyRetVal;
	}
	
	extern "C" PyObject *PyPrintOwners (PyObject *self, PyObject *args)
	{
	/*	CMainFrame *mpMainFrm = checked_cast<CMainFrame *> (AfxGetMainWnd ());
	
		mpMainFrm->getScriptView ()->print (_T ("Current brush owners:"));
		
		editorCore::BrushList::iterator i;
	    for (i = g_pGlobals->mpInterface->GetBrushList ().begin (); i != g_pGlobals->mpInterface->GetBrushList ().end (); i++)
		{
			Brush *b = *i;
			str s;
			s.printf (_T ("  %0.2d -- %s"), i, str (b->GetOwner ()->GetEClass ()->GetName ()).c_str ());
			mpMainFrm->getScriptView ()->print (s);
		}
		for (i = g_pGlobals->mpInterface->GetSelection ().begin (); i != g_pGlobals->mpInterface->GetSelection ().end (); i++)
		{
			Brush *b = *i;
			str s;
			s.printf (_T ("  %0.2d -- %s"), i, str (b->GetOwner ()->GetEClass ()->GetName ()).c_str ());
			mpMainFrm->getScriptView ()->print (s);
		}
		mpMainFrm->getScriptView ()->print (_T ("Ready"));
	*/
	    return PyRetVal;
	}
	
	extern "C" PyObject *PyBrush_Create (PyObject *self, PyObject *args)
	{
		fprintf (stderr, "PyBrush_Create\n");
		vector3 mins, maxs;
		if (!PyArg_ParseTuple (args, (char*)"ffffff", &mins.x, &mins.y, &mins.z, &maxs.x, &maxs.y, &maxs.z))
		{
			PyErr_Print ();
			PyErr_Clear ();
			return NULL;
		}
	
		brushPtr b = new brush (mins, maxs);
		fprintf (stderr, "create brush *brush=0x%p\n", (const brush *)b);
		g_editor->addCommand (new CmdCreateBrush (b));
	    PyObject *retval = Py_BuildValue ((char*)"O", b->getPyObject ());
		b->decrefPyObject ();
		return retval;
	}
	
	// FIXME: add another command for that!
	extern "C" PyObject *PyBrush_Rotate (PyObject *self, PyObject *args)
	{
		PyObject *pyobj;
		vector3 axis;
		vector3 origin;
		float angle;
		fprintf (stderr, "brush_rotate\n");
		if (!PyArg_ParseTuple (args, (char*)"Offfffff", &pyobj, &axis.x, &axis.y, &axis.z, &origin.x, &origin.y, &origin.z, &angle))
		{
			PyErr_Print ();
			PyErr_Clear ();
			return NULL;
		}
		pyObjectHolder<brush> *b = (pyObjectHolder<brush> *)pyobj;
		fprintf (stderr, "rotate brush *brush=0x%X\n", (const brush *)b->getObject ());
	
		g_editor->addCommand (new CmdRotateBrush (b->getObject (), axis, origin, (int)angle));
	
	    return PyRetVal;
	}
	
	extern "C" PyObject *PyDlg_Create (PyObject *self, PyObject *args)
	{
	#if 0
	    char *cap;
		if (!PyArg_ParseTuple (args, "s", &cap))
		{
			PyErr_Print ();
			PyErr_Clear ();
			return Py_None;
		}
	
		PythonDlg *dlg = new PythonDlg (CString (cap));
	    return Py_BuildValue ("i", dlg);
	#endif
		return PyRetVal;
	}
	
	extern "C" PyObject *PyDlg_AddEditBox (PyObject *self, PyObject *args)
	{
	#if 0
	    PythonDlg *dlg;
		char *cap, *val;
		if (!PyArg_ParseTuple (args, "iss", &dlg, &cap, &val))
		{
			PyErr_Print ();
			PyErr_Clear ();
			return Py_None;
		}
	
		dlg->AddEditBox (CString (cap), CString (val));
	#endif
	    return PyRetVal;
	}
	
	extern "C" PyObject *PyDlg_Run (PyObject *self, PyObject *args)
	{
	#if 0
		PythonDlg *dlg;
		if (!PyArg_ParseTuple (args, "i", &dlg))
		{
			PyErr_Print ();
			PyErr_Clear ();
			return Py_None;
		}
	
	    return Py_BuildValue ("i", dlg->Run (AfxGetMainWnd ()));
	#endif
		return PyRetVal;
	}
	
	extern "C" PyObject *PyDlg_Free (PyObject *self, PyObject *args)
	{
	#if 0
	    PythonDlg *dlg;
		if (!PyArg_ParseTuple (args, "i", &dlg))
		{
			PyErr_Print ();
			PyErr_Clear ();
			return Py_None;
		}
		delete dlg1;
	#endif
	    return PyRetVal;
	}
	
	// clipper
	void			editorCore::clipperToggle (void)
	{
		mbClipperMode = !mbClipperMode;
		mClipperNumPoints = 0;
		redrawAllViews ();
	}
	
	bool			editorCore::clipperGetMode (void) const
	{
		return mbClipperMode;
	}
	
	void			editorCore::clipperSplitSelection (void)
	{
		smartPtr<editorViewport> vp = getViewport ();
		if (mClipperNumPoints < 2 || !mbClipperMode)
			return;
	
		if (mClipperNumPoints == 2)
		{
			editorViewport::orthoMode m = vp->getOrthoMode ();
			int nDim1 = (m == editorViewport::yz) ? 1 : 0;
			int nDim2 = (m == editorViewport::xy) ? 1 : 2;
			vector3 pos (1.0e+5f, 1.0e+5f, 1.0e+5f);
			pos[nDim1] = mClipperPoints[1][nDim1];
			pos[nDim2] = mClipperPoints[1][nDim2];
			mClipperPoints[mClipperNumPoints] = pos;
	/*		fprintf (stderr, "splitting selection with: %f %f %f     %f %f %f   %f %f %f\n"
				, mClipperPoints[0][0], mClipperPoints[0][1], mClipperPoints[0][2]
				, mClipperPoints[1][0], mClipperPoints[1][1], mClipperPoints[1][2]
				, mClipperPoints[2][0], mClipperPoints[2][1], mClipperPoints[2][2]);*/
		}
	
		addCommand (new CmdSplitSelection (mClipperPoints));
	//	if (clipperGetMode ())
			clipperToggle ();
	}
	
	void			editorCore::clipperClipSelection (void)
	{
	}
	
	void			editorCore::clipperFlipNormal (void)
	{
	}
	
	void			editorCore::rotateToggle (void)
	{
		mbRotateMode = !mbRotateMode;
		if (mbRotateMode)
		{
			mbRotationMovingOrigin = false;
			// get origin
			mRotationOrigin = vector3::zero;
			for (brushList::iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++)
			{
				mRotationOrigin += (*i)->mins ();
				mRotationOrigin += (*i)->maxs ();
			}
			mRotationOrigin /= mpMap->getSelection ().size () * 2.f;
		}
		redrawAllViews ();
	}
	
	bool			editorCore::rotateGetMode (void) const
	{
		return mbRotateMode;
	}
	
	void			editorCore::loadConfig (const char *fname)
	{
		fe::charParser p(NULL, fname, true);
		for (;;)
		{
			p.errMode (0);
			if (p.getToken ())
				break;
			p.errMode (1);
			cStr key = p.token ();
			if (strncmp (key.c_str (), "editor_", 7))
			{
				cStr s = key;
				while (!p.isEOL ())
				{
					p.getToken ();
					s += " \"";
					s += p.token ();
					s += "\"";
				}
				fe::g_engine->getConsole ()->command (s);
			}
			else
			{
				p.getToken ();
				mSettings[key] = p.token ();
			}
		}
	}
	
	// resets current map to scratch
	void			editorCore::reset (void)
	{
		mpMap->reset ();
		// delete commands
		for (std::vector<editorCmd *>::iterator it = mCommands.begin (); it != mCommands.end (); it++)
			 delete (*it);
		mCommands = cmdList ();
		mCurrentCommand = -1;	// empty command list
	}
	
	// camera access
	const matrix4& editorCore::getCameraMatrix (void) const
	{
		return mCameraMatrix;
	}
	void editorCore::setCameraMatrix (const matrix4 &mtx)
	{
		mCameraMatrix = mtx;
	}
	
	smartPtr <fe::material>	editorCore::getEntMtl (void) const
	{
		return mpEntMtl;
	}
	
	bool editorCore::canCreateEntityFromSelection (const eClass *eclass) const
	{
		const brushList &lst = mpMap->getSelection ();
		if (!eclass)
		{
			getMainFrm ()->getLogView ()->print ("invalid eclass!\n");
			return false;
		}
		
		if (!eclass->isFixedSize () && lst.empty ())
		{
			getMainFrm ()->getLogView ()->print ("this type of entity needs selected brushes\n");
			return false;
		}
	
		if (!eclass->isFixedSize ())
		{
			for (brushList::const_iterator it = lst.begin (); it != lst.end (); it++)
			{
				if ((*it)->getOwnerEnt () != mpMap->getWorldEntity ())
				{
					getMainFrm ()->getLogView ()->print ("only world brushes allowed for entity creation\n");
					return false;
				}
			}
		}
		return true;
	}
	
	smartPtr <brush> editorCore::getEntBrush (const entity *ent) const
	{
		for (brushList::const_iterator i = mpMap->getSelection ().begin (); i != mpMap->getSelection ().end (); i++)
		{
			if ((*i)->getOwnerEnt () == ent)
				return *i;
		}
		for (brushList::const_iterator i = mpMap->getBrushList ().begin (); i != mpMap->getBrushList ().end (); i++)
		{
			if ((*i)->getOwnerEnt () == ent)
				return *i;
		}
		return NULL;
	}
	
	smartPtr <editorMap>	editorCore::getMap (void) const
	{
		return mpMap;
	}
	
	void editorCore::toggleGrid (void)
	{
		mbDrawGrid = !mbDrawGrid;
	}
	
	const char *editorCore::getValueForKey (const char *key) const
	{
		std::map <cStr, cStr>::const_iterator i;
		i = mSettings.find (key);
		if (i != mSettings.end ())
			return (*i).second.c_str ();
		return "";
	}

	bool editorCore::isDrawingSelectionOutlines (void) const
	{
		return mbDrawingSelectionOutlines;
	}
	
	void editorCore::selectBegin (int x, int y)
	{
		mbSelectingArea = true;
		mMoveStartMousePt.x = x;
		mMoveStartMousePt.y = y;
	}

	void editorCore::selectUpdate (int x, int y)
	{
		mLastMousePt.x = x;
		mLastMousePt.y = y;
	}

	void editorCore::selectEnd (int x, int y)
	{
		mbSelectingArea = false;
		mLastMousePt.x = x;
		mLastMousePt.y = y;
		if (mLastMousePt.x == mMoveStartMousePt.x
			&& mLastMousePt.y == mMoveStartMousePt.y)
		{
			if (mbSelectingBrushes)
				selectBrush (mLastMousePt);
			else if (mbSelectingFaces)
				selectFace (mLastMousePt);
			return;
		}

		smartPtr<editorViewport> vp = mpCurrentViewport;

		// create 4 planes
		vector3 frustum[4][3];
		int nDim1, nDim2, nDim3;
		point smins, smaxs;
		brushList sel, desel;

		if (vp->getViewMode () == editorViewport::ortho)
		{
			editorViewport::orthoMode m = vp->getOrthoMode ();
			nDim1 = (m == editorViewport::yz) ? 1 : 0;
			nDim2 = (m == editorViewport::xy) ? 1 : 2;
			nDim3 = (m == editorViewport::xy) ? 2 : 1;
			fprintf (stderr, "%d %d %d\n", nDim1, nDim2, nDim3);

			vector3 eyepos = vp->getEyePos ();
			float scale = vp->getOrthoScale ();
			mMoveStartMousePt.x = (int)((mMoveStartMousePt.x - vp->getWidth () / 2.f) / scale + eyepos[nDim1]);
			mLastMousePt.x = (int)((mLastMousePt.x - vp->getWidth () / 2.f) / scale + eyepos[nDim1]);

			mMoveStartMousePt.y = (int)(-(mMoveStartMousePt.y - vp->getHeight () / 2.f) / scale + eyepos[nDim2]);
			mLastMousePt.y = (int)(-(mLastMousePt.y - vp->getHeight () / 2.f) / scale + eyepos[nDim2]);

			smins.x = std::min (mLastMousePt.x, mMoveStartMousePt.x);
			smins.y = std::min (mLastMousePt.y, mMoveStartMousePt.y);
			smaxs.x = std::max (mLastMousePt.x, mMoveStartMousePt.x);
			smaxs.y = std::max (mLastMousePt.y, mMoveStartMousePt.y);

			if (smins.x == smaxs.x)
				smaxs.x += 1;
			if (smins.y == smaxs.y)
				smaxs.y += 1;

#define init_frust(v,x,y,z) {v[nDim1] = x; v[nDim2] = y; v[nDim3] = z;}

			// left
			init_frust (frustum[0][0], smins.x, smins.y, 0);
			init_frust (frustum[0][1], smins.x, smaxs.y, 0);
			init_frust (frustum[0][2], smins.x, smins.y, 32);
			
			// right
			init_frust (frustum[1][0], smaxs.x, smaxs.y, 0);
			init_frust (frustum[1][1], smaxs.x, smins.y, 0);
			init_frust (frustum[1][2], smaxs.x, smaxs.y, 32);

			// bottom
			init_frust (frustum[2][0], smaxs.x, smins.y, 0);
			init_frust (frustum[2][1], smins.x, smins.y, 0);
			init_frust (frustum[2][2], smaxs.x, smins.y, 32);
			
			// top
			init_frust (frustum[3][0], smins.x, smaxs.y, 0);
			init_frust (frustum[3][1], smaxs.x, smaxs.y, 0);
			init_frust (frustum[3][2], smins.x, smaxs.y, 32);
#undef init_frust
		}
		else
		{
			smins.x = std::min (mLastMousePt.x, mMoveStartMousePt.x);
			smins.y = std::min (mLastMousePt.y, mMoveStartMousePt.y);
			smaxs.x = std::max (mLastMousePt.x, mMoveStartMousePt.x);
			smaxs.y = std::max (mLastMousePt.y, mMoveStartMousePt.y);

			if (smins.x == smaxs.x)
				smaxs.x += 1;
			if (smins.y == smaxs.y)
				smaxs.y += 1;
			
			// create frustum planes from 4 rays
			point pt[4] = {
				smins,
				{smaxs.x, smins.y},
				smaxs,
				{smins.x, smaxs.y}
			};
			ray3 rays[4];
			rays[0] = rayFromMousePos (pt[0], vp->getWidth (), vp->getHeight (), mCameraMatrix);
			rays[1] = rayFromMousePos (pt[1], vp->getWidth (), vp->getHeight (), mCameraMatrix);
			rays[2] = rayFromMousePos (pt[2], vp->getWidth (), vp->getHeight (), mCameraMatrix);
			rays[3] = rayFromMousePos (pt[3], vp->getWidth (), vp->getHeight (), mCameraMatrix);

			for (int i = 0; i < 4; i++)
			{
				fprintf (stderr, "ray %d: [%0.2f %0.2f %0.2f] [%0.2f %0.2f %0.2f]\n"
					, i, rays[i].origin().x, rays[i].origin().y, rays[i].origin().z
					, rays[i].direction().x, rays[i].direction().y, rays[i].direction().z);
			}

			frustum[0][0] = rays[0].origin ();
			frustum[0][1] = rays[3].origin () + rays[3].direction () * 100;
			frustum[0][2] = rays[0].origin () + rays[0].direction () * 100;
			
			frustum[1][0] = rays[1].origin ();
			frustum[1][1] = rays[1].origin () + rays[1].direction () * 100;
			frustum[1][2] = rays[2].origin () + rays[2].direction () * 100;
			
			frustum[2][0] = rays[0].origin ();
			frustum[2][1] = rays[0].origin () + rays[0].direction () * 100;
			frustum[2][2] = rays[1].origin () + rays[1].direction () * 100;
			
			frustum[3][0] = rays[2].origin ();
			frustum[3][1] = rays[2].origin () + rays[2].direction () * 100;
			frustum[3][2] = rays[3].origin () + rays[3].direction () * 100;
			
			for (int i = 0; i < 4; i++)
			{
				fe::plane pl;
				pl.fromPoints (frustum[i][0], frustum[i][1], frustum[i][2]);
				fprintf (stderr, "points for %d: ", i);
				for (int k = 0; k < 3; k++)
				{
					fprintf (stderr, "[%0.2f %0.2f %0.2f] ", frustum[i][k].x, frustum[i][k].y, frustum[i][k].z);
				}
				fprintf (stderr, "\n");
				fprintf (stderr, "plane %d: [%0.2f %0.2f %0.2f %0.2f]\n"
					, i, pl.normal().x, pl.normal().y, pl.normal().z, pl.constant());
			}
		}

		for (brushList::iterator b = mpMap->getBrushList ().begin (); b != mpMap->getBrushList ().end (); b++)
		{
			bool aab_intr = false;

			if (vp->getViewMode () == editorViewport::perspective)
				aab_intr = true;
			else
			{
				vector3 mins = (*b)->mins ();
				vector3 maxs = (*b)->maxs ();
				// this will speed-up intersection testing in xy-view
				if (!((mins[nDim1] > smaxs.x || maxs[nDim1] < smins.x) 
							|| (mins[nDim2] > smaxs.y || maxs[nDim2] < smins.y)))
				{
					aab_intr = true;
				}
			}

			if (aab_intr)
			{
				// more precise check
				brush bb(*(*b));
				int i;
				for (i = 0; i < 4; i++)
				{
					fprintf (stderr, "clipping with plane %d\n", i);
					if (!bb.clip (frustum[i]))
						break;
				}

				if (i == 4)
					sel.push_back (*b);
			}			
		}
		for (brushList::iterator b = mpMap->getSelection ().begin (); b != mpMap->getSelection ().end (); b++)
		{
			bool aab_intr = false;

			if (vp->getViewMode () == editorViewport::perspective)
				aab_intr = true;
			else
			{
				vector3 mins = (*b)->mins ();
				vector3 maxs = (*b)->maxs ();
				// this will speed-up intersection testing in xy-view
				if (!((mins[nDim1] > smaxs.x || maxs[nDim1] < smins.x) 
							|| (mins[nDim2] > smaxs.y || maxs[nDim2] < smins.y)))
				{
					aab_intr = true;
				}
			}

			if (aab_intr)
			{
				// more precise check
				brush bb(*(*b));
				int i;
				for (i = 0; i < 4; i++)
				{
					fprintf (stderr, "clipping with plane %d\n", i);
					if (!bb.clip (frustum[i]))
						break;
				}

				if (i == 4)
					desel.push_back (*b);
			}			
		}
		if (!sel.empty () || !desel.empty ())
			addCommand (new CmdSelectOrDeselectBrushes (sel, desel));
	}

}
