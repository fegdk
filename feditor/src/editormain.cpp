// $Id$

// application entry point
// gets application object pointer and starts it up
#include "pch.h"
#include <fegdk/f_engine.h>
#include "editorapp.h"

using namespace feditor;

#ifdef _WIN32
int
CALLBACK WinMain (HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	int argc = __argc;
	char **argv = __argv;
#else
int
main (int argc, char *argv[])
{
#endif
	fe::engine *eng = new fe::engine;
	smartPtr <fe::application> app = new feditor::editorApp (argc, argv);

	eng->run (app, argc, argv);	// app will tell the engine about the way it want's to be running
	app.dynamicCast <feditor::editorApp> ()->run (argc, argv); // execute editor

	app = NULL;
	eng->release ();
	return 0;
}

