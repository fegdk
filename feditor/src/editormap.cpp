#include "pch.h"
#include <fegdk/f_engine.h>
#include <fegdk/f_scenemanager.h>
#include <gtk/gtk.h>
#include <iostream>
#include <fstream>
#include "editormap.h"
#include "entity.h"
#include "editorcore.h"
#include "mainfrm.h"
#include "entityinspector.h"
#include "brush.h"
#include "editorcmd.h"

using namespace std;

namespace feditor
{
	
	editorMap::editorMap (void)
	{
		mbIsModified = false;
		smartPtr <entity> ent = new entity ();
		ent->setAttrValue ("classname", "worldspawn");
		mEntList.push_back (ent);
	}
	
	editorMap::~editorMap (void)
	{
		reset ();
	}
	
	// resets map
	void editorMap::reset (void)
	{
		// brush can keep references to entities, so we need to unlink
		// (we'll get memleaks if we don't do that)
		clearSelection ();
		for (brushList::iterator it = mBrushList.begin (); it != mBrushList.end (); it++)
			fe::g_engine->getSceneManager ()->remove ((*it).dynamicCast <fe::sceneObject> ());
		mBrushList = brushList ();
		mSelection = brushList ();
	}
	
	// inserts brush into scene
	void			editorMap::insertBrush (smartPtr <brush> obj, bool select)
	{
		assert (obj);
		assert (find (mBrushList.begin (), mBrushList.end (), obj) == mBrushList.end ());
	
		if (select)
			addSelection (obj);
		else
			mBrushList.push_back (obj);
		
		fe::g_engine->getSceneManager ()->add (obj.dynamicCast <fe::sceneObject> ());
	}
		
	// removes brush from scene
	void			editorMap::removeBrush (smartPtr <brush> obj)
	{
		brushList::iterator it;
		it = find (mBrushList.begin (), mBrushList.end (), obj);
		if (it != mBrushList.end ())
		{
			mBrushList.erase (it);
		}
		else
		{
			it = find (mSelection.begin (), mSelection.end (), obj);
			if (it != mSelection.end ())
			{
				mSelection.erase (it);
			}
			else
			{
				GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (g_editor->getMainFrm ()->getWidget ()),
								GTK_DIALOG_DESTROY_WITH_PARENT,
								GTK_MESSAGE_INFO,
								GTK_BUTTONS_CLOSE,
								"Failed to remove brush: it's not in lists");
				gtk_dialog_run (GTK_DIALOG (dialog));
				gtk_widget_destroy (GTK_WIDGET (dialog));
			}
		}
		fe::g_engine->getSceneManager ()->remove (obj.dynamicCast <fe::sceneObject> ());
		if (mSelection.empty ())
			g_editor->getMainFrm ()->getEntityInspector ()->setEntity (NULL);
		else
			g_editor->getMainFrm ()->getEntityInspector ()->setEntity (mSelection.front ()->getOwnerEnt ());
	}
	
	// returns list of scene brushes
	const brushList&		editorMap::getBrushList (void) const
	{
		return mBrushList;
	}
	
	brushList&		editorMap::getBrushList (void)
	{
		return mBrushList;
	}
	
	// returns list of selected brushes
	const brushList&	editorMap::getSelection (void) const
	{
		return mSelection;
	}
	
	brushList&	editorMap::getSelection (void)
	{
		return mSelection;
	}
	
	// selects specified brush
	void			editorMap::addSelection (smartPtr <brush> o)
	{
		// now we don't auto-select all brushes belonging to entity, and all brushes are only present in one list simultaneously
		brushList::iterator it = find (mBrushList.begin (), mBrushList.end (), o);
		assert (it != mBrushList.end ());
	
		if (it != mBrushList.end ())
		{
			mBrushList.erase (it);
			mSelection.push_back (o);
			o->select (true);
	//		if (mpActiveBrush)
	//			g_editor->getMainFrm ()->getEntityInspector ()->setEntity (NULL);
	//		else
				g_editor->getMainFrm ()->getEntityInspector ()->setEntity (mSelection.front ()->getOwnerEnt ());
	
		}
	}
	
	// unselects specified brush
	void			editorMap::removeSelection (smartPtr <brush> o)
	{
		brushList::iterator it = find (mSelection.begin (), mSelection.end (), o);
		assert (it != mSelection.end ());
		if (it != mSelection.end ())
		{
			mSelection.erase (it);
			mBrushList.push_back (o);
			o->select (false);
			if (mSelection.empty ())
				g_editor->getMainFrm ()->getEntityInspector ()->setEntity (NULL);
			else
				g_editor->getMainFrm ()->getEntityInspector ()->setEntity (mSelection.front ()->getOwnerEnt ());
		}
	}
	
	// sets "modified" flag
	void			editorMap::setModifiedFlag (bool isModified)
	{
		mbIsModified = true;
	}
	
	// returns "modified" flag
	bool			editorMap::getModifiedFlag (void) const
	{
		return mbIsModified;
	}
	
	// clears selection
	void			editorMap::clearSelection (void)
	{
		for (brushList::iterator i = mSelection.begin (); i != mSelection.end (); i++)
		{
			 (*i)->select (false);
			mBrushList.push_back (*i);
		}
		mSelection.clear ();
		if (g_editor->getMainFrm ())
			g_editor->getMainFrm ()->getEntityInspector ()->setEntity (NULL);
	}
	
	// returns list of entities
	const entList&	editorMap::getEntities (void) const
	{
		return mEntList;
	}
	
	// returns worldspawn entity
	smartPtr <entity>	editorMap::getWorldEntity (void) const
	{
		return mEntList.front ();
	}
	
	void editorMap::save (void) const
	{
		if (mFileName.empty ())
			saveAs ();
		else
		{
			ofstream file (mFileName, ofstream::out|ofstream::trunc);
			for (entList::const_iterator it = mEntList.begin (); it != mEntList.end (); it++)
			{
				saveEntity (file, *it);
			}
			file.close ();
		}
	}
	
	void editorMap::save (stringstream &s, bool selected) const
	{
		for (entList::const_iterator it = mEntList.begin (); it != mEntList.end (); it++)
		{
			saveEntity (s, *it, selected);
		}
	}
	
	void editorMap::saveAs (void) const
	{
		GtkWidget *dlg = gtk_file_chooser_dialog_new ("Save As ...", GTK_WINDOW (g_editor->getMainFrm ()->getWidget ()), GTK_FILE_CHOOSER_ACTION_SAVE, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_SAVE, GTK_RESPONSE_OK, NULL);
		
		GtkFileFilter* flt;
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "FEditor map files (.map)");
		gtk_file_filter_add_pattern (flt, "*.map");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
		gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (dlg), flt);
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "Other files (*)");
		gtk_file_filter_add_pattern (flt, "*");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
	
		if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_OK)
		{
			char *filename;
		    filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dlg));
			mFileName = filename;
			g_free (filename);
			ofstream file (mFileName, ofstream::out|ofstream::trunc);
			for (entList::const_iterator it = mEntList.begin (); it != mEntList.end (); it++)
			{
				saveEntity (file, *it);
			}
			file.close ();
		}
		gtk_widget_destroy (dlg);
	}
	
	// saves the map as set of world polygons (textured triangles) and entity list (parms)
	// non-world geometry is NOT written
	// file format is text
	void editorMap::exportAsRaw (void) const
	{
		GtkWidget *dlg = gtk_file_chooser_dialog_new ("Export As ...", GTK_WINDOW (g_editor->getMainFrm ()->getWidget ()), GTK_FILE_CHOOSER_ACTION_SAVE, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_SAVE, GTK_RESPONSE_OK, NULL);
		
		GtkFileFilter* flt;
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "fegdk raw files (.raw)");
		gtk_file_filter_add_pattern (flt, "*.raw");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
		gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (dlg), flt);
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "Other files (*)");
		gtk_file_filter_add_pattern (flt, "*");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
	
		if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_OK)
		{
			char *filename;
		    filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dlg));
			mFileName = filename;
			g_free (filename);
			ofstream file (mFileName, ofstream::out|ofstream::trunc);

			entityPtr world = getWorldEntity ();

			// create world brush list
			vector <brushPtr> lst;
			brushList::const_iterator it;

			for (it = getBrushList ().begin (); it != getBrushList ().end (); it++)
			{
				if ((*it)->getOwnerEnt () == world || (*it)->getOwnerEnt ()->getEClass ()->getName () == "func_group")
					lst.push_back (*it);
			}
			for (it = getSelection ().begin (); it != getSelection ().end (); it++)
			{
				if ((*it)->getOwnerEnt () == world || (*it)->getOwnerEnt ()->getEClass ()->getName () == "func_group")
					lst.push_back (*it);
			}

			file << "polygons {" << endl;

			for (vector <brushPtr>::iterator b = lst.begin (); b != lst.end (); b++)
			{
				int sz = (*b)->numFaces ();
				for (int f = 0;  f < sz; f++)
				{
					const brushFace& fc = (*b)->faces (f);
					const brushWinding &w = fc.winding ();
					int np = w.numPoints ();

					file << "\t{ ";
					// mtl
					file << "\"" << fc.texdef ().mtl.c_str () << "\" ";
					// numpoints
					file << w.numPoints () << " ";
					// points in form "x y z u v"
					for (int pt = 0; pt < np; pt++)
					{
						file << w.point (pt).x << " ";
						file << w.point (pt).y << " ";
						file << w.point (pt).z << " ";
						file << w.texCoord (pt).x << " ";
						file << w.texCoord (pt).y << " ";
					}
					file << "}" << endl;
				}
			}

			file << "}" << endl;

			for (entList::const_iterator e = getEntities ().begin (); e != getEntities ().end (); e++)
			{
				if ((*e) != world && (*e)->getEClass ()->getName () != "func_group")
				{
					const entity::ePairList& lst = (*e)->getEPairs ();
					file << "entity {" << endl;
					for (entity::ePairList::const_iterator p = lst.begin (); p != lst.end (); p++)
					{
						file << "\t\"" << (*p).getName () << "\" " << "\"" << (*p).getValue () << "\"" << endl;
					}
					file << "}" << endl;
				}
			}

			file << "}" << endl;

			file.close ();
		}
		gtk_widget_destroy (dlg);
	}
	
	void editorMap::saveSelectionAsPrefab (void) const
	{
		GtkWidget *dlg = gtk_file_chooser_dialog_new ("Save As Prefab ...", GTK_WINDOW (g_editor->getMainFrm ()->getWidget ()), GTK_FILE_CHOOSER_ACTION_SAVE, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_SAVE, GTK_RESPONSE_OK, NULL);
		
		GtkFileFilter* flt;
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "FEditor prefab files (.prefab)");
		gtk_file_filter_add_pattern (flt, "*.prefab");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
		gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (dlg), flt);
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "Other files (*)");
		gtk_file_filter_add_pattern (flt, "*");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
	
		if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_OK)
		{
			char *filename;
		    filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dlg));
			ofstream file (filename, ofstream::out|ofstream::trunc);
			g_free (filename);
			for (entList::const_iterator it = mEntList.begin (); it != mEntList.end (); it++)
			{
				saveEntity (file, *it, true);
			}
			file.close ();
		}
		gtk_widget_destroy (dlg);
	}
	
	template <class T> void editorMap::saveBrush (T &file, smartPtr <brush> b) const
	{
		file << "\t{\n";
	
		for (int ff = 0; ff < b->numFaces (); ff++)
		{
			brushFace &f = b->faces (ff);
	
			cStr s;
			s.printf ("(%0.4f %0.4f %0.4f)", f.planePt (0).x, f.planePt (0).y, f.planePt (0).z);
			file << "\t\t" << s << " ";
			s.printf ("(%0.4f %0.4f %0.4f)", f.planePt (1).x, f.planePt (1).y, f.planePt (1).z);
			file << s << " ";
			s.printf ("(%f %f %f)", f.planePt (2).x, f.planePt (2).y, f.planePt (2).z);
			file << s << " ";
			s = "\"" + f.texdef ().mtl + "\"";
			file << s << " ";
	
			s.printf ("%d %d %d %0.4f %0.4f\n"
					, f.texdef ().shift[0]
					, f.texdef ().shift[1]
					, f.texdef ().rotate
					, f.texdef ().scale[0]
					, f.texdef ().scale[1]
					);
			file << s;
		}
		file << "\t}\n";
	}
	
	template <class T> void		editorMap::saveEntity (T &file, smartPtr <entity> e, bool sel) const
	{
		// check if the entity is selected
		cStr ss;
	
		bool selected;
		if (sel)
		{
			selected = false;
	
			if (e->getEClass ()->isFixedSize ())
			{
				smartPtr <brush> b = g_editor->getEntBrush (e);
				if (b && b->isSelected ())
					selected = true;
			}
			else
			{
				for (brushList::const_iterator it = mSelection.begin (); it != mSelection.end (); it++)
				{
					if ((*it)->getOwnerEnt () == e)
					{
						selected = true;
						break;
					}
				}
			}
		}
		else
		{
			selected = true;
		}
	
		if (selected)
		{
			// write entity
			file << "{\n";
			for (size_t i = 0; i < e->getEPairs ().size (); i++)
			{
				file << "\t\"" << e->getEPairs ()[i].getName () << "\" \"" << e->getEPairs ()[i].getValue () << "\"\n";
			}
			if (!e->getEClass ()->isFixedSize ())
			{
				brushList::const_iterator it;
				if (!sel)
				{
					for (it = getBrushList ().begin (); it != getBrushList ().end (); it++)
					{
						if ((*it)->getOwnerEnt () == e)
							saveBrush (file, *it);
					}
				}
				for (it = getSelection ().begin (); it != getSelection ().end (); it++)
				{
					if ((*it)->getOwnerEnt () == e)
						saveBrush (file, *it);
				}
			}
	
			file << "}\n";
		}
	}
	
	smartPtr <brush> editorMap::loadBrush (fe::charParser &p)
	{
		vector< brushFace > faces;
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
				break;
			else if (!p.cmptoken ("("))
			{
				faces.resize (faces.size () + 1);
				brushFace &f = faces[faces.size () - 1];
				brushFaceTexdef &ft = f.texdef ();
	
				int i;
				for (i = 0; i < 3; i++)
				{
					p.getToken ();
					f.planePt (0)[i] = atof (p.token ());
				}
				p.matchToken (")");
				p.matchToken ("(");
				for (i = 0; i < 3; i++)
				{
					p.getToken ();
					f.planePt (1)[i] = atof (p.token ());
				}
				p.matchToken (")");
				p.matchToken ("(");
				for (i = 0; i < 3; i++)
				{
					p.getToken ();
					f.planePt (2)[i] = atof (p.token ());
				}
				p.matchToken (")");
				p.getToken ();
				ft.mtl = p.token ();
				p.getToken ();
				ft.shift[0] = atoi (p.token ());
				p.getToken ();
				ft.shift[1] = atoi (p.token ());
				p.getToken ();
				ft.rotate = atoi (p.token ());
				p.getToken ();
				ft.scale[0] = atof (p.token ());
				p.getToken ();
				ft.scale[1] = atof (p.token ());
			}
			else
				p.generalSyntaxError ();
		}
		smartPtr <brush> b = new brush (faces);
		// apply materials from texdefs
		for (size_t f = 0; f < b->numFaces (); f++)
		{
			b->faces (f).applyTexdef ();
		}
		return b;
	}
	
	void editorMap::loadEntity (fe::charParser &p, brushList &brushes, entList &ents)
	{
		smartPtr <entity> ent;
		p.matchToken ("{");
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
			{
				// entity done
				break;
			}
			else if (!p.cmptoken ("{"))
			{
				// brush
				smartPtr <brush> b = loadBrush (p);
				ents.push_back (ent);
				brushes.push_back (b);
			}
			else
			{
				// epair
				cStr key = p.token ();
				p.getToken ();
				cStr value = p.token ();
				if (key == "classname")
				{
					if (value == "worldspawn")
					{
						ent = getWorldEntity ();
					}
					else
					{
						ent = new entity ();
					}
				}
				ent->setAttrValue (key, value);
			}
		}
		if (ent->getEClass ()->isFixedSize ())
		{
			// create brush at origin
			smartPtr <brush> b  = new brush (ent->getEClass ()->getMins () + ent->getOrigin (), ent->getEClass ()->getMaxs () + ent->getOrigin ());
			ents.push_back (ent);
			brushes.push_back (b);
		}
	}
	
	void editorMap::mergeFromFile (const char *filename, bool select, bool undoable)
	{
		brushList brushes;
		entList ents;
	
		fe::charParser p (NULL, filename, true);
		g_editor->reset ();
		for (;;)
		{
			loadEntity (p, brushes, ents);
		}
	
		if (undoable)
			g_editor->addCommand (new CmdInsertBrushes (ents, brushes));
		else
		{
			smartPtr <entity> prevEnt = NULL;
			brushList::iterator brush = brushes.begin ();
			entList::iterator ent = ents.begin ();
			for (; brush != brushes.end (); brush++, ent++)
			{
				insertBrush (*brush);
				if (select)
					addSelection (*brush);
				(*brush)->link (*ent);
				if (prevEnt != (*ent))
				{
					prevEnt = (*ent);
					if (strcmp ((*ent)->getEClass ()->getName (), "worldspawn"))
						insertEntity ((*ent));
				}
			}
		}
	}
	
	void editorMap::load (void)
	{
		GtkWidget *dlg = gtk_file_chooser_dialog_new ("Open ...", GTK_WINDOW (g_editor->getMainFrm ()->getWidget ()), GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN, GTK_RESPONSE_OK, NULL);
	
		GtkFileFilter* flt;
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "FEditor map files (.map)");
		gtk_file_filter_add_pattern (flt, "*.map");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
		gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (dlg), flt);
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "Other files (*)");
		gtk_file_filter_add_pattern (flt, "*");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
	
		if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_OK)
		{
			char *filename;
		    filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dlg));
			mFileName = filename;
			g_free (filename);
	
			mergeFromFile (mFileName, false, false);
			g_editor->redrawAllViews ();
		}
		gtk_widget_destroy (dlg);
	}
	
	void editorMap::loadPrefab (void)
	{
		GtkWidget *dlg = gtk_file_chooser_dialog_new ("Open ...", GTK_WINDOW (g_editor->getMainFrm ()->getWidget ()), GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN, GTK_RESPONSE_OK, NULL);
	
		GtkFileFilter* flt;
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "FEditor prefab files (.prefab)");
		gtk_file_filter_add_pattern (flt, "*.prefab");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
		gtk_file_chooser_set_filter (GTK_FILE_CHOOSER (dlg), flt);
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "FEditor map files (.map)");
		gtk_file_filter_add_pattern (flt, "*.map");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
		flt = gtk_file_filter_new ();
		gtk_file_filter_set_name (flt, "Other files (*)");
		gtk_file_filter_add_pattern (flt, "*");
		gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), flt);
	
		if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_OK)
		{
			char *filename;
		    filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dlg));
			mergeFromFile (filename, true, true);
			g_free (filename);
			g_editor->redrawAllViews ();
		}
		gtk_widget_destroy (dlg);
	}
	
	void editorMap::load (const char *s, brushList &brushes, entList &ents)
	{
		fe::charParser p (NULL, s, false, strlen (s));
		for (;;)
		{
			loadEntity (p, brushes, ents);
		}
	}
	
	
	// inserts entity into scene
	void			editorMap::insertEntity (smartPtr <entity> e)
	{
		assert (strcmp (e->getEClass ()->getName (), "worldspawn"));
		mEntList.push_back (e);
	}
		
	// removes entity from scene
	void			editorMap::removeEntity (smartPtr <entity> e)
	{
		entList::iterator it;
		it = find (mEntList.begin (), mEntList.end (), e);
		if (it != mEntList.end ())
		{
			mEntList.erase (it);
		}
	}
	
	bool editorMap::isEmptyEnt (smartPtr <entity> ent) const
	{
		brushList::const_iterator b;
		for (b = mBrushList.begin (); b != mBrushList.end (); b++)
		{
			if ((*b)->getOwnerEnt () == ent)
				return false;
		}
		for (b = mSelection.begin (); b != mSelection.end (); b++)
		{
			if ((*b)->getOwnerEnt () == ent)
				return false;
		}
		return true;
	}
	
}
