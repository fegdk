#ifndef __FEDITOR_H
#define __FEDITOR_H

#include <fegdk/f_math.h>

using fe::ulong;
using fe::ubyte;
using fe::uchar;
using fe::uint;
using fe::ushort;
using fe::handle;
using fe::int16;
using fe::uint16;
using fe::int32;
using fe::uint32;
using fe::int64;
using fe::uint64;
using fe::int_ptr;
using fe::uint_ptr;
using fe::smartPtr;
using fe::cStr;
using fe::wStr;
using fe::ray3;
using fe::vector3;
using fe::float4;
using fe::vector2;
using fe::point;
using fe::rect;
using fe::matrix4;

#endif // __FEDITOR_H
