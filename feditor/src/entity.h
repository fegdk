#ifndef __F_ENTITY_H
#define __F_ENTITY_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <fegdk/f_string.h>
#include <fegdk/f_math.h>
#include <fegdk/f_baseobject.h>
#include <fegdk/f_parser.h>
#include <list>
#include <vector>
#include "feditor.h"

namespace feditor
{

	class brush;
	class editorViewport;
	
	//class CMemFile;	// mfc class, should be replaced
	
	class ePair
	{
	
	private:
	
		cStr		mName;
		cStr		mValue;
	
	public:
	
		ePair (void) {}
		ePair (const char *name, const char *value) { mName = name; mValue = value; }
		ePair (const ePair& epair) { mName = epair.mName; mValue = epair.mValue; }
	
		const char*		getName () const { return mName.c_str (); }
		const char*		getValue () const { return mValue.c_str (); }
		void			setName (const char *name) { mName = name; }
		void			setValue (const char *value) { mValue = value; }
	
		static const char*	stringByName (const std::vector< ePair > &epairs, const char *name)
		{
			std::vector< ePair >::const_iterator it;
			for (it = epairs.begin (); it != epairs.end (); it++)
			{
				if (!strcmp ((*it).getName (), name))
					return (*it).getValue ();
			}
			return NULL;
		}
	
	};
	
	class texDef
	{
	
	private:
	
		char*		mName;
	
	public:
	
	/*	texDef (const texDef &texdef)
		{
			mName = NULL;
			if (&texdef != this)
			{
				setName (texdef.getName ());
			}
		}*/
	
		texDef (const texDef &texdef)
		{
			mName = NULL;
			if (&texdef != this)
			{
				setName (texdef.getName ());
			}
		}
	
		texDef (void)
		{
			mName = new char[1];
			mName[0] = 0;
		}
	
		~texDef (void)
		{
			if (mName)
			{
				delete[] mName;
				mName = NULL;
			}
		}
	
		void setName (const char *name)
		{
			if (mName)
				delete[] mName;
			if (name)
				mName = strcpy (new char[strlen (name)+1], name);
			else
		{
				mName = new char[1];
				mName[0] = 0;
			}
		}
		const char* getName (void) const
		{
			return mName;
		}
	};
	
	class eClass
	{
	private:
		static std::vector< eClass >	mEClass;
		static bool						mbInit;
	public:
		enum { MAX_FLAGS = 8 };
		enum {
			ECLASS_LIGHT		= 0x00000001,
			ECLASS_ANGLE		= 0x00000002,
			ECLASS_PATH			= 0x00000004,
			ECLASS_MISCMODEL	= 0x00000008
		};
	private:
		cStr			mName;
		bool			mbFixedSize;
		vector3	mMins, mMaxs;
		vector3	mColor;
		cStr			mDescription;
		unsigned long	mViewFlags;
	public:
	
		bool operator < (const eClass &a) const
		{
			return mName < a.mName;
		}
	
		enum parmType {string, boolean}; // TODO: add support for model, sound, etc
		struct eParm {
			parmType type;
			cStr name;
		};
	
		typedef std::map <cStr, eParm>	eParmsMap;
		typedef std::map <cStr, int>	eFlagsMap;
		typedef std::vector <eClass> eClassList;
	private:
		eParmsMap	mEParms;
		eFlagsMap	mEFlags;
	
	public:
	
		eClass (void);
		eClass (const eClass &ec);
	
		void load (const char *cname, fe::charParser &p);
		const cStr&		getName (void) const;
		const cStr&		getDescription (void) const;
		bool				isFixedSize (void) const;
		const vector3&	getMins (void) const;
		const vector3&	getMaxs (void) const;
		const vector3&	getColor (void) const;
	
		const eParmsMap& getEParms (void) const;
		const eFlagsMap& getEFlags (void) const;
		
		static const eClass*	getEClassByName (const char *name);
		static const eClassList& getEClass (void);
		static int getEClassIdx (const char *name);
	};
	
	class entity : public fe::baseObject
	{
	
	public:
	
		typedef std::vector <ePair> ePairList;
	
	private:
	
		const eClass*	mpEClass;
		ePairList		mEPairs;
	    
	public:
	
		enum {
			attr_nomovebrush = 1	// don't move brush if origin changed
		};
		
		entity (void);
		~entity (void);
	
		const char*	getAttrValue (const char *key) const;
		void		setAttrValue (const char *key, const char *value, uint32 flags = 0);
		void		eraseAttr (const char *name);
		const ePairList&	getEPairs (void) const;
	
		// some convenience methods
		const eClass*	getEClass (void) const;
		void setOrigin (const vector3 &pos);
		vector3	getOrigin (void) const;
	
	//	void		render (editorViewport *pViewport);
	
	};

	typedef smartPtr <entity> entityPtr;
	
}

#endif // __F_ENTITY_H

