#ifndef __F_TEXDEF_H
#define __F_TEXDEF_H

#include <fegdk/f_baseobject.h>
#include <gtk/gtk.h>

namespace feditor
{

	class editorWnd;
	
	class surfaceInspector : public fe::baseObject
	{
	private:
		fe::smartPtr <editorWnd> mpWindow;
		GtkWidget *mpMtlEntry;
		GtkWidget *mpRotateEntry;
		GtkWidget *mpShiftEntry[2];
		GtkWidget *mpScaleEntry[2];
	
		static void texdefChanged (GtkWidget *widg, surfaceInspector *insp);
	
	public:
		surfaceInspector (void);
		~surfaceInspector (void);
	
		// takes texdef from the 1st encountered selected face
		// which will later be applied to all selected faces;
		// if no faces selected - control widgets should be disabled
		void update (void);
	
		void toggle (void);
		bool isVisible (void) const;
	};
	
}

#endif // __F_TEXDEF_H
