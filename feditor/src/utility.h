#ifndef __F_UTILITY_H
#define __F_UTILITY_H

#include <fegdk/f_math.h>
#include "editorvp.h"
#include "feditor.h"

namespace feditor
{

	inline ray3 rayFromMousePos (point pt, int w, int h, const matrix4 &viewmatrix)
	{
		float nearplane = 0.1f;
		float fov = M_PI / 3.f;
	
		matrix4 invview = viewmatrix.inverse ();
	
		float size = (float) (2.0f * nearplane * tan (fov / 2.0f));
		vector3 dir;
		dir.x = (pt.x - w / 2.f) * size / h;
		dir.y = - (pt.y - h / 2.f) * size / h;
		dir.z = nearplane;
	
		dir = dir * invview;
	
		vector3 origin = (vector3&)invview._41;
		dir -= origin;
		dir.normalize ();
		
		ray3 ray;
		ray.origin () = origin;
		ray.direction () = dir;
		return ray;
	}
	
	// returns true is something remained on negative side of plane
	inline bool clipLineWithPlane (vector3 &v1, vector3 &v2, const fe::plane &pl)
	{
		// find intr
		float d1 = pl.distanceTo (v1);
		float d2 = pl.distanceTo (v2);
	
		if (d1 > 0 && d2 > 0)
			return false;		// completely inside
	
		if (d1 < 0 && d2 < 0)
			return true;		// completely outside
	
		float dist = (v1 - v2).length ();
		float dd1 = fabs (d1);
		float dd2 = fabs (d2);
		float y = dist * dd2 / (dd1 + dd2);
		float x = dist - y;
		x/=dist;
		y/=dist;
	
		// clip
		if (d1 > 0 && d2 < 0)
		{
			v1 = v2 + (v1 - v2) * y;
		}
		else
		{
			v2 = v1 + (v2 - v1) * x;
		}
	
		return true;
	}
	
}

#endif // __F_UTILITY_H

