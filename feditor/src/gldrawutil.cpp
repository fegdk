// $Id$

// opengl implementation of feDrawUtil
// experimental and kinda slow

#include "pch.h"
#include "f_drawutil.h"
#include "f_viewport.h"
#include "f_math.h"
#include "f_editorcore.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include "f_basetexture.h"

struct vertex
{
	feVector3 pos;
	ulong color;
	feVector2 uv;
	
	vertex( const feVector3 &_pos, ulong _color, const feVector2 &_uv )
	{
		pos = _pos;
		color = _color;
		uv = _uv;
	}
};

bool	feDrawUtil::mbAllowTransforms = true;
float	feDrawUtil::mZValue = 0.01f;

void feDrawUtil::allowTransforms( bool onoff )
{
	mbAllowTransforms = onoff;
}

void feDrawUtil::setOrthoTransforms( void )
{
	if ( mbAllowTransforms )
	{
		glMatrixMode (GL_PROJECTION);
		glLoadIdentity ();
		gluOrtho2D (0, feEditorCore::get ()->getViewport ()->getWidth (), 0, feEditorCore::get ()->getViewport ()->getHeight ());
		glMatrixMode (GL_MODELVIEW);
		glLoadIdentity ();
	}
}

void feDrawUtil::free( void )
{
}

void feDrawUtil::drawPic( float x, float y, float w, float h, float s1, float t1, float s2, float t2, float4 color, bool alpha )
{
		return;
	setOrthoTransforms ();

	return;
	
	if (alpha)
	{
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
		glDisable (GL_BLEND);

	return;

	int height = feEditorCore::get()->getViewport()->getHeight();

	glBegin (GL_TRIANGLE_STRIP);
	ulong rgba = color.rgba ();
	glColor4ubv ((const GLubyte *)&rgba);
	glTexCoord2f (s1, t1);
	glVertex3f (x+.5f, height-y+.5f, mZValue);
	glTexCoord2f (s2, t1);
	glVertex3f (x+w+.5f, height-y+.5f, mZValue);
	glTexCoord2f (s1, t2);
	glVertex3f (x+.5f, height-y-h+.5f, mZValue);
	glTexCoord2f (s2, t2);
	glVertex3f (x+w+.5f, height-y-h+.5f, mZValue);
	glEnd ();

	

#if 0	// d3d8 version
	
	// get vb
	feDynamicVB< vertex > *vb = feD3DUtil::getVertexBuffer< vertex >( 4 );
	uint start;
	vertex *vx = vb->lock( 4, start );

	int height = feEngine::get()->viewport()->getHeight();

	vx[0] = vertex( feVector3( x + 0.5f,     height-y+0.5f, mZValue ), color, feVector2( s1, t1 ) );
	vx[1] = vertex( feVector3( x + w + 0.5f, height-y+0.5f,   mZValue ), color, feVector2( s2, t1 ) );
	vx[2] = vertex( feVector3( x + 0.5f,     height-y-h+0.5f,   mZValue ), color, feVector2( s1, t2 ) );
	vx[3] = vertex( feVector3( x + w + 0.5f, height-y-h+0.5f, mZValue ), color, feVector2( s2, t2 ) );

	vb->unlock();

	// render
	IDirect3DDevice8 *dev = feEngine::get()->renderer()->getRendererData()->getDevice();
	dev->SetStreamSource( 0, vb->interfacePtr(), sizeof( vertex ) );
	dev->SetVertexShader( vertex::fvf );

	if ( mbAllowTransforms )
	{
		dev->SetTransform( D3DTS_WORLD, ( D3DXMATRIX * )&feEngine::get()->effectParms()->getMatrix( feEffectParm_WorldMatrix ) );
		dev->SetTransform( D3DTS_VIEW, ( D3DXMATRIX * )&feEngine::get()->effectParms()->getMatrix( feEffectParm_ViewMatrix ) );
		dev->SetTransform( D3DTS_PROJECTION, ( D3DXMATRIX * )&feEngine::get()->effectParms()->getMatrix( feEffectParm_ProjMatrix ));
	}

	dev->SetTexture( 0, NULL );
	dev->SetRenderState( D3DRS_ZBIAS, 0 );
	dev->SetRenderState( D3DRS_ZENABLE, TRUE );
	dev->SetRenderState( D3DRS_ZFUNC, D3DCMP_LESSEQUAL );
	dev->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
	dev->SetRenderState( D3DRS_ALPHABLENDENABLE, alpha ? TRUE : FALSE );
	if ( alpha )
	{
		dev->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );
		dev->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
	}

	dev->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_MODULATE );
	dev->SetTextureStageState( 0, D3DTSS_ALPHAOP, D3DTOP_MODULATE );
	dev->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
	dev->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
	dev->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
	dev->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );

	dev->SetTextureStageState( 1, D3DTSS_COLOROP, D3DTOP_DISABLE );
	dev->SetTextureStageState( 1, D3DTSS_ALPHAOP, D3DTOP_DISABLE );

	feEngine::get()->renderer()->getRendererData()->getDevice()->DrawPrimitive( D3DPT_TRIANGLESTRIP, start, 2 );
#endif
}

void feDrawUtil::drawPic( float x, float y, float w, float h, float s1, float t1, float s2, float t2, float4 color, feMaterial *mtl )
{
	setOrthoTransforms();
#if 0
	// get vb
	feDynamicVB< vertex > *vb = feD3DUtil::getVertexBuffer< vertex >( 4 );
	uint start;
	vertex *vx = vb->lock( 4, start );

	int height = feEngine::get()->viewport()->getHeight();

	vx[0] = vertex( feVector3( x + 0.5f,     height-y, mZValue ), color, feVector2( s1, t1 ) );
	vx[1] = vertex( feVector3( x + w + 0.5f, height-y,   mZValue ), color, feVector2( s2, t1 ) );
	vx[2] = vertex( feVector3( x + 0.5f,     height-y-h,   mZValue ), color, feVector2( s1, t2 ) );
	vx[3] = vertex( feVector3( x + w + 0.5f, height-y-h, mZValue ), color, feVector2( s2, t2 ) );

	vb->unlock();

	// render
	IDirect3DDevice8 *dev = feEngine::get()->renderer()->getRendererData()->getDevice();
	dev->SetStreamSource( 0, vb->interfacePtr(), sizeof( vertex ) );
	dev->SetVertexShader( vertex::fvf );

	feEffect *fx = mtl->getEffect();
	fx->setBestTechnique();
	mtl->setupEffectInputs();
	int npass = fx->begin();
	for ( int p = 0; p < npass; p++ )
	{
		fx->pass( p );
		feEngine::get()->renderer()->getRendererData()->getDevice()->DrawPrimitive( D3DPT_TRIANGLESTRIP, start, 2 );
	}
	fx->end();
#endif
}

void feDrawUtil::drawPic( float x, float y, float w, float h, float s1, float t1, float s2, float t2, float4 color, feEffect *fx )
{
	setOrthoTransforms();
#if 0
	// get vb
	feDynamicVB< vertex > *vb = feD3DUtil::getVertexBuffer< vertex >( 4 );
	uint start;
	vertex *vx = vb->lock( 4, start );

	int height = feEngine::get()->viewport()->getHeight();

	vx[0] = vertex( feVector3( x + 0.5f,     height-y + 0.5f, mZValue ), color, feVector2( s1, t1 ) );
	vx[1] = vertex( feVector3( x + w + 0.5f, height-y + 0.5f,   mZValue ), color, feVector2( s2, t1 ) );
	vx[2] = vertex( feVector3( x + 0.5f,     height-y-h + 0.5f,   mZValue ), color, feVector2( s1, t2 ) );
	vx[3] = vertex( feVector3( x + w + 0.5f, height-y-h + 0.5f, mZValue ), color, feVector2( s2, t2 ) );

	vb->unlock();

	// render
	IDirect3DDevice8 *dev = feEngine::get()->renderer()->getRendererData()->getDevice();
	dev->SetStreamSource( 0, vb->interfacePtr(), sizeof( vertex ) );
	dev->SetVertexShader( vertex::fvf );

	fx->setBestTechnique();
	int npass = fx->begin();
	for ( int p = 0; p < npass; p++ )
	{
		fx->pass( p );
		feEngine::get()->renderer()->getRendererData()->getDevice()->DrawPrimitive( D3DPT_TRIANGLESTRIP, start, 2 );
	}
	fx->end();
#endif
}

void feDrawUtil::drawLine( float x1, float y1, float x2, float y2, float4 color, bool alpha )
{
	setOrthoTransforms();
	glDisable (GL_TEXTURE_2D);
	glBegin (GL_LINES);
	glColor4f (color.r, color.g, color.b, color.a);
	glVertex2f (x1, y1);
	glVertex2f (x2, y2);
	glEnd ();
	glEnable (GL_TEXTURE_2D);
#if 0
	// get vb
	feDynamicVB< vertex > *vb = feD3DUtil::getVertexBuffer< vertex >( 2 );
	uint start;
	vertex *vx = vb->lock( 2, start );

	int height = feEngine::get()->viewport()->getHeight();

	vx[0] = vertex( feVector3( x1 + 0.5f,     height-y1+0.5f, mZValue ), color, feVector2( 0, 0 ) );
	vx[1] = vertex( feVector3( x2 + 0.5f,     height-y2+0.5f, mZValue ), color, feVector2( 1, 1 ) );

	vb->unlock();

	// render
	IDirect3DDevice8 *dev = feEngine::get()->renderer()->getRendererData()->getDevice();
	dev->SetStreamSource( 0, vb->interfacePtr(), sizeof( vertex ) );
	dev->SetVertexShader( vertex::fvf );

	if ( mbAllowTransforms )
	{
		dev->SetTransform( D3DTS_WORLD, ( D3DXMATRIX * )&feEngine::get()->effectParms()->getMatrix( feEffectParm_WorldMatrix ) );
		dev->SetTransform( D3DTS_VIEW, ( D3DXMATRIX * )&feEngine::get()->effectParms()->getMatrix( feEffectParm_ViewMatrix ) );
		dev->SetTransform( D3DTS_PROJECTION, ( D3DXMATRIX * )&feEngine::get()->effectParms()->getMatrix( feEffectParm_ProjMatrix ));
	}

	dev->SetTexture( 0, NULL );
	dev->SetRenderState( D3DRS_ZBIAS, 0 );
	dev->SetRenderState( D3DRS_ZENABLE, TRUE );
	dev->SetRenderState( D3DRS_ZFUNC, D3DCMP_LESSEQUAL );
	dev->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
	dev->SetRenderState( D3DRS_ALPHABLENDENABLE, alpha ? TRUE : FALSE );
	if ( alpha )
	{
		dev->SetRenderState( D3DRS_SRCBLEND, D3DBLEND_SRCALPHA );
		dev->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
	}

	dev->SetTextureStageState( 0, D3DTSS_COLOROP, D3DTOP_MODULATE );
	dev->SetTextureStageState( 0, D3DTSS_ALPHAOP, D3DTOP_MODULATE );
	dev->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
	dev->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
	dev->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
	dev->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE );

	dev->SetTextureStageState( 1, D3DTSS_COLOROP, D3DTOP_DISABLE );
	dev->SetTextureStageState( 1, D3DTSS_ALPHAOP, D3DTOP_DISABLE );

	feEngine::get()->renderer()->getRendererData()->getDevice()->DrawPrimitive( D3DPT_LINELIST, start, 1 );
#endif
}


void feDrawUtil::drawPic( float x, float y, float w, float h, float s1, float t1, float s2, float t2, float4 color, feBaseTexture *tex )
{
	setOrthoTransforms ();
	int height = feEditorCore::get()->getViewport()->getHeight();

	if (tex)
		tex->bind (0);
	else
		glBindTexture (GL_TEXTURE_2D, 0);

	glDisable (GL_CULL_FACE);
	glDisable (GL_DEPTH_TEST);
	glBegin (GL_TRIANGLE_STRIP);
	ulong rgba = color.rgba ();
	glColor4ubv ((const GLubyte *)&rgba);
	glTexCoord2f (s1, t1);
	glVertex3f (x, height-y, mZValue);
	glTexCoord2f (s2, t1);
	glVertex3f (x+w, height-y, mZValue);
	glTexCoord2f (s1, t2);
	glVertex3f (x, height-y-h, mZValue);
	glTexCoord2f (s2, t2);
	glVertex3f (x+w, height-y-h, mZValue);
	glEnd ();
}

void feDrawUtil::fastDrawBegin( void )
{
}

void feDrawUtil::fastDrawEnd( void )
{
}

