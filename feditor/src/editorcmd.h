#ifndef __F_EDITORCMD_H
#define __F_EDITORCMD_H

#include <fegdk/f_math.h>
#include <fegdk/f_string.h>
#include <fegdk/f_helpers.h>
#include <vector>
#include <list>
#include <map>
#include <set>
#include "entity.h"
#include "brush.h"
#include "editortypes.h"

namespace feditor
{

	class editorCore;
	class brush;
	class entity;
	class material;
	class eClass;
	
	// base command class
	class editorCmd
	{
	public:
		virtual	~editorCmd () {}
		virtual cStr		screenName (void) const = 0;
		virtual void		doIt (void) = 0;
		virtual void		undo (void) = 0;
	};
	
	// command related to selection (contains face selection list)
	class selectionCmd : public editorCmd
	{
	protected:
		std::vector< bool >		mFaceSelection;
	
		void	SaveSelectionList (const std::list< smartPtr <brush> > &brushes, std::vector< bool > *selection = NULL);
		void	RestoreSelectionList (std::list< smartPtr <brush> > &brushes, const std::vector< bool > *selection = NULL) const;
		void	SaveSelection (smartPtr <brush> b, std::vector< bool > *selection = NULL);
		void	RestoreSelection (smartPtr <brush> b, const std::vector< bool > *selection = NULL) const;
	};
	
	// doIt: removes selected objects from scene
	// undo: inserts removed objects back and restores selection
	class CmdDeleteSelection : public selectionCmd
	{
	private:
		std::vector< bool >		mFaceSelection;
		std::list< smartPtr <brush> >	mBrushes;
		std::vector< smartPtr <entity> >	mEntities;
	public:
		CmdDeleteSelection (void);
		~CmdDeleteSelection (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	#if 0	// obsolete
	// doIt: resets selection selects single object
	// undo: restores selection that was before 'doIt' call
	class CmdSelectBrush : public editorCmd
	{
	private:
		smartPtr <brush>					mpBrush;
		std::list< smartPtr <brush> >	mBrushes;
	public:
		CmdSelectBrush (smartPtr <brush> obj);
		~CmdSelectBrush (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	#endif
	
	// doIt: adds single object to selection
	// undo: restores selection that was before 'doIt' call
	class CmdSelectBrushAdd : public selectionCmd
	{
	private:
		std::list< smartPtr <brush> >		mBrushes;
	public:
		CmdSelectBrushAdd (smartPtr <brush> obj);
		~CmdSelectBrushAdd (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: removes single object from selection
	// undo: restores selection that was before 'doIt' call
	class CmdSelectBrushRemove : public selectionCmd
	{
	private:
		std::list< smartPtr <brush> >		mBrushes;
	public:
		CmdSelectBrushRemove (smartPtr <brush> obj);
		~CmdSelectBrushRemove (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};

	// doIt: adds/removes objects to/from selection
	// undo: restores selection that was before 'doIt' call
	class CmdSelectOrDeselectBrushes : public selectionCmd
	{
	private:
		brushList		mSelect;
		brushList		mDeselect;
		std::vector< bool > mSaveSelect;
		std::vector< bool > mSaveDeselect;

	public:
		CmdSelectOrDeselectBrushes (const brushList &sel, const brushList &desel);
		~CmdSelectOrDeselectBrushes (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: clears current selection
	// undo: restores selection that was before 'doIt' call
	class CmdSelectionClear : public selectionCmd
	{
	private:
		std::list< smartPtr <brush> >	mBrushes;
		std::vector< bool >		mFaceSelection;
	public:
		CmdSelectionClear (void);
		~CmdSelectionClear (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	#if 0
	// doIt: moves object
	// undo: moves object back
	class CmdMoveBrush : public editorCmd
	{
	private:
		smartPtr <brush>				mpBrush;
		vector3		mDirection;
	public:
		CmdMoveBrush (smartPtr <brush> obj, const vector3 &dir);
		~CmdMoveBrush (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	#endif
	
	// doIt: moves object
	// undo: moves object back
	class CmdMoveSelection : public editorCmd
	{
	private:
		vector3		mDirection;
	public:
		CmdMoveSelection (const vector3 &dir);
		~CmdMoveSelection (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: creates object
	// undo: undoes object creation
	class CmdCreateBrush : public editorCmd
	{
	private:
		smartPtr <brush>		mpBrush;
	//	CmdSelectBrushRemove*	mpDeselectCmd;
	public:
		CmdCreateBrush (smartPtr <brush> obj);
		~CmdCreateBrush (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: rotates specified brush
	// undo: cancels rotation
	class CmdRotateBrush : public editorCmd
	{
	private:
		vector3		mAxis;
		vector3		mOrigin;
		int					mAngle;
		smartPtr <brush>				mpBrush;
	public:
		CmdRotateBrush (smartPtr <brush> b, const vector3 &axis, const vector3 &origin, int angle);
		~CmdRotateBrush (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	
	// doIt: rotates selected objects
	// undo: cancels rotation
	class CmdRotateSelection : public editorCmd
	{
	private:
		vector3		mAxis;
		vector3		mOrigin;
		int					mAngle;
	public:
		CmdRotateSelection (const vector3 &axis, const vector3 &origin, int angle);
		~CmdRotateSelection (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: splits selection with a clipper
	// undo: restores back everything
	class CmdSplitSelection : public selectionCmd
	{
	private:
		vector3		mPoints[3];
		std::list<smartPtr <brush> >	mAdd;
		std::vector<smartPtr <entity> >	mAddOwners;
		std::list<smartPtr <brush> >	mRemove;
		std::vector<smartPtr <entity> >	mRemoveOwners;
		std::vector< bool >		mAddFaceSelection;
	public:
		CmdSplitSelection (const vector3 points[3]);
		~CmdSplitSelection (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: performs drag face operation on selection
	// undo: restores back everything
	class CmdBrushDragFaces : public editorCmd
	{
	
	private:
	
		std::vector< std::vector< int > >		mFaces;
		vector3							mDirection;
	
	public:
	
		CmdBrushDragFaces (const std::vector< std::vector< int > > &faces, const vector3 &dir);
		~CmdBrushDragFaces (void);
	
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	
	};
	
	// doIt: makes brush sided
	// undo: restores back everything
	class CmdMakeSidedBrush : public selectionCmd
	{
	private:
		smartPtr <brush>		mpOldBrush;
		smartPtr <brush>		mpBrush;
		smartPtr <entity>		mpOwner;
	public:
		CmdMakeSidedBrush (smartPtr <brush> b, int numsides);
		~CmdMakeSidedBrush ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: makes brush hollow
	// undo: restores back everything
	class CmdMakeBrushHollow : public selectionCmd
	{
	private:
		std::list<smartPtr <brush> >		mOldBrushes;
		std::list<smartPtr <brush> >		mNewBrushes;
		std::vector<smartPtr <entity> >	mOldOwners;
		std::vector<smartPtr <entity> >	mNewOwners;
	public:
		CmdMakeBrushHollow (void);
		~CmdMakeBrushHollow ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: subtracts current selection from current region
	// undo: restores back everything
	class CmdCSGSubtract : public selectionCmd
	{
	private:
		//                              actions:     doIt         undo            ctor       dtor
		std::list<smartPtr <brush> >		mSrcBrushes;	// remove      insert          addref     release
		std::list<smartPtr <brush> >		mSubBrushes;	// remove      insert/select   addref     release
		std::list<smartPtr <brush> >		mResBrushes;	// insert      remove          create     release
	
		// owners of listed brushes
		std::vector<smartPtr <entity> >	mSrcOwners;
		std::vector<smartPtr <entity> >	mSubOwners;
		std::vector<smartPtr <entity> >	mResOwners;
	public:
		CmdCSGSubtract (void);
		~CmdCSGSubtract ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: creates new entity on a selected set of brushes
	// undo: restores back everything
	class CmdCreateEntity : public selectionCmd
	{
	private:
		//                              actions:     doIt         undo            ctor       dtor
		std::list<smartPtr <brush> >		mBrushes;	 // remove|leave  insert|leave    addref     release
		smartPtr <entity>					mpEntity;
		smartPtr <entity>					mpOldEntity;
		smartPtr <brush>					mpEntBrush;
	
		// owners of listed brushes
	public:
		CmdCreateEntity (const eClass *ec);
		~CmdCreateEntity ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: ungroups all entities from specified list
	// undo: restores back everything
	class CmdUngroupEntity : public selectionCmd
	{
	private:
		std::list<smartPtr <brush> >		mBrushes;
		std::vector<smartPtr <entity> >	mEntities;
	
		// owners of listed brushes
	public:
		CmdUngroupEntity (const std::set<smartPtr <entity> > &ents);
		~CmdUngroupEntity ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: flips selected brushes respective to specified axis
	// undo: restores back everything
	class CmdFlipSelection : public editorCmd
	{
	private:
		vector3			mAxis;
		vector3			mOrigin;
	
		// owners of listed brushes
	public:
		CmdFlipSelection (const vector3& axis);
		~CmdFlipSelection ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: inserts set of brushes into scene
	// undo: restores back everything
	class CmdInsertBrushes : public selectionCmd
	{
	private:
		brushList mSelBrushes;
		brushList mBrushes;
		entList mOwners;
	
		// owners of listed brushes
	public:
		CmdInsertBrushes (const entList &entities, const brushList &brushes);
		~CmdInsertBrushes ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: selects all brushes in the scene
	// undo: restores back everything
	class CmdSelectAll : public selectionCmd
	{
	private:
		std::list<smartPtr <brush> >		mBrushes;
	
		// owners of listed brushes
	public:
		CmdSelectAll (void);
		~CmdSelectAll ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: changes texdefs of specified faces to specified values
	// undo: restores back everything
	class CmdSetSelectionTexdef : public editorCmd
	{
	private:
		brushFaceTexdef		mTargetTexdef;
		std::vector< brushFaceTexdef >		mTexdefs;
		// owners of listed brushes
	public:
		CmdSetSelectionTexdef (const char *mtl, int shift[2], float scale[2], int rotate);
		~CmdSetSelectionTexdef ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: does all face selection stuff (ctrl+shift+mouse1 handler)
	// undo: restores back everything
	class CmdToggleFaceSelection : public editorCmd
	{
	private:
		smartPtr <brush>		mpBrush;
		int			mFace;
	
	public:
		CmdToggleFaceSelection (smartPtr <brush> b, int face);
		~CmdToggleFaceSelection ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: sets specified material to all selected faces
	// undo: restores back everything
	class CmdSetMaterial : public editorCmd
	{
	private:
		std::vector< cStr >	mMaterials;
		cStr	mNewMaterial;
	
	public:
		CmdSetMaterial (const char *mtl);
		~CmdSetMaterial ();
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: selects objects using specified algorithm
	// undo: restores back everything
	class CmdSelectWithBrush : public selectionCmd
	{
	public:
	
		enum selection_type {
			select_complete_tall,	// completely inside of XY-projection of selection brush
			select_partial_tall,	// touching XY-projection of selection brush
			select_inside,			// completely inside of selection brush
			select_touching			// touching selection brush
		};
	
	private:
		std::list< smartPtr <brush> >	mBrushes;
		std::list< smartPtr <brush> >	mSelection;
		smartPtr <brush>					mSelBrush;
		int						mOrthoMode;
	
		bool	IsInside (smartPtr <brush> b, selection_type t);
		bool	IsInside (smartPtr <entity> e, selection_type t);
	
	public:
	
		CmdSelectWithBrush (smartPtr <brush> sel, selection_type t, int ortho_mode = 0);
		~CmdSelectWithBrush (void);
		virtual cStr		screenName (void) const;
		virtual void		doIt (void);
		virtual void		undo (void);
	};
	
	// doIt: sets new attr value
	// undo: restores old value, removes attribute if needed
	class CmdSetEntityAttribute : public editorCmd
	{
	private:
		smartPtr <entity> mpEntity;
		cStr mKey;
		cStr mOldValue;
		bool mbDoKill;
		bool mbUndoKill;
		cStr mNewValue;
	public:
		CmdSetEntityAttribute (smartPtr <entity> ent, const char *key, const char *value);
		virtual cStr screenName (void) const;
		virtual void doIt (void);
		virtual void undo (void);
	};
	
	// doIt: clears all ent attributes except classname
	// undo: restores old values
	class CmdClearAllEntAttributes : public editorCmd
	{
	private:
		smartPtr <entity> mpEntity;
		std::vector <ePair> mAttrValues;
	public:
		CmdClearAllEntAttributes (smartPtr <entity> ent);
		virtual cStr screenName (void) const;
		virtual void doIt (void);
		virtual void undo (void);
	};
	
}

#endif // __F_EDITORCMD_H

