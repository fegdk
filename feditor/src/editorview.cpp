#include "pch.h"
#include <GL/gl.h>
#include <fegdk/f_fontft.h>
#include <fegdk/f_drawutil.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_resourcemgr.h>
#include "editorview.h"
#include "editorcore.h"
#include "editorvp.h"
#include "mainfrm.h"
#include "editormenu.h"

namespace feditor
{

	editorView::editorView (void)
	{
		// mouse events
		g_signal_connect_after (G_OBJECT (mpWidget), "button-press-event", G_CALLBACK (gtkButtonPress), this);
		g_signal_connect_after (G_OBJECT (mpWidget), "button-release-event", G_CALLBACK (gtkButtonRelease), this);
		g_signal_connect_after (G_OBJECT (mpWidget), "motion-notify-event", G_CALLBACK (gtkMotionNotify), this);
		g_signal_connect_after (G_OBJECT (mpWidget), "scroll-event", G_CALLBACK (gtkScroll), this);
	
		mbMovingCamera = false;
		mbAllowContextMenu = false;
	}
	
	editorView::~editorView (void)
	{
	}
	
	void editorView::init (void)
	{
		beginGL ();
		if (!mpFont)
			mpFont = fe::g_engine->getResourceMgr ()->createFontFT ("ter-116n.pcf");
		endGL (false);
	}
	
	void editorView::reshape (void)
	{
		beginGL ();
		glViewport (0, 0, mpWidget->allocation.width, mpWidget->allocation.height);
		endGL (false);
	}
	
	void editorView::draw (void)
	{
		fe::g_engine->setViewport (mpViewport.dynamicCast <fe::baseViewport> ());
		beginGL ();
		
/*		glClearColor (0.9, 0.9, 0.9, 1);
		glClearDepth (1);
		glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);*/
	//	engine->drawUtil ()->setOrthoTransforms ();
	
		glBindTexture (GL_TEXTURE_2D, 0);
	
		// render scene
		g_editor->render ();
		
		glEnable (GL_TEXTURE_2D);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//	mpFont->drawTextString (L"editor view", 0, 0, 0xff00ff00);
		
		endGL ();
	}
	
	gboolean editorView::gtkButtonPress (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
	{
		editorView *edView = (editorView *)user_data;
		g_editor->setViewport (edView->mpViewport);
	//	engine->setViewport (edView->mpViewport.dynamicCast <baseViewport> ());
		switch (event->button)
		{
		case 1:
			break;
		case 2:
			break;
		case 3:
			edView->mbAllowContextMenu = true;
			edView->mbMovingCamera = true;
			break;
		}
		
		// do not pass mouse events to engine input subsystem
		g_editor->mouseEvent (event->type, event->button, event->state, event->x, event->y);
		return TRUE;
	}
	
	gboolean editorView::gtkButtonRelease (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
	{
		editorView *edView = (editorView *)user_data;
		switch (event->button)
		{
		case 1:
			break;
		case 2:
			break;
		case 3:
			edView->mbMovingCamera = false;
			if (edView->mbAllowContextMenu)
			{
				GtkWidget *widg = g_editor->getMainFrm ()->getEditorMenu ()->getWidget ();
				// i dunno why, but we need to pass btn 1 to make menu work properly
				gtk_menu_popup (GTK_MENU (widg), NULL, NULL, 0, 0, 1, event->time);
				return FALSE;
			}
			break;
		}
		
		if (g_editor->getViewport () == edView->mpViewport)
			g_editor->mouseEvent (event->type, event->button, event->state, event->x, event->y);
		return TRUE;
	}
	
	gboolean editorView::gtkMotionNotify (GtkWidget *widget, GdkEventMotion *event, gpointer user_data)
	{
		editorView *edView = (editorView *)user_data;
		edView->mbAllowContextMenu = false;
	
		if (edView->mbMovingCamera)
		{
			if (edView->mpViewport->getViewMode () == editorViewport::ortho)
			{
				int nDim1 = (edView->mpViewport->getOrthoMode () == editorViewport::yz) ? 1 : 0;
				int nDim2 = (edView->mpViewport->getOrthoMode () == editorViewport::xy) ? 1 : 2;
				float scale = edView->mpViewport->getOrthoScale();
				vector3 dir( 0, 0, 0 );
				dir[nDim1] = -(event->x - g_editor->getLastMousePt ().x) / scale;
				dir[nDim2] = (event->y - g_editor->getLastMousePt ().y) / scale;
				edView->mpViewport->move( dir );
				edView->draw();
				point pt;
				pt.x = (int)event->x;
				pt.y = (int)event->y;
				g_editor->setLastMousePt (pt);
			}
		}
		
		// do not pass mouse events to engine input subsystem
		if (g_editor->getViewport () == edView->mpViewport)
			g_editor->mouseEvent (event->type, 0, event->state, event->x, event->y);
		
		return TRUE;
	}
	
	gboolean editorView::gtkScroll (GtkWidget *widget, GdkEventScroll *event, gpointer user_data)
	{
		// do not pass mouse events to engine input subsystem
		editorView *edView = (editorView *)user_data;
		g_editor->setViewport (edView->mpViewport);
		float scl = edView->mpViewport->getOrthoScale ();
		float sign = 0;
		switch (event->direction)
		{
		case GDK_SCROLL_DOWN:
			sign = -1;
			break;
		case GDK_SCROLL_UP:
			sign = 1;
			break;
		}
		
		scl *= 1 + sign * 0.1f;
		if ( scl < 0.001f )
			scl = 0.001f;
		if ( scl > 30 )
			scl = 30.f;
		edView->mpViewport->setOrthoScale (scl);
		edView->draw ();
		return TRUE;
	}
	
}
