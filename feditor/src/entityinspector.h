#ifndef __F_ENTITYINSPECTOR_H
#define __F_ENTITYINSPECTOR_H

#include <fegdk/f_baseobject.h>
#include <gtk/gtk.h>
#include "entity.h"
#include "editorwnd.h"

namespace feditor
{
	
	class entFlagsBox : public fe::baseObject
	{
	private:
		enum {max_flags = 32};
		GtkWidget *mpFlags[max_flags];
		GtkWidget *mpParent;
	public:
		entFlagsBox (GtkWidget *parent);
		~entFlagsBox (void);
		void build (const eClass *eclass);
		void fill (smartPtr <entity> ent);
	};
	
	class entParmWidget : public fe::baseObject
	{
	protected:
		GtkWidget *mpWidget;
	public:
		entParmWidget (void);
		virtual cStr getValue (void) const = 0;
		virtual void setValue (const char *value) = 0;
		GtkWidget *getWidget (void) const;
	};
	
	class entParmString : public entParmWidget
	{
	public:
		entParmString (void);
		virtual cStr getValue (void) const;
		virtual void setValue (const char *value);
	};
	
	class entParmBool : public entParmWidget
	{
	public:
		entParmBool (void);
		virtual cStr getValue (void) const;
		virtual void setValue (const char *value);
	};
	
	
	class entParmsBox : public fe::baseObject
	{
	private:
		typedef std::map <cStr, smartPtr <entParmWidget> > parmsMap;
		parmsMap mParms;
		GtkWidget *mpScroller;	// contains table, represents the parms box widget
		GtkWidget *mpTable;		// contains parms
		GtkWidget *mpParent;	// parent window, must be vbox
	public:
		entParmsBox (GtkWidget *parent);
		~entParmsBox (void);
		void build (const eClass *eclass);
		void fill (smartPtr <entity> ent);
	};
	
	class entityInspector : public fe::baseObject
	{
	private:
		enum {max_flags = 16};
		smartPtr <editorWnd> mpWindow;
	
		smartPtr <entFlagsBox>	mpFlagsBox;
		smartPtr <entParmsBox>	mpParmsBox;
	
		smartPtr <entity> mpEntity;
		GtkTextBuffer *mpDescription;
		GtkWidget *mpKeyEntry;
		GtkWidget *mpValueEntry;
		GtkListStore *mpEClasses;
		GtkWidget *mpEClassesWidget;
		GtkListStore *mpEPairs;
		GtkWidget *mpEPairsWidget;
	
		static void setEClass (GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, entityInspector *insp);
		static void deleteKey (GtkButton *button, entityInspector *insp);
		static void clearAll (GtkButton *button, entityInspector *insp);
		static void setEntityAttr (GtkEntry *entry, entityInspector *insp);
		static void ePairsListCursorChanged (GtkTreeView *list, entityInspector *insp);
	
	public:
		entityInspector (void);
		~entityInspector (void);
		void setEntity (smartPtr <entity> ent);
		void update (void);
		void toggle (void);
		bool isVisible (void) const;
	};
	
}

#endif // __F_ENTITYINSPECTOR_H
