#ifndef __F_EDITORAPP_H
#define __F_EDITORAPP_H

#include <fegdk/f_application.h>
#include "editorcore.h"

namespace feditor
{

	class editorApp : public fe::application
	{
	private:
		smartPtr <editorCore>	mpEditorCore;
		int	mArgc;
		char **mArgv;
	public:
	
		editorApp (int argc, char *argv[]);
		~editorApp (void);
	
		int run (int argc, char *argv[]);
	
		// application methods
		virtual void 	configure (void);
		virtual void 	init (void);
		virtual void 	free (void);
		virtual const char* 	appName (void) const;
		virtual const char * 	dataPath (void) const;
	
		// following methods will not be used
	
	//	virtual void 	keyDown (int keycode);
	//	virtual void 	keyUp (int keycode);
	//	virtual void 	mouseMove (int x, int y);
	//	virtual void 	mouseDown (int keycode);
	//	virtual void 	mouseUp (int keycode);
	//	virtual void 	mouseWheel (int delta);
	//	virtual void 	update (void);
	//	virtual void 	render (void) const;
	
	};
	
}

#endif // __F_EDITORAPP_H

