#ifndef __F_MTLMGR_H
#define __F_MTLMGR_H

#include "glview.h"

namespace fe
{
	class fontFT;
	class material;
}

namespace feditor
{

	class mtlView : public glView
	{
	private:
		smartPtr <fe::fontFT>	mpFont;
		int mPos;
		int mSelection;
	public:
		mtlView (void);
		void init (void);
		void reshape (void);
		void draw (void);
		void setPos (int pos);
		int getPos (void) const;
		void setSelected (int sel);
		int getSelected (void) const;
	};
	
	class mtlBrowser
	{
	private:
		mtlView *mpTexBrowser;
		int mSelection;
		static gboolean click (GtkWidget *widget, GdkEventButton *event, gpointer user_data);
		static void draw_view (GtkWidget *widget, GdkEventExpose *event, gpointer data);
		static void scroll_changed (GtkRange *range, GtkScrollType scroll, gdouble value, gpointer user_data);
		static gboolean scroll (GtkWidget *widget, GdkEventScroll *event, mtlBrowser *browser);
		static void set_texture_from_entry (GtkEntry *entry, mtlBrowser *browser);
	
		GtkVScrollbar *mpScroll;
		GtkWidget *mpTextureEntry;
	
		void init (void);
		int mOldParms[3]; // w, h, mtlCnt
	
	public:
		mtlBrowser (GtkWidget *w);
		~mtlBrowser (void);
		int getSelection (void) const;
	};
	
	class mtlBrowserWnd
	{
	private:
		mtlBrowser *mpBrowser;
		GtkWidget *mpWindow;
	public:
		mtlBrowserWnd (void);
		~mtlBrowserWnd (void);
		void toggle (void);
		bool isVisible (void) const;
		GtkWidget *getWindow (void) const;
		fe::material* getSelectedMtl (void) const;
		cStr getSelectedMtlName (void) const;
	};
	
}

#endif

