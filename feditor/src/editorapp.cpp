#include "pch.h"
#include <fegdk/f_engine.h>
#include <fegdk/f_console.h>
#include <fegdk/f_baserenderer.h>
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <gtk/gtkgl.h>
#include <fegdk/f_error.h>
#include <GL/gl.h>
#include "editorapp.h"
#include "editorcore.h"
#include <fegdk/cvars.h>
#include <fegdk/f_filesystem.h>

namespace feditor
{

	editorApp::editorApp (int argc, char *argv[])
	{
		mArgc = argc;
		mArgv = argv;
		gtk_init (&mArgc, &mArgv);
		gtk_gl_init (&mArgc, &mArgv);
	}
	
	editorApp::~editorApp (void)
	{
	}
	
	int				editorApp::run (int argc, char *argv[])
	{
		// create main window
		fprintf (stderr, "running gtk mainloop\n");
		gtk_main();	
	
		return 0;
	}
	
	void
	editorApp::configure (void)
	{
		// tell engine (through cvars) to:
		// 1. disable internal mainloop
		// 2. to use external renderer initialization
		// 3. to use internal gl renderer
		fe::cvarManager *cvars = fe::g_engine->getCVarManager ();
		cvars->set ("sys_int_mainloop", "0", true);
		cvars->set ("sys_profile", "external_opengl", true);
		cvars->set ("in_keyb_autorepeat", "1", true);
	
		// create widgets so that we have valid opengl context[s]
		fprintf (stderr, "creating editorcore obj\n");
		mpEditorCore = new editorCore ();
		
		// default config
		mpEditorCore->loadConfig (PKGDATADIR"/config");
		// user config
		mpEditorCore->loadConfig (getenv ("HOME") + cStr ("/.feditor/config"));

		// add fs directories specified in configs
		fe::g_engine->getFileSystem ()->init (g_editor->getValueForKey ("editor_includedirs"));
	}
	
	void
	editorApp::init (void)
	{
	}
	
	void
	editorApp::free (void)
	{
		// free core stuff
		mpEditorCore = NULL;
	}
	
	const char*
	editorApp::appName (void) const
	{
		return PACKAGE " " VERSION;
	}
	
	const char* 	editorApp::dataPath (void) const
	{
		return PKGDATADIR;
	}
	
}
