#include "pch.h"
#include <fegdk/f_helpers.h>
#include <fegdk/f_material.h>
#include <fegdk/f_engine.h>
#include "editorcmd.h"
#include "editorcore.h"
#include "brush.h"
#include "entity.h"
#include "editorvp.h"
#include "mainfrm.h"
#include "mtlbrowser.h"
#include "entityinspector.h"
#include "editormap.h"
#include <set>

namespace feditor
{

	//---------------------------------------------
	// selectionCmd
	//---------------------------------------------
	void	selectionCmd::SaveSelectionList (const std::list< smartPtr <brush>  > &brushes, std::vector< bool > *selection)
	{
		std::vector< bool > &sel = selection ? *selection : mFaceSelection;
		for (brushList::const_iterator i = brushes.begin (); i != brushes.end (); i++)
		{
			smartPtr <brush> b = *i;
			for (int j = 0; j < b->numFaces (); j++)
			{
				sel.push_back (b->isFaceSelected (j));
			}
		}
	}
	
	void	selectionCmd::RestoreSelectionList (std::list< smartPtr <brush>  > &brushes, const std::vector< bool > *selection) const
	{
		const std::vector< bool > &sel = selection ? *selection : mFaceSelection;
		int f = 0;
		for (brushList::iterator i = brushes.begin (); i != brushes.end (); i++)
		{
			smartPtr <brush> b = *i;
			bool bWasSelected = b->isSelected ();
			int numselfaces = 0;
			for (int j = 0; j < b->numFaces (); j++)
			{
				b->selectFace (j, sel[f]);
				if (sel[f])
					numselfaces++;
				f++;
			}
			if (bWasSelected && !numselfaces)
				g_editor->getMap ()->removeSelection (b);
			else if (!bWasSelected && numselfaces)
				g_editor->getMap ()->addSelection (b);
		}
	}
	
	void	selectionCmd::SaveSelection (const smartPtr <brush> b, std::vector< bool > *selection)
	{
		std::vector< bool > &sel = selection ? *selection : mFaceSelection;
		for (int j = 0; j < b->numFaces (); j++)
		{
			sel.push_back (b->isFaceSelected (j));
		}
	}
	
	void	selectionCmd::RestoreSelection (smartPtr <brush> b, const std::vector< bool > *selection) const
	{
		const std::vector< bool > &sel = selection ? *selection : mFaceSelection;
		g_editor->getMap ()->addSelection (b);
		for (int j = 0; j < b->numFaces (); j++)
		{
			b->selectFace (j, sel[j]);
		}
	}
	
	//---------------------------------------------
	// CmdDeleteSelection
	//---------------------------------------------
	
	CmdDeleteSelection::CmdDeleteSelection (void)
	{
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			mBrushes.push_back ((*i));
			mEntities.push_back ((*i)->getOwnerEnt ());
		}
	
		SaveSelectionList (mBrushes);
	}
	
	CmdDeleteSelection::~CmdDeleteSelection (void)
	{
	}
	
	cStr		CmdDeleteSelection::screenName (void) const
	{
		return "Delete selection";
	}
	
	void		CmdDeleteSelection::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
		int ee = 0;
		for (brushList::iterator i = mBrushes.begin (); i != mBrushes.end (); i++, ee++)
		{
			smartPtr <entity> e = mEntities[ee];
			(*i)->unlink ();
			g_editor->getMap ()->removeBrush (*i);
			if (e != g_editor->getMap ()->getWorldEntity ()
				&& g_editor->getMap ()->isEmptyEnt (e))
			{
				g_editor->getMap ()->removeEntity (e);
			}
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdDeleteSelection::undo (void)
	{
		int ee = 0;
		for (brushList::iterator i = mBrushes.begin (); i != mBrushes.end (); i++, ee++)
		{
			smartPtr <entity> e = mEntities[ee];
			if (e != g_editor->getMap ()->getWorldEntity ()
				&& g_editor->getMap ()->isEmptyEnt (e))
			{
				g_editor->getMap ()->insertEntity (e);
			}
			g_editor->getMap ()->insertBrush (*i);
			(*i)->link (e);
		}
		RestoreSelectionList (mBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdSelectBrushAdd
	//---------------------------------------------
	CmdSelectBrushAdd::CmdSelectBrushAdd (smartPtr <brush> b)
	{
		assert (b);
		assert (false == b->isSelected ());
	
		if (b->getOwnerEnt () == g_editor->getMap ()->getWorldEntity ())
		{
			mBrushes.push_back (b);
		}
		else
		{
			smartPtr <entity> e = b->getOwnerEnt ();
	
			// add all brushes linked to this entity
			for (brushList::iterator i = g_editor->getMap ()->getBrushList ().begin (); i != g_editor->getMap ()->getBrushList ().end (); ++i)
			{
				if ((*i)->getOwnerEnt () == e)
					mBrushes.push_back (*i);
			}
		}
	
		SaveSelectionList (mBrushes);
	}
	
	CmdSelectBrushAdd::~CmdSelectBrushAdd (void)
	{
	}
	
	cStr		CmdSelectBrushAdd::screenName (void) const
	{
		return "Select brush";
	}
	
	void		CmdSelectBrushAdd::doIt (void)
	{
		for (brushList::iterator i = mBrushes.begin (); i != mBrushes.end (); i++)
		{
			g_editor->getMap ()->addSelection (*i);
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdSelectBrushAdd::undo (void)
	{
		RestoreSelectionList (mBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdSelectBrushRemove
	//---------------------------------------------
	CmdSelectBrushRemove::CmdSelectBrushRemove (smartPtr <brush> b)
	{
		assert (b);
		assert (true == b->isSelected ());
		if (b->getOwnerEnt () == g_editor->getMap ()->getWorldEntity ())
		{
			mBrushes.push_back (b);
		}
		else
		{
			smartPtr <entity> e = b->getOwnerEnt ();
			for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); ++i)
			{
				if ((*i)->getOwnerEnt () == e)
					mBrushes.push_back (*i);
			}
		}
	
		SaveSelectionList (mBrushes);
	}
	
	CmdSelectBrushRemove::~CmdSelectBrushRemove (void)
	{
	}
	
	cStr		CmdSelectBrushRemove::screenName (void) const
	{
		return "Deselect brush";
	}
	
	void		CmdSelectBrushRemove::doIt (void)
	{
		for (brushList::iterator i = mBrushes.begin (); i != mBrushes.end (); i++)
		{
			g_editor->getMap ()->removeSelection (*i);
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdSelectBrushRemove::undo (void)
	{
		RestoreSelectionList (mBrushes);
		g_editor->redrawAllViews ();
	}
	
	CmdSelectOrDeselectBrushes::CmdSelectOrDeselectBrushes (const brushList &sel, const brushList &desel)
	{
		mSelect = sel;
		mDeselect = desel;
		SaveSelectionList (mSelect, &mSaveSelect);
		SaveSelectionList (mDeselect, &mSaveDeselect);
	}

	CmdSelectOrDeselectBrushes::~CmdSelectOrDeselectBrushes (void)
	{
	}

	cStr		CmdSelectOrDeselectBrushes::screenName (void) const
	{
		return "Select/deselect brushes";
	}

	void		CmdSelectOrDeselectBrushes::doIt (void)
	{
		for (brushList::iterator i = mSelect.begin (); i != mSelect.end (); i++)
		{
			g_editor->getMap ()->addSelection (*i);
		}
		for (brushList::iterator i = mDeselect.begin (); i != mDeselect.end (); i++)
		{
			g_editor->getMap ()->removeSelection (*i);
		}
		g_editor->redrawAllViews ();
	}

	void		CmdSelectOrDeselectBrushes::undo (void)
	{
		RestoreSelectionList (mSelect, &mSaveSelect);
		RestoreSelectionList (mDeselect, &mSaveDeselect);
		g_editor->redrawAllViews ();
	}

	//---------------------------------------------
	// CmdSelectionClear
	//---------------------------------------------
	CmdSelectionClear::CmdSelectionClear (void)
	{
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> obj = *i;
			mBrushes.push_back (obj);
		}
	
		SaveSelectionList (mBrushes);
	}
	
	CmdSelectionClear::~CmdSelectionClear (void)
	{
	}
	
	cStr		CmdSelectionClear::screenName (void) const
	{
		return "Clear selection";
	}
	
	void		CmdSelectionClear::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
		g_editor->redrawAllViews ();
	}
	
	void		CmdSelectionClear::undo (void)
	{
		RestoreSelectionList (mBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdMoveSelection
	//---------------------------------------------
	
	CmdMoveSelection::CmdMoveSelection (const vector3 &dir)
	{
		mDirection = dir;
	}
	
	CmdMoveSelection::~CmdMoveSelection (void)
	{
	}
	
	cStr		CmdMoveSelection::screenName (void) const
	{
		return "move selection";
	}
	
	void		CmdMoveSelection::doIt (void)
	{
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> obj = *i;
			obj->move (mDirection);
			if (obj->getOwnerEnt ()->getEClass ()->isFixedSize ())
			{
	            obj->getOwnerEnt ()->setOrigin (obj->getOwnerEnt ()->getOrigin () + mDirection);
				g_editor->getMainFrm ()->getEntityInspector ()->update ();
			}
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdMoveSelection::undo (void)
	{
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> obj = *i;
			obj->move (-mDirection);
			if (obj->getOwnerEnt ()->getEClass ()->isFixedSize ())
			{
	            obj->getOwnerEnt ()->setOrigin (obj->getOwnerEnt ()->getOrigin () - mDirection);
				g_editor->getMainFrm ()->getEntityInspector ()->update ();
			}
		}
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdCreateBrush
	//---------------------------------------------
	
	CmdCreateBrush::CmdCreateBrush (smartPtr <brush> obj)
	{
		assert (obj);
		mpBrush = obj;
	}
	
	CmdCreateBrush::~CmdCreateBrush (void)
	{
	}
	
	cStr		CmdCreateBrush::screenName (void) const
	{
		return "Create object";
	}
	
	void		CmdCreateBrush::doIt (void)
	{
		g_editor->getMap ()->insertBrush (mpBrush);
		mpBrush->link (g_editor->getMap ()->getWorldEntity ());
		g_editor->getMap ()->addSelection (mpBrush);
		g_editor->redrawAllViews ();
	}
	
	void		CmdCreateBrush::undo (void)
	{
		mpBrush->unlink ();
		g_editor->getMap ()->removeBrush (mpBrush);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdRotateBrush
	//---------------------------------------------
	CmdRotateBrush::CmdRotateBrush (smartPtr <brush> brush, const vector3 &axis, const vector3 &origin, int angle)
	{
		mpBrush = brush;
		mAxis = axis;
		mOrigin = origin;
		mAngle = angle;
	}
	
	CmdRotateBrush::~CmdRotateBrush (void)
	{
	}
	
	cStr		CmdRotateBrush::screenName (void) const
	{
		return "rotate brush";
	}
	
	void		CmdRotateBrush::doIt (void)
	{
		mpBrush->rotate (mAxis, mOrigin, mAngle * 3.14f / 180.f);
		g_editor->redrawAllViews ();
	}
	
	void		CmdRotateBrush::undo (void)
	{
		mpBrush->rotate (mAxis, mOrigin, -mAngle * 3.14f / 180.f);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdRotateSelection
	//---------------------------------------------
	
	CmdRotateSelection::CmdRotateSelection (const vector3 &axis, const vector3 &origin, int angle)
	{
		mAxis = axis;
		mOrigin = origin;
		mAngle = angle;
	}
	
	CmdRotateSelection::~CmdRotateSelection (void)
	{
	}
	
	cStr		CmdRotateSelection::screenName (void) const
	{
		return "rotate selection";
	}
	
	void		CmdRotateSelection::doIt (void)
	{
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
			 (*i)->rotate (mAxis, mOrigin, mAngle * 3.14f / 180.f);
		g_editor->redrawAllViews ();
	}
	
	void		CmdRotateSelection::undo (void)
	{
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
			 (*i)->rotate (mAxis, mOrigin, -mAngle * 3.14f / 180.f);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdRotateSelection
	//---------------------------------------------
	
	CmdSplitSelection::CmdSplitSelection (const vector3 points[3])
	{
		smartPtr <fe::material> mtl = g_editor->getMainFrm ()->getMtlBrowser ()->getSelectedMtl ();
		memcpy (mPoints, points, 3 * sizeof (vector3));
	
		brushFace f;
		f.planePt (0) = mPoints[0];
		f.planePt (1) = mPoints[1];
		f.planePt (2) = mPoints[2];
		f.setMtl (mtl ? mtl->name () : "");
	
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> o = (*i);
			smartPtr <entity> e = o->getOwnerEnt ();
			assert (e);
			if (!e)
				continue;
			
			if (e->getEClass ()->isFixedSize ())
				continue;
			size_t first = mAdd.size ();
			o->split (f, mAdd);
			for (size_t i = first; i < mAdd.size (); i++)
			{
				mAddOwners.push_back (o->getOwnerEnt ());
			}
			mRemove.push_back (o);
			mRemoveOwners.push_back (o->getOwnerEnt ());
		}
	
		SaveSelectionList (mRemove);
		SaveSelectionList (mAdd, &mAddFaceSelection);
	}
	
	CmdSplitSelection::~CmdSplitSelection (void)
	{
	}
	
	cStr		CmdSplitSelection::screenName (void) const
	{
		return "split selection";
	}
	
	void		CmdSplitSelection::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
	
		brushList::iterator i;
		int e;
		for (e = 0, i = mRemove.begin (); i != mRemove.end (); i++, e++)
		{
			(*i)->unlink ();
			g_editor->getMap ()->removeBrush (*i);
		}
	
		for (e = 0, i = mAdd.begin (); i != mAdd.end (); i++, e++)
		{
			(*i)->link (mAddOwners[e]);
			g_editor->getMap ()->insertBrush (*i);
		}
		
		RestoreSelectionList (mAdd, &mAddFaceSelection);
		g_editor->redrawAllViews ();
	}
	
	void		CmdSplitSelection::undo (void)
	{
		brushList::iterator i;
		int e;
	
		g_editor->getMap ()->clearSelection ();
		for (e = 0, i = mAdd.begin (); i != mAdd.end (); i++, e++)
		{
			(*i)->unlink ();
	//		mAddOwners[e]->unlinkBrush (*i);
			g_editor->getMap ()->removeBrush (*i);
		}
	
		for (e = 0, i = mRemove.begin (); i != mRemove.end (); i++, e++)
		{
			(*i)->link (mRemoveOwners[e]);
	//		mRemoveOwners[e]->linkBrush (*i);
			g_editor->getMap ()->insertBrush (*i);
		}
		RestoreSelectionList (mRemove);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdBrushDragFaces
	//---------------------------------------------
	CmdBrushDragFaces::CmdBrushDragFaces (const std::vector< std::vector< int > > &faces, const vector3 &dir)
	{
		mFaces = faces;
		mDirection = dir;
	}
	
	CmdBrushDragFaces::~CmdBrushDragFaces (void)
	{
	}
	
	cStr		CmdBrushDragFaces::screenName (void) const
	{
		return "Drag faces";
	}
	
	void		CmdBrushDragFaces::doIt (void)
	{
		int fi = 0;
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++, fi++)
		{
			smartPtr <brush> b = (*i);
			for (size_t f = 0; f < mFaces[fi].size (); f++)
			{
				b->faces ( (int)mFaces[fi][f]).planePt (0) += mDirection;
				b->faces ( (int)mFaces[fi][f]).planePt (1) += mDirection;
				b->faces ( (int)mFaces[fi][f]).planePt (2) += mDirection;
			}
			b->build ();
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdBrushDragFaces::undo (void)
	{
		int fi = 0;
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++, fi++)
		{
			smartPtr <brush> b = (*i);
			for (size_t f = 0; f < mFaces[fi].size (); f++)
			{
				b->faces ( (int)mFaces[fi][f]).planePt (0) -= mDirection;
				b->faces ( (int)mFaces[fi][f]).planePt (1) -= mDirection;
				b->faces ( (int)mFaces[fi][f]).planePt (2) -= mDirection;
			}
			b->build ();
		}
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdMakeSidedBrush
	//---------------------------------------------
	CmdMakeSidedBrush::CmdMakeSidedBrush (smartPtr <brush> b, int numsides)
	{
		assert (b);
		mpOldBrush = b;
		mpOwner = mpOldBrush->getOwnerEnt ();
		// material is taken from current mtlbrowser selectection
		mpBrush = new brush (*b);
		mpBrush->makeSided (numsides);
	
		cStr mtl = g_editor->getMainFrm ()->getMtlBrowser ()->getSelectedMtlName ();	
		for (int f = 0; f < mpBrush->numFaces (); f++)
		{
			mpBrush->faces (f).setMtl (mtl);
		}
	
		SaveSelection (mpOldBrush);
	}
	
	CmdMakeSidedBrush::~CmdMakeSidedBrush ()
	{
	}
	
	cStr		CmdMakeSidedBrush::screenName (void) const
	{
		return "Make sided brush";
	}
	
	void		CmdMakeSidedBrush::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
		mpOldBrush->unlink ();
		g_editor->getMap ()->removeBrush (mpOldBrush);
		mpBrush->link (mpOwner);
		g_editor->getMap ()->insertBrush (mpBrush);
		g_editor->getMap ()->addSelection (mpBrush);
		g_editor->redrawAllViews ();
	}
	
	void		CmdMakeSidedBrush::undo (void)
	{
		g_editor->getMap ()->clearSelection ();
		mpBrush->unlink ();
		g_editor->getMap ()->removeBrush (mpBrush);
		mpOldBrush->link (mpOwner);
		g_editor->getMap ()->insertBrush (mpOldBrush);
		RestoreSelection (mpOldBrush);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdMakeBrushHollow
	//---------------------------------------------
	CmdMakeBrushHollow::CmdMakeBrushHollow (void)
	{
		brushList::iterator i;
	
		mOldOwners.reserve (g_editor->getMap ()->getSelection ().size ());
		for (i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			mOldBrushes.push_back (*i);
			mOldOwners.push_back ( (*i)->getOwnerEnt ());
		}
	
		int e = 0;
		for (i = mOldBrushes.begin (); i != mOldBrushes.end (); i++, e++)
		{
			if (mOldOwners[e]->getEClass ()->isFixedSize ())
				continue;
	
			smartPtr <brush> b = *i;
	
			for (int f = 0; f < b->numFaces (); f++)
			{
				brushFace split = b->faces (f);
				vector3 move = split.plane ().normal () * 8;
				for (int k = 0; k < 3; k++)
					split.planePt (k) -= move;
				smartPtr <brush> front, back;
				b->splitByFace (split, front, back);
				if (front)
				{
					smartPtr <brush> newb = new brush (*front);
					mNewBrushes.push_back (newb);
					mNewOwners.push_back (newb->getOwnerEnt ());
				}
			}
		}
	
		SaveSelectionList (mOldBrushes);
	}
	
	CmdMakeBrushHollow::~CmdMakeBrushHollow ()
	{
	}
	
	cStr		CmdMakeBrushHollow::screenName (void) const
	{
		return "Make brush hollow";
	}
	
	void		CmdMakeBrushHollow::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
	
		brushList::iterator i;
		int k;
	
		for (k = 0, i = mNewBrushes.begin (); i != mNewBrushes.end (); i++, k++)
		{
			(*i)->link (mNewOwners[k]);
			g_editor->getMap ()->insertBrush (*i);
			g_editor->getMap ()->addSelection (*i);
		}
		for (k = 0, i = mOldBrushes.begin (); i != mOldBrushes.end (); i++, k++)
		{
			(*i)->unlink ();
			g_editor->getMap ()->removeBrush (*i);
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdMakeBrushHollow::undo (void)
	{
		g_editor->getMap ()->clearSelection ();
	
		brushList::iterator i;
		int k;
	
		for (k = 0, i = mNewBrushes.begin (); i != mNewBrushes.end (); k++, i++)
		{
			(*i)->unlink ();
			g_editor->getMap ()->removeBrush (*i);
		}
		for (k = 0, i = mOldBrushes.begin (); i != mOldBrushes.end (); k++, i++)
		{
			(*i)->link (mOldOwners[k]);
			g_editor->getMap ()->insertBrush (*i);
		}
		RestoreSelectionList (mOldBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdCSGSubtract
	//---------------------------------------------
	CmdCSGSubtract::CmdCSGSubtract (void)
	{
		// remember everything
		brushList::iterator i;
	    
		for (i = g_editor->getMap ()->getBrushList ().begin (); i != g_editor->getMap ()->getBrushList ().end (); i++)
		{
			smartPtr <brush> b = *i;
			// TODO: check for intersection w/ selection
			mSrcBrushes.push_back (b);
			mSrcOwners.push_back (b->getOwnerEnt ());
		}
	
		for (i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = *i;
			mSubBrushes.push_back (b);
			mSubOwners.push_back (b->getOwnerEnt ());
		}
	
		// subtract
		for (brushList::iterator i = mSubBrushes.begin (); i != mSubBrushes.end (); i++)
		{
			for (brushList::iterator j = mSrcBrushes.begin (); j != mSrcBrushes.end (); j++)
			{
				smartPtr <brush> b = *i;
				std::vector< smartPtr <brush>  > res;
				b->csgSubtract (res, *j);
				for (size_t k = 0; k < res.size (); k++)
				{
					smartPtr <brush> bb = new brush (*res[k]);
					mResBrushes.push_back (bb);
					mResOwners.push_back ( (*j)->getOwnerEnt ());
				}
			}
		}
	
		SaveSelectionList (mSubBrushes);
	}
	
	CmdCSGSubtract::~CmdCSGSubtract ()
	{
	}
	
	cStr		CmdCSGSubtract::screenName (void) const
	{
		return "CSG Subtract";
	}
	
	void		CmdCSGSubtract::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
	
		brushList::iterator i;
		int k;
	
		for (k = 0, i = mSrcBrushes.begin (); i != mSrcBrushes.end (); i++, k++)
		{
			(*i)->unlink ();
			g_editor->getMap ()->removeBrush (*i);
		}
		for (k = 0, i = mSubBrushes.begin (); i != mSubBrushes.end (); i++, k++)
		{
			(*i)->unlink ();
			g_editor->getMap ()->removeBrush (*i);
		}
		for (k = 0, i = mResBrushes.begin (); i != mResBrushes.end (); i++, k++)
		{
			(*i)->link (mResOwners[k]);
			g_editor->getMap ()->insertBrush (*i);
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdCSGSubtract::undo (void)
	{
		g_editor->getMap ()->clearSelection ();
	
		brushList::iterator i;
		int k;
	
		for (k = 0, i = mSrcBrushes.begin (); i != mSrcBrushes.end (); i++, k++)
		{
			(*i)->link (mSrcOwners[k]);
			g_editor->getMap ()->insertBrush (*i);
		}
		for (k = 0, i = mSubBrushes.begin (); i != mSubBrushes.end (); i++, k++)
		{
			(*i)->link (mSubOwners[k]);
			g_editor->getMap ()->insertBrush (*i);
		}
		for (k = 0, i = mResBrushes.begin (); i != mResBrushes.end (); i++, k++)
		{
			(*i)->unlink ();
			g_editor->getMap ()->removeBrush (*i);
		}
		RestoreSelectionList (mSubBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdCreateEntity
	//---------------------------------------------
	CmdCreateEntity::CmdCreateEntity (const eClass *ec)
	{
		mpEntBrush = NULL;
		mpOldEntity = NULL;
	
		// remember everything
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = (*i);
			if (!mpOldEntity)
				mpOldEntity = b->getOwnerEnt ();
			mBrushes.push_back (b);
		}
	
		// create new
		if (ec->getName () == "worldspawn")
		{
			mpEntity = g_editor->getMap ()->getWorldEntity ();
		}
		else
		{
			mpEntity = new entity ();
			mpEntity->setAttrValue ("classname", ec->getName ());
			if (ec->isFixedSize ())
			{
				vector3 origin;
				mpEntBrush  = new brush (ec->getMins (), ec->getMaxs ());
				mpEntity->setOrigin (vector3 (0, 0, 0));
			}
		}
	
		SaveSelectionList (mBrushes);
	}
	
	CmdCreateEntity::~CmdCreateEntity ()
	{
	}
	
	cStr		CmdCreateEntity::screenName (void) const
	{
		return "Create entity";
	}
	
	void		CmdCreateEntity::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
		g_editor->getMap ()->insertEntity (mpEntity);
		if (!mpEntity->getEClass ()->isFixedSize ())
		{
			for (brushList::iterator i = mBrushes.begin (); i != mBrushes.end (); i++)
			{
				 (*i)->link (mpEntity);
				 g_editor->getMap ()->addSelection (*i);
			}
		}
		else
		{
			for (brushList::iterator i = mBrushes.begin (); i != mBrushes.end (); i++)
			{
				 (*i)->unlink ();
				g_editor->getMap ()->removeBrush ( (*i));
			}
			mpEntBrush->link (mpEntity);
			g_editor->getMap ()->insertBrush (mpEntBrush);
			g_editor->getMap ()->addSelection (mpEntBrush);
		}
	
		// any entity except worldspawn should be removed now
		g_editor->getMap ()->removeEntity (mpOldEntity);
		g_editor->redrawAllViews ();
	}
	
	void		CmdCreateEntity::undo (void)
	{
		g_editor->getMap ()->clearSelection ();
	
		if (mpEntBrush)
		{
			mpEntBrush->unlink ();
			g_editor->getMap ()->removeBrush (mpEntBrush);
		}
			
		for (brushList::iterator i = mBrushes.begin (); i != mBrushes.end (); i++)
		{
			if (mpEntity->getEClass ()->isFixedSize ())	// old brushes were removed
				g_editor->getMap ()->insertBrush ( (*i));
			// link back to old entity
			(*i)->link (mpOldEntity);
		}
	
		g_editor->getMap ()->removeEntity (mpEntity);
		RestoreSelectionList (mBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdUngroupEntity
	//---------------------------------------------
	CmdUngroupEntity::CmdUngroupEntity (const std::set <smartPtr <entity> > &ents)
	{
		// remember brushes
		std::set <smartPtr <entity> >::const_iterator it;
		for (it = ents.begin (); it != ents.end (); it++)
		{
			smartPtr <entity> e = (*it);
			brushList::iterator bb;
			for (bb = g_editor->getMap ()->getBrushList ().begin (); bb != g_editor->getMap ()->getBrushList ().end (); ++bb)
			{
				if ((*bb)->getOwnerEnt () == e)
				{
					mBrushes.push_back (*bb);
					mEntities.push_back (e);
				}
			}
			for (bb = g_editor->getMap ()->getSelection ().begin (); bb != g_editor->getMap ()->getSelection ().end (); ++bb)
			{
				if ((*bb)->getOwnerEnt () == e)
				{
					mBrushes.push_back (*bb);
					mEntities.push_back (e);
				}
			}
		}
		
		SaveSelectionList (mBrushes);
	}
	
	CmdUngroupEntity::~CmdUngroupEntity ()
	{
	}
	
	cStr		CmdUngroupEntity::screenName (void) const
	{
		return "Ungroup entity";
	}
	
	void		CmdUngroupEntity::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
		int ee = 0;
		for (brushList::iterator bb = mBrushes.begin (); bb != mBrushes.end (); bb++, ee++)
		{
			smartPtr <entity> e = mEntities[ee];
			 (*bb)->link (g_editor->getMap ()->getWorldEntity ());
			g_editor->getMap ()->removeEntity (e);
		}
	
		g_editor->redrawAllViews ();
	}
	
	void		CmdUngroupEntity::undo (void)
	{
		g_editor->getMap ()->clearSelection ();
		int ee = 0;
		for (brushList::iterator bb = mBrushes.begin (); bb != mBrushes.end (); bb++, ee++)
		{
			 // dangerous, potential multiple addition of entity
			g_editor->getMap ()->insertEntity (mEntities[ee]);
			(*bb)->link (mEntities[ee]);
		}
		RestoreSelectionList (mBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdFlipSelection
	//---------------------------------------------
	CmdFlipSelection::CmdFlipSelection (const vector3& axis)
	{
		mAxis = axis;
	}
	
	CmdFlipSelection::~CmdFlipSelection ()
	{
	}
	
	cStr		CmdFlipSelection::screenName (void) const
	{
		return "Flip selection";
	}
	
	void		CmdFlipSelection::doIt (void)
	{
		brushList::iterator bb;
		// get origin
		mOrigin = vector3 (0, 0, 0);
		for (bb = g_editor->getMap ()->getSelection ().begin (); bb != g_editor->getMap ()->getSelection ().end (); bb++)
		{
			smartPtr <brush> b = *bb;
			mOrigin += b->mins () + b->maxs ();
		}
		mOrigin /= (float)g_editor->getMap ()->getSelection ().size () * 2;
	
		for (bb = g_editor->getMap ()->getSelection ().begin (); bb != g_editor->getMap ()->getSelection ().end (); bb++)
		{
			smartPtr <brush> b = *bb;
			b->flip (mAxis, mOrigin);
		}
	
		g_editor->redrawAllViews ();
	}
	
	void		CmdFlipSelection::undo (void)
	{
		brushList::iterator bb;
		for (bb = g_editor->getMap ()->getSelection ().begin (); bb != g_editor->getMap ()->getSelection ().end (); bb++)
		{
			smartPtr <brush> b = *bb;
			b->flip (-mAxis, mOrigin);
		}
	
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdInsertBrushes
	//---------------------------------------------
	CmdInsertBrushes::CmdInsertBrushes (const std::vector< smartPtr <entity>  > &entities, const std::list< smartPtr <brush>  > &brushes)
	{
		mOwners = entities;
		mBrushes = brushes;
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			mSelBrushes.push_back (*i);
		}
		SaveSelectionList (mSelBrushes);
	}
	
	CmdInsertBrushes::~CmdInsertBrushes ()
	{
	}
	cStr		CmdInsertBrushes::screenName (void) const
	{
		return "Insert Brushes";
	}
	
	void		CmdInsertBrushes::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
		brushList::iterator b;
		entList::iterator e;
		for (e = mOwners.begin (), b = mBrushes.begin (); b != mBrushes.end (); b++, e++)
		{
			smartPtr <entity> ent = *e;
			smartPtr <brush> newbrush = *b;
			
			if (strcmp (ent->getEClass ()->getName (), "worldspawn")
				&& g_editor->getMap ()->isEmptyEnt (ent))
			{
				g_editor->getMap ()->insertEntity (ent);
			}
			g_editor->getMap ()->insertBrush (newbrush);
			g_editor->getMap ()->addSelection (newbrush);
			newbrush->link (ent);
		}
	
		g_editor->redrawAllViews ();
	}
	
	void		CmdInsertBrushes::undo (void)
	{
		brushList::iterator b;
		entList::iterator e;
		for (e = mOwners.begin (), b = mBrushes.begin (); b != mBrushes.end (); b++, e++)
		{
			smartPtr <entity> ent = *e;
			smartPtr <brush> newbrush = *b;
			newbrush->unlink ();
			g_editor->getMap ()->removeBrush (newbrush);
			if (strcmp (ent->getEClass ()->getName (), "worldspawn")
				&& g_editor->getMap ()->isEmptyEnt (ent))
			{
				g_editor->getMap ()->removeEntity (ent);
			}
		}
		RestoreSelectionList (mSelBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdSelectAll
	//---------------------------------------------
	CmdSelectAll::CmdSelectAll (void)
	{
		// build list of currently selected brushes and addref 'em
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = (*i);
			mBrushes.push_back (b);
		}
		SaveSelectionList (mBrushes);
	}
	
	CmdSelectAll::~CmdSelectAll ()
	{
	}
	
	cStr		CmdSelectAll::screenName (void) const
	{
		return "Select all";
	}
	
	void		CmdSelectAll::doIt (void)
	{
		// clear selection
		g_editor->getMap ()->clearSelection ();
	
		brushList lst = g_editor->getMap ()->getBrushList ();
		for (brushList::iterator i = lst.begin (); i != lst.end (); i++)
			g_editor->getMap ()->addSelection (*i);
	
		g_editor->redrawAllViews ();
	}
	
	void		CmdSelectAll::undo (void)
	{
		// clear selection
		g_editor->getMap ()->clearSelection ();
		RestoreSelectionList (mBrushes);
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdSetSelectionTexdef
	//---------------------------------------------
	CmdSetSelectionTexdef::CmdSetSelectionTexdef (const char *mtl, int shift[2], float scale[2], int rotate)
	{
		mTargetTexdef.mtl = mtl;
		mTargetTexdef.shift[0] = shift[0];
		mTargetTexdef.shift[1] = shift[1];
		mTargetTexdef.scale[0] = scale[0];
		mTargetTexdef.scale[1] = scale[1];
		mTargetTexdef.rotate = rotate;
	
		// remember texdef values
		int f = 0;
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = (*i);
			for (int j = 0; j < b->numFaces (); j++)
			{
				if (false == b->isFaceSelected (j))
					continue;
				mTexdefs.resize (f + 1);
				const brushFaceTexdef &td = b->faces (j).texdef ();
				mTexdefs[f].mtl = td.mtl;
				mTexdefs[f].shift[0] = td.shift[0];
				mTexdefs[f].shift[1] = td.shift[1];
				mTexdefs[f].scale[0] = td.scale[0];
				mTexdefs[f].scale[1] = td.scale[1];
				mTexdefs[f].rotate = td.rotate;
				f++;
			}
		}
	}
	
	CmdSetSelectionTexdef::~CmdSetSelectionTexdef ()
	{
	}
	
	cStr		CmdSetSelectionTexdef::screenName (void) const
	{
		return "Set selection texdef";
	}
	
	void		CmdSetSelectionTexdef::doIt (void)
	{
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = (*i);
			for (int j = 0; j < b->numFaces (); j++)
			{
				if (false == b->isFaceSelected (j))
					continue;
				brushFaceTexdef &td = b->faces (j).texdef ();
				td.mtl = mTargetTexdef.mtl;
				td.shift[0] = mTargetTexdef.shift[0];
				td.shift[1] = mTargetTexdef.shift[1];
				td.scale[0] = mTargetTexdef.scale[0];
				td.scale[1] = mTargetTexdef.scale[1];
				td.rotate = mTargetTexdef.rotate;
				b->faces (j).applyTexdef ();
			}
			b->invalidateRendererCache ();
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdSetSelectionTexdef::undo (void)
	{
		int f = 0;
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = (*i);
			for (int j = 0; j < b->numFaces (); j++)
			{
				if (false == b->isFaceSelected (j))
					continue;
				brushFaceTexdef &td = b->faces (j).texdef ();
				td.mtl = mTexdefs[f].mtl;
				td.shift[0] = mTexdefs[f].shift[0];
				td.shift[1] = mTexdefs[f].shift[1];
				td.scale[0] = mTexdefs[f].scale[0];
				td.scale[1] = mTexdefs[f].scale[1];
				td.rotate = mTexdefs[f].rotate;
				b->faces (j).applyTexdef ();
				f++;
			}
			b->invalidateRendererCache ();
		}
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdToggleFaceSelection
	//---------------------------------------------
	CmdToggleFaceSelection::CmdToggleFaceSelection (smartPtr <brush> b, int face)
	{
		mpBrush = b;
		mFace = face;
	}
	
	CmdToggleFaceSelection::~CmdToggleFaceSelection ()
	{
	}
	
	cStr		CmdToggleFaceSelection::screenName (void) const
	{
		return "Toggle face selection";
	}
	
	void		CmdToggleFaceSelection::doIt (void)
	{
		if (mpBrush->isSelected ())
		{
			mpBrush->selectFace (mFace, mpBrush->isFaceSelected (mFace) ? false : true);
			if (!mpBrush->isSelected ())
				g_editor->getMap ()->removeSelection (mpBrush);
		}
		else
		{
			g_editor->getMap ()->addSelection (mpBrush);
			for (int i = 0; i < mpBrush->numFaces (); i++)
				mpBrush->selectFace (i, i == mFace ? true : false);
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdToggleFaceSelection::undo (void)
	{
		if (mpBrush->isSelected ())
		{
			mpBrush->selectFace (mFace, mpBrush->isFaceSelected (mFace) ? false : true);
			if (!mpBrush->isSelected ())
				g_editor->getMap ()->removeSelection (mpBrush);
		}
		else
		{
			g_editor->getMap ()->addSelection (mpBrush);
			for (int i = 0; i < mpBrush->numFaces (); i++)
				mpBrush->selectFace (i, i == mFace ? true : false);
		}
		g_editor->redrawAllViews ();
	}
	
	
	//---------------------------------------------
	// CmdSetMaterial
	//---------------------------------------------
	CmdSetMaterial::CmdSetMaterial (const char *mtl)
	{
		mNewMaterial = mtl;
	
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = (*i);
			for (int j = 0; j < b->numFaces (); j++)
			{
				if (b->isFaceSelected (j))
				{
					mMaterials.push_back (b->faces (j).texdef ().mtl);
				}
			}
		}
	}
	
	CmdSetMaterial::~CmdSetMaterial ()
	{
	}
	
	cStr		CmdSetMaterial::screenName (void) const
	{
		return "Set material";
	}
	
	void		CmdSetMaterial::doIt (void)
	{
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = (*i);
			for (int j = 0; j < b->numFaces (); j++)
			{
				if (b->isFaceSelected (j))
					b->faces (j).setMtl (mNewMaterial);
			}
			b->invalidateRendererCache ();
		}
		g_editor->redrawAllViews ();
	}
	
	void		CmdSetMaterial::undo (void)
	{
		int f = 0;
		for (brushList::iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = (*i);
			for (int j = 0; j < b->numFaces (); j++)
			{
				if (b->isFaceSelected (j))
				{
					b->faces (j).setMtl (mMaterials[f]);
					f++;
				}
			}
			b->invalidateRendererCache ();
		}
		g_editor->redrawAllViews ();
	}
	
	//---------------------------------------------
	// CmdSelectWithBrush
	//---------------------------------------------
	CmdSelectWithBrush::CmdSelectWithBrush (smartPtr <brush> sel, CmdSelectWithBrush::selection_type t, int ortho_mode)
	{
		assert (sel);
		mSelBrush = sel;
		mOrthoMode = ortho_mode;
		
		// remember selection
		brushList::iterator i;
		for (i = mBrushes.begin (); i != mBrushes.end (); i++)
		{
			smartPtr <brush> obj = *i;
			mBrushes.push_back (obj);
		}
	
		// store new selection into mSelection
		brushList::const_iterator bb;
		for (bb = g_editor->getMap ()->getBrushList ().begin (); bb != g_editor->getMap ()->getBrushList ().end (); ++bb)
		{
			if ((*bb)->getOwnerEnt () == g_editor->getMap ()->getWorldEntity ())
			{
				if (IsInside (*bb, t))
					mSelection.push_back (*bb);
			}
		}
	
		for (size_t ee = 0; ee < g_editor->getMap ()->getEntities ().size (); ee++)
		{
			smartPtr <entity> e = g_editor->getMap ()->getEntities ()[ee];
			if (IsInside (e, t))
			{
				for (bb = g_editor->getMap ()->getBrushList ().begin (); bb != g_editor->getMap ()->getBrushList ().end (); ++bb)
				{
					if (e == (*bb)->getOwnerEnt ())
					{
						mSelection.push_back (*bb);
					}
				}
			}
		}
	
		SaveSelectionList (mBrushes);
	}
	
	CmdSelectWithBrush::~CmdSelectWithBrush ()
	{
	}
	
	cStr		CmdSelectWithBrush::screenName (void) const
	{
		return "Select with brush";
	}
	
	void		CmdSelectWithBrush::doIt (void)
	{
		g_editor->getMap ()->clearSelection ();
		g_editor->getMap ()->removeBrush (mSelBrush);
		brushList::iterator i;
		for (i = mSelection.begin (); i != mSelection.end (); i++)
			g_editor->getMap ()->addSelection (*i);
		g_editor->redrawAllViews ();
	}
	
	void		CmdSelectWithBrush::undo (void)
	{
		g_editor->getMap ()->clearSelection ();
		g_editor->getMap ()->insertBrush (mSelBrush);
		RestoreSelectionList (mBrushes);
		g_editor->redrawAllViews ();
	}
	
	bool	CmdSelectWithBrush::IsInside (smartPtr <brush> b, CmdSelectWithBrush::selection_type t)
	{
		if (mSelBrush == b)
			return false;
		if (t == select_touching)
		{
			// touching is when intersects or has one or more touching faces
			// FIXME: maybe i should add some threshold when comparing planes...
	        // NOTE: it is much better (more precise) than radiant's implementation (which uses AABs), though slower
			// NOTE: (while doing linux port) mabbe radiant way is more reilable since artist can just need that small precision, but who cares
			for (int fsel_i = 0; fsel_i < mSelBrush->numFaces (); fsel_i++)
			{
				const brushFace &fsel = mSelBrush->faces (fsel_i);
				int face_i;
				for (face_i = 0; face_i < b->numFaces (); face_i++)
				{
					const brushFace &face = b->faces (face_i);
	
					if (fsel.plane ().normal () == -face.plane ().normal () && fsel.plane ().constant () == -face.plane ().constant ())
						break;
				}
				if (face_i == b->numFaces ())
				{
					// split
					smartPtr <brush> front, back;
					b->splitByFace (fsel, front, back);
					if (back == NULL)
					return false;
				}
			}
			return true;
		}
		// FIXME: these 3 kinds are the same as radiant's (quick and dirty way)
		else if (t == select_inside)
		{
			vector3 mins = mSelBrush->mins (), maxs = mSelBrush->maxs ();
			int i;
	        for (i = 0; i < 3; i++)
			{
				if (b->maxs ()[i] > maxs[i] || b->mins ()[i] < mins[i])
					break;
			}
			if (i == 3)
				return true;
		}
		else if (t == select_complete_tall)
		{
			vector3 mins = mSelBrush->mins (), maxs = mSelBrush->maxs ();
			int nDim1 = (mOrthoMode == editorViewport::yz) ? 1 : 0;
			int nDim2 = (mOrthoMode == editorViewport::xy) ? 1 : 2;
			if (! ( (b->maxs ()[nDim1] > maxs[nDim1] || b->mins ()[nDim1] < mins[nDim1]) 
				|| (b->maxs ()[nDim2] > maxs[nDim2] || b->mins ()[nDim2] < mins[nDim2])))
				return true;
		}
		else if (t == select_partial_tall)
		{
			vector3 mins = mSelBrush->mins (), maxs = mSelBrush->maxs ();
			int nDim1 = (mOrthoMode == editorViewport::yz) ? 1 : 0;
			int nDim2 = (mOrthoMode == editorViewport::xy) ? 1 : 2;
			if (! ( (b->mins ()[nDim1] > maxs[nDim1] || b->maxs ()[nDim1] < mins[nDim1]) 
				|| (b->mins ()[nDim2] > maxs[nDim2] || b->maxs ()[nDim2] < mins[nDim2])))
				return true;
		}
	
		return false;
	}
	
	bool	CmdSelectWithBrush::IsInside (smartPtr <entity> e, CmdSelectWithBrush::selection_type t)
	{
		brushList::const_iterator bb;
		size_t cnt = 0;
		size_t sz = 0;
		
		for (bb = g_editor->getMap ()->getBrushList ().begin (); bb != g_editor->getMap ()->getBrushList ().end (); ++bb)
		{
			if ((*bb)->getOwnerEnt () == e)
			{
				sz++;
				if (IsInside (*bb, t))
					cnt++;
			}
		}
		if (t == select_touching && cnt != 0)
			return true;	// everything is inside
		else if (t == select_inside && cnt == sz)
			return true;	// partially inside
		else if (t == select_complete_tall && cnt == sz)
			return true;	// everything is inside
		else if (t == select_partial_tall && cnt != 0)
			return true;	// partially inside
	
		return false;
	}
	
	CmdSetEntityAttribute::CmdSetEntityAttribute (smartPtr <entity> ent, const char *key, const char *value)
	{
		mpEntity = ent;
		mKey = key;
	
		if (value)
		{
			mbDoKill = false;
			mNewValue = value;
		}
		else
			mbDoKill = true;
	
		const char *val = ent->getAttrValue (key);
		if (val)
		{
			mbUndoKill = false;
			mOldValue = val;
		}
		else
			mbUndoKill = true;
	}
	
	cStr CmdSetEntityAttribute::screenName (void) const
	{
		return "Set Entity Attribute";
	}
	
	void CmdSetEntityAttribute::doIt (void)
	{
		if (mbDoKill)
			mpEntity->eraseAttr (mKey);
		else
			mpEntity->setAttrValue (mKey, mNewValue);
		g_editor->getMainFrm ()->getEntityInspector ()->update ();
	}
	
	void CmdSetEntityAttribute::undo (void)
	{
		if (mbUndoKill)
			mpEntity->eraseAttr (mKey);
		else
			mpEntity->setAttrValue (mKey, mOldValue);
		g_editor->getMainFrm ()->getEntityInspector ()->update ();
	}
	
	CmdClearAllEntAttributes::CmdClearAllEntAttributes (smartPtr <entity> ent)
	{
		mpEntity = ent;
		mAttrValues = ent->getEPairs ();
	}
	
	cStr CmdClearAllEntAttributes::screenName (void) const
	{
		return "Clear all entity attributes";
	}
	
	void CmdClearAllEntAttributes::doIt (void)
	{
		for (std::vector <ePair>::iterator i = mAttrValues.begin (); i != mAttrValues.end (); i++)
		{
			if (strcmp ((*i).getName (), "classname"))
				mpEntity->eraseAttr ((*i).getName ());
		}
		g_editor->getMainFrm ()->getEntityInspector ()->update ();
	}
	
	void CmdClearAllEntAttributes::undo (void)
	{
		for (std::vector <ePair>::iterator i = mAttrValues.begin (); i != mAttrValues.end (); i++)
		{
			if (strcmp ((*i).getName (), "classname"))
				mpEntity->setAttrValue ((*i).getName (), (*i).getValue ());
		}	
		g_editor->getMainFrm ()->getEntityInspector ()->update ();
	}
	
}
