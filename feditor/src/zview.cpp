#include "pch.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <fegdk/f_texture.h>
#include <fegdk/f_fontft.h>
#include <fegdk/f_drawutil.h>
#include <fegdk/f_engine.h>
#include <fegdk/f_resourcemgr.h>
#include "editorcore.h"
#include "zview.h"
#include "glview.h"
#include "brush.h"
#include "editormap.h"

namespace feditor
{

	zView::zView (void)
	{
		mbScroll = false;
		mScroll = 0;
		// mouse events
		g_signal_connect_after (G_OBJECT (mpWidget), "button-press-event",
						G_CALLBACK (gtkButtonPress), this);
		g_signal_connect_after (G_OBJECT (mpWidget), "button-release-event",
						G_CALLBACK (gtkButtonRelease), this);
		g_signal_connect_after (G_OBJECT (mpWidget), "motion-notify-event",
						G_CALLBACK (gtkMotionNotify), this);
	}
	
	zView::~zView (void)
	{
	}
	
	void zView::init (void)
	{
		beginGL ();
	
		// get a font
		if (!mpFont)
			mpFont = fe::g_engine->getResourceMgr ()->createFontFT ("ter-116n.pcf");
		
		endGL ();
	}
	
	void zView::draw (void)
	{
		// q3radiant style colors
		float majorclr[] = {.5f, .5f, .5f, 1.f};
		float minorclr[] = {.8f, .8f, .8f, 1.f};
		float originclr[] = {.0f, .0f, .0f, 1.f};
		float orthobackclr[] = {1.f, 1.f, 1.f};
		float perspbackclr[] = {.25f, .25f, .56f, 1.f};
		float textclr[] = {.0f, .62f, .0f, 1.f};
		float textshadowclr[] = {.0f, .0f, .0f, .5f};

		smartPtr <fe::drawUtil> du = fe::g_engine->getDrawUtil ();
	
		fe::g_engine->setViewport (mpViewport.dynamicCast <fe::baseViewport> ());
		beginGL ();
		
		glClearColor (0.9f, 0.9f, 0.9f, 1.f);
		glClear (GL_COLOR_BUFFER_BIT);
		glDisable (GL_DEPTH_TEST);
	
		du->setOrthoTransforms ();
		du->allowTransforms (false);
	
		float region_mins = -128*1024;
		float region_maxs = 128*1024;
		int majorstep = 64;
	
		long h = mpWidget->allocation.height;
		long yb = (long) (-h/2 + mScroll - 64);
		if (yb < region_mins)
			yb = (long)region_mins;
		yb = (long) (majorstep * floor ((float) (yb/majorstep)));
		long ye = (long) (h/2 + mScroll + 64);
		if (ye > region_maxs)
			ye = (long)region_maxs;
		ye = (long) (majorstep * ceil ((float) (ye/majorstep)));
	
		int y;
		float ox = mpWidget->allocation.width/2.f;
		float oy = mpWidget->allocation.height/2.f-mScroll;
		float ow = mpWidget->allocation.width;
		float oh = mpWidget->allocation.height;
		for (y = yb; y <= ye; y += 8)
		{
			float4 color;
			if (0 == y)
				color = originclr;
			else if (y % 64)
				color = minorclr;
			else
				color = majorclr;
			du->drawLine (ox-mpWidget->allocation.width/2.f, oy+ (float)y, ox+mpWidget->allocation.width/2.f, oy+ (float)y, color);
		}
	//	du->drawLine (ox+0, 0, ox+0, oy+mpWidget->allocation.height/2.f + mScroll, majorclr);
		// draw text
		for (y = yb; y <= ye; y += 8)
		{
			if ( (y % 64) == 0)
			{
				cStr s;
				s.printf ("%d", y);
	
				// text uses inverted y-axis, so we must invert it
				float text_y = mpWidget->allocation.height - (oy+y);
				mpFont->drawTextString (s, (int)ox-mpWidget->allocation.width/2, (int)text_y, 0xff000000);
			}
		}
	
	
		for (brushList::const_iterator i = g_editor->getMap ()->getSelection ().begin (); i != g_editor->getMap ()->getSelection ().end (); i++)
		{
			smartPtr <brush> b = *i;
			const vector3 &mins = b->mins ();
			const vector3 &maxs = b->maxs ();
	
			if (mins.z < region_mins || maxs.z > region_maxs
				|| mins.z > region_maxs || maxs.z < region_mins)
				continue;
			du->drawLine (ox-ow/2+8, oy+mins.z, ox+ow/2-8, oy+mins.z, float4 (0.f, 0, 1.f, 1.f));
			du->drawLine (ox+ow/2-8, oy+mins.z, ox+ow/2-8, oy+maxs.z, float4 (0.f, 0, 1.f, 1.f));
			du->drawLine (ox+ow/2-8, oy+maxs.z, ox-ow/2+8, oy+maxs.z, float4 (0.f, 0, 1.f, 1.f));
			du->drawLine (ox-ow/2+8, oy+maxs.z, ox-ow/2+8, oy+mins.z, float4 (0.f, 0, 1.f, 1.f));
		}
	
		du->allowTransforms (true);
	
		endGL ();
		
	}
	
	void zView::reshape (void)
	{
		GLfloat h = (GLfloat) (mpWidget->allocation.height) /
			   (GLfloat) (mpWidget->allocation.width);
		beginGL ();
		
		glViewport (0, 0, mpWidget->allocation.width, mpWidget->allocation.height);
		glMatrixMode (GL_PROJECTION);
		glLoadIdentity ();
		glMatrixMode (GL_MODELVIEW);
		glLoadIdentity ();
		
		endGL (false);
	}
	
	gboolean zView::gtkButtonPress (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
	{
		zView *view = (zView *)user_data;
		if (event->button == 3)
		{
			// as far as i can tell gtk does 'setcapture' by default (sorry my windows lame)
			// so we don't do anything special here
			view->mbScroll = true;
			view->mLastMousePt.x = (int)event->x;
			view->mLastMousePt.y = (int)event->y;
		}
		return TRUE;
	}
	
	gboolean zView::gtkButtonRelease (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
	{
		zView *view = (zView *)user_data;
		if (event->button == 3)
			view->mbScroll = false;
		return TRUE;
	}
	
	gboolean zView::gtkMotionNotify (GtkWidget *widget, GdkEventMotion *event, gpointer user_data)
	{
		zView *view = (zView *)user_data;
		if (view->mbScroll)
		{
			view->mScroll += (int)event->y - view->mLastMousePt.y;
			view->mLastMousePt.x = (int)event->x;
			view->mLastMousePt.y = (int)event->y;
			view->draw ();
		}
		return TRUE;
	}
	
}
