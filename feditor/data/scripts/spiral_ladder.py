import feimport math

w = 64
h = 64
z = 0

def rotate_around_z(point, angle):    sint = math.sin(angle)
    cost = math.cos(angle)
    pt = []
    pt.append( point[0]*cost+point[1]*sint )
    pt.append( point[0]*(-sint)+point[1]*cost )
    pt.append( point[2] )
    return pt

i = 0.0pt2 = [0.0, 0.0, 0.0]

while i < 32:    pt = [100.0, 0.0, 0.0]
    angle = i*0.785397/2
    pt2 = rotate_around_z(pt,angle)
    brush = fe.Brush_Create( pt2[0]-w,pt2[1]-h,pt2[2]+z,pt2[0]+w,pt2[1]+h,pt2[2]+z+8)
    fe.Brush_Rotate(brush,0,0,1,pt2[0],pt2[1],pt2[2]+z+4,angle/3.14159*180)
    i = i+1
    z = z+8

