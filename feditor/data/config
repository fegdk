# $Header$

# keybindings

# editor assigns default bindings before loading this file;
# 'unbindall' command resets all bindings;
# can be especially useful when execing config from python script;
# can be altered when unbinding isn't required
unbindall

# warning:
# 'alt' key cannot be used for editor keybindings
# due to potential win32 menu conflicts;
# engine input layer doesn't have this limitation when using dinput;
# ctrl is an alias for both lcontrol and rcontrol

# syntax:
# bind [ctrl+][shift+]keycode actionname
# key-combo can be arbitrarily arranged, e.g. "a+ctrl+shift" or "shift+x+ctrl"
# keycode IS requirement for key-combo 
# ctrl and shift are modifiers and may be unused
# action names containing '+' character must be enclosed with quotation marks

# note:
# actionnames are all used in code/editor/globals.cpp
# currently there is no other doc for that

# note:
# you cannot change mouse-related bindings
# - such as brush select/move/dragfaces
# - you can't change default mouse behavior at all
# - even from python
# - you can hack into editor source code (undesirable)

bind backspace	"editor_deleteselection"
bind "lctrl+z"	"editor_undo"
bind "rctrl+z"	"editor_undo"
bind "lctrl+y"	"editor_redo"
bind "rctrl+y"	"editor_redo"
bind "lctrl+a"	"editor_selectall"
bind "rctrl+a"	"editor_selectall"
bind escape		"editor_cancel"
# context action, clears selection in default context, cancels out clipper, rotation or whatever
bind enter		"editor_apply"
# used to apply default action of the current context. e.g. does "clip selection" in splitter context
bind x			"editor_toggleclipper"
bind s			"editor_togglesurfaceinspector"
bind e			"editor_toggleentityinspector"
bind s			"editor_togglesurfaceinspector"
bind r			"editor_togglerotation"
bind tab		"editor_cyclelayout"
bind "lctrl+h"	"editor_csghollow"
bind "lctrl+s"	"editor_csgsubtract"

bind "lctrl+c" "editor_copyselection"
bind "lctrl+v" "editor_pasteselection"
bind "lctrl+x" "editor_cutselection"

bind m editor_togglemtlmgr
bind c editor_togglecamera
bind w editor_togglecamwireframe

bind "lshift+g" "editor_togglegrid"

bind "lctrl+3"	"editor_makesidedbrush 16"

# camera control
# note: '+' actions, such as '+left' or '+moveforward' have empty release action
bind up "stepforward"
bind down	"stepbackward"
bind left	"rotatestepleft"
bind right	"rotatestepright"
bind pgup		"stepup"
bind pgdn		"stepdown"
bind home		"rotatestepup"
bind end		"rotatestepdown"
bind "lctrl+left"	"stepleft"
bind "lctrl+right"	"stepright"
# bind "t" "testmove"

# misc settings
editor_drawentnames		1
editor_drawrulers			1
# q3radiant does 'clip', so this is default. some users (like me) may prefer 'split'
editor_defaultclipaction	split
# mtl browser settings
# specifies what mtl groups to load at startup
addmaterials common
/*
mtlgroup base_floor
mtlgroup base_button
mtlgroup base_light
mtlgroup base_trim
mtlgroup base_wall
mtlgroup base_object
mtlgroup base_support
*/

# material directories
# add directory to list
# so you can repeatedly issue mtldir to specify more than one directory
editor_mtldir	materials


