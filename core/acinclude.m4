dnl @synopsis AC_C_COMPILE_VALUE (COMPILE-VALUE, ALIAS, INCLUDES)
dnl
dnl The AC_C_COMPILE_VALUE macro determines a compile time value
dnl by generating the object code and reading the value from the
dnl code.  Static data initializers like sizeof(int) are
dnl unavailable to preprocessor.  The macro calculates the values
dnl known to compiler's static initializer.
dnl
dnl Assumptions:
dnl The sought value should not exceed 65535.
dnl The shell interpreter and the sed utility are expected to
dnl exist and work similarly across possible build platforms.
dnl
dnl Result:
dnl The resulting configure script will generate the preprocessor
dnl symbol definition:
dnl
dnl   #define COMPILE_VALUE_<ALIAS> <NUMBER>
dnl
dnl It was important that the value was embedded into the object
dnl file in a predefined byte order during the test.  This ensured
dnl that the result was independent from the target platform's byte
dnl order.
dnl
dnl The existing AC_CHECK_SIZEOF macro also computes the size of
dnl the given type without running the test program.   However, the
dnl existing macro will produce a piece of configure script that
dnl will take the time proportional to the logarithm of the sought
dnl value.
dnl
dnl Example of use in configure.in:
dnl
dnl   AC_C_COMPILE_VALUE(sizeof(int), sizeof_int)
dnl   AC_C_COMPILE_VALUE([sizeof(int[[543]])], sizeof_int543)
dnl
dnl As a result of runnfing the generated configure script, the
dnl following definition will appear in config.h:
dnl
dnl   #define COMPILE_VALUE_SIZEOF_INT 4
dnl   #define COMPILE_VALUE_SIZEOF_INT543 2172
dnl
dnl @version $Id$
dnl @author Ilguiz Latypov <ilatypov@superbt.com>
dnl
## Portability defines that help interoperate with classic and modern autoconfs
ifdef([AC_TR_SH],[
define([AC_TR_SH_REUSE],[AC_TR_SH([$1])])
define([AC_TR_CPP_REUSE],[AC_TR_CPP([$1])])
], [
define([AC_TR_SH_REUSE],
       [patsubst(translit([[$1]], [*+], [pp]), [[^a-zA-Z0-9_]], [_])])
define([AC_TR_CPP_REUSE],
       [patsubst(translit([[$1]],
                          [*abcdefghijklmnopqrstuvwxyz],
                          [PABCDEFGHIJKLMNOPQRSTUVWXYZ]),
                 [[^A-Z0-9_]], [_])])
])

AC_DEFUN([AC_C_COMPILE_VALUE], [
  pushdef([ac_c_compile_value],
    AC_TR_SH_REUSE([ac_cv_c_compile_value_$2]))dnl
  ac_c_compile_value_expand="$1"
  AC_CACHE_CHECK([value of $1 by analyzing object code],
                 ac_c_compile_value, [
    save_CFLAGS="$CFLAGS"
    CFLAGS="$CFLAGS -c -o conftest.o"
    AC_TRY_COMPILE([$3
      #include <stddef.h>
      #include <stdint.h>
      #include <stdlib.h>
      #define COMPILE_VALUE $ac_c_compile_value_expand
      #define HEX_DIGIT(n)      ((n) >= 10 ? 'a' + (n) - 10 : '0' + (n))
      char object_code_block[] = {
        '\n', 'e', '4', 'V', 'A',
        '0', 'x',
        (char) HEX_DIGIT((((COMPILE_VALUE / 16) / 16) / 16) % 16),
        (char) HEX_DIGIT(((COMPILE_VALUE / 16) / 16) % 16),
        (char) HEX_DIGIT((COMPILE_VALUE / 16) % 16),
        (char) HEX_DIGIT(COMPILE_VALUE % 16),
        'Y', '3', 'p', 'M', '\n'
      };],
      [],
      [ac_c_compile_value=`
        typeset -i n=\`sed -ne 's/^e4VA0x\(.*\)Y3pM$/0x\1/p' < conftest.o\`;
        echo $n`],
      [ac_c_compile_value=0])
    CFLAGS="$save_CFLAGS"])
  AC_DEFINE_UNQUOTED(AC_TR_CPP_REUSE(compile_value_$2),
                     [$[]ac_c_compile_value],
                     [$1])
  popdef([ac_c_compile_value])dnl
])
dnl @synopsis AC_C_BIGENDIAN_CROSS
dnl
dnl Check endianess even when crosscompiling
dnl (partially based on the original AC_C_BIGENDIAN).
dnl
dnl The implementation will create a binary, but instead of running
dnl the binary it will be grep'ed for some symbols that differ for
dnl different endianess of the binary.
dnl
dnl NOTE: The upcoming autoconf 2.53 does include the idea of this macro,
dnl what makes it superfluous by then.
dnl
dnl @version $Id$
dnl @author Guido Draheim <guidod@gmx.de>
dnl
AC_DEFUN([AC_C_BIGENDIAN_CROSS],
[AC_CACHE_CHECK(whether byte ordering is bigendian, ac_cv_c_bigendian,
[ac_cv_c_bigendian=unknown
# See if sys/param.h defines the BYTE_ORDER macro.
AC_TRY_COMPILE([#include <sys/types.h>
#include <sys/param.h>], [
#if !BYTE_ORDER || !BIG_ENDIAN || !LITTLE_ENDIAN
 bogus endian macros
#endif], [# It does; now see whether it defined to BIG_ENDIAN or not.
AC_TRY_COMPILE([#include <sys/types.h>
#include <sys/param.h>], [
#if BYTE_ORDER != BIG_ENDIAN
 not big endian
#endif], ac_cv_c_bigendian=yes, ac_cv_c_bigendian=no)])
if test $ac_cv_c_bigendian = unknown; then
AC_TRY_RUN([main () {
  /* Are we little or big endian?  From Harbison&Steele.  */
  union
  {
    long l;
    char c[sizeof (long)];
  } u;
  u.l = 1;
  exit (u.c[sizeof (long) - 1] == 1);
}], ac_cv_c_bigendian=no, ac_cv_c_bigendian=yes,
[ echo $ac_n "cross-compiling... " 2>&AC_FD_MSG ])
fi])
if test $ac_cv_c_bigendian = unknown; then
AC_MSG_CHECKING(to probe for byte ordering)
[
cat >conftest.c <<EOF
short ascii_mm[] = { 0x4249, 0x4765, 0x6E44, 0x6961, 0x6E53, 0x7953, 0 };
short ascii_ii[] = { 0x694C, 0x5454, 0x656C, 0x6E45, 0x6944, 0x6E61, 0 };
void _ascii() { char* s = (char*) ascii_mm; s = (char*) ascii_ii; }
short ebcdic_ii[] = { 0x89D3, 0xE3E3, 0x8593, 0x95C5, 0x89C4, 0x9581, 0 };
short ebcdic_mm[] = { 0xC2C9, 0xC785, 0x95C4, 0x8981, 0x95E2, 0xA8E2, 0 };
void _ebcdic() { char* s = (char*) ebcdic_mm; s = (char*) ebcdic_ii; }
int main() { _ascii (); _ebcdic (); return 0; }
EOF
] if test -f conftest.c ; then
     if ${CC-cc} -c conftest.c -o conftest.o && test -f conftest.o ; then
        if test `grep -l BIGenDianSyS conftest.o` ; then
           echo $ac_n ' big endian probe OK, ' 1>&AC_FD_MSG
	   ac_cv_c_bigendian=yes
        fi
        if test `grep -l LiTTleEnDian conftest.o` ; then
           echo $ac_n ' little endian probe OK, ' 1>&AC_FD_MSG
	   if test $ac_cv_c_bigendian = yes ; then
	    ac_cv_c_bigendian=unknown;
           else
            ac_cv_c_bigendian=no
           fi
        fi
	echo $ac_n 'guessing bigendian ...  ' >&AC_FD_MSG
     fi
  fi
AC_MSG_RESULT($ac_cv_c_bigendian)
fi
if test $ac_cv_c_bigendian = yes; then
  AC_DEFINE(WORDS_BIGENDIAN, 1, [whether byteorder is bigendian])
  BYTEORDER=4321
else
  BYTEORDER=1234
fi
AC_DEFINE_UNQUOTED(BYTEORDER, $BYTEORDER, [1234 = LIL_ENDIAN, 4321 = BIGENDIAN])
if test $ac_cv_c_bigendian = unknown; then
  AC_MSG_ERROR(unknown endianess - sorry, please pre-set ac_cv_c_bigendian)
fi
])

dnl @synopsis AC_C_BIGENDIAN_CROSS
dnl
dnl Check endianess even when crosscompiling
dnl (partially based on the original AC_C_BIGENDIAN).
dnl
dnl The implementation will create a binary, but instead of running
dnl the binary it will be grep'ed for some symbols that differ for
dnl different endianess of the binary.
dnl
dnl NOTE: The upcoming autoconf 2.53 does include the idea of this macro,
dnl what makes it superfluous by then.
dnl
dnl @version $Id$
dnl @author Guido Draheim <guidod@gmx.de>
dnl
AC_DEFUN([AC_C_BIGENDIAN_CROSS],
[AC_CACHE_CHECK(whether byte ordering is bigendian, ac_cv_c_bigendian,
[ac_cv_c_bigendian=unknown
# See if sys/param.h defines the BYTE_ORDER macro.
AC_TRY_COMPILE([#include <sys/types.h>
#include <sys/param.h>], [
#if !BYTE_ORDER || !BIG_ENDIAN || !LITTLE_ENDIAN
 bogus endian macros
#endif], [# It does; now see whether it defined to BIG_ENDIAN or not.
AC_TRY_COMPILE([#include <sys/types.h>
#include <sys/param.h>], [
#if BYTE_ORDER != BIG_ENDIAN
 not big endian
#endif], ac_cv_c_bigendian=yes, ac_cv_c_bigendian=no)])
if test $ac_cv_c_bigendian = unknown; then
AC_TRY_RUN([main () {
  /* Are we little or big endian?  From Harbison&Steele.  */
  union
  {
    long l;
    char c[sizeof (long)];
  } u;
  u.l = 1;
  exit (u.c[sizeof (long) - 1] == 1);
}], ac_cv_c_bigendian=no, ac_cv_c_bigendian=yes,
[ echo $ac_n "cross-compiling... " 2>&AC_FD_MSG ])
fi])
if test $ac_cv_c_bigendian = unknown; then
AC_MSG_CHECKING(to probe for byte ordering)
[
cat >conftest.c <<EOF
short ascii_mm[] = { 0x4249, 0x4765, 0x6E44, 0x6961, 0x6E53, 0x7953, 0 };
short ascii_ii[] = { 0x694C, 0x5454, 0x656C, 0x6E45, 0x6944, 0x6E61, 0 };
void _ascii() { char* s = (char*) ascii_mm; s = (char*) ascii_ii; }
short ebcdic_ii[] = { 0x89D3, 0xE3E3, 0x8593, 0x95C5, 0x89C4, 0x9581, 0 };
short ebcdic_mm[] = { 0xC2C9, 0xC785, 0x95C4, 0x8981, 0x95E2, 0xA8E2, 0 };
void _ebcdic() { char* s = (char*) ebcdic_mm; s = (char*) ebcdic_ii; }
int main() { _ascii (); _ebcdic (); return 0; }
EOF
] if test -f conftest.c ; then
     if ${CC-cc} -c conftest.c -o conftest.o && test -f conftest.o ; then
        if test `grep -l BIGenDianSyS conftest.o` ; then
           echo $ac_n ' big endian probe OK, ' 1>&AC_FD_MSG
	   ac_cv_c_bigendian=yes
        fi
        if test `grep -l LiTTleEnDian conftest.o` ; then
           echo $ac_n ' little endian probe OK, ' 1>&AC_FD_MSG
	   if test $ac_cv_c_bigendian = yes ; then
	    ac_cv_c_bigendian=unknown;
           else
            ac_cv_c_bigendian=no
           fi
        fi
	echo $ac_n 'guessing bigendian ...  ' >&AC_FD_MSG
     fi
  fi
AC_MSG_RESULT($ac_cv_c_bigendian)
fi
if test $ac_cv_c_bigendian = yes; then
  AC_DEFINE(WORDS_BIGENDIAN, 1, [whether byteorder is bigendian])
  BYTEORDER=4321
else
  BYTEORDER=1234
fi
AC_DEFINE_UNQUOTED(BYTEORDER, $BYTEORDER, [1234 = LIL_ENDIAN, 4321 = BIGENDIAN])
if test $ac_cv_c_bigendian = unknown; then
  AC_MSG_ERROR(unknown endianess - sorry, please pre-set ac_cv_c_bigendian)
fi
])

