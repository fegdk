/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_spline.h"

namespace fe
{
	
	//--------------------------------
	// catmull-rom spline
	//--------------------------------
	
	catmullRomSpline::catmullRomSpline (const vector3 &a, const vector3 &b, const vector3 &c, const vector3 &d)
	{
		mControlPoints[0] = a;
		mControlPoints[1] = b;
		mControlPoints[2] = c;
		mControlPoints[3] = d;
	}
	
	// takes 0..1 for entire spline
	vector3 catmullRomSpline::point( float t )
	{
		return vector3( 0, 0, 0 );
	}
	
	// takes 0..1 between points p1 and p2
	vector3 catmullRomSpline::point( int p1, int p2, float t )
	{
		int p0 = ( 0 == p1 ) ? 3 : ( p1 - 1 );
		int p3 = ( p2 == 3 ) ? 0 : ( p2 + 1 );
		float t2 = t * t;
		float t3 = t2 * t;
		vector3 v = 0.5f * ( ( 2.0f * mControlPoints[p1] ) + 
			( -mControlPoints[p0] + mControlPoints[p2] ) * t +
			( 2.0f * mControlPoints[p0] - 5 * mControlPoints[p1] + 4 * mControlPoints[p2] - mControlPoints[p3] ) * t2 +
			( -mControlPoints[p0] + 3 * mControlPoints[p1] - 3 * mControlPoints[p2] + mControlPoints[p3] ) * t3 );
		return v;
	}
	
	//--------------------------------
	// quadratic bezier spline
	//--------------------------------
	
	// takes 0..1 for entire spline
	vector3 quadraticBezierSpline::point( float t )
	{
		vector3 v;
		float t1 = t * t;
		float t2 = 2 * t * ( 1 - t );
		float t3 = ( 1 - t ) * ( 1 - t );
		v.x = mControlPoints[0].x * t1 + mControlPoints[1].x * t2 + mControlPoints[2].x * t3;
		v.y = mControlPoints[0].y * t1 + mControlPoints[1].y * t2 + mControlPoints[2].y * t3;
		v.z = mControlPoints[0].z * t1 + mControlPoints[1].z * t2 + mControlPoints[2].z * t3;
		return v;
	}
	
	//--------------------------------
	// cubic bezier spline
	//--------------------------------
	
	cubicBezierSpline::cubicBezierSpline (const vector3 &a, const vector3 &b, const vector3 &c, const vector3 &d)
	{
		mControlPoints[0] = a;
		mControlPoints[1] = b;
		mControlPoints[2] = c;
		mControlPoints[3] = d;
	}
	
	// takes 0..1 for entire spline
	vector3 cubicBezierSpline::point( float t )
	{
		vector3 v;
		float sqr_a = t * t;
		float cube_a = sqr_a * t;
		float sqr_b = ( 1 - t ) * ( 1 - t );
		float cube_b = sqr_b * ( 1 - t );
	
		float t1 = cube_a;
		float t2 = 3 * sqr_a * ( 1 - t );
		float t3 = 3 * t * sqr_b;
		float t4 = cube_b;
	
		v.x = mControlPoints[0].x * t1 + mControlPoints[1].x * t2 + mControlPoints[2].x * t3 + mControlPoints[3].x * t4;
		v.y = mControlPoints[0].y * t1 + mControlPoints[1].y * t2 + mControlPoints[2].y * t3 + mControlPoints[3].y * t4;
		v.z = mControlPoints[0].z * t1 + mControlPoints[1].z * t2 + mControlPoints[2].z * t3 + mControlPoints[3].z * t4;
	
		return v;
	}
	
}
