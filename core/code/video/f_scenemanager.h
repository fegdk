/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_SCENEMANAGER_H
#define __F_SCENEMANAGER_H

#include "f_baseobject.h"
#include "f_sceneobject.h"
#include "f_material.h"

namespace fe
{
	
	/**
	 * This class represents scene management system.
	 * You must populate scene with objects (lights, models, etc). Than (optionally) call update, and draw.
	 * Scene manager is responsible for culling non-visible geometry, updating, and adding visible objects to rendering queue than.
	 */
	class FE_API sceneManager : public baseObject
	{
	private:
	
		// for now - it's all stub
		smartPtr<sceneObject> mpSceneObjectList;
		const material *mpCurrentMtl;
	
	public:
	
		sceneManager (void);
		~sceneManager (void);
		
		/**
		 * Used to insert object into scene. Hierarchy of original object is kept intact.
		 * @param obj object to be inserted.
		 */
		void add (sceneObject *obj);
		void remove (sceneObject *obj);
	
		/**
		 * Used to animate scene, update effects, etc.
		 */
		void update (void);
		
		/**
		 * Used to draw current state of the scene.
		 */
		void draw (void);

		void setCurrentMtl (const material *mtl);
		const material* getCurrentMtl (void) const;
	
	};

	typedef smartPtr <sceneManager> sceneManagerPtr;
	
}

#endif // __F_SCENEMANAGER_H
