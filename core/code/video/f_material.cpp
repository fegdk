/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_material.h"
#include "f_engine.h"
#include "f_resourcemgr.h"
#include "metashader.h"
#include "f_baserenderer.h"

namespace fe
{
	
	material::material (const char *name)
	{
		mMetaShaderFlags = 0;
		fprintf (stderr, "WARNING: trying to load non-script mtl %s\n", name);
		mpEffect = NULL;
		mName = name;
		memset (&mMapParms, 0, sizeof (mMapParms));
		mMapParms.vertexScale = 1.0f;
		mMapParms.contents = 1;
		mMapParms.backsplashFraction = 0.05;
		mMapParms.backsplashDistance = 24;
		mNumTextures = mNumFloat4x4s = mNumFloat4s = mNumSurfaceParms = 0;
		load ();
	}
	
	material::material (charParser &parser, const char *name)
	{
		mMetaShaderFlags = 0;
		mpEffect = NULL;
		setName (name);
		memset (&mMapParms, 0, sizeof (mMapParms));
		mMapParms.vertexScale = 1.0f;
		mMapParms.contents = 1;
		mMapParms.backsplashFraction = 0.05;
		mMapParms.backsplashDistance = 24;
		mNumTextures = mNumFloat4x4s = mNumFloat4s = mNumSurfaceParms = 0;
		load (parser);
	}
	
	material::~material ()
	{
		if (mpEffect)
			delete mpEffect;
	}
	
	void	material::checkEffect (void)
	{
		return;
		if (!mpEffect)
		{
			mpEffect = new effectRef;
	
			// FIXME: effect must be chosen using mtl flags/hints
			effectPtr e = g_engine->getResourceMgr ()->createEffect ("effects/std/std.fx");
			mpEffect->fx = e;
	
			// setup inputs
			initEffectRef ();
		}
	}

	void material::setupAfterLoading (void)
	{
		//fprintf (stderr, "getting effect for mtl %s\n", mName.c_str ());
		// get metashader fx if appropriate
		if (!mpEffect)
		{
			if (g_engine->getRenderer ())
			{
				effectPtr e = g_engine->getMetaShader ()->getEffectForFlags (mMetaShaderFlags);
				if (!e)
				{
					fprintf (stderr, "WARNING: metashader fails for mtl '%s' (0x%X)\n", mName.c_str (), mMetaShaderFlags);
				}
				else
				{
					if (!mpEffect)
						mpEffect = new effectRef;
					mpEffect->clean ();
					mpEffect->fx = e;
					initEffectRef ();
				}
			}
		}
		// find internal techniques
		if (mpEffect)
		{
			checkEffect ();
			mpEffect->ambient = mpEffect->fx->findTechnique ("AMBIENT");
			mpEffect->pointlight = mpEffect->fx->findTechnique ("POINTLIGHT");
			mpEffect->directlight = mpEffect->fx->findTechnique ("DIRECTLIGHT");
			mpEffect->spotlight = mpEffect->fx->findTechnique ("SPOTLIGHT");

			//fprintf (stderr, "mtl %s refs: %d %d %d %d\n", name (), mpEffect->ambient, mpEffect->pointlight, mpEffect->directlight, mpEffect->spotlight);
			// fill stdmap bindings
			const char *names[] = {
				"map",
				"normalmap",
				"specularmap",
				"lightmap",
				NULL
			};
			for (int i = 0; names[i]; i++)
			{
				if (mStdMaps[i])
				{
					if (mNumTextures == FXREF_MAX_TEXTURES)
						sys_error ("max mtl texture limit exceeded loading of mtl '%s'", name ());
					texInput *efi = &mTextures[mNumTextures++];
					efi->name = names[i];
					efi->texture = mStdMaps[i];
					checkEffect ();
					mpEffect->textures[mpEffect->numtextures++] = mpEffect->fx->varIndexByName (efi->name);
				}
			}
		}
	}
	
	baseTexturePtr material::loadMap (const char *name, charParser &p)
	{
		if (mNumTextures == FXREF_MAX_TEXTURES)
			sys_error ("max mtl texture limit exceeded loading of mtl '%s'", this->name ());
		texInput *efi = &mTextures[mNumTextures++];
		efi->name = name;
		p.getToken ();
		efi->texture = g_engine->getResourceMgr ()->createTexture (p.token ());
	
		checkEffect ();
	
		mpEffect->textures[mpEffect->numtextures++] = mpEffect->fx->varIndexByName (efi->name);
		return efi->texture;
	}
	
	baseTexturePtr material::loadStdMap (charParser &p)
	{
		p.getToken ();
		return g_engine->getResourceMgr ()->createTexture (p.token ());
	}
	
	void material::load (charParser &p)
	{
		p.matchToken ("{");
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
				break;
			else if (!p.cmptoken ("surfaceparm"))
			{
				if (mNumSurfaceParms == MTL_MAX_SURFACE_PARMS)
					sys_error ("mtl surfparm limit exceeded during load '%s'", name ());
				p.getToken ();
//				if (cvar_r_loadsurfaceparms.ivalue)
				{
					mSurfaceParms[mNumSurfaceParms++] = p.token ();
				}
			}
			else if (!p.cmptoken ("effectfile") || !p.cmptoken ("fx"))
			{
				p.getToken ();
				if (g_engine->getRenderer ())
				{
					if (!mpEffect)
						mpEffect = new effectRef;
					mpEffect->clean ();
	
					effectPtr e = g_engine->getResourceMgr ()->createEffect (p.token ());
					mpEffect->fx = e;
	
					// setup inputs
					initEffectRef ();
				}
			}
			else if (!p.cmptoken ("float3"))
			{
				if (mNumFloat4s == FXREF_MAX_FLOAT4S)
					sys_error ("mtl float4 limit exceeded during load '%s'", name ());
				p.getToken ();
				float4Input *i = &mFloat4s[mNumFloat4s++];
				i->name = p.token ();
				cStr e;
				// read to EOL
				do {
					p.getToken ();
					e += p.token ();
				} while (!p.isEOL ());
	
				const char *s = e.c_str ();
				cStr expr;
				while (*s && *s != ',')
				{
					expr += *s;
					s++;
				}
				if (*s != ',')
					p.generalSyntaxError ();
				mathExpressionPtr u = new mathExpression (expr);
	
				s++;
				expr = "";
	
				while (*s && *s != ',')
				{
					expr += *s;
					s++;
				}
	
				mathExpressionPtr v = new mathExpression (expr);
	
				if (*s != ',')
					p.generalSyntaxError ();
				s++;
	
				mathExpressionPtr w = new mathExpression (s);
	
				mtlFloat4BasePtr f = new mtlFloat3 (u, v, w);
				i->value = f;
				checkEffect ();
				mpEffect->float4s[mpEffect->numfloat4s++] = mpEffect->fx->varIndexByName (i->name.c_str ());
			}
			else if (!p.cmptoken ("float4"))
			{
				if (mNumFloat4s == FXREF_MAX_FLOAT4S)
					sys_error ("mtl float4 limit exceeded during load '%s'", name ());
				p.getToken ();
				float4Input *i = &mFloat4s[mNumFloat4s++];
				i->name = p.token ();
				cStr e;
				do {
					p.getToken ();
					e += p.token ();
				} while (!p.isEOL ());
				mathExpressionPtr expr = new mathExpression (e);
				mtlFloat4BasePtr f = new mtlFloat4 (expr);
				i->value = f;
				checkEffect ();
				mpEffect->float4s[mpEffect->numfloat4s++] = mpEffect->fx->varIndexByName (i->name);
			}
			else if (!p.cmptoken ("float4x4"))
			{
				if (mNumFloat4x4s == FXREF_MAX_FLOAT4X4S)
					sys_error ("mtl float4x4 limit exceeded during loading '%s'", name ());
				p.getToken ();
				float4x4Input *mtx = &mFloat4x4s[mNumFloat4x4s++];
				mtx->name = p.token ();
				p.getToken ();
				if (!p.cmptoken ("scroll"))
				{
					cStr e;
					// read to EOL
					do {
						p.getToken ();
						e += p.token ();
					} while (!p.isEOL ());
	
					const char *s = e.c_str ();
					cStr expr;
					while (*s && *s != ',')
					{
						expr += *s;
						s++;
					}
					if (*s != ',')
						p.generalSyntaxError ();
					mathExpressionPtr u = new mathExpression (expr);
	
					s++;
	
					mathExpressionPtr v = new mathExpression (s);
					mtlMat4x4Ptr scroll = new mat4x4Scroll (u, v);
					mtx->value = scroll;
					checkEffect ();
					mpEffect->float4x4s[mpEffect->numfloat4x4s++] = mpEffect->fx->varIndexByName (mtx->name);
				}
				else if (!p.cmptoken ("scale"))
				{
					cStr e;
					// read to EOL
					do {
						p.getToken ();
						e += p.token ();
					} while (!p.isEOL ());
	
					const char *s = e.c_str ();
					cStr expr;
					while (*s && *s != ',')
					{
						expr += *s;
						s++;
					}
					if (*s != ',')
						p.generalSyntaxError ();
					mathExpressionPtr u = new mathExpression (expr);
	
					s++;
	
					mathExpressionPtr v = new mathExpression (s);
					mtlMat4x4Ptr scale = new mat4x4Scale (u, v);
					mtx->value = scale;
					checkEffect ();
					mpEffect->float4x4s[mpEffect->numfloat4x4s++] = mpEffect->fx->varIndexByName (mtx->name);
				}
				else if (!p.cmptoken ("rotate"))
				{
					cStr e;
					// read to EOL
					do {
						p.getToken ();
						e += p.token ();
					} while (!p.isEOL ());
	
					const char *s = e.c_str ();
					mathExpressionPtr r = new mathExpression (s);
					mtlMat4x4Ptr rotate = new mat4x4Rotate (r);
					mtx->value = rotate;
					checkEffect ();
					mpEffect->float4x4s[mpEffect->numfloat4x4s] = mpEffect->fx->varIndexByName (mtx->name);
				}
				else
					p.generalSyntaxError ();
			}
			// parse standard maps
			else if (!p.cmptoken ("map"))
			{
				mStdMaps[map] = loadStdMap (p);
				mMetaShaderFlags |= MS_USE_MAP;
			}
			else if (!p.cmptoken ("normalmap"))
			{
				mStdMaps[normalmap] = loadStdMap (p);
				mMetaShaderFlags |= MS_USE_NORMALMAP;
			}
			else if (!p.cmptoken ("specularmap"))
			{
				mStdMaps[specularmap] = loadStdMap (p);
				mMetaShaderFlags |= MS_USE_SPECULARMAP;
			}
			else if (!p.cmptoken ("lightmap"))
			{
				mStdMaps[lightmap] = loadStdMap (p);
				mMetaShaderFlags |= MS_USE_LIGHTMAP;
			}
			else if (!p.cmptoken ("nocull"))
			{
				mMetaShaderFlags |= MS_NOCULL;
			}
			else if (!p.cmptoken ("nolight"))
			{
				mMetaShaderFlags |= MS_NOLIGHT;
			}
			else if (!p.cmptoken ("nospecular"))
			{
				mMetaShaderFlags |= MS_NOSPECULAR;
			}
			else if (!p.cmptoken ("ed_editorimage"))
			{
				p.getToken ();
				mMapParms.editorimage = p.token ();
			}
			else
				p.generalSyntaxError ();
		}
		setupAfterLoading ();
	}
	void material::load (void)
	{
		cStr fname  = mName;
		charParser p (g_engine->getFileSystem (), fname + ".fmtl", g_engine->getFileSystem () ? false : true);
		p.errMode (0);
		if (!p.getToken ()) {
			p.errMode (1);
			load (p);
		}
		else
		{
			// load mtl as a single texture
			mStdMaps[map] = g_engine->getResourceMgr ()->createTexture (fname);
			mMetaShaderFlags |= MS_USE_MAP | MS_NOLIGHT;
			setupAfterLoading ();
		}
	}
	
	void	material::initEffectRef (void)
	{
		size_t i;
		for (i = 0; i < mNumTextures; i++)
		{
			mpEffect->textures[mpEffect->numtextures++] = mpEffect->fx->varIndexByName (mTextures[i].name);
		}
		for (i = 0; i < mNumFloat4x4s; i++)
		{
			mpEffect->float4x4s[mpEffect->numfloat4x4s++] = mpEffect->fx->varIndexByName (mFloat4x4s[i].name);
		}
		for (i = 0; i < mNumFloat4s; i++)
		{
			mpEffect->float4s[mpEffect->numfloat4s++] = mpEffect->fx->varIndexByName (mFloat4s[i].name);
		}
	}
	
	void	material::setupEffectInputs ()
	{
		checkEffect ();
	
		size_t i;
		for (i = 0; i < mpEffect->numtextures; i++)
		{
			if (-1 == mpEffect->textures[i])
				continue;
			mpEffect->fx->setTexture (mpEffect->textures[i], mTextures[i].texture);
		}
		for (i = 0; i < mpEffect->numfloat4x4s; i++)
		{
			if (-1 == mpEffect->float4x4s[i])
				continue;
			matrix4 &m = mpEffect->fx->getMatrix (mpEffect->float4x4s[i]);
			m.identity ();
		}
		for (i = 0; i < mpEffect->numfloat4x4s; i++)
		{
			if (-1 == mpEffect->float4x4s[i])
				continue;
			matrix4 &m = mpEffect->fx->getMatrix (mpEffect->float4x4s[i]);
			m = m * mFloat4x4s[i].value->get ();
		}
		for (i = 0; i < mpEffect->numfloat4s; i++)
		{
			if (-1 == mpEffect->float4s[i])
				continue;
			float4& f = mpEffect->fx->getFloat4 (mpEffect->float4s[i]);
			f = mFloat4s[i].value->get ();
		}
	}
	
	baseTexturePtr			material::getTexture (int index) const
	{
	    if ( (size_t)index >= mNumTextures)
			return NULL;
		else
			return mTextures[index].texture;
	}
	
	effectPtr			material::getEffect (void) const
	{
		return mpEffect ? mpEffect->fx : NULL;
	}
	
	mat4x4Scroll::mat4x4Scroll (const mathExpressionPtr &u, const mathExpressionPtr &v)
	{
		mExprU = u;
		mExprV = v;
	}
	
	matrix4	mat4x4Scroll::get (void)
	{
		matrix4 m (true);
		m._41 = mExprU->evaluate ();
		m._42 = mExprV->evaluate ();
		return m;
	}
	
	
	mat4x4Scale::mat4x4Scale (const mathExpressionPtr &u, const mathExpressionPtr &v)
	{
		mExprU = u;
		mExprV = v;
	}
	
	matrix4	mat4x4Scale::get (void)
	{
		matrix4 m (true);
		m._11 = mExprU->evaluate ();
		m._22 = mExprV->evaluate ();
		return m;
	}
	
	
	mtlFloat3::mtlFloat3 (const mathExpressionPtr &x, const mathExpressionPtr &y, const mathExpressionPtr &z)
	{
		mExprX = x;
		mExprY = y;
		mExprZ = z;
	}
	
	float4	mtlFloat3::get (void)
	{
		float x = mExprX->evaluate ();
		float y = mExprY->evaluate ();
		float z = mExprZ->evaluate ();
		return float4 (x, y, z, 1);
	}
	
	mtlFloat4::mtlFloat4 (const mathExpressionPtr &e)
	{
		mExpr = e;
	}
	
	float4	mtlFloat4::get (void)
	{
		return mExpr->evaluate ();
	}
	
	mat4x4Rotate::mat4x4Rotate (const mathExpressionPtr &angle)
	{
		mExprAngle = angle;
	}
	
	matrix4	mat4x4Rotate::get (void)
	{
		matrix4 mrot (true);
		float angle = mExprAngle->evaluate ();
	
		float sint = (float)sin (angle);
		float cost = (float)cos (angle);
	
		mrot._11 = cost;
		mrot._12 = sint;
		mrot._21 = -sint;
		mrot._22 = cost;
	
		return mrot;
	}
	
	baseTexturePtr	material::getStdMap (material::stdmap idx) const
	{
		return mStdMaps[idx];
	}
	
	material::effectRef*		material::getEffectRef (void) const
	{
		return mpEffect;
	}
	
	int material::getNumSurfaceParms (void) const
	{
		return mNumSurfaceParms;
	}

	const char *material::getSurfaceParm (int parm) const
	{
		return mSurfaceParms[parm].c_str ();
	}

	const mtlMapParms_t *material::getMapParms (void) const
	{
		return &mMapParms;
	}
}
