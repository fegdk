/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_effectparms.h"

namespace fe
{
	
	effectParms::effectParms (void)
	{
		for (int i = 0; i < effectParm_Last; i++)
	        mValidParms[i] = false;
	}
	
	effectParms::~effectParms (void)
	{
	}
	
	const matrix4&	effectParms::getMatrix (effectParm_t index)
	{
		if (!mValidParms[index])
		{
			// check if we can derive this from valid parms
			switch (index)
			{
			case effectParm_WorldViewProjMatrix:
				{
					setMatrix (effectParm_WorldViewProjMatrix, (* ((matrix4 *)mParms[effectParm_WorldMatrix].matrix)) *
						 (* ((matrix4 *)mParms[effectParm_ViewMatrix].matrix)) *
						 (* ((matrix4 *)mParms[effectParm_ProjMatrix].matrix)));
				}
				break;
			case effectParm_WorldViewMatrix:
				{
					setMatrix (effectParm_WorldViewMatrix, (* ((matrix4 *)mParms[effectParm_WorldMatrix].matrix)) *
						 (* ((matrix4 *)mParms[effectParm_ViewMatrix].matrix)));
				}
				break;
			case effectParm_InvWorldMatrix:
				{
					setMatrix (effectParm_InvWorldMatrix, (* ((matrix4 *)mParms[effectParm_WorldMatrix].matrix)).inverse ());
				}
				break;
			case effectParm_InvViewMatrix:
				{
					setMatrix (effectParm_InvViewMatrix, (* ((matrix4 *)mParms[effectParm_ViewMatrix].matrix)).inverse ());
				}
				break;
			case effectParm_ViewToModelMatrix:
				{
					setMatrix (effectParm_ViewToModelMatrix, getMatrix (effectParm_WorldViewMatrix).inverse ());
				}
				break;
			}
		}
		return * ((matrix4 *)mParms[index].matrix);
	}
	
	baseTexturePtr	effectParms::getTexture (effectParm_t index)
	{
		return mParms[index].texture;
	}
	
	const float4&	effectParms::getFloat4 (effectParm_t index)
	{
		if (!mValidParms[index])
		{
			switch (index)
			{
			case effectParm_EyePosition:
				{
					const matrix4 &ivm = getMatrix (effectParm_InvViewMatrix);
					setFloat4 (effectParm_EyePosition, (float4 &)ivm._41);
				}
				break;
			case effectParm_EyeDirection:
				{
					const matrix4 &ivm = getMatrix (effectParm_InvViewMatrix);
					setFloat4 (effectParm_EyePosition, (float4 &)ivm._31);
				}
				break;
			}
		}
		return * ((float4 *)mParms[index].vector);
	}
	
	void	effectParms::setMatrix (effectParm_t index, const matrix4 &m)
	{
		mValidParms[index] = true;
		 (* ((matrix4 *)mParms[index].matrix)) = m;
		invalidateDependents (index);
	}
	
	void	effectParms::setFloat4 (effectParm_t index, const float4 &v)
	{
		mValidParms[index] = true;
		 (* ((float4 *)mParms[index].vector)) = v;
	}
	
	void	effectParms::setFloat4 (effectParm_t index, const vector3 &v)
	{
		mValidParms[index] = true;
		 (* ((vector3 *)mParms[index].vector)) = v;
		mParms[index].vector[3] = 1.f;
	}
	
	void	effectParms::setTexture (effectParm_t index, const baseTexturePtr &tex)
	{
		mValidParms[index] = true;
		mParms[index].texture = tex;
	}
	
	void	effectParms::invalidateDependents (effectParm_t index)
	{
		switch (index)
		{
		case effectParm_WorldMatrix:
			mValidParms[effectParm_InvWorldMatrix] = false;
			mValidParms[effectParm_WorldViewProjMatrix] = false;
			mValidParms[effectParm_WorldViewMatrix] = false;
			mValidParms[effectParm_ViewToModelMatrix] = false;
			break;
		case effectParm_ViewMatrix:
			mValidParms[effectParm_InvViewMatrix] = false;
			mValidParms[effectParm_WorldViewProjMatrix] = false;
			mValidParms[effectParm_WorldViewMatrix] = false;
			mValidParms[effectParm_ViewToModelMatrix] = false;
			mValidParms[effectParm_EyePosition] = false;
			mValidParms[effectParm_EyeDirection] = false;
		case effectParm_ProjMatrix:
			mValidParms[effectParm_WorldViewProjMatrix] = false;
			break;
		}
	}

}
