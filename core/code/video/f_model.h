/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_MODEL_H
#define __F_MODEL_H

#include "f_types.h"
#include "f_math.h"
#include "f_parser.h"
#include "f_sceneobject.h"
#include "f_material.h"

namespace fe
{
	
	class baseTexture;
	class file;
	class lightSource;
	
	struct silEdge_t
	{
		ushort f1, f2;		// faces forming the edge	
		ushort v1, v2;		// verts forming the edge
	};
	
	struct shadowVertex_t
	{
		vector4 xyzw;
	};
	
	struct modelTriangles_t
	{
		// this stuff is loaded externally
		int32 numVerts;
		drawVertex_t* verts;
		int32 numIndexes;
		ushort* indexes;

		// this stuff is build load-time
//		char* mtlIndexes;
		ushort* silIndexes;  // not using duplicated verts (ignoring texcoords, etc)
		plane* facePlanes;
		int32 numSilEdges;
		silEdge_t* silEdges;	// size must be at least [numVerts*2] (not including duplicates)
								// FIXME find a way to cache results for static light/mesh combos
		
		shadowVertex_t *shadowVerts;	// this is NULL when using vertex shaders
		int32 numShadowVolumeIndexes;
		ushort* shadowVolumeIndexes;	// size at least equal to silEdges
		int32 numDupVerts;
		ushort* dupVerts; // pairs {vertex, dupvertex}
	};
	
	struct modelSubset_t
	{
		modelTriangles_t* tris;
		int32 firstVertex;
		int32 firstFace;
		int32 numVerts;
		int32 numFaces;
		materialPtr mtl;
		baseTexturePtr lightmap;
	};
	
//	class modelData;
	struct modelMtlList
	{
	public:
		materialPtr *list;
		int cnt;
		modelMtlList (void);
		~modelMtlList (void);
		void load (file *f);
	};
	
	
	class FE_API model : public geomSource
	{
		friend class modelData;
	protected:

		// !!! moved to meshData_t !!! (see geomSource)
//		unsigned mbLightmaps : 1;
//		unsigned mbTangents : 1;
//		unsigned mbColors : 1;
//		modelTriangles_t                  mTris;
//		std::vector <modelSubset_t>       mSurfaces;
	private:
	
//		smartPtr <modelData> mpData;
	
		sceneObject* load (file *f, modelMtlList &mtls, sceneObject *parent = NULL);
		
		/**
		 * @brief builds all data which is not being loaded from file/memory storage.
		 * @brief initializes mTris, obb, builds tangents, etc.
		 */
		void init (void);
	
	public:
	
		model (void);
		sceneObject* load (const char *fname);
		void loadMesh (file *f, int majorver, int minorver, modelMtlList &mtls, int numSubsets, renderable *rend);

		~model (void);
	
		/**
		 * @brief ctor used to create model from data in memory.
		 * @brief may be used for e.g. procedural geometry or testing purposes
		 * @brief which require better performance
		 * @param verts vertices
		 * @param inds indexes
		 * @param subs subsets
		 * @parms mtls material list, used in creation of mtlBlock
		 */
//		model (const char *name, model *parent, const matrix4 &mtx, const std::vector <drawVertex_t> &verts, std::vector <ushort> &inds, const std::vector <modelSubset_t> &subs, const std::vector <materialPtr> &mtls);

	};
	
	typedef smartPtr <model> modelPtr;
	
}

#endif // __F_MODEL_H

