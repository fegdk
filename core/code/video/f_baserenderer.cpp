#include "pch.h"
#include "f_baserenderer.h"
#include "f_fragileobject.h"
#include "f_engine.h"
#include "f_resourcemgr.h"
#include "cvars.h"

namespace fe
{
	cvar_t *r_fullscreen;
	cvar_t *r_vidmode;
	cvar_t *r_show_tris;
	cvar_t *r_loadsurfaceparms;
	cvar_t *r_use_vbo;
	cvar_t *r_texture_filtering;
	cvar_t *r_use_mips;
	cvar_t *r_metashader;

	static void
	R_Init (void)
	{
		cvarManager *cvars = g_engine->getCVarManager ();
		
		r_fullscreen = cvars->get ("r_fullscreen", "0", CVAR_STORE | CVAR_LATCH);
		r_vidmode = cvars->get ("r_vidmode", "-1", CVAR_STORE | CVAR_LATCH);
		r_show_tris = cvars->get ("r_show_tris", "0", CVAR_STORE);
		r_loadsurfaceparms = cvars->get ("r_loadsurfaceparms", "0", CVAR_STORE);
		r_use_vbo = cvars->get ("r_use_vbo", "1", CVAR_STORE);
		r_texture_filtering = cvars->get ("r_texture_filtering", "1", CVAR_STORE | CVAR_LATCH);
		r_use_mips = cvars->get ("r_use_mips", "1", CVAR_STORE | CVAR_LATCH);
		r_metashader = cvars->get ("r_metashader", "materials/metainfo.txt", CVAR_STORE | CVAR_READONLY);
	}

	baseRenderer::baseRenderer (void)
	{
		R_Init ();
		mpFragileObjectRegistry = new fragileObjectRegistry;
	}
	
	baseRenderer::~baseRenderer (void)
	{
		mpFragileObjectRegistry->looseAll();
		if ( mpFragileObjectRegistry )
		{
			delete mpFragileObjectRegistry;
			mpFragileObjectRegistry = NULL;
		}
	}
	
	fragileObjectRegistry*	baseRenderer::fragileRegistry( void ) const
	{
		return mpFragileObjectRegistry;
	}
	
	void baseRenderer::reset (void)
	{
		R_Init ();
		mpFragileObjectRegistry->looseAll();
		resize ();
		mpFragileObjectRegistry->restoreAll ();
	}
	
}
