#ifndef __METASHADER_H
#define __METASHADER_H

#include "f_baseobject.h"
#include "f_effect.h"

namespace fe
{
	enum
	{
		MS_USE_MAP = 0x00000001,
		MS_USE_NORMALMAP = 0x00000002,
		MS_USE_SPECULARMAP = 0x00000003,
		MS_USE_LIGHTMAP = 0x00000004,
		MS_USE_TCMOD = 0x00000008,
		MS_NOLIGHT = 0x00000010,
		MS_NOCULL = 0x00000020,
		MS_NOSPECULAR = 0x00000040
	};


	class metaShader : public baseObject
	{
	private:
		typedef std::map <uint32, smartPtr <effect> > effectMap_t;
		effectMap_t mEffectMap;
	public:
		metaShader (const char *fname);
		~metaShader (void);

		smartPtr <effect> getEffectForFlags (uint32 flags) const;

	};
}

#endif // __METASHADER_H

