/*
ps14toATIfs - parses and wraps DirectX 8.1 pixel shaders version 1.4 to
              OpenGL using the ATI_fragment_shader extension.
Copyright (C) 2003  Shawn Kirst

For conditions of distribution and use, see copyright notice in ps14toATIfs.h
*/

#ifndef __PS14_ERRORS_H
#define __PS14_ERRORS_H

void ps14_errors_clear(void);
int ps14_error_count(void);
const char *ps14_error_get(int num);
void ps14_error_add(const char *fmt, ...);

#endif
