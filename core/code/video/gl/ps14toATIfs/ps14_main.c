/*
ps14toATIfs - parses and wraps DirectX 8.1 pixel shaders version 1.4 to
              OpenGL using the ATI_fragment_shader extension.
Copyright (C) 2003  Shawn Kirst

For conditions of distribution and use, see copyright notice in ps14toATIfs.h
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ps14_errors.h"

int ps14_init(char *);
int ps14_parse(void);

extern int ps14_line_number;

int ps14toATIFragmentShader(const char *program)
{
   int rc = -1;
   char *instr;
   
   ps14_errors_clear();
   ps14_line_number = 0;
   
   if(program == 0 || *program == 0)
      return(-1);
   
   instr = strdup(program);
   if(instr == 0) return(-1);
      
   if(ps14_init(instr))
      rc = ps14_parse();
   
   free(instr);
   
#ifdef PS14_DEBUG
   printf("errors: %d\n", ps14_error_count());
#endif  
      
   return((rc != 0) || ps14_error_count());
}
   
int ps14toATIfs_GetErrorCount(void)
{
   return(ps14_error_count());
}
   
const char *ps14toATIfs_GetErrorString(int errnum)
{
   if(errnum >= ps14_error_count()) return(0);
   return(ps14_error_get(errnum));
}
