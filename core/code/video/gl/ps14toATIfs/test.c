/*
ps14toATIfs - parses and wraps DirectX 8.1 pixel shaders version 1.4 to
              OpenGL using the ATI_fragment_shader extension.
Copyright (C) 2003  Shawn Kirst

For conditions of distribution and use, see copyright notice in ps14toATIfs.h
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/glext.h>
#include <GL/glx.h>

#include "ati.h"
#include "ps14toATIfs.h"

PFNGLGENFRAGMENTSHADERSATIPROC glGenFragmentShadersATI = 0;
PFNGLBINDFRAGMENTSHADERATIPROC glBindFragmentShaderATI = 0;
PFNGLDELETEFRAGMENTSHADERATIPROC glDeleteFragmentShaderATI = 0;
PFNGLBEGINFRAGMENTSHADERATIPROC glBeginFragmentShaderATI = 0;
PFNGLENDFRAGMENTSHADERATIPROC glEndFragmentShaderATI = 0;
PFNGLPASSTEXCOORDATIPROC glPassTexCoordATI = 0;
PFNGLSAMPLEMAPATIPROC glSampleMapATI = 0;
PFNGLCOLORFRAGMENTOP1ATIPROC glColorFragmentOp1ATI = 0;
PFNGLCOLORFRAGMENTOP2ATIPROC glColorFragmentOp2ATI = 0;
PFNGLCOLORFRAGMENTOP3ATIPROC glColorFragmentOp3ATI = 0;
PFNGLALPHAFRAGMENTOP1ATIPROC glAlphaFragmentOp1ATI = 0;
PFNGLALPHAFRAGMENTOP2ATIPROC glAlphaFragmentOp2ATI = 0;
PFNGLALPHAFRAGMENTOP3ATIPROC glAlphaFragmentOp3ATI = 0;
PFNGLSETFRAGMENTSHADERCONSTANTATIPROC glSetFragmentShaderConstantATI = 0;
/*
const char test_shader[]=
   "ps.1.4\n"
   "def        c0, 1, 0.5, 0.25, 0\n"
   "texcrd     r0, t0\n"
   "dp3_sat    r0.rgb, r0, v0\n"
   "+mov_sat   r0.a, c0.r\n";
*/
const char test_shader[]=
   "ps.1.4\n"
   "def        c0, 1, 0.5, 0, 0\n"
   "texld      r0, t0\n"
   "mov        r2, v0_bx2\n"
   "mul        r1, -r2, r2\n"
   "add        r0, r0, r1\n"
   "+add_x4    r0.a, r2.b, r2.b\n"
   "mov        r0.rgb, c0.r\n"
   "+mul       r0.a, r0.a, r0.b\n";

GLuint shader;

void init(void)
{
   int i,rc,errc;
   
   if(!strstr((char*)glGetString(GL_EXTENSIONS),"GL_ATI_fragment_shader"))
   {
      fprintf(stderr,"ATI_fragment_shader required!\n");
      exit(-1);
   }
   
#define GLPROC(type,func) (func) = (type)glXGetProcAddressARB((GLubyte*)#func)
   
   GLPROC(PFNGLGENFRAGMENTSHADERSATIPROC,glGenFragmentShadersATI);
   GLPROC(PFNGLBINDFRAGMENTSHADERATIPROC,glBindFragmentShaderATI);
   GLPROC(PFNGLDELETEFRAGMENTSHADERATIPROC,glDeleteFragmentShaderATI);
   GLPROC(PFNGLBEGINFRAGMENTSHADERATIPROC,glBeginFragmentShaderATI);
   GLPROC(PFNGLENDFRAGMENTSHADERATIPROC,glEndFragmentShaderATI);
   GLPROC(PFNGLPASSTEXCOORDATIPROC,glPassTexCoordATI);
   GLPROC(PFNGLSAMPLEMAPATIPROC,glSampleMapATI);
   GLPROC(PFNGLCOLORFRAGMENTOP1ATIPROC,glColorFragmentOp1ATI);
   GLPROC(PFNGLCOLORFRAGMENTOP2ATIPROC,glColorFragmentOp2ATI);
   GLPROC(PFNGLCOLORFRAGMENTOP3ATIPROC,glColorFragmentOp3ATI);
   GLPROC(PFNGLALPHAFRAGMENTOP1ATIPROC,glAlphaFragmentOp1ATI);
   GLPROC(PFNGLALPHAFRAGMENTOP2ATIPROC,glAlphaFragmentOp2ATI);
   GLPROC(PFNGLALPHAFRAGMENTOP3ATIPROC,glAlphaFragmentOp3ATI);
   GLPROC(PFNGLSETFRAGMENTSHADERCONSTANTATIPROC,glSetFragmentShaderConstantATI);
  
#undef GLPROC

   glEnable(GL_FRAGMENT_SHADER_ATI);
   
   shader = glGenFragmentShadersATI(1);
   glBindFragmentShaderATI(shader);
   
   glBeginFragmentShaderATI();
   rc = ps14toATIFragmentShader(test_shader);
   glEndFragmentShaderATI();
   
   if(rc)
   {
      errc = ps14toATIfs_GetErrorCount();
      for(i = 0; i < errc; ++i)
         fprintf(stderr,"%s\n",ps14toATIfs_GetErrorString(i));
   }
}

void display(void)
{
   glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
   
   glBegin(GL_QUADS);
   {
      glColor3f(0.3f,0.59f,0.11f);
      glTexCoord3f(1,0,0);
      glVertex3f(-1,1,0);
      glTexCoord3f(0,1,0);
      glVertex3f(1,1,0);
      glTexCoord3f(0,0,1);
      glVertex3f(1,-1,0);
      glTexCoord3f(1,1,0);
      glVertex3f(-1,-1,0);
   }
   glEnd();
   
   glutSwapBuffers();
}

void reshape(int w, int h)
{
   glViewport(0,0,w,h);
   
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(90,(float)w/(float)h,1,100);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glTranslatef(0,0,-2);
}

void keyboard(unsigned char key, int x, int y)
{
   if(key == 27) exit(0);
}

int main(int argc, char **argv)
{
   glutInit(&argc,argv);
   glutInitDisplayString("rgba depth double");
   glutInitWindowSize(500,500);
   glutCreateWindow("ps14toATIfs test");
   
   init();
   
   glutDisplayFunc(display);
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   
   glutMainLoop();
   
   return(0);
}
