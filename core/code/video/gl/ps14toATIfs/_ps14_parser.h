/* A Bison parser, made by GNU Bison 1.875d.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     HEADER = 258,
     NEWLINE = 259,
     PHASE = 260,
     INVERT = 261,
     NEGATE = 262,
     NUMBER = 263,
     REG = 264,
     DEF = 265,
     ADDROP = 266,
     BLENDOP = 267,
     SRCMOD = 268,
     TEXMOD = 269
   };
#endif
#define HEADER 258
#define NEWLINE 259
#define PHASE 260
#define INVERT 261
#define NEGATE 262
#define NUMBER 263
#define REG 264
#define DEF 265
#define ADDROP 266
#define BLENDOP 267
#define SRCMOD 268
#define TEXMOD 269




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 24 "ps14_grammar.y"
typedef union YYSTYPE {
	int ival;
	float fval;
   
	char cval[8];
	char *sval;
	constdef *cdef;
	instruction *line;
} YYSTYPE;
/* Line 1285 of yacc.c.  */
#line 75 "_ps14_parser.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE ps14_lval;



