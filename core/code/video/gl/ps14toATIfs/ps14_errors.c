/*
ps14toATIfs - parses and wraps DirectX 8.1 pixel shaders version 1.4 to
              OpenGL using the ATI_fragment_shader extension.
Copyright (C) 2003  Shawn Kirst

For conditions of distribution and use, see copyright notice in ps14toATIfs.h
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#define MAX_ERRORS 32

static char *errors[MAX_ERRORS + 1];
static int error_count = 0;

void ps14_errors_clear(void)
{
   int i;
   
   for(i = 0; i < error_count; ++i)
      free(errors[i]);
   
   error_count = 0;
}

int ps14_error_count(void)
{
   return(error_count);
}

const char *ps14_error_get(int num)
{
   if(num < error_count)
      return(errors[num]);
   return(0);
}

void ps14_error_add(const char *fmt, ...)
{
   static char buf[256];
   va_list args;
   
   if(error_count < MAX_ERRORS)
   {
      va_start(args, fmt);
      vsnprintf(buf, sizeof(buf), fmt, args);
      va_end(args);
      
      errors[error_count] = strdup(buf);
      ++error_count;
   }
}
