/*
ps14toATIfs - parses and wraps DirectX 8.1 pixel shaders version 1.4 to
              OpenGL using the ATI_fragment_shader extension.
Copyright (C) 2003  Shawn Kirst

For conditions of distribution and use, see copyright notice in ps14toATIfs.h
*/

#include "ps14_program.h"

#include <GL/glew.h>
#if 0
#ifdef _WIN32
# include <windows.h>
#endif
#include <GL/gl.h>
#include <GL/glext.h>
#endif
#include "ati.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "ps14_errors.h"

#if 0
/* ATI_fragment_shader must be initialized in the application linked to */
extern PFNGLPASSTEXCOORDATIPROC glPassTexCoordATI;
extern PFNGLSAMPLEMAPATIPROC glSampleMapATI;
extern PFNGLCOLORFRAGMENTOP1ATIPROC glColorFragmentOp1ATI;
extern PFNGLCOLORFRAGMENTOP2ATIPROC glColorFragmentOp2ATI;
extern PFNGLCOLORFRAGMENTOP3ATIPROC glColorFragmentOp3ATI;
extern PFNGLALPHAFRAGMENTOP1ATIPROC glAlphaFragmentOp1ATI;
extern PFNGLALPHAFRAGMENTOP2ATIPROC glAlphaFragmentOp2ATI;
extern PFNGLALPHAFRAGMENTOP3ATIPROC glAlphaFragmentOp3ATI;
extern PFNGLSETFRAGMENTSHADERCONSTANTATIPROC glSetFragmentShaderConstantATI;
#endif

static int pass = 0;

/* keep track of register use for 2 pass shaders */
static int reg_preserve[6];

static struct 
{
   char *name;
   GLenum constant;
} constant_table[] =
{
   {"r0", GL_REG_0_ATI},
   {"r1", GL_REG_1_ATI},
   {"r2", GL_REG_2_ATI},
   {"r3", GL_REG_3_ATI},
   {"r4", GL_REG_4_ATI},
   {"r5", GL_REG_5_ATI},
   
   {"t0", GL_TEXTURE0_ARB},
   {"t1", GL_TEXTURE1_ARB},
   {"t2", GL_TEXTURE2_ARB},
   {"t3", GL_TEXTURE3_ARB},
   {"t4", GL_TEXTURE4_ARB},
   {"t5", GL_TEXTURE5_ARB},
   {"t6", GL_TEXTURE6_ARB},
   {"t7", GL_TEXTURE7_ARB},
   
   {"c0", GL_CON_0_ATI},
   {"c1", GL_CON_1_ATI},
   {"c2", GL_CON_2_ATI},
   {"c3", GL_CON_3_ATI},
   {"c4", GL_CON_4_ATI},
   {"c5", GL_CON_5_ATI},
   {"c6", GL_CON_6_ATI},
   {"c7", GL_CON_7_ATI},
   
   {"v0", GL_PRIMARY_COLOR_ARB},
   {"v1", GL_SECONDARY_INTERPOLATOR_ATI},

   {"add", GL_ADD_ATI},
   {"cmp", GL_CND0_ATI},
   {"cnd", GL_CND_ATI},
   {"d2a", GL_DOT2_ADD_ATI},
   {"dp3", GL_DOT3_ATI},
   {"dp4", GL_DOT4_ATI},
   {"lrp", GL_LERP_ATI},
   {"mad", GL_MAD_ATI},
   {"mov", GL_MOV_ATI},
   {"mul", GL_MUL_ATI},
   {"sub", GL_SUB_ATI},

   {0, 0}
};

static struct
{
   char *instr;
   int args;
} argument_counts[] = 
{
   {"add", 2},
   {"cmp", 3},
   {"cnd", 3},
   {"d2a", 3},
   {"dp3", 2},
   {"dp4", 2},
   {"lrp", 3},
   {"mad", 3},
   {"mov", 1},
   {"mul", 2},
   {"sub", 2},
   
   {0, 0}
};

#ifdef PS14_DEBUG
void dbg(const char *fmt, ...)
{
   static char buf[512];
   va_list args;
   
   va_start(args, fmt);
   vsnprintf(buf, sizeof(buf), fmt, args);
   va_end(args);
   fprintf(stderr, buf);
}
#endif

static GLenum get_constant(const char *str)
{
   int i;
   for(i = 0; constant_table[i].name; ++i)
   {
      if(!strcmp(constant_table[i].name, str))
         return(constant_table[i].constant);
   }
   return(GL_NONE);
}

static void add_error(const char *err, int line)
{
   ps14_error_add("error on line %d: %s", line, err);
}

static void set_constant(constdef *c)
{
   GLuint reg;
   
   if((c->reg[0] != 'c') || (strlen(c->reg) != 2))
   {
      add_error("def lines must use constant registers", c->linenum);
      return;
   }
   
#ifdef PS14_DEBUG
   dbg("CONSTDEF: def       %s, %g, %g, %g, %g\n",
       c->reg, c->v[0], c->v[1], c->v[2], c->v[3]);
#endif   
   
   reg = GL_CON_0_ATI + (c->reg[1] - '0');
   glSetFragmentShaderConstantATI(reg, c->v);
}

static void set_texture_address(instruction *instr)
{
   char *op, *dst, *src, *p;
   GLenum swizzle, src_reg, dst_reg;
   
   if(instr->argc != 3)
   {
      add_error("incorrect texture address instruction", instr->linenum);
      return;
   }
   
#ifdef PS14_DEBUG
   dbg("TEXADDR:  %-7s   %s, %s\n", instr->argv[0], instr->argv[1], instr->argv[2]);
#endif   
      
   op  = instr->argv[0];
   dst = instr->argv[1];
   src = instr->argv[2];
   
   if(dst[0] != 'r')
   {
      add_error("incorrect register type for destination of textue address",
                instr->linenum);
      return;
   }
      
   if((src[0] != 'r') && (src[0] != 't'))
   {
      add_error("incorrect register type for source of texture address",
                instr->linenum);
      return;
   }
   
   p = strchr(src, '_');
   if(p == 0)
      p = strchr(src, '.');
   
   swizzle = GL_SWIZZLE_STR_ATI;
   
   if(p && *p)
   {
      if(!strcmp(p, ".xyz"))
         swizzle = GL_SWIZZLE_STR_ATI;
      else if(!strcmp(p, ".xyw"))
         swizzle = GL_SWIZZLE_STQ_ATI;
      else if(!strcmp(p, ".xyzw"))
         swizzle = GL_SWIZZLE_STRQ_ATI;
      else if(!strcmp(p, "_dw.xyzw"))
         swizzle = GL_SWIZZLE_STRQ_DQ_ATI;
      else if(!strcmp(p, "_dz.xyz") || !strcmp(p, "_dz"))
         swizzle = GL_SWIZZLE_STR_DR_ATI;
      else if(!strcmp(p, "_dw.xyw") || !strcmp(p, "_dw"))
         swizzle = GL_SWIZZLE_STQ_DQ_ATI;
      else
      {
         add_error("invalid swizzle for texture register", instr->linenum);
         return;
      }
   }
   
   src[2] = 0;
   dst[2] = 0;
      
   src_reg = get_constant(src);
   dst_reg = get_constant(dst);
   
   if(!strcmp(op, "texld"))
      glSampleMapATI(dst_reg, src_reg, swizzle);
   else
      glPassTexCoordATI(dst_reg, src_reg, swizzle);
      
   /* if in first pass (pass==0) of a 2 pass shader, mark register as 
    * being used.  if in second pass, set back to false.  this is done
    * because the 'register preservation' is done after the second pass
    * texture addressing instructions are executed.  */
   reg_preserve[dst_reg - GL_REG_0_ATI] = (pass == 0) ? 1 : 0;
}

typedef struct
{
   GLenum reg;
   GLuint mod;
   GLenum rep;
} sreg;

static sreg get_sreg(const char *str)
{
   char s[16],*p;
   sreg r;
 
   r.mod = 0;
   r.rep = GL_NONE;
   
   p = (char*)str;
   
   if((p[0] == '1') && (p[1] == '-'))
   {
      r.mod |= GL_COMP_BIT_ATI;
      p += 2;
   }
   else if(p[0] == '-')
   {
      r.mod |= GL_NEGATE_BIT_ATI;
      p += 1;
   }
   
   strcpy(s, p);

   if((p = strstr(s, "_bias")))
   {
      r.mod |= GL_BIAS_BIT_ATI;
      *p = 0;
   }
   else if((p = strstr(s, "_bx2")))
   {
      r.mod |= (GL_BIAS_BIT_ATI | GL_2X_BIT_ATI);
      *p = 0;
   }
   else if((p = strstr(s, "_x2")))
   {
      r.mod |= GL_2X_BIT_ATI;
      *p = 0;
   }

   if((p = strstr(s, ".")))
   {
      p++;
      switch(*p)
      {
         case 'r': r.rep = GL_RED;   break;
         case 'g': r.rep = GL_GREEN; break;
         case 'b': r.rep = GL_BLUE;  break;
         case 'a': r.rep = GL_ALPHA; break;
         default: break;
      }
   }

   s[2] = 0;
   r.reg = get_constant(s);
   
   return(r);
}
   

static void set_shader_instruction(instruction *instr)
{
   int i;
   int num_args;
   int alpha_op = 0;
   char *p, *op, *dst;
   GLuint dst_mod = 0;
   GLuint dst_mask = 0;
   GLenum op_enum, dst_reg;
   sreg src1, src2, src3;
   
   if(instr->argc < 3)
   {
      add_error("invalid shader insrtuction", instr->linenum);
      return;
   }
   
   if(pass == 0)
   {
      for(i = 2; i < instr->argc; ++i)
      {
         if(instr->argv[i][0] == 'v')
         {
            add_error("color interpolator registers can only be used in "
                      "the last pass of a 2 pass shader", instr->linenum);
            return;
         }
      }
   }
   
   op = instr->argv[0];
   if(op[0] == '+')
   {
      alpha_op = 1;
      op += 1;
      while(*op && *op == ' ') ++op;
   }
   
   dst = instr->argv[1];
   
   if(dst[0] != 'r')
   {
      add_error("destination must be a temporary register", instr->linenum);
      return;
   }
   
   if(strstr(op, "_x2"))
      dst_mod |= GL_2X_BIT_ATI;
   else if(strstr(op, "_x4"))
      dst_mod |= GL_4X_BIT_ATI;
   else if(strstr(op, "_x8"))
      dst_mod |= GL_8X_BIT_ATI;
   else if(strstr(op, "_d2"))
      dst_mod |= GL_HALF_BIT_ATI;
   else if(strstr(op, "_d4"))
      dst_mod |= GL_QUARTER_BIT_ATI;
   else if(strstr(op, "_d8"))
      dst_mod |= GL_EIGHTH_BIT_ATI;
   
   if(strstr(op, "_sat"))
      dst_mod |= GL_SATURATE_BIT_ATI;
   
   if(strstr(dst, ".rgba"))
      dst_mask = GL_NONE;
   else if(strstr(dst, ".rgb"))
      dst_mask = GL_RED_BIT_ATI | GL_BLUE_BIT_ATI | GL_GREEN_BIT_ATI;
   else if(strstr(dst, ".a"))
      alpha_op = 1;
   else if((p = strstr(dst, ".")))
   {
      p += 1;
      for(i = 0; (i < 3) && p[i]; ++i)
      {
         if(p[i] == 'r')
            dst_mask |= GL_RED_BIT_ATI;
         else if(p[i] == 'g')
            dst_mask |= GL_GREEN_BIT_ATI;
         else if(p[i] == 'b')
            dst_mask |= GL_BLUE_BIT_ATI;
      }
   }
   
   if((p = strstr(op, "_")))
      *p = 0;
   
   dst[2] = 0;
   
   op_enum = get_constant(op);
   dst_reg = get_constant(dst);

#ifdef PS14_DEBUG
   dbg("%s %-7s   ", alpha_op ? "ALPHA:   " : "RGB:     ", instr->argv[0]);
   for(i = 1; i < instr->argc; ++i)
      dbg("%s%s", instr->argv[i], (i == (instr->argc - 1)) ? "\n" : ", ");
#endif   
   
   num_args = 0;
   for(i = 0; argument_counts[i].instr; ++i)
   {
      if(!strcmp(argument_counts[i].instr, op))
      {
         num_args = argument_counts[i].args;
         break;
      }
   }
   
   if(num_args == 1)
   {
      if(instr->argc != 3)
      {
         add_error("incorrect instruction", instr->linenum);
         return;
      }
      
      src1 = get_sreg(instr->argv[2]);
      if(!alpha_op)
         glColorFragmentOp1ATI(op_enum, dst_reg, dst_mask, dst_mod,
                               src1.reg, src1.rep, src1.mod);
      else
         glAlphaFragmentOp1ATI(op_enum, dst_reg, dst_mod,
                               src1.reg, src1.rep, src1.mod);
   }
   else if(num_args == 2)
   {
      if(instr->argc != 4)
      {
         add_error("incorrect instruction", instr->linenum);
         return;
      }
      
      src1 = get_sreg(instr->argv[2]);
      src2 = get_sreg(instr->argv[3]);
      
      if(!alpha_op)
         glColorFragmentOp2ATI(op_enum, dst_reg, dst_mask, dst_mod,
                               src1.reg, src1.rep, src1.mod,
                               src2.reg, src2.rep, src2.mod);
      else
         glAlphaFragmentOp2ATI(op_enum, dst_reg, dst_mod,
                               src1.reg, src1.rep, src1.mod,
                               src2.reg, src2.rep, src2.mod);
   }
   else if(num_args == 3)
   {
      if(instr->argc != 5)
      {
         add_error("incorrect instruction", instr->linenum);
         return;
      }

      if(!strcmp(op, "cmp") || !strcmp(op, "cnd"))
      {
         src1 = get_sreg(instr->argv[3]);
         src2 = get_sreg(instr->argv[4]);
         src3 = get_sreg(instr->argv[2]);
      }
      else
      {
         src1 = get_sreg(instr->argv[2]);
         src2 = get_sreg(instr->argv[3]);
         src3 = get_sreg(instr->argv[4]);
      }
      
      if(!alpha_op)
         glColorFragmentOp3ATI(op_enum, dst_reg, dst_mask, dst_mod,
                               src1.reg, src1.rep, src1.mod,
                               src2.reg, src2.rep, src2.mod,
                               src3.reg, src3.rep, src3.mod);
      else
         glAlphaFragmentOp3ATI(op_enum, dst_reg, dst_mod,
                               src1.reg, src1.rep, src1.mod,
                               src2.reg, src2.rep, src2.mod,
                               src3.reg, src3.rep, src3.mod);
   }
   
   /* if in first pass of a 2 pass shader, mark register as used */
   if(pass == 0)
      reg_preserve[dst_reg - GL_REG_0_ATI] = 1;
}

void ps14_execute(constdef *c,
                  instruction *pass1a,
                  instruction *pass1b,
                  instruction *pass2a,
                  instruction *pass2b)
{
#define FOR_EACH(list, itor, func) { \
   if(list) { \
      itor = list; \
      while(itor) { \
         func(itor); \
         itor = (itor)->next; \
      } \
   } \
}
   
   int i;
   constdef *cdef,*ctmp;
   instruction *instr,*itmp;
   
   for(i = 0; i < 6; ++i)
      reg_preserve[i] = 0;
   
   pass = 0;
   /* if this is a single pass shader, we run as if in the second pass of a 
    * two pass shader */
   if(pass2b == 0)
      pass = 1;
   
   /* set constant egistes */
   FOR_EACH(c, cdef, set_constant);
   
   if(pass1b)
   {
      FOR_EACH(pass1a, instr, set_texture_address);
      FOR_EACH(pass1b, instr, set_shader_instruction);
      ++pass;
   }
   
   if(pass2b)
   {
#ifdef PS14_DEBUG
      dbg("phase\n");
#endif
      
      FOR_EACH(pass2a, instr, set_texture_address);
      
      /* preserve registers used in first pass that weren't used in the second
       * pass texture addresses.  allows registers used in the first pass to
       * be used in the second without manually adding the texcrd instructions */
      for(i = 0; i < 6; ++i)
      {
         if(reg_preserve[i])
            glPassTexCoordATI(GL_REG_0_ATI + i, GL_REG_0_ATI + i,
                              GL_SWIZZLE_STR_ATI);
      }
      
      FOR_EACH(pass2b, instr, set_shader_instruction);
   }
   
   if(c)
   {
      cdef = c;
      while(cdef)
      {
         ctmp = cdef->next;
         free(cdef);
         cdef = ctmp;
      }
   }
   
   if(pass1a)
   {
      instr = pass1a;
      while(instr)
      {
         itmp = instr->next;
         free(instr);
         instr = itmp;
      }
   }

   if(pass1b)
   {
      instr = pass1b;
      while(instr)
      {
         itmp = instr->next;
         free(instr);
         instr = itmp;
      }
   }
   
   if(pass2a)
   {
      instr = pass2a;
      while(instr)
      {
         itmp = instr->next;
         free(instr);
         instr = itmp;
      }
   }
   
   if(pass2b)
   {
      instr = pass2b;
      while(instr)
      {
         itmp = instr->next;
         free(instr);
         instr = itmp;
      }
   }   
}
