/*
ps14toATIfs - parses and wraps DirectX 8.1 pixel shaders version 1.4 to
              OpenGL using the ATI_fragment_shader extension.
Copyright (C) 2003  Shawn Kirst

For conditions of distribution and use, see copyright notice in ps14toATIfs.h
*/

#ifndef __PS14_PROGRAM_H
#define __PS14_PROGRAM_H

typedef struct constdef_s
{
   int linenum;
   char reg[16];
   float v[4];
   struct constdef_s *next;
} constdef;

#define PS14_MAX_ARGS 5
   
typedef struct instruction_s
{
   char argv[PS14_MAX_ARGS][16];
   int argc;
   int linenum;
   struct instruction_s *next;
} instruction;

void ps14_execute(constdef *c,
                  instruction *pass1a,
                  instruction *pass1b,
                  instruction *pass2a,
                  instruction *pass2b);
   
#endif
