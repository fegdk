/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "config.h"
#include "f_effectpass.h"
#include "f_engine.h"
#include "f_basetexture.h"
#include "f_glrenderer.h"
#include "f_shader.h"
#include "f_error.h"
#include "f_resourcemgr.h"
#include "backend.h"
#include "cvars.h"

namespace fe
{

	//------------------------------------------------------------
	// effectPass implementation
	//------------------------------------------------------------
	
	effectPass::effectPass (void)
	{
		memset (mTexBinding, 0, sizeof (mTexBinding));
		mNumTexBindings = 0;
	#if 0
		memset (mVscBinding, 0, sizeof (mVscBinding));
		mNumVscBindings = 0;
		memset (mPscBinding, 0, sizeof (mPscBinding));
		mNumPscBindings = 0;
		memset (mTFactorBinding, 0, sizeof (mTFactorBinding));
		mNumTFactorBindings = 0;
		mPSBinding = -1;
		mVSBinding = -1;
	#endif
		mpVertexProgram = NULL;
		mpFragmentProgram = NULL;
		memset (mVertBinding, 0, sizeof (mVertBinding));
		mNumVertBindings = 0;
		memset (mFragBinding, 0, sizeof (mFragBinding));
		mNumFragBindings = 0;
		memset (mTransformBinding, 0, sizeof (mTransformBinding));
		mNumTransformBindings = 0;
	
		memset (&mVertProgInfo, 0, sizeof (mVertProgInfo));
		memset (&mFragProgInfo, 0, sizeof (mFragProgInfo));
	
		mbNeedResolveDeps = true;
	
		mListId = -1;
	}
	
	effectPass::~effectPass (void)
	{
		glDeleteLists (mListId, 1);
	}
	
	void effectPass::readTransform (charParser &p, effect &s, GLenum t, int reg)
	{
		p.getToken ();
		if (!p.cmptoken ("<"))
		{
			p.getToken ();
			int binding = s.varIndexByName (p.token ());
			if (binding == -1)
				p.generalSyntaxError ();
			mTransformBinding[mNumTransformBindings].reg = reg;
			mTransformBinding[mNumTransformBindings].binding = binding;
			mNumTransformBindings++;
			p.matchToken (">");
		}
		else // 16 floats
		{
			float m[16];
			for (int i = 0; i < 16; i++)
			{
				p.getToken ();
				m[i] = atof (p.token ());
			}
			if (t > GL_TEXTURE && GLEW_ARB_multitexture)
				glClientActiveTexture (t - GL_TEXTURE + GL_TEXTURE0_ARB);
			glMatrixMode (t);
			glLoadMatrixf ( (GLfloat*)m);
			if (t > GL_TEXTURE && GLEW_ARB_multitexture)
				glClientActiveTexture (GL_TEXTURE0_ARB);
		}
		p.matchToken (";");
	}
	
	bool effectPass::load (charParser &p, effect &s)
	{
		mbNeedResolveDeps = true;
		p.matchToken ("{");
	
		bool bValid = true;
		bool b_enable_combine = false;
	#define check (x) (x)
	
		mListId = glGenLists (1);
		glNewList (mListId, GL_COMPILE);
	
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
				break;
			// texture
			else if (!p.cmptoken ("texture"))
			{
				p.matchToken ("[");
				p.getToken ();
				if (strlen (p.token ()) != 1 || !isdigit (p.token ()[0]))
					p.generalSyntaxError ();
				int stage = atoi (p.token ());
	
				if (stage > 0 && !GLEW_ARB_multitexture)
					bValid = false;
				
				if (stage > 7)
					p.generalSyntaxError ();
				p.matchToken ("]");
				p.matchToken ("<");
				// bindingection
				p.getToken ();
				int binding = s.varIndexByName (p.token ());
				if (binding == -1)
					p.generalSyntaxError ();
				mTexBinding[mNumTexBindings].reg = stage;
				mTexBinding[mNumTexBindings].binding = binding;
				mNumTexBindings++;
				p.matchToken (">");
				p.matchToken (";");
				if (GLEW_ARB_multitexture)
					glActiveTextureARB (GL_TEXTURE0_ARB + stage);
				glBindTexture (GL_TEXTURE_2D, 0);
			}
			// transforms
			else if (!p.cmptoken ("projectiontransform"))
				readTransform (p, s, GL_PROJECTION, 0);
	//		else if (!p.cmptoken ("worldtransform"))
	//			readTransform (p, s, D3DTS_WORLD, 1);
			else if (!p.cmptoken ("modelviewtransform"))
				readTransform (p, s, GL_MODELVIEW, 2);
			else if (!p.cmptoken ("texturetransform"))
			{
				p.matchToken ("[");
				p.getToken ();
				int stage = atoi (p.token ());
				if (stage > 0 && !GLEW_ARB_multitexture)
					bValid = false;
				if (stage > 7)
					p.generalSyntaxError ();
				p.matchToken ("]");
				readTransform (p, s, GL_TEXTURE+stage, 3 + stage);
			}
		// gl enable/disable
			#define parse_gl_en(x) else if (!p.cmptoken (#x)) readGlEnable (p, x)
			parse_gl_en (GL_ALPHA_TEST);
			parse_gl_en (GL_AUTO_NORMAL);
			parse_gl_en (GL_BLEND);
			parse_gl_en (GL_CLIP_PLANE0);
			parse_gl_en (GL_CLIP_PLANE1);
			parse_gl_en (GL_CLIP_PLANE2);
			parse_gl_en (GL_CLIP_PLANE3);
			parse_gl_en (GL_CLIP_PLANE4);
			parse_gl_en (GL_CLIP_PLANE5);
			parse_gl_en (GL_COLOR_LOGIC_OP);
			parse_gl_en (GL_COLOR_MATERIAL);
			parse_gl_en (GL_COLOR_TABLE);
			parse_gl_en (GL_CONVOLUTION_1D);
			parse_gl_en (GL_CONVOLUTION_2D);
			parse_gl_en (GL_CULL_FACE);
			parse_gl_en (GL_DEPTH_TEST);
			parse_gl_en (GL_DITHER);
			parse_gl_en (GL_FOG);
			parse_gl_en (GL_HISTOGRAM);
			parse_gl_en (GL_INDEX_LOGIC_OP);
			parse_gl_en (GL_LIGHT0);
			parse_gl_en (GL_LIGHT1);
			parse_gl_en (GL_LIGHT2);
			parse_gl_en (GL_LIGHT3);
			parse_gl_en (GL_LIGHT4);
			parse_gl_en (GL_LIGHT5);
			parse_gl_en (GL_LIGHT6);
			parse_gl_en (GL_LIGHT7);
			parse_gl_en (GL_LIGHTING);
			parse_gl_en (GL_LINE_SMOOTH);
			parse_gl_en (GL_LINE_STIPPLE);
			parse_gl_en (GL_MAP1_COLOR_4);
			parse_gl_en (GL_MAP1_INDEX);
			parse_gl_en (GL_MAP1_NORMAL);
			parse_gl_en (GL_MAP1_TEXTURE_COORD_1);
			parse_gl_en (GL_MAP1_TEXTURE_COORD_2);
			parse_gl_en (GL_MAP1_TEXTURE_COORD_3);
			parse_gl_en (GL_MAP1_TEXTURE_COORD_4);
			parse_gl_en (GL_MAP1_VERTEX_3);
			parse_gl_en (GL_MAP1_VERTEX_4);
			parse_gl_en (GL_MAP2_COLOR_4);
			parse_gl_en (GL_MAP2_INDEX);
			parse_gl_en (GL_MAP2_NORMAL);
			parse_gl_en (GL_MAP2_TEXTURE_COORD_1);
			parse_gl_en (GL_MAP2_TEXTURE_COORD_2);
			parse_gl_en (GL_MAP2_TEXTURE_COORD_3);
			parse_gl_en (GL_MAP2_TEXTURE_COORD_4);
			parse_gl_en (GL_MAP2_VERTEX_3);
			parse_gl_en (GL_MAP2_VERTEX_4);
			parse_gl_en (GL_MINMAX);
			parse_gl_en (GL_NORMALIZE);
			parse_gl_en (GL_POINT_SMOOTH);
			parse_gl_en (GL_POLYGON_OFFSET_FILL);
			parse_gl_en (GL_POLYGON_OFFSET_LINE);
			parse_gl_en (GL_POLYGON_OFFSET_POINT);
			parse_gl_en (GL_POLYGON_SMOOTH);
			parse_gl_en (GL_POLYGON_STIPPLE);
			parse_gl_en (GL_POST_COLOR_MATRIX_COLOR_TABLE);
			parse_gl_en (GL_POST_CONVOLUTION_COLOR_TABLE);
			parse_gl_en (GL_RESCALE_NORMAL);
			parse_gl_en (GL_SEPARABLE_2D);
			parse_gl_en (GL_SCISSOR_TEST);
			parse_gl_en (GL_STENCIL_TEST);
			parse_gl_en (GL_TEXTURE_1D);
			parse_gl_en (GL_TEXTURE_2D);
			parse_gl_en (GL_TEXTURE_3D);
			parse_gl_en (GL_TEXTURE_GEN_Q);
			parse_gl_en (GL_TEXTURE_GEN_R);
			parse_gl_en (GL_TEXTURE_GEN_S);
			parse_gl_en (GL_TEXTURE_GEN_T);
			#undef parse_gl_en
		// blendfuncs, etc
			else if (!p.cmptoken ("blendfunc"))
			{
				GLenum src, dest;
				src = readCmpFunc (p);
				dest = readCmpFunc (p);
				glBlendFunc (src, dest);
				p.matchToken (";");
			}
			else if (!p.cmptoken ("alphafunc"))
			{
				GLenum func;
				func = readCmpFunc (p);
				p.getToken ();
				glAlphaFunc (func, atof (p.token ()));
				p.matchToken (";");
			}
			else if (!p.cmptoken ("depthfunc"))
			{
				GLenum func;
				func = readCmpFunc (p);
				glDepthFunc (func);
				p.matchToken (";");
			}
			else if (!p.cmptoken ("depthrange"))
			{
				double rnear, rfar;
				p.getToken ();
				rnear = atof (p.token ());
				p.getToken ();
				rfar = atof (p.token ());
				glDepthRange (rnear, rfar);
				p.matchToken (";");
			}
			else if (!p.cmptoken ("polygonmode"))
			{
				p.getToken ();
				if (!p.cmptoken ("fill"))
					glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
				else if (!p.cmptoken ("line"))
					glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
				else if (!p.cmptoken ("point"))
					glPolygonMode (GL_FRONT_AND_BACK, GL_POINT);
				else
					p.generalSyntaxError ();
				p.matchToken (";");
			}
			// primary color
			else if (!p.cmptoken ("ffp_primary_color"))
			{
				p.getToken ();
				if (!p.cmptoken ("<"))
				{
					p.getToken ();
					int binding = s.varIndexByName (p.token ());
					if (binding == -1)
						p.generalSyntaxError ();
					mPrimColorBinding = binding;
					p.matchToken (">");
				}
				else
				{
					float rgba[4];
					for (int i = 0; i < 4; i++)
					{
						if (i)
							p.getToken ();
						rgba[i] = atof (p.token ());
					}
					glColor4fv (rgba);
				}
				p.matchToken (";");
			}
	
			// tss (arb_texture_env_combine)
			// requires texture_env_mode be set to GL_COMBINE
			// FIXME: check extension support here
	#define parse_tss(x,pname) \
			else if (!p.cmptoken (x)) { \
				if (!GLEW_ARB_texture_env_combine) \
					bValid = false; \
				readTSS (p, pname); \
				if (!b_enable_combine && GLEW_ARB_texture_env_combine) { \
					b_enable_combine = true; \
					glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB); \
				} \
			}
			parse_tss ("combine_rgb", GL_COMBINE_RGB)
			parse_tss ("combine_alpha", GL_COMBINE_ALPHA)
			parse_tss ("source0_rgb", GL_SOURCE0_RGB)
			parse_tss ("source1_rgb", GL_SOURCE1_RGB)
			parse_tss ("source2_rgb", GL_SOURCE2_RGB)
			parse_tss ("source0_alpha", GL_SOURCE0_ALPHA)
			parse_tss ("source1_alpha", GL_SOURCE1_ALPHA)
			parse_tss ("source2_alpha", GL_SOURCE2_ALPHA)
			parse_tss ("operand0_rgb", GL_OPERAND0_RGB)
			parse_tss ("operand1_rgb", GL_OPERAND1_RGB)
			parse_tss ("operand2_rgb", GL_OPERAND2_RGB)
			parse_tss ("operand0_alpha", GL_OPERAND0_ALPHA)
			parse_tss ("operand1_alpha", GL_OPERAND1_ALPHA)
			parse_tss ("operand2_alpha", GL_OPERAND2_ALPHA)
			parse_tss ("rgb_scale", GL_RGB_SCALE)
			parse_tss ("alpha_scale", GL_ALPHA_SCALE)
	#undef parse_tss
	
			// shader support
			//
			// note: ATI fglrx driver doesn't allow to compile/load shaders while compiling display list
			// so we gather all info into memory, than use it when display list is done
			else if (!p.cmptoken ("vertexprogram"))
			{
				mVertProgInfo.valid = true;
				p.matchToken ("{");
				for (;;)
				{
					p.getToken ();
					if (!p.cmptoken ("}"))
						break;
	
					// syntax for shader loading is : "file[:entry:profile]"
					else if (!p.cmptoken ("type"))
					{
						p.getToken ();
						mVertProgInfo.type = p.token ();
						p.matchToken (";");
					}
					else if (!p.cmptoken ("entry"))
					{
						p.getToken ();
						mVertProgInfo.entry = p.token ();
						p.matchToken (";");
					}
					else if (!p.cmptoken ("profile"))
					{
						p.getToken ();
						mVertProgInfo.profile = p.token ();
						p.matchToken (";");
					}
					else if (!p.cmptoken ("file"))
					{
						p.getToken ();
						mVertProgInfo.name = p.token ();
						p.matchToken (";");
					}
					else if (!p.cmptoken ("bind"))
					{
						p.getToken ();	// parameter name
						mVertBinding[mNumVertBindings].parmname = p.token ();
	
						p.matchToken ("<");
						p.getToken ();
						int binding = s.varIndexByName (p.token ());
						if (binding == -1)
							p.generalSyntaxError ();
						mVertBinding[mNumVertBindings].binding = binding;
						mNumVertBindings++;
						p.matchToken (">");
						
						p.matchToken (";");
					}
					else
						p.generalSyntaxError ();
				}
			}
			
			else if (!p.cmptoken ("fragmentprogram"))
			{
				mFragProgInfo.valid = true;
				p.matchToken ("{");
				for (;;)
				{
					p.getToken ();
					if (!p.cmptoken ("}"))
					{
						break;
					}
					// syntax for shader loading is : "file:entry:profile"
					else if (!p.cmptoken ("type"))
					{
						p.getToken ();
						mFragProgInfo.type = p.token ();
						p.matchToken (";");
					}
					else if (!p.cmptoken ("entry"))
					{
						p.getToken ();
						mFragProgInfo.entry = p.token ();
						p.matchToken (";");
					}
					else if (!p.cmptoken ("profile"))
					{
						p.getToken ();
						mFragProgInfo.profile = p.token ();
						p.matchToken (";");
					}
					else if (!p.cmptoken ("file"))
					{
						p.getToken ();
						mFragProgInfo.name = p.token ();
						p.matchToken (";");
					}
					else if (!p.cmptoken ("bind"))
					{
						p.getToken ();	// parameter name
						mFragBinding[mNumFragBindings].parmname = p.token ();
						// for now, just allow <> syntax
						p.matchToken ("<");
						p.getToken ();
						int binding = s.varIndexByName (p.token ());
						if (binding == -1)
							p.generalSyntaxError ();
						mFragBinding[mNumFragBindings].binding = binding;
						mNumFragBindings++;
						p.matchToken (">");
						
						p.matchToken (";");
					}
					else
						p.generalSyntaxError ();
				}
			}
				
			else
				p.generalSyntaxError ();
		}
	
		if (!b_enable_combine)
		{
			glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		}
	
		glEndList ();
	
		// create shaders if any
		if (bValid)
		{
			if (mVertProgInfo.valid)
			{
				cStr name = mVertProgInfo.name;
				if (!mVertProgInfo.entry.empty ())
					name += ":" + mVertProgInfo.entry;
				if (!mVertProgInfo.profile.empty ())
					name += ":" + mVertProgInfo.profile;
	
				//fprintf (stderr, "creating %s shader\n", name.c_str ());
#ifdef HAVE_LIBCG
				if (!strcmp (mVertProgInfo.type, "cgprogram"))
					mpVertexProgram = (shader*)g_engine->getResourceMgr ()->createCGShader (name);
				else
#endif
				if (!strcmp (mVertProgInfo.type, "ps14program"))
					mpVertexProgram = (shader*)g_engine->getResourceMgr ()->createPS14Shader (name);
				else
				{
					//fprintf (stderr, "unsupported shader type: %s\n", mVertProgInfo.type.c_str ());
				}
				if (!mpVertexProgram || !mpVertexProgram->isValid ())
					bValid = false;
				else
				{
					for (int i = 0; i < mNumVertBindings; i++)
					{
						mVertBinding[i].parm = mpVertexProgram->parmByName (mVertBinding[i].parmname);
						if (!mVertBinding[i].parm)
							sys_error ("parm not found: %s", mVertBinding[i].parmname.c_str ());
					}
				}
			}
			if (mFragProgInfo.valid)
			{
				cStr name = mFragProgInfo.name;
				if (!mFragProgInfo.entry.empty ())
					name += ":" + mFragProgInfo.entry;
				if (!mFragProgInfo.profile.empty ())
					name += ":" + mFragProgInfo.profile;
				//fprintf (stderr, "creating %s shader\n", name.c_str ());
#ifdef HAVE_LIBCG
				if (!strcmp (mFragProgInfo.type, "cgprogram"))
					mpFragmentProgram = (shader*)g_engine->getResourceMgr ()->createCGShader (name);
				else
#endif
				if (!strcmp (mFragProgInfo.type, "ps14program"))
					mpFragmentProgram = (shader*)g_engine->getResourceMgr ()->createPS14Shader (name);
				else
				{
					//fprintf (stderr, "unsupported shader type: %s\n", mVertProgInfo.type.c_str ());
				}
				if (!mpFragmentProgram || !mpFragmentProgram->isValid ())
					bValid = false;
				else
				{
					for (int i = 0; i < mNumFragBindings; i++)
					{
						mFragBinding[i].parm = mpFragmentProgram->parmByName (mFragBinding[i].parmname);
						if (!mFragBinding[i].parm)
							sys_error ("parm not found: %s\n", mFragBinding[i].parmname.c_str ());
					}
				}
			}
			restore ();
		}
			
		return bValid;
	}
	
	void effectPass::readTSS (charParser &p, const GLenum pname)
	{
		int stage;
		p.matchToken ("[");
		p.getToken ();
		stage = atoi (p.token ());
		p.matchToken ("]");
	
		if (GLEW_ARB_multitexture)
			glActiveTextureARB (GL_TEXTURE0_ARB+stage);
	
		p.getToken ();
	
	#define state(x, value) else if (!p.cmptoken (x)) {if (GLEW_ARB_texture_env_combine) glTexEnvi (GL_TEXTURE_ENV, pname, value);}
		if (pname == GL_COMBINE_RGB || pname == GL_COMBINE_ALPHA)
		{
			if (0) {}
			state ("replace", GL_REPLACE)
			state ("modulate", GL_MODULATE)
			state ("add", GL_ADD)
			state ("add_signed", GL_ADD_SIGNED)
			state ("interpolate", GL_INTERPOLATE)
			state ("subtract", GL_SUBTRACT)
			else
				p.generalSyntaxError ();
		}
		else if (pname == GL_SOURCE0_RGB || pname == GL_SOURCE1_RGB || pname == GL_SOURCE2_RGB
			|| pname == GL_SOURCE0_ALPHA || pname == GL_SOURCE1_ALPHA || pname == GL_SOURCE2_ALPHA)
		{
			if (0) {}
			state ("texture", GL_TEXTURE)
			state ("constant", GL_CONSTANT)
			state ("primary_color", GL_PRIMARY_COLOR)
			state ("previous", GL_PREVIOUS)
			else
				p.generalSyntaxError ();
		}
		else if (pname == GL_OPERAND0_RGB || pname == GL_OPERAND1_RGB || pname == GL_OPERAND2_RGB)
		{
			if (0) {}
			state ("src_color", GL_SRC_COLOR)
			state ("one_minus_src_color", GL_ONE_MINUS_SRC_COLOR)
			state ("src_alpha", GL_SRC_ALPHA)
			state ("one_minus_src_alpha", GL_ONE_MINUS_SRC_ALPHA)
			else
				p.generalSyntaxError ();
		}
		else if (pname == GL_OPERAND0_ALPHA || pname == GL_OPERAND1_ALPHA || pname == GL_OPERAND2_ALPHA)
		{
			if (0) {}
			state ("src_alpha", GL_SRC_ALPHA)
			state ("one_minus_src_alpha", GL_ONE_MINUS_SRC_ALPHA)
			else
				p.generalSyntaxError ();
		}
	#undef state
		else if (pname == GL_RGB_SCALE || pname == GL_ALPHA_SCALE)
		{
			if (GLEW_ARB_texture_env_combine)
				glTexEnvf (GL_TEXTURE_ENV, pname, atof (p.token ()));
		}
		else
			p.generalSyntaxError ();
	
		p.matchToken (";");
	}
	
	void effectPass::readGlEnable (charParser &p, const GLenum cap)
	{
		p.getToken ();
		if (!p.cmptoken ("true") || !p.cmptoken ("1"))
			glEnable (cap);
		else
			glDisable (cap);
		p.matchToken (";");
	}
	
	GLenum	effectPass::readCmpFunc (charParser &p)
	{
		p.getToken ();
		if (!p.cmptoken ("never"))
			return GL_NEVER;
		else if (!p.cmptoken ("less"))
			return GL_LESS;
		else if (!p.cmptoken ("equal"))
			return GL_EQUAL;
		else if (!p.cmptoken ("lequal"))
			return GL_LEQUAL;
		else if (!p.cmptoken ("greater"))
			return GL_GREATER;
		else if (!p.cmptoken ("notequal"))
			return GL_NOTEQUAL;
		else if (!p.cmptoken ("gequal"))
			return GL_GEQUAL;
		else if (!p.cmptoken ("always"))
			return GL_ALWAYS;
		else
			p.generalSyntaxError ();
		return (GLenum)-1;
	}
	
	void effectPass::apply (effect &s, uint32 flags)
	{
		if (mbNeedResolveDeps)
		{
			mbNeedResolveDeps = false;
			if (mpVertexProgram)
			{
				for (int i = 0; i < mNumVertBindings; i++)
				{
					mVertBinding[i].parm = mpVertexProgram->parmByName (mVertBinding[i].parmname);
					if (!mVertBinding[i].parm)
						sys_error ("parm not found: %s", mVertBinding[i].parmname.c_str ());
				}
			}
			if (mpFragmentProgram)
			{
				for (int i = 0; i < mNumFragBindings; i++)
				{
					mFragBinding[i].parm = mpFragmentProgram->parmByName (mFragBinding[i].parmname);
					if (!mFragBinding[i].parm)
						sys_error ("parm not found: %s\n", mFragBinding[i].parmname.c_str ());
				}
			}
		}
		// by semantic
		size_t k;
		smartPtr <effectParms> ep = g_engine->getEffectParms ();
		for (k = 0; k < s.mNumSemantBindings; k++) {
			effect::semantBinding_t *sem = &s.mSemantBindings[k];
			switch (sem->type) {
			case effect::bind_vector:
				s.setFloat4 (s.mSemantBindings[k].var, ep->getFloat4 (s.mSemantBindings[k].parm));
				break;
			case effect::bind_matrix:
				s.setMatrix (s.mSemantBindings[k].var, ep->getMatrix (s.mSemantBindings[k].parm));
				break;
			case effect::bind_texture:
				s.setTexture (s.mSemantBindings[k].var, ep->getTexture (s.mSemantBindings[k].parm));
				break;
			}
		}
	
		// apply state block
		if (!(flags & PASS_NO_DL))
		{
			glCallLists (1, GL_INT, &mListId);
		}
	
		if (!(flags & PASS_NO_VP))
		{
			if (!mpVertexProgram)
			{
		//		GLint maxunits;
		//		glGetIntegerv (GL_MAX_TEXTURE_UNITS, &maxunits);
				bool b_changed_tex[8] = {false, false, false, false, false, false, false, false};
				int max_used_unit = -1;
				int i;
				for (i = 0; i < mNumTransformBindings; i++)
				{
					effectVar *v = &s.mVars[mTransformBinding[i].binding];
					switch (mTransformBinding[i].reg)
					{
					case 0: glMatrixMode (GL_PROJECTION); break;
					case 1: break;
					case 2: glMatrixMode (GL_MODELVIEW); break;
					default:
						if (GLEW_ARB_multitexture)
							glActiveTextureARB (GL_TEXTURE0_ARB + mTransformBinding[i].reg - 3);
						glMatrixMode (GL_TEXTURE);
						b_changed_tex[mTransformBinding[i].reg - 3] = true;
						if (max_used_unit < mTransformBinding[i].reg - 3)
		                	max_used_unit = mTransformBinding[i].reg - 3;
						break;
					}
					glLoadMatrixf ( (GLfloat *)v->value ());
				}
				for (i = 0; i <= max_used_unit; i++)
				{
					if (!b_changed_tex[i])
					{
						if (GLEW_ARB_multitexture)
							glActiveTextureARB (GL_TEXTURE0_ARB + i);
						glMatrixMode (GL_TEXTURE);
						glLoadIdentity ();
					}
				}
			}
			else//	if (mpVertexProgram)
			{
				mpVertexProgram->bind ();
				for (int i = 0; i < mNumVertBindings; i++)
				{
					effectVar *v = &s.mVars[mVertBinding[i].binding];
					switch (v->type ())
					{
					case svtFloat4:
						mpVertexProgram->setFloat4 (mVertBinding[i].parm, * ((float4 *) (*v)));
						break;
					case svtMatrix:
						mpVertexProgram->setMatrix (mVertBinding[i].parm, * ((matrix4*) (*v)));
						break;
					default:
						sys_error ("invalid vertex shader constant binding in effect '%s'", s.name ());
					}
				}
			}
		}

		if (!(flags & PASS_NO_FP))
		{
			if (!mpFragmentProgram)
			{
				int i;
				int maxunit = -1;
			    // apply bindingections
				for (i = 0; i < mNumTexBindings; i++)
				{
					baseTexture *t = s.mVars[mTexBinding[i].binding];
			
					// FIXME: should be changed to something like baseTexture::nullTex.bind (mTexBinding[i].reg)
					if (t)
					{
						t->bind (mTexBinding[i].reg);
					}
		/*			else
					{
						if (GLEW_ARB_multitexture)
							glActiveTextureARB (mTexBinding[i].reg + GL_TEXTURE0_ARB);
						glBindTexture (GL_TEXTURE_2D, 0);
						fprintf (stderr, "no texture for %d, disabling %d\n", i, mTexBinding[i].reg);
						glDisable (GL_TEXTURE_2D);
					}*/
			
					if (mTexBinding[i].reg > maxunit)
						maxunit = mTexBinding[i].reg;
				}
				
		/*		if (GLEW_ARB_multitexture)
				{
					fprintf (stderr, "disabling texture %d\n", maxunit + 1);
					glActiveTextureARB (maxunit + 1 + GL_TEXTURE0_ARB);
					glBindTexture (GL_TEXTURE_2D, 0);
					glDisable (GL_TEXTURE_2D);
				}*/
				glActiveTextureARB (GL_TEXTURE0_ARB);
		
			}
			else // if (mpFragmentProgram)
			{
				mpFragmentProgram->bind ();
				for (int i = 0; i < mNumFragBindings; i++)
				{
					effectVar *v = &s.mVars[mFragBinding[i].binding];
					switch (v->type ())
					{
					case svtFloat4:
						mpFragmentProgram->setFloat4 (mFragBinding[i].parm, * ((float4 *) (*v)));
						break;
					case svtMatrix:
						mpFragmentProgram->setMatrix (mFragBinding[i].parm, * ((matrix4*) (*v)));
						break;
					case svtTexture:
						{
							baseTexture *t = *v;
							mpVertexProgram->setTexture (mFragBinding[i].parm, t);
						}
						break;
					default:
						sys_error ("invalid fragment shader constant binding in effect '%s'", s.name ());
					}
				}
			}
		}
	#if 0	// may be used for color bindingections or whatever... when not using shaders...
		for (i = 0; i < mNumTFactorBindings; i++)
		{
			float4 *f = *s.mVars[mTFactorBinding[i].binding];
			ulong clr;
			uchar *pclr = (uchar *)&clr;
			pclr[0] = min (255, max (0, f->b * 255));
			pclr[1] = min (255, max (0, f->g * 255));
			pclr[2] = min (255, max (0, f->r * 255));
			pclr[3] = min (255, max (0, f->a * 255));
			dev->SetRenderState (D3DRS_TEXTUREFACTOR, clr);
		}
	#endif

		// bind drawsurf
		drawSurf_t *surf = g_engine->getRBackend ()->getCurrentDrawSurf ();
		if (surf)
		{
			bool use_vbo = GLEW_ARB_vertex_buffer_object && r_use_vbo->ivalue && surf->mesh->vbo && surf->mesh->ibo;

			uchar *vptr = NULL;
			uchar *inds = NULL;
			if (use_vbo)
			{
				glBindBufferARB (GL_ARRAY_BUFFER_ARB, surf->mesh->vbo);
				glBindBufferARB (GL_ELEMENT_ARRAY_BUFFER_ARB, surf->mesh->ibo);
			}
			else
			{
				vptr = surf->mesh->verts;
				inds = surf->mesh->inds;
			}
			if (mpVertexProgram)
			{
				mpVertexProgram->bindDrawSurf (surf);
			}
			else
			{
				vptr += surf->mesh->firstVertex * surf->mesh->vertexSize;
				// bind for ffp
				glEnableClientState (GL_VERTEX_ARRAY);
				glEnableClientState (GL_NORMAL_ARRAY);
				glEnableClientState (GL_TEXTURE_COORD_ARRAY);
				glVertexPointer (3, GL_FLOAT, surf->mesh->vertexSize, vptr);
				glNormalPointer (GL_FLOAT, surf->mesh->vertexSize, vptr + sizeof (vector3));
				glTexCoordPointer (2, GL_FLOAT, surf->mesh->vertexSize, vptr + sizeof (vector3) * 2);

				// FIXME: this must be done by effect file
				if (surf->mesh->have_lightmaps)
				{
					// setup multitextured lightmap
					if (GLEW_ARB_multitexture)
					{
						glClientActiveTextureARB (GL_TEXTURE1_ARB);
						glEnableClientState (GL_TEXTURE_COORD_ARRAY);
						glTexCoordPointer (2, GL_FLOAT, surf->mesh->vertexSize, vptr + sizeof (vector3) * 2 + sizeof (vector2));
						glClientActiveTextureARB (GL_TEXTURE0_ARB);
					}
					else
					{
						// lightmap will be painted in separate pass
						// FIXME: effect file must provide texcoord channel index
						fprintf (stderr, "ERROR: codepath for multipass lightmap not implemented!\n");
					}
				}
			}
		}
	}
	
	shaderPtr	effectPass::getVertProgram (void) const
	{
		return mpVertexProgram;
	}
	
	shaderPtr	effectPass::getFragProgram (void) const
	{
		return mpFragmentProgram;
	}
	
	void			effectPass::finalize (void)
	{
		if (mpVertexProgram)
			mpVertexProgram->unbind ();
		if (mpFragmentProgram)
			mpFragmentProgram->unbind ();
		// unbind drawsurf
		drawSurf_t *surf = g_engine->getRBackend ()->getCurrentDrawSurf ();
		if (surf)
		{
			bool use_vbo = GLEW_ARB_vertex_buffer_object && r_use_vbo->ivalue && surf->mesh->vbo && surf->mesh->ibo;

			// FIXME: this will reduce batching
			if (use_vbo)
			{
				glBindBufferARB (GL_ARRAY_BUFFER_ARB, 0);
				glBindBufferARB (GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
			}
			if (mpVertexProgram)
			{
				mpVertexProgram->unbindDrawSurf (surf);
			}
			else
			{
				glDisableClientState (GL_VERTEX_ARRAY);
				glDisableClientState (GL_NORMAL_ARRAY);
				glDisableClientState (GL_TEXTURE_COORD_ARRAY);
			}
		}
	}
	
	void effectPass::loose (void)
	{
		mbNeedResolveDeps = true;
//		memset (mVertBinding, 0, sizeof (mVertBinding));
//		memset (mFragBinding, 0, sizeof (mFragBinding));
	}

	void effectPass::restore (void)
	{
	}
	
}
