#include "pch.h"
#include "cgcontext.h"

namespace fe
{
	cgContext::cgContext (void)
	{
		mCgContext = cgCreateContext ();
		fprintf (stderr, "cg context ptr: 0x%X\n", mCgContext);
	}

	cgContext::~cgContext (void)
	{
		if (mCgContext)
		{
			cgDestroyContext (mCgContext);
			mCgContext = 0;
		}
	}

	CGcontext cgContext::getContext (void) const
	{
		return mCgContext;
	}
}
