/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_PS14SHADER_H
#define __F_PS14SHADER_H

#include <GL/glew.h>
#include "f_types.h"
#include "f_baseobject.h"
#include "f_math.h"
#include "f_helpers.h"
#include "f_parser.h"
#include "f_fragileobject.h"
#include "f_shader.h"

namespace fe
{
	
	class FE_API ps14Shader : public shader
	{
	private:
		GLuint		mShaderId;
	public:
	
		ps14Shader (const char *fname);
		ps14Shader (charParser &parser, const char *name);
		~ps14Shader (void);
	
		void save (const char *fname) const;
		void bind (void) const;
		void unbind (void) const;
	
		handle parmByName (const char *name);
		void setFloat (handle parm, const float value);
		void setFloat4 (handle parm, const float4 &value);
		void setMatrix (handle parm, const matrix4 &value);
		void setTexture (handle parm, const smartPtr <baseTexture> &tex);
		
		virtual void enableStream (handle stream);
		virtual void disableStream (handle stream);
		virtual void bindStreamData (handle stream, int numComponents, streamParmType parmType, int stride, void *data);
	
		virtual void loose (void);
		virtual void restore (void);
	};
	
	typedef smartPtr <ps14Shader> ps14ShaderPtr;
	
}

#endif // __F_PS14SHADER_H


