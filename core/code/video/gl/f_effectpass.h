/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_EFFECT_PASS
#define __F_EFFECT_PASS

#include "f_effect.h"
#include "f_shader.h"
#include <GL/glew.h>

namespace fe
{

	const int MAX_FX_TEX_BINDINGS = 8;

	class FE_API effectPass : public fragileObject
	{
	
	friend class effectTechnique;
	friend class effect;
	
	private:
	
		GLint		mListId;
	
		struct binding_s
		{
			int reg;
			int binding;
		};
		
		struct shader_binding_s
		{
			cStr	parmname;
			handle parm;
			int binding;
		};
		
		binding_s					mTexBinding[MAX_FX_TEX_BINDINGS];
		int						mNumTexBindings;
	
		struct shader_info_t
		{
			bool valid;
			cStr type;
			cStr entry;
			cStr profile;
			cStr name;
		};
	
		shader_info_t			mVertProgInfo;
		shader_info_t			mFragProgInfo;
	
		shaderPtr			mpVertexProgram;
		shaderPtr			mpFragmentProgram;
		
		shader_binding_s			mVertBinding[256];
		int						mNumVertBindings;
		
		shader_binding_s			mFragBinding[256];
		int						mNumFragBindings;
	
		binding_s					mTransformBinding[11];	// world, view, proj, tex[8]
		int						mNumTransformBindings;
	
		int						mPrimColorBinding;
		bool mbNeedResolveDeps;

		void loose (void);
		void restore (void);
	
	protected:
	
		// rendering
		void apply (effect &s, uint32 flags);
		void finalize (void);	// cancels post-effects of using programmable pipeline, etc
	
		// parser
		bool load (charParser &p, effect &s);
	
		// parser helpers (transforms)
		void readTransform (charParser &p, effect &s, GLenum t, int reg);
		void readGlEnable (charParser &p, const GLenum cap);
		GLenum	readCmpFunc (charParser &p);
		void readTSS (charParser &p, GLenum pname);
	
	public:
	
		// ctor & dtor
		effectPass (void);
		~effectPass (void);
	
		shaderPtr	getVertProgram (void) const;
		shaderPtr	getFragProgram (void) const;
	
	};
	
}

#endif // __F_EFFECT_PASS

