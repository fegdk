/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_CGSHADER_H
#define __F_CGSHADER_H

#include <GL/glew.h>
#include <Cg/cg.h>
#include <Cg/cgGL.h>
#include "f_types.h"
#include "f_baseobject.h"
#include "f_math.h"
#include "f_helpers.h"
#include "f_parser.h"
#include "f_fragileobject.h"
#include "f_shader.h"

namespace fe
{
	
	class FE_API cgShader : public shader
	{
	private:
	
		int			mSourceType;
		CGprofile	mProfile;
		CGprogram	mProgram;
		bool checkCGError (void) const;
	
		cgShader (baseObject *o);
	
	public:
	
		enum {source_textfile, source_binaryfile, source_text, source_binary};
		enum {best_vertex_profile = -1, best_fragment_profile = -2};
		
		cgShader (const char *fname);
		cgShader (charParser &parser, const char *name);
		~cgShader (void);
	
		void save (const char *fname) const;
		void bind (void) const;
		void unbind (void) const;
	
		handle parmByName (const char *name);
		void setFloat (handle parm, const float value);
		void setFloat4 (handle parm, const float4 &value);
		void setMatrix (handle parm, const matrix4 &value);
		void setTexture (handle parm, const smartPtr <baseTexture> &tex);
		
		virtual void enableStream (handle stream);
		virtual void disableStream (handle stream);
		virtual void bindStreamData (handle stream, int numComponents, streamParmType parmType, int stride, void *data);
	
		virtual void loose (void);
		virtual void restore (void);
	};
	
	typedef smartPtr <cgShader> cgShaderPtr;
	
}

#endif // __F_CGSHADER_H

