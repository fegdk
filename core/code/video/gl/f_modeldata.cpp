/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_model.h"
#include "f_modeldata.h"
#include "f_engine.h"
#include "f_effectparms.h"
#include "f_glrenderer.h"
#include <GL/glew.h>
#include "f_cgshader.h"
#include "f_effectpass.h"
#include "f_material.h"
#include "f_scenemanager.h"

namespace fe
{

	modelData::modelData (model *mdl)
	{
		mpModel = mdl;
		
		mpVertexBuffer = NULL;
		mpIndexBuffer = NULL;
		mVertexBufferObject = 0;
		mIndexBufferObject = 0;
		mVertexSize = 0;

		restore ();
		
	}
	
	modelData::~modelData (void)
	{
		loose ();
	}
	
	void modelData::drawSubset (int subset, int npass) const
	{
	#if 0
		// get mtl and effect
		materialPtr mtl = g_engine->getSceneManager ()->getCurrentMtl ();
		if (!mtl)
			return;
		effectPtr fx = mtl->getEffect ();
		if (!fx)
			return;
		assert (npass != -1);
		if (npass == -1)
			return;

		smartPtr <effectPass> pass = fx->getCurrentTechnique ()->getPass (npass);
	
		// create vbo if possible
		uchar *vptr = NULL;
		if (!GLEW_ARB_vertex_buffer_object)
			vptr = mpVertexBuffer;
		else
		{
			glBindBufferARB (GL_ARRAY_BUFFER_ARB, mVertexBufferObject);
			glBindBufferARB (GL_ELEMENT_ARRAY_BUFFER_ARB, mIndexBufferObject);
		}
		vptr += mpModel->mSurfaces[subset].firstVertex * mVertexSize;
	
		// bind vertexprogram
		shaderPtr vertProgram = pass ? pass->getVertProgram () : NULL;
		handle pos, norm, color, tex0, tex1;
		const vp_binding_t *binding = NULL;
		vp_binding_t new_binding;
	
		if (vertProgram)
		{
			// find binding
			vp_binding_map_t::const_iterator it;
			it = mVPBindings.find (vertProgram->name ());
			if (it == mVPBindings.end ())
			{
				// not found, setup for new shader
				new_binding.pos = vertProgram->parmByName ("position");
				new_binding.norm = vertProgram->parmByName ("normal");
				new_binding.color = vertProgram->parmByName ("color");
				new_binding.tex0 = vertProgram->parmByName ("texcoord0");
				new_binding.tex1 = vertProgram->parmByName ("texcoord1");
				new_binding.tangent = vertProgram->parmByName ("tangent");
				new_binding.binormal = vertProgram->parmByName ("binormal");
				mVPBindings[vertProgram->name ()] = new_binding;
				binding = &new_binding;
			}
			else
				binding = &(*it).second;
	
			int offs = 0;
			if (binding->pos)
			{
				vertProgram->enableStream (binding->pos);
				vertProgram->bindStreamData (binding->pos, 3, shader::FLOAT, mVertexSize, vptr + offs);
			}
			offs += sizeof (vector3);
			if (binding->norm)
			{
				vertProgram->enableStream (binding->norm);
				vertProgram->bindStreamData (binding->norm, 3, shader::FLOAT, mVertexSize, vptr + offs);
			}
			offs += sizeof (vector3);
			if (binding->tex0)
			{
				vertProgram->enableStream (binding->tex0);
				vertProgram->bindStreamData (binding->tex0, 2, shader::FLOAT, mVertexSize, vptr + offs);
			}
			offs += sizeof (vector2);
			if (mpModel->mbColors && binding->color)
			{
				vertProgram->enableStream (binding->color);
				vertProgram->bindStreamData (binding->color, 4, shader::UNSIGNED_BYTE, mVertexSize, vptr + offs);
				offs += 4;
			}
			if (mpModel->mSurfaces[subset].lightmap && binding->tex1)
			{
				vertProgram->enableStream (binding->tex1);
				vertProgram->bindStreamData (binding->tex1, 2, shader::FLOAT, mVertexSize, vptr + offs);
				offs += sizeof (vector2);
			}
			if (mpModel->mbTangents && binding->tangent)
			{
				vertProgram->enableStream (binding->tangent);
				vertProgram->bindStreamData (binding->tangent, 3, shader::FLOAT, mVertexSize, vptr + offs);
				offs += sizeof (vector3);
			}
			if (mpModel->mbTangents && binding->binormal)
			{
				vertProgram->enableStream (binding->binormal);
				vertProgram->bindStreamData (binding->binormal, 3, shader::FLOAT, mVertexSize, vptr + offs);
				offs += sizeof (vector3);
			}
		}
		else
		{
			// bind for ffp
			glEnableClientState (GL_VERTEX_ARRAY);
			if (npass != -1)
				glEnableClientState (GL_NORMAL_ARRAY);
			glEnableClientState (GL_TEXTURE_COORD_ARRAY);
			glVertexPointer (3, GL_FLOAT, mVertexSize, vptr);
			if (npass != -1)
				glNormalPointer (GL_FLOAT, mVertexSize, vptr + sizeof (vector3));
			glTexCoordPointer (2, GL_FLOAT, mVertexSize, vptr + sizeof (vector3) * 2);
		
			if (npass != -1 && mpModel->mSurfaces[subset].lightmap)
			{
				if (GLEW_ARB_multitexture)
					glClientActiveTextureARB (GL_TEXTURE1_ARB);
				glEnableClientState (GL_TEXTURE_COORD_ARRAY);
				glTexCoordPointer (2, GL_FLOAT, mVertexSize, vptr + sizeof (vector3) * 2 + sizeof (vector2));
				if (GLEW_ARB_multitexture)
					glClientActiveTextureARB (GL_TEXTURE0_ARB);
			}
		}
	
#if 0
		// FIXME: kill this
		if (npass == -1)
		{
			setupTransforms ();
			baseTexturePtr t = mpModel->mSurfaces[subset].mtl->getStdMap (material::map);
			if (t)
				t->bind (0);
			else
			{
				if (GLEW_ARB_multitexture)
					glActiveTexture (GL_TEXTURE0_ARB);
				glDisable (GL_TEXTURE_2D);
			}
		}
#endif
	
		// draw
		glDrawElements (GL_TRIANGLES, mpModel->mSurfaces[subset].numFaces * 3, GL_UNSIGNED_SHORT, ((ushort*)mpIndexBuffer) + mpModel->mSurfaces[subset].firstFace * 3);
		
		// unbind
		if (vertProgram)
		{
			if (binding->pos)
			{
				vertProgram->disableStream (binding->pos);
			}
			if (binding->norm)
			{
				vertProgram->disableStream (binding->norm);
			}
			if (binding->tex0)
			{
				vertProgram->disableStream (binding->tex0);
			}
			if (mpModel->mSurfaces[subset].lightmap && binding->tex1)
			{
				vertProgram->disableStream (binding->tex1);
			}
			if (mpModel->mbTangents && binding->tangent)
			{
				vertProgram->disableStream (binding->tangent);
			}
			if (mpModel->mbTangents && binding->binormal)
			{
				vertProgram->disableStream (binding->binormal);
			}
		}
		else
		{
			glDisableClientState (GL_VERTEX_ARRAY);
			if (npass != -1)
				glDisableClientState (GL_NORMAL_ARRAY);
			glDisableClientState (GL_TEXTURE_COORD_ARRAY);
			if (npass != -1 && mpModel->mSurfaces[subset].lightmap)
			{
				if (GLEW_ARB_multitexture)
					glClientActiveTextureARB (GL_TEXTURE1_ARB);
				glDisableClientState (GL_TEXTURE_COORD_ARRAY);
				if (GLEW_ARB_multitexture)
					glClientActiveTextureARB (GL_TEXTURE0_ARB);
			}
		}
		if (GLEW_ARB_vertex_buffer_object)
		{
			glBindBufferARB (GL_ARRAY_BUFFER_ARB, 0);
			glBindBufferARB (GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
		}
	/*
		// draw tangent space
		// NOTE: shaders must be disabled
	
		mpModel->mSurfaces[0].mtl->getEffect ()->end ();
		glActiveTextureARB (GL_TEXTURE0_ARB);
		glDisable (GL_TEXTURE_2D);
		glActiveTextureARB (GL_TEXTURE1_ARB);
		glDisable (GL_TEXTURE_2D);
		glBegin (GL_LINES);
		for (int i = 0; i < mpModel->mTris.numVerts; i++)
		{
			vector3 s = mpModel->mTris.verts[i].pos * mpModel->getWorldMatrix ();
			vector3 norm = mpModel->mTris.verts[i].tangents[0].cross (mpModel->mTris.verts[i].tangents[1]);
			vector3 e = s + mpModel->getWorldMatrix ().transform (mpModel->mTris.verts[i].tangents[0]);
			glColor3f (1, 0, 0);
			glVertex3fv ((GLfloat *)&s);
			glVertex3fv ((GLfloat *)&e);
			e = s + mpModel->getWorldMatrix ().transform (mpModel->mTris.verts[i].tangents[1]);
			glColor3f (0, 1, 0);
			glVertex3fv ((GLfloat *)&s);
			glVertex3fv ((GLfloat *)&e);
			e = s + mpModel->getWorldMatrix ().transform (norm);
			glColor3f (0, 0, 1);
			glVertex3fv ((GLfloat *)&s);
			glVertex3fv ((GLfloat *)&e);
		}
		glEnd ();*/
		#endif
	}
	
	void modelData::setupTransforms (void) const
	{
		glMatrixMode (GL_PROJECTION);
		smartPtr<effectParms> ep = g_engine->getEffectParms ();
		const matrix4 &mp = ep->getMatrix (effectParm_ProjMatrix);
		glLoadMatrixf ((GLfloat *)&mp);
	
		glMatrixMode (GL_MODELVIEW);
		const matrix4 &m = ep->getMatrix (effectParm_WorldViewMatrix);
		glLoadMatrixf ((GLfloat *)&m);
	}

#if 0
	void modelData::renderShadowVolume (void) const
	{
		smartPtr<effectParms> ep = g_engine->getEffectParms ();
		// current implementation is stub, which uses sysmem vertex/index data
		ep->setMatrix (effectParm_WorldMatrix, mpModel->getWorldMatrix ());
		setupTransforms ();
		glEnableClientState (GL_VERTEX_ARRAY);	
		glVertexPointer (4, GL_FLOAT, sizeof (shadowVertex_t), mpModel->mTris.shadowVerts);
		glDrawElements (GL_TRIANGLES, mpModel->mTris.numShadowVolumeIndexes, GL_UNSIGNED_SHORT, mpModel->mTris.shadowVolumeIndexes);
		glDisableClientState (GL_VERTEX_ARRAY);
	}
#endif
	
	void modelData::loose (void)
	{
		mVPBindings.clear ();
		if (mVertexBufferObject)
		{
			glDeleteBuffersARB (1, &mVertexBufferObject);
			mVertexBufferObject = 0;
		}
		if (mIndexBufferObject)
		{
			glDeleteBuffersARB (1, &mIndexBufferObject);
			mIndexBufferObject = 0;
		}
		if (mpVertexBuffer)
		{
			delete[] mpVertexBuffer;
			mpVertexBuffer = NULL;
		}
	
		if (mpIndexBuffer)
		{
			delete[] mpIndexBuffer;
			mpIndexBuffer = NULL;
		}
		if (mpModel->mpDrawSurfs)
		{
			delete[] mpModel->mpDrawSurfs;
			mpModel->mpDrawSurfs = NULL;
		}
		mpModel->mNumDrawSurfs = 0;
	}
	
	void modelData::restore (void)
	{
		mVertexSize = sizeof (vector3) * 2; // pos + norm
		mVertexSize += sizeof (vector2); // uv
		if (mpModel->mbColors)
			mVertexSize += 4;
		if (mpModel->mbLightmaps)
			mVertexSize += sizeof (vector2);
		if (mpModel->mbTangents)
			mVertexSize += sizeof (vector3) * 2;
	
		int size = (int)(mVertexSize * mpModel->mTris.numVerts);
	
		// init vertex/index buffers
		mpVertexBuffer = NULL;
		mpIndexBuffer = NULL;
		mVertexBufferObject = 0;
		mIndexBufferObject = 0;
	
		uchar *ptr;
		mpVertexBuffer = new uchar[size];
		ptr = mpVertexBuffer;
	
		// copy verts
		size_t i;
		for (i = 0; i < mpModel->mTris.numVerts; i++)
		{
			memcpy (ptr, &mpModel->mTris.verts[i].pos, sizeof (vector3));
			ptr += sizeof (vector3);
			memcpy (ptr, &mpModel->mTris.verts[i].norm, sizeof (vector3));
			ptr += sizeof (vector3);
			memcpy (ptr, &mpModel->mTris.verts[i].uv, sizeof (vector2));
			ptr += sizeof (vector2);
			if (mpModel->mbColors)
			{
				memcpy (ptr, mpModel->mTris.verts[i].color, 4);
				ptr += 4;
			}
			if (mpModel->mbLightmaps)
			{
				memcpy (ptr, &mpModel->mTris.verts[i].uvLightmap, sizeof (vector2));
				ptr += sizeof (vector2);
			}
			if (mpModel->mbTangents)
			{
				memcpy (ptr, &mpModel->mTris.verts[i].tangents[0], sizeof (vector3));
				ptr += sizeof (vector3);
				memcpy (ptr, &mpModel->mTris.verts[i].tangents[1], sizeof (vector3));
				ptr += sizeof (vector3);
			}
		}

		if (GLEW_ARB_vertex_buffer_object)
		{
			glGenBuffersARB (1, &mVertexBufferObject);
			glBindBufferARB (GL_ARRAY_BUFFER_ARB, mVertexBufferObject);
			glBufferDataARB (GL_ARRAY_BUFFER_ARB, size, NULL, GL_STATIC_DRAW_ARB);
			uchar *p = (uchar*)glMapBufferARB (GL_ARRAY_BUFFER_ARB, GL_WRITE_ONLY_ARB);
			memcpy (p, mpVertexBuffer, size);
			glUnmapBufferARB (GL_ARRAY_BUFFER_ARB);
		}
	
		// copy faces
		mpIndexBuffer = new uchar[sizeof (ushort) * mpModel->mTris.numIndexes];
		ptr = mpIndexBuffer;
		for (i = 0; i < mpModel->mTris.numIndexes; i++)
		{
			memcpy (ptr, &mpModel->mTris.indexes[i], sizeof (ushort));
			ptr += sizeof (ushort);
		}

		if (GLEW_ARB_vertex_buffer_object)
		{
			glGenBuffersARB (1, &mIndexBufferObject);
			glBindBufferARB (GL_ELEMENT_ARRAY_BUFFER_ARB, mIndexBufferObject);
			glBufferDataARB (GL_ELEMENT_ARRAY_BUFFER_ARB, mpModel->mTris.numIndexes * sizeof (ushort), mpModel->mTris.indexes, GL_STATIC_DRAW_ARB);
		}
		// init drawsurfs
		mpModel->mNumDrawSurfs = mpModel->mSurfaces.size ();
		mpModel->mpDrawSurfs = new drawSurf_t[mpModel->mNumDrawSurfs];
		memset (mpModel->mpDrawSurfs, 0, sizeof (drawSurf_t) * mpModel->mNumDrawSurfs);
		for (int i = 0; i < mpModel->mNumDrawSurfs; i++)
		{
			drawSurf_t *s = &mpModel->mpDrawSurfs[i];
			s->primType = GL_TRIANGLES;
			s->obj = mpModel;
			s->mtl = mpModel->mSurfaces[i].mtl;
			s->lightmap = mpModel->mSurfaces[i].lightmap;
			
			s->vbo = mVertexBufferObject;
			s->verts = mpVertexBuffer;
			s->vertexSize = mVertexSize;

			s->ibo = mIndexBufferObject;
			s->inds = mpIndexBuffer;

			s->firstVertex = mpModel->mSurfaces[i].firstVertex;
			s->numVerts = mpModel->mSurfaces[i].numVerts;
			s->firstIndex = mpModel->mSurfaces[i].firstFace*3;
			s->numInds = mpModel->mSurfaces[i].numFaces*3;

			s->have_colors = mpModel->mbColors ? 1 : 0;
			s->have_lightmaps = mpModel->mbLightmaps ? 1 : 0;
			s->have_tangents = mpModel->mbTangents ? 1 : 0;
		}
	}

}
