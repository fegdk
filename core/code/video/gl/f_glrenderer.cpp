/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "config.h"
#include "f_glrenderer.h"
#include <GL/glew.h>
#include "f_engine.h"
#include "f_console.h"
#include "f_resourcemgr.h"
#include "f_ps14shader.h"

#if HAVE_LIBCG
#include "cgcontext.h"
#endif

namespace fe
{
	#if !(HAVE_LIBCG)
	class cgContext : public baseObject
	{
	};
	#endif
	
	glRenderer::glRenderer (void)
	{
		mbInitialized = false;
	}
	
	glRenderer::~glRenderer (void)
	{
	}
	
	void Cmd_r_listmodes_f (void)
	{
		smartPtr <console> con = g_engine->getConsole ();
		const baseRenderer *renderer = g_engine->getRenderer();
		ulong adapter = renderer->getCurrentAdapter();
		ulong device = renderer->getCurrentDevice( adapter );
	
		for ( ulong i = 0; i < renderer->numDisplayModes( adapter, device ); i++ )
		{
			cStr mode_string = renderer->getDisplayModeDescr( adapter, device, i );
			cStr s;
			s.printf( "%0.3d %s", i, mode_string.c_str() );
			con->msg( s );
		}
	}
	
	void Cmd_r_listadapters_f (void)
	{
		smartPtr <console> con = g_engine->getConsole ();
		for ( ulong i = 0; i < g_engine->getRenderer()->numAdapters(); i++ )
		{
			cStr adapter_string = g_engine->getRenderer()->getAdapterDescr( i );
			cStr s;
			s.printf( "%0.3d %s", i, adapter_string.c_str() );
			con->msg( s );
		}
	}
	
	void Cmd_r_listdevices_f (void)
	{
		smartPtr <console> con = g_engine->getConsole ();
		ulong adapter = g_engine->getRenderer()->getCurrentAdapter();
		for ( ulong i = 0; i < g_engine->getRenderer()->numDevices( adapter ); i++ )
		{
			cStr device_string = g_engine->getRenderer()->getDeviceDescr( adapter, i );
			cStr s;
			s.printf( "%0.3d %s", i, device_string.c_str() );
			con->msg( s );
		}
	}
	
	void glRenderer::init (void)
	{
		smartPtr <console> con = g_engine->getConsole ();
		con->msg( "---------------------" );
		con->msg( "\\c2glRenderer::init" );
	
/*		con->msg ("registering cg shader manager...");
		
		if (GLEW_ATI_fragment_shader)
		{
			con->msg ("registering ATI_fs shader manager...");
		}*/
		
		con->msg ("setting some default opengl states...");
		
		con->msg ("registering render-related commands...");
	
		FE_REGISTER_CONSOLE_CMD (r_listmodes);
		FE_REGISTER_CONSOLE_CMD (r_listadapters);
		FE_REGISTER_CONSOLE_CMD (r_listdevices);
		con->msg ("calling resize...");
		resize ();
	}
	
	cgContext*	glRenderer::getCgContext (void) const
	{
		return mpCgContext;
	}
	
	void glRenderer::setViewport (int sx, int sy, int sw, int sh)
	{
		glViewport (sx, sy, sw, sh);
	}
	
	void glRenderer::begin (void) const
	{
	}
	
	void glRenderer::end (void) const
	{
	}
	
	void glRenderer::clear (int numRects, const rect *rects, ulong flags, float4 color, float z, ulong stencil) const
	{
		// only clears entire viewport
	
		if (flags & (clear_z | clear_stencil))
			glDepthMask (GL_TRUE);	// allow to clear depthstencil
	
		ulong glflags = 0;
		if (flags & clear_target)
		{
			glClearColor (color.r, color.g, color.b, color.a);
			glflags |= GL_COLOR_BUFFER_BIT;
		}
		if (flags & clear_z)
		{
			glClearDepth (z);
			glflags |= GL_DEPTH_BUFFER_BIT;
		}
		if (flags & clear_stencil)
		{
			glClearStencil (stencil);
			glflags |= GL_STENCIL_BUFFER_BIT;
		}
	
		glClear(glflags);
	}
	
	void glRenderer::resize (void)
	{
		if (!mbInitialized)
		{
			glewInit ();
			fprintf (stderr, "available (used) GL extensions:");
			if (GLEW_ARB_multitexture)
				fprintf (stderr, "ARB_multitexture\n");
			if (GLEW_ARB_texture_env_combine)
				fprintf (stderr, "ARB_texture_env_combine\n");
			if (GLEW_ARB_texture_env_dot3)
				fprintf (stderr, "ARB_texture_env_dot3\n");
			if (GLEW_ARB_fragment_program)
				fprintf (stderr, "ARB_fragment_program\n");
			if (GLEW_ARB_vertex_program)
				fprintf (stderr, "ARB_vertex_program\n");
			if (GLEW_ATI_fragment_shader)
				fprintf (stderr, "ATI_fragment_shader\n");
			if (GLEW_ARB_vertex_buffer_object)
				fprintf (stderr, "ARB_vertex_buffer_object\n");
			if (GLEW_EXT_depth_bounds_test)
				fprintf (stderr, "EXT_depth_bound_test\n");
			if (GLEW_EXT_stencil_two_side)
				fprintf (stderr, "EXT_stencil_two_side\n");
			if (GLEW_EXT_stencil_wrap)
				fprintf (stderr, "EXT_stencil_wrap\n");

#if HAVE_LIBCG
			mpCgContext = new cgContext;
#endif
	
			mbInitialized = true;
		}
		
		// default states
		glEnable (GL_DEPTH_TEST);
		glDepthFunc (GL_LEQUAL);
		glShadeModel (GL_SMOOTH);
	
		glCullFace (GL_BACK);
		glFrontFace (GL_CW);
		glEnable (GL_CULL_FACE);

		fprintf (stderr, "INFO: resize done.\n");
	}
	
	bool				glRenderer::isLost (void) const
	{
		return false;
	}
	
	void				glRenderer::present (void)
	{
	}
	
	ulong				glRenderer::numDisplayModes (ulong adapter, ulong device) const
	{
		return 0;
	}
	
	cStr				glRenderer::getDisplayModeDescr (ulong adapter, ulong device, ulong mode) const
	{
		return "";
	}
	
	ulong				glRenderer::numDevices (ulong adapter) const
	{
		return 0;
	}
	
	cStr				glRenderer::getDeviceDescr (ulong adapter, ulong device) const
	{
		return "";
	}
	
	ulong				glRenderer::numAdapters (void) const
	{
		return 0;
	}
	
	cStr				glRenderer::getAdapterDescr (ulong adapter) const
	{
		return "";
	}
	
	ulong				glRenderer::getCurrentAdapter (void) const
	{
		return 0;
	}
	
	ulong				glRenderer::getCurrentDevice (ulong adapter) const
	{
		return 0;
	}
	
	ulong				glRenderer::getCurrentMode (ulong adapter, ulong device) const
	{
		return 0;
	}
	
	void				glRenderer::setWindow (baseViewport *vp)
	{
	}
	
}
