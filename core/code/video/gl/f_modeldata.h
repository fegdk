/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_MODELDATA_H
#define __F_MODELDATA_H

#include "f_types.h"
#include <GL/glew.h>
#include "f_shader.h"
#include "f_fragileobject.h"

namespace fe
{

	class modelData : public fragileObject
	{
	private:
	
		// cg programs parameter bindings
		struct vp_binding_t
		{
			handle pos, norm, color, tex0, tex1, tangent, binormal;
		};
	
		typedef std::map <cStr, vp_binding_t>		vp_binding_map_t;
		mutable vp_binding_map_t		mVPBindings;
	
	protected:
		
		model* mpModel;
		uchar* mpVertexBuffer;
		uchar* mpIndexBuffer;
	
		GLuint	mVertexBufferObject;
		GLuint	mIndexBufferObject;
		
		int		mVertexSize;
	
	public:
	
		modelData (model *owner);
		~modelData (void);
	
		void drawSubset (int subset, int npass) const;
		void renderShadowVolume (void) const;
		void setupTransforms (void) const;
	
		virtual void loose (void);
		virtual void restore (void);
		
	};
	
}

#endif // __F_MODELDATA_H

