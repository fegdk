/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

// opengl 2D texture implementation

#include "pch.h"
#include "config.h"
#include "f_types.h"
#include "f_filesystem.h"
#include "f_engine.h"
#include "f_texture.h"
#include "f_texturedata.h"
#include "f_error.h"
#include "f_baserenderer.h"
#include "f_helpers.h"
#include "f_resourcemgr.h"
#include "mmanager.h"
#include <GL/glew.h>
#include <GL/glu.h>
#include <png.h>
extern "C" {
#include <jpeglib.h>
#include <jerror.h>
}
#include "cvars.h"

#ifdef HAVE_LIBCG
#include <Cg/cgGL.h>
#endif

namespace fe
{
	
	textureData::textureData (texture *container)
	{
		assert (container);
		if (!container)
			sys_error ("textureData::textureData got a NULL container");
		memset (mLocks, 0, sizeof (mLocks));
		mName = container->name () + cStr ("--texdata");
		mpContainer = container;
		mTextureId = 0;
		mbLost = true;
		mWidth = 0;
		mHeight = 0;
	}
	
	textureData::textureData (texture *container, ulong w, ulong h)
	{
		assert (container);
		if (!container)
			sys_error ("textureData::textureData (gl) error -- got a NULL container");
		memset (mLocks, 0, sizeof (mLocks));
		mpContainer = container;
		mTextureId = 0;
		mbLost = true;
		mWidth = w;
		mHeight = h;
		mName = container->name ();
		restore ();
	}
	
	textureData::~textureData (void)
	{
		if (mTextureId != 0)
			glDeleteTextures (1, &mTextureId);
	}
	
	void	textureData::loose (void)
	{
		if (mTextureId != 0)
		{
			glDeleteTextures (1, &mTextureId);
			mTextureId = 0;
		}
		mbLost = true;
	}
	
	void	textureData::setFiltering (bool mips)
	{
		if (!r_use_mips->ivalue)
			mips = false;
		switch (r_texture_filtering->ivalue)
		{
			case 0:
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mips ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST);
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				break;
			case 1:
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mips ? GL_LINEAR_MIPMAP_NEAREST : GL_LINEAR);
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				break;
			case 2:
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mips ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				break;
			case 3:
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mips ? GL_LINEAR : GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				break;
			default:
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mips ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST);
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				break;
		}
	}

	void	textureData::restore (void)
	{
		assert (false != mbLost);
		if (mbLost)
		{
			if (!strncmp (mpContainer->name (), "empty:", 6))
			{
				// create surface now
				glGenTextures (1, &mTextureId);
				if (GLEW_ARB_multitexture)
					glActiveTextureARB (GL_TEXTURE0_ARB);
				glBindTexture (GL_TEXTURE_2D, mTextureId);
				// evil hack -- alloc buffer of specified size and feed in
//				fprintf (stderr, "restoring empty texture: %s (%dx%d )\n", mpContainer->name (), mWidth, mHeight);
				heapBuffer<char> buf (mWidth*mHeight*4);
				memset (buf, 0xfe, mWidth*mHeight*4);
				setFiltering (false);
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
				glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
				glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf);
			    GLboolean ist = glIsTexture (mTextureId);
			}
			else
				mpContainer->createFromFile ();
			mbLost = false;
		}
	}
	
	#define FE_MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
		 ((ulong) (uchar) (ch0) | ( (ulong) (uchar) (ch1) << 8) |   \
		 ( (ulong) (uchar) (ch2) << 16) | ( (ulong) (uchar) (ch3) << 24))
	
	#define FOURCC_DXT1 (FE_MAKEFOURCC ('D','X','T','1'))
	#define FOURCC_DXT2 (FE_MAKEFOURCC ('D','X','T','2'))
	#define FOURCC_DXT3 (FE_MAKEFOURCC ('D','X','T','3'))
	#define FOURCC_DXT4 (FE_MAKEFOURCC ('D','X','T','4'))
	#define FOURCC_DXT5 (FE_MAKEFOURCC ('D','X','T','5'))
	typedef struct
	{
	    ulong       dwColorSpaceLowValue;   // low boundary of color space that is to
	                                        // be treated as Color Key, inclusive
	    ulong       dwColorSpaceHighValue;  // high boundary of color space that is
	                                        // to be treated as Color Key, inclusive
	} ddColorKey_t;
	
	typedef struct
	{
	    ulong       dwCaps;         // capabilities of surface wanted
	    ulong       dwCaps2;
	    ulong       dwCaps3;
	    union
	    {
	        ulong       dwCaps4;
	        ulong       dwVolumeDepth;
	    } voldepth_union;
	} ddsCaps2_t;
	
	typedef struct
	{
	    ulong       dwSize;                 // size of structure
	    ulong       dwFlags;                // pixel format flags
	    ulong       dwFourCC;               // (FOURCC code)
	    union
	    {
	        ulong   dwRGBBitCount;          // how many bits per pixel
	        ulong   dwYUVBitCount;          // how many bits per pixel
	        ulong   dwZBufferBitDepth;      // how many total bits/pixel in z buffer (including any stencil bits)
	        ulong   dwAlphaBitDepth;        // how many bits for alpha channels
	        ulong   dwLuminanceBitCount;    // how many bits per pixel
	        ulong   dwBumpBitCount;         // how many bits per "buxel", total
	        ulong   dwPrivateFormatBitCount;// Bits per pixel of private driver formats. Only valid in texture
	                                        // format list and if DDPF_D3DFORMAT is set
	    } rgbBitCount_union;
	    union
	    {
	        ulong   dwRBitMask;             // mask for red bit
	        ulong   dwYBitMask;             // mask for Y bits
	        ulong   dwStencilBitDepth;      // how many stencil bits (note: dwZBufferBitDepth-dwStencilBitDepth is total Z-only bits)
	        ulong   dwLuminanceBitMask;     // mask for luminance bits
	        ulong   dwBumpDuBitMask;        // mask for bump map U delta bits
	        ulong   dwOperations;           // DDPF_D3DFORMAT Operations
	    } rBitMask_union;
	    union
	    {
	        ulong   dwGBitMask;             // mask for green bits
	        ulong   dwUBitMask;             // mask for U bits
	        ulong   dwZBitMask;             // mask for Z bits
	        ulong   dwBumpDvBitMask;        // mask for bump map V delta bits
	        struct
	        {
	            ushort    wFlipMSTypes;       // Multisample methods supported via flip for this D3DFORMAT
	            ushort    wBltMSTypes;        // Multisample methods supported via blt for this D3DFORMAT
	        } MultiSampleCaps;
	
	    } gBitMaskUnion;
	    union
	    {
	        ulong   dwBBitMask;             // mask for blue bits
	        ulong   dwVBitMask;             // mask for V bits
	        ulong   dwStencilBitMask;       // mask for stencil bits
	        ulong   dwBumpLuminanceBitMask; // mask for luminance in bump map
	    } bBitMaskUnion;
	    union
	    {
	        ulong   dwRGBAlphaBitMask;      // mask for alpha channel
	        ulong   dwYUVAlphaBitMask;      // mask for alpha channel
	        ulong   dwLuminanceAlphaBitMask;// mask for alpha channel
	        ulong   dwRGBZBitMask;          // mask for Z channel
	        ulong   dwYUVZBitMask;          // mask for Z channel
	    } rgbAlphaBitMask_union;
	} ddPixelFormat_t;
	
	typedef struct
	{
	    ulong               dwSize;                 // size of the DDSURFACEDESC structure
	    ulong               dwFlags;                // determines what fields are valid
	    ulong               dwHeight;               // height of surface to be created
	    ulong               dwWidth;                // width of input surface
	    union
	    {
	        long            lPitch;                 // distance to start of next line (return value only)
	        ulong           dwLinearSize;           // Formless late-allocated optimized surface size
	    } pitch_union;
	    union
	    {
	        ulong           dwBackBufferCount;      // number of back buffers requested
	        ulong           dwDepth;                // the depth if this is a volume texture 
	    } depth_union;
	    union
	    {
	        ulong           dwMipMapCount;          // number of mip-map levels requestde
	                                                // dwZBufferBitDepth removed, use ddpfPixelFormat one instead
	        ulong           dwRefreshRate;          // refresh rate (used when display mode is described)
	        ulong           dwSrcVBHandle;          // The source used in VB::Optimize
	    } mipmap_union;
	    ulong               dwAlphaBitDepth;        // depth of alpha buffer requested
	    ulong               dwReserved;             // reserved
	    void*               lpSurface;              // pointer to the associated surface memory
	    union
	    {
	        ddColorKey_t      ddckCKDestOverlay;      // color key for destination overlay use
	        ulong           dwEmptyFaceColor;       // Physical color for empty cubemap faces
	    } ckey_union;
	    ddColorKey_t          ddckCKDestBlt;          // color key for destination blt use
	    ddColorKey_t          ddckCKSrcOverlay;       // color key for source overlay use
	    ddColorKey_t          ddckCKSrcBlt;           // color key for source blt use
	    union
	    {
	        ddPixelFormat_t   ddpfPixelFormat;        // pixel format description of the surface
	        ulong           dwFVF;                  // vertex format description of vertex buffers
	    } pixelformat_union;
	    ddsCaps2_t            ddsCaps;                // direct draw surface capabilities
	    ulong               dwTextureStage;         // stage in multitexture cascade
	} ddSurfaceDesc2_t;
	
	
	int
	textureData::loadDDS (const char *filename)
	{
		ddSurfaceDesc2_t ddsd;
		char filecode[4];
	
		smartPtr <fileSystem> fs = g_engine->getFileSystem ();
		
		file *f;
		/* try to open the file */
		f = fs->openFile (filename, FS_MODE_READ);
		if (!f)
		{
			return -1;
		}
		/* verify the type of file */
		f->read (filecode, 4);
		if (strncmp (filecode, "DDS ", 4) != 0) {
			f->close ();
			return -1;
		}
		/* get the surface desc */
		f->read (&ddsd, sizeof (ddsd));
	
		/* how big is it going to be including all mipmaps? */
		ulong bpp = (ddsd.pixelformat_union.ddpfPixelFormat.dwFourCC == FOURCC_DXT1) ? 3 : 4;
		ulong bufsize = f->getSize () - sizeof (ddsd);
	
//		heapBuffer<uchar> pixels (bufsize);
		stackAllocatorHandle salloc_handle;
		ubyte *pixels = (ubyte*)salloc_handle.alloc (bufsize);

		f->read ((uchar*)pixels, bufsize);
		/* close the file pointer */
		f->close ();
		ulong w = ddsd.dwWidth;
		ulong h = ddsd.dwHeight;
		ulong numMipmaps = ddsd.mipmap_union.dwMipMapCount;
		ulong format;
		mWidth = w;
		mHeight = h;
		if (!g_engine->getRenderer ())
			return 0;
	
		switch (ddsd.pixelformat_union.ddpfPixelFormat.dwFourCC)
		{
			case FOURCC_DXT1:
				format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
				break;
			case FOURCC_DXT3:
				format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
				break;
			case FOURCC_DXT5:
				format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
				break;
			default:
				return -1;
		}
		
		ulong blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16;
		ulong offset = 0;
		
		glGenTextures (1, &mTextureId);
		if (GLEW_ARB_multitexture)
			glActiveTextureARB (GL_TEXTURE0_ARB);
		glBindTexture (GL_TEXTURE_2D, mTextureId);
		setFiltering (numMipmaps > 1);
		/* load the mipmaps */
		for (ulong i = 0; i < numMipmaps && (w || h); ++i)
		{
			if (w == 0)
				w = 1;
			if (h == 0)
				h = 1;
			ulong size = ( (w+3)/4)* ((h+3)/4)*blockSize;
			//glCompressedTexImage2D (GL_TEXTURE_2D, i, format, w, h, 0, size, pixels + offset);
			offset += size;
			w >>= 1;
			h >>= 1;
		}
	
		return 0;
	}
	
	static void PNG_user_read_data (png_structp png_ptr, png_bytep data, png_size_t length)
	{
		file *f = (file *)png_ptr->io_ptr;
		f->read (data, (ulong)length);
	}
	
	int textureData::loadPNG (const char *filename)
	{
		png_structp png_ptr;
		png_infop info_ptr;
		unsigned int sig_read = 0;
		png_uint_32 width, height;
		int bit_depth, color_type, interlace_type;
		int number_passes;
		png_bytep *row_pointers;
		png_uint_32 row;
		int bpp;
		int err = 0;
	
		smartPtr <fileSystem> fs = g_engine->getFileSystem ();
		
		file *f = fs->openFile (filename, FS_MODE_READ);
		if (!f)	
			return -1;
		
		png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		
		if (png_ptr != NULL)
		{
			/* Allocate/initialize the memory for image information.  REQUIRED. */
			info_ptr = png_create_info_struct (png_ptr);
			if (info_ptr != NULL)
			{
				if (!setjmp (png_ptr->jmpbuf))
				{
					png_set_read_fn (png_ptr, (void *)f, PNG_user_read_data);
					
					png_set_sig_bytes (png_ptr, sig_read);
					
					png_read_info (png_ptr, info_ptr);
					
					png_get_IHDR (png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
						&interlace_type, NULL, NULL);
				
					mWidth = width;
					mHeight = height;
				
					if (!g_engine->getRenderer ())
					{
						png_destroy_read_struct (&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
						f->close ();
						return 0;
					}
					
					if (color_type == PNG_COLOR_TYPE_PALETTE)
						png_set_expand (png_ptr);
					
					if (png_get_valid (png_ptr, info_ptr, PNG_INFO_tRNS))
						png_set_expand (png_ptr);
					
					png_set_bgr (png_ptr);
					
					number_passes = png_set_interlace_handling (png_ptr);

					stackAllocatorHandle salloc_handle;

//					row_pointers=new uchar*[height];
					row_pointers = (uchar**)salloc_handle.alloc(sizeof(uchar*) * height);
					for (row = 0; row < height; row++)
//						row_pointers[row] = new uchar[png_get_rowbytes (png_ptr, info_ptr)];
						row_pointers[row] = (uchar*)salloc_handle.alloc(png_get_rowbytes (png_ptr, info_ptr));
					
					png_read_image (png_ptr, row_pointers);
	
					bpp=png_get_rowbytes (png_ptr, info_ptr)/width;
	
					// copy to bits
//					heapBuffer <uchar> bits (height*png_get_rowbytes (png_ptr, info_ptr));
					ubyte *bits = (ubyte*)salloc_handle.alloc (height * png_get_rowbytes (png_ptr, info_ptr));

					for (row = 0; row < height; row++)
					{
						memcpy (&bits[row*png_get_rowbytes (png_ptr, info_ptr)], row_pointers[row], png_get_rowbytes (png_ptr, info_ptr));
//						delete[] row_pointers[row];
					}
	
//					delete[] row_pointers;
	
					png_read_end (png_ptr, info_ptr);
					
					png_destroy_read_struct (&png_ptr, &info_ptr, (png_infopp)NULL);
					
					// create gl texture
					
					glGenTextures (1, &mTextureId);
	
					if (GLEW_ARB_multitexture)
						glActiveTextureARB (GL_TEXTURE0_ARB);
	
					glBindTexture (GL_TEXTURE_2D, mTextureId);
					setFiltering ();
					gluBuild2DMipmaps (GL_TEXTURE_2D, bpp, width, height, bpp == 3 ? GL_BGR : GL_BGRA, GL_UNSIGNED_BYTE, bits);
				}
				else
				{
					err = -1;
					png_destroy_read_struct (&png_ptr, &info_ptr, (png_infopp)NULL);
				}
			}
			else
			{
				err = -1;
				png_destroy_read_struct (&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
			}
		}
	
		f->close ();
	
		return err;
	}
	
	#define INPUT_BUF_SIZE  4096	/* choose an efficiently fread'able size */
	typedef struct
	{
		struct jpeg_source_mgr pub;		/* public fields */
		file *f;
		JOCTET * buffer;				/* start of buffer */
		boolean start_of_file;			/* have we gotten any data yet? */
	} fe_jpeg_src_mgr_t;
	
	/*
	 * Initialize source --- called by jpeg_read_header
	 * before any data is actually read.
	 */
	
	METHODDEF (void)
	fe_jpeg_init_source (j_decompress_ptr cinfo)
	{
	  fe_jpeg_src_mgr_t* src = (fe_jpeg_src_mgr_t*) cinfo->src;
	
	  /* We reset the empty-input-file flag for each image,
	   * but we don't clear the input buffer.
	   * This is correct behavior for reading a series of images from one source.
	   */
	  src->start_of_file = TRUE;
	}
	
	
	/*
	 * Fill the input buffer --- called whenever buffer is emptied.
	 *
	 * In typical applications, this should read fresh data into the buffer
	 * (ignoring the current state of next_input_byte & bytes_in_buffer),
	 * reset the pointer & count to the start of the buffer, and return TRUE
	 * indicating that the buffer has been reloaded.  It is not necessary to
	 * fill the buffer entirely, only to obtain at least one more byte.
	 *
	 * There is no such thing as an EOF return.  If the end of the file has been
	 * reached, the routine has a choice of ERREXIT () or inserting fake data into
	 * the buffer.  In most cases, generating a warning message and inserting a
	 * fake EOI marker is the best course of action --- this will allow the
	 * decompressor to output however much of the image is there.  However,
	 * the resulting error message is misleading if the real problem is an empty
	 * input file, so we handle that case specially.
	 *
	 * In applications that need to be able to suspend compression due to input
	 * not being available yet, a FALSE return indicates that no more data can be
	 * obtained right now, but more may be forthcoming later.  In this situation,
	 * the decompressor will return to its caller (with an indication of the
	 * number of scanlines it has read, if any).  The application should resume
	 * decompression after it has loaded more data into the input buffer.  Note
	 * that there are substantial restrictions on the use of suspension --- see
	 * the documentation.
	 *
	 * When suspending, the decompressor will back up to a convenient restart point
	 * (typically the start of the current MCU). next_input_byte & bytes_in_buffer
	 * indicate where the restart point will be if the current call returns FALSE.
	 * the front of the buffer rather than discarding it.
	 */
	
	METHODDEF (boolean)
	fe_jpeg_fill_input_buffer (j_decompress_ptr cinfo)
	{
	  fe_jpeg_src_mgr_t* src = (fe_jpeg_src_mgr_t*) cinfo->src;
	  size_t nbytes;
	
	  nbytes = src->f->read (src->buffer, INPUT_BUF_SIZE);
	
	  if (nbytes <= 0) {
	    if (src->start_of_file)	/* Treat empty input file as fatal error */
	      ERREXIT (cinfo, JERR_INPUT_EMPTY);
	    WARNMS (cinfo, JWRN_JPEG_EOF);
	    /* Insert a fake EOI marker */
	    src->buffer[0] = (JOCTET) 0xFF;
	    src->buffer[1] = (JOCTET) JPEG_EOI;
	    nbytes = 2;
	  }
	
	  src->pub.next_input_byte = src->buffer;
	  src->pub.bytes_in_buffer = nbytes;
	  src->start_of_file = FALSE;
	
	  return TRUE;
	}
	
	
	/*
	 * Skip data --- used to skip over a potentially large amount of
	 * uninteresting data (such as an APPn marker).
	 *
	 * Writers of suspendable-input applications must note that skip_input_data
	 * is not granted the right to give a suspension return.  If the skip extends
	 * beyond the data currently in the buffer, the buffer can be marked empty so
	 * that the next read will cause a fill_input_buffer call that can suspend.
	 * Arranging for additional bytes to be discarded before reloading the input
	 * buffer is the application writer's problem.
	 */
	
	METHODDEF (void)
	fe_jpeg_skip_input_data (j_decompress_ptr cinfo, long num_bytes)
	{
	  fe_jpeg_src_mgr_t* src = (fe_jpeg_src_mgr_t*) cinfo->src;
	
	  /* Just a dumb implementation for now.  Could use fseek () except
	   * it doesn't work on pipes.  Not clear that being smart is worth
	   * any trouble anyway --- large skips are infrequent.
	   */
	  if (num_bytes > 0) {
	    while (num_bytes > (long) src->pub.bytes_in_buffer) {
	      num_bytes -= (long) src->pub.bytes_in_buffer;
	      (void) fe_jpeg_fill_input_buffer (cinfo);
	      /* note we assume that fill_input_buffer will never return FALSE,
	       * so suspension need not be handled.
	       */
	    }
	    src->pub.next_input_byte += (size_t) num_bytes;
	    src->pub.bytes_in_buffer -= (size_t) num_bytes;
	  }
	}
	
	
	/*
	 * An additional method that can be provided by data source modules is the
	 * resync_to_restart method for error recovery in the presence of RST markers.
	 * For the moment, this source module just uses the default resync method
	 * provided by the JPEG library.  That method assumes that no backtracking
	 * is possible.
	 */
	
	
	/*
	 * Terminate source --- called by jpeg_finish_decompress
	 * after all data has been read.  Often a no-op.
	 *
	 * NB: *not* called by jpeg_abort or jpeg_destroy; surrounding
	 * application must deal with any cleanup that should happen even
	 * for error exit.
	 */
	
	METHODDEF (void)
	fe_jpeg_term_source (j_decompress_ptr cinfo)
	{
	  /* no work necessary here */
	}
	
	
	// jpeg file reading routines
	static void jpeg_fe_src (j_decompress_ptr cinfo, file *f)
	{
		cinfo->src = (struct jpeg_source_mgr *)
	      (*cinfo->mem->alloc_small) ( (j_common_ptr) cinfo, JPOOL_PERMANENT,
	                  sizeof (fe_jpeg_src_mgr_t));
	
	
		fe_jpeg_src_mgr_t *src = (fe_jpeg_src_mgr_t *)cinfo->src;
		src->buffer = (JOCTET *)
	      (*cinfo->mem->alloc_small) ( (j_common_ptr) cinfo, JPOOL_PERMANENT,
	                  INPUT_BUF_SIZE * sizeof (JOCTET));
		src->f = f;
	
		src->pub.init_source = fe_jpeg_init_source;
		src->pub.fill_input_buffer = fe_jpeg_fill_input_buffer;
		src->pub.skip_input_data = fe_jpeg_skip_input_data;
		src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
		src->pub.term_source = fe_jpeg_term_source;
		src->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
		src->pub.next_input_byte = NULL; /* until buffer loaded */
	}
	
	int		textureData::loadJPG (const char *filename)
	{
		long w, h;
		
		struct jpeg_decompress_struct cinfo;
		struct jpeg_error_mgr jerr;
		JSAMPARRAY buffer;		/* Output row buffer */
		int row_stride;			/* physical row width in output buffer */

		uchar *c;

		cinfo.err = jpeg_std_error (&jerr);
		jpeg_create_decompress (&cinfo);

		smartPtr <fileSystem> fs = g_engine->getFileSystem ();
		file *file = fs->openFile (filename, FS_MODE_READ);
		if (!file)
			return -1;

		jpeg_fe_src (&cinfo, file);

		(void) jpeg_read_header (&cinfo, TRUE);
		(void) jpeg_start_decompress (&cinfo);

		w=cinfo.output_width;
		h=cinfo.output_height;
		mWidth = w;
		mHeight = h;
		if (!g_engine->getRenderer ())
		{
			(void) jpeg_finish_decompress (&cinfo);
			jpeg_destroy_decompress (&cinfo);
			file->close ();
			return 0;
		}


		row_stride = cinfo.output_width * cinfo.output_components;
		//	buffer = (*cinfo.mem->alloc_sarray)	 ((j_common_ptr) &cinfo, JPOOL_IMAGE, row_stride, 1);

//		heapBuffer <uchar> img (w * h * 3);
		stackAllocatorHandle salloc_handle;
		ubyte *img = (ubyte*)salloc_handle.alloc (w * h * 3);

		while (cinfo.output_scanline < cinfo.output_height)
		{
			c=&img[ (cinfo.output_scanline)*cinfo.output_width*3];
			jpeg_read_scanlines (&cinfo, (uchar **)&c, 1);
		}

		(void) jpeg_finish_decompress (&cinfo);
		jpeg_destroy_decompress (&cinfo);

		file->close ();

#if 0
		/* convert bgr 2 rgb */
		rgbs=img;
		rgbd=bits;
		for (y=0; y<h; y++)
		{
			for (x=0; x<w; x++)
			{
				* (rgbd+0)=* (rgbs+2);
				* (rgbd+1)=* (rgbs+1);
				* (rgbd+2)=* (rgbs+0);

				rgbs+=3;
				rgbd+=3;
			}
		}
#endif

		// create gl tex
		glGenTextures (1, &mTextureId);
		if (GLEW_ARB_multitexture)
			glActiveTextureARB (GL_TEXTURE0_ARB);
		glBindTexture (GL_TEXTURE_2D, mTextureId);
		setFiltering ();
		gluBuild2DMipmaps (GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, img);


		return 0;
	}
	
	bool	textureData::loadTextureFromFile (const char *fname)
	{
	// FIXME: gl texture should behave the same way -- through vfs
		if (!g_engine->getRenderer ())
			return true;
	
		char name[100];
		const char *ext = strchr (fname, '.');
		if (ext)
		{
			strncpy (name, fname, ext-fname);
			name[ext - fname] = 0;
		}
		else
			strcpy (name, fname);
	
		if (loadDDS (name + cStr (".dds")))
		{
			if (loadPNG (name + cStr (".png")))
			{
				if (loadJPG (name + cStr (".jpg")))
				{
					fprintf (stderr, "WARNING: failed to load texture '%s'\n", fname);
					return false;
				}
			}
		}
		
		return true;
	}
	
	int					textureData::getTextureId (void) const
	{
		return mTextureId;
	}
	
	ulong				textureData::getWidth (void)
	{
		return mWidth;
	}
	
	ulong				textureData::getHeight (void)
	{
		return mHeight;
	}
	
	bool				textureData::isLost (void)
	{
		return mbLost;
	}
	
	uchar*				textureData::lock (uint lvl)
	{
		// on GL we can't really get a pointer to graphics surface
		// so emulate this behavior: alloc some mem, return to user, free on unlock
	
		if (lvl	>= MAX_TEX_MIPS)
			sys_error ("attemp to lock mip level %d, while max is %d\n", lvl, MAX_TEX_MIPS-1);
	
		// throw if already locked
		if (mLocks[lvl])
			sys_error ("texture::lockRect : texture %d is already locked at lvl %d", mTextureId, lvl);
		else
			sys_printf ("locked texture %d lvl %d\n", mTextureId, lvl);
		ulong w = getWidth () >> lvl;
		ulong h = getHeight () >> lvl;
		uchar *pixels = new uchar[w*h*4];
		mLocks[lvl] = pixels;
		glBindTexture (GL_TEXTURE_2D, mTextureId);
		glGetTexImage (GL_TEXTURE_2D, lvl, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		return pixels;
	}
	
	void				textureData::unlock (uint lvl)
	{
		GLboolean ist = glIsTexture (mTextureId);
	//	printf ("handle: %d, istexture: %d\n", mTextureId, ist);
		// throw if not locked
		if (lvl	>= MAX_TEX_MIPS)
			sys_error ("attemp to unlock mip level %d, while max is %d\n", lvl, MAX_TEX_MIPS-1);
		if (!mLocks[lvl])
			sys_error ("texture::unlockRect : texture %s is not locked at lvl %d", name (),  lvl);
		else
			sys_printf ("unlocked texture %d lvl %d\n", mTextureId, lvl);
	
		glEnable (GL_TEXTURE_2D);
		if (GLEW_ARB_multitexture)
			glActiveTextureARB (GL_TEXTURE0_ARB);
		glBindTexture (GL_TEXTURE_2D, mTextureId);
		glTexImage2D (GL_TEXTURE_2D, lvl, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mLocks[lvl]);
		delete[] mLocks[lvl];
		mLocks[lvl] = NULL;
	}
	
	void textureData::bind (int stage)
	{
		if (GLEW_ARB_multitexture)
			glActiveTextureARB (GL_TEXTURE0_ARB + stage);
		glBindTexture (GL_TEXTURE_2D, mTextureId);
		glEnable (GL_TEXTURE_2D);
	}
	
	GLuint		textureData::getObject (void) const
	{
		return mTextureId;
	}
	
	// FIXME: this ctor is somehow being required by smartPtr
	texture::texture (void)
	{
	}
	
	texture::texture (const char *name)
	{
		mName	= name;
		char stmp[1000];
		char *tmp = stmp;
		int w = -1, h = -1;
		
		strcpy (tmp, name);
	
		char *colon;
	
		colon = strchr (tmp, ':');
		if (colon)
		{
			*colon = 0;
			if (!strcmp (tmp, "empty"))
			{
				*colon = ':';
				tmp = colon + 1;
	
				colon = strchr (tmp, ':');
				if (!colon)
					sys_error ("texture ctor: width expected for '%s'", name);
				else
				{
					*colon = 0;
					w = atoi (tmp);
					*colon = ':';
					tmp = colon + 1;
	
					colon = strchr (tmp, ':');
					if (colon)
						*colon = 0;
					h = atoi (tmp);
				}
				// create empty
				mpTextureData = new textureData (this, w, h);
			}
			else
				sys_error ("texture ctor: invalid type id for '%s'", name);
		}
		else
		{
			mpTextureData = new textureData (this);
			createFromFile ();
		}
	}
	
	texture::texture (charParser &parser, const char *name)
	{
	}
	
	texture::~texture ()
	{
	}
	
	void texture::createFromFile (void)
	{
		if (mpTextureData->loadTextureFromFile (mName))
			mbValid = true;
		else
			mbValid = false;
	}
	
	void				texture::bind (int stage)
	{
		mpTextureData->bind (stage);
	}
	
	ulong				texture::getWidth () const
	{
		return mpTextureData->getWidth ();
	}
	
	ulong				texture::getHeight () const
	{
		return mpTextureData->getHeight ();
	}
	
	smartPtr <textureData>		texture::getTextureData (void) const
	{
		return mpTextureData;
	}
	
	lockedRect texture::lockRect (uint lvl, rect *rc, bool readOnly)
	{
		lockedRect r;
		r.pitch = (mpTextureData->getWidth () >> lvl) * 4;
		r.pBits = mpTextureData->lock (lvl);
	
		return r;
	}
	
	void texture::unlockRect (uint lvl)
	{
		mpTextureData->unlock (lvl);
	}
	
	int_ptr texture::getObject (void) const
	{
		return (int_ptr)mpTextureData->getObject ();
	}
	
}
