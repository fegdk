/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/
#ifndef __CGCONTEXT_H
#define __CGCONTEXT_H

#include <Cg/cgGL.h>
#include "f_baseobject.h"

namespace fe
{
	class cgContext : public baseObject
	{
	private:
		CGcontext	mCgContext;
	public:
		cgContext (void);
		~cgContext (void);

		CGcontext getContext (void) const;
	};
}
#endif // __CGCONTEXT_H
