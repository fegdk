/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_GLRENDERER_H
#define __F_GLRENDERER_H

#include "f_baserenderer.h"

namespace fe
{

	class cgContext;

	class FE_API glRenderer : public baseRenderer
	{
	private:
		bool mbInitialized;
		smartPtr <cgContext> mpCgContext;
	public:
		glRenderer (void);
		~glRenderer (void);
		void init (void);
		cgContext* getCgContext (void) const;
		void setViewport (int sx, int sy, int sw, int sh);
		void begin (void) const;
		void end (void) const;
		void clear (int numRects, const rect *rects, ulong flags, float4 color, float z, ulong stencil) const;
		void resize (void);
	
		virtual bool				isLost (void) const;
		virtual void				present (void);
	
		virtual ulong				numDisplayModes (ulong adapter, ulong device) const;
		virtual cStr				getDisplayModeDescr (ulong adapter, ulong device, ulong mode) const;
	
		virtual ulong				numDevices (ulong adapter) const;
		virtual cStr				getDeviceDescr (ulong adapter, ulong device) const;
	
		virtual ulong				numAdapters (void) const;
		virtual cStr				getAdapterDescr (ulong adapter) const;
	
		virtual ulong				getCurrentAdapter (void) const;
		virtual ulong				getCurrentDevice (ulong adapter) const;
		virtual ulong				getCurrentMode (ulong adapter, ulong device) const;
	
		virtual void				setWindow (baseViewport *vp);
	};
	
}

#endif // __F_GLRENDERER_H

