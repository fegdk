/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "config.h"
#include "f_cgshader.h"
#include "f_error.h"
#include "f_engine.h"
#include "f_helpers.h"
#include "f_glrenderer.h"
#include "f_filesystem.h"
#include "cgcontext.h"

#ifdef HAVE_LIBCG
#include "cgcontext.h"
namespace fe
{

	bool cgShader::checkCGError (void) const
	{
		CGerror err = cgGetError ();
	
		if (err != CG_NO_ERROR)
		{
			fprintf (stderr, "%d: %s\n", err, cgGetErrorString (err));
	//		fprintf (stderr, "name=%s, profile=%d\n", name(), mProfile);
	//		fprintf (stderr, "cgGLIsProfileSupported(%d)=%s\n", mProfile, cgGLIsProfileSupported(mProfile) ? "true" : "false");
			return true;
		}
		return false;
	}
	
	void cgShader::loose (void)
	{
		unbind ();
		if (mProgram)
		{
			cgDestroyProgram (mProgram);
			mProgram = NULL;
		}
		memset (&mDSBinding, 0, sizeof (mDSBinding));
		mbValid = false;
	}
	
	void cgShader::restore (void)
	{
		mbValid = true;
	
		cStr filename;
		cStr entry;
		cStr profile_name;
	
		// parse fname
		const char *colon;
	
		colon = strchr (name (), ':');
		if (!colon)
		{
			mbValid = false;
			return;
		}
		filename = cStr (name (), colon-name ());
	
		colon++;
		const char *entryptr = colon;
		colon = strchr (colon, ':');
		if (!entryptr)
		{
			mbValid = false;
			return;
		}
		if (!colon)
		{
			entry = entryptr;
			fprintf (stderr, "ERROR: shader profile is unspecified, defaulting to bestvp\n");
			profile_name = "bestvp";
		}
		else
		{
			entry = cStr (entryptr, colon-entryptr);
			colon++;
			profile_name = colon;
		}
	
		const char *profile_strings[] = { "bestvp", "arbvp1", "vp20", "vp30"
	#if (CG_VERSION_NUM>=1300)
		, "vp40"
	#endif
	#if (CG_VERSION_NUM>=1500)
		, "glslv"
	#endif
		, "bestfp", "arbfp1", "fp20", "fp30"
	#if (CG_VERSION_NUM>=1300)
		, "fp40"
	#endif
	#if (CG_VERSION_NUM>=1500)
		, "glslf"
	#endif
		, NULL };
	
		const CGprofile valid_profiles[] = {(CGprofile)best_vertex_profile, CG_PROFILE_ARBVP1, CG_PROFILE_VP20, CG_PROFILE_VP30
	#if (CG_VERSION_NUM>=1300)
		, CG_PROFILE_VP40
	#endif
	#if (CG_VERSION_NUM>=1500)
		, CG_PROFILE_GLSLV
	#endif
		, (CGprofile)best_fragment_profile, CG_PROFILE_ARBFP1, CG_PROFILE_FP20, CG_PROFILE_FP30
	#if (CG_VERSION_NUM>=1300)
		, CG_PROFILE_FP40
	#endif
	#if (CG_VERSION_NUM>=1500)
		, CG_PROFILE_GLSLF
	#endif
		};
	
		int prof;
		for (prof = 0; profile_strings[prof]; prof++)
		{
			if (!strcmp (profile_strings[prof], profile_name))
				break;
		}
		if (profile_strings[prof])
		{
			mProfile = valid_profiles[prof];
		}
		else
		{
			mbValid = false;
			return;
		}
	
		mSourceType = source_textfile;
		mProgram = 0;
		
	
	
		if (mProfile == (CGprofile)best_vertex_profile)
			mProfile = cgGLGetLatestProfile (CG_GL_VERTEX);
		else if (mProfile == (CGprofile)best_fragment_profile)
			mProfile = cgGLGetLatestProfile (CG_GL_FRAGMENT);
		if (checkCGError ())
		{
			mbValid = false;
			return;
		}
		
		CGenum cgSource;
	
		char *code = NULL;
		switch (mSourceType)
		{
		case source_text:
			code = (char*)name ();
			cgSource = CG_SOURCE;
			break;
		case source_binary:
			code = (char*)name ();
			cgSource = CG_OBJECT;
			break;
		case source_textfile:
		case source_binaryfile:
			{
				cgSource = mSourceType == source_textfile ? CG_SOURCE : CG_OBJECT;
				file *f = g_engine->getFileSystem ()->openFile (filename, FS_MODE_READ);
				if (!f)
					sys_error ("invalid file: %s", filename.c_str ());
				code = new char[f->getSize () + 1];
				f->read (code, f->getSize ());
				code[f->getSize ()] = 0;
				f->close ();
			}
			break;
		default:
			mbValid = false;
			return;
		}
		
		CGcontext context = checked_cast <glRenderer*>(g_engine->getRenderer ())->getCgContext ()->getContext ();
		if (!context)
		{
			mbValid = false;
			if (mSourceType == source_textfile || mSourceType == source_binaryfile)
				delete[] code;
			return;
		}
		cgGLSetOptimalOptions (mProfile);
	
		mProgram = cgCreateProgram (context, cgSource, code, mProfile, entry, NULL);
	
		if (!mProgram || checkCGError ())
		{
			mbValid = false;
			fprintf (stderr, "shader failed. errors: '%s'\n", cgGetLastListing (context));
		}
		if (mSourceType == source_textfile || mSourceType == source_binaryfile)
			delete[] code;
		if (!mbValid)
			return;
		
		if (!mProgram)
		{
			mbValid = false;
			return;
		}
	
		cgGLLoadProgram (mProgram);
		if (checkCGError ())
		{
			mbValid = false;
			return;
		}
		mDSBinding.pos = parmByName ("position");
		mDSBinding.norm = parmByName ("normal");
		mDSBinding.color = parmByName ("color");
		mDSBinding.tex0 = parmByName ("texcoord0");
		mDSBinding.tex1 = parmByName ("texcoord1");
		mDSBinding.tangent = parmByName ("tangent");
		mDSBinding.binormal = parmByName ("binormal");
	}
	
	cgShader::cgShader (const char *fname)
	{
		mName = fname;
		restore ();
	}
	
	cgShader::cgShader (charParser &parser, const char *name)
		: shader (parser, name)
	{
	}
	
	cgShader::~cgShader (void)
	{
		loose ();
	}
	
	void cgShader::save (const char *fname) const
	{
	}
	
	void cgShader::bind (void) const
	{
		cgGLEnableProfile (mProfile);
		if (!checkCGError ())
		{
			cgGLBindProgram (mProgram);
			checkCGError ();
		}
	}
	
	void cgShader::unbind (void) const
	{
		cgGLUnbindProgram (mProfile);
		cgGLDisableProfile (mProfile);
	}
	
	handle cgShader::parmByName (const char *name)
	{
		return (handle)cgGetNamedParameter(mProgram, name);
	}
	
	void cgShader::setFloat (handle parm, const float value)
	{
		cgGLSetParameter1f ((CGparameter)parm, value);
	}
	
	void cgShader::setFloat4 (handle parm, const float4 &value)
	{
		cgGLSetParameter4fv ((CGparameter)parm, (const float *)&value);
	}
	
	void cgShader::setMatrix (handle parm, const matrix4 &value)
	{
		cgGLSetMatrixParameterfc ((CGparameter)parm, (const float *)&value);
	}
	
	void cgShader::setTexture (handle parm, const smartPtr <baseTexture> &tex)
	{
		int_ptr obj = tex->getObject ();
		cgGLSetTextureParameter ((CGparameter)parm, (GLuint)obj);
		cgGLEnableTextureParameter ((CGparameter)parm);
	}
	
	void cgShader::enableStream (handle stream)
	{
		cgGLEnableClientState ((CGparameter)stream);
	}
	
	void cgShader::disableStream (handle stream)
	{
		cgGLDisableClientState ((CGparameter)stream);
	}
	
	void cgShader::bindStreamData (handle stream, int numComponents, streamParmType parmType, int stride, void *data)
	{
		static const GLuint mapping[] = {GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, GL_UNSIGNED_INT, GL_FLOAT, GL_2_BYTES, GL_3_BYTES, GL_4_BYTES, GL_DOUBLE, GL_DOUBLE_EXT};
		cgGLSetParameterPointer ((CGparameter)stream, numComponents, mapping[parmType], stride, data);
	}
	
}
#endif
