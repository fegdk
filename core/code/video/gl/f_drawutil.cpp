/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

// $Id$

// opengl implementation of drawUtil
// experimental and kinda slow

#include "pch.h"
#include "f_types.h"
#include "f_drawutil.h"
#include "f_baseviewport.h"
#include "f_math.h"
#include <GL/glew.h>
#include <GL/glu.h>
#include "f_basetexture.h"
#include "f_engine.h"
#include "f_helpers.h"
#include "f_glrenderer.h"
#include "f_console.h"

namespace fe
{

	struct vertex
	{
		vector3 pos;
		ulong color;
		vector2 uv;
		
		vertex (const vector3 &_pos, ulong _color, const vector2 &_uv)
		{
			pos = _pos;
			color = _color;
			uv = _uv;
		}
	};
	
	drawUtil::drawUtil (void)
	{
		mbAllowTransforms = true;
		mZValue = 1.f;
	}
	
	void drawUtil::allowTransforms (bool onoff)
	{
		mbAllowTransforms = onoff;
	}
	
	void drawUtil::setOrthoTransforms (void)
	{
		if (mbAllowTransforms)
		{
			smartPtr <baseViewport> vp = g_engine->getViewport ();
			glMatrixMode (GL_PROJECTION);
			glLoadIdentity ();
			glOrtho (0, vp->getWidth (), 0, vp->getHeight (), -1, 1);
			glMatrixMode (GL_MODELVIEW);
			glLoadIdentity ();
		}
	}
	
	void drawUtil::drawPic (float x, float y, float w, float h, float s1, float t1, float s2, float t2, float4 color, bool alpha)
	{
		smartPtr <baseViewport> vp = g_engine->getViewport ();
		int height = vp->getHeight ();
		
		setOrthoTransforms ();
		glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	
		if (alpha)
		{
			glEnable (GL_BLEND);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		else
			glDisable (GL_BLEND);
	
		glBegin (GL_TRIANGLE_STRIP);
		glColor4fv ( (GLfloat*)&color);
		glTexCoord2f (s1, t1);
		glVertex3f (x, height-y, mZValue);
		glTexCoord2f (s2, t1);
		glVertex3f (x+w, height-y, mZValue);
		glTexCoord2f (s1, t2);
		glVertex3f (x, height-y-h, mZValue);
		glTexCoord2f (s2, t2);
		glVertex3f (x+w, height-y-h, mZValue);
		glEnd ();
	}
	
	void drawUtil::drawLine (float x1, float y1, float x2, float y2, float4 color, bool alpha)
	{
		setOrthoTransforms ();
		glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glDisable (GL_TEXTURE_2D);
		glBegin (GL_LINES);
		glColor4fv ((GLfloat*)&color);
		glVertex2f (x1, y1);
		glVertex2f (x2, y2);
		glEnd ();
		glEnable (GL_TEXTURE_2D);
	}
	
	
	void drawUtil::drawPic (float x, float y, float w, float h, float s1, float t1, float s2, float t2, float4 color, baseTexturePtr tex, bool alpha)
	{
		smartPtr <baseViewport> vp = g_engine->getViewport ();
		int height = vp->getHeight ();
		glEnable (GL_TEXTURE_2D);
		tex->bind (0);
		
		setOrthoTransforms ();
		glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	
		if (alpha)
		{
			glEnable (GL_BLEND);
			glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		else
			glDisable (GL_BLEND);
	
		glBegin (GL_TRIANGLE_STRIP);
		glColor4fv ((GLfloat*)&color);
		glTexCoord2f (s1, t1);
		glVertex3f (x, height-y, mZValue);
		glTexCoord2f (s2, t1);
		glVertex3f (x+w, height-y, mZValue);
		glTexCoord2f (s1, t2);
		glVertex3f (x, height-y-h, mZValue);
		glTexCoord2f (s2, t2);
		glVertex3f (x+w, height-y-h, mZValue);
		glEnd ();
	}
	
	void drawUtil::fastDrawBegin (void)
	{
	}
	
	void drawUtil::fastDrawEnd (void)
	{
	}
	
	void drawUtil::ambientPass (bool onoff)
	{
		if (onoff)
		{
			glDepthMask (GL_TRUE);
			glColor4f (0.1, 0.1, 0.1, 1);
			glDepthFunc (GL_LEQUAL);
		}
		else
		{
			glDepthMask (GL_FALSE);
			glColor4f (1, 1, 1, 1);
		}
	}
	
	/*void drawUtil::setSVDepthStencilFuncs (bool onoff)
	{
		if (onoff)
		{
			glEnable (GL_STENCIL_TEST);
			glStencilFunc (GL_ALWAYS, 0, ~0);
			glEnable (GL_DEPTH_TEST);
			glDepthFunc (GL_LESS);
			glColorMask (GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
			glDepthMask (GL_FALSE);
		}
		else
		{
			glDisable (GL_STENCIL_TEST);
			glDepthFunc (GL_LEQUAL);
			glColorMask (GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
			glDepthMask (GL_TRUE);
		}
	}*/
	
	void drawUtil::enableStencilTest (bool onoff) const
	{
		if (onoff)
		{
			glEnable (GL_STENCIL_TEST);
		}
		else
		{
			glDisable (GL_STENCIL_TEST);
		}
	}
	
	void drawUtil::setStencilFunc (drawUtil::cmp func) const
	{
		GLenum funcs[] = {GL_NEVER, GL_LESS, GL_EQUAL, GL_LEQUAL, GL_GREATER, GL_NOTEQUAL, GL_GEQUAL, GL_ALWAYS};
		glStencilFunc (funcs[func], 0, ~0);
	}
	
	void drawUtil::enableZWrite (bool onoff) const
	{
		glDepthMask (onoff ? GL_TRUE : GL_FALSE);
	}
	
	void drawUtil::enableZTest (bool onoff) const
	{
		if (onoff)
			glEnable (GL_DEPTH_TEST);
		else
			glDisable (GL_DEPTH_TEST);
	}
	
	void drawUtil::setZFunc (drawUtil::cmp func) const
	{
		GLenum funcs[] = {GL_NEVER, GL_LESS, GL_EQUAL, GL_LEQUAL, GL_GREATER, GL_NOTEQUAL, GL_GEQUAL, GL_ALWAYS};
		glDepthFunc (funcs[func]);
	}
	
	void drawUtil::enableColorWrite (bool onoff) const
	{
		if (onoff)
			glColorMask (GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		else
			glColorMask (GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	}
	
	void drawUtil::setStencilZOps (stencilop zfail, stencilop zpass) const
	{
		GLenum ops[] = {GL_KEEP, GL_INCR, GL_DECR};
		glStencilOp (GL_KEEP, ops[zfail], ops[zpass]);
	}
	
	void drawUtil::setTwoSidedStencilZOps (stencilop zfail_front, stencilop zpass_front, stencilop zfail_back, stencilop zpass_back) const
	{
		if (GLEW_EXT_stencil_two_side && GLEW_EXT_stencil_wrap)
		{
			GLenum ops[] = {GL_KEEP, GL_INCR, GL_DECR, GL_INCR_WRAP_EXT, GL_DECR_WRAP_EXT};
			glActiveStencilFaceEXT (GL_FRONT);
			glStencilOp (GL_KEEP, ops[zfail_front], ops[zpass_front]);
			glActiveStencilFaceEXT (GL_BACK);
			glStencilOp (GL_KEEP, ops[zfail_back], ops[zpass_back]);
		}
	}
	
	void drawUtil::setCullMode (drawUtil::cullmode mode) const
	{
		glCullFace (mode == cull_front ? GL_FRONT : GL_BACK);
	}
	
	void drawUtil::setStencilOps (drawUtil::stencilop fail, drawUtil::stencilop zfail, drawUtil::stencilop zpass) const
	{
		GLenum ops[] = {GL_KEEP, GL_INCR, GL_DECR};
		glStencilOp (ops[fail], ops[zfail], ops[zpass]);
	}
	
	void drawUtil::lightingPass (bool onoff)
	{
		if (onoff)
		{
			glColor4f (1, 1, 1, 1);
			glEnable (GL_BLEND);
			glBlendFunc (GL_ONE, GL_ONE);
			glDepthFunc (GL_EQUAL);
		}
		else
		{
			glDisable (GL_BLEND);
			glDepthFunc (GL_LEQUAL);
			if (GLEW_ARB_multitexture)
			{
				glActiveTextureARB (GL_TEXTURE1_ARB);
				glDisable (GL_TEXTURE_2D);
				glActiveTextureARB (GL_TEXTURE0_ARB);
				glMatrixMode (GL_TEXTURE);
				glLoadIdentity ();
				glMatrixMode (GL_MODELVIEW);
			}
		}
	}
	
	void drawUtil::unbindTexture (int stage)
	{
		if (GLEW_ARB_multitexture)
		{
			glActiveTextureARB (stage);
		}
		glBindTexture (GL_TEXTURE_2D, 0);
	}
	
}
