/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_ps14shader.h"
#include "f_engine.h"
#include "f_filesystem.h"
#include "f_error.h"
#include "ps14toATIfs/ps14toATIfs.h"

namespace fe
{

	ps14Shader::ps14Shader (const char *fname)
	{
		setName (fname);
		restore ();
	}
	
	ps14Shader::ps14Shader (charParser &parser, const char *name)
	{
	}
	
	ps14Shader::~ps14Shader (void)
	{
		loose ();
	}
	
	void ps14Shader::loose (void)
	{
		mbValid = false;
		if (mShaderId != (GLuint)-1)
		{
			glDeleteFragmentShaderATI (mShaderId);
			mShaderId = (GLuint)-1;
		}
	}
	
	void ps14Shader::restore (void)
	{
		glEnable (GL_FRAGMENT_SHADER_ATI);
		mShaderId = glGenFragmentShadersATI (1);
		glBindFragmentShaderATI (mShaderId);
		glBeginFragmentShaderATI ();
	
		file *f = g_engine->getFileSystem ()->openFile (mName, FS_MODE_READ);
		if (!f)
			sys_error ("invalid file: %s", mName.c_str ());
		char *code = new char[f->getSize () + 1];
		f->read (code, f->getSize ());
		code[f->getSize ()] = 0;
		f->close ();
	
		int rc = ps14toATIFragmentShader (code);
		glEndFragmentShaderATI ();
	
		delete[] code;
		if (rc)
		{
			int errc = ps14toATIfs_GetErrorCount ();
			for (int i = 0; i < errc; ++i)
				fprintf (stderr, "%s\n", ps14toATIfs_GetErrorString (i));
		}
		else
			mbValid = true;
		glDisable (GL_FRAGMENT_SHADER_ATI);
	}
	
	void ps14Shader::bind (void) const
	{
		glEnable (GL_FRAGMENT_SHADER_ATI);
		glBindFragmentShaderATI (mShaderId);
	}
	
	void ps14Shader::unbind (void) const
	{
		glDisable (GL_FRAGMENT_SHADER_ATI);
	}
	
	handle ps14Shader::parmByName (const char *name)
	{
		// shader parms are:
		//  constants are values from 0 to NUM_FRAGMENT_CONSTANTS (0..7) named using "c" prefix
		//  samplers (textures) are values 100..199 named using "t" prefix
		switch (name[0])
		{
		case 'c':
			return (handle)(atoi (name+1));
		case 't':
			return (handle)(atoi (name+1) + 100);
		default:
			sys_error ("unknown parameter name passed to ps14shader: %s", name);
		}
		return NULL;
	}
	
	void ps14Shader::setFloat (handle prm, const float value)
	{
		// can only be constant reg
		float4 f (value, value, value, value);
		int parm = (int)prm;
		if (parm >= 0 && parm <= 99)
			glSetFragmentShaderConstantATI (GL_CON_0_ATI+parm, (const GLfloat*)&f);
		else
			sys_error ("app attempted to set value of invalid ps14 constant c%d", parm);
	}
	
	void ps14Shader::setFloat4 (handle prm, const float4 &value)
	{
		int parm = (int)prm;
		// can only be constant reg
		if (parm >= 0 && parm <= 99)
			glSetFragmentShaderConstantATI (GL_CON_0_ATI+parm, (const GLfloat*)&value);
		else
			sys_error ("app attempted to set value of invalid ps14 constant c%d", parm);
	}
	
	void ps14Shader::setMatrix (handle prm, const matrix4 &value)
	{
		int parm = (int)prm;
		// can only be constant reg
		if (parm >= 0 && parm <= 99)
		{
			matrix4 mtx = value.transpose ();
			for (int i = 0; i < 0; i++)
				glSetFragmentShaderConstantATI (GL_CON_0_ATI+parm+i, (const GLfloat*)mtx.m[i]);
		}
		else
			sys_error ("app attempted to set value of invalid ps14 constant c%d", parm);
	}
	
	void ps14Shader::setTexture (handle prm, const smartPtr <baseTexture> &tex)
	{
		int parm = (int)prm;
		// sampler
		if (parm >= 100 && parm <= 199)
			tex->bind (parm-100);
		else
			sys_error ("app attempted to set value of invalid ps14 texture t%d", parm);
	}
	
	void ps14Shader::save (const char *fname) const
	{
	}
	
	void ps14Shader::enableStream (handle stream)
	{
	}
	
	void ps14Shader::disableStream (handle stream)
	{
	}
	
	void ps14Shader::bindStreamData (handle stream, int numComponents, streamParmType parmType, int stride, void *data)
	{
	}
	
}
