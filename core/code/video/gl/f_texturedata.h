/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_TEXTUREDATA_H
#define __F_TEXTUREDATA_H

#include <GL/glew.h>
#include "f_types.h"
#include "f_fragileobject.h"

namespace fe
{

	class texture;
	const int MAX_TEX_MIPS = 12; // assume textures no larger than x8192
	
	class textureData : public fragileObject
	{
		
	private:
	
		texture*	mpContainer;
		uint		mTextureId;
		bool		mbLost;
		ulong		mWidth;
		ulong		mHeight;
		uchar *mLocks[MAX_TEX_MIPS];
	
		void		loose (void);
		void		restore (void);
	
		// adopted nvidia code for loading dds files
		int 		loadDDS (const char *filename);
		int			loadPNG (const char *filename);
		int			loadJPG (const char *filename);
	
		void	setFiltering (bool mips = true);
	public:
	
		textureData (texture *container);
		textureData (texture *container, ulong w, ulong h);
		~textureData (void);
	
		bool				loadTextureFromFile (const char *fname);
	
		// texture loading routines
		
		int					getTextureId (void) const;
		ulong				getWidth (void);
		ulong				getHeight (void);
		bool				isLost (void);
		uchar*				lock (uint lvl);
		void				unlock (uint lvl);
		void				bind (int stage);
		GLuint				getObject (void) const;
	};
	
}

#endif

