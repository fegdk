#include "pch.h"
#include "backend.h"
#include "f_renderablesceneobject.h"
#include "f_engine.h"
#include "f_scenemanager.h"
#include "cvars.h"

// FIXME: opengl-specific code must be moved into video/gl/glbackend.cpp
#include <GL/glew.h>
#include <GL/gl.h>
#include "f_baserenderer.h"

namespace fe
{
	float4 g_ambientcolor (0.2, 0.2, 0.2, 1);

	cvar_t *gl_polygon_offset_units_line;
	cvar_t *gl_polygon_offset_factor_line;

	void backend_draw_surf (drawSurf_t *surf)
	{
		if (!surf->obj)
			return;
		bool use_vbo = GLEW_ARB_vertex_buffer_object && r_use_vbo->ivalue && surf->mesh->vbo && surf->mesh->ibo;
		uchar *inds = NULL;
		if (!use_vbo)
			inds = surf->mesh->inds;
		if (surf->mesh->inds || surf->mesh->ibo)
		{
			glDrawElements (surf->mesh->primType, surf->mesh->numInds, GL_UNSIGNED_SHORT, ((ushort*)inds) + surf->mesh->firstIndex);
		}
		else
		{
			glDrawArrays (surf->mesh->primType, 0, surf->mesh->numVerts);
		}
	}
	

	void rBackendAmbient::draw (renderable **objects, int numobjects, drawSurf_t **drawsurfs, int numds)
	{
		glColor4f (1,1,1,1);
		glDisable (GL_BLEND);
		glDepthMask (GL_TRUE);
		glDepthFunc (GL_LEQUAL);
		g_engine->getEffectParms ()->setFloat4 (effectParm_AmbientColor, g_ambientcolor);
		int i;
		sceneManagerPtr scene = g_engine->getSceneManager ();
		sceneObject *prev = NULL;
		effectParmsPtr parms = g_engine->getEffectParms ();
		rBackend *be = g_engine->getRBackend ();

		for (i = 0; i < numds; i++)
		{
			drawSurf_t *surf = drawsurfs[i];
			be->setCurrentDrawSurf (surf);
			if (surf->mtl)
			{
				scene->setCurrentMtl (surf->mtl);
				material::effectRef *effect = surf->mtl->getEffectRef ();
				if (effect && effect->ambient != -1 && effect->fx)
				{
					effect->fx->setTechnique (effect->ambient);

					// prepare obj-specific states
					if (surf->obj != prev)
					{
						parms->setMatrix (effectParm_WorldMatrix, surf->obj->getWorldMatrix ());
						// FIXME: lightmap can differ in one object
						//							parms->setTexture (effectParm_LightmapTexture, surf->lightmap);
						surf->mtl->setupEffectInputs ();
						prev = surf->obj;
					}

					int np = effect->fx->begin ();
					for (int p = 0; p < np; p++)
					{
						effect->fx->pass (p);
						backend_draw_surf (surf);
						effect->fx->finalizePass (p);
					}
					effect->fx->end ();
				}
			}
		}
		be->setCurrentDrawSurf (NULL);
		glDepthMask (GL_FALSE);
	}

	void rBackendLight::draw (renderable **objects, int numobjects, drawSurf_t **drawsurfs, int numds)
	{
		glEnable (GL_BLEND);
		glBlendFunc (GL_ONE, GL_ONE);
		glDepthFunc (GL_EQUAL);
		vector3 ldir(1,1,1);
		ldir.normalize ();
		g_engine->getEffectParms ()->setFloat4 (effectParm_LightDir0, float4 (ldir.x, ldir.y, ldir.z, 0));
		g_engine->getEffectParms ()->setFloat4 (effectParm_LightDiffuse0, float4 (0.7, 0.7, 0.7, 1.0));
		int i;
		sceneManagerPtr scene = g_engine->getSceneManager ();
		sceneObject *prev = NULL;
		effectParmsPtr parms = g_engine->getEffectParms ();
		rBackend *be = g_engine->getRBackend ();

		for (i = 0; i < numds; i++)
		{
			drawSurf_t *surf = drawsurfs[i];
			be->setCurrentDrawSurf (surf);
			if (surf->mtl)
			{
				scene->setCurrentMtl (surf->mtl);
				material::effectRef *effect = surf->mtl->getEffectRef ();
				if (effect && effect->directlight != -1 && effect->fx)
				{
					effect->fx->setTechnique (effect->directlight);

					// prepare obj-specific states
					if (surf->obj != prev)
					{
						parms->setMatrix (effectParm_WorldMatrix, surf->obj->getWorldMatrix ());
						surf->mtl->setupEffectInputs ();
						prev = surf->obj;
					}

					int np = effect->fx->begin ();
					for (int p = 0; p < np; p++)
					{
						effect->fx->pass (p);
						backend_draw_surf (surf);
						effect->fx->finalizePass (p);
					}
					effect->fx->end ();
				}
			}
		}
		be->setCurrentDrawSurf (NULL);
		glDisable (GL_BLEND);
		glDepthFunc (GL_LEQUAL);
		if (GLEW_ARB_multitexture)
		{
			glActiveTextureARB (GL_TEXTURE1_ARB);
			glDisable (GL_TEXTURE_2D);
			glActiveTextureARB (GL_TEXTURE0_ARB);
			glMatrixMode (GL_TEXTURE);
			glLoadIdentity ();
			glMatrixMode (GL_MODELVIEW);
		}
	}

	void rBackendWire::draw (renderable **objects, int numobjects, drawSurf_t **drawsurfs, int numds)
	{
		glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
		glDisable (GL_TEXTURE_2D);
		glDepthFunc (GL_LEQUAL);
		glColor4f (1,1,1,1);
		glPolygonOffset (gl_polygon_offset_factor_line->fvalue, gl_polygon_offset_units_line->fvalue);
		glEnable (GL_POLYGON_OFFSET_LINE);
		int i;
		sceneManagerPtr scene = g_engine->getSceneManager ();
		sceneObject *prev = NULL;
		effectParmsPtr parms = g_engine->getEffectParms ();
		rBackend *be = g_engine->getRBackend ();

		for (i = 0; i < numds; i++)
		{
			drawSurf_t *surf = drawsurfs[i];
			be->setCurrentDrawSurf (surf);
			if (surf->mtl)
			{
				scene->setCurrentMtl (surf->mtl);
				material::effectRef *effect = surf->mtl->getEffectRef ();
				if (effect && effect->ambient != -1 && effect->fx)
				{
					effect->fx->setTechnique (effect->ambient);

					// prepare obj-specific states
					if (surf->obj != prev)
					{
						parms->setMatrix (effectParm_WorldMatrix, surf->obj->getWorldMatrix ());
						surf->mtl->setupEffectInputs ();
						prev = surf->obj;
					}

					int np = effect->fx->begin ();
					for (int p = 0; p < np; p++)
					{
						effect->fx->pass (p, fe::PASS_NO_DL | fe::PASS_NO_FP);
						backend_draw_surf (surf);
						effect->fx->finalizePass (p);
					}
					effect->fx->end ();
				}
			}
		}
		be->setCurrentDrawSurf (NULL);
		glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		glPolygonOffset (0, 0);
		glDisable (GL_POLYGON_OFFSET_LINE);
		glDepthFunc (GL_LEQUAL);
	}


	rBackend::rBackend (void)
	{
		memset (mObjects, 0, sizeof (mObjects));
		memset (mDrawSurfs, 0, sizeof (mDrawSurfs));
		memset (mPasses, 0, sizeof (mPasses));
		mNumObjects = 0;
		mNumDrawSurfs = 0;
		mNumPasses = 0;
//		mCurrentPass = 0;
		mpCurrentDrawSurf = NULL;

		addPass (&mAmbPass);
		addPass (&mLightPass);

		cvarManager *cvars = g_engine->getCVarManager ();
		gl_polygon_offset_factor_line = cvars->get ("gl_polygon_offset_factor_line", "-0.5", CVAR_STORE);
		gl_polygon_offset_units_line = cvars->get ("gl_polygon_offset_units_line", "0", CVAR_STORE);
	}

	rBackend::~rBackend (void)
	{
	}

	void rBackend::add (renderable *obj)
	{
		int i;
		int sz = obj->numDrawSurfs ();
		for (i = 0; i < sz; i++)
		{
			if (mNumDrawSurfs == R_BACKEND_MAX_DRAWSURFS)
			{
				fprintf (stderr, "ERROR: backend subset overflow\n");
				return;
			}
			mDrawSurfs[mNumDrawSurfs++] = obj->getDrawSurf (i);
		}
		if (mNumObjects == R_BACKEND_MAX_OBJECTS)
		{
			fprintf (stderr, "ERROR: backend object overflow\n");
			return;
		}
		mObjects[mNumObjects++] = obj;
	}
	
	static int cmp_ds (const drawSurf_t *a, const drawSurf_t *b)
	{
		if (a == b)
			return 0;
		return a>b?1:-1;
		// FIXME: this must be more intelligent
	}

	void rBackend::flush (void)
	{
		qsort (mDrawSurfs, mNumDrawSurfs, sizeof (drawSurf_t), (int (*)(const void *, const void *))cmp_ds);
		for (int n = 0; n < mNumPasses; n++)
		{
//			mCurrentPass = n;
			mPasses[n]->draw (mObjects, mNumObjects, mDrawSurfs, mNumDrawSurfs);
		}
		if (r_show_tris->ivalue)
		{
			mWirePass.draw (mObjects, mNumDrawSurfs, mDrawSurfs, mNumDrawSurfs);
		}
		mNumObjects = 0;
		mNumDrawSurfs = 0;
	}

/*	int rBackend::getCurrentPass (void) const
	{
		return mCurrentPass;
	}*/

	void rBackend::addPass (rBackendPass* pass)
	{
		if (mNumPasses == R_BACKEND_MAX_PASSES)
		{
			fprintf (stderr, "ERROR: backend pass overflow\n");
			return;
		}
		mPasses[mNumPasses++] = pass;
	}

	void rBackend::removePass (rBackendPass* pass)
	{
		for (int i = 0; i < mNumPasses; i++)
		{
			if (mPasses[i] == pass)
			{
				if (i != mNumPasses-1)
					memmove (&mPasses[i], &mPasses[i+1], (mNumPasses-i-1) * sizeof (rBackendPass*));
				mNumPasses--;
			}
		}
	}

}
