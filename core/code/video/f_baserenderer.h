/*
    gdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_BASERENDERER_H
#define __F_BASERENDERER_H

#include "f_baseobject.h"
#include "f_types.h"

namespace fe
{
	struct cvar_t;
	extern cvar_t *r_fullscreen;
	extern cvar_t *r_vidmode;
	extern cvar_t *r_show_tris;
	extern cvar_t *r_loadsurfaceparms;
	extern cvar_t *r_use_vbo;
	extern cvar_t *r_texture_filtering;
	extern cvar_t *r_use_mips;
	extern cvar_t *r_metashader;
	
	class fragileObjectRegistry;
	class rendererData;
	class baseViewport;
	
	/**
	 * BaseRenderer class is not needed in normal situation,
	 * but we will need it for some kinds of environments
	 * where we need to override engine's internal renderers.
	 */
	
	// null renderer, should work on any platform
	class baseRenderer : public baseObject
	{
	private:
	
		fragileObjectRegistry*		mpFragileObjectRegistry;
	
	public:
		
		enum clearFlags { clear_none = 0, clear_target = 1, clear_z = 2, clear_stencil = 4 };
	
		baseRenderer (void);
		virtual ~baseRenderer (void);
	
		virtual void init (void) = 0;
	
		/**
		 * @return device state (can be always false on consoles and such)
		 */
		virtual bool				isLost (void) const = 0;
	
		/**
		 * presents current viewport
		 */
		virtual void				present (void) = 0;
	
		/**
		 * resizes to current viewport size
		 */
		virtual void				resize (void) = 0;
	
		/**
		 * resets device to current settings
		 */
		virtual void				reset (void);
	
		/**
		 * display mode list access (user readable form)
		 */
		virtual ulong				numDisplayModes (ulong adapter, ulong device) const = 0;
		virtual cStr				getDisplayModeDescr (ulong adapter, ulong device, ulong mode) const = 0;
	
		// device list access (user readable form)
		virtual ulong				numDevices (ulong adapter) const = 0;
		virtual cStr				getDeviceDescr (ulong adapter, ulong device) const = 0;
	
		// adapter list access (user readable form)
		virtual ulong				numAdapters (void) const = 0;
		virtual cStr				getAdapterDescr (ulong adapter) const = 0;
	
		virtual ulong				getCurrentAdapter (void) const = 0;
		virtual ulong				getCurrentDevice (ulong adapter) const = 0;
		virtual ulong				getCurrentMode (ulong adapter, ulong device) const = 0;
	
		// scene drawing
		virtual void				setWindow (baseViewport *vp) = 0;
		virtual void				setViewport (int sx, int sy, int sw, int sh) = 0;
		virtual void				begin (void) const = 0;
		virtual void				end (void) const = 0;
		virtual void				clear (int numRects, const rect *rects, ulong flags, float4 color, float z, ulong stencil) const = 0;
	
		virtual fragileObjectRegistry*	fragileRegistry (void) const;
	};
	
}

#endif // __F_BASERENDERER_H

