#ifndef __BACKEND_H
#define __BACKEND_H

#include "f_baseobject.h"
#include "f_math.h"

namespace fe
{
	// backend stores both subset array and object array
	// object array is used by some passes to draw shadow volumes, etc.
	
	class renderable;
	class sceneObject;
	class material;
	class baseTexture;

	struct drawVertex_t
	{
		vector3 pos;
		vector3 norm;
		vector2 uv;
		vector2 uvLightmap;
		vector3 tangents[2];
		uchar color[4];
	
		bool operator == (const drawVertex_t &v)
		{
			return (pos == v.pos
				&& norm == v.norm
				&& uv == v.uv
				&& uvLightmap == v.uvLightmap
				&& tangents[0] == v.tangents[0]
				&& tangents[1] == v.tangents[1]
				&& color[0] == v.color[0]
				&& color[1] == v.color[1]
				&& color[2] == v.color[2]
				&& color[3] == v.color[3]
			);
		}
	};
	
	struct meshData_t
	{
		uint32 primType;
		
		uint_ptr vbo;	// vertex buffer object or 0
		uchar *verts;	// may NOT be 0
		uint32 vertexSize;
		
		uint_ptr ibo;	// index buffer object or 0
		uchar *inds;	// use glDrawArrays if NULL

		uint32 firstVertex;
		uint32 numVerts;
		uint32 firstIndex;
		uint32 numInds;
		
		unsigned have_colors : 1;
		unsigned have_lightmaps : 1;
		unsigned have_tangents : 1;
	};

	struct drawSurf_t
	{
		sceneObject *obj; // NULL disables drawing of this drawsurf
		meshData_t *mesh;

		material *mtl;
		baseTexture *lightmap;

		// app flags allow more control when app creates custom passes
		// feditor uses that to draw selected faces only, etc
		uint32 appFlags;
	};

	class rBackendPass
	{
	public:
		virtual void draw (renderable **objects, int numobjects, drawSurf_t **drawsurfs, int numds) = 0;
	};

	class rBackendAmbient : public rBackendPass
	{
	public:
		void draw (renderable **objects, int numobjects, drawSurf_t **drawsurfs, int numds);
	};

	class rBackendLight : public rBackendPass
	{
	public:
		void draw (renderable **objects, int numobjects, drawSurf_t **drawsurfs, int numds);
	};

	class rBackendWire : public rBackendPass
	{
	public:
		void draw (renderable **objects, int numobjects, drawSurf_t **drawsurfs, int numds);
	};

	enum {
		R_BACKEND_MAX_OBJECTS		= 1000,
		R_BACKEND_MAX_DRAWSURFS		= 10000,
		R_BACKEND_MAX_PASSES		= 10
	};

	class rBackend : public baseObject
	{
	private:

		drawSurf_t* mDrawSurfs[R_BACKEND_MAX_DRAWSURFS];
		int mNumDrawSurfs;
		renderable *mObjects[R_BACKEND_MAX_OBJECTS];
		int mNumObjects;
		rBackendPass* mPasses[R_BACKEND_MAX_PASSES];
		int mNumPasses;
//		int mCurrentPass;
		rBackendAmbient mAmbPass;
		rBackendLight mLightPass;
		rBackendWire mWirePass;
		drawSurf_t *mpCurrentDrawSurf;

	public:
		rBackend (void);
		~rBackend (void);
		void add (renderable *obj);
		void flush (void);
//		int getCurrentPass (void) const;
		void addPass (rBackendPass* pass);
		void removePass (rBackendPass* pass);

		void setCurrentDrawSurf (drawSurf_t *ds)
		{
			mpCurrentDrawSurf = ds;
		}

		drawSurf_t *getCurrentDrawSurf (void) const
		{
			return mpCurrentDrawSurf;
		}
	};
	
	void backend_draw_surf (drawSurf_t *surf);
}

#endif // __BACKEND_H
