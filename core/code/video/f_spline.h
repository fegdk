/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_SPLINE_H
#define __F_SPLINE_H

#include "f_math.h"

namespace fe
{
	
	class catmullRomSpline
	{
	
	protected:
	
		vector3 mControlPoints[4];
	
	public:
		catmullRomSpline (const vector3 &a, const vector3 &b, const vector3 &c, const vector3 &d);
	
		// takes 0..1 for entire spline
		vector3 point( float t );
		// takes 0..1 between points p1 and p2
		vector3 point( int p1, int p2, float t );
	
	};
	
	class quadraticBezierInterpolator
	{
	public:
		static vector2 evaluate (const vector2 &p1, const vector3 &k, const vector2 &p2, float t)
		{
			vector2 v;
			float t1 = t * t;
			float t2 = 2 * t * ( 1 - t );
			float t3 = ( 1 - t ) * ( 1 - t );
			v.x = p1.x * t1 + k.x * t2 + p2.x * t3;
			v.y = p1.y * t1 + k.y * t2 + p2.y * t3;
			return v;
		}
	};
	
	class quadraticBezierSpline
	{
	
		// bernsteins basis Function:
		// 1 = t + (1 - t)
	
		// quadratic bezier splines:
		// 1 = sqr( t + (1 - t) );
	
		// decomp:
		// 1 = sqr( t ) + 2 * t * ( 1 - t ) + sqr( 1 - t )
	
	protected:
	
		vector3 mControlPoints[3];
	
	public:
	
		// takes 0..1 for entire spline
		vector3	point( float t );
		vector3&	controlPoints( int i ) { return mControlPoints[i]; }
	
	};
	
	class cubicBezierSpline
	{
		// cubic bezier splines:
		// 1 = ( t + (1 - t) ) ^ 3;
		// decomp:
		// 1 = t^3 + 3 * ( t ^ 2 ) * ( 1 - t ) + 3 * t * ( ( 1 - t ) ^ 2 ) + ( 1 - t ) ^ 3
	
	protected:
	
		vector3 mControlPoints[4];
	
	public:
	
		cubicBezierSpline (const vector3 &a, const vector3 &b, const vector3 &c, const vector3 &d);
	
		// takes 0..1 for entire spline
		vector3	point( float t );
		vector3&	controlPoints( int i ) { return mControlPoints[i]; }
	
	};
	
}

#endif // __F_SPLINE_H

