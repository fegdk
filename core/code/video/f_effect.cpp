/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_effect.h"
#include "f_engine.h"
#include "f_error.h"
#include "f_effectpass.h"
#include "f_baserenderer.h"
#include "f_util.h"

namespace fe
{
	
	effect::effect (const char *name)
	{
		mName = name;
		mbLost = true;
		mNumSemantBindings = 0;
		mNumTechniques = 0;
		mNumVars = 0;
		for (int i = 0; i < FX_VARS_HASH_SIZE; i++)
			mVarsHash[i] = -1;
		mCurrentTechnique = 0;
		mBestTechnique = -1;
		restore ();
	}
	effect::effect (charParser &parser, const char *name)
	{
	}
	
	effect::~effect (void)
	{
		loose ();
	}
	
	void effect::loose (void)
	{
		// cleanup
		mbLost = true;
		size_t i;
		mNumTechniques = 0;
		mNumSemantBindings = 0;
	
		mNumVars = 0;
		for (i = 0; i < FX_VARS_HASH_SIZE; i++)
			mVarsHash[i]=-1;
	
		mBestTechnique = -1;
	}
	
	void effect::restore (void)
	{
		if (NULL == g_engine->getRenderer ())
			return;
		assert (mbLost);
		// load from file
		mBestTechnique = -1;
		mbLost = false;
		bool expEndOfFile = true;
		charParser p (g_engine->getFileSystem (), mName, g_engine->getFileSystem () ? false : true);
		for (;;)
		{
			expEndOfFile = true;
			p.errMode (0);
			if (p.getToken ())
				break;
			p.errMode (1);
			expEndOfFile = false;

			// vars
			if (!p.cmptoken ("dword"))
				loadDword (p);
			else if (!p.cmptoken ("float"))
				loadFloat (p);
			else if (!p.cmptoken ("vector") || !p.cmptoken ("float4"))
				loadVector (p);
			else if (!p.cmptoken ("matrix") || !p.cmptoken ("float4x4"))
				loadMatrix (p);
			else if (!p.cmptoken ("texture"))
				loadTexture (p);
			else if (!p.cmptoken ("string"))
				loadString (p);
			else if (!p.cmptoken ("technique"))
			{
				if (!loadTechnique (p))
				{
					continue;
				}
					
				if (mBestTechnique == -1)
				{
					mBestTechnique = mNumTechniques-1;
					fprintf (stderr, "effect: %s, best tech: %d (%s)\n", name (), mBestTechnique, mTechniques[mBestTechnique]->name ());
/*					size_t i;
					effectTechnique *t = mTechniques.back ();
					for (i = 0; i < t->numPasses (); i++)
					{
						t->pass ((int)i, *this);
						DWORD res;
						IDirect3DDevice8 *dev = engine::get ()->renderer ()->getRendererData ()->getDevice ();
						HRESULT hr = dev->ValidateDevice (&res);
						if (FAILED (hr) || res != 1)
							break;
					}
					if (i == t->numPasses () && t->isValid ())
					{
						// technique is valid
						mBestTechnique = (int)mNumTechniques-1;
					}*/
				}
			}
			else
				p.generalSyntaxError ();
		}
	}
	
	int		effect::varIndexByName (const char *name) const
	{
		int hash = hashValue (name, FX_VARS_HASH_SIZE);
		int v = mVarsHash[hash];
//		for (int i = 0; i < mNumVars; i++) {
//			printf ("var: %s, hash: %d, hashNext: %d\n", mVars[i].name, hashValue(mVars[i].name, FX_VARS_HASH_SIZE), mVars[i].hashNext);
//		}
//		exit (0);
		while (v != -1) {
//			printf ("%s: %d (%s)\n", name, v, mVars[v].name);
//			exit(0);
			if (!strcmp (mVars[v].name, name))
				return v;
			v = mVars[v].hashNext;
		}
		return -1;
	}
	
	int 	effect::findTechnique (const char *name_start) const
	{
		for (int i = 0; i < mNumTechniques; i++) {
			if (mTechniques[i]->isValid() && strncmp (mTechniques[i]->name(), name_start, strlen (name_start)))
				return i;
		}
		return -1;
	}
	
	int		effect::techniqueIndexByName (const char *name) const
	{
		for (int i = 0; i < mNumTechniques; i++) {
			if (mTechniques[i]->name() == name)
				return i;
		}
		sys_error ("invalid technique name '%s' requested from effect '%s'", name, mName.c_str ());
		return -1;
	}
	
	void	effect::setTechnique (int nTechnique)
	{
		mCurrentTechnique = nTechnique;
	}
	
	void	effect::setBestTechnique (void)
	{
		mCurrentTechnique = mBestTechnique;
	}
		
	void	effect::setTechnique (const char *techniqueName)
	{
		for (int i = 0; i < mNumTechniques; i++) {
			if (mTechniques[i]->name() == techniqueName)
				mCurrentTechnique = i;
		}
		sys_error ("invalid technique name '%s' requested from effect '%s'", techniqueName, mName.c_str ());
	}
	
	int		effect::begin (void)
	{
		if (mCurrentTechnique == -1)
			return 0;
	
	#if 0
		// set all matrices to identity
		for (size_t i = 0; i < mNumVars; i++)
		{
			if (mVars[i]->type () == svtMatrix)
			{
				matrix4 *m = *mVars[i];
				m->identity ();
			}
		}
	#endif
	
		return mTechniques[mCurrentTechnique]->numPasses ();
	}
	
	void	effect::end (void)
	{
	}
	
	int		effect::passIndexByName (const char *passName) const
	{
		assert (mCurrentTechnique != -1);
		for (int i = 0; i < mTechniques[mCurrentTechnique]->mNumPasses; i++) {
			if (passName == mTechniques[mCurrentTechnique]->mPasses[i]->name ())
				return i;
		}
		sys_error ("invalid pass name '%s' requested from effect '%s'", passName, mName.c_str ());
		return -1;
	}
	
	void	effect::pass (int nPass, uint32 flags)
	{
		mTechniques[mCurrentTechnique]->pass (nPass, *this, flags);
	}
	
	void	effect::finalizePass (int nPass)
	{
		mTechniques[mCurrentTechnique]->finalizePass (nPass);
	}
	
	void	effect::loadDword (charParser &p)
	{
		if (mNumVars == MAX_FX_VARS)
			sys_error ("too many vars in effect %s", name());
		p.getToken ();
		cStr name = p.token ();
		validateVarName (name);
		p.getToken ();
		uint32 value = 0;
		if (p.cmptoken (";"))
		{
			value = atoi (p.token ());
			p.matchToken (";");
		}
		int hash = hashValue (name, FX_VARS_HASH_SIZE);
		strcpy (mVars[mNumVars].name, name);
		mVars[mNumVars].hashNext = mVarsHash[hash];
		mVarsHash[hash] = mNumVars;
		mVars[mNumVars].set (svtDword, (void*)&value, sizeof (value));
		mNumVars++;
	}
	
	void	effect::loadFloat (charParser &p)
	{
		if (mNumVars == MAX_FX_VARS)
			sys_error ("too many vars in effect %s", name());
		p.getToken ();
		cStr name = p.token ();
		validateVarName (name);
		p.getToken ();
		float value = 0;
		if (p.cmptoken (";"))
		{
			value = atof (p.token ());
			p.matchToken (";");
		}
		int hash = hashValue (name, FX_VARS_HASH_SIZE);
		strcpy (mVars[mNumVars].name, name);
		mVars[mNumVars].hashNext = mVarsHash[hash];
		mVarsHash[hash] = mNumVars;
		mVars[mNumVars].set (svtFloat, (void*)&value, sizeof (value));
		mNumVars++;
	}
	
	void	effect::loadVector (charParser &p)
	{
		if (mNumVars == MAX_FX_VARS)
			sys_error ("too many vars in effect %s", name());
		p.getToken ();
		cStr name = p.token ();
		validateVarName (name);
		float4 value (0, 0, 0, 0);
		int i;
		for (i = 0; i < 4; i++)
		{
			p.getToken ();
			if (!p.cmptoken (";") || !p.cmptoken (":"))
			{
				if (i == 0)
					break;
				else
					p.generalSyntaxError ();
			}
			value[i] = atof (p.token ());
		}
	
		if (i == 4)
			p.getToken ();
	
		if (!p.cmptoken (":"))
		{
			if (mNumSemantBindings == MAX_FX_SEMANT_BINDINGS)
				sys_error ("too many semantic bindings in effect %s", this->name ());
			semantBinding_t *sr = &mSemantBindings[mNumSemantBindings++];
			sr->type = bind_vector;
			// read semantic
			p.getToken ();
			cStr smnt = p.token ();
	
			sr->var = mNumVars;
	
			if (smnt == "EYEPOSITION")
				sr->parm = effectParm_EyePosition;
			else if (smnt == "EYEDIRECTION")
				sr->parm = effectParm_EyeDirection;
			else if (smnt == "LIGHTPOS0")
				sr->parm = effectParm_LightPos0;
			else if (smnt == "LIGHTPOS1")
				sr->parm = effectParm_LightPos1;
			else if (smnt == "LIGHTPOS2")
				sr->parm = effectParm_LightPos2;
			else if (smnt == "LIGHTPOS3")
				sr->parm = effectParm_LightPos3;
			else if (smnt == "LIGHTPOS4")
				sr->parm = effectParm_LightPos4;
			else if (smnt == "LIGHTPOS5")
				sr->parm = effectParm_LightPos5;
			else if (smnt == "LIGHTPOS6")
				sr->parm = effectParm_LightPos6;
			else if (smnt == "LIGHTPOS7")
				sr->parm = effectParm_LightPos7;
			else if (smnt == "AMBIENTCOLOR")
				sr->parm = effectParm_AmbientColor;
			else if (smnt == "LIGHTDIR0")
				sr->parm = effectParm_LightDir0;
	//		else if (smnt == "LIGHTPOS0")
	//			sr->parm = effectParm_LightPos0;
			else if (smnt == "LIGHTDIFFUSE0")
				sr->parm = effectParm_LightDiffuse0;
			else if (smnt == "LIGHTSPECULAR0")
				sr->parm = effectParm_LightSpecular0;
			else
				p.generalSyntaxError ();
			
	//		validateVarName (p.token ());
	//		mVarsSemanticMap[p.token ()] = mNumVars;
			p.matchToken (";");
		}
		else if (p.cmptoken (";"))
			p.generalSyntaxError ();
	
		int hash = hashValue (name, FX_VARS_HASH_SIZE);
		strcpy (mVars[mNumVars].name, name);
		mVars[mNumVars].hashNext = mVarsHash[hash];
		mVarsHash[hash] = mNumVars;
		mVars[mNumVars].set (svtFloat4, (void*)&value, sizeof (value));
		mNumVars++;
	}
	
	void	effect::loadMatrix (charParser &p)
	{
		if (mNumVars == MAX_FX_VARS)
			sys_error ("too many vars in effect %s", name());
		p.getToken ();
		cStr name = p.token ();
		validateVarName (name);
		matrix4 value (true);
		int i;
		for (i = 0; i < 16; i++)
		{
			p.getToken ();
			if (!p.cmptoken (";") || !p.cmptoken (":"))
			{
				if (i == 0)
					break;
				else
					p.generalSyntaxError ();
			}
			value[i] = atof (p.token ());
		}
	
		if (i == 16)
			p.getToken ();
	
		if (!p.cmptoken (":"))
		{
			if (mNumSemantBindings == MAX_FX_SEMANT_BINDINGS)
				sys_error ("too many semantic bindings in effect %s", this->name ());
			semantBinding_t *sr = &mSemantBindings[mNumSemantBindings++];
			sr->type = bind_matrix;
			// read semantic
			p.getToken ();
			cStr smnt = p.token ();
	
			sr->var = mNumVars;
	
			if (smnt == "WORLDMATRIX")
				sr->parm = effectParm_WorldMatrix;
			else if (smnt == "VIEWMATRIX")
				sr->parm = effectParm_ViewMatrix;
			else if (smnt == "PROJMATRIX")
				sr->parm = effectParm_ProjMatrix;
			else if (smnt == "WORLDVIEWPROJMATRIX")
				sr->parm = effectParm_WorldViewProjMatrix;
			else if (smnt == "WORLDVIEWMATRIX")
				sr->parm = effectParm_WorldViewMatrix;
			else if (smnt == "INVWORLDMATRIX")
				sr->parm = effectParm_InvWorldMatrix;
			else if (smnt == "INVVIEWMATRIX")
				sr->parm = effectParm_InvViewMatrix;
			else if (smnt == "VIEWTOMODELMATRIX")
				sr->parm = effectParm_ViewToModelMatrix;
			else
				p.generalSyntaxError ();
	
	//		validateVarName (p.token ());
	//		mVarsSemanticMap[p.token ()] = mNumVars;
			p.matchToken (";");
		}
		else if (p.cmptoken (";"))
			p.generalSyntaxError ();
	
		int hash = hashValue (name, FX_VARS_HASH_SIZE);
		strcpy (mVars[mNumVars].name, name);
		mVars[mNumVars].hashNext = mVarsHash[hash];
		mVarsHash[hash] = mNumVars;
		mVars[mNumVars].set (svtMatrix, (void*)&value, sizeof (value));
		mNumVars++;
	}
	
	void	effect::loadTexture (charParser &p)
	{
		if (mNumVars == MAX_FX_VARS)
			sys_error ("too many vars in effect %s", name());
		p.getToken ();
		cStr name = p.token ();
		validateVarName (name);
		baseTexture *value = NULL;
		p.getToken ();
		if (!p.cmptoken (":"))
		{
			if (mNumSemantBindings == MAX_FX_SEMANT_BINDINGS)
				sys_error ("too many semantic bindings in effect %s", this->name ());
			semantBinding_t *sr = &mSemantBindings[mNumSemantBindings++];
			sr->type = bind_texture;
			p.getToken ();
			cStr smnt = p.token ();
	
			sr->var = mNumVars;
	
/*			if (smnt == "LIGHTMAP")
				sr->parm = effectParm_LightmapTexture;
			else*/
				p.generalSyntaxError ();
	
			p.matchToken (";");
		}
		else if (p.cmptoken (";"))
			p.generalSyntaxError ();
		
		int hash = hashValue (name, FX_VARS_HASH_SIZE);
		strcpy (mVars[mNumVars].name, name);
		mVars[mNumVars].hashNext = mVarsHash[hash];
		mVarsHash[hash] = mNumVars;
		mVars[mNumVars].set (svtTexture, (void*)&value, sizeof (value));
		mNumVars++;
	}
	
	void	effect::loadString (charParser &p)
	{
		if (mNumVars == MAX_FX_VARS)
			sys_error ("too many vars in effect %s", name());
		p.getToken ();
		cStr name = p.token ();
		validateVarName (name);
		cStr value;
		p.getToken ();
		if (p.cmptoken (";"))
		{
			value = p.token ();
			p.matchToken (";");
		}
		int hash = hashValue (name, FX_VARS_HASH_SIZE);
		strcpy (mVars[mNumVars].name, name);
		mVars[mNumVars].hashNext = mVarsHash[hash];
		mVarsHash[hash] = mNumVars;
		mVars[mNumVars].set (svtString, (void*)&value, sizeof (value));
		mNumVars++;
	}
	
	bool	effect::loadTechnique (charParser &p)
	{
		if (mNumTechniques == MAX_FX_TECHNIQUES)
			sys_error ("too many techniques in effect %s\n", name());
		p.getToken ();
		cStr name = p.token ();
		validateVarName (name);
	
		smartPtr <effectTechnique> t = new effectTechnique ();
		t->setName (name);
		if (!t->load (p, *this))
		{
			//fprintf (stderr, "technique %s is invalid\n", name.c_str ());
			return false;
		}
		mTechniques[mNumTechniques++] = t;
		return true;
	}
	
	void	effect::setMatrix (int nParm, const matrix4 &m)
	{
	//	matrix4 v = m.transpose ();
		mVars[nParm].set (svtMatrix, (void *)&m, sizeof (matrix4));
	}
	
	matrix4&	effect::getMatrix (int nParm)
	{
		matrix4 *m = mVars[nParm];
		return *m;
	}
	
	float4&	effect::getFloat4 (int nParm)
	{
		float4 *f = mVars[nParm];
		return *f;
	}
	
	void	effect::setFloat4 (int nParm, const float4 &m)
	{
		mVars[nParm].set (svtFloat4, (void *)&m, sizeof (float4));
	}
	
	void	effect::setTexture (int nParm, const baseTexturePtr &t)
	{
		if (nParm != -1)
			mVars[nParm].set (svtTexture, (void *)&t, 4);
	}
	
	shaderPtr effect::getVertexProgram (int npass) const
	{
		return mTechniques[mCurrentTechnique]->getPass (npass)->getVertProgram ();
	}
	
	shaderPtr effect::getFragmentProgram (int npass) const
	{
		return mTechniques[mCurrentTechnique]->getPass (npass)->getFragProgram ();
	}
	
	//------------------------------------------------------------
	// effectVar implementation
	//------------------------------------------------------------
	
	effectVar::effectVar (const effectVar &v)
	{
		init ();
		effectVar (v.mType, v.mValue);
	}
	
	// slow but simple constructor
	effectVar::effectVar (effectVarType type, const uchar *value)
	{
		init ();
		mType = type;
		size_t sz;
		switch (mType)
		{
		case svtDword:
			sz = sizeof (ulong);
			break;
		case svtFloat:
			sz = sizeof (float);
			break;
		case svtFloat4:
			sz = sizeof (float4);
			break;
		case svtMatrix:
			sz = sizeof (matrix4);
			break;
		case svtTexture:
			sz = sizeof (baseTexture*);
			break;
		case svtString:
			sz = strlen ((char *)value) + 1;
			break;
		}
		memcpy (mValue, value, sz);
	}
	
	// separate constructors for each type
	effectVar::effectVar (ulong value)
	{
		init ();
		set (svtDword, (uchar *)&value, sizeof (value));		
	}
	
	effectVar::effectVar (float value)
	{
		init ();
		set (svtFloat, (uchar *)&value, sizeof (value));		
	}
	
	effectVar::effectVar (const float4 &value)
	{
		init ();
		set (svtFloat4, (uchar *)&value, sizeof (value));		
	}
	
	effectVar::effectVar (const matrix4 &value)
	{
		init ();
		set (svtMatrix, (uchar *)&value, sizeof (value));		
	}
	
	effectVar::effectVar (baseTexture *value)
	{
		init ();
		set (svtTexture, (uchar *)&value, sizeof (value));		
	}
	
	effectVar::effectVar (const cStr &value)
	{
		init ();
		set (svtString, (uchar *)value.c_str (), value.size ()+1);		
	}
	
	effectVar::~effectVar (void)
	{
	}
	
	//------------------------------------------------------------
	// effectTechnique implementation
	//------------------------------------------------------------
	
	effectTechnique::effectTechnique (void)
	{
		mbValid = false;
		mNumPasses = 0;
	}
	
	effectTechnique::~effectTechnique (void)
	{
	}
	
	int effectTechnique::numPasses (void) const
	{
		return mNumPasses;
	}

	bool effectTechnique::load (charParser &p, effect &s)
	{
		mbValid = true;
		p.matchToken ("{");
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
			{
				break;
			}
			else if (!p.cmptoken ("pass"))
			{
				if (mNumPasses == MAX_TECH_PASSES)
					sys_error ("too many passes in technique %s of effect %s", name(), s.name());
				p.getToken ();
				cStr name = p.token ();
				validateVarName (name);
				smartPtr <effectPass> pass = new effectPass ();
				pass->setName (name);
				if (!pass->load (p, s))
					mbValid = false;
				mPasses[mNumPasses++] = pass;
			}
			else
			{
				p.generalSyntaxError ();
			}
		}
		return mbValid;
	}
	
	void effectTechnique::pass (int nPass, effect &s, uint32 flags)
	{
		mPasses[nPass]->apply (s, flags);
	}
	
	void effectTechnique::finalizePass (int nPass)
	{
		mPasses[nPass]->finalize ();
	}
	
	smartPtr <effectTechnique> effect::getCurrentTechnique (void) const
	{
		return mTechniques[mCurrentTechnique];
	}
	
	smartPtr <effectPass>	effectTechnique::getPass (int pass) const
	{
		return mPasses[pass];
	}
	
}
