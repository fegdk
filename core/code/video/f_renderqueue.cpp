/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_renderqueue.h"
#include "f_material.h"
#include "f_renderablesceneobject.h"
#include "f_engine.h"
#include "f_drawutil.h"
#include "f_shader.h"
#include "f_baserenderer.h"
#include "f_resourcemgr.h"
#include <GL/glew.h>
#include "f_scenemanager.h"
#include "f_timer.h"
#include "cvars.h"

namespace fe
{
	
	renderQueue::renderQueue (void)
	{
		mName = "rqueue";
		mpExtrudePointVP = g_engine->getResourceMgr ()->createObject ("cgprogram", "cg/std/extrude.cg:extrude_point:bestvp").dynamicCast<shader>();
	
		mpExtrudeDirectionalVP = g_engine->getResourceMgr ()->createObject ("cgprogram", "cg/std/extrude.cg:extrude_directional:bestvp").dynamicCast<shader>();
		mpWhiteTexture = g_engine->getResourceMgr ()->createObject ("texture2d", "textures/white.png").dynamicCast <baseTexture> ();
		mCurrentPass = PASS_OUTSIDE;
	}
	
	renderQueue::~renderQueue (void)
	{
	}
	
	// algorithm is following:
	//
	//    draw_ambient_pass (); // pass -1 as 2nd parameter
	//
	//    for_each_light {
	//       
	//       if (shadows) draw_stencil_buffer ();
	//
	//       // must use stenciltest if shadows are enabled and supported
	//       draw_additive_geometry ();
	//    }
	
	void renderQueue::flush (void)
	{
		std::vector <qelement_t>::const_iterator o;
		std::vector <renderableSceneObjectPtr>::const_iterator o_it;
	
		// setup render states for ambient pass
		g_engine->getDrawUtil ()->ambientPass (true);
		g_engine->getEffectParms ()->setFloat4 (effectParm_AmbientColor, float4 (0.2, 0.2, 0.2, 1.0));
		g_engine->getEffectParms ()->setFloat4 (effectParm_LightDiffuse0, float4 (0.7, 0.7, 0.7, 1.0));
		vector3 ldir(1,1,1);
/*		matrix4 rotX, rotZ;
		rotX.rotateX (g_engine->getTimer ()->getSysTime ());
		rotZ.rotateZ (g_engine->getTimer ()->getSysTime ());
		ldir = ldir * rotX * rotZ;*/

		ldir.normalize ();
		g_engine->getEffectParms ()->setFloat4 (effectParm_LightDir0, float4 (ldir.x, ldir.y, ldir.z, 0));
	
		// fill depth-buffer, draw objects as ambient*texture 
		for (o = mQElements.begin (); o != mQElements.end (); o++)
		{
			materialPtr mtl = (*o).obj->getMtl((*o).subset);
			if (mtl)
			{
				g_engine->getSceneManager ()->setCurrentMtl (mtl);
				material::effectRef *effect = mtl->getEffectRef ();
				if (effect && effect->ambient != -1 && effect->fx)
				{
					effect->fx->setTechnique (effect->ambient);
	
					(*o).obj->prepare ((*o).subset);
					mtl->setupEffectInputs ();
					int np = effect->fx->begin ();
					for (int p = 0; p < np; p++)
					{
						effect->fx->pass (p);
						(*o).obj->render ((*o).subset, p);
						effect->fx->finalizePass (p);
					}
					effect->fx->end ();
				}
			}
		}
		g_engine->getDrawUtil ()->ambientPass (false);
	
	#if 1
		// draw shadow volumes into stencil buffer
		//
		// !! this is test implementation (unoptimized) so we always render capped SVs
	
		bool capped = false;
		bool shadows = true;
		bool show_volumes = false;
	
		
		if (shadows)
		{
			mCurrentPass = PASS_SHADOW;
			// TODO must calculate shadow volume here
	
			// clear stencil buffer
			g_engine->getRenderer ()->clear (0, NULL, baseRenderer::clear_stencil, 0x00000000ul, 1, 0);
	
			if (show_volumes)
			{
				glActiveTextureARB (GL_TEXTURE0_ARB);
				glDisable (GL_TEXTURE_2D);
				glColor4f (0.2,0,0.2,1);
				glEnable (GL_BLEND);
				glBlendFunc (GL_ONE, GL_ONE);
			}
			else
			{
				glDisable (GL_BLEND);
			}
			g_engine->getDrawUtil ()->enableStencilTest (true);
			g_engine->getDrawUtil ()->setStencilFunc (drawUtil::always);
			g_engine->getDrawUtil ()->enableZWrite (false); // clear enables zwrite
	//		g_engine->getDrawUtil ()->enableZTest (true); // ztest is enabled here anyhow
			g_engine->getDrawUtil ()->setZFunc (drawUtil::less);
			g_engine->getDrawUtil ()->enableColorWrite (show_volumes);
			if (capped)
			{
			}
			else
			{
				g_engine->getDrawUtil ()->setCullMode (drawUtil::cull_back);
				g_engine->getDrawUtil ()->setStencilZOps (drawUtil::keep, drawUtil::incr);
			}
	
			for (o_it = mQObjects.begin (); o_it != mQObjects.end (); o_it++)
			{
				(*o_it)->renderShadowVolume (true);
			}
	
			if (capped)
			{
			}
			else
			{
				g_engine->getDrawUtil ()->setCullMode (drawUtil::cull_front);
				g_engine->getDrawUtil ()->setStencilZOps (drawUtil::keep, drawUtil::decr);
			}
			for (o_it = mQObjects.begin (); o_it != mQObjects.end (); o_it++)
			{
				(*o_it)->renderShadowVolume (true);
			}
	
			g_engine->getDrawUtil ()->setStencilZOps (drawUtil::keep, drawUtil::keep);
			g_engine->getDrawUtil ()->setStencilFunc (drawUtil::equal);
			// restore default culling
			g_engine->getDrawUtil ()->setCullMode (drawUtil::cull_back);
			g_engine->getDrawUtil ()->enableColorWrite (true);
		}
		
		// ligting is rendered by ADDing into framebuffer
		g_engine->getDrawUtil ()->lightingPass (true);
	
	//	if (shadows)
	//	{
	//		g_engine->getDrawUtil ()->enableStencilTest (true);	// enabled earlier
	//	}
	
		mCurrentPass = PASS_LIGHT;
		for (o = mQElements.begin (); o != mQElements.end (); o++)
		{
			materialPtr mtl = (*o).obj->getMtl((*o).subset);
			if (mtl)
			{
				material::effectRef *effect = mtl->getEffectRef ();
				if (effect && effect->directlight != -1 && effect->fx)
				{
					effect->fx->setTechnique (effect->directlight);
	
					(*o).obj->prepare ((*o).subset);
					mtl->setupEffectInputs ();
					int np = effect->fx->begin ();
					for (int p = 0; p < np; p++)
					{
						effect->fx->pass (p);
						(*o).obj->render ((*o).subset, p);
						effect->fx->finalizePass (p);
					}
					effect->fx->end ();
				}
			}
		}
	
		if (shadows)
		{
			// restore default depth/stencil funcs for light rendering
			g_engine->getDrawUtil ()->enableStencilTest (false);
			g_engine->getDrawUtil ()->enableZWrite (true);
		}
	
		// end lighting pass
		g_engine->getDrawUtil ()->lightingPass (false);

		// draw tris
		if (cvar_r_showtris.ivalue == 1)
		{
			mCurrentPass = PASS_WIREFRAME;
			glActiveTextureARB (GL_TEXTURE0_ARB);
			glDisable (GL_BLEND);
			mpWhiteTexture->bind (0);
			glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
			glColor4f (1,1,1,1);
			g_engine->getEffectParms ()->setFloat4 (effectParm_AmbientColor, float4 (1.0, 1.0, 1.0, 1.0));
			glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

			for (o = mQElements.begin (); o != mQElements.end (); o++)
			{
				materialPtr mtl = (*o).obj->getMtl((*o).subset);
				if (mtl)
				{
					g_engine->getSceneManager ()->setCurrentMtl (mtl);
					material::effectRef *effect = mtl->getEffectRef ();
					if (effect && effect->ambient != -1 && effect->fx)
					{
						effect->fx->setTechnique (effect->ambient);
		
						(*o).obj->prepare ((*o).subset);
						mtl->setupEffectInputs ();
						int np = effect->fx->begin ();
						for (int p = 0; p < np; p++)
						{
							effect->fx->pass (p);
							(*o).obj->render ((*o).subset, p);
							effect->fx->finalizePass (p);
						}
						effect->fx->end ();
					}
				}
			}

			glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
		}
	#endif
	
		mQObjects.clear ();
		mQElements.clear ();
		mCurrentPass = PASS_OUTSIDE;
	}
	
	void renderQueue::add (const renderableSceneObjectPtr &obj, bool with_children)
	{
		for (int s = 0; s < obj->numSubsets (); s++)
		{
			qelement_t e;
			e.obj = obj;
			e.subset = s;
			mQElements.push_back (e);
		}
		mQObjects.push_back (obj);
	
		if (with_children)
		{
			for (int i = 0; i < obj->numChildren (); i++)
			{
				sceneObjectPtr child = obj->getChild (i);
				if (child->isTypeOf (renderableSceneObject::TypeId))
				{
					add (child.dynamicCast<renderableSceneObject>(), with_children);
				}
			}
		}
	}
	
	int renderQueue::getCurrentPass (void) const
	{
		return mCurrentPass;
	}
	
	void renderQueue::setCurrentPass (int p)
	{
		mCurrentPass = p;
	}
}
