/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_RENDERABLESCENEOBJECT_H
#define __F_RENDERABLESCENEOBJECT_H

#include "f_sceneobject.h"
#include "f_objectid.h"
#include "f_material.h"
#include "f_helpers.h"
#include "backend.h"

namespace fe
{
	
	// renderables are objects which can be passed through rendering queue
	// i.e. they got sorted, optimized, etc to perform optimally during rendering
	//
	// each object must be material based (we can support NULL materials also)
	//
	// common usage:
	//   do material setup (mathexpression evaluator variables, shader constants, etc) in "prepare"
	//   do all rendering (drawprims) in "render"
	//   do not setup materials yourself, queue will do it
	//
	//   application should be allowed to redefine "prepare" functionality by deriving own class off of e.g. triobject class, and implementing own "prepare" method
	//
	
#if 0
	class FE_API renderableSceneObject : public sceneObject
	{
	protected:
		drawSurf_t *mpDrawSurfs;
		int mNumDrawSurfs;

	public:
		int numDrawSurfs (void) const
		{
			return mNumDrawSurfs;
		}

		drawSurf_t *getDrawSurf (int n) const
		{
			return mpDrawSurfs+n;
		}

		enum {TypeId = FE_ID_RENDERABLE};
		
		renderableSceneObject (sceneObject *parent);
		~renderableSceneObject (void);
//		virtual void prepare (int subset) const = 0;		// prepares object to draw subset
//		virtual void render (int subset, int pass) const = 0;	// renders specified subset
		virtual bool isTypeOf (int type) const;
		virtual void renderShadowVolume (bool withChildren = false) const;	// must just draw all shadow volume triangles; do not change any gl states here (i mean lighting, blending, texturing, etc).
	};
	
	typedef smartPtr<renderableSceneObject>	renderableSceneObjectPtr;
#endif
	
}

#endif // __F_RENDERABLESCENEOBJECT_H

