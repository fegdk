/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_RENDERQUEUE_H
#define __F_RENDERQUEUE_H

#include "f_baseobject.h"
#include "f_renderablesceneobject.h"
#include "f_shader.h"
#include "f_basetexture.h"

namespace fe
{

	// it might be simple std::map, which insert-sorts by texture/shader/fvf/whatever
	// the only thing i dislike about map is dynamic mem allocations, which could be avoided by preallocating pool and making own allocator
	
	enum
	{
		PASS_AMBIENT,
		PASS_SHADOW,
		PASS_WIREFRAME,
		PASS_LIGHT,
		PASS_OUTSIDE
	};

	class FE_API renderQueue : public baseObject
	{
	private:
	
		struct qelement_t
		{
			renderableSceneObjectPtr obj;
			int subset;
		};
	
		std::vector <qelement_t>	mQElements;		// used to draw objects
		std::vector <renderableSceneObjectPtr>	mQObjects;	// used to draw shadow volumes
	
		shaderPtr		mpExtrudePointVP;
		shaderPtr		mpExtrudeDirectionalVP;
		smartPtr <baseTexture>	mpWhiteTexture;
		int mCurrentPass;
		
	public:
		renderQueue (void);
		~renderQueue (void);
	
		void flush (void);
		void add (const renderableSceneObjectPtr &obj, bool with_children = true);
		int getCurrentPass (void) const;
		void setCurrentPass (int p);
	};
	
}

#endif // __F_RENDERQUEUE_H

