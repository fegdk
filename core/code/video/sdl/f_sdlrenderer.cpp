/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

// $Header$
#include "pch.h"
#include "config.h"

#include "f_types.h"
#include "f_sdlrenderer.h"
#include "f_baseviewport.h"
#include "f_math.h"
#include "f_sdlviewport.h"
#include "f_sdlviewportdata.h"
#include "f_engine.h"
#include "f_application.h"
#include "f_console.h"
#include "f_error.h"
#include "f_fragileobject.h"
#include "f_resourcemgr.h"
#include <GL/glew.h>
#include "f_resourcemgr.h"
#include <SDL/SDL.h>
#include <cvars.h>

namespace fe
{

	//-----------------------------------
	// sdlRendererData
	//-----------------------------------
	
	sdlRendererData::sdlRendererData (void)
	{
		// enum vidmodes
	
		mNumVidModes = 0;
		mbWindowed = true;
	
		SDL_Rect **modes;
		SDL_PixelFormat fmt32 = {NULL, 32, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		SDL_PixelFormat fmt16 = {NULL, 16, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		
		modes = SDL_ListModes (&fmt32, SDL_FULLSCREEN | SDL_HWSURFACE |
		SDL_DOUBLEBUF | SDL_OPENGL);
		addModes (modes, 32);
		modes = SDL_ListModes (&fmt16, SDL_FULLSCREEN | SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_OPENGL);
		addModes (modes, 16);
		
		if (!mNumVidModes)
			sys_error ("no suitable video modes found!\n");
		size_t i;
		int mode_800x600 = -1;
		int mode_640x480 = -1;
		for (i = 0; i < mNumVidModes; i++)
		{
			fprintf (stderr, "found vidmode %dx%dx%d\n", mVidModes[i].w, mVidModes[i].h, mVidModes[i].bpp);
			if (mVidModes[i].w == 800 && mVidModes[i].h == 600)
				mode_800x600 = i;
			if (mVidModes[i].w == 640 && mVidModes[i].h == 480)
				mode_640x480 = i;
		}
	
		mCurrentMode = 0;
		if (mode_800x600 != -1)
			mCurrentMode = mode_800x600;
		else if (mode_640x480 != -1)
			mCurrentMode = mode_640x480;
	}
	
	void	sdlRendererData::reset (void)
	{
		SDL_GL_SetAttribute (SDL_GL_DOUBLEBUFFER, 1);
//		SDL_GL_SetAttribute (SDL_GL_STENCIL_SIZE, 8);
		SDL_GL_SetAttribute (SDL_GL_DEPTH_SIZE, 16);
	
		int flags = SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_OPENGL;
		if (!mbWindowed)
			flags |= SDL_FULLSCREEN;
		SDL_SetVideoMode (mVidModes[mCurrentMode].w, mVidModes[mCurrentMode].h, mVidModes[mCurrentMode].bpp, flags);
		
		// fullscreen viewport
	    glViewport (0, 0, mVidModes[mCurrentMode].w, mVidModes[mCurrentMode].h);
	}
	
	void
	sdlRendererData::addModes (SDL_Rect **modes, int bpp)
	{
		if (!modes)
			return;
	
		if (modes == (void*) (-1))
			sys_error ("SDL_ListModes returned -1. evil.");
	
		for (; *modes; modes++)
		{
			SDL_Rect *mode = *modes;
	
			size_t i;
			for (i = 0; i < mNumVidModes; i++)
			{
				if (mVidModes[i].w == mode->w && mVidModes[i].h == mode->h && mVidModes[i].bpp == bpp)
					break;
			}
	
			if (i == mNumVidModes)
			{
				if (mNumVidModes == SDL_MAX_VIDMODES)
					sys_error ("too many vidmodes for sdl driver (limit of %d modes exceeded)", SDL_MAX_VIDMODES);
				vidMode_t *vmode = &mVidModes[mNumVidModes++];
				vmode->w = mode->w;
				vmode->h = mode->h;
				vmode->bpp = bpp;
			}		
		}
	}
	
	sdlRendererData::~sdlRendererData (void)
	{
	}
	
	size_t	sdlRendererData::numVidModes (void) const
	{
		return mNumVidModes;
	}
	
	size_t	sdlRendererData::getCurrentVidMode (void) const
	{
		return mCurrentMode;
	}
	
	void	sdlRendererData::setCurrentVidMode (size_t mode)
	{
		mCurrentMode = mode;
	}
	
	const sdlRendererData::vidMode_t& sdlRendererData::getVidModeDescr (int mode) const
	{
		return mVidModes[mode];
	}
	
	void	sdlRendererData::setWindowed (bool windowed)
	{
		mbWindowed = windowed;
	}
	
	sdlRenderer::sdlRenderer (void)
	{
		mbResetting = false;
	    /* Setup our viewport. */
		size_t curmode = mRendererData.getCurrentVidMode ();
		const sdlRendererData::vidMode_t &descr = mRendererData.getVidModeDescr ( (int)curmode);
		
		fprintf (stderr, "SDL: setting render-related cvars...\n");
		
		// set cvars
		cvarManager *cvars = g_engine->getCVarManager ();
		cvars->setInt ("r_fullscreen", 0, true);
		cvars->setInt ("r_vidmode", curmode, true);
		
		glRenderer::init ();
		
		fprintf (stderr, "sdl_opengl renderer initialization done.\n");
	}
	
	sdlRenderer::~sdlRenderer (void)
	{
	}
	
	void				sdlRenderer::present (void)
	{
		SDL_GL_SwapBuffers ();
	}
	
	bool	sdlRenderer::isLost (void) const
	{
		// for now - assume opengl buffers are restored automatically (though remember it is not really true)
		return false;
	}
	
	void				sdlRenderer::resize (void)
	{
		int mode = r_vidmode->ivalue;
		int fullscreen = r_fullscreen->ivalue;
	
		// set modes to rendererdata
		mRendererData.setCurrentVidMode (mode);
		mRendererData.setWindowed (fullscreen ? false : true);
		mRendererData.reset ();
	
		glRenderer::resize ();
	}
	
	ulong				sdlRenderer::numDisplayModes (ulong adapter, ulong device) const
	{
		return (ulong)mRendererData.numVidModes ();
	}
	
	cStr				sdlRenderer::getDisplayModeDescr (ulong adapter, ulong device, ulong mode) const
	{
		const sdlRendererData::vidMode_t &vmode = mRendererData.getVidModeDescr (mode);
		cStr s;
		s.printf ("%d x %d x %d", vmode.w, vmode.h, vmode.bpp);
		return s;
	}
	
	ulong				sdlRenderer::numDevices (ulong adapter) const
	{
		return 1;
	}
	
	cStr				sdlRenderer::getDeviceDescr (ulong adapter, ulong device) const
	{
		return "SDL-opengl";
	}
	
	ulong				sdlRenderer::numAdapters (void) const
	{
		return 1;
	}
	
	cStr				sdlRenderer::getAdapterDescr (ulong adapter) const
	{
		return "default adapter";
	}
	
	ulong				sdlRenderer::getCurrentAdapter (void) const
	{
		return 0;
	}
	
	ulong				sdlRenderer::getCurrentDevice (ulong adapter) const
	{
		return 0;
	}
	
	ulong				sdlRenderer::getCurrentMode (ulong adapter, ulong device) const
	{
		return 0;
	}
	
	void				sdlRenderer::setWindow (baseViewport *vp)
	{
		// setting external window is not supported for opengl
		// FIXME: should this method be completely abandoned?
	}
	
	
	sdlRendererData*		sdlRenderer::getData (void)
	{
		return &mRendererData;
	}
	
}

