/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "config.h"

#include "f_sdlviewport.h"
#include "f_sdlviewportdata.h"
#include "f_helpers.h"
#include "f_engine.h"
#include "f_input.h"
#include "f_application.h"
#include <iostream>
#include <SDL/SDL.h>
#include "cvars.h"

namespace fe
{
	
	// table to translate sdl keycodes into native fe codes
	int sdl_to_key[] = {
		0, 0, 0, 0, 0, 0, 0, 0, Key_BackSpace, Key_Tab, 0, 0, Key_CR, 0, 0, 0, 0,
		0, Key_Pause, 0, 0, 0, 0, 0, 0, 0, Key_Esc, 0, 0, 0, 0, Key_Space,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, Key_Del, 0, 0, 0,	// del is 127
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0,					// 255
	
		// keypad
		
		Key_Kp_Ins, Key_Kp_DownArrow, Key_Kp_End, Key_Kp_PgDn,
		Key_Kp_LeftArrow, Key_Kp_5, Key_Kp_RightArrow,
		Key_Kp_Home, Key_Kp_UpArrow, Key_Kp_PgUp,
		Key_Kp_Del, Key_Kp_Slash, Key_Kp_Star,
		Key_Kp_Minus, Key_Kp_Plus, Key_Kp_Enter, Key_Kp_Equals,
	
		Key_UpArrow, Key_DownArrow, Key_RightArrow, Key_LeftArrow,
		Key_Ins, Key_Home, Key_End, Key_PgUp, Key_PgDn,
	
		// functional keys
		Key_F1, Key_F2, Key_F3, Key_F4, Key_F5, Key_F6, Key_F7, Key_F8, Key_F9,
		Key_F10, Key_F11, Key_F12, Key_F13, Key_F14, Key_F15,
	
		// unknown codes
		0, 0, 0,
	
		Key_Kp_NumLock, Key_CapsLock, Key_ScrollLock, Key_RShift, Key_LShift,
		Key_RCtrl, Key_LCtrl, Key_RAlt, Key_LAlt, Key_RMeta, Key_LMeta,
		Key_LSuper, Key_RSuper
	};
	
	// viewport data
	sdlViewportData::sdlViewportData (void)
	{
	}
	
	sdlViewportData::~sdlViewportData (void)
	{
		SDL_Quit ();
	}
	
	void
	sdlViewportData::init (const char *appName)
	{
		// create and show window
		SDL_Init (SDL_INIT_VIDEO);
		SDL_WM_SetCaption (appName, NULL);
		SDL_EnableUNICODE (1);
		SDL_EnableKeyRepeat (200, 50);
	}
	
	void
	sdlViewportData::handleEvent (SDL_Event *event)
	{
		switch (event->type)
		{
		case SDL_MOUSEMOTION:
			onMouseMove (event->motion.x, event->motion.y);
			break;
		case SDL_MOUSEBUTTONDOWN:
			onMouseBtnDown (event->button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			onMouseBtnUp (event->button.button);
			break;
		case SDL_KEYDOWN:
			onKeyDown (event->key.keysym.sym, event->key.keysym.unicode);
			break;
		case SDL_KEYUP:
			onKeyUp (event->key.keysym.sym);
			break;
		}
	}
	
	void
	sdlViewportData::onMouseMove (int x, int y)
	{
		smartPtr <inputDevice> mouse = g_engine->getInputDevice ();
		mouse->mouseMove (x, y);
		g_engine->getApplication ()->mouseMove (x, y);
	}
		
	void
	sdlViewportData::onMouseBtnDown (int sdl_btn)
	{
		smartPtr <inputDevice> mouse = g_engine->getInputDevice ();
		
		switch (sdl_btn)
		{
		case SDL_BUTTON_WHEELUP:
			g_engine->getApplication ()->mouseWheel (1);
			break;
		case SDL_BUTTON_WHEELDOWN:
			g_engine->getApplication ()->mouseWheel (-1);
			break;
		case SDL_BUTTON_LEFT:
			mouse->lButtonDown ();
			g_engine->getApplication ()->mouseDown (Key_Mouse1);
			break;
		case SDL_BUTTON_RIGHT:
			mouse->rButtonDown ();
			g_engine->getApplication ()->mouseDown (Key_Mouse2);
			break;
		case SDL_BUTTON_MIDDLE:
			mouse->mButtonDown ();
			g_engine->getApplication ()->mouseDown (Key_Mouse3);
			break;
		}
	}
	
	void
	sdlViewportData::onMouseBtnUp (int sdl_btn)
	{
		smartPtr <inputDevice> mouse = g_engine->getInputDevice ();
		
		switch (sdl_btn)
		{
		case SDL_BUTTON_LEFT:
			mouse->lButtonUp ();
			g_engine->getApplication ()->mouseUp (Key_Mouse1);
			break;
		case SDL_BUTTON_RIGHT:
			mouse->rButtonUp ();
			g_engine->getApplication ()->mouseUp (Key_Mouse2);
			break;
		case SDL_BUTTON_MIDDLE:
			mouse->mButtonUp ();
			g_engine->getApplication ()->mouseUp (Key_Mouse3);
			break;
		}
	}
	
	void
	sdlViewportData::onKeyDown (int sdl_key, int unicode)
	{
		if (in_debugkeys->ivalue)
			fprintf (stderr, "SDL keydown: sdl_key=%d, unicode=%d\n", sdl_key, unicode);
		// this routine could be more complex then win32 one, cause we don't have
		// anything like WM_CHAR here
	
		smartPtr <inputDevice> keyb = g_engine->getInputDevice ();
		if (unicode != 0x7f	// del is exception
			&& unicode >= 0x20)
		{
			keyb->printableChar (unicode);
		}
	
		// remap sdl codes to fe codes
		if (sdl_key >= (sizeof (sdl_to_key) / sizeof (int)) || sdl_to_key[sdl_key] == 0)
		{
			if (in_debugkeys->ivalue)
				fprintf (stderr, "translated to %d\n", sdl_key);
			keyb->keyDown (sdl_key);
		}
		else
		{
			if (in_debugkeys->ivalue)
				fprintf (stderr, "translated to %d\n", sdl_to_key[sdl_key]);
			keyb->keyDown (sdl_to_key[sdl_key]);
		}
	}
	
	void
	sdlViewportData::onKeyUp (int sdl_key)
	{
		smartPtr <inputDevice> keyb = g_engine->getInputDevice ();
		// remap sdl codes to fe codes
		if (sdl_key >= (sizeof (sdl_to_key) / sizeof (int)) || sdl_to_key[sdl_key] == 0)
			keyb->keyUp (sdl_key);
		else
			keyb->keyUp (sdl_to_key[sdl_key]);
	}
	
	// viewport base
	
	sdlViewport::sdlViewport (void)
	{
		mpViewportData = new sdlViewportData ();
		init (g_engine->getApplication ()->getTitle ());
	}
	
	sdlViewport::~sdlViewport (void)
	{
		if (mpViewportData)
		{
			delete mpViewportData;
			mpViewportData = NULL;
		}
	}
	
	void
	sdlViewport::init (const char *appName)
	{
		mpViewportData->init (appName);
	}
	
	// common window access
	void
	sdlViewport::setPosition (int x, int y)
	{
	}
	
	void
	sdlViewport::setSize (int w, int h)
	{
	}
	
	int
	sdlViewport::getLeft (void) const
	{
		return 0;
	}
	
	int
	sdlViewport::getTop (void) const
	{
		return 0;
	}
	
	int
	sdlViewport::getWidth (void) const
	{
		return SDL_GetVideoSurface ()->w;
	}
	
	int
	sdlViewport::getHeight (void) const
	{
		return SDL_GetVideoSurface ()->h;
	}
	
	void
	sdlViewport::close (ulong flags)
	{
		// emulate SDL_QUIT
		SDL_Event event;
		memset (&event, 0, sizeof (SDL_Event));
		event.type = SDL_QUIT;
		SDL_PushEvent (&event);
	}
	
	sdlViewportData*
	sdlViewport::getViewportData (void)
	{
		return mpViewportData;
	}
	
	void sdlViewport::mainLoop (void)
	{
		// run system loop
		bool mbRunning = true;
		while (mbRunning)
		{
			SDL_Event event;
	
			while (SDL_PollEvent(&event))
			{
				if (event.type == SDL_QUIT)
				{
					mbRunning = false;
					break;
				}
				mpViewportData->handleEvent (&event);	
			}
	
			if (mbRunning)
				g_engine->tick();
		}
	}
	
}

