/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_SDLRENDERERDATA_H
#define __F_SDLRENDERERDATA_H

#include <SDL/SDL.h>

namespace fe
{
	
	const int SDL_MAX_VIDMODES = 256;
	class sdlRendererData : public baseObject
	{
		friend class sdlRenderer;
	
	protected:
		
		struct vidMode_t
		{
			int w, h, bpp;
		};
		
		size_t	getCurrentVidMode (void) const;
		void	setCurrentVidMode (size_t mode);
		const vidMode_t& getVidModeDescr (int mode) const;
		void	setWindowed (bool windowed);
		size_t	numVidModes (void) const;
	
		void	reset (void);
	
	private:
	
		bool		mbWindowed;
		size_t		mCurrentMode;
		vidMode_t	mVidModes[SDL_MAX_VIDMODES];
		int mNumVidModes;
		void addModes (SDL_Rect **modes, int bpp);
	
	public:
	
		sdlRendererData (void);
		~sdlRendererData (void);
	
	/*	const displayMode&			getDisplayModeDescr( ulong adapter, ulong device, ulong mode ) const;
		const sdlRenderDeviceInfo&		getDeviceDescr( ulong adapter, ulong device ) const;
		const displayAdapterInfo&		getAdapterDescr( ulong adapter ) const;
	
		displayMode&					getDisplayModeDescr( ulong adapter, ulong device, ulong mode );
		sdlRenderDeviceInfo&				getDeviceDescr( ulong adapter, ulong device );
		displayAdapterInfo&			getAdapterDescr( ulong adapter );*/
	};
	
}

#endif // __F_SDLRENDERERDATA_H

