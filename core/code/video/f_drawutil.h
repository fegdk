/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_DRAWUTIL_H
#define __F_DRAWUTIL_H

#include "f_types.h"
#include "f_baseobject.h"

namespace fe
{
	
	// some routines for quick and easy drawing of different widgets
	class baseTexture;
	
	class FE_API drawUtil : public baseObject
	{
	
	private:
	
		bool	mbAllowTransforms;
		float	mZValue;
		
	
	public:
		
		enum cullmode {cull_front, cull_back};
		enum stencilop {keep, incr, decr, incr_wrap, decr_wrap};
		enum cmp {never, less, equal, lequal, greater, notequal, gequal, always};
	
		drawUtil (void);
	
		// misc renderstates
		void ambientPass (bool onoff);
		void lightingPass (bool onoff);
	
		bool hasTwoSidedStencil (void) const;
	
		// stencilfunc is "always 0 ~0"
		void enableStencilTest (bool onoff) const;
		void setStencilFunc (cmp func) const;
	
		void enableZWrite (bool onoff) const;
		void enableZTest (bool onoff) const;
		void setZFunc (cmp func) const;
	
		void enableColorWrite (bool onoff) const;
	
		// here we have only z ops
		void setStencilZOps (stencilop zfail, stencilop zpass) const;
		void setTwoSidedStencilZOps (stencilop zfail_front, stencilop zpass_front, stencilop zfail_back, stencilop zpass_back) const;
	
		void setCullMode (cullmode mode) const;
	
		// here we have both z and stencil ops
		void setStencilOps (stencilop fail, stencilop zfail, stencilop zpass) const;
	
		// transformation config
		void allowTransforms (bool onoff);
		void setZValue (float z) { mZValue = z; }
		float getZValue (void) { return mZValue; }
	
		// picture drawing
		void	setOrthoTransforms (void);
		void drawPic (float x, float y, float w, float h, float s1, float t1, float s2, float t2, float4 color, smartPtr <baseTexture> tex, bool alpha);
		void drawPic (float x, float y, float w, float h, float s1, float t1, float s2, float t2, float4 color, bool alpha);
		void drawLine (float x1, float y1, float x2, float y2, float4 color, bool alpha=false);
	
		// very fast (GPU-friendly) routines for rendering primitives
		// primitive order is lost during usage
		// just use drawPic between Begin and End
		void fastDrawBegin (void);
		void fastDrawEnd (void);
	
		// unbinds texture from specified TU
		void unbindTexture (int stage);
	
	};
	
}

#endif

