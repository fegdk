/*
    gdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_BASETEXTURE_H
#define __F_BASETEXTURE_H

#include "f_baseobject.h"
#include "f_helpers.h"

namespace fe
{
	
	// FIXME: should this be in types.h (?)
	struct lockedRect
	{
		uint	pitch;
		void*	pBits;
	};
	
	class FE_API baseTexture : public baseObject
	{
		
	public:
	
		virtual void	bind (int stage) = 0;
	
		/**
		 * required to bind texture object to shader
		 * @return id or pointer which may be
		 * passed directly to specific graphics API
		 */
		virtual int_ptr getObject (void) const = 0;
	};
	
	typedef smartPtr <baseTexture> baseTexturePtr;
	
}

#endif // __F_BASETEXTURE_H
