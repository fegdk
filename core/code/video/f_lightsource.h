/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_LIGHTSOURCE_H
#define __F_LIGHTSOURCE_H

#include "f_sceneobject.h"

namespace fe
{

	// light source class used for dynamic lighting calculation
	
	class FE_API lightSource : public sceneObject
	{
	public:
	
		enum type {omni, spot, direct};
	
	private:
			
		type mType;
		vector3 mPosition;
		vector3 mDirection;
		
	public:
	
		lightSource (void);
		virtual ~lightSource (void);
	
		const vector3& getPosition (void) const;
		const vector3& getDirection (void) const;
		void setPosition (const vector3 &pos);
		void setDirection (const vector3 &dir);
		type getType (void) const;
		void setType (const type t);
		
		virtual void loose (void);
		virtual void restore (void);
	};
	
}

#endif // __F_LIGHTSOURCE_H

