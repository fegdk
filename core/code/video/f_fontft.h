/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_FONTFT_H
#define __F_FONTFT_H

#include "f_helpers.h"
#include "f_texture.h"
#include "f_fragileobject.h"

struct FT_LibraryRec_;
typedef struct FT_LibraryRec_  *FT_Library;
struct FT_FaceRec_;
typedef struct FT_FaceRec_*  FT_Face;
typedef unsigned char  FT_Byte;

namespace fe
{
	const int FONT_GLYPH_HASH_SIZE = 256;
	const int FONT_MAX_TEXTURES = 16;
	const int FONT_INITIAL_GLYPHS = 256;
	
	class FE_API fontFT : public fragileObject
	{
	public:
		// format flags
	
		// alignment
		static const int	hAlignLeft;		// default
		static const int	hAlignRight;
		static const int	hAlignCenter;
		static const int	hAlignStretch;
		static const int	vAlignTop;
		static const int	vAlignBottom;
		static const int	vAlignCenter;
	
		// word wrap
		static const int	wordWrap;
		// removes line gap before first line
		static const int	remove1stLineGap;
		static const int	useClipRect;
	
	private:
	
		struct glyph
		{
			int hashNext;		// next in mGlyphHash
			int code;			// unicode character
			int ft_glyph_index;	// index of freetype glyph
			int texture;		// texture page
			int origin[2];		// glyph origin
			int mins[2];		// x and y of glyph's upper left corner
			int maxs[2];		// x and y of glyphs lower right corner
			int advance[2];		// advance of a glyph
		};
	
		cStr mFontFileName;
		FT_Library mLibrary;
		FT_Face mFace;
	
		mutable texturePtr mpTextures[FONT_MAX_TEXTURES];
		mutable int mNumTextures;
		mutable glyph *mpGlyphs;
		mutable int mNumGlyphs;
		mutable int mReservedGlyphs;
		mutable int mGlyphHash[FONT_GLYPH_HASH_SIZE];
		mutable int mCurrentX, mCurrentY;
		FT_Byte* mpFntCache;	// memory-copy of font file
		size_t mFntCacheSize;
	
		bool		mbLost;
	
		// parms
		int			mHeight;
		bool		mbBold;
		bool		mbItalic;
		bool		mbAntialias;
		
		texturePtr allocTexture (void) const;
		int getGlyphIdx (int c) const;
	
	protected:
	
		~fontFT( void );
	
	private:
	
		void restore( void );
		void loose( void );
	
		char*	preformat( const char *text
			, int wx, int wy, int ww, int wh	// page window
			, unsigned long format_flags ) const;	// flags
	
		int			getLineCnt( const char *preformatted_text
			, int wx, int wy, int ww, int wh	// page window
			, unsigned long format_flags ) const;	// flags
	
		void		freePreformatted( char *preformatted_text ) const;
	
		void drawTextStringPreformatted (const char *formatted, int sx, int sy, int sw, int sh, int wx, int wy, int ww, int wh, unsigned long color, unsigned long flags) const;
	
	public:
	
		// syntax:
		// "name[:i_size[:b_antialias[:b_bold[:b_italic]]]"
		// antialiased, normal, regular by default
		fontFT (const char *name);
		fontFT (charParser &parser, const char *name);
	
	//	static fontFT*	createFromFile( const char *fname, int size, bool antialias = true, bool bold = false, bool italic = false );
	
		void	drawTextString( const char *text, int sx, int sy, unsigned long color ) const;
		void	drawTextStringEx( const char *text, int size, int sx, int sy, unsigned long color, float spacing, bool setProj = true ) const;
	
		int		getTextHeight( void ) const;
		int		getLineGap( void ) const;
	
		void	getTextExtent( const char *text, size_t length, int &cx, int &cy ) const;
	
		// slow! recommended to cache returned value!
		int		getNumberOfLines( const char *text
			, int wx, int wy, int ww, int wh	// page window
			, unsigned long format_flags ) const;	// flags
	
		void	drawTextStringFormatted( const char *text
			, int sx, int sy, int sw, int sh	// display window
			, int wx, int wy, int ww, int wh	// page window
			, unsigned long color				// outer color
			, unsigned long format_flags ) const;		// flags 
	
	};
	
	typedef smartPtr <fontFT> fontFTPtr;
	
}

#endif // __F_FONTFT_H
