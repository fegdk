/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_FRAGILEOBJECT_H
#define __F_FRAGILEOBJECT_H

#include "f_baseobject.h"

namespace fe
{

	class FE_API fragileObject : public baseObject
	{
	friend class fragileObjectRegistry;
	
	protected:
		fragileObject *next;
		fragileObject *prev;
	
		fragileObject (void);
		virtual ~fragileObject (void);
	
	public:
	
		virtual void loose (void) = 0;
		virtual void restore (void) = 0;
	};
	
	class FE_API fragileObjectRegistry
	{
	
	protected:
	
//		typedef std::list <fragileObjectPtr> fragObjList;
//		fragObjList			mResourceList;
		fragileObject *mResourceList;
	
	public:
		
		fragileObjectRegistry (void) {
			mResourceList = NULL;
		}
	
		void				registerResource (fragileObject *res )
		{
			assert (res && !res->next && !res->prev);
			res->next = mResourceList;
			if (mResourceList)
				mResourceList->prev = res;
			mResourceList = res;
		}
	
		void				unregisterResource (fragileObject *res)
		{
			if (mResourceList == res)
				mResourceList = res->next;
			if (res->prev)
				res->prev->next = res->next;
			if (res->next)
				res->next->prev = res->prev;
			res->next = res->prev = NULL;
		}
	
		void				looseAll (void)
		{
			fragileObject *o = mResourceList;
			while (o) {
				o->loose ();
				o = o->next;
			}
		}
		
		void				restoreAll (void)
		{
			fragileObject *o = mResourceList;
			while (o) {
				o->restore ();
				o = o->next;
			}
		}
	
	};
	
}

#endif // __F_FRAGILEOBJECT_H

