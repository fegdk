/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_EFFECTPARMS_H
#define __F_EFFECTPARMS_H

#include "f_baseobject.h"
#include "f_math.h"
#include "f_basetexture.h"

namespace fe
{

	enum effectParm_t
	{
		effectParm_WorldMatrix,
		effectParm_ViewMatrix,
		effectParm_ProjMatrix,
		
		effectParm_WorldViewProjMatrix,
		
		effectParm_WorldViewMatrix,
		effectParm_InvWorldMatrix,
		effectParm_InvViewMatrix,
		effectParm_ViewToModelMatrix,
	
		effectParm_EyePosition,
		effectParm_EyeDirection,
	
		effectParm_AmbientColor,
		
		effectParm_LightPos0,
		effectParm_LightDir0,
		effectParm_LightDiffuse0,
		effectParm_LightSpecular0,
		effectParm_LightPos1,
		effectParm_LightPos2,
		effectParm_LightPos3,
		effectParm_LightPos4,
		effectParm_LightPos5,
		effectParm_LightPos6,
		effectParm_LightPos7,
	
//		effectParm_LightmapTexture,
	
		effectParm_Last
	};
	
	class FE_API effectParms : public baseObject
	{
	
	protected:
	
		struct parm_t {
			union
			{
				float	matrix[16];	// in row-vector form
				float	vector[4];
			};
			baseTexturePtr	texture;
		};
	
	    parm_t mParms[effectParm_Last];
		bool mValidParms[effectParm_Last];
	
		void	invalidateDependents (effectParm_t index);
	
	public:
		effectParms (void);
		~effectParms (void);
	
		// FIXME: use const/mutable
		const matrix4&	getMatrix (effectParm_t index);	// can't be const since auto-derive may take place
		const float4&	getFloat4 (effectParm_t index);	// can't be const since auto-derive may take place
		baseTexturePtr	getTexture (effectParm_t index);	// can't be const since auto-derive may take place
	
		void	setMatrix (effectParm_t index, const matrix4 &m);
		void	setFloat4 (effectParm_t index, const float4 &v);
		void	setFloat4 (effectParm_t index, const vector3 &v);
		void	setTexture (effectParm_t index, const baseTexturePtr &tex);
	
	};

	typedef smartPtr <effectParms> effectParmsPtr;
	
}

#endif // __F_EFFECTPARMS_H

