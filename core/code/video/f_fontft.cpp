/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_fontft.h"
#include "f_error.h"
#include "f_filesystem.h"
#include "f_engine.h"
#include "f_effect.h"
#include "f_baseviewport.h"
#include "f_texture.h"
#include "f_drawutil.h"
#include "f_baserenderer.h"
#include "f_resourcemgr.h"
#include "utf8.h"
#include "cvars.h"

extern "C" {
#include <jpeglib.h>
}

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
//#include FT_INTERNAL_OBJECTS_H
//#include FT_INTERNAL_CALC_H
#include FT_OUTLINE_H
#include FT_TRIGONOMETRY_H
#include FT_SYNTHESIS_H

namespace fe
{
#if 0
	#define FT_BOLD_THRESHOLD  0x0100
	
	
	/*************************************************************************/
	/*************************************************************************/
	/****                                                                 ****/
	/****   EXPERIMENTAL OBLIQUING SUPPORT                                ****/
	/****                                                                 ****/
	/*************************************************************************/
	/*************************************************************************/
	
	FT_EXPORT_DEF (void)
	FT_GlyphSlot_Oblique (FT_GlyphSlot  slot)
	{
		FT_Matrix    transform;
		FT_Outline*  outline = &slot->outline;
	
	
		/* only oblique outline glyphs */
		if (slot->format != FT_GLYPH_FORMAT_OUTLINE)
			return;
	
		/* we don't touch the advance width */
	
		/* For italic, simply apply a shear transform, with an angle */
		/* of about 12 degrees.                                      */
	
		transform.xx = 0x10000L;
		transform.yx = 0x00000L;
	
		transform.xy = 0x06000L;
		transform.yy = 0x10000L;
	
		FT_Outline_Transform (outline, &transform);
	}
	
	
	/*************************************************************************/
	/*************************************************************************/
	/****                                                                 ****/
	/****   EXPERIMENTAL EMBOLDENING/OUTLINING SUPPORT                    ****/
	/****                                                                 ****/
	/*************************************************************************/
	/*************************************************************************/
	
	
	
	static int
	ft_test_extrema (FT_Outline*  outline,
					int          n)
	{
		FT_Vector  *prev, *cur, *next;
		FT_Pos      product;
		FT_Int      c, first, last;
	
	
		/* we need to compute the `previous' and `next' point */
		/* for these extrema.                                 */
		cur  = outline->points + n;
		prev = cur - 1;
		next = cur + 1;
	
		first = 0;
		for (c = 0; c < outline->n_contours; c++)
		{
			last = outline->contours[c];
	
			if (n == first)
				prev = outline->points + last;
	
			if (n == last)
				next = outline->points + first;
	
			first = last + 1;
		}
	
		product = FT_MulDiv (cur->x - prev->x,   /* in.x  */
			next->y - cur->y,   /* out.y */
			0x40)
			-
			FT_MulDiv (cur->y - prev->y,   /* in.y  */
			next->x - cur->x,   /* out.x */
			0x40);
	
		if (product)
			product = product > 0 ? 1 : -1;
	
		return product;
	}
	
	
	/* Compute the orientation of path filling.  It differs between TrueType */
	/* and Type1 formats.  We could use the `FT_OUTLINE_REVERSE_FILL' flag,  */
	/* but it is better to re-compute it directly (it seems that this flag   */
	/* isn't correctly set for some weird composite glyphs currently).       */
	/*                                                                       */
	/* We do this by computing bounding box points, and computing their      */
	/* curvature.                                                            */
	/*                                                                       */
	/* The function returns either 1 or -1.                                  */
	/*                                                                       */
	static int
	ft_get_orientation (FT_Outline*  outline)
	{
		FT_BBox  box;
		FT_BBox  indices;
		int      n, last;
	
	
		indices.xMin = -1;
		indices.yMin = -1;
		indices.xMax = -1;
		indices.yMax = -1;
	
		box.xMin = box.yMin =  32767;
		box.xMax = box.yMax = -32768;
	
		/* is it empty ? */
		if (outline->n_contours < 1)
			return 1;
	
		last = outline->contours[outline->n_contours - 1];
	
		for (n = 0; n <= last; n++)
		{
			FT_Pos  x, y;
	
	
			x = outline->points[n].x;
			if (x < box.xMin)
			{
				box.xMin     = x;
				indices.xMin = n;
			}
			if (x > box.xMax)
			{
				box.xMax     = x;
				indices.xMax = n;
			}
	
			y = outline->points[n].y;
			if (y < box.yMin)
			{
				box.yMin     = y;
				indices.yMin = n;
			}
			if (y > box.yMax)
			{
				box.yMax     = y;
				indices.yMax = n;
			}
		}
	
		/* test orientation of the xmin */
		n = ft_test_extrema (outline, indices.xMin);
		if (n)
			goto Exit;
	
		n = ft_test_extrema (outline, indices.yMin);
		if (n)
			goto Exit;
	
		n = ft_test_extrema (outline, indices.xMax);
		if (n)
			goto Exit;
	
		n = ft_test_extrema (outline, indices.yMax);
		if (!n)
			n = 1;
	
	Exit:
		return n;
	}
	
	
	FT_EXPORT_DEF (void)
	FT_GlyphSlot_Embolden (FT_GlyphSlot  slot)
	{
		FT_Vector*   points;
		FT_Vector    v_prev, v_first, v_next, v_cur;
		FT_Pos       distance;
		FT_Outline*  outline = &slot->outline;
		FT_Face      face = FT_SLOT_FACE (slot);
		FT_Angle     rotate, angle_in, angle_out;
		FT_Int       c, n, first, orientation;
	
	
		/* only embolden outline glyph images */
		if (slot->format != FT_GLYPH_FORMAT_OUTLINE)
			return;
	
		/* compute control distance */
		distance = FT_MulFix (face->units_per_EM / 60,
			face->size->metrics.y_scale);
	
		orientation = ft_get_orientation (outline);
		rotate      = FT_ANGLE_PI2*orientation;
	
		points = outline->points;
	
		first = 0;
		for (c = 0; c < outline->n_contours; c++)
		{
			int  last = outline->contours[c];
	
	
			v_first = points[first];
			v_prev  = points[last];
			v_cur   = v_first;
	
			for (n = first; n <= last; n++)
			{
				FT_Pos     d;
				FT_Vector  in, out;
				FT_Fixed   scale;
				FT_Angle   angle_diff;
	
	
				if (n < last) v_next = points[n + 1];
				else            v_next = v_first;
	
				/* compute the in and out vectors */
				in.x  = v_cur.x - v_prev.x;
				in.y  = v_cur.y - v_prev.y;
	
				out.x = v_next.x - v_cur.x;
				out.y = v_next.y - v_cur.y;
	
				angle_in   = FT_Atan2 (in.x, in.y);
				angle_out  = FT_Atan2 (out.x, out.y);
				angle_diff = FT_Angle_Diff (angle_in, angle_out);
				scale      = FT_Cos (angle_diff/2);
	
				if (scale < 0x400L && scale > -0x400L)
				{
					if (scale >= 0)
						scale = 0x400L;
					else
						scale = -0x400L;
				}
	
				d = FT_DivFix (distance, scale);
	
				FT_Vector_From_Polar (&in, d, angle_in + angle_diff/2 - rotate);
	
				outline->points[n].x = v_cur.x + distance + in.x;
				outline->points[n].y = v_cur.y + distance + in.y;
	
				v_prev = v_cur;
				v_cur  = v_next;
			}
	
			first = last + 1;
		}
	
		slot->metrics.horiAdvance = (slot->metrics.horiAdvance + distance*4) & -64;
	}
	
	
	/* END */
#endif
	
	const int	FontTextureSize = 256;
	
	// horz alignment
	const int	fontFT::hAlignLeft	= 0x00000001;		// could it be a default
	const int	fontFT::hAlignRight	= 0x00000002;
	const int	fontFT::hAlignCenter	= 0x00000004;
	const int	fontFT::hAlignStretch = 0x00000008;
	
	// vert alignment
	const int	fontFT::vAlignTop		= 0x00000010;
	const int	fontFT::vAlignBottom	= 0x00000020;
	const int	fontFT::vAlignCenter	= 0x00000040;
	
	// word wrap
	const int	fontFT::wordWrap		= 0x00000100;
	
	// removes line gap before first line
	const int	fontFT::remove1stLineGap = 0x00000200;
	const int	fontFT::useClipRect = 0x00000400;
	
	//fontFT::fontFT (const char *fname, const char *fontname, int size, bool antialias, bool bold, bool italic)
	fontFT::fontFT (const char *name)
	{
		mLibrary = NULL;
		mFace = NULL;
		mpFntCache = NULL;
	
		// parse fname
		char *buf = new char[strlen (name)+1];
		char *tmp = buf;
		strcpy (tmp, name);
		char *colon;
	
		mbAntialias = 0;
		mHeight = 0;
		mbBold = 0;
		mbItalic = 0;
	
		// font file name
		colon = strchr (tmp, ':');
		if (colon)
		{
			*colon = 0;
			mFontFileName = tmp;
			*colon = ':';
			tmp = colon + 1;
	
			// size
			colon = strchr (tmp, ':');
			if (colon)
			{
				*colon = 0;
				mHeight = atoi (tmp);
				*colon = ':';
				tmp = colon + 1;
	
				// antialias
				colon = strchr (tmp, ':');
				if (colon)
				{
					*colon = 0;
					mbAntialias = atoi (tmp) ? true : false;
					*colon = ':';
					tmp = colon + 1;
	
					// bold
					colon = strchr (tmp, ':');
					if (colon)
					{
						*colon = 0;
						mbBold = atoi (tmp) ? true : false;
						*colon = ':';
						tmp = colon + 1;
	
						// italic
						colon = strchr (tmp, ':');
						if (colon)
						{
							*colon = 0;
							mbItalic = atoi (tmp) ? true : false;
							*colon = ':';
							tmp = colon + 1;
						}
						else if (*tmp)
							mbItalic = atoi (tmp) ? true : false;
					}
					else if (*tmp)
						mbBold = atoi (tmp) ? true : false;
				}			
				else if (*tmp)
					mbAntialias = atoi (tmp) ? true : false;
			}
			else if (*tmp)
				mHeight = atoi (tmp);
		}
		else if (*tmp)
			mFontFileName = name;
	
		delete[] buf;
			
		setName (name);
	
	/*
		NOTE: oooooooold stuff from win32 gdi days
		96 is the default windows value for LOGPIXELSY
		72 is the value from formula from msdn article on LOGFONT structure
		[q] 	For the MM_TEXT mapping mode, you can use the following formula to specify a height for a font with a specified point size: 
		lfHeight = -MulDiv (PointSize, GetDeviceCaps (hDC, LOGPIXELSY), 72); 	[/q]
	*/
	
		// try to load the glyph name as-is
		char fname[100] = "fonts/";
		strcat (fname, mFontFileName);
		file *f;
		// open using fe filesystem
		f = g_engine->getFileSystem ()->openFile (fname, FS_MODE_READ);
		if (!f)
		{
			if (!strstr (fname, ".ttf"))
				strcat (fname, ".ttf");
			f = g_engine->getFileSystem ()->openFile (fname, FS_MODE_READ);
			if (!f)
				sys_error ("failed to open font file (%s)", name);
		}
	
		mFntCacheSize = f->getSize ();
		mpFntCache = new FT_Byte[mFntCacheSize];
		f->read (mpFntCache, mFntCacheSize);
		f->close ();
	
		mbLost = true;

		mpGlyphs = new glyph[FONT_INITIAL_GLYPHS];
		mReservedGlyphs = FONT_INITIAL_GLYPHS;
		mNumTextures = 0;
	
		restore ();
	}
	
	fontFT::fontFT (charParser &parser, const char *name)
	{
	}
	
	fontFT::~fontFT (void)
	{
		if (!mbLost)
			loose ();
		if (mpFntCache)
		{
			delete[] mpFntCache;
			mpFntCache = NULL;
		}

		if (mpGlyphs)
		{
			delete[] mpGlyphs;
		}
	}
	
	texturePtr fontFT::allocTexture (void) const
	{
		if (mNumTextures == FONT_MAX_TEXTURES)
			sys_error ("too many font pages for %s", name());
		cStr texture_name;
		texture_name.printf ("empty:%d:%d:%s:%d", FontTextureSize, FontTextureSize, mName.c_str (), mNumTextures);
		texturePtr t = g_engine->getResourceMgr ()->createTexture (texture_name);
		mpTextures[mNumTextures++] = t;
	
		// will black
		lockedRect rc = t->lockRect (0, NULL, false);
		memset (rc.pBits, 0x00, rc.pitch * t->getHeight ());
		t->unlockRect (0);
	
		return t;
	}
	
	void
	screenshot_write (const char *fname, const char *bits, int pitch)
	{
		struct jpeg_compress_struct cinfo;
		struct jpeg_error_mgr       jerr;
	
		FILE*      fp;     //Target file 
		int        nSampsPerRow; //Physical row width in image buffer 
		int i;
		
		cinfo.err = jpeg_std_error(&jerr); //Use default error handling (ugly!)
	
		jpeg_create_compress(&cinfo);
	
		fp = fopen (fname, "w+b");
	
		jpeg_stdio_dest(&cinfo, fp);
	
		cinfo.image_width      = FontTextureSize;  //Image width and height, in pixels 
		cinfo.image_height     = FontTextureSize;
		cinfo.input_components = 1;              //Color components per pixel
		//(RGB_PIXELSIZE - see jmorecfg.h)
		cinfo.in_color_space   = JCS_GRAYSCALE;      //Colorspace of input image
	
		jpeg_set_defaults(&cinfo);
	
		jpeg_set_quality(&cinfo,
			100, //Quality: 0-100 scale
			TRUE);    //Limit to baseline-JPEG values
	
		jpeg_start_compress(&cinfo, TRUE);
	
		//JSAMPLEs per row in output buffer
		nSampsPerRow = cinfo.image_width * cinfo.input_components; 
	
		//Write the array of scan lines to the JPEG file
		for (i = 0; i < FontTextureSize; i++)
		{
	//		unsigned char *c = &bits[i*width*3];
			unsigned char *c = new unsigned char[FontTextureSize];
			for (int k = 0; k < FontTextureSize; k++)
			{
				c[k] = bits[i * pitch + k*4+2];
			}
			jpeg_write_scanlines(&cinfo, &c, 1);
			delete[] c;
		}
	
		jpeg_finish_compress(&cinfo); //Always finish
	
		fclose(fp);
	
		jpeg_destroy_compress(&cinfo); //Free resources
	}
	
	int fontFT::getGlyphIdx (int c) const
	{
		int  ptsize = mHeight;
		FT_Error     error;
		FT_Encoding  encoding = ft_encoding_unicode;
		int hash = c&(FONT_GLYPH_HASH_SIZE-1);
		int v = mGlyphHash[hash];
		glyph *g = NULL;
		while (v != -1) {
			if (c == mpGlyphs[v].code) {
				g = &mpGlyphs[v];
				break;
			}
			v = mpGlyphs[v].hashNext;
		}

		if (g)
			return v;
		if (mNumGlyphs == mReservedGlyphs) {
			// reallocate glyphs
			mReservedGlyphs += FONT_INITIAL_GLYPHS;
			glyph *ng = new glyph[mReservedGlyphs];
			memcpy (ng, mpGlyphs, mNumGlyphs * sizeof (glyph));
			delete[] mpGlyphs;
			mpGlyphs = ng;
		}
		int sx = mCurrentX, sy = mCurrentY;
		texturePtr t;
		if (!mNumTextures)
			t = allocTexture ();
		else
			t = mpTextures[mNumTextures - 1];
		lockedRect rc = t->lockRect (0, NULL, 0);
		// add glyph
		g = &mpGlyphs[mNumGlyphs];
		int idx = mNumGlyphs;
		g->hashNext = mGlyphHash[hash];
		mGlyphHash[hash] = idx;
		mNumGlyphs++;
		g->texture = mNumTextures - 1;
		g->mins[0] = 0;
		g->mins[1] = 1;
		g->maxs[0] = 0;
		g->maxs[1] = 1;
		FT_UInt    load_flags = FT_LOAD_DEFAULT;
		error = FT_Select_Charmap (mFace, encoding);
		if (error)
			sys_error ("Invalid charmap");
		g->ft_glyph_index = FT_Get_Char_Index (mFace, (FT_ULong)c);
		g->code = c;
		error = FT_Load_Glyph (mFace, g->ft_glyph_index, load_flags);
		if (error)
			sys_error ("FT_Load_Glyph failed!");

/*			if (mbBold)
			FT_GlyphSlot_Embolden (mFace->glyph);
		if (mbItalic)
			FT_GlyphSlot_Oblique (mFace->glyph);*/

		FT_Glyph gimage;
		error = FT_Get_Glyph (mFace->glyph, &gimage);
		if (error)
			sys_error ("FT_Get_Glyph failed!");

		FT_BBox bb;
		FT_Glyph_Get_CBox (gimage, ft_glyph_bbox_truncate, &bb);
		g->origin[0] = bb.xMin;
		g->origin[1] = bb.yMin;

		// convert to bitmap
		for (;;)
		{
			FT_Glyph image;
			error = FT_Glyph_Copy (gimage, &image);
			if (error)
				break;
			error = FT_Glyph_To_Bitmap (&image, mbAntialias ? FT_RENDER_MODE_NORMAL : FT_RENDER_MODE_MONO, 0, 1);
			if (error)
				break;
			//				throw genericError ("FT_Glyph_To_Bitmap failed!");
			FT_BitmapGlyph  bitmap = (FT_BitmapGlyph)image;
			FT_Bitmap*      source = &bitmap->bitmap;

			// blit to texture
			int rows   = source->rows;
			int width  = source->width;
			int pitch  = source->pitch;
			unsigned char *buffer = source->buffer;

			int max = source->num_grays - 1;
			if (max == 0)
				max = 1;
			if (!mbAntialias)
				max = 0xff;

			{
				int             y;
				unsigned char*  read;
				ulong*  write;

				read   = buffer;

				if (sx + width >= FontTextureSize)
				{
					sx = 0;
					sy += ptsize;
				}

				g->mins[0] = sx;
				g->mins[1] = sy;
				g->maxs[0] = sx + width;
				g->maxs[1] = sy + rows;

				g->advance[0] = mFace->glyph->advance.x >> 6;
				g->advance[1] = mFace->glyph->advance.y >> 6;
/*
// this hack was used for some crazy font which had tiny space symbol
				if (c == 32)
				{
					// space may be to small
					g.advance[0]*=2;
				}*/

				uchar *dst = (unsigned char *)rc.pBits;
				write  = (ulong*) (dst + sx*4 + rc.pitch * sy);
				y = rows;
				do
				{
					if (sy + (rows-y) >= FontTextureSize)
					{
						// out of texture space
						sx = 0; sy = 0;
						t->unlockRect (0);
						t = allocTexture ();
						lockedRect rc = t->lockRect (0, NULL, 0);
						y = rows;
						dst = (unsigned char *)rc.pBits;
						read   = buffer;
						write  = (ulong*) (dst + sx*4 + rc.pitch * sy);
						g->mins[0] = sx;
						g->mins[1] = sy;
						g->maxs[0] = sx + width;
						g->maxs[1] = sy + rows;
						g->texture = mNumTextures - 1;
						continue;
					}
					unsigned char*  _read  = read;
					uchar*  _write = (uchar *)write;
					int             x      = width;
					int				xx = 7;

					while (x > 0)
					{
						unsigned char    val;

						val = *_read;

						if (!mbAntialias)
							val = ( (val & (1<<xx))>>xx) ? 0xff : 0x00;

						if (val)
						{
							if (val == max)
							{
								_write[0] = 0xff;
								_write[1] = 0xff;
								_write[2] = 0xff;
								_write[3] = 0xff;
							}
							else
							{
								/* compose gray value */
								unsigned char pix;

								pix = _write[0];

								int  d, half = max >> 1;
								d = (int)0xff - pix;
								pix += (unsigned char) ((val*d + half)/max);
								_write[0] = pix;
								_write[1] = pix;
								_write[2] = pix;
								_write[3] = pix;
							}
						}
						_write +=4;
						xx--;
						if (mbAntialias || xx < 0)
						{
							xx = 7;
							_read  ++;
						}
						x--;
					}

					read  += pitch;
					write = (ulong*) (( (uchar *)write)+rc.pitch);
					y--;
				}
				while (y > 0);
			}
			sx += width+1;
			FT_Done_Glyph (image);
			break;
		}

		FT_Done_Glyph (gimage);
		if (sys_debug_freetype->ivalue)
		{
			char str[100];
			sprintf (str, "%s%d.jpg", name (), mNumTextures-1);
			char *ptr;
			while (ptr = strstr (str, ":"))
			{
				*ptr = '-';
			}
			screenshot_write (str, (char *)rc.pBits, rc.pitch);
		}
		t->unlockRect (0);
		mCurrentX = sx;
		mCurrentY = sy;
		return idx;
	}
	
	void fontFT::restore (void)
	{
		FT_Error     error;
		int  hinted    = 1;
		int  antialias = mbAntialias;
		int  use_sbits = 0;
		int  kerning   = 1;
		int  use_gamma = 0;
		int  res = 72;
		int  ptsize = mHeight;
	
		error = FT_Init_FreeType (&mLibrary);
		if (error)
			sys_error ("Could not initialize FreeType library");
	
		error = FT_New_Memory_Face (mLibrary, mpFntCache, mFntCacheSize, 0, &mFace);
		if (error)
			sys_error ("FT_New_Memory_Face failed (%s)", mName.c_str ());
	
		// FIXME we should ALWAYS select from available font sizes instead of forcing font to scale
		if (ptsize)
		{
			(void)FT_Set_Char_Size (mFace,
					ptsize << 6,
					ptsize << 6,
					res,
					res);
		}
		else
		{
			mHeight = (mFace->available_sizes[0].size>>6)+1;
			fprintf (stderr, "INFO: autodetected %s font size is %d\n", mName.c_str (), mHeight);
		}
	
		mCurrentX = mCurrentY = 0;
	
		printf ("resetting glyph hash\n");
		mNumGlyphs = 0;
		for (int i = 0; i < FONT_GLYPH_HASH_SIZE; i++)
			mGlyphHash[i] = -1;
	
		mbLost = false;
	}
	
	void fontFT::loose (void)
	{
		for (int i = 0; i < mNumTextures; i++)
			mpTextures[i] = NULL;
		mNumTextures = 0;
		mNumGlyphs = 0;
		printf ("resetting glyph hash\n");
		for (int i = 0; i < FONT_GLYPH_HASH_SIZE; i++)
			mGlyphHash[i] = -1;
		if (mFace)
		{
			FT_Done_Face (mFace);
			mFace = NULL;
		}
		if (mLibrary)
		{
			FT_Done_FreeType (mLibrary);
			mLibrary = NULL;
		}
	
		mbLost = true;
	}
	
	void		fontFT::drawTextString (const char *text, int sx, int sy, unsigned long color) const
	{
		drawTextStringEx (text, -1, sx, sy, color, 0);
	}
	
	void		fontFT::drawTextStringEx (const char *text, int size, int sx_, int sy, unsigned long color, float spacing, bool setProj) const
	{
		int sz;
		char clr = 0;
		int clrstackdepth = 0;
		char clrstack[100] = { 0 };
	
		unsigned long colortable[] = {
			color,
			0xffff0000,
			0xff00ff00,
			0xff0000ff,
			0xffffff00,
			0xff00ffff,
			0xffff00ff,
			0xffffffff,
			0xff000000,
			0xff000000
		};
	
		// this will tell us size IN BYTES
		if (size == -1)
			sz = strlen (text);
		else
			sz = size;
	
		if (0 == sz)
			return;
	
		float sx = (float)sx_;
	
	//	g_engine->getDrawUtil ()->fastDrawBegin ();
	
	/*	if (setProj)
		{
			matrix4 mProj;
			matrix4 mIdent (true);
			mProj.orthoOffCenterLH (0, g_engine->getViewport ()->getWidth (), 0, g_engine->getViewport ()->getHeight (), .001f, 1.f);
			g_engine->getEffectParms ()->setMatrix (effectParm_WorldMatrix, mIdent);
			g_engine->getEffectParms ()->setMatrix (effectParm_ViewMatrix, mIdent);
			g_engine->getEffectParms ()->setMatrix (effectParm_ProjMatrix, mProj);
		}*/
	
		g_engine->getDrawUtil ()->allowTransforms (true);
	
		int fontpage = -1;
	
		const glyph *gprev = NULL;
		const char *c = text;
		int numchars = 0;
		while (c - text < sz)
		{
			if (*c == 1)
			{
				// change color
				c++;
				if (*c == 0)
					continue;
				else if (*c == (char)-1)
				{
					// prev color
					if (clrstackdepth > 0)
					{
						clrstackdepth--;
						clr = clrstack[clrstackdepth];
					}
					c++;
					continue;
				}
				else if (*c >= 1 && *c <= 10)
				{
					clrstackdepth++;
					clr = *c - 1;
					clrstack[clrstackdepth] = clr;
					c++;
					continue;
				}
			}
			else if (*c == 2 || *c == 3)
			{
				// skip
				c++;
				if (0 == *c)
					break;
				c++;
				continue;
			}
			else if (*c == '\t')
			{
				// make tab
				c++;
				sx += mpGlyphs[0].advance[0] * 8;
				continue;
			}
			else if (*c < 32 && *c > 0)
			{
				c++;
				continue;
			}
	
			// get glyph
			uint32 charcode;
			int incr = 0;
			if (*c > 0)
			{
				charcode = *c;
				incr = 1;
			}
			else
			{
				charcode = u8_nextchar (c, &incr);
			}
			int gidx = getGlyphIdx (charcode);
			const glyph *g = &mpGlyphs[gidx];
	
			int x = (int)floor (sx);
			int y = sy;
	
			if (gprev && 32 != charcode)
			{
				FT_Vector kern;
				FT_Get_Kerning (mFace, gprev->ft_glyph_index, g->ft_glyph_index,
					ft_kerning_default,
					&kern);
				x += kern.x >> 6;
				sx += kern.x >> 6;
			}
	
			// lower left
			x += g->origin[0];
			y -= g->origin[1];
			// convert to upper left
			y -= (g->maxs[1] - g->mins[1]);
			y += this->getTextHeight ();
	
			if (g->texture != fontpage)
			{
				fontpage = g->texture;
				mpTextures[fontpage]->bind (0);
			}
			g_engine->getDrawUtil ()->drawPic (x, y, g->maxs[0] - g->mins[0], g->maxs[1] - g->mins[1], (float)g->mins[0] / (float)FontTextureSize, (float)g->mins[1] / (float)FontTextureSize, (float)g->maxs[0] / (float)FontTextureSize, (float)g->maxs[1] / (float)FontTextureSize, colortable[clr], true);
	
			sx = sx + g->advance[0];
			if (32 == *c)
				sx += spacing;
			gprev = g;
			c += incr;
			numchars++;
		}
	}
	
	int		fontFT::getLineGap (void) const
	{
		return mHeight - ( (mFace->size->metrics.ascender + mFace->size->metrics.descender) >> 6);
	}
	
	int		fontFT::getTextHeight (void) const
	{
		return mHeight;
	}
	
	char*	fontFT::preformat (const char *text
									, int wx, int wy, int ww, int wh	// page window
									, unsigned long format_flags) const	// flags
	{
		if (NULL == text)
			return NULL;
		const char *start;
	
		// determine number of '\n's
		int numindents = 1;
		for (start = text; *start; start++)
		{
			if (*start == '\\' && * (start+1) == 'n')
				numindents++;
		}
	
		size_t str_len = strlen (text)+1+numindents * 4;
		char *formatted = new char[str_len];
		const char *end = text;
		char *fptr = formatted;
	
		// format whole text
		char *indent_ptr = fptr;
		indent_ptr[0] = 2;
		indent_ptr[1] = 1;
		indent_ptr[2] = 3;
		indent_ptr[3] = 1;
		fptr = indent_ptr + 4;
		for (;;)
		{
			if (0 == *end)
			{
				*fptr = *end;
				break;
			}
	
			if (*end < 32 && *end > 0)
			{
				end++;
				continue;
			}
	
			if (*end == '\\')
			{
				end++;
				if (*end == '0')
				{
					*fptr = 0;
					break;
				}
				else if (*end == '\\') // pass '\' char
				{
					*fptr = *end;
					fptr++;
					end++;
				}
				else if (*end == 'n')
				{
					// newline
					*fptr = '\n';
					end++;
					fptr++;
					indent_ptr = fptr;
					indent_ptr[0] = 2;
					indent_ptr[1] = 1;
					fptr += 2;
				}
				else if (*end == 't')
				{
					*fptr = '\t';
					end++;
					fptr++;
				}
				else if (*end == 'c')
				{
					end++;
					if (*end == 0)
					{
						*fptr = 0;
						break;
					}
					*fptr = 1;	// color tag
					fptr++;
					if (*end == '-')
						*fptr = -1;
					else if (*end >= '0' && *end <= '9')
						*fptr = (*end) - '0' + 1;	// FIXME: is it the right way?
					else // typo
					{
						* (fptr-1) = '\\';
						*fptr = 'c';
						fptr++;
						*fptr = *end;
					}
					fptr++;
					end++;
				}
				else if (*end == 'i')	// newline indentation
				{
					end++;
					if (*end == 0)
					{
						*fptr = 0;
						break;
					}
	
					indent_ptr[1] = *end - '0' + 1;
					end++;
				}
				else if (*end == 'I')	// newline indentation
				{
					end++;
					if (*end == 0)
					{
						*fptr = 0;
						break;
					}
	
					indent_ptr[3] = *end - '0' + 1;
					end++;
				}
			}
/*			else if (*end < 0)
			{
				*fptr = L'?';
				fptr++;
				end++;
			}*/
			else
			{
				*fptr = *end;
				fptr++;
				end++;
			}
		}
	
		assert (fptr - formatted + 1 <= (int)str_len);
		return formatted;
	}
	
	int			fontFT::getLineCnt (const char *text
									 , int wx, int wy, int ww, int wh	// page window
									 , unsigned long flags) const		// flags
	{
		if (NULL == text)
			return 0;
		// do precalc on numlines
		const char *start = text;
		int numlines = 0;
		bool i1 = true;
		bool i2 = false;
		int ix = 0, ix2 = 0;
		for (;;)
		{
			// break on terminating zero
			if (0 == *start)
			{
				break;
			}
			int wordcnt = 0;
			int w = 0;
	
			while (*start == 32)
				start++;
	
			const char *end = start;
			const char *lastword = start;
			int lastword_w = 0;
			int numchars = 0;
	
			// calculate a line
			for (;;)
			{
				if (0 == *end)
					break;
				else if ('\n' == *end)
				{
					end++;
					i1 = true;
					i2 = false;
					break;
				}
				else if (1 == *end)
				{
					end++;
					if (0 == *end)
						break;
					end++;
					continue;
				}
				else if (2 == *end)
				{
					end++;
					if (0 == *end)
						break;
					ix = *end-1;
					end++;
					continue;
				}
				else if (3 == *end)
				{
					end++;
					if (0 == *end)
						break;
					ix2 = *end-1;
					end++;
					continue;
				}
				else if (32 > *end)
				{
					end++;
					continue;
				}
	
				// get glyph
				int gidx = getGlyphIdx (*end);
				const glyph *g = &mpGlyphs[gidx];
				if (w + g->advance[0] >= ww && (flags & wordWrap))
				{
					if (0 == wordcnt)
					{
						// window is less than word width
					}
					else
					{
						end = lastword;
						w = lastword_w;
					}
	
					if (*start == 32)
					{
						start++;
					}
					if (* (end-1) == 32)
					{
						end--;
					}
	
					break;
				}
	
				if (*end == 32)
				{
					lastword = end;
					lastword_w = w;
					wordcnt++;
				}
				if (0 == numchars)
				{
					if (i1)
						w += ix * mpGlyphs[0].advance[0];
					else if (i2)
						w += ix2 * mpGlyphs[0].advance[0];
				}
				w += g->advance[0];
				numchars++;
	
				end++;
			}
			numlines++;
			start = end;
		}
		return numlines;
	}
	
	void		fontFT::freePreformatted (char *text) const
	{
		if (text)
			delete[] text;
	}
	
	int		fontFT::getNumberOfLines (const char *text
									   , int wx, int wy, int ww, int wh	// page window
									   , unsigned long flags) const		// flags
	{
		if (NULL == text)
			return 0;
		char *formatted = preformat (text, wx, wy, ww, wh, flags);
		int lines = getLineCnt (formatted, wx, wy, ww, wh, flags);
		freePreformatted (formatted);
		return lines;
	}
	
	void fontFT::drawTextStringPreformatted (const char *formatted, int sx, int sy, int sw, int sh, int wx, int wy, int ww, int wh, unsigned long color, unsigned long flags) const
	{
		bool vp_changed = false;
		int H = g_engine->getViewport ()->getHeight ();
		matrix4 mProj;
		if (flags&useClipRect)
		{
			mProj.orthoOffCenterLH (sx, sx + sw, H - (sy + sh), H - sy, .001f, 1.f);
		}
	
		const char *start = formatted;
		int ix = 0;
		int ix2 = 0;
	
		bool i1 = true;
		bool i2 = false;
	
		int line_gap = 0;
		if (flags & remove1stLineGap)
		{
			line_gap = getLineGap ();
		}
	
		int numlines = 0;
		if ( (flags & vAlignBottom) || (flags&vAlignCenter))
		{
			numlines = getLineCnt (formatted, wx, wy, ww, wh, flags);
			if (flags & vAlignBottom)
				wy = wy + wh - (numlines * getTextHeight () - line_gap) - line_gap;
			else if (flags&vAlignCenter)
				wy = wy + wh / 2 - (numlines * getTextHeight () - line_gap) / 2 - line_gap;
		}
		else
			wy -= line_gap;
	
		start = formatted;
		ix = 0;
		ix2 = 0;
	
		i1 = true;
		i2 = false;
		int nline = 0;
		for (;;)
		{
			// break on terminating zero
			if (0 == *start)
				break;
			int wordcnt = 0;
			int w = 0;
	
			while (*start == 32)
				start++;
	
			const char *end = start;
			const char *lastword = start;
			int lastword_w = 0;
			int numchars = 0;
	
			// calculate a line
			int prevchar = -1;
			int previncr = 0;
			for (;;)
			{
				if (0 == *end)
					break;
				else if ('\n' == *end)
				{
					end++;
					i1 = true;
					i2 = false;
					break;
				}
				else if (1 == *end)
				{
					end++;
					if (0 == *end)
						break;
					end++;
					continue;
				}
				else if (2 == *end)
				{
					end++;
					if (0 == *end)
						break;
					if (i1)
						w -= ix * mpGlyphs[0].advance[0];
					ix = *end-1;
					if (i1)
						w += ix * mpGlyphs[0].advance[0];
					end++;
					continue;
				}
				else if (3 == *end)
				{
					end++;
					if (0 == *end)
						break;
					if (i2)
						w -= ix2 * mpGlyphs[0].advance[0];
					ix2 = *end-1;
					if (i2)
						w += ix2 * mpGlyphs[0].advance[0];
					end++;
					continue;
				}
				else if (32 > *end)
				{
					end++;
					continue;
				}
	
				// get glyph
				uint32 charcode = 0;
				int incr = 0;
				if (*end > 0)
				{
					charcode = *end;
					incr = 1;
				}
				else
				{
					charcode = u8_nextchar (end, &incr);
					fprintf (stderr, "INFO: printing char %d, incr=%d\n", charcode, incr);
				}
				int gidx = getGlyphIdx (charcode);
				const glyph *g = &mpGlyphs[gidx];
				if (w + g->advance[0] >= ww && (flags & wordWrap))
				{
					if (0 == wordcnt)
					{
						// window is less than word width
					}
					else
					{
						end = lastword;
						w = lastword_w;
					}
	
					if (*start == 32)
					{
						start++;
					}
					if (prevchar == 32)
					{
						end -= previncr;
					}
	
					break;
				}
	
				if (charcode == 32)
				{
					lastword = end;
					lastword_w = w;
					wordcnt++;
				}
				if (0 == numchars)
				{
					if (i1)
						w += ix * mpGlyphs[0].advance[0];
					else if (i2)
						w += ix2 * mpGlyphs[0].advance[0];
				}
				w += g->advance[0];
				numchars++;
				prevchar = charcode;
				previncr = incr;
				end += incr;
			}
	
			//			assert (w <= ww);
	
			// ok, we have a line between a 'start' and 'end'
			// width is 'w' (in pixels)
	
			// do alignment (relative to page window)
			int x = wx;
			float spacing;
	
			// check if we've got a EOL
			const char *eol = end;
			while (*eol && *eol <= 32)
			{
				if (*eol < 32)
					break;
				eol++;
			}
			if (*eol > 32)
				eol = NULL;
	
			if (flags & hAlignLeft)
			{
				// do nothing, it's aligned already
				x = wx;
				spacing = 0;
			}
			else if (flags & hAlignRight)
			{
				// make it right
				x = wx + ww - w;
				spacing = 0;
			}
			else if (flags & hAlignCenter)
			{
				// make it center
				x = wx + ww / 2 - w / 2;
				spacing = 0;
			}
			else if (flags & hAlignStretch)
			{
				// stretch -- calc number of pixels required to fill a line (float value?)
	
				x = wx;
				if (eol)
					spacing = 0;
				else
					spacing = wordcnt > 1 ? (float) (ww - w) / (wordcnt - 1) : 0;
			}
			else	// same as 'AlignLeft'
			{
				x = wx;
				spacing = 0;
			}
	
			int y = /*g_engine->getViewport ()->getHeight () - getTextHeight () - */wy;
	
			// render a line
	
			if (false == vp_changed && (flags & useClipRect))
			{
				g_engine->getRenderer ()->setViewport (sx, sy, sw, sh);
	
				vp_changed = true;
			}
	
			matrix4 mIdent (true);
			g_engine->getEffectParms ()->setMatrix (effectParm_WorldMatrix, mIdent);
			g_engine->getEffectParms ()->setMatrix (effectParm_ViewMatrix, mIdent);
			g_engine->getEffectParms ()->setMatrix (effectParm_ProjMatrix, mProj);
	
			drawTextStringEx (start, end-start
				, (i1 ? mpGlyphs[0].advance[0] * ix : 0) + (i2 ? mpGlyphs[0].advance[0] * ix2 : 0) + x
				, y
				, color
				, spacing
				, !vp_changed);
	
			nline++;
			i1 = false;
			i2 = true;
	
			start = end;
			wy += getTextHeight ();
		}
	
		if (vp_changed)
		{
			g_engine->getRenderer ()->setViewport (0, 0, g_engine->getViewport ()->getWidth (), g_engine->getViewport ()->getHeight ());
		}
	}
	
	void	fontFT::drawTextStringFormatted (const char *text
											  , int sx, int sy, int sw, int sh	// display window
											  , int wx, int wy, int ww, int wh	// page window
											  , unsigned long color				// outer color
											  , unsigned long flags) const		// flags
	{
		if (NULL == text)
			return;
		// NOTE: it is guaranteed that formatted line will be smaller
		// NOTE: in size than original, due to the fact that any
		// NOTE: formatting sequence will be converted into lesser
		// NOTE: or exactly the same number of string characters
		// NOTE: e.g.: \c0 = 0x0101, \n = 0x0a, \i5 = 0x0206
		// NOTE: though, each indented block is being prepended
		// NOTE: with 4 words of indentation parameters
	
		char *formatted = preformat (text, wx, wy, ww, wh, flags);
		drawTextStringPreformatted (formatted, sx, sy, sw, sh, wx, wy, ww, wh, color, flags);
		freePreformatted (formatted);
	
	}
	
	void	fontFT::getTextExtent (const char *text, size_t sz, int &sx, int &sy) const
	{
		const char *c = text;
		sx = 0;
		while (c - text < (int)sz)
		{
			// get glyph
			int incr = 0;
			wchar_t wc = u8_nextchar (c, &incr);
			const glyph *g = &mpGlyphs[getGlyphIdx (wc)];
			sx = sx + g->advance[0];
			c+=incr;
		}
		sy = getTextHeight ();
	}
	
	}
