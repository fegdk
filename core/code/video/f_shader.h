/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_SHADER_H
#define __F_SHADER_H

#include "f_fragileobject.h"
#include "f_basetexture.h"
#include "f_parser.h"
#include "f_math.h"

namespace fe
{
	struct drawSurf_t;
	class FE_API shader : public fragileObject
	{
	protected:
		struct drawSurfBinding_t
		{
			handle pos, norm, color, tex0, tex1, tangent, binormal;
		};

		drawSurfBinding_t mDSBinding;

	public:
	
		// for now maps directly to opengl types
		enum streamParmType { BYTE, UNSIGNED_BYTE, SHORT, UNSIGNED_SHORT, INT, UNSIGNED_INT, FLOAT, BYTE2, BYTE3, BYTE4, DOUBLE, DOUBLE_EXT };
	
		shader (void);
//		shader (const char *shader);
		shader (charParser &parser, const char *name);
		virtual ~shader (void);
	
		virtual void save (const char *fname) const = 0;
		virtual void bind (void) const = 0;
		virtual void unbind (void) const = 0;
		
		virtual handle parmByName (const char *name) = 0;
		virtual void setFloat (handle parm, const float value) = 0;
		virtual void setFloat4 (handle parm, const float4 &value) = 0;
		virtual void setMatrix (handle parm, const matrix4 &value) = 0;
		virtual void setTexture (handle parm, const smartPtr <baseTexture> &tex) = 0;
		virtual void enableStream (handle stream) = 0;
		virtual void disableStream (handle stream) = 0;
		virtual void bindStreamData (handle stream, int numComponents, streamParmType parmType, int stride, void *data) = 0;
		void bindDrawSurf (drawSurf_t *surf);
		void unbindDrawSurf (drawSurf_t *surf);
	};
	
	typedef smartPtr <shader> shaderPtr;
	
}

#endif // __F_SHADER_H

