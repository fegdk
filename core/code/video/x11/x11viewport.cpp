/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#include "pch.h"
#include "config.h"

#include "x11viewport.h"
#include "x11viewportdata.h"
#include "x11renderer.h"
#include "f_helpers.h"
#include "f_engine.h"
#include "f_input.h"
#include "f_application.h"
#include "f_baserenderer.h"
#include "f_util.h"
#include <X11/keysym.h>
#include "utf8.h"

namespace fe
{

	// table to translate x11 keycodes into native fe codes
	int x11_to_key[] = {
		0, 0, 0, 0, 0, 0, 0, 0, Key_BackSpace, Key_Tab,
		0, 0, 0, Key_CR, 0, 0, 0, 0, 0, Key_Pause,
		Key_ScrollLock, 0, 0, 0, 0, 0, 0, Key_Esc, 0, 0,
		0, 0, Key_Space, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		Key_Home, Key_LeftArrow, Key_UpArrow, Key_RightArrow, Key_DownArrow, Key_PgUp, Key_PgDn, Key_End, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, Key_Kp_Ins,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, Key_Kp_NumLock,	0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, Key_Kp_Enter, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		Key_F1, Key_F2, Key_F3, Key_F4, Key_F5, Key_F6, Key_F7, Key_F8, Key_F9, Key_F10,
		Key_F11, Key_F12, Key_F13, Key_F14, Key_F15, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, Key_LShift, Key_RShift, Key_RCtrl, Key_LCtrl, Key_CapsLock,
		Key_LMeta, Key_RMeta, Key_LAlt, Key_RAlt, Key_LSuper, Key_RSuper, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, Key_Del,					// 255
	
		// keypad
		
		0, Key_Kp_DownArrow, 0, Key_Kp_PgDn,
		Key_Kp_LeftArrow, Key_Kp_5, Key_Kp_RightArrow,
		0, Key_Kp_UpArrow, Key_Kp_PgUp,
		Key_Kp_Del, Key_Kp_Slash, Key_Kp_Star,
		Key_Kp_Minus, Key_Kp_Plus, Key_Kp_Enter, Key_Kp_Equals,
	};

	// viewport data
	x11ViewportData::x11ViewportData (void)
	{
	}

	x11ViewportData::~x11ViewportData (void)
	{
	}

	uint16 x11ViewportData::xkeyToUcs4 (XKeyEvent *xkey)
	{
		Status state;
		char keybuf[7];
		x11RendererData *rd = checked_cast <x11Renderer*> (g_engine->getRenderer ())->getData ();
		if (Xutf8LookupString (rd->mXIC, xkey, keybuf, sizeof (keybuf), NULL, &state))
		{
			char *utf8 = (char*)keybuf;
			uint32 ucs4[2] = { 0, 0 };
			u8_toucs (ucs4, 2, utf8, 6);
			return ucs4[0];
		}
		return 0;
	}

	void x11ViewportData::handleEvent (XEvent *event)
	{
		x11Renderer *rend = checked_cast <x11Renderer*> (g_engine->getRenderer ());
		switch (event->type)
		{
		case MotionNotify:
			onMouseMove (event->xmotion.x, event->xmotion.y);
			break;
		case ButtonPress:
			onMouseBtnDown (event->xbutton.button);
			break;
		case ButtonRelease:
			onMouseBtnUp (event->xbutton.button);
			break;
		case KeyPress:
			{
				int keysym = XKeycodeToKeysym (rend->getData ()->mpDisplay, event->xkey.keycode, 0) & 0xff;
				int unicode = xkeyToUcs4 (&event->xkey);
				onKeyDown (keysym, unicode);
				fprintf (stderr, "INFO: keycode=%d, keysym=%d, unicode=%d\n", event->xkey.keycode, keysym, unicode);
			}
			break;
		case KeyRelease:
			onKeyUp (XKeycodeToKeysym (rend->getData ()->mpDisplay, event->xkey.keycode, 0) & 0xff);
			break;
		}
	}

	void x11ViewportData::onMouseMove (int x, int y)
	{
		smartPtr <inputDevice> mouse = g_engine->getInputDevice ();
		mouse->mouseMove (x, y);
		g_engine->getApplication ()->mouseMove (x, y);
	}

	void x11ViewportData::onMouseBtnDown (int x11_btn)
	{
		smartPtr <inputDevice> mouse = g_engine->getInputDevice ();

		switch (x11_btn)
		{
		case Button4:	// wheel up
			g_engine->getApplication ()->mouseWheel (1);
			break;
		case Button5:	// wheel down
			g_engine->getApplication ()->mouseWheel (-1);
			break;
		case Button1:	// left button
			mouse->lButtonDown ();
			g_engine->getApplication ()->mouseDown (Key_Mouse1);
			break;
		case Button3:	// right button
			mouse->rButtonDown ();
			g_engine->getApplication ()->mouseDown (Key_Mouse2);
			break;
		case Button2:	// middle button
			mouse->mButtonDown ();
			g_engine->getApplication ()->mouseDown (Key_Mouse3);
			break;
		}
	}

	void x11ViewportData::onMouseBtnUp (int x11_btn)
	{
		smartPtr <inputDevice> mouse = g_engine->getInputDevice ();

		switch (x11_btn)
		{
		case Button1:
			mouse->lButtonUp ();
			g_engine->getApplication ()->mouseUp (Key_Mouse1);
			break;
		case Button3:
			mouse->rButtonUp ();
			g_engine->getApplication ()->mouseUp (Key_Mouse2);
			break;
		case Button2:
			mouse->mButtonUp ();
			g_engine->getApplication ()->mouseUp (Key_Mouse3);
			break;
		}
	}

	static bool filterSpecialCases (int x11_key, int unicode)
	{
		// capslock sends this when we have
		// 'Option "XkbOptions" "grp:caps_toggle,grp_led:caps"' in xorg.conf
		if (x11_key == 8 && unicode == 0)
			return true;

		return false;
	}

	void x11ViewportData::onKeyDown (int x11_key, int unicode)
	{
		if (filterSpecialCases (x11_key, unicode))
		{
			return;
		}
		smartPtr <inputDevice> keyb = g_engine->getInputDevice ();
		// FIXME: we need to check keys such as alt, super, shift, etc
		if (unicode >= 0x20
			&& unicode != 0x7f // del should not be passed
			)
		{
			keyb->printableChar (unicode);
		}

		// remap x11 codes to fe codes
		if (x11_key >= (sizeof (x11_to_key) / sizeof (int)) || x11_to_key[x11_key] == 0)
		{
			if (x11_key < 127)
				keyb->keyDown (x11_key); // this should never happen
			else
			{
				fprintf (stderr, "WARNING: onKeyDown got unhandled x11 keycode %d\n", x11_key);
			}
		}
		else
		{
			keyb->keyDown (x11_to_key[x11_key]);
		}
	}

	void x11ViewportData::onKeyUp (int x11_key)
	{
		smartPtr <inputDevice> keyb = g_engine->getInputDevice ();
		// remap x11 codes to fe codes
		if (x11_key >= (sizeof (x11_to_key) / sizeof (int)) || x11_to_key[x11_key] == 0)
		{
			if (x11_key < 127)
				keyb->keyUp (x11_key);
			else
			{
				fprintf (stderr, "WARNING: onKeyUp got unhandled x11 keycode %d\n", x11_key);
			}
		}
		else
		{
			keyb->keyUp (x11_to_key[x11_key]);
		}
	}
	
	void x11ViewportData::init (const char *appname)
	{
	}


	// viewport base
	x11Viewport::x11Viewport (void)
	{
		mpViewportData = new x11ViewportData ();
		init (g_engine->getApplication ()->getTitle ());
	}

	x11Viewport::~x11Viewport (void)
	{
		if (mpViewportData)
		{
			delete mpViewportData;
			mpViewportData = NULL;
		}
	}

	void x11Viewport::init (const char *appName)
	{
		mpViewportData->init (appName);
	}

	// common window access
	void x11Viewport::setPosition (int x, int y)
	{
	}

	void x11Viewport::setSize (int w, int h)
	{
	}

	int x11Viewport::getLeft (void) const
	{
		return 0;
	}

	int x11Viewport::getTop (void) const
	{
		return 0;
	}

	int x11Viewport::getWidth (void) const
	{
		x11Renderer *rend = checked_cast <x11Renderer*> (g_engine->getRenderer ());
		x11RendererData *rd = rend->getData();
		return rd->mVidModes[rd->mLastMode].w;
	}

	int x11Viewport::getHeight (void) const
	{
		x11Renderer *rend = checked_cast <x11Renderer*> (g_engine->getRenderer ());
		x11RendererData *rd = rend->getData();
		return rd->mVidModes[rd->mLastMode].h;
	}

	void x11Viewport::close (ulong flags)
	{
		// emulate QUIT
		x11Renderer *rend = checked_cast <x11Renderer*> (g_engine->getRenderer ());
		x11RendererData *rd = rend->getData();
		XExposeEvent event;
		event.type = Expose;
		XSendEvent (rd->mpDisplay, rd->mWindow, false, 0, (XEvent*)&event);
	}

	void x11Viewport::mainLoop (void)
	{
		// run system loop
		bool mbRunning = true;
		x11Renderer *rend = checked_cast <x11Renderer*> (g_engine->getRenderer ());
		Display *display = rend->getData ()->mpDisplay;
		Atom wm_delete_window = XInternAtom (display, "WM_DELETE_WINDOW", false);
		while (mbRunning)
		{
			XEvent event;

			while (XPending (display))
			{
				XNextEvent(display, &event);
				if (event.type == Expose || (event.type == ClientMessage && event.xclient.format == 32 && event.xclient.data.l[0] == (long)wm_delete_window))
				{
					mbRunning = false;
					break;
				}

				mpViewportData->handleEvent (&event);
			}

			if (mbRunning)
			{
				g_engine->tick ();
			}
		}
/*		// run system loop
		bool mbRunning = true;
		Display *display = g_engine->getRenderer ().dynamicCast <x11Renderer> ()->getData ()->mpDisplay;
		while (mbRunning)
		{
			XEvent event;

			while (XPending (display))
			{
				XNextEvent(display, &event);
				mpViewportData->handleEvent (&event);
			}

			if (mbRunning)
				g_engine->tick ();
		}*/
	}

	x11ViewportData* x11Viewport::getViewportData (void)
	{
		return mpViewportData;
	}
	
}

