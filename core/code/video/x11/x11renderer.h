/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#ifndef __F_X11RENDERER_H
#define __F_X11RENDERER_H

#include "f_baseobject.h"
#include "f_math.h"
#include "f_types.h"
#include "x11rendererdata.h"
#include "f_glrenderer.h"

namespace fe
{

	class fragileObjectRegistry;

	class x11Renderer : public glRenderer
	{
	private:
		bool mbResetting;

	protected:
		x11RendererData mRendererData;

	public:
		x11Renderer (void);
		~x11Renderer (void);

		// baseRenderer overrides
		x11RendererData* getData (void);

		void init (const smartPtr <baseViewport> &viewport);
		bool isLost (void) const;
		void present (void);
		void resize (void);
		ulong numDisplayModes (ulong adapter, ulong device) const;
		cStr getDisplayModeDescr (ulong adapter, ulong device, ulong mode) const;
		ulong numDevices (ulong adapter) const;
		cStr getDeviceDescr (ulong adapter, ulong device) const;
		ulong numAdapters (void) const;
		cStr getAdapterDescr (ulong adapter) const;
		ulong getCurrentAdapter (void) const;
		ulong getCurrentDevice (ulong adapter) const;
		ulong getCurrentMode (ulong adapter, ulong device) const;
		void setWindow (baseViewport *vp);
	};

}

#endif // __F_X11RENDERER_H
