/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#ifndef __F_X11VIEWPORT_H
#define __F_X11VIEWPORT_H

#include "f_string.h"
#include "f_baseobject.h"
#include "f_baseviewport.h"

namespace fe
{
	
	class engine;
	
	class x11ViewportData;
	
	class x11Viewport : public baseViewport
	{
	
	private:
	
		x11ViewportData*		mpViewportData;
	
		void init (const char *appName);
	
	public:
	
		x11Viewport (void);
		~x11Viewport (void);
	
		// misc routines
	//	virtual void msgBox (const char *message, const char *caption, ulong flags);
	
		// common window access
		virtual void	setPosition (int x, int y);
		virtual void	setSize (int w, int h);
		virtual int		getLeft (void) const;
		virtual int		getTop (void) const;
		virtual int		getWidth (void) const;
		virtual int		getHeight (void) const;
		virtual void	close (ulong flags);

		virtual void mainLoop (void);

		virtual x11ViewportData*		getViewportData( void );
	};
	
}

#endif // __F_X11VIEWPORT_H
