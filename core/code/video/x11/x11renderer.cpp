/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#include "pch.h"
#include "config.h"

#include "f_types.h"
#include <GL/glew.h>
#include "x11renderer.h"
#include "f_baseviewport.h"
#include "f_math.h"
#include "x11viewport.h"
#include "x11viewportdata.h"
#include "f_engine.h"
#include "f_application.h"
#include "f_console.h"
#include "f_error.h"
#include "f_fragileobject.h"
#include "cvars.h"

namespace fe
{

	// x11RendererData
	x11RendererData::x11RendererData (void)
		: mpDisplay (NULL), mGLXContext (0), mWindow (0), mScreen (-1), mXIC (0), mXIM (0), mpModeLines (NULL), mNumModeLines (0), mNumVidModes (0)
	{
		resetDisplay ();
	}

	void x11RendererData::resetDisplay (void)
	{
		mpDisplay = XOpenDisplay (NULL);
		if (!mpDisplay)
			sys_error ("unable to open X display\n");
		mScreen = DefaultScreen (mpDisplay);

		// enum vidmodes
		if (!mNumVidModes)
		{
			XF86VidModeGetAllModeLines (mpDisplay, mScreen, &mNumModeLines, &mpModeLines);
			addModes (mpModeLines, mNumModeLines, 24);	// XXX how to query available bpp?
			if (mNumVidModes)
				sys_error ("no suitable video modes found!\n");
			mDesktopMode = 0;
		}

		// guess vidmode
		if (r_vidmode->ivalue == -1)
		{
			size_t i;
			int mode_800x600 = -1;
			int mode_640x480 = -1;
			for (i = 0; i < mNumVidModes; i++)
			{
				fprintf (stderr, "found vidmode %dx%dx%d\n", mVidModes[i].w, mVidModes[i].h, mVidModes[i].bpp);
				if (mVidModes[i].w == 800 && mVidModes[i].h == 600)
					mode_800x600 = i;
				if (mVidModes[i].w == 640 && mVidModes[i].h == 480)
					mode_640x480 = i;
			}
	
			r_vidmode->ivalue = 0;
			if (mode_800x600 != -1)
				r_vidmode->ivalue = mode_800x600;
			else if (mode_640x480 != -1)
				r_vidmode->ivalue = mode_640x480;
		}
	}

	void x11RendererData::reset (void)
	{
		free ();

		resetDisplay ();

		fprintf (stderr, "starting video..\n");
		mLastMode = r_vidmode->ivalue;

		mXIM = XOpenIM(mpDisplay, NULL, NULL, NULL);
		if (!mXIM)
			sys_error ("XOpenIM failed");
		mXIC = XCreateIC(mXIM, XNInputStyle, (XIMPreeditNothing | XIMStatusNothing), NULL);
		if (!mXIC)
			sys_error ("XCreateIC failed");
		int attribs[] = {
			GLX_RGBA,
			GLX_RED_SIZE, 8,
			GLX_GREEN_SIZE, 8,
			GLX_BLUE_SIZE, 8,
			GLX_ALPHA_SIZE, 8,
			GLX_DEPTH_SIZE, 24,
			GLX_STENCIL_SIZE, 8,
			GLX_DOUBLEBUFFER,
			None
		};
		XVisualInfo *visual = glXChooseVisual (mpDisplay, mScreen, attribs);

		mbWindowed = r_fullscreen->ivalue ? false : true;

		XSetWindowAttributes attr;
		uint32 mask;
		attr.background_pixel = 0;
		attr.border_pixel = 0;
		attr.colormap = XCreateColormap (mpDisplay, RootWindow (mpDisplay, mScreen), visual->visual, AllocNone);
		attr.event_mask = StructureNotifyMask | KeyPressMask | KeyReleaseMask | PointerMotionMask | ButtonPressMask | ButtonReleaseMask;
		if (!mbWindowed)
		{
			mask = CWBackPixel | CWColormap | CWOverrideRedirect | CWSaveUnder | CWBackingStore | CWEventMask;
			attr.override_redirect = True;
			attr.backing_store = NotUseful;
			attr.save_under = False;
		}
		else
		{
			mask = CWBackPixel | CWBorderPixel | CWColormap | CWEventMask;
		}
		fprintf (stderr, "creating window %dx%d\n", mVidModes[r_vidmode->ivalue].w, mVidModes[r_vidmode->ivalue].h);
		
		mWindow = XCreateWindow (mpDisplay, RootWindow (mpDisplay, mScreen), 0, 0, mVidModes[r_vidmode->ivalue].w, mVidModes[r_vidmode->ivalue].h, 0, visual->depth, InputOutput, visual->visual, mask, &attr);
		XMapWindow(mpDisplay, mWindow);

		Atom wm_delete_window = XInternAtom (mpDisplay, "WM_DELETE_WINDOW", false);
		XSetWMProtocols (mpDisplay, mWindow, &wm_delete_window, 1);

		XSync (mpDisplay, False);

		if (!mbWindowed)
		{
			XMoveWindow (mpDisplay, mWindow, 0, 0);
			XRaiseWindow (mpDisplay, mWindow);
			XFlush (mpDisplay);
			XGrabPointer (mpDisplay, mWindow, True, 0, GrabModeAsync, GrabModeAsync, mWindow, None, CurrentTime);
			XGrabKeyboard (mpDisplay, mWindow, True, GrabModeAsync, GrabModeAsync, CurrentTime);
			XF86VidModeSwitchToMode (mpDisplay, mScreen, mpModeLines[r_vidmode->ivalue]);
			XF86VidModeSetViewPort (mpDisplay, mScreen, 0, 0);
		}

		XFlush(mpDisplay);

		mGLXContext = glXCreateContext(mpDisplay, visual, NULL, True);
		if (!mGLXContext)
		{
			sys_error ("glXCreateContext failed");
		}

		glXMakeCurrent (mpDisplay, mWindow, mGLXContext);
		// fullscreen viewport
	    glViewport (0, 0, mVidModes[r_vidmode->ivalue].w, mVidModes[r_vidmode->ivalue].h);
	}

	void x11RendererData::addModes (XF86VidModeModeInfo **modes, int nummodes, int bpp)
	{
		if (!modes)
			return;

		for (int j = 0; j != nummodes; ++j)
		{
			XF86VidModeModeInfo *mode = modes[j];
			size_t i;
			for (i = 0; i < mNumVidModes; i++)
			{
				if (mVidModes[i].w == mode->hdisplay && mVidModes[i].h == mode->vdisplay && mVidModes[i].bpp == bpp)
					break;
			}
	
			if (i == mNumVidModes)
			{
				if (i == X11_MAX_VIDMODES)
					sys_error ("too many vidmodes for x11 driver (limit of %d modes exceeded)", X11_MAX_VIDMODES);
				vidMode_t *vmode = &mVidModes[mNumVidModes++];
				vmode->w = mode->hdisplay;
				vmode->h = mode->vdisplay;
				vmode->bpp = bpp;
			}		
		}
	}

	void x11RendererData::free (void)
	{
		if (!mbWindowed && mpModeLines)
		{
			XF86VidModeSwitchToMode (mpDisplay, mScreen, mpModeLines[0]);
		}

		if (mGLXContext && mpDisplay && mWindow)
		{
			glXDestroyContext (mpDisplay, mGLXContext);
			mGLXContext = NULL;
		}

		if (mXIC)
		{
			fprintf (stderr, "XDestroyIC...\n");
			XDestroyIC (mXIC);
			mXIC = NULL;
		}
		if (mXIM)
		{
			fprintf (stderr, "XCloseIM...\n");
			XCloseIM (mXIM);
			mXIM = NULL;
		}
		if (mWindow)
		{
			fprintf (stderr, "XDestroyWindow...\n");
			XDestroyWindow (mpDisplay, mWindow);
			mWindow = 0;
		}
		if (mpDisplay)
		{
			fprintf (stderr, "XCloseDisplay...\n");
			XCloseDisplay (mpDisplay);
			mpDisplay = NULL;
		}
	}

	x11RendererData::~x11RendererData (void)
	{
		free ();
	}

	size_t x11RendererData::numVidModes (void) const
	{
		return mNumVidModes;
	}

	const x11RendererData::vidMode_t& x11RendererData::getVidModeDescr (int mode) const
	{
		return mVidModes[mode];
	}


	// x11Renderer
	x11Renderer::x11Renderer (void)
	{
		mbResetting = false;
	    // Setup our viewport
		size_t curmode = r_vidmode->ivalue;
		const x11RendererData::vidMode_t &descr = mRendererData.getVidModeDescr ( (int)curmode);

		g_engine->getConsole ()->msg ("setting render-related cvars...");

		// set cvars
		r_fullscreen->ivalue = 0;

		glRenderer::init ();

		g_engine->getConsole ()->msg ("x11_opengl renderer initialization done.");
	}

	x11Renderer::~x11Renderer (void)
	{
	}

	void x11Renderer::present (void)
	{
		glXSwapBuffers (mRendererData.mpDisplay, mRendererData.mWindow);
	}
	
	bool x11Renderer::isLost (void) const
	{
		// for now - assume opengl buffers are restored automatically (though remember it is not really true)
		return false;
	}

	void x11Renderer::resize (void)
	{
		// set modes to rendererdata
		mRendererData.reset ();
		glRenderer::resize ();
	}

	ulong x11Renderer::numDisplayModes (ulong adapter, ulong device) const
	{
		return (ulong)mRendererData.numVidModes ();
	}

	cStr x11Renderer::getDisplayModeDescr (ulong adapter, ulong device, ulong mode) const
	{
		const x11RendererData::vidMode_t &vmode = mRendererData.getVidModeDescr (mode);
		cStr s;
		s.printf ("%d x %d x %d", vmode.w, vmode.h, vmode.bpp);
		return s;
	}

	ulong x11Renderer::numDevices (ulong adapter) const
	{
		return 1;
	}

	cStr x11Renderer::getDeviceDescr (ulong adapter, ulong device) const
	{
		return "X11-opengl";
	}

	ulong x11Renderer::numAdapters (void) const
	{
		return 1;
	}

	cStr x11Renderer::getAdapterDescr (ulong adapter) const
	{
		return "default adapter";
	}

	ulong x11Renderer::getCurrentAdapter (void) const
	{
		return 0;
	}

	ulong x11Renderer::getCurrentDevice (ulong adapter) const
	{
		return 0;
	}

	ulong x11Renderer::getCurrentMode (ulong adapter, ulong device) const
	{
		return 0;
	}

	void x11Renderer::setWindow (baseViewport *vp)
	{
		// setting external window is not supported for opengl
		// FIXME: should this method be completely abandoned?
	}
	
	x11RendererData*		x11Renderer::getData (void)
	{
		return &mRendererData;
	}	

}

