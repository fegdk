/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#ifndef __F_X11VIEWPORTDATA_H
#define __F_X11VIEWPORTDATA_H

#include <X11/Xlib.h>

namespace fe
{

	class x11ViewportData : public baseObject
	{
	private:
		uint16 xkeyToUcs4 (XKeyEvent *xkey);

		void onMouseMove (int x, int y);
		
		void onMouseBtnDown (int x11_btn);
		void onMouseBtnUp (int x11_btn);
	
		void onKeyDown (int x11_key, int unicode);
		void onKeyUp (int x11_key);
	
	public:
	
		x11ViewportData (void);
		~x11ViewportData (void);
	
		void init (const char *appName);
	
		void handleEvent (XEvent *event);
	};

}

#endif // __F_X11VIEWPORTDATA_H

