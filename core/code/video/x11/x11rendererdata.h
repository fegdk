/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#ifndef __F_X11RENDERERDATA_H
#define __F_X11RENDERERDATA_H

#include <X11/Xlib.h>
#include <GL/glx.h>
#include <X11/extensions/xf86vmode.h>
#ifdef HAVE_LIBCG
#include <Cg/cg.h>
#endif

namespace fe
{

	const int X11_MAX_VIDMODES = 256;
	
	class x11RendererData : public baseObject
	{
		friend class x11Renderer;
		friend class x11Viewport;
		friend class x11ViewportData;
		friend class engine;

	protected:
		int mLastMode;
		struct vidMode_t
		{
			int w, h, bpp;
			XF86VidModeModeLine *line;
		};
		const vidMode_t& getVidModeDescr (int mode) const;
		size_t numVidModes (void) const;

		void reset (void);
		void free (void);

		void resetDisplay (void);

	private:
		bool mbWindowed;
		vidMode_t mVidModes[X11_MAX_VIDMODES];
		int mNumVidModes;
		Display* mpDisplay;
		GLXContext mGLXContext;
		Window mWindow;
		XIM mXIM;
		XIC mXIC;
		int mScreen;
		int mDesktopMode;
		XF86VidModeModeInfo **mpModeLines;
		int mNumModeLines;

		void addModes (XF86VidModeModeInfo **modes, int numModes, int bpp);

	public:
		x11RendererData (void);
		~x11RendererData (void);

	/*	const displayMode&			getDisplayModeDescr( ulong adapter, ulong device, ulong mode ) const;
		const x11RenderDeviceInfo&		getDeviceDescr( ulong adapter, ulong device ) const;
		const displayAdapterInfo&		getAdapterDescr( ulong adapter ) const;

		displayMode&					getDisplayModeDescr( ulong adapter, ulong device, ulong mode );
		x11RenderDeviceInfo&				getDeviceDescr( ulong adapter, ulong device );
		displayAdapterInfo&			getAdapterDescr( ulong adapter );*/
	};

}

#endif // __F_X11RENDERERDATA_H
