/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

// GENERAL INFO AND SYNTAX
// -----------------------
//
// material <name> {
//
//   /* these maps may be used with internal renderer, as well as custom effects */
//
//   basemap <mapname>
//   normalmap <mapname>
//   specularmap <mapname>
//   basecubemap <mapname>
//   normalcubemap <mapname>
//   specularcubemap <mapname>
//
//   /* texcoord modifiers and generators for std maps are applied to following inputs:
//   	base_tcmod
//      normal_tcmod
//      specular_tcmod */
//
//   /* texcoord generators for std maps are aplied to predefined inputs:
//      base_texgen
//      normal_texgen
//      specular_texgen */
//
//   /* rgba/xyzw generators aer applied to predefined inputs:
//      material_diffuse
//      material_ambient
//      material_specular */
//   
//   /* effectfile and related stuff */
//   
//   effectfile <fx_file_name>
//
//   /* maps */
//   map <inputname> <mapname>
//   cubemap <inputname> <mapname>
//
//   /* generators */
//   float <input_name> <generator_def>
//   float3 <input_name> <generator_def>
//   float4 <input_name> <generator_def>
//
//   /* modifiers */
//   float4x4 <input_name> <modifier_def>
//
// }

// EFFECT-FILES usage
// ------------------
//
// material may reference effectfile
// 
// effect file may contain some techniques which are identified and used by the engine
//
// they are identified by names, constructed as NAME****,
// where NAME is one of reserved ids, and **** is whatever effect writer wants.
//
// engine (rendering queue) looks for specific techniques while it renders scene.
//
// if technique is not identified - it is rendered after all identified techniques
//
// if some of reserved techniques are not included - they are not rendered
//
// list of predefined technique names (always UPPERCASED):
//
// AMBIENT -- used to draw ambient pass and fill depth-buffer
// LIGHTING -- used to draw lighting for a single light source (additive). should not touch depth buffer
//

// MATERIALS WITHOUT EFFECT-FILES
// ------------------------------
//
// engine can render materials without effect files
// in such cases renderstates, shaders and other stuff
// are set by the engine automatically, based on material flags
// it might be implemented as a fixed set of predefined effect files
//



#ifndef __F_MATERIAL_H
#define __F_MATERIAL_H

#include "f_baseobject.h"
#include "f_helpers.h"
#include "f_math.h"
#include "f_texture.h"
#include "f_effect.h"
#include "f_parser.h"
#include "f_mathexpression.h"

namespace fe
{

	class mtlMat4x4 : public baseObject
	{
	public:
		virtual ~mtlMat4x4 (void) {}
		virtual matrix4	get (void) = 0;
	};
	
	typedef smartPtr <mtlMat4x4>	mtlMat4x4Ptr;
	
	class mat4x4Scroll : public mtlMat4x4
	{
	private:
		mathExpressionPtr	mExprU;
		mathExpressionPtr	mExprV;
	public:
		mat4x4Scroll (const mathExpressionPtr &u, const mathExpressionPtr &v);
		virtual matrix4	get (void);
	};
	
	class mat4x4Scale : public mtlMat4x4
	{
	private:
		mathExpressionPtr	mExprU;
		mathExpressionPtr	mExprV;
	public:
		mat4x4Scale (const mathExpressionPtr &u, const mathExpressionPtr &v);
		virtual matrix4	get (void);
	};
	
	class mat4x4Rotate : public mtlMat4x4
	{
	private:
		mathExpressionPtr mExprAngle;
	public:
		mat4x4Rotate (const mathExpressionPtr &angle);
		virtual matrix4	get (void);
	};
	
	class mtlFloat4Base : public baseObject
	{
	public:
		virtual float4	get (void) = 0;
	};
	
	typedef smartPtr <mtlFloat4Base>	mtlFloat4BasePtr;
	
	class mtlFloat3 : public mtlFloat4Base
	{
	private:
		mathExpressionPtr mExprX;
		mathExpressionPtr mExprY;
		mathExpressionPtr mExprZ;
	public:
		mtlFloat3 (const mathExpressionPtr &x, const mathExpressionPtr &y, const mathExpressionPtr &z);
		virtual float4	get (void);
	};
	
	class mtlFloat4 : public mtlFloat4Base
	{
	private:
		mathExpressionPtr mExpr;
	public:
		mtlFloat4 (const mathExpressionPtr &e);
		virtual float4	get (void);
	};

	struct mtlMapParms_t
	{
		uint32 surfaceFlags;
		uint32 contents;
		// map compiler params
		cStr editorimage;
		cStr lightimage;
		int surfacelight;
		int lightSubdivide;
		int lightmapSampleSize;
		bool forceTraceLight;
		bool forceVLight;
		bool patchShadows;
		bool vertexShadows;
		bool noVertexShadows;
		bool forceSunLight;
		float vertexScale;
		bool notjunc;
		bool globalTexture;
		float backsplashFraction;
		float backsplashDistance;
		cStr backShader;
		cStr flareShader;
		vector3 sunLight;
		vector3 sunDirection;
		float subdivisions;
		bool twoSided;
	};
	
	const int FXREF_MAX_TEXTURES = 8;
	const int FXREF_MAX_FLOAT4X4S = 8;
	const int FXREF_MAX_FLOAT4S = 8;
	const int MTL_MAX_SURFACE_PARMS = 20;

	class material : public baseObject
	{
	public:
	
		enum stdmap {map, normalmap, specularmap, lightmap, lastmap};
	
		struct effectRef
		{
			effectRef (void)
			{
				fx = NULL;
				ambient = -1;
				pointlight = -1;
				directlight = -1;
				spotlight = -1;
				numtextures = numfloat4x4s = numfloat4s = 0;
			}
	
			void clean (void)
			{
				fx = NULL;
				numtextures = numfloat4x4s = numfloat4s = 0;
			}
	
			smartPtr< effect >			fx;
			int								ambient;
			int								pointlight;
			int								directlight;
			int								spotlight;

			int textures[FXREF_MAX_TEXTURES];
			int numtextures;
			int float4x4s[FXREF_MAX_FLOAT4X4S];
			int numfloat4x4s;
			int float4s[FXREF_MAX_FLOAT4S];
			int numfloat4s;
		};
	
	private:
	
		struct texInput
		{
			cStr name;
			smartPtr<baseTexture>	texture;
		};
	
		struct float4x4Input
		{
			cStr name;
			smartPtr< mtlMat4x4 > value;
		};
	
		struct float4Input
		{
			cStr name;
			smartPtr< mtlFloat4Base > value;
		};
	
		texInput mTextures[FXREF_MAX_TEXTURES];
		int mNumTextures;
		float4x4Input mFloat4x4s[FXREF_MAX_FLOAT4X4S];
		int mNumFloat4x4s;
		float4Input mFloat4s[FXREF_MAX_FLOAT4S];
		int mNumFloat4s;
		effectRef*								mpEffect;
		cStr mSurfaceParms[MTL_MAX_SURFACE_PARMS];
		int mNumSurfaceParms;
		mtlMapParms_t mMapParms;

		void 	load (charParser &p);
		void	load (void);
		baseTexturePtr loadMap (const char *name, charParser &p);
		baseTexturePtr loadStdMap (charParser &p);
		void	checkEffect (void);
		void	initEffectRef (void);
		void	setupAfterLoading (void);
		// base-pass stuff
		baseTexturePtr mStdMaps[lastmap];
		int mLightMapParm;	// -1 if none
		uint32 mMetaShaderFlags;
	
	public:
	
	
		~material (void);
		material (const char *name);
		material (charParser &parser, const char *name);
		effectPtr			getEffect (void) const;
		void				setupEffectInputs ();
		baseTexturePtr	getTexture (int index) const;
	
		effectRef*		getEffectRef (void) const;
		baseTexturePtr	getStdMap (stdmap idx = map) const;

		int getNumSurfaceParms (void) const;
		const char *getSurfaceParm (int parm) const;
		const mtlMapParms_t *getMapParms (void) const;
	};
	
	typedef smartPtr <material> materialPtr;
	
}

#endif // __F_MATERIAL_H

