#include "pch.h"
#include "metashader.h"
#include "f_parser.h"
#include "f_engine.h"
#include "f_resourcemgr.h"
#include "f_error.h"

namespace fe
{
	metaShader::metaShader (const char *fname)
	{
		charParser p(g_engine->getFileSystem (), fname);
		for (;;)
		{
			p.errMode (0);
			if (p.getToken ())
				break;
			p.errMode (1);
			cStr effectName = p.token ();
			uint32 flags = 0;
			p.matchToken ("{");
			for (;;)
			{
				p.getToken ();
				if (!p.cmptoken ("}"))
				{
					effectPtr fx = g_engine->getResourceMgr ()->createEffect (effectName);
					mEffectMap[flags] = fx;
					fprintf (stderr, "INFO: added %s for flags 0x%X\n", effectName.c_str (), flags);
					break;
				}
				else if (!p.cmptoken ("uses"))
				{
					p.getToken ();
					if (!p.cmptoken ("map"))
						flags |= MS_USE_MAP;
					else if (!p.cmptoken ("normalmap"))
						flags |= MS_USE_NORMALMAP;
					else if (!p.cmptoken ("specularmap"))
						flags |= MS_USE_SPECULARMAP;
					else if (!p.cmptoken ("lightmap"))
						flags |= MS_USE_LIGHTMAP;
					else if (!p.cmptoken ("tcmod"))
						flags |= MS_USE_TCMOD;
					else
						p.generalSyntaxError ();
				}
				else if (!p.cmptoken ("defines"))
				{
					p.getToken ();
					if (!p.cmptoken ("specpower"))
					{}
					else if (!p.cmptoken ("nolight"))
						flags |= MS_NOLIGHT;
					else if (!p.cmptoken ("nospecular"))
						flags |= MS_NOSPECULAR;
					else
						p.generalSyntaxError ();
				}
				else
					p.generalSyntaxError ();
			}
		}
	}
	
	metaShader::~metaShader (void)
	{
	}

	smartPtr <effect> metaShader::getEffectForFlags (uint32 flags) const
	{
		effectMap_t::const_iterator it = mEffectMap.find (flags);
		if (it != mEffectMap.end ())
			return (*it).second;
		return NULL;
	}
}

