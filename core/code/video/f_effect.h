/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_EFFECT_H
#define __F_EFFECT_H

#include "f_parser.h"
#include "f_effectparms.h"
#include "f_basetexture.h"

namespace fe
{
	
	// replacement for ID3DXEffect
	// should support d3d8, d3d9, opengl with vs/ps and nvidia's cg
	// global interface used on all platforms
	
	// effect, technique, variable -- portable classes, should all be here
	// pass will be platform specific, all loading must be done there
	
	enum effectVarType
	{
		svtUninitialized,
		svtDword,
		svtFloat,
		svtFloat4,
		svtMatrix,
		svtTexture,
		svtString,
	};
	
	class baseTexture;
	class effect;
	class effectTechnique;
	class effectPass;
	class shader;
	
	const int FX_VAR_SIZE = sizeof(float)*16;
	const int FX_VAR_NAME_SIZE = 32;
	class effectVar
	{
	friend class effect;
	
	protected:
		int hashNext;
		char name[FX_VAR_NAME_SIZE];
		effectVarType		mType;
		uchar				mValue[FX_VAR_SIZE];
	
		inline void init (void)
		{
			name[0] = 0;
			hashNext = -1;
			mType = svtUninitialized;
		}
	
	public:
	
		effectVar () { init ();}
		effectVar (const effectVar &v);
		~effectVar (void);
	
		inline void set (effectVarType type, void *value, size_t sz)
		{
			assert (sz <= FX_VAR_SIZE);
			mType = type;
			memcpy (mValue, value, sz);
		}

		// slow but simple ctor
		effectVar (effectVarType type, const uchar *value);
	
		// separate ctors for each type
		effectVar (ulong value);
		effectVar (float value);
		effectVar (const float4 &value);
		effectVar (const matrix4 &value);
		effectVar (baseTexture *value);
		effectVar (const cStr &value);
	
		operator baseTexture * ()
		{
			assert (mType = svtTexture);
			return * ((baseTexture **)mValue);
		}
		operator char * ()
		{
			assert (mType == svtString);
			return (char *)mValue;
		}
	
		operator matrix4 * ()
		{
			assert (mType == svtMatrix);
			return (matrix4 *)mValue;
		}

		operator float4 * ()
		{
			assert (mType == svtFloat4);
			return (float4 *)mValue;
		}
	
		effectVarType type (void) { return mType; }
		void *value (void) { return mValue; }
	
	};

	const int MAX_TECH_PASSES = 5;
	
	class FE_API effectTechnique : public baseObject
	{
	
	friend class effect;
	
	private:
	
		// passes
		smartPtr<effectPass> mPasses[MAX_TECH_PASSES];
		int mNumPasses;
	
		// misc
		void validateVarName (const cStr &v) {}
	
		bool									mbValid;
	
	protected:
	
		// returns true if technique is valid
		bool load (charParser &p, effect &s);
		void pass (int nPass, effect &s, uint32 flags);
		void finalizePass (int nPass);
	
	public:
	
		// ctor & dtor
		effectTechnique (void);
		~effectTechnique (void);
	
		int numPasses (void) const;
		bool	isValid (void) { return mbValid; }
		smartPtr <effectPass>	getPass (int pass) const;
	
	};

	enum
	{
		PASS_NO_DL = 0x00000001,
		PASS_NO_VP = 0x00000002,
		PASS_NO_FP = 0x00000004,
	};

	const int MAX_FX_VARS = 16;
	const int FX_VARS_HASH_SIZE = 16;
	const int MAX_FX_SEMANT_BINDINGS = 16;
	const int MAX_FX_TECHNIQUES = 8;

	class FE_API effect : public baseObject
	{
		friend class effectPass;
	private:
	
		bool mbLost;
		effectVar mVars[MAX_FX_VARS];
		int mNumVars;
		int mVarsHash[FX_VARS_HASH_SIZE];
		
		enum { bind_undef, bind_vector, bind_matrix, bind_texture };
		struct semantBinding_t
		{
			int type;
			effectParm_t parm;
			int var;
		};
		semantBinding_t		mSemantBindings[MAX_FX_SEMANT_BINDINGS];
		int mNumSemantBindings;
		
		// techniques
		smartPtr<effectTechnique> mTechniques[MAX_FX_TECHNIQUES];
		int mNumTechniques;
		
		// current technique
		int									mCurrentTechnique;
		int									mBestTechnique;
		
		// parser
		void	loadDword (charParser &p);
		void	loadFloat (charParser &p);
		void	loadVector (charParser &p);
		void	loadMatrix (charParser &p);
		void	loadTexture (charParser &p);
		void	loadString (charParser &p);
		bool	loadTechnique (charParser &p);
	
		// misc
		void	validateVarName (const cStr &v) const {}
		
		// fragileObject
		void loose (void);
		void restore (void);
	public:
		effect (void) {}
		effect (const char *name);
		effect (charParser &parser, const char *name);
		~effect (void);
	
		int varIndexByName (const char *name) const;
		int techniqueIndexByName (const char *name) const;
		int findTechnique (const char *name_start) const;
		void setTechnique (int nTechnique);
		void setBestTechnique (void);
		void setTechnique (const char *techniqueName);
		int	begin (void);
		void end (void);
		int	 passIndexByName (const char *passName) const;
		void pass (int nPass, uint32 flags = 0);
		void finalizePass (int nPass);
	
		void setMatrix (int nParm, const matrix4 &m);
		matrix4& getMatrix (int nParm);
		const matrix4&	getMatrix (int nParm) const;
		float4&	getFloat4 (int nParm);
		const float4& getFloat4 (int nParm) const;
		void setFloat4 (int nParm, const float4 &m);
		void setTexture (int nParm, const baseTexturePtr &t);
	
		// pass data access
		smartPtr <shader> getVertexProgram (int npass) const;
		smartPtr <shader> getFragmentProgram (int npass) const;
	
		smartPtr <effectTechnique>	getCurrentTechnique (void) const;
		
	};
	
	typedef smartPtr <effect> effectPtr;
	
}

#endif // __F_EFFECT_H

