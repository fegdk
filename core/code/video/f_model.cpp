/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_model.h"
#include "f_engine.h"
#include "f_error.h"
#include "f_resourcemgr.h"
#include "f_basetexture.h"
#include "f_material.h"
#include "f_modeldata.h"
#include "f_lightsource.h"

namespace fe
{
	
	modelMtlList::modelMtlList (void) {
		list = NULL;
		cnt = 0;
	}
	modelMtlList::~modelMtlList (void) {
		if (list) {
			delete[] list;
			list = NULL;
		}
	}

	void modelMtlList::load (file *f)
	{
		uint32 sp;
		f->readUINT32 (&sp, 1);
		if (!sp)
			return;
		list = new materialPtr[sp];
		cnt = sp;
		for (uint32 i = 0; i < sp; i++)
		{
			char str[256];
			uchar ln;
			f->read (&ln, 1);
			f->read (str, ln);
			str[ln]=0;
			materialPtr m = g_engine->getResourceMgr ()->createMaterial (str);
			list[i] = m;
		}
	}

	model::model (void)
	{
/*		mbLightmaps = 0;
		mbTangents = 0;
		mbColors = 0;
		
		memset (&mTris, 0, sizeof (mTris));*/
	}

	sceneObject* model::load (const char *fname)
	{
		mbValid = true;
		if (!fname)
		{
			mbValid = false;
			return NULL;
		}
		
		mName = fname;
	
	#if 0
		bool noscript = false;
		// check if we got bmdl
		if (strstr (fname, ".bmdl"))
		{
			// yes, just load it
			noscript = true;
		}
		else
		{
			// no, try loading script
			try {
				charParser p(g_engine->getFileSystem (), fname);
				// file must contain only model def, and nothing more (no name, nothing)
				return load (p);
			}
			catch (parserException e)
			{
				if (e == parserException::FILE_NOT_FOUND)
					noscript = true;
				else if (e != parserException::END_OF_FILE)
				{
					fprintf (stderr, "ERROR: failed to load script for model %s,\nerrors: %s", fname, cStr (e).c_str ());
					mbValid = false;
					return NULL;
				}
			}
		}
		if (noscript)
	#endif
		{
			file *f = g_engine->getFileSystem ()->openFile (fname, FS_MODE_READ);
			modelMtlList mtls;
			return load (f, mtls);
		}
	}
	
	#if 0
	model::model (charParser &parser, const char *fname, model *parent)
	{
		mbLightmaps = 0;
		mbTangents = 0;
		mbColors = 0;
		memset (&mTris, 0, sizeof (mTris));
		try {
			charParser p(g_engine->getFileSystem (), fname);
			// file must contain only model def, and nothing more (no name, nothing)
			load (p);
		}
		catch (parserException e)
		{
			if (e != parserException::END_OF_FILE)
				throw genericError ("failed to load script for model %s,\nerrors: %s",
						fname, cStr (e).c_str ());
		}
	
	}
	model::model (const char *name, model *parent, const matrix4 &mtx, const std::vector <drawVertex_t> &verts, std::vector <ushort> &inds, const std::vector <modelSubset_t> &subs, const std::vector <materialPtr> &mtls)
	{
		mName = name;
		mbLightmaps = 0;
		mbTangents = 0;
		mbColors = 0;
		memset (&mTris, 0, sizeof (mTris));
		mRelativeMatrix = mtx;
		mWorldMatrix = mpParentObject ? mRelativeMatrix * mpParentObject->getWorldMatrix() : mRelativeMatrix;

		// init surfaces
		mSurfaces = subs;
		size_t nsubs = subs.size ();
		for (size_t s = 0; s < nsubs; s++)
		{
			mSurfaces[s].tris = &mTris;
		}

		// init tris
		mTris.numVerts = verts.size ();
		mTris.verts = new drawVertex_t[verts.size ()];
		memcpy (mTris.verts, &verts.front (), verts.size () * sizeof (drawVertex_t));
		mTris.numIndexes = inds.size ();
		mTris.indexes = new ushort[inds.size ()];
		memcpy (mTris.indexes, &inds.front (), inds.size () * sizeof (ushort));

		init ();
	}
	#endif

	
	void model::init (void)
	{
	#if 0	// FIXME: all this should be in mesh data!!
		// build faceplanes
		mTris.facePlanes = new plane[mTris.numIndexes/3];
		for (int i = 0; i < mTris.numIndexes/3; i++)
		{
			mTris.facePlanes[i].fromPoints (
					mTris.verts[mTris.indexes[i*3+0]].pos,
					mTris.verts[mTris.indexes[i*3+1]].pos,
					mTris.verts[mTris.indexes[i*3+2]].pos);
		}
	
		// build silIndexes
		// FIXME: move to fgp
		mTris.silIndexes = new ushort[mTris.numIndexes];
		for (int i = 0; i < mTris.numIndexes; i++)
		{
			vector3 *v = &mTris.verts[mTris.indexes[i]].pos;
			size_t j;
			for (j = 0; j < mTris.numVerts; j++)
			{
				if (*v == mTris.verts[j].pos)
				{
					mTris.silIndexes[i] = j;
					break;
				}
			}
			assert (j != mTris.numVerts);
		}
		
		// silEdges
		// vertex indexes are from silIndexes
		// TODO: move to fgp
		mTris.numSilEdges = 0;
		mTris.silEdges = new silEdge_t[mTris.numIndexes/3 * 2];
		for (int i = 0; i < mTris.numIndexes/3; i++)
		{
			for (int k = 0; k < 3; k++)
			{
				int v1 = mTris.silIndexes[i * 3 + k];
				int v2 = mTris.silIndexes[i * 3 + ((k+1)%3)];
				
				for (int j = 0; j < mTris.numIndexes/3; j++)
				{
					if (i == j)
						continue;
					for (int n = 0; n < 3; n++)
					{
						int vv1 = mTris.silIndexes[j * 3 + n];
						int vv2 = mTris.silIndexes[j * 3 + ((n+1)%3)];
						if (v1 == vv2 && v2 == vv1)
						{
							// check for dupe
							int m;
							for (m = 0; m < mTris.numSilEdges; m++)
							{
								if ((mTris.silEdges[m].v1 == v1 && mTris.silEdges[m].v2 == v2)
										|| (mTris.silEdges[m].v1 == v2 && mTris.silEdges[m].v2 == v1))
								{
									break;
								}
							}
							if (m == mTris.numSilEdges)
							{
								// add edge
								mTris.silEdges[mTris.numSilEdges].f1 = i;
								mTris.silEdges[mTris.numSilEdges].f2 = j;
								mTris.silEdges[mTris.numSilEdges].v1 = v1;
								mTris.silEdges[mTris.numSilEdges].v2 = v2;
								mTris.numSilEdges++;
							}
						}
					}
				}
			}
		}
		
	
		// each edge forms a quad, which is 2 triangles, which is 6 indexes
		// in case we're using vertexshader - we will index normal vertex array, and pass
		// extrusion as one of vertex attributes
		// in case we're not using vertex shader - we'll make dynamic vertex array,
		// which will contain entire (extruded) shadow volume
		mTris.shadowVolumeIndexes = new ushort[mTris.numIndexes/3 * 2 * 6 + mTris.numIndexes];
		mTris.shadowVerts = new shadowVertex_t[mTris.numVerts * 2];
	
		// verts are going in interleaved order, i.e. v0: normal, v1: extruded (to minimize cache misses)
		for (int i = 0; i < mTris.numVerts; i++)
		{
			mTris.shadowVerts[i].xyzw = vector4 (mTris.verts[i].pos.x, mTris.verts[i].pos.y, mTris.verts[i].pos.z, 1);
		}
		
		mbTangents = true;
	
		calcOBB (false);
	
		// mpData = new modelData (this);

		// fill meshData from model
#endif
	}
	
	model::~model (void)
	{
	#if 0
		if (mTris.verts)
			delete[] mTris.verts;
		if (mTris.indexes)
			delete[] mTris.indexes;
		if (mTris.silIndexes)
			delete[] mTris.silIndexes;
		if (mTris.facePlanes)
			delete[] mTris.facePlanes;
		if (mTris.silEdges)
			delete[] mTris.silEdges;
//		if (mTris.mtlIndexes)
//			delete[] mTris.mtlIndexes;
		if (mTris.dupVerts)
			delete[] mTris.dupVerts;
		if (mTris.shadowVolumeIndexes)
			delete[] mTris.shadowVolumeIndexes;
		if (mTris.shadowVerts)
			delete[] mTris.shadowVerts;
	#endif
		if (mpMeshData)
		{
			if (mpMeshData->vbo)
			{
				glDeleteBuffersARB (1, &mpMeshData->vbo);
				mpMeshData->vbo = 0;
			}
			if (mpMeshData->ibo)
			{
				glDeleteBuffersARB (1, &mpMeshData->ibo);
				mpMeshData->ibo = 0;
			}
			if (mpMeshData->verts)
			{
				delete[] mpMeshData->verts;
				mpMeshData->verts = NULL;
			}
			if (mpMeshData->inds)
			{
				delete[] mpMeshData->inds;
				mpMeshData->inds = NULL;
			}
		}
	}
	
	void model::loadMesh (file *f, int majorver, int minorver, modelMtlList &mtls, int numSubsets, renderable *rend)
	{
		drawSurf_t *ds = NULL;
		if (numSubsets)
		{
			mMeshDataCount = numSubsets;
			mpMeshData = new meshData_t[mMeshDataCount];
			ds = new drawSurf_t[mMeshDataCount];
			memset (mpMeshData, 0, sizeof (meshData_t) * mMeshDataCount);
			memset (ds, 0, sizeof (drawSurf_t) * mMeshDataCount);
			rend->setDrawSurfs (ds, mMeshDataCount);
			for (int i = 0; i < mMeshDataCount; i++)
			{
				// meshData/geomSource stuff (shared)
				f->readUINT32 (&mpMeshData[i].firstVertex, 1);
				f->readUINT32 (&mpMeshData[i].firstIndex, 1);
				mpMeshData[i].firstIndex *= 3;
				f->readUINT32 (&mpMeshData[i].numVerts, 1);
				f->readUINT32 (&mpMeshData[i].numInds, 1);
				mpMeshData[i].numInds *= 3;
				// material is going to drawSurf (not shared)
				uint32 mtl;
				f->readUINT32 (&mtl, 1);
				assert (mtl < mtls.cnt);
				ds[i].mtl = mtls.list[mtl];
//				ds[i].obj = node;	// no need to set this at load time! it will be set during getNewInstance
				ds[i].mesh = &mpMeshData[i];
//				mSurfaces[i].tris = &mTris;
			}
		}
		uint32 sp;
	
		// hastangents
		uchar tangents;
		f->read (&tangents, sizeof (uchar));
		//fprintf (stderr, "tangents: %s\n", tangents ? "yes" : "no");
	
		uchar lightmaps = 0;
		if (minorver >= 1)
		{
			f->read (&lightmaps, sizeof (uchar));
		}
		//fprintf (stderr, "lightmaps: %s\n", lightmaps ? "yes" : "no");
		if (lightmaps)
		{
			uchar sz;
			f->read (&sz, sizeof (uchar));
			if (sz)
			{
				cStr s;
				s.resize (sz);
				f->read ((char *)&s[0], sz);
				// read lightmap texture
				baseTexturePtr t = g_engine->getResourceMgr ()->createTexture ("lightmaps/" + s);
				for (size_t i = 0; i < mMeshDataCount; i++)
					ds[i].lightmap = t;
			}
		}

		uchar colors = 0;
	
		// vertex buffer (meshData stuff)
		// it is shared for entire model (all subsets) so don't delete VBO for each subset please!

		f->readUINT32 (&sp, 1);
		//fprintf (stderr, "%d verts\n", sp);
		if (sp)
		{
			int numVerts = sp;
			int vertSize;
			vertSize = sizeof (vector3) * 2 + sizeof (vector2); // pos + norm + uv
			if (colors)
				vertSize += 4;
			if (lightmaps)
				vertSize += sizeof (vector2);
			if (tangents)
				vertSize += sizeof (vector3) * 2;
			int size = (int)(vertSize * numVerts);
			// init vertex/index buffers
			uchar* vb;
			GLuint	vbo;
			vb = NULL;
			vbo = 0;
			
			uchar *ptr;
			vb = new uchar[size];
			ptr = vb;

//			mTris.verts = new drawVertex_t[mTris.numVerts];
			for (int i = 0; i < numVerts; i++)
			{
				f->readFloat ((float*)ptr, 3);
				ptr += sizeof (vector3);
				f->readFloat ((float*)ptr, 3);
				ptr += sizeof (vector3);
				f->readFloat ((float*)ptr, 2);
				ptr += sizeof (vector2);
				if (colors)	// FIXME: bmdl never contain colors now
				{
					f->read (ptr, 4);
					ptr += 4;
				}
				if (!lightmaps)
				{
					vector2 lm;
					f->readFloat ((float*)&lm, 2);
				}
				else
				{
					f->readFloat ((float*)ptr, 2);
					ptr += sizeof (vector2);
				}
				if (!tangents)
				{
					vector3 tn[2];
					f->readFloat ((float*)&tn, 6);
				}
				else
				{
					f->readFloat ((float*)ptr, 6);
					ptr += sizeof (vector3) * 2;
				}
				if (minorver >= 2)
				{
					uint32 clr;
					f->read (&clr, sizeof (clr));	// color?
				}
			}
			if (GLEW_ARB_vertex_buffer_object)
			{
				glGenBuffersARB (1, &vbo);
				glBindBufferARB (GL_ARRAY_BUFFER_ARB, vbo);
				glBufferDataARB (GL_ARRAY_BUFFER_ARB, size, NULL, GL_STATIC_DRAW_ARB);
				uchar *p = (uchar*)glMapBufferARB (GL_ARRAY_BUFFER_ARB, GL_WRITE_ONLY_ARB);
				memcpy (p, vb, size);
				glUnmapBufferARB (GL_ARRAY_BUFFER_ARB);
			}

			// set vb to meshdata
			for (int j = 0; j < mMeshDataCount; j++)
			{
				mpMeshData[j].verts = vb;
				mpMeshData[j].vbo = vbo;
				mpMeshData[j].primType = GL_TRIANGLES;
				mpMeshData[j].vertexSize = vertSize;
				mpMeshData[j].have_colors = colors ? 1 : 0;
				mpMeshData[j].have_lightmaps = lightmaps ? 1 : 0;
				mpMeshData[j].have_tangents = tangents ? 1 : 0;
			}
		}
	
		// faces
		f->readUINT32 (&sp, 1);
		if (sp)
		{
			// copy faces
			int numIndexes = sp * 3;
			uchar* ib;
			GLuint	ibo;
			ib = NULL;
			ibo = 0;

			ib = new uchar[sizeof (ushort) * numIndexes];
			f->readUINT16 ((uint16*)ib, numIndexes);

			if (GLEW_ARB_vertex_buffer_object)
			{
				glGenBuffersARB (1, &ibo);
				glBindBufferARB (GL_ELEMENT_ARRAY_BUFFER_ARB, ibo);
				glBufferDataARB (GL_ELEMENT_ARRAY_BUFFER_ARB, numIndexes * sizeof (ushort), ib, GL_STATIC_DRAW_ARB);
			}
			// set ib to meshdata
			for (int j = 0; j < mMeshDataCount; j++)
			{
				mpMeshData[j].inds = ib;
				mpMeshData[j].ibo = ibo;
			}
		}
	
		if (minorver < 2)
		{
			// skip old version animation flag
			uchar b;
			f->read (&b, sizeof (b));
		}
	}

	sceneObject* model::load (file *f, modelMtlList &mtls, sceneObject *parent)
	{
		// FIXME: pass as formal parameter?
		static int minorver = 0;
		static int majorver = 1;
		sceneObject *node = new sceneObject (parent);
		renderable *rend = new renderable (this);
		node->attachRenderable (rend);
		if (!parent)
		{
			// read magic
			char mgc[10];
			f->read( mgc, 9 );
			mgc[9] = 0;
			if (!strcmp (mgc, "FEMDL0101"))
			{
				majorver = 1;
				minorver = 1;
			}
			else if (!strcmp (mgc, "FEMDL0102"))
			{
				majorver = 1;
				minorver = 2;
			}
			else
				sys_error ( "Invalid model file signature (%s).\n", f->name());
	
			// load materials
			mtls.load (f);
		}
	
		if (minorver >= 2)
		{
			char nsz;
			f->read (&nsz, sizeof (char));
			if (nsz)
			{
				char nm[100];
				f->read (nm, nsz);
				nm[nsz] = 0;
				setName (nm);
				node->setName (nm);
			}
			//fprintf (stderr, "got object %s\n", name ());
		}
	
		uint32 sp;
	
		// matrix
		matrix4 mtx;
		f->readMatrix4 (&mtx, 1);
		matrix4 worldMtx = parent ? mtx * parent->getWorldMatrix() : mtx;
		node->setRelativeMatrix (mtx);
		node->setWorldMatrix (worldMtx);
	
		// subsets
		f->readUINT32 (&sp, 1);
		//fprintf (stderr, "got %d subsets\n", sp);
		drawSurf_t *ds = NULL;
		if (sp)
		{
//			mSurfaces.resize( sp );
			mMeshDataCount = sp;
			mpMeshData = new meshData_t[sp];
			ds = new drawSurf_t[sp];
			memset (mpMeshData, 0, sizeof (meshData_t) * sp);
			memset (ds, 0, sizeof (drawSurf_t) * sp);
			rend->setDrawSurfs (ds, sp);
			for (size_t i = 0; i < sp; i++)
			{
				// meshData/geomSource stuff (shared)
				f->readUINT32 (&mpMeshData[i].firstVertex, 1);
				f->readUINT32 (&mpMeshData[i].firstIndex, 1);
				mpMeshData[i].firstIndex *= 3;
				f->readUINT32 (&mpMeshData[i].numVerts, 1);
				f->readUINT32 (&mpMeshData[i].numInds, 1);
				mpMeshData[i].numInds *= 3;
				// material is going to drawSurf (not shared)
				uint32 mtl;
				f->readUINT32 (&mtl, 1);
				assert (mtl < mtls.cnt);
				ds[i].mtl = mtls.list[mtl];
				ds[i].obj = node;
				ds[i].mesh = &mpMeshData[i];
//				mSurfaces[i].tris = &mTris;
			}
		}
	
		// hastangents
		uchar tangents;
		f->read(&tangents, sizeof(uchar));
		//fprintf (stderr, "tangents: %s\n", tangents ? "yes" : "no");
	
		uchar lightmaps = 0;
		if (minorver >= 1)
		{
			f->read (&lightmaps, sizeof (uchar));
		}
		//fprintf (stderr, "lightmaps: %s\n", lightmaps ? "yes" : "no");
		if (lightmaps)
		{
			uchar sz;
			f->read (&sz, sizeof (uchar));
			if (sz)
			{
				char nm[200] = "lightmaps/";
				int l = strlen (nm);
				f->read (nm + l, sz);
				nm[l+sz] = 0;
				// read lightmap texture
				baseTexturePtr t = g_engine->getResourceMgr ()->createTexture (nm);
				for (size_t i = 0; i < mMeshDataCount; i++)
					ds[i].lightmap = t;
			}
		}

		uchar colors = 0;
	
		// vertex buffer (meshData stuff)
		// it is shared for entire model (all subsets) so don't delete VBO for each subset please!

		f->readUINT32 (&sp, 1);
		//fprintf (stderr, "%d verts\n", sp);
		if (sp)
		{
			int numVerts = sp;
			int vertSize;
			vertSize = sizeof (vector3) * 2 + sizeof (vector2); // pos + norm + uv
			if (colors)
				vertSize += 4;
			if (lightmaps)
				vertSize += sizeof (vector2);
			if (tangents)
				vertSize += sizeof (vector3) * 2;
			int size = (int)(vertSize * numVerts);
			// init vertex/index buffers
			uchar* vb;
			GLuint	vbo;
			vb = NULL;
			vbo = 0;
			
			uchar *ptr;
			vb = new uchar[size];
			ptr = vb;

//			mTris.verts = new drawVertex_t[mTris.numVerts];
			for (int i = 0; i < numVerts; i++)
			{
				f->readFloat ((float*)ptr, 3);
				ptr += sizeof (vector3);
				f->readFloat ((float*)ptr, 3);
				ptr += sizeof (vector3);
				f->readFloat ((float*)ptr, 2);
				ptr += sizeof (vector2);
				if (colors)	// FIXME: bmdl never contain colors now
				{
					f->read (ptr, 4);
					ptr += 4;
				}
				if (!lightmaps)
				{
					// skipt lightmap stub
					// FIXME: do not include this when not needed
					vector2 lm;
					f->readFloat ((float*)&lm, 2);
				}
				else
				{
					f->readFloat ((float*)ptr, 2);
					ptr += sizeof (vector2);
				}
				if (!tangents)
				{
					vector3 tn[2];
					f->readFloat ((float*)&tn, 3);
				}
				else
				{
					f->readFloat ((float*)ptr, 6);
					ptr += sizeof (vector3) * 2;
				}
				if (minorver >= 2)
				{
					// stub-skip colors (where is this came from?)
					uint32 clr;
					f->read (&clr, sizeof (clr));	// color?
				}
			}
			if (GLEW_ARB_vertex_buffer_object)
			{
				glGenBuffersARB (1, &vbo);
				glBindBufferARB (GL_ARRAY_BUFFER_ARB, vbo);
				glBufferDataARB (GL_ARRAY_BUFFER_ARB, size, NULL, GL_STATIC_DRAW_ARB);
				uchar *p = (uchar*)glMapBufferARB (GL_ARRAY_BUFFER_ARB, GL_WRITE_ONLY_ARB);
				memcpy (p, vb, size);
				glUnmapBufferARB (GL_ARRAY_BUFFER_ARB);
			}

			// set vb to meshdata
			for (int j = 0; j < mMeshDataCount; j++)
			{
				mpMeshData[j].verts = vb;
				mpMeshData[j].vbo = vbo;
				mpMeshData[j].primType = GL_TRIANGLES;
				mpMeshData[j].vertexSize = vertSize;
				mpMeshData[j].have_colors = colors ? 1 : 0;
				mpMeshData[j].have_lightmaps = lightmaps ? 1 : 0;
				mpMeshData[j].have_tangents = tangents ? 1 : 0;
			}
		}
	
		// faces
		f->readUINT32 (&sp, 1);
		if (sp)
		{
			// copy faces
			int numIndexes = sp * 3;
			uchar* ib;
			GLuint	ibo;
			ib = NULL;
			ibo = 0;

			ib = new uchar[sizeof (ushort) * numIndexes];
			f->readUINT16 ((uint16*)ib, numIndexes);

			if (GLEW_ARB_vertex_buffer_object)
			{
				glGenBuffersARB (1, &ibo);
				glBindBufferARB (GL_ELEMENT_ARRAY_BUFFER_ARB, ibo);
				glBufferDataARB (GL_ELEMENT_ARRAY_BUFFER_ARB, numIndexes * sizeof (ushort), ib, GL_STATIC_DRAW_ARB);
			}
			// set ib to meshdata
			for (int j = 0; j < mMeshDataCount; j++)
			{
				mpMeshData[j].inds = ib;
				mpMeshData[j].ibo = ibo;
			}
		}
	
		if (minorver < 2)
		{
			// skip old version animation flag
			uchar b;
			f->read (&b, sizeof (b));
		}
	
		// children
		f->readUINT32 (&sp, 1);
		int nChildren = sp;
//		mChildren.resize( sp );
		for (uint32 i = 0; i < nChildren; i++)
		{
			model* m = new model;
			sceneObject *c = m->load (f, mtls, node);
			node->addChild (c);
		}
		
		init ();

		return node;
	}
	
/*		typedef smartPtr <mtlBlock> mtlBlockPtr;
	
		mtlBlockPtr mpMtlBlock;*/
	
/*	// renderable implementation
	int model::numSubsets (void) const
	{
		return (int)mSurfaces.size (); 
	}
	
	materialPtr	model::getMtl (int subset) const
	{
		return mpMtlBlock ? mSurfaces[subset].mtl : NULL;
	}*/
#if 0	
	void model::prepare (int subset) const
	{
	/*
		g_engine->getEffectParms()->setMatrix( effectParm_WorldMatrix, mWorldMatrix );
		if (mSurfaces[subset].lightmap)
			g_engine->getEffectParms ()->setTexture (effectParm_LightmapTexture, mSurfaces[subset].lightmap);*/
	}
	
	void model::render (int subset, int pass) const
	{
		if (mpData)
			mpData->drawSubset (subset, pass);
	}
	#endif
/*	void model::renderShadowVolume (bool withChildren) const
	{
		if (mpData)	// FIXME: model can contain NULL mpData, but this is the case for dummy object, which can be handled in more efficient way
			mpData->renderShadowVolume ();
		sceneObject::renderShadowVolume (withChildren);
	}*/

#if 0
	void	model::buildShadowVolume (const lightSource &src)
	{
	#if 1
		lightSource::type t = src.getType ();
		
		// lightpos is transformed just like everything other, but we want it in model space
		matrix4 light2model = src.getWorldMatrix () * getWorldMatrix ().inverse ();
		vector3 pos = src.getPosition () * light2model;
		vector3 dir;
		vector3 L = src.getPosition () * light2model;
		if (src.getType () == lightSource::direct)
		{
			vector3 dir = light2model.transform (src.getDirection ());
			dir.normalize ();
		}
	
		// extrude verts
		for (int i = 0; i < mTris.numVerts; i++)
		{
			if (src.getType () != lightSource::direct)
			{
				dir = (vector3 &)mTris.shadowVerts[i] - src.getPosition ();
				dir.normalize ();
			}
	
	//	V.w*L + float4 (V.xyz - L.xyz, 0)
			mTris.shadowVerts[i + mTris.numVerts].xyzw.x = mTris.shadowVerts[i].xyzw.x - L.x;
			mTris.shadowVerts[i + mTris.numVerts].xyzw.y = mTris.shadowVerts[i].xyzw.y - L.y;
			mTris.shadowVerts[i + mTris.numVerts].xyzw.z = mTris.shadowVerts[i].xyzw.z - L.z;
			mTris.shadowVerts[i + mTris.numVerts].xyzw.w = 0;
		}
	
		// write indexes, build shadow volume
		mTris.numShadowVolumeIndexes = 0;
		for (int i = 0; i < mTris.numSilEdges; i++)
		{
			// check if only one of two faces forming the edge is visible from light
			const vector3 &n1 = mTris.facePlanes[mTris.silEdges[i].f1].normal ();
			const vector3 &n2 = mTris.facePlanes[mTris.silEdges[i].f2].normal ();
			float d1 = n1.dot (dir);
			float d2 = n2.dot (dir);
	#define s_idx (mTris.shadowVolumeIndexes)
	#define n_idx (mTris.numShadowVolumeIndexes)
			// add edge verts to list of shadow volume indexes, forming a quad
			if (d1 < 0 && d2 > 0) // f1 is visible, v2-v1 form CCW order
			{
				s_idx[n_idx++] = mTris.silEdges[i].v2;
				s_idx[n_idx++] = mTris.silEdges[i].v1;
				s_idx[n_idx++] = mTris.silEdges[i].v1 + mTris.numVerts;
				s_idx[n_idx++] = mTris.silEdges[i].v2;
				s_idx[n_idx++] = mTris.silEdges[i].v1 + mTris.numVerts;
				s_idx[n_idx++] = mTris.silEdges[i].v2 + mTris.numVerts;
			}
			else if (d2 < 0 && d1 > 0) // f2 is visible, v1-v2 form CCW order
			{
				s_idx[n_idx++] = mTris.silEdges[i].v1;
				s_idx[n_idx++] = mTris.silEdges[i].v2;
				s_idx[n_idx++] = mTris.silEdges[i].v1 + mTris.numVerts;
				s_idx[n_idx++] = mTris.silEdges[i].v2;
				s_idx[n_idx++] = mTris.silEdges[i].v2 + mTris.numVerts;
				s_idx[n_idx++] = mTris.silEdges[i].v1 + mTris.numVerts;
			}
		}
	
		// front/back caps are not added to shadow vertex list, because they are taken from normal vertex array
		// but we write caps indexes
	#if 0
		for (int i = 0; i < mTris.numIndexes/3; i++)
		{
			float d = mTris.facePlanes[i].normal ().dot (dir);
			if (d < 0)	// visible
			{
				// front cap
				s_idx[n_idx++] = mTris.silIndexes[i*3+0];
				s_idx[n_idx++] = mTris.silIndexes[i*3+2];
				s_idx[n_idx++] = mTris.silIndexes[i*3+1];
			}
			else
			{
				// back cap (from extruded list)
				s_idx[n_idx++] = mTris.silIndexes[i*3+0] + mTris.numVerts;
				s_idx[n_idx++] = mTris.silIndexes[i*3+2] + mTris.numVerts;
				s_idx[n_idx++] = mTris.silIndexes[i*3+1] + mTris.numVerts;
			}
		}
	#endif
	#undef s_idx
		for (size_t i = 0; i < mChildren.size (); i++)
		{
			if (mChildren[i]->isTypeOf (TypeId))
			{
				mChildren[i].dynamicCast<model>()->buildShadowVolume (src);
			}
		}
		
	#endif
	}
	#endif

#if 0
	const modelTriangles_t& model::getTriangles (void) const
	{
		return mTris;
	}
	#endif
	
}
