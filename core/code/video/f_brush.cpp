/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_brush.h"
#include "f_error.h"
#include "f_texture.h"
#include "f_material.h"
#include "f_effect.h"

//-----------------------------------------------
//	texdef
//-----------------------------------------------
feBrushFaceTexdef::feBrushFaceTexdef()
	: mContents( 0 )
	, mFlags( 0 )
	, mValue( 0 )
{
	mScale[0] = mScale[1] = 1;
	mShift[0] = mShift[1] = 0;
	mRotate = 0;
}

//-----------------------------------------------
//	winding
//-----------------------------------------------

feBrushWinding::feBrushWinding()
{
}

feBrushWinding::feBrushWinding( const feBrushWinding &w )
{
	mPoints = w.mPoints;
	mTexCoords = w.mTexCoords;
}

feBrushWinding::~feBrushWinding()
{
}

void			feBrushWinding::baseForPlane( const fePlane &p )
{
	feVector3	org, vright, vup( 0, 0, 0 );
	float max = -1.0e+5f;
	int x = -1;
	int i;
	float v;
	const feVector3 &n = p.normal();
	for ( i = 0; i < 3; i++ )
	{
		v = fabs( ((const float *)&n)[i] );
		if ( v > max )
		{
			x = i;
			max = v;
		}
	}
	switch (x)
	{
	case 0:
	case 1:
		vup[2] = 1;
		break;		
	case 2:
		vup[0] = 1;
		break;		
	}
	v = vup.dot( n );
	vup = vup - v * n;
	vup.normalize();

	org = p.normal() * p.constant();
	vright = vup.cross( p.normal() );

	vup = vup * 1.0e+5f;
	vright = vright * 1.0e+5f;

	// project a really big	axis aligned box onto the plane
	mPoints.resize( 4 );
	mPoints[0] = org - vright;
	mPoints[0] = mPoints[0] + vup;

	mPoints[1] = org + vright;
	mPoints[1] = mPoints[1] + vup;

	mPoints[2] = org + vright;
	mPoints[2] = mPoints[2] - vup;

	mPoints[3] = org - vright;
	mPoints[3]= mPoints[3] - vup;
}

bool			feBrushWinding::clip ( const fePlane &split, bool keepon )
{
#define MAX_POINTS_ON_WINDING	100
#define ON_EPSILON				0.01f
#define	SIDE_FRONT		0
#define	SIDE_ON			2
#define	SIDE_BACK		1
#define	SIDE_CROSS		-2
	float	dists[MAX_POINTS_ON_WINDING];
	int		sides[MAX_POINTS_ON_WINDING];
	int		counts[3];
	float	dot;
	int		i, j;
	float	*p1, *p2;
	feVector3	mid;
	feBrushWinding	neww;
	int		maxpts;
	
	counts[0] = counts[1] = counts[2] = 0;

	// determine sides for each point
	for ( i = 0; i < numPoints(); i++ )
	{
		dot = mPoints[i].dot( split.normal() );
		dot -= split.constant();
		dists[i] = dot;
		if (dot > ON_EPSILON)
			sides[i] = SIDE_FRONT;
		else if (dot < -ON_EPSILON)
			sides[i] = SIDE_BACK;
		else
			sides[i] = SIDE_ON;
		counts[sides[i]]++;
	}
	sides[i] = sides[0];
	dists[i] = dists[0];
	
	if ( keepon && !counts[0] && !counts[1] )
	{
		// leave winding unclipped
		return true;
	}
		
	if ( !counts[0] )
	{
		// clipped away
		return false;
	}

	if ( !counts[1] )
	{
		// unclipped too
		return true;
	}
	
	maxpts = numPoints() + 4;	// can't use counts[0]+2 because
								// of fp grouping errors
	neww.mPoints.reserve( maxpts );
		
	for ( i = 0; i < numPoints(); i++ )
	{
		p1 = mPoints[i];
		
		if ( sides[i] == SIDE_ON )
		{
			neww.mPoints.push_back( p1 );
			continue;
		}
	
		if ( sides[i] == SIDE_FRONT )
		{
			neww.mPoints.push_back( p1 );
		}
		
		if ( sides[i+1] == SIDE_ON || sides[i+1] == sides[i] )
			continue;
			
		// generate a split point
		p2 = mPoints[ ( i + 1 ) % numPoints() ];
		
		dot = dists[i] / (dists[i]-dists[i+1]);
		for (j=0 ; j<3 ; j++)
		{	// avoid round off error when possible
			if (split.normal()[j] == 1)
				mid[j] = split.constant();
			else if (split.normal()[j] == -1)
				mid[j] = -split.constant();
			else
				mid[j] = p1[j] + dot*(p2[j]-p1[j]);
		}
			
		neww.mPoints.push_back( mid );
	}
	
	// free the original winding
	( *this ) = neww;
	
	return true;
}


//-----------------------------------------------
// face
//-----------------------------------------------

feBrushFace::feBrushFace()
{
	mpMtl = NULL;
}

feBrushFace::feBrushFace( const feBrushFace &f )
{
	mpMtl = f.mpMtl;
	mPlane = f.mPlane;
	mPlanePts[0] = f.mPlanePts[0];
	mPlanePts[1] = f.mPlanePts[1];
	mPlanePts[2] = f.mPlanePts[2];
	mWinding = f.mWinding;
	mTexdef = f.mTexdef;
}

feBrushFace::~feBrushFace()
{
}

// FIXME: where to move this stuff? it shouldn't be here..
float	baseaxis[18][3] =
{
	{0,0,1}, {1,0,0}, {0,-1,0},			// floor
	{0,0,-1}, {1,0,0}, {0,-1,0},		// ceiling
	{1,0,0}, {0,1,0}, {0,0,-1},			// west wall
	{-1,0,0}, {0,1,0}, {0,0,-1},		// east wall
	{0,1,0}, {1,0,0}, {0,0,-1},			// south wall
	{0,-1,0}, {1,0,0}, {0,0,-1}			// north wall
};

void TextureAxisFromPlane( const fePlane &pln, feVector3 &xv, feVector3 &yv)
{
	int		bestaxis;
	float	dot,best;
	int		i;
	
	best = 0;
	bestaxis = 0;
	
	for (i=0 ; i<6 ; i++)
	{
		dot = pln.normal().dot( *(( feVector3 * )baseaxis[i*3]) );
		if (dot > best)
		{
			best = dot;
			bestaxis = i;
		}
	}
	
	xv = ( *(( feVector3 * )baseaxis[bestaxis*3+1]) );
	yv = ( *(( feVector3 * )baseaxis[bestaxis*3+2]) );
}

void	feBrushFace::textureVectors( float STfromXYZ[2][4] )
{
	feVector3	pvecs[2];
	int			sv, tv;
	float		ang, sinv, cosv;
	float		ns, nt;
	int			i,j;
//	feBaseTexture *q = material() ? material()->getTexture( 0 ) : NULL;
	feBaseTexture *q = NULL;

	feBrushFaceTexdef	*td = &texdef();

	memset( STfromXYZ, 0, 8 * sizeof( float ) );

	if (!td->scale()[0])
		td->scale()[0] = 0.5f;
	if (!td->scale()[1])
		td->scale()[1] = 0.5f;

	// get natural texture axis
	TextureAxisFromPlane(plane(), pvecs[0], pvecs[1]);

	// rotate axis
	if (td->rotate() == 0)
		{ sinv = 0 ; cosv = 1; }
	else if (td->rotate() == 90)
		{ sinv = 1 ; cosv = 0; }
	else if (td->rotate() == 180)
		{ sinv = 0 ; cosv = -1; }
	else if (td->rotate() == 270)
		{ sinv = -1 ; cosv = 0; }
	else
	{	
		ang = td->rotate() / 180.f * M_PI;
		sinv = sin(ang);
		cosv = cos(ang);
	}

	if (pvecs[0][0])
		sv = 0;
	else if (pvecs[0][1])
		sv = 1;
	else
		sv = 2;
				
	if (pvecs[1][0])
		tv = 0;
	else if (pvecs[1][1])
		tv = 1;
	else
		tv = 2;
					
	for (i=0 ; i<2 ; i++) {
		ns = cosv * pvecs[i][sv] - sinv * pvecs[i][tv];
		nt = sinv * pvecs[i][sv] +  cosv * pvecs[i][tv];
		STfromXYZ[i][sv] = ns;
		STfromXYZ[i][tv] = nt;
	}

	// scale
	for (i=0 ; i<2 ; i++)
		for (j=0 ; j<3 ; j++)
			STfromXYZ[i][j] = STfromXYZ[i][j] / td->scale()[i];

	// shift
	STfromXYZ[0][3] = td->shift()[0];
	STfromXYZ[1][3] = td->shift()[1];

	for (j=0 ; j<4 ; j++) {
		STfromXYZ[0][j] /= q ? checked_cast<feTexture*>( q )->getWidth() : 64;
		STfromXYZ[1][j] /= q ? checked_cast<feTexture*>( q )->getHeight() : 64;
	}
}

feVector2		feBrushFace::emitTexCoords( const feVector3 &pt )
{
	float STfromXYZ[2][4];
	textureVectors ( STfromXYZ );
	feVector2 out;
	out.x = pt.dot( STfromXYZ[0] ) + STfromXYZ[0][3];
	out.y = pt.dot( STfromXYZ[1] ) + STfromXYZ[1][3];
	return out;
}

void				feBrushFace::applyTexdef( void )
{
	for ( int w = 0; w < winding().numPoints(); w++ )
	{
		winding().texCoord( w ) = emitTexCoords( winding().point( w ) );
	}
}

void				feBrushFace::setMaterial( const feMaterialPtr &mtl )
{
	mpMtl = mtl;
	applyTexdef();
}

//-----------------------------------------------
// brush
//-----------------------------------------------

feBrush::feBrush()
{
}

feBrush::feBrush( const feVector3 &mins, const feVector3 &maxs )
{
	create( mins, maxs );
}

void	feBrush::create( const feVector3 &mins, const feVector3 &maxs )
{
	mMins = mins;
	mMaxs = maxs;

	int		i, j;
	feVector3	pts[4][2];

	for ( i = 0; i < 3; i++ )
	{
		if ( mMaxs[i] < mMins[i] )
			throw feGenericError( "feBrush::create error, inadequate bounds" );
	}

	pts[0][0][0] = mMins[0];
	pts[0][0][1] = mMins[1];
	
	pts[1][0][0] = mMins[0];
	pts[1][0][1] = mMaxs[1];
	
	pts[2][0][0] = mMaxs[0];
	pts[2][0][1] = mMaxs[1];
	
	pts[3][0][0] = mMaxs[0];
	pts[3][0][1] = mMins[1];
	
	for ( i = 0; i < 4; i++ )
	{
		pts[i][0][2] = mMins[2];
		pts[i][1][0] = pts[i][0][0];
		pts[i][1][1] = pts[i][0][1];
		pts[i][1][2] = mMaxs[2];
	}

	mFaces.resize( 6 );
	feBrushFace *f;
	for ( i = 0; i < 4; i++ )
	{
//		f->texdef = *texdef;
//		f->texdef.flags &= ~SURF_KEEP;
//		f->texdef.contents &= ~CONTENTS_KEEP;
//		f->next = b->brush_faces;
//		b->brush_faces = f;
		f = &mFaces[i];
		j = (i+1)%4;
		f->planePt( 0 ) = pts[j][1];
		f->planePt( 1 ) = pts[i][1];
		f->planePt( 2 ) = pts[i][0];
	}
	
	f = &mFaces[4];
//	f->texdef = *texdef;
//	f->texdef->flags &= ~SURF_KEEP;
//	f->texdef->contents &= ~CONTENTS_KEEP;
//	f->next = b->brush_faces;
//	b->brush_faces = f;
	f->planePt( 0 ) = pts[0][1];
	f->planePt( 1 ) = pts[1][1];
	f->planePt( 2 ) = pts[2][1];

	f = &mFaces[5];
//	f->texdef = *texdef;
//	f->texdef->flags &= ~SURF_KEEP;
//	f->texdef->contents &= ~CONTENTS_KEEP;
//	f->next = b->brush_faces;
//	b->brush_faces = f;
	f->planePt( 0 ) = pts[2][0];
	f->planePt( 1 ) = pts[1][0];
	f->planePt( 2 ) = pts[0][0];

	build();
}

feBrush::feBrush( const feBrush &b )
{
	mFaces = b.mFaces;
	mMins = b.mMins;
	mMaxs = b.mMaxs;
}

feBrush::~feBrush()
{
}

bool	feBrush::makeFaceWinding( int face )
{
	feBrushWinding w;
	w.baseForPlane( mFaces[face].plane() );

	fePlane plane;
	bool past = false;
	for ( size_t clip = 0; clip < mFaces.size(); clip++  )
	{
		if ( (int)clip == face )
		{
			past = true;
			continue;	// do not clip by itself
		}
		const fePlane &facePlane = mFaces[face].plane();
		const fePlane &clipPlane = mFaces[clip].plane();
		if ( facePlane.normal().dot( clipPlane.normal() ) > 0.999f
			&& fabs( facePlane.constant() - clipPlane.constant() ) < 0.01f )
		{
			// identical plane, use the later one
			if ( past )
			{
				// no winding
				return false;
			}
			continue;
		}
		// flip the plane, because we want to keep the back side
		plane.normal() = -clipPlane.normal();
		plane.constant() = -clipPlane.constant();
		if ( !w.clip( plane, false ) )
			return false;
	}
	if ( w.numPoints() < 3 )
		return false;

	mFaces[face].mWinding = w;

	return true;
}

bool		feBrush::makeFacePlane( int face )
{
	feVector3 v1, v2;
	v1 = mFaces[face].mPlanePts[1] - mFaces[face].mPlanePts[0];
	v2 = mFaces[face].mPlanePts[2] - mFaces[face].mPlanePts[1];
	mFaces[face].mPlane.normal() = v2.cross( v1 );
    if ( mFaces[face].mPlane.normal().normalize() < 0.1f )
		return false;
	mFaces[face].mPlane.constant() = mFaces[face].mPlanePts[0].dot( mFaces[face].mPlane.normal() );
	return true;
}

void		feBrush::buildWindings( bool snap )
{
	size_t face;

	mMins = feVector3( 1.0e+5f, 1.0e+5f, 1.0e+5f );
	mMaxs = feVector3( -1.0e+5f, -1.0e+5f, -1.0e+5f );

	for ( face = 0; face < mFaces.size(); face++ )
	{
		makeFacePlane( face );
	}
	for ( face = 0; face < mFaces.size(); face++ )
	{
		if ( !makeFaceWinding( face ) )
		{
			faces(face).winding().mPoints.clear();
			faces(face).winding().mTexCoords.clear();
			mMins = mMaxs = feVector3( 0, 0, 0 );
		}
		else
		{
			faces(face).winding().mTexCoords.resize( faces(face).winding().numPoints() );
			for ( int i = 0; i < faces(face).winding().numPoints(); i++ )
			{
				const feVector3 &pt = faces(face).winding().point( i );
				for ( int k = 0; k < 3; k++ )
				{
					if ( pt[k] < mMins[k] )
						mMins[k] = pt[k];
					if ( pt[k] > mMaxs[k] )
						mMaxs[k] = pt[k];
				}
				faces(face).winding().texCoord( i ) = faces(face).emitTexCoords( pt );
			}
		}
	}
}

void		feBrush::move( const feVector3 &moveVec, bool snap )
{
	for ( int i = 0; i < numFaces(); i++ )
	{
		for ( int j = 0; j < 3; j++ )
			mFaces[i].planePt(j) += moveVec;
	}
	mMins += moveVec;
	mMaxs += moveVec;
	build();
}

void		feBrush::splitByFace( const feBrushFace &f, feBrush *&front, feBrush *&back )
{
	feBrush	*b = new feBrush( *this );
	feVector3	temp;
	b->addFace( f );
	b->build();
	b->removeEmptyFaces();

	if ( !b->numFaces() )
	{
		// completely clipped away
		delete b;
		back = NULL;
	}
	else
	{
//		Entity_LinkBrush (in->owner, b);
		back = b;
	}

	b = new feBrush( *this );

	// swap the plane winding
	feBrushFace nf = f;
	temp = nf.planePt( 0 );
	nf.planePt( 0 ) = nf.planePt( 1 );
	nf.planePt( 1 ) = temp;
	b->addFace( nf );
	b->build();
	b->removeEmptyFaces ();
	if ( !b->numFaces() )
	{
		// completely clipped away
		delete b;
		front = NULL;
	}
	else
	{
//		Entity_LinkBrush (in->owner, b);
		front = b;
	}
}

void		feBrush::snapToGrid()
{
}

void		feBrush::snapPlanePts()
{
}

void		feBrush::rotate( const feVector3 &angle, const feVector3 &origin, bool build )
{
}

void		feBrush::flip( const feVector3 &axis, const feVector3 &origin )
{
//	feVector3 origin = ( mins() + maxs() ) / 2;
	for ( int ff = 0; ff < numFaces(); ff++ )
	{
		feBrushFace &f = faces( ff );
		feVector3 newpts[3];
		for ( int pp = 0; pp < 3; pp++ )
		{
			feVector3 dir = origin - f.planePt( pp );
			float dot = dir.dot( axis );
			float sign = ( dot == 0 ) ? 0 : ( ( dot < 0 ) ? -1 : 1 );
			dir.x *= axis.x;
			dir.y *= axis.y;
			dir.z *= axis.z;
			float d = dir.length();
			newpts[pp] = f.planePt( pp ) + axis * d * 2 * sign;
		}
		f.planePt( 0 ) = newpts[2];
		f.planePt( 1 ) = newpts[1];
		f.planePt( 2 ) = newpts[0];
	}
	build();
}

void		feBrush::removeEmptyFaces()
{
	int i;
	for ( ;; )
	{
		int nf = numFaces();
		for ( i = 0; i < nf; i++ )
		{
			feBrushFace &f = mFaces[i];
			feBrushWinding &w = f.winding();
			if ( w.numPoints() == 0 )
				break;
		}
		if ( i == nf )
			break;
		else
		{
			// remove face #i
			std::vector<feBrushFace>::iterator it = mFaces.begin() + i;
			mFaces.erase( it );
		}
	}
	if ( numFaces() < 3 )
		mFaces.clear();
}

void		feBrush::build( bool bSnap )
{
	/*
	** build the windings and generate the bounding box
	*/
	buildWindings( bSnap );
}

void		feBrush::buildTexturing( void )
{
	for ( int face = 0; face < numFaces(); face++ )
	{
		faces( face).applyTexdef();
	}
}

void		feBrush::makeSided ( int sides, int axis )
{
	mFaces.clear();
	int		i;
	feVector3	mins, maxs;
	//		texdef_t	*texdef;
	feBrushFace	f;
	feVector3	mid;
	float	width;
	float	sv, cv;

	if (sides < 3)
		throw( _T( "Bad sides number." ) );


	mins = mMins;
	maxs = mMaxs;

#ifdef F_EDITOR
	switch(g_pParentWnd->ActiveXY()->GetViewType())
	{
	case XY: axis = 2; break;
	case XZ: axis = 1; break;
	case YZ: axis = 0; break;
	}
#endif

	// find center of brush
	width = 8;
	for (i = 0; i < 3; i++)
	{
		mid[i] = (maxs[i] + mins[i]) * 0.5f;
		if (i == axis) continue;
		if ((maxs[i] - mins[i]) * 0.5 > width)
			width = (maxs[i] - mins[i]) * 0.5f;
	}

	// create top face
	f.planePt(2)[(axis+1)%3] = mins[(axis+1)%3]; f.planePt(2)[(axis+2)%3] = mins[(axis+2)%3]; f.planePt(2)[axis] = maxs[axis];
	f.planePt(1)[(axis+1)%3] = maxs[(axis+1)%3]; f.planePt(1)[(axis+2)%3] = mins[(axis+2)%3]; f.planePt(1)[axis] = maxs[axis];
	f.planePt(0)[(axis+1)%3] = maxs[(axis+1)%3]; f.planePt(0)[(axis+2)%3] = maxs[(axis+2)%3]; f.planePt(0)[axis] = maxs[axis];
	addFace( f );

	// create bottom face
	f.planePt(0)[(axis+1)%3] = mins[(axis+1)%3]; f.planePt(0)[(axis+2)%3] = mins[(axis+2)%3]; f.planePt(0)[axis] = mins[axis];
	f.planePt(1)[(axis+1)%3] = maxs[(axis+1)%3]; f.planePt(1)[(axis+2)%3] = mins[(axis+2)%3]; f.planePt(1)[axis] = mins[axis];
	f.planePt(2)[(axis+1)%3] = maxs[(axis+1)%3]; f.planePt(2)[(axis+2)%3] = maxs[(axis+2)%3]; f.planePt(2)[axis] = mins[axis];
	addFace( f );

	for (i=0 ; i<sides ; i++)
	{
		sv = sin (i*3.14159265*2/sides);
		cv = cos (i*3.14159265*2/sides);

		f.planePt(0)[(axis+1)%3] = floor(mid[(axis+1)%3]+width*cv+0.5);
		f.planePt(0)[(axis+2)%3] = floor(mid[(axis+2)%3]+width*sv+0.5);
		f.planePt(0)[axis] = mins[axis];

		f.planePt(1)[(axis+1)%3] = f.planePt(0)[(axis+1)%3];
		f.planePt(1)[(axis+2)%3] = f.planePt(0)[(axis+2)%3];
		f.planePt(1)[axis] = maxs[axis];

		f.planePt(2)[(axis+1)%3] = floor(f.planePt(0)[(axis+1)%3] - width*sv + 0.5);
		f.planePt(2)[(axis+2)%3] = floor(f.planePt(0)[(axis+2)%3] + width*cv + 0.5);
		f.planePt(2)[axis] = maxs[axis];

		addFace( f );
	}

	build();
}

void		feBrush::makeSidedCone ( int sides )
{
}

void		feBrush::makeSidedSphere ( int sides )
{
}

// subtracts this from a
// stores resulting fragment(s) into list
// FIXME: change std::vector<feBrush *> to std::vector<feBrush>
void		feBrush::csgSubtract( std::vector<feBrush *> &out, feBrush *a )
{
	feBrush *b = this;
	feBrush *in = a;
	feBrush *front, *back;

	for ( int f = 0; in && f < b->numFaces(); f++ )
	{
		// check if we're need to split by this face - it can be out of bounds
#if 1
		feBrushWinding &w = b->faces( f ).winding();
		int ff;
		for ( ff = 0; ff < in->numFaces(); ff++ )
		{
			const feBrushFace &f = in->faces( ff );
			bool positive = false;
			int pt;
			for ( pt = 0; pt < w.numPoints(); pt++ )
			{
				if ( f.plane().distanceTo( w.point( pt ) ) <= 0 )
				{
					positive = true;
					break;
				}
			}
			if ( positive == false )
				break;
		}
		if ( ff < in->numFaces() )
			continue;
#endif

		in->splitByFace( b->faces( f ), front, back );
		if ( in != a )
			delete in;
		if ( front )
			out.push_back( front );
		in = back;
		if ( !in )
			break;	// clipped away
	}

	//NOTE: in != a just in case brush b has no faces
	if ( in && in != a )
		delete in;
	else
	{
		// didn't really intersect
		for ( int i = 0; i < (int)out.size(); i++ )
		{
			delete out[i];
		}
		out.clear();
		out.push_back( a );
	}
}
