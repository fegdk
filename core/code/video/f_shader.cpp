/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_shader.h"
#include "backend.h"
#include <GL/glew.h>
#include "cvars.h"
#include "f_baserenderer.h"

namespace fe
{

	shader::shader (void)
	{
		memset (&mDSBinding, 0, sizeof (mDSBinding));
	}
	
/*	shader::shader (const char *shader)
	{
	}*/
	
	shader::shader (charParser &parser, const char *name)
	{
		memset (&mDSBinding, 0, sizeof (mDSBinding));
	}
	
	shader::~shader (void)
	{
	}
	
	void shader::bindDrawSurf (drawSurf_t *surf)
	{
		uchar *vptr = NULL;
		bool use_vbo = GLEW_ARB_vertex_buffer_object && r_use_vbo->ivalue && surf->mesh->vbo && surf->mesh->ibo;

		if (!use_vbo)
			vptr = surf->mesh->verts;
		vptr += surf->mesh->firstVertex * surf->mesh->vertexSize;
		int offs = 0;
		if (mDSBinding.pos)
		{
			enableStream (mDSBinding.pos);
			bindStreamData (mDSBinding.pos, 3, shader::FLOAT, surf->mesh->vertexSize, vptr + offs);
		}
		offs += sizeof (vector3);
		if (mDSBinding.norm)
		{
			enableStream (mDSBinding.norm);
			bindStreamData (mDSBinding.norm, 3, shader::FLOAT, surf->mesh->vertexSize, vptr + offs);
		}
		offs += sizeof (vector3);
		if (mDSBinding.tex0)
		{
			enableStream (mDSBinding.tex0);
			bindStreamData (mDSBinding.tex0, 2, shader::FLOAT, surf->mesh->vertexSize, vptr + offs);
		}
		offs += sizeof (vector2);
		if (surf->mesh->have_colors && mDSBinding.color)
		{
			enableStream (mDSBinding.color);
			bindStreamData (mDSBinding.color, 4, shader::UNSIGNED_BYTE, surf->mesh->vertexSize, vptr + offs);
			offs += 4;
		}
		if (surf->mesh->have_lightmaps && mDSBinding.tex1)
		{
			enableStream (mDSBinding.tex1);
			bindStreamData (mDSBinding.tex1, 2, shader::FLOAT, surf->mesh->vertexSize, vptr + offs);
			offs += sizeof (vector2);
		}
		if (surf->mesh->have_tangents && mDSBinding.tangent)
		{
			enableStream (mDSBinding.tangent);
			bindStreamData (mDSBinding.tangent, 3, shader::FLOAT, surf->mesh->vertexSize, vptr + offs);
			offs += sizeof (vector3);
		}
		if (surf->mesh->have_tangents && mDSBinding.binormal)
		{
			enableStream (mDSBinding.binormal);
			bindStreamData (mDSBinding.binormal, 3, shader::FLOAT, surf->mesh->vertexSize, vptr + offs);
			offs += sizeof (vector3);
		}

	}

	void shader::unbindDrawSurf (drawSurf_t *surf)
	{
		if (mDSBinding.pos)
		{
			disableStream (mDSBinding.pos);
		}
		if (mDSBinding.norm)
		{
			disableStream (mDSBinding.norm);
		}
		if (mDSBinding.tex0)
		{
			disableStream (mDSBinding.tex0);
		}
		if (surf->mesh->have_lightmaps && mDSBinding.tex1)
		{
			disableStream (mDSBinding.tex1);
		}
		if (surf->mesh->have_tangents && mDSBinding.tangent)
		{
			disableStream (mDSBinding.tangent);
		}
		if (surf->mesh->have_tangents && mDSBinding.binormal)
		{
			disableStream (mDSBinding.binormal);
		}
	}
	
}
