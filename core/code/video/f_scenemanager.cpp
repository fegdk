/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_scenemanager.h"
#include "f_helpers.h"
#include "f_sceneobject.h"
#include "f_engine.h"
#include "backend.h"
#include "f_renderablesceneobject.h"

namespace fe
{

	sceneManager::sceneManager (void)
	{
	}
	
	sceneManager::~sceneManager (void)
	{
		while (mpSceneObjectList) {
			mpSceneObjectList = mpSceneObjectList->mpSceneNext;
		}
	}
	
	void sceneManager::add (sceneObject *obj)
	{
		assert (!obj->mpSceneNext && !obj->mpScenePrev);
		if (mpSceneObjectList) {
			mpSceneObjectList->mpScenePrev = obj;
			obj->mpSceneNext = mpSceneObjectList;
		}
		mpSceneObjectList = obj;
	}
	
	void sceneManager::remove (sceneObject *obj)
	{
		if (obj->mpScenePrev) {
			obj->mpScenePrev->mpSceneNext = obj->mpSceneNext;
		}
		else {
			mpSceneObjectList = obj->mpSceneNext;
		}
		if (obj->mpSceneNext) {
			obj->mpSceneNext->mpScenePrev = obj->mpScenePrev;
		}
	}
	
	void sceneManager::update (void)
	{
		sceneObject *o = mpSceneObjectList;
		while (o) {
			o->update ();
			o = o->mpSceneNext;
		}
	}
	
	static void addSceneObjectToBackend (rBackend *be, sceneObject *o)
	{
		renderable *r = o->getRenderable ();
		if (r)
			be->add (r);
		for (sceneObject *c = o->getChildren (); c; c = c->getNextChild ())
			addSceneObjectToBackend (be, c);
	}

	void sceneManager::draw (void)
	{
		rBackend *be = g_engine->getRBackend ();
		sceneObject *o = mpSceneObjectList;
		while (o) {
			addSceneObjectToBackend (be, o);
			o = o->mpSceneNext;
		}
		g_engine->getRBackend ()->flush ();
	}
	
	void sceneManager::setCurrentMtl (const material *mtl)
	{
		mpCurrentMtl = mtl;
	}

	const material *sceneManager::getCurrentMtl (void) const
	{
		return mpCurrentMtl;
	}
	
}
