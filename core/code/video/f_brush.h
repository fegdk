/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_BRUSH_H
#define __F_BRUSH_H

#include "f_math.h"
#include "f_string.h"
#include "f_types.h"
#include "f_helpers.h"
#include "f_material.h"
#include "f_effect.h"
#include "f_basetexture.h"

namespace fe
{

	class brushWinding
	{
		friend class brushFace;
		friend class brush;
	protected:
	
		std::vector< vector3 >	mPoints;
		std::vector< vector2 >	mTexCoords;
	
	public:
	
		brushWinding ();
		brushWinding (const brushWinding &w);
		~brushWinding ();
	
		// properties
		vector3&	point (int iPoint) { return mPoints[iPoint]; }
		const vector3&	point (int iPoint) const { return mPoints[iPoint]; }
		int				numPoints (void) const { return (int)mPoints.size (); }
		vector2&	texCoord (int iPoint) { return mTexCoords[iPoint]; }
		const vector2&	texCoord (int iPoint) const { return mTexCoords[iPoint]; }
		void			baseForPlane (const plane &p);
		bool			clip (const plane &split, bool keepon);
	
	};
	
	class brushFaceTexdef
	{
	
	private:
		
		cStr			mName;
		int				mShift[2];
		int				mRotate;
		float			mScale[2];
		long			mContents;
		long			mFlags;
		long			mValue;
	
	public:
	
		brushFaceTexdef ();
		
		cStr& name (void) { return mName; }
		const cStr& name (void) const { return mName; }
		int* shift (void) { return mShift; }
		const int* shift (void) const { return mShift; }
		int& rotate (void) { return mRotate; }
		const int& rotate (void) const { return mRotate; }
		float* scale (void) { return mScale; }
		const float* scale (void) const { return mScale; }
		long& contents (void) { return mContents; }
		const long& contents (void) const { return mContents; }
		long& flags (void) { return mFlags; }
		const long& flags (void) const { return mFlags; }
		long& value (void) { return mValue; }
		const long& value (void) const { return mValue; }
	};
	
	class brushFace
	{
		friend class brush;
	protected:
	
		plane			mPlane;
		vector3		mPlanePts[3];
		brushWinding		mWinding;
		brushFaceTexdef	mTexdef;
		materialPtr		mpMtl;
	
	public:
	
		brushFace ();
		brushFace (const brushFace &f);
		~brushFace ();
	
		plane&			plane () { return mPlane; }
		const plane&	plane () const { return mPlane; }
		vector3&		planePt (uint iPoint) { assert (iPoint < 3); return mPlanePts[iPoint]; }
		const vector3&	planePt (uint iPoint) const { assert (iPoint < 3); return mPlanePts[iPoint]; }
		brushWinding&		winding () { return mWinding; }
		const brushWinding&	winding () const { return mWinding; }
		materialPtr		material () const { return mpMtl; }
		void				setMaterial (const materialPtr &mtl);
		brushFaceTexdef&	texdef () { return mTexdef; }
		void				textureVectors (float stFromXYZ[2][4]);
		vector2		emitTexCoords (const vector3 &pt);
		void				applyTexdef (void);
	
	};
	
	class brush
	{
	
	protected:
	
		std::vector< brushFace >	mFaces;
		vector3				mMins;
		vector3				mMaxs;
	
		bool	makeFaceWinding (int face);
		bool	makeFacePlane (int face);
	
	public:
	
		brush ();
		brush (const vector3 &mins, const vector3 &maxs);
		void	create (const vector3 &mins, const vector3 &maxs);
		brush (const brush &b);
		~brush ();
		void clear (void) { mFaces.clear (); }
	
		// edit
		void		buildWindings (bool snap = true);
		void		move (const vector3 &moveVec, bool snap = true);
		void		splitByFace (const brushFace &f, brush *&front, brush *&back);
		void		snapToGrid ();
		void		snapPlanePts ();
		void		rotate (const vector3 &angle, const vector3 &origin, bool build = true);
		void		flip (const vector3 &axis, const vector3 &origin);
		void		removeEmptyFaces ();
		virtual void		build (bool bSnap = true);
		void		buildTexturing (void);
	
		// primitives
		void		makeSided (int sides, int axis = 2); 	// default axis is z
		void		makeSidedCone (int sides);
		void		makeSidedSphere (int sides);
	
		// properties
		brushFace&	faces (int iFace) { return mFaces[iFace]; }
		const brushFace&	faces (int iFace) const { return mFaces[iFace]; }
		int				numFaces (void) const { return (int)mFaces.size (); }
		vector3&	mins () { return mMins; }
		vector3&	maxs () { return mMaxs; }
		void			addFace (const brushFace& f) { mFaces.push_back (f); }
	
		// csg
	//	void			csgAdd (std::vector<brush *> &outBrushList, brush *b);
		void			csgSubtract (std::vector<brush *> &outBrushList, brush *b);
	//	void			csgIntersection (std::vector<brush *> &outBrushList, brush *b);
	//	void			csgDifference (std::vector<brush *> &outBrushList, brush *b);
	};
	
}

#endif // __F_BRUSH_H
