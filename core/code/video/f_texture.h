/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_TEXTURE_H
#define __F_TEXTURE_H

#include "f_types.h"
#include "f_basetexture.h"
#include "f_parser.h"

namespace fe
{
	
	class textureData;
	
	class texture : public baseTexture
	{
	friend class textureData;
	
	private:
	
	
		smartPtr <textureData>		mpTextureData;
		ulong				mFlags;
	
	protected:
	
		texture (void);     // FIXME: this ctor is somehow being required by smartPtr
		void				createFromFile (void);
		~texture (void);
	
	public:
	
		// name syntax:
		// "name"
		// "empty:w:h:name"
		texture (const char *name);
		texture (charParser &parser, const char *name);
	
		virtual void				bind (int stage );
		ulong						getWidth (void) const;
		ulong						getHeight (void) const;
	
		smartPtr <textureData>				getTextureData (void) const;
	
		lockedRect				lockRect (uint lvl, rect *rc, bool readOnly);
		void						unlockRect (uint lvl);
		
		virtual int_ptr getObject (void) const;
	
	};
	
	typedef smartPtr <texture>	texturePtr;
	
}

#endif // __F_TEXTURE_H

