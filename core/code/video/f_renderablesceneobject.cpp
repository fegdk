/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_helpers.h"
#include "f_renderablesceneobject.h"

namespace fe
{

	renderableSceneObject::renderableSceneObject (sceneObject *parent)
		: sceneObject (parent)
	{
		mpDrawSurfs = NULL;
		mNumDrawSurfs = 0;
	}

	renderableSceneObject::~renderableSceneObject (void)
	{
		if (mpDrawSurfs)
		{
			delete[] mpDrawSurfs;
			mpDrawSurfs = NULL;
		}
	}

	bool renderableSceneObject::isTypeOf (int type) const
	{
		return sceneObject::isTypeOf (type) ? true : type == TypeId;
	}
	
	void renderableSceneObject::renderShadowVolume (bool withChildren) const
	{
		// stub method. for objects w/o shadow volumes
		if (withChildren)
		{
			for (size_t i = 0; i < mChildren.size (); i++)
			{
				if (mChildren[i]->isTypeOf (renderableSceneObject::TypeId))
					mChildren[i].dynamicCast <renderableSceneObject> ()->renderShadowVolume (withChildren);
			}
		}
	}
	
}
