#include "pch.h"
#include <stdio.h>
#include "NxPhysics.h"
#include "f_types.h"
#include "entclasses.h"
#include "f_model.h"

namespace fe {

NxPhysicsSDK *g_physx = NULL;
NxScene *g_physx_scene = NULL;

int
physx_init (void)
{
	if (!(g_physx = NxCreatePhysicsSDK (NX_PHYSICS_SDK_VERSION, NULL, NULL))) {
		sys_printf ("NxCreatePhysicsSDK error\n");
		return -1;
	}
	NxSceneDesc sceneDesc;
	sceneDesc.gravity = NxVec3(0, 0, -9.8f);
	if (!(g_physx_scene = g_physx->createScene (sceneDesc))) {
		sys_printf ("g_physx->createScene error\n");
		return -1;
	}

	// Set default material
    NxMaterial* defaultMaterial = g_physx_scene->getMaterialFromIndex(0);
    defaultMaterial->setRestitution(0.0f);
    defaultMaterial->setStaticFriction(0.5f);
    defaultMaterial->setDynamicFriction(0.5f);

	NxPlaneShapeDesc planeDesc;
	planeDesc.normal = NxVec3 (0, 0, 1);
	planeDesc.d = 0;
    NxActorDesc actorDesc;
    actorDesc.shapes.pushBack(&planeDesc);
    g_physx_scene->createActor(actorDesc);


	return 0;
}

int
physx_free (void)
{
	if (g_physx_scene) {
		if (g_physx) {
			g_physx->releaseScene (*g_physx_scene);
		}
		g_physx_scene = NULL;
	}
	if (g_physx) {
		g_physx->release ();
		g_physx = NULL;
	}
}

void
physx_add_entity (entPhysXObj *ent)
{
	// get bounding box from model
	sceneObject *mdl = ent->getModel ();
	aab3 b = mdl->getBoundingBox (true);
	b.mins += ent->getInitPos ();
	b.maxs += ent->getInitPos ();

	NxBodyDesc bodyDesc;
//	bodyDesc.angularDamping = 0.5f;
	bodyDesc.linearVelocity = NxVec3(0,0,0);
	NxBoxShapeDesc boxDesc;
	const vector3& pos = (b.maxs + b.mins) / 2;
	boxDesc.dimensions = NxVec3 ((b.maxs[0] - b.mins[0])/2, (b.maxs[1] - b.mins[1])/2, (b.maxs[2] - b.mins[2])/2);
	NxActorDesc actorDesc;
	actorDesc.shapes.pushBack(&boxDesc);
	actorDesc.body = &bodyDesc;
	actorDesc.density = 10.f;
	actorDesc.globalPose.t = NxVec3 (pos[0], pos[1], pos[2]);
	fprintf (stderr, "creating physx actor\n");
	NxActor *actor = g_physx_scene->createActor (actorDesc);
	fprintf (stderr, "actor is %x\n", actor);
	actor->userData = (void*)ent;
}

void
physx_tick (float t)
{
	g_physx_scene->simulate (t);
}

void
physx_fetch_results (void)
{
	if (!g_physx_scene)
		return;
	int nbActors = g_physx_scene->getNbActors();
    NxActor** actors = g_physx_scene->getActors();
    while(nbActors--)
	{
		NxActor* actor = *actors++;
        if (!actor->userData)
			continue;
		entPhysXObj *e = (entPhysXObj*)actor->userData;
		matrix4 m;
        actor->getGlobalPose().getColumnMajor44((float *)&m);
		matrix4 orig = e->getOrigMtx ();
		orig = orig * m;
		e->getModel()->setWorldMatrix (orig);
	}
	g_physx_scene->flushStream ();
	g_physx_scene->fetchResults (NX_RIGID_BODY_FINISHED, true);

}

}
