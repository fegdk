#ifndef __PHYSX_H
#define __PHYSX_H

namespace fe {

class entPhysXObj;

int
physx_init (void);

int
physx_free (void);

void
physx_add_entity (entPhysXObj *ent);

void
physx_tick (float t);

void
physx_fetch_results (void);

}

#endif // __PHYSX_H
