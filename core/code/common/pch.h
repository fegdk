/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __PCH_H
#define __PCH_H

#ifdef _WIN32
#define _WIN32_WINNT 0x0400
#endif


#if defined _DEBUG && defined _WIN32
#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#else
#include <stdlib.h>
#endif

#if 0
#ifndef _WIN32
#include "mstchar.h"
#else
#include <TCHAR.h>
#endif
#endif

//-----------------------------------
// misc msvc warning disables
//-----------------------------------

#ifdef _MSC_VER
#pragma warning (disable:4244)
//#pragma warning (disable:4305)
//#pragma warning (disable:4142)
//#pragma warning (disable:4018)
#pragma warning (disable:4786)
#endif


//-----------------------------------
// win32 includes
//-----------------------------------

#ifdef _WIN32
#include <windows.h>
/*#include <assert.h>
#include <mmsystem.h>
#include <objbase.h>*/
#endif

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <ctype.h>

#if 0
#define DIRECT3D_VERSION         0x0800
#include <d3d8.h>
#include <d3dx8.h>
#endif

#endif // __PCH_H

