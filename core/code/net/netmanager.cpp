/*
	fegdk: FE Game Development Kit
	Copyright (C) 2001-2008 Alexey "waker" Yakovenko

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Library General Public
	License as published by the Free Software Foundation; either
	version 2 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Library General Public License for more details.

	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Alexander Maltsev
	keltar@users.sourceforge.net
*/

#include "pch.h"
#include "netmanager.h"
#include "config.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>	// memset

#include <fcntl.h>

namespace fe
{

	netSocket::~netSocket (void)
	{
		close (mSocket);
	}

	int netSocket::send (const void *buf, uint32 len)
	{
		if (!mSend)
		{
			fprintf (stderr, "netSocket::send error: socket is recive-only\n");
			return -1;
		}
		return ::send (mSocket, buf, len, 0);
	}

	int netSocket::recive (void *buf, uint32 max_len, uint32 *sender_ip)
	{
		if (mSend)
		{
			fprintf (stderr, "netSocket::recive error: socket is send-only\n");
			return -1;
		}

		struct sockaddr_in s_in;
		memset (&s_in, 0, sizeof (struct sockaddr_in));
		socklen_t slen = sizeof (struct sockaddr_in);
		ssize_t rlen = ::recvfrom (mSocket, buf, max_len, 0, (struct sockaddr*)&s_in, &slen);
		if (sender_ip)
		{
			memcpy (sender_ip, &s_in.sin_addr.s_addr, sizeof (uint32));
		}
		return rlen;
	}


	uint32 netManager::resolveHostname (const char *hostname)
	{
		struct hostent *he = gethostbyname (hostname);
		return (he && he->h_length > 0) ? *(uint32*)he->h_addr : 0;
	}

	smartPtr <netSocket> netManager::createSendingSocket (uint32 ip, uint16 port)
	{
		netSocket::Socket sock = socket (PF_INET, SOCK_DGRAM, 0);
		struct sockaddr_in s_in;

		if (sock == -1)
		{
			fprintf (stderr, "netManager::createSocket failed: unable to open socket\n");
			return NULL;
		}

		memset (&s_in, 0, sizeof (struct sockaddr_in));
		s_in.sin_family = AF_INET;
		s_in.sin_port = htons (port);
		s_in.sin_addr.s_addr = ip;

		if (::connect (sock, (struct sockaddr*)&s_in, sizeof (s_in)) < 0)
		{
			// hmm, is UDP connect can fail?
			close (sock);
			fprintf (stderr, "netManager::createSendingSocket failed: unable to connect to %d.%d.%d.%d:%d\n", ip & 0xff, ip & 0x00ff, ip & 0x0000ff, ip & 0x000000ff, port);
			return NULL;
		}

		// disable blocking
		fcntl (sock, F_SETFL, O_NONBLOCK | fcntl (sock, F_GETFL));

		smartPtr <netSocket> ns (new netSocket ());
		ns->mSocket = sock;
		ns->mSend = true;
		ns->mHostAddress = ip;
		ns->mPort = port;
		mSockets.insert (ns);
		return ns;
	}

	smartPtr <netSocket> netManager::createReceivingSocket (uint16 port)
	{
		netSocket::Socket sock = socket (PF_INET, SOCK_DGRAM, 0);
		struct sockaddr_in s_in;

		if (sock == -1)
		{
			fprintf (stderr, "netManager::createSocket failed: unable to open socket\n");
			return NULL;
		}

		memset (&s_in, 0, sizeof (struct sockaddr_in));
		s_in.sin_family = AF_INET;
		s_in.sin_port = htons (port);
		s_in.sin_addr.s_addr = htonl (INADDR_ANY);

		if (::bind (sock, (struct sockaddr*)&s_in, sizeof (s_in)) < 0)
		{
			close (sock);
			fprintf (stderr, "netManager::createReceivingSocket failed: unable to bind socket to port %d\n", port);
			return NULL;
		}

		// disable blocking
		fcntl (sock, F_SETFL, O_NONBLOCK | fcntl (sock, F_GETFL));

		smartPtr <netSocket> ns (new netSocket ());
		ns->mSocket = sock;
		ns->mSend = false;
		ns->mHostAddress = 127 | 1 << 24;
		ns->mPort = port;
		mSockets.insert (ns);
		return ns;
	}
}
