/*
	fegdk: FE Game Development Kit
	Copyright (C) 2001-2008 Alexey "waker" Yakovenko

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Library General Public
	License as published by the Free Software Foundation; either
	version 2 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Library General Public License for more details.

	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Alexander Maltsev
	keltar@users.sourceforge.net
*/

#ifndef __F_NETMANAGER_H
#define __F_NETMANAGER_H

#include "f_baseobject.h"
#include "f_types.h"
#include <set>

namespace fe
{

	class FE_API netSocket : public baseObject
	{
		friend class netManager;

	protected:
		typedef int Socket;

		Socket mSocket;
		bool mSend;	// true if this is sending socket
		uint32 mHostAddress;	// ip address which socket connected with
		uint16 mPort;

		netSocket (void) {}
		~netSocket (void);

	public:
		uint32 getHostAddress (void) const { return mHostAddress; }
		uint16 getPort (void) const { return mPort; }

		int send (const void *buf, uint32 len);
		int recive (void *buf, uint32 max_len, uint32 *sender_ip = NULL);
	};

	class FE_API netManager : public baseObject
	{
	protected:
		std::set <smartPtr <netSocket> > mSockets;

	public:
		netManager (void) {}
		~netManager (void) {}

		uint32 resolveHostname (const char *hostname);

		smartPtr <netSocket> createSendingSocket (uint32 ip, uint16 port);
		smartPtr <netSocket> createReceivingSocket (uint16 port);
	};

}

#endif
