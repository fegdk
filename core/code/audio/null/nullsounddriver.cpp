/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#include "pch.h"
#include "nullsounddriver.h"
#include "mmanager.h"

namespace fe
{

nullSoundBuffer::nullSoundBuffer (nullSoundDriver *driver, int freq, int sample_rate, bool stereo, size_t size, bool streaming) : baseSoundBuffer (driver, freq, sample_rate, stereo, size, streaming)
{
}

nullSoundBuffer::~nullSoundBuffer (void)
{
}

void nullSoundBuffer::write (void *data, int size, int fmt, int sample_rate)
{
}

void nullSoundBuffer::setLooping (bool onOff)
{
}

void nullSoundBuffer::play (void)
{
}

void nullSoundBuffer::stop (void)
{
}

int nullSoundBuffer::getChannelsCount (void) const
{
	return 0;
}

bool nullSoundBuffer::isPlaying (void) const
{
	return false;
}

void nullSoundBuffer::setFeeder (genericCallback *callback)
{
}

void nullSoundBuffer::update (void)
{
}

void nullSoundBuffer::setVolume (float vol)
{
}

void nullSoundBuffer::setMaxSources (int num)
{
}

void nullSoundBuffer::pause (bool onoff)
{
}

int nullSoundBuffer::getNumBuffers (void) const
{
	return 0;
}


nullSoundDriver::nullSoundDriver (void)
{
	mNumPools = 1;
	mpPools[0] = new pool(sizeof(nullSoundBuffer), mPoolsSize);
}

nullSoundDriver::~nullSoundDriver (void)
{
	for(int i = 0; i != mNumPools; ++i) {
		delete mpPools[i];
	}
}
	
void nullSoundDriver::update (void)
{
}

nullSoundBuffer* nullSoundDriver::createBuffer (int freq, int sample_rate, bool stereo, size_t size, bool streaming)
{
	return new nullSoundBuffer(this, freq, sample_rate, stereo, size, streaming);
}

void nullSoundDriver::createPool ()
{
	mpPools[mNumPools++] = new pool(sizeof(nullSoundDriver), mPoolsSize);
}

}
