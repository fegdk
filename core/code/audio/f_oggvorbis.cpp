/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_oggvorbis.h"
#include "f_helpers.h"
#include "basesounddriver.h"
#include "f_engine.h"
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>
#include "f_filesystem.h"
#include "cvars.h"

namespace fe
{

	static size_t readFile (void *ptr, size_t size, size_t nmemb, void *ds);
	static int seekFile (void *ds, ogg_int64_t offset, int whence);
	static int closeFile (void *ds);
	static long tellFile (void *ds);

	oggFeeder::oggFeeder (vorbisFile *file)
	{
		mpFile = file;
	}
	void oggFeeder::call (void *data)
	{
		mpFile->update ();
	}
	
	vorbisFile::vorbisFile (const char *name)
		: baseSound (name)
		, mFeeder (this)
	{
		mName = name;
	
		mpBuffer = NULL;
		mpPCMOut = NULL;
		mbEOF = false;
		mCurrentSection = -1;
		mpFile = NULL;
		mHaveBytes = 0;
		mbLooping = false;
		mbPaused = false;
		mVolume = 1;
		mpVorbisFile = NULL;
		mpVI = NULL;
	
		mpFile = g_engine->getFileSystem ()->openFile (mName, FS_MODE_READ);
	
		fprintf (stderr, "loading %s from disk....\n", this->name ());
	
	
		if (mpFile)
		{
			mpVorbisFile = new OggVorbis_File;
			ov_callbacks cb;
			cb.read_func = readFile;
			cb.seek_func = seekFile;
			cb.close_func = closeFile;
			cb.tell_func = tellFile;
			ov_open_callbacks (mpFile, mpVorbisFile, NULL, 0, cb);
			mpVI = ov_info (mpVorbisFile, -1);
			int filesize = ov_pcm_total (mpVorbisFile, -1) * mpVI->channels * 2;
		    
			smartPtr <baseSoundDriver> soundDriver = g_engine->getSoundDriver ();
			if (filesize < 2000000)
			{
				mbStreaming = false;
				mpBuffer = soundDriver->createBuffer (mpVI->rate, 16, mpVI->channels == 1 ? false : true, filesize, false);
				mpPCMOut = new char[filesize];
				mHaveBytes = 0;
				while (mHaveBytes < filesize)
				{
					long ret=ov_read (mpVorbisFile, mpPCMOut + mHaveBytes, filesize - mHaveBytes, 0, 2, 1, &mCurrentSection);
					if (!ret)
						break;
					else
						mHaveBytes += ret;
				}
				mpBuffer->write (mpPCMOut, filesize, baseSoundDriver::fmt_stereo16, mpVI->rate);
				delete[] mpPCMOut;
				mpPCMOut = NULL;
				ov_clear (mpVorbisFile);
				delete mpVorbisFile;
				mpVorbisFile = NULL;
			}
			else
			{
				mbStreaming = true;
				mpPCMOut = new char[snd_streamchunksize->ivalue];
				mpBuffer = soundDriver->createBuffer (mpVI->rate, 16, mpVI->channels == 1 ? false : true, snd_streamchunksize->ivalue, true);
				mpBuffer->setFeeder (&mFeeder);
			}
	
			ov_clear (mpVorbisFile);
			mpFile = NULL;
		}
	}
	
	vorbisFile::~vorbisFile (void)
	{
		stop ();
		if (mpBuffer)
		{
			mpBuffer->stop ();
			mpBuffer->release ();
			mpBuffer = NULL;
		}
		if (mpPCMOut)
		{
			delete[] mpPCMOut;
			mpPCMOut = NULL;
		}
		ov_clear (mpVorbisFile);
		delete mpVorbisFile;
		mpVorbisFile = NULL;
		mpVI = NULL;
	}
	
	void vorbisFile::play (bool looping)
	{
		if (!mpBuffer)
			return;
	
		if (mpBuffer)
			mpBuffer->setVolume (mVolume);
		mbLooping = looping;
		if (mbStreaming)
		{
			if (isPlaying ())
			{
				// rewind & unpause
				ov_raw_seek (mpVorbisFile, 0);
				mbEOF = false;
			}
			else
				open ();
		}
		else
			mpBuffer->setLooping (mbLooping);
		
		mpBuffer->play ();
	}
	
	void vorbisFile::stop (void)
	{
		if (mpBuffer)
		{
			mpBuffer->stop ();
		}
		ov_clear (mpVorbisFile);
		mpFile = NULL;
	
		mHaveBytes = 0;
	}
	
	int vorbisFile::getChannelsCount (void) const
	{
		return mpBuffer->getChannelsCount ();
	}
	
	
	void vorbisFile::pause (bool onoff)
	{
		if (mpBuffer)
			mpBuffer->pause (onoff);
	}
	
	void vorbisFile::setVolume (float volume)
	{
		mVolume = volume;
		if (mpBuffer)
			mpBuffer->setVolume (volume);
	}
	
	bool vorbisFile::isPlaying (void) const
	{
		return (mpFile && ! (mbEOF && !mbLooping))? true : false;
	}
	
	void vorbisFile::open ()
	{
		smartPtr <baseSoundDriver> baseSoundDriver = g_engine->getSoundDriver ();
		mbEOF = false;
		mCurrentSection = -1;
		mHaveBytes = 0;
		// sanity check
		if (mpFile)
		{
			mpFile->close ();
			mpFile = NULL;
		}
		mpFile = g_engine->getFileSystem ()->openFile (mName, FS_MODE_READ);
		ov_callbacks cb;
		cb.read_func = readFile;
		cb.seek_func = seekFile;
		cb.close_func = closeFile;
		cb.tell_func = tellFile;
		ov_open_callbacks (mpFile, mpVorbisFile, NULL, 0, cb);
		mpVI = ov_info (mpVorbisFile, -1);
		mpBuffer->setFeeder (&mFeeder);
	}
	
	void vorbisFile::update (void)
	{
		if (!mpFile)
			return;
		for (;;)
		{
			if (!mpBuffer)
				break;
	
			if (mbPaused)
				break;
	
			while (!mbEOF && mHaveBytes < snd_streamchunksize->ivalue)
			{
				// read ogg
				long ret=ov_read (mpVorbisFile, mpPCMOut + mHaveBytes, snd_streamchunksize->ivalue - mHaveBytes, 0, 2, 1, &mCurrentSection);
				if (ret == 0)
				{
					if (mbLooping)
					{
						// rewind
						ov_raw_seek (mpVorbisFile, 0);
						mbEOF = false;
					}
					else
						mbEOF = true;
				}
				else if (ret < 0)
				{
					// error
				}
				else
				{
					mHaveBytes += ret;
				}
			}
	
			if (mbEOF && !mbLooping)
			{
				stop ();
				return;
			}
	
	
			if (mHaveBytes || mbEOF)
			{
				if (mbEOF && !mHaveBytes)
				{
					break;
				}
	
				bool updated = false;
	
				if (mHaveBytes < snd_streamchunksize->ivalue)
				{
					memset (mpPCMOut + snd_streamchunksize->ivalue, 0, snd_streamchunksize->ivalue - mHaveBytes);
					mHaveBytes = snd_streamchunksize->ivalue;
				}
				int copybytes = min (snd_streamchunksize->ivalue, mHaveBytes);
	
				mpBuffer->write (mpPCMOut, copybytes, baseSoundDriver::fmt_stereo16, mpVI->rate);
				
				updated = true;
	
				if (updated && mHaveBytes > snd_streamchunksize->ivalue)
				{
					memmove (mpPCMOut, &mpPCMOut[snd_streamchunksize->ivalue], mHaveBytes - snd_streamchunksize->ivalue);
					mHaveBytes -= snd_streamchunksize->ivalue;
				}
				else if (updated)
					mHaveBytes = 0;
			}
			break;
		}
	}
	
	smartPtr <baseSound>	vorbisFile::duplicate (void)
	{
		return new vorbisFile (name ());
	}
	
	static size_t readFile (void *ptr, size_t size, size_t nmemb, void *ds)
	{
		return (size_t) ((file *)ds)->read (ptr, size * nmemb);
	}
	
	static int seekFile (void *ds, ogg_int64_t offset, int whence)
	{
		return (size_t) ((file *)ds)->seek (offset, whence);
	}
	
	static int closeFile (void *ds)
	{
		 ((file *)ds)->close ();
		return 0;
	}
	
	static long tellFile (void *ds)
	{
		return ( (file *)ds)->tell ();
	}
	
}

// old log

// Revision 1.12  2003/12/28 00:08:53  wkr
// major (forced) commit, important one
// a lot of improvements is there to make the engine portable
// fontft and console are fully portable now (not using d3d and win32 api anymore)
// some new fast portable drawing routines
// MANY files were moved to os-specific directories
// mdlview is portable too
// editor is not working (i.e. completely broken due to major codebase refactoring)
// renderer interface is much more clear now
// application has acces to video-driver properties w/o even knowing which OS is running
// see updates to code/engine/docs/progress (late december of 2003) for more info
// tired of typin' here
//
// Revision 1.11  2003/12/19 19:55:14  waker
// minor adjustment to reflect filesystem constant changes
//
// Revision 1.10  2003/11/09 10:51:29  waker
// fixed bug with non-looping playback
//
// Revision 1.8  2003/06/18 09:58:42  waker
// fixed volume control (btw, seems working, but doesn't look right)
//
// Revision 1.7  2003/06/09 09:18:01  waker
// fixed non-looping ogg-vorbis files endplayback event
//
// Revision 1.6  2003/06/08 15:59:38  waker
// removed redundant unicode stuff; changed most of 'const str &' occurances to 'const char *' due to performance reasons
//
// Revision 1.5  2003/04/17 10:45:28  waker
// removed FE_USE_ENGINE, added FE_NO_FILESYSTEM, removed file f_string.cpp, fixed all stuff to compile
//
// Revision 1.4  2002/12/10 11:26:55  waker
// oggvorbis support fixed (transferred from LD)
//

