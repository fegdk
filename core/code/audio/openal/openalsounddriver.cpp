/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "openalsounddriver.h"
#include "f_helpers.h"
#include "f_engine.h"
#include "f_console.h"
#include <AL/al.h>
#include <AL/alc.h>
#include "cvars.h"
#include "mmanager.h"

namespace fe
{
	
	static int
	dumpALErrors (const char *src)
	{
		int res = 0;
		ALenum err;
		for (;;)
		{
			err = alGetError ();
			if (err != AL_NO_ERROR)
			{
				fprintf (stderr, "OPENAL ERROR: %s: %X\n", src ? src : "<unknown>", err);
				res = 1;
			}
			else
				break;
		}
		return res;
	}
	
	void openalSoundBuffer::addBuffer (void)
	{
		buffer_t *buffer = new buffer_t;
		alGenBuffers (1, &buffer->id);
		buffer->next = mpBuffers;
		mpBuffers = buffer;
	}

	void openalSoundBuffer::freeBuffers (void)
	{
		buffer_t *next;
		for (buffer_t *buf = mpBuffers; buf; buf = next)
		{
			next = buf->next;
			alDeleteBuffers (1, &buf->id);
			delete buf;
		}
		mpBuffers = NULL;
		mpCurrentBuffer = NULL;
	}

	openalSoundBuffer::openalSoundBuffer (openalSoundDriver *driver, int freq, int sample_rate, bool stereo, size_t size, bool streaming) : baseSoundBuffer (driver, freq, sample_rate, stereo, size, streaming)
	{
		mbPaused = false;
		mpDriver = driver;
		mNumSources = 0;
		mNumBuffers = streaming ? snd_numstreamchunks->ivalue : 1;
		if (streaming && mNumBuffers < 2)
		{
			sys_error ("snd_numstreamchunks < 2");
		}
		
		// append required number of buffers
		mpBuffers = NULL;

		for (int i = 0; i < mNumBuffers; i++)
		{
			addBuffer ();
		}
		mpCurrentBuffer = mpBuffers;
		mbLooping = false;
		mpFeeder = NULL;
		mVolume = 1;
		mNumSources = 1;
		mSourceIds[0] = 0;
	
		mpDriver->addBuffer (this);
	}
	
	openalSoundBuffer::~openalSoundBuffer (void)
	{
		while (isPlaying ())
			stop ();
		freeBuffers ();
		mpDriver->removeBuffer (this);
	}
	
	void openalSoundBuffer::setMaxSources (int num)
	{
		if (mNumBuffers == 1)
		{
			size_t prev = mNumSources;
			mNumSources = num;
			for (size_t i = prev; i < mNumSources; i++)
				mSourceIds[i] = 0;
		}
	}
	
	void openalSoundBuffer::write (void *data, int size, int fmt, int sample_rate)
	{
		// writes to next buffer
		int formats[] = {
			AL_FORMAT_MONO8,
			AL_FORMAT_MONO16,
			AL_FORMAT_STEREO8,
			AL_FORMAT_STEREO16,
		};
		alBufferData(mpCurrentBuffer->id, formats[fmt],data, size, sample_rate);
	}
	
	void openalSoundBuffer::setLooping (bool onOff)
	{
		mbLooping = onOff;
	}
	
	int num_sounds_playing = 0;

	openalSoundBuffer::buffer_t *openalSoundBuffer::getNextBuffer (openalSoundBuffer::buffer_t *buffer)
	{
		if (buffer->next)
			return buffer->next;
		return mpBuffers;
	}
	
	void openalSoundBuffer::play (void)
	{
		// get 1st free slot
		int src;
		for (src = 0; src < mNumSources; src++)
		{
	        if (mSourceIds[src] == 0)
				break;
		}
	
		if (src == mNumSources)
		{
	        // no free slots, don't play
			return;
		}
	
		// gen source name
		alGenSources (1, &mSourceIds[src]);
		if (mNumBuffers == 1)	// not streamed
			alSourcei (mSourceIds[src], AL_LOOPING, mbLooping ? AL_TRUE : AL_FALSE);
		// feed buffer
		if (mNumBuffers != 1)
		{
			// load start into buffers
			for (int i = 0; i < mNumBuffers; i++)
			{
				mpFeeder->call (this);
				alSourceQueueBuffers(mSourceIds[src], 1, (ALuint*)&mpCurrentBuffer->id);
				mpCurrentBuffer = getNextBuffer (mpCurrentBuffer);
			}
		}
		else
		{
			alSourcei (mSourceIds[src], AL_BUFFER, mpCurrentBuffer->id);
		}
		
		alSourcef (mSourceIds[src], AL_GAIN, mVolume);
	    alSourcePlay(mSourceIds[src]);
		num_sounds_playing++;
		if (dumpALErrors ("sb::play"))
		{
			fprintf (stderr, "OPENAL: mSourceId = %d\n", mSourceIds[src]);
			fprintf (stderr, "OPENAL: buffers: ");
			for (buffer_t *b = mpBuffers; b; b = b->next)
				fprintf (stderr, "%d ", b->id);
			fprintf (stderr, "\n");
		}
	}
	
	void openalSoundBuffer::stop ()
	{
		// stop 1st playing copy, shift source ids
		if (mSourceIds[0] != 0/* && isPlaying ()*/)
		{
			alSourceStop (mSourceIds[0]);
			if (mpFeeder)
			{
				ALuint buffer;
				int nbuf;
				alGetSourcei(mSourceIds[0], AL_BUFFERS_PROCESSED, &nbuf);
				while (nbuf--)
				{
					alSourceUnqueueBuffers(mSourceIds[0], 1, &buffer);
					dumpALErrors ("sb::stop unqueued buffers");
				}
			}
			alDeleteSources (1, &mSourceIds[0]);
			num_sounds_playing--;
			mSourceIds[0] = 0;
	
			// shift buffer
			for (int i = 0; i < mNumSources-1; i++)
				mSourceIds[i] = mSourceIds[i+1];
			mSourceIds[mNumSources-1] = 0;
		}
		if (dumpALErrors ("sb::stop"))
		{
		}
	}
	
	int openalSoundBuffer::getChannelsCount (void) const
	{
		return mSourceIds[0] == 0 ? 0 : mNumSources;
	}
	
	bool openalSoundBuffer::isPlaying (void) const
	{
		int state = AL_STOPPED;
		if (mSourceIds[0] != 0)
			alGetSourcei(mSourceIds[0], AL_SOURCE_STATE, &state);
		dumpALErrors ("sb::isPlaying");
		return state == AL_PLAYING;
	}
	
	void openalSoundBuffer::setFeeder  (genericCallback *callback)
	{
		mpFeeder = callback;
	}
	
	void openalSoundBuffer::update (void)
	{
		assert (mNumSources);
		if (mNumBuffers == 1 && !isPlaying () && !mbPaused)
		{
			if (mSourceIds[0] != 0)
			{
				alDeleteSources (1, &mSourceIds[0]);
				for (int i = 0; i < mNumSources-1; i++)
					mSourceIds[i] = mSourceIds[i+1];
				mSourceIds[mNumSources-1] = 0;
				num_sounds_playing--;
			}
			return;
		}
		// streaming sounds can not be duplicated, so mNumSources==1 is always true
		if (mpFeeder && mSourceIds[0] != 0)
		{
			int processed = 0;
			alGetSourcei(mSourceIds[0], AL_BUFFERS_PROCESSED, &processed);
			int stopped;
			alGetSourcei(mSourceIds[0], AL_SOURCE_STATE, &stopped);
			if (stopped == AL_STOPPED)
			{
				processed = 0;
				// unqueue all buffers
				mpCurrentBuffer = mpBuffers;
				int i;
				for (i = 0; i < mNumBuffers; i++)
				{
					ALuint b = mpCurrentBuffer->id;
					alSourceUnqueueBuffers(mSourceIds[0], 1, &b);
					if (dumpALErrors ("sb::update unqueue buffers"))
						fprintf (stderr, "i=%d, id: %d\n", i, mSourceIds[i]);
					mpCurrentBuffer = getNextBuffer (mpCurrentBuffer);
				}
				// restart
				for (i = 0; i < mNumBuffers; i++)
				{
					mpFeeder->call (this);
					alSourceQueueBuffers(mSourceIds[0], 1, (ALuint*)&mpCurrentBuffer->id);
					if (dumpALErrors ("sb::update queue buffers"))
						fprintf (stderr, "i=%d, id: %d\n", i, mSourceIds[i]);
					mpCurrentBuffer = getNextBuffer (mpCurrentBuffer);
				}

				alSourcePlay(mSourceIds[0]);
				dumpALErrors ("sb::update sourceplay");
			}
	
			while (processed--)
			{
				ALuint buffer;
		        
				alSourceUnqueueBuffers(mSourceIds[0], 1, &buffer);
				dumpALErrors ("sb::update stream unqueue buffer");
				mpFeeder->call (this);
				if (mSourceIds[0] == 0)
					break; // stopped
				alSourceQueueBuffers(mSourceIds[0], 1, &buffer);
				dumpALErrors ("sb::update stream queue buffer");
				mpCurrentBuffer = getNextBuffer (mpCurrentBuffer);
			}
		}
		dumpALErrors ("sb::update");
	}
	
	void openalSoundBuffer::setVolume (float vol)
	{
		mVolume = vol;
		for (size_t i = 0; i < mNumSources; i++)
		{
			if (mSourceIds[i] != 0)
			{
				alSourcef (mSourceIds[i], AL_GAIN, mVolume);
			}
		}
		dumpALErrors ("sb::setVolume");
	}
	
	void openalSoundBuffer::pause (bool onoff)
	{
		if (onoff)
		{
			for (size_t i = 0; i < mNumSources; i++)
			{
				if (mSourceIds[i] != 0)
				{
					alSourcePause (mSourceIds[i]);
				}
			}
			mbPaused = true;
		}
		else
		{
			for (size_t i = 0; i < mNumSources; i++)
			{
				if (mSourceIds[i] != 0)
				{
					alSourcePlay (mSourceIds[i]);
				}
			}
			mbPaused = false;
		}
	}

	int openalSoundBuffer::getNumBuffers () const
	{
		return mNumBuffers;
	}
	
	openalSoundDriver::openalSoundDriver (void)
	{
		snd_numstreamchunks = g_engine->getCVarManager ()->get ("snd_numstreamchunks", "16", CVAR_READONLY);
		snd_maxdupsources = g_engine->getCVarManager ()->get ("snd_maxdupsources", "3", CVAR_READONLY);
		snd_maxnumbuffers = g_engine->getCVarManager ()->get ("snd_maxnumbuffers", "1000", CVAR_READONLY);
		snd_streamchunksize = g_engine->getCVarManager ()->get ("snd_streamchunksize", "10000", CVAR_READONLY);
		ALCcontext *Context;
		ALCdevice *Device;
		fprintf (stderr, "alcOpenDevice...\n");
		Device=alcOpenDevice(NULL);
		fprintf (stderr, "alcCreateContext...\n");
		Context=alcCreateContext(Device,NULL);
		fprintf (stderr, "alcMakeContextCurrent (Context)...\n");
		alcMakeContextCurrent(Context);

		mMaxBuffers = snd_maxnumbuffers->ivalue;
		mpBuffers = new openalSoundBuffer *[mMaxBuffers];
		mNumBuffers = 0;
		memset (mpBuffers, 0, mMaxBuffers * sizeof (openalSoundBuffer *));

		mNumPools = 1;
		mpPools[0] = new pool(sizeof(openalSoundBuffer), mPoolsSize);
	}
	
	openalSoundDriver::~openalSoundDriver (void)
	{
		fprintf (stderr, "cleaning sound driver...\n");
		for (uint32 i = 0; i != mNumPools; ++i)
		{
			delete mpPools[i];
		}
		mNumPools = 0;
		if (mpBuffers)
		{
			delete[] mpBuffers;
			mpBuffers = NULL;
		}
		if (mNumBuffers)
		{
			fprintf (stderr, "WARNING: sound driver still holds %d buffers!\n", mNumBuffers);
		}

		ALCcontext *Context;
		ALCdevice *Device;
		fprintf (stderr, "getting current context...\n");
		Context=alcGetCurrentContext();
		if (Context) {
			fprintf (stderr, "getting current device...\n");
			Device=alcGetContextsDevice(Context);
			fprintf (stderr, "setting current context to NULL...\n");
			alcMakeContextCurrent(NULL);
			fprintf (stderr, "cleaning context...\n");
			alcDestroyContext(Context);
			fprintf (stderr, "cleaning device...\n");
			alcCloseDevice(Device);
			fprintf (stderr, "sound driver clean!\n");
		}
	}
	
	openalSoundBuffer* openalSoundDriver::createBuffer (int freq, int sample_rate, bool stereo, size_t size, bool streaming)
	{
		openalSoundBuffer *buf = new openalSoundBuffer (this, freq, sample_rate, stereo, size, streaming);
		buf->setMaxSources (snd_maxdupsources->ivalue);
		return buf;
	}
	
	void openalSoundDriver::update (void)
	{
		for (size_t i = 0; i < mNumBuffers; i++)
			mpBuffers[i]->update ();
	}
	
	void openalSoundDriver::addBuffer (openalSoundBuffer *buf)
	{
		if (mNumBuffers >= mMaxBuffers)
		{
			fprintf (stderr, "ERROR: sound system buffer list exceeded.\n");
			return;
		}
		mpBuffers[mNumBuffers++] = buf;
	}
	
	void openalSoundDriver::removeBuffer (openalSoundBuffer *buf)
	{
		int i;
		bool removed = false;
		for (i = 0; i < mNumBuffers; i++)
		{
			if (mpBuffers[i] == buf)
			{
				if (i != mNumBuffers-1)
				{
					mpBuffers[i] = mpBuffers[mNumBuffers-1];
				}
				mNumBuffers--;
				removed = true;
				break;
			}
		}
		if (!removed)
		{
			fprintf (stderr, "WARNING: sound system attempted to remove buffer which is not in list.\n");
		}
	}

	void openalSoundDriver::createPool ()
	{
		mpPools[mNumPools++] = new pool(sizeof(openalSoundDriver), mPoolsSize);
	}
	
}
