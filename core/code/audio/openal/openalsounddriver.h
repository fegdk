/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#ifndef __F_OPENALSOUNDDRIVER_H
#define __F_OPENALSOUNDDRIVER_H

#include "basesounddriver.h"

namespace fe
{

	class openalSoundDriverData;
	class openalSoundDriver;
	
	const int AL_MAX_SOURCES_PER_BUFFER = 16;

	class FE_API openalSoundBuffer : public baseSoundBuffer
	{
	private:
		bool mbPaused;

		uint mSourceIds[AL_MAX_SOURCES_PER_BUFFER];
		int mNumSources;
		int			mNumBuffers;
		struct buffer_t
		{
			uint id;
			buffer_t *next;
		};
		buffer_t*	mpBuffers;
		buffer_t*	mpCurrentBuffer;
		bool		mbLooping;
		genericCallback	*mpFeeder;
		openalSoundDriver*	mpDriver;
		float		mVolume;
		
		void addBuffer (void);
		void freeBuffers (void);
		buffer_t *getNextBuffer (buffer_t *buffer);
	public:
		openalSoundBuffer (openalSoundDriver *driver, int freq, int sample_rate, bool stereo, size_t size, bool streaming);
		~openalSoundBuffer (void);
	
		void write (void *data, int size, int fmt, int sample_rate);
		void setLooping (bool onOff);
		void play (void);
		void stop (void);
		int getChannelsCount (void) const;
		bool isPlaying (void) const;
		void setFeeder (genericCallback *callback);
		void update (void);
		void setVolume (float vol);
		void setMaxSources (int num);
		void pause (bool onoff);
		int getNumBuffers (void) const;
	};
	
	class FE_API openalSoundDriver : public baseSoundDriver
	{
		friend class openalSoundBuffer;
	private:
		openalSoundDriverData *mpData;
		openalSoundBuffer** mpBuffers;
		int mMaxBuffers;
		int mNumBuffers;
	
	protected:
	
		void addBuffer (openalSoundBuffer *buf);
		void removeBuffer (openalSoundBuffer *buf);
		
	public:
		openalSoundDriver (void);
		~openalSoundDriver (void);
	
		void	update (void);
//		openalSoundDriverData *getData (void);
		openalSoundBuffer* createBuffer (int freq, int sample_rate, bool stereo, size_t size, bool streaming);

	protected:
		void createPool ();
	};
	
}

#endif // __F_OPENALSOUNDDRIVER_H
