/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

// NOTE: libmikmod doesn't support playing multiple modules simultaneously
// it also forces us to use lot's of crappy global variables, etc, and hope we don't break anything

#include "pch.h"
#include "f_modmusic.h"
#include "basesounddriver.h"
#include "f_engine.h"
#include "f_filesystem.h"
#include "f_error.h"
#include <mikmod.h>

namespace fe
{
	
	static baseSoundBuffer*	fe_mikmod_buffer = NULL;
	// FIXME: pcm buffer should be shared for all stream sounds
	static const int mod_buffer_size = 10000;
	
	// mikmod fe driver
	static BOOL FE_IsThere(void)
	{
		return 1;
	}
	
	static BOOL FE_Init(void)
	{
		return VC_Init();
	}
	
	static void FE_Exit(void)
	{
		VC_Exit();
	}
	
	static void FE_Update(void)
	{
		ULONG done;
	
	/*	while (buffersout<NUMBUFFERS) {
			done=VC_WriteBytes(buffer[nextbuffer],buffersize);
			if (!done) break;
			header[nextbuffer].dwBufferLength=done;
			waveOutWrite(hwaveout,&header[nextbuffer],sizeof(WAVEHDR));
			if (++nextbuffer>=NUMBUFFERS) nextbuffer%=NUMBUFFERS;
			++buffersout;
		}*/
		SBYTE buf[mod_buffer_size];
		done=VC_WriteBytes(buf,mod_buffer_size);
	
		
		if (done < mod_buffer_size)
		{
			fprintf (stderr, "mikmod VC_WriteBytes returned %d\n", done);
		}
		
		if (done > 0)
		{
			fe_mikmod_buffer->write (buf, done, baseSoundDriver::fmt_stereo16, 44100);
		}
	}
	
	static void FE_PlayStop(void)
	{
		fprintf (stderr, "mikmod called PlayStop\n");
		VC_PlayStop();
	}
	
	static MDRIVER drv_fe={
		NULL,
		(char*)"fe mod audio",
		(char*)"fe mikmod audio driver v0.1",
		0,255,
		(char*)"fedrv",
	
		NULL,
		FE_IsThere,
		VC_SampleLoad,
		VC_SampleUnload,
		VC_SampleSpace,
		VC_SampleLength,
		FE_Init,
		FE_Exit,
		NULL,
		VC_SetNumVoices,
		VC_PlayStart,
		FE_PlayStop,
		FE_Update,
		NULL,
		VC_VoiceSetVolume,
		VC_VoiceGetVolume,
		VC_VoiceSetFrequency,
		VC_VoiceGetFrequency,
		VC_VoiceSetPanning,
		VC_VoiceGetPanning,
		VC_VoicePlay,
		VC_VoiceStop,
		VC_VoiceStopped,
		VC_VoiceGetPosition,
		VC_VoiceRealVolume
	};
	
	modFeeder::modFeeder (modMusic *file)
	{
		mpFile = file;
	}
	
	void modFeeder::call (void *data)
	{
		mpFile->update ();
	}
	
	struct mikmodMREADER : public MREADER
	{
	private:
		file *f;
	public:		
		mikmodMREADER (const char *fname)
		{
			f = g_engine->getFileSystem ()->openFile (fname, FS_MODE_READ);
			if (!f)
				sys_error ("failed to open file %s", fname);
		
			Seek = seek;
			Tell = tell;
			Read = read;
			Get = get;
			Eof = eof;		
		}
		~mikmodMREADER (void)
		{
			f->close ();
		}
		static BOOL seek (MREADER *reader, long offset, int whence)
		{
			return ((mikmodMREADER*)reader)->f->seek (offset, whence);
		}
		static long tell (MREADER *reader)
		{
			((mikmodMREADER*)reader)->f->tell ();
	
		}
		static BOOL read (MREADER *reader, void *buffer, size_t sz)
		{
			return ((mikmodMREADER*)reader)->f->read (buffer, sz);
		}
		static int get (MREADER *reader)
		{
			char c;
			((mikmodMREADER*)reader)->f->read (&c, 1);
			return c;
		}
		static BOOL eof (MREADER *reader)
		{
			return ((mikmodMREADER*)reader)->f->tell () == ((mikmodMREADER*)reader)->f->getSize ();
		}
	};
	
	modMusic::modMusic (const char *fname)
		: baseSound (fname)
		, mFeeder (this)
	{
		mVolume = 1;
		MikMod_RegisterAllDrivers();
		MikMod_RegisterAllLoaders();
		MikMod_RegisterDriver (&drv_fe);
		int drv = MikMod_DriverFromAlias ((char*)"fedrv");
		md_device = drv;
		MikMod_Init((char*)"");
		mikmodMREADER mreader (fname);
		mpModule = Player_LoadGeneric (&mreader, 64, 0);
		mpBuffer = g_engine->getSoundDriver ()->createBuffer (44100, 16, true, mod_buffer_size, true);
		mpBuffer->setFeeder (&mFeeder);
		
		fe_mikmod_buffer = mpBuffer;
	}
	
	modMusic::~modMusic (void)
	{
		Player_Free(mpModule);
		mpBuffer->release ();
		MikMod_Exit();
	}
	
	void modMusic::play (bool looping)
	{
		mpBuffer->setVolume (mVolume);
		if (Player_Paused ())
			Player_TogglePause ();
		mpModule->wrap = looping ? 1 : 0;
		mpModule->loop = looping ? 0 : 1;
		Player_Start(mpModule);
		mpBuffer->play ();
	}
	
	void modMusic::stop (void)
	{
		Player_SetPosition (0);
		mpBuffer->stop ();
	}
	
	void modMusic::update (void)
	{
		if (Player_Active())
			MikMod_Update();
	}
	
	void modMusic::pause (bool onoff)
	{
		if (mpBuffer)
		{
			if (!Player_Paused ())
				Player_TogglePause ();
			mpBuffer->stop ();
		}
	}
	
	void modMusic::setVolume (float volume)
	{
		mVolume = volume;
		if (mpBuffer)
			mpBuffer->setVolume (volume);
	}
	
	bool modMusic::isPlaying (void) const
	{
		if (Player_Active () && !Player_Paused ())
			return true;
		return false;
	}
	
	smartPtr <baseSound>	modMusic::duplicate (void)
	{
		return new modMusic (name ());
	}
	
}

