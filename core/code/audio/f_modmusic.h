/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_MODMUSIC_H
#define __F_MODMUSIC_H

#include "f_baseobject.h"
#include "f_helpers.h"
#include "f_basesound.h"

struct MODULE;

namespace fe
{

	class baseSoundBuffer;
	class modMusic;
	
	class FE_API modFeeder : public genericCallback
	{
	private:
		modMusic *mpFile;
	public:
		modFeeder (modMusic *file);
		virtual void call (void *data);
	};
	
	class FE_API modMusic : public baseSound
	{
		friend class modFeeder;
	private:
		
		struct MODULE *mpModule;
		baseSoundBuffer *mpBuffer;
		modFeeder		mFeeder;
		float mVolume;
	
	protected:
	
		void update (void);
	
	public:
	
		modMusic (const char *fname);
		~modMusic (void);
	
		void play (bool looping);
		void stop (void);
		void pause (bool onoff);
		void setVolume (float volume);
		bool isPlaying (void) const;
		smartPtr <baseSound>	duplicate (void);
	};
	
}

#endif // __F_MODMUSIC_H
