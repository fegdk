/*
	fegdk: FE Game Development Kit
	Copyright (C) 2001-2008 Alexey "waker" Yakovenko

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Library General Public
	License as published by the Free Software Foundation; either
	version 2 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Library General Public License for more details.

	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Alexander Maltsev
	keltar@users.sourceforge.net
*/

#include "pch.h"
#include "basesounddriver.h"
#include "mmanager.h"
#include "f_engine.h"

namespace fe
{

cvar_t *snd_numstreamchunks;
cvar_t *snd_maxnumbuffers;
cvar_t *snd_maxdupsources;
cvar_t *snd_streamchunksize;

void *baseSoundBuffer::operator new (size_t size)
{
	for (int i = 0; i != 8; ++i)
	{
		pool *p = g_engine->getSoundDriver ()->getBuffersPool (i);
		if (p)
		{
			void *ret = p->alloc ();
			if (ret)
			{
				((baseSoundBuffer*)ret)->mPoolId = i;
				return ret;
			}
		}
	}
}

void baseSoundBuffer::operator delete (void *ptr)
{
	int pool_id = ((baseSoundBuffer*)ptr)->mPoolId;
	g_engine->getSoundDriver ()->getBuffersPool (pool_id)->free (ptr);
}

pool *baseSoundDriver::getBuffersPool (int pool_id)
{
	if (pool_id == mNumPools + 1)
	{
		createPool ();
	}
	return mpPools[pool_id];
}

}
