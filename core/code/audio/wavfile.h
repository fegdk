/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#ifndef __F_WAVFILE_H
#define __F_WAVFILE_H

#include "f_baseobject.h"
#include "f_helpers.h"
#include "f_basesound.h"
#include "f_types.h"
#include "f_filesystem.h"

namespace fe
{
	
	class baseSoundBuffer;
	class wavFile;
	
	struct wavHeader {
		uint32 riffMagic;
		uint32 size;
		uint32 waveMagic;
		uint32 fmtMagic;
		uint32 chunkSize;
		uint16 formatTag;
		uint16 channels;
		uint32 samplesPerSecond;
		uint32 bytesPerSecond;
		uint16 blockAlignment;
		uint16 bitsPerSample;
		uint32 dataMagic;
		uint32 dataSize;
	};
	
	class FE_API wavFeeder : public genericCallback
	{
	private:
		wavFile *mpFile;
	public:
		wavFeeder (wavFile *file);
		virtual void call (void *data);
	};
	
	class FE_API wavFile : public baseSound
	{
		friend class wavFeeder;
	
	protected:
	
		enum { buffer_size = 200000 };	// NOTE: 200kb should be enough, but better check
	
		wavFeeder				mFeeder;
	    baseSoundBuffer*		mpBuffer;
		wavHeader				mWavHeader;
		char*					mpPCMOut;
		bool					mbEOF;
		int						mCurrentSection;
		file*					mpFile;
		int						mHaveBytes;
		bool					mbLooping;
		bool					mbPaused;
		float					mVolume;
		bool					mbStreaming;
		bool					mbPlaying;
	
		
		void open (void);
	
	
		void update (void);
	
	public:
	
		wavFile (const char *name);
		~wavFile (void);
	
		void play (bool looped);
		void stop (void);
		void pause (bool onoff);
		void setVolume (float volume);
		bool isPlaying (void) const;
		int getChannelsCount (void) const;
		smartPtr <baseSound> duplicate (void);
	};
	
}

#endif // __F_OGGVORBIS_H
