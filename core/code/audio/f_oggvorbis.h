/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_OGGVORBIS_H
#define __F_OGGVORBIS_H

#include "f_baseobject.h"
#include "f_helpers.h"
#include "f_basesound.h"

struct OggVorbis_File;
struct vorbis_info;

// FIXME: make vorbis streaming and playback into separate classes

namespace fe
{
	
	class baseSoundBuffer;
	class vorbisFile;
	class file;
	
	class FE_API oggFeeder : public genericCallback
	{
	private:
		vorbisFile *mpFile;
	public:
		oggFeeder (vorbisFile *file);
		virtual void call (void *data);
	};
	
	class FE_API vorbisFile : public baseSound
	{
		friend class oggFeeder;
	
	protected:
	
		oggFeeder				mFeeder;
	    baseSoundBuffer*		mpBuffer;
		char*					mpPCMOut;
		OggVorbis_File*			mpVorbisFile;
		bool					mbEOF;
		int						mCurrentSection;
		file*					mpFile;
		int						mHaveBytes;
		bool					mbLooping;
		bool					mbPaused;
		float					mVolume;
		bool					mbStreaming;
		vorbis_info*			mpVI;
	
		void open (void);
		void update (void);
	
	public:
	
		vorbisFile (const char *name);
		~vorbisFile (void);
	
		void play (bool looped);
		void stop (void);
		void pause (bool onoff);
		void setVolume (float volume);
		bool isPlaying (void) const;
		int getChannelsCount (void) const;
		smartPtr <baseSound>	duplicate (void);
	};
	
}

#endif // __F_OGGVORBIS_H
