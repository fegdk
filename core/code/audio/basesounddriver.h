/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#ifndef __F_BASESOUNDDRIVER_H
#define __F_BASESOUNDDRIVER_H

#include "f_baseobject.h"

namespace fe
{

extern struct cvar_t *snd_numstreamchunks;
extern struct cvar_t *snd_maxnumbuffers;
extern struct cvar_t *snd_maxdupsources;
extern struct cvar_t *snd_streamchunksize;

class baseSoundDriverData;
class genericCallback;
class baseSoundDriver;
class pool;

class baseSoundBuffer : public baseObject
{
public:
	baseSoundBuffer (baseSoundDriver *driver, int freq, int sample_rate, bool stereo, size_t size, bool streaming) {}
	virtual ~baseSoundBuffer (void) {}

	virtual void write (void *data, int size, int fmt, int sample_rate) = 0;
	virtual void setLooping (bool onOff) = 0;
	virtual void play (void) = 0;
	virtual void stop (void) = 0;
	virtual int getChannelsCount (void) const = 0;
	virtual bool isPlaying (void) const = 0;
	virtual void setFeeder (genericCallback *callback) = 0;
	virtual void update (void) = 0;
	virtual void setVolume (float vol) = 0;
	virtual void setMaxSources (int num) = 0;
	virtual void pause (bool onoff) = 0;
	virtual int getNumBuffers (void) const = 0;

	// may be bad idea.. Should be a better way to use soundDriver's internal memory managment...
	// \keltar
	static void *operator new (size_t size);
	static void operator delete (void *ptr);

private:
	int mPoolId;
};

class baseSoundDriver : public baseObject
{
	friend class baseSoundBuffer;

public:
	// formats
	enum {fmt_mono8, fmt_mono16, fmt_stereo8, fmt_stereo16};
	
	baseSoundDriver () {}
	virtual ~baseSoundDriver () {}

	virtual void update () = 0;
//	virtual baseSoundDriverData *getData () = 0;
	virtual baseSoundBuffer *createBuffer (int freq, int sample_rate, bool stereo, size_t size, bool streaming) = 0;

protected:
	pool *getBuffersPool(int pool_id);
	virtual void createPool () = 0;

protected:
	uint32 mNumPools;
	enum { mPoolsSize = 1024 };
	pool *mpPools[8];
};

}

#endif	// __F_BASESOUNDDRIVER_H
