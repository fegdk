/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#include "pch.h"
#include "wavfile.h"
#include "f_helpers.h"
#include "basesounddriver.h"
#include "f_engine.h"

namespace fe
{
	
	wavFeeder::wavFeeder (wavFile *file)
	{
		mpFile = file;
	}
	
	void wavFeeder::call (void *data)
	{
		mpFile->update ();
	}
	
	wavFile::wavFile (const char *name)
		: baseSound (name)
		, mFeeder (this)
	{
		mName = name;
	
		mpBuffer = NULL;
		mpPCMOut = NULL;
		mbEOF = false;
		mCurrentSection = -1;
		mpFile = NULL;
		mHaveBytes = 0;
		mbLooping = false;
		mbPaused = false;
		mVolume = 0;
		mbPlaying = false;
	
		mpFile = g_engine->getFileSystem ()->openFile (mName, FS_MODE_READ);
	
		fprintf (stderr, "loading %s from disk....\n", this->name ());
	
		if (mpFile)
		{
			// read WAV header
			if (!mpFile->read (&mWavHeader, sizeof (wavHeader)))
			{
				fprintf (stderr, "WAV header not found in %s\n", this->name ());
				mpFile->close ();
				mpFile = NULL;
				return;
			}
	
			// checking
			// TODO implement it
	
			if (mWavHeader.size < 2000000)
			{
				mbStreaming = false;
				mpBuffer = g_engine->getSoundDriver ()->createBuffer (mWavHeader.samplesPerSecond, 16, mWavHeader.channels == 1 ? false : true, mWavHeader.dataSize, false);
				mpPCMOut = new char[mWavHeader.dataSize];
				mHaveBytes = mpFile->read (mpPCMOut, mWavHeader.dataSize);
				mpBuffer->write (mpPCMOut, mWavHeader.dataSize, (mWavHeader.channels == 1) ? baseSoundDriver::fmt_mono16 : baseSoundDriver::fmt_stereo16, mWavHeader.samplesPerSecond);
				delete[] mpPCMOut;
				mpPCMOut = NULL;
	
				if (mpFile)
				{
					mpFile->close ();
					mpFile = NULL;
				}
			}
			else
			{
				mbStreaming = true;
				mpPCMOut = new char[buffer_size * 2];
				mpBuffer = g_engine->getSoundDriver ()->createBuffer (mWavHeader.samplesPerSecond, 16, mWavHeader.channels == 1 ? false : true, buffer_size, true);
				mpBuffer->setFeeder (&mFeeder);
			}
		}
	}
	
	wavFile::~wavFile (void)
	{
		stop ();
		if (mpBuffer)
		{
			mpBuffer->stop ();
			mpBuffer->release ();
			mpBuffer = NULL;
		}
		if (mpPCMOut)
		{
			delete[] mpPCMOut;
			mpPCMOut = NULL;
		}
	
		if (mpFile)
		{
			mpFile->close ();
			mpFile = NULL;
		}
	}
	
	void wavFile::play (bool looping)
	{
		if (!mpBuffer)
			return;
	
		mbLooping = looping;
		if (mbStreaming)
		{
			if (isPlaying ())
			{
				// rewind & unpause
				mpFile->seek (sizeof (wavHeader), SEEK_SET);
				mbEOF = false;
			}
			else
				open ();
		}
		else
			mpBuffer->setLooping (mbLooping);
		
		mpBuffer->play ();
		mbPlaying = true;
	}
	
	void wavFile::stop (void)
	{
		if (mpBuffer)
		{
			mpBuffer->stop ();
			mbPlaying = false;
		}
		if (mpFile)
		{
			mpFile->close ();
			mpFile = NULL;
		}
	
		mHaveBytes = 0;
	}
	
	void wavFile::pause (bool onoff)
	{
		if (mpBuffer)
		{
			mpBuffer->pause (onoff);
		}
		mbPlaying = !onoff;
	}
	
	void wavFile::setVolume (float volume)
	{
		mVolume = volume;
		if (mpBuffer)
			mpBuffer->setVolume (volume);
	}
	
	bool wavFile::isPlaying (void) const
	{
		return mbPlaying;
	}
	
	int wavFile::getChannelsCount (void) const
	{
		return mpBuffer->getChannelsCount ();
	}
	
	void wavFile::open ()
	{
		mbEOF = false;
		mCurrentSection = -1;
		mHaveBytes = 0;
		mpFile = g_engine->getFileSystem ()->openFile (mName, FS_MODE_READ);
		mpFile->read (&mWavHeader, sizeof (wavHeader));
		mpBuffer->setFeeder (&mFeeder);
	}
	
	void wavFile::update (void)
	{
		if (!mpFile)
			return;
		for (;;)
		{
			if (!mpBuffer)
				break;
	
			if (mbPaused)
				break;
	
			while (!mbEOF && mHaveBytes < buffer_size)
			{
				// read wav
				long ret = mpFile->read (mpPCMOut + mHaveBytes, buffer_size - mHaveBytes);
				if (ret == 0)
				{
					if (mbLooping)
					{
						// rewind
						mpFile->seek (sizeof (wavHeader), SEEK_SET);
						mbEOF = false;
					}
					else
						mbEOF = true;
				}
				else if (ret < 0)
				{
					// error
				}
				else
				{
					mHaveBytes += ret;
				}
			}
	
			if (mbEOF && !mbLooping)
			{
				stop ();
				return;
			}
	
	
			if (mHaveBytes || mbEOF)
			{
	/*			if (mEventStop)
				{
					DWORD signStop = WaitForSingleObject (mEventStop, 10);
					if (signStop == WAIT_OBJECT_0 && mbEOF && !mHaveBytes)
					{
						stop ();
						break;
					}
				}*/
	
				if (mbEOF && !mHaveBytes)
				{
					break;
				}
	
				bool updated = false;
	
				if (mHaveBytes < buffer_size)
				{
					memset (mpPCMOut + buffer_size, 0, buffer_size - mHaveBytes);
					mHaveBytes = buffer_size;
				}
				int copybytes = min ((int)buffer_size, (int)mHaveBytes);
	
				mpBuffer->write (mpPCMOut, copybytes, baseSoundDriver::fmt_stereo16, mWavHeader.samplesPerSecond);
				
				updated = true;
	
				if (updated && mHaveBytes > buffer_size)
				{
					memmove (mpPCMOut, &mpPCMOut[buffer_size], mHaveBytes - buffer_size);
					mHaveBytes -= buffer_size;
				}
				else if (updated)
					mHaveBytes = 0;
			}
			break;
		}
	}
	
	smartPtr <baseSound> wavFile::duplicate (void)
	{
		return new wavFile (name ());
	}
	
}

