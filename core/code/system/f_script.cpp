/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_script.h"
#include "f_engine.h"
#include "f_parser.h"
#include "f_resourcemgr.h"
#include "f_filesystem.h"
#include "f_error.h"

namespace fe
{

	script::script (void)
	{
	}
	
	script::~script (void)
	{
	}
	
	void script::load (const char *fname)
	{
		charParser p (g_engine->getFileSystem (), fname);
		load (p);
	}
	
	void script::load (charParser &p)
	{
		for (;;)
		{
			p.errMode (0);
			if (p.getToken ())
				break;
			p.errMode (1);
			if (!p.cmptoken ("load"))
			{
				p.getToken ();
				try {
					charParser parser (g_engine->getFileSystem (), p.token ());
					load (parser);
				}
				catch (parserException e)
				{
				}
			}
			else if (!p.cmptoken ("mtl"))
			{
				if (mNumObjects == SCRIPT_MAXOBJECTS)
					sys_error ("scripted object count is too large!");
				baseObject *o = (baseObject*)g_engine->getResourceMgr ()->createMaterial (p);
				mObjects[mNumObjects++] = o;
			}
			else
				p.generalSyntaxError ();
		}
	}
	
	void script::prefetch (void)
	{
		for (int i = 0; i < mNumObjects; i++)
		{
			if (mObjects[i]->isUsed ())
				mObjects[i]->loadData ();
		}
	}
	
	void script::flushUnused (void)
	{
		// NOT FINISHED YET!
	#if 0
		FE_EPTR_FROM_OWNER;
		std::vector<smartPtr<baseObject> > tmp = mObjects;
		for (std::vector<smartPtr<baseObject> >::iterator it = tmp.begin (); it != tmp.end (); it++)
		{
			if (!(*it)->isUsed ())
			{
				std::vector<smartPtr<baseObject> >::iterator obj;
				obj = std::find (mObjects.begin (), mObjects.end (), (*it));
				assert (obj != mObjects.end ());
				if (obj != mObjects.end ())
				{
					mObjects.erase (obj);
				}
			}
		}
	#endif
	}
	
}


