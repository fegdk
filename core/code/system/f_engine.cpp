/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "config.h"
#include "f_types.h"
#include "f_baseviewport.h"
#include "f_filesystem.h"
#include "f_input.h"
#include "f_application.h"
#include "f_engine.h"
#include "f_timer.h"
#include "f_console.h"
#include "f_error.h"
#include "f_parmmgr.h"
#ifndef SOUND_DISABLED
#include "basesounddriver.h"
#endif
#include "f_drawutil.h"
#include "f_baserenderer.h"
#include "f_resourcemgr.h"
#include "f_texture.h"
#include "f_material.h"
#include "f_effect.h"
#include "f_effectparms.h"
#include "f_fontft.h"
#include "f_model.h"
#include "f_logger.h"
#include "f_scenemanager.h"
#include "f_keybinder.h"
#include "f_script.h"
#include "entitymanager.h"
#include "entclasses.h"
#include "metashader.h"
#include "cvars.h"
#include "backend.h"
#include "animsequencer.h"
#include "mmanager.h"
#if !PHYSX_DISABLED
#include "physx.h"
#endif

// platform specific stuff
// FIXME: should be controlled using configure script
#include "../video/sdl/f_sdlviewport.h"
#include "../video/sdl/f_sdlrenderer.h"
#include "../video/x11/x11viewport.h"
#include "../video/x11/x11renderer.h"
#include "../video/gl/f_glrenderer.h"
#ifndef SOUND_DISABLED
#include "../audio/openal/openalsounddriver.h"
#include "../audio/null/nullsounddriver.h"
#endif

namespace fe
{
	
	// sys cvars
	cvar_t *sys_int_mainloop;
	cvar_t *sys_profile;
	cvar_t *sys_mainscript;
	cvar_t *sys_debug_freetype;
	cvar_t *sys_stack_size;
	cvar_t *sys_audio_profile;

	//-----------------------------------
	// engine implementation
	//-----------------------------------
	
	engine *g_engine;

	engine::engine (void)
	{
		g_engine = this;
		setName ("engine");
		mpTmpBuffer = NULL;
		mpCVarManager = new cvarManager;
		sys_int_mainloop = mpCVarManager->get ("sys_int_mainloop", "1", 0);
		sys_profile = mpCVarManager->get ("sys_profile", "sdl_opengl", 0);
		sys_mainscript = mpCVarManager->get ("sys_mainscript", "scripts/main.txt", CVAR_READONLY);
		sys_debug_freetype = mpCVarManager->get ("sys_debug_freetype", "0", 0);
		sys_stack_size = mpCVarManager->get ("sys_stack_size", "8", CVAR_READONLY);
		sys_audio_profile = mpCVarManager->get ("sys_audio_profile", "openal", CVAR_READONLY);
	}
	
	engine::~engine (void)
	{
		fprintf (stderr, "engine::~engine called!\n");
	
		mpApp->free ();
#if !PHYSX_DISABLED
		physx_free ();
#endif
		mpMetaShader = NULL;
	
		if (mpTmpBuffer)
			delete[] mpTmpBuffer;
	
#ifndef SOUND_DISABLED
		mpSoundDriver = NULL;
#endif
		fprintf (stderr, "engine::~engine finished!\n");
	}
	
	
	void Cmd_unbindall_f (void)
	{
		g_engine->getKeyBinder ()->unbindAll ();
	}
	
	void Cmd_bind_f (void)
	{
		if (Cmd_Argc () <= 2)
		{
			Con_Printf ("usage: bind key action");
			return;
		}
		int keycombo = g_engine->getKeyBinder ()->keycodeForString (Cmd_Argv (1));
		if (keycombo == -1)
		{
			Con_Printf ("invalid key combo: %s", Cmd_Argv (1));
			return;
		}
		g_engine->getKeyBinder ()->bind (keycombo, Cmd_Argv (2));
	}
	
	void Cmd_unbind_f (void)
	{
		if (Cmd_Argc () <= 1)
		{
			Con_Printf ("usage: unbind key");
			return;
		}
		int keycombo = g_engine->getKeyBinder ()->keycodeForString (Cmd_Argv (1));
		if (keycombo == -1)
		{
			Con_Printf ("invalid key combo: %s", Cmd_Argv (1));
			return;
		}
		g_engine->getKeyBinder ()->unbind (keycombo);
	}
	
	void Cmd_toggleconsole_f (void)
	{
		g_engine->getConsole ()->toggle ();
	}
	
	void Cmd_echo_f (void)
	{
		if (Cmd_Argc () <= 1)
		{
			Con_Printf ("usage: echo message text");
			return;
		}
		for (int i = 1; i < Cmd_Argc (); i++)
		{
			Con_Printf ("%s ", Cmd_Argv (i));
		}
	}
	
	void Cmd_map_f (void)
	{
		if (Cmd_Argc () <= 1)
		{
			Con_Printf ("usage: map file");
			return;
		}
		g_engine->getEntManager ()->clean ();
		entWorld* world = (entWorld*)g_engine->getEntManager ()->createEntity (ECLASS_WORLD);
		world->load (Cmd_Argv (1));
	}
	
	void engine::sharedInit (void)
	{
		sys_printf ("init cvars...\n");
		cvarManager *cvars = getCVarManager ();
		sys_printf ("init stackAllocator...\n");
		mpStackAllocator = new stackAllocator ((sys_stack_size->ivalue << 10) * 1024);
		sys_printf ("init fxparms...\n");
		mpEffectParms = new effectParms ();
		sys_printf ("init parmMgr...\n");
		mpParmMgr = new parmMgr ();
		sys_printf ("init inputDev...\n");
		mpInputDevice = new inputDevice ();

//#ifndef SOUND_DISABLED
//		if (!strcmp (sys_audio_profile->string, "openal"))
//			mpSoundDriver = new openalSoundDriver ();
//		else if (!strcmp (sys_audio_profile->string, "null"))
//			mpSoundDriver = new nullSoundDriver ();
//		else
//			sys_error ("unknown audio profile: %s", sys_audio_profile->string);
//#else
//		fprintf (stderr, "sound support is disabled at compile time\n");
//#endif
	}
	
	void engine::run (const smartPtr <baseRenderer> &render, const char *dataPath)
	{
		mpRenderer = render;
	
		// create tmp buffer
		mpTmpBuffer = new ubyte[default_tmpbuffer_size];
	
		if (!mpResourceMgr)
		{
			restartVideo ();
			restartAudio ();
		}
	
		// create console
		if (!mpConsole)
			mpConsole = new console ();
	
		// create filesystem
		mpFileSystem = new fileSystem ();
		// default path is "./"
		mpFileSystem->init (dataPath);

		// shared init
		sharedInit ();
		sys_printf ("sharedInit done!\n");
	}

	void Cmd_r_restart_f (void)
	{
		g_engine->restartVideo ();
	}

	void engine::restartVideo (void)
	{
		if (!mpViewport || !mpRenderer)
		{
			mpViewport = NULL;
			mpRenderer = NULL;
			if (!strcmp ("sdl_opengl", sys_profile->string))
			{
				// create mpViewport
				mpViewport = new sdlViewport ();
	
				// create render device
				smartPtr <sdlRenderer>	rend = new sdlRenderer ();
				mpRenderer = rend.dynamicCast<baseRenderer> ();
			}
			if (!strcmp ("x11_opengl", sys_profile->string))
			{
				mpViewport = new x11Viewport ();
				smartPtr <x11Renderer>	rend = new x11Renderer ();
				mpRenderer = rend.dynamicCast<baseRenderer> ();
			}
			else if (!strcmp ("external_opengl", sys_profile->string))
			{
				smartPtr <glRenderer> rend = new glRenderer ();
				mpRenderer = rend.dynamicCast<baseRenderer> ();
				mpRenderer->init ();
			}
			else if (!strcmp ("external_null", sys_profile->string))
			{
				// do not create renderer
				// useful for console-mode apps
			}
		}
		else
		{
			// resize viewport/renderer for current vidmode
			mpRenderer->reset ();
		}
	}

	void Cmd_snd_restart_f (void)
	{
		g_engine->restartAudio ();
	}

	void engine::restartAudio (void)
	{
#ifndef SOUND_DISABLED
		if (!mpSoundDriver)
		{
			if (!strcmp ("openal", sys_audio_profile->string))
			{
				mpSoundDriver = new openalSoundDriver ();
			} else
			{
				mpSoundDriver = new nullSoundDriver ();
			}
		}
#endif
	}
	
	void engine::run (const smartPtr <application> &_app, int argc, char *argv[])
	{
		bool initError = false;
		try {
			initProcessTime ();
			mpTimer = new timer ();

			// create tmp buffer
			mpTmpBuffer = new ubyte[default_tmpbuffer_size];
	
			// remember and reference application object
			assert( _app );
			mpApp = _app;
	
			// create console
			mpConsole = new console ();
	
			// create filesystem
			mpFileSystem = new fileSystem ();
			
			// we will concatenate engine's PKGDATADIR with application-supplied datadir
			// so it will first look into engine standart stuff, than into application directories
			cStr pathlist = PKGDATADIR;
			cStr appPath = mpApp->dataPath ();
			if (!appPath.empty ())
			{
				pathlist += ":";
				pathlist += appPath;
			}
			mpFileSystem->init (pathlist);
			sys_printf ("init keyBinder\n");
			mpKeyBinder = new keyBinder ();
			sys_printf ("reg sys cvars\n");
			// register rendering ignition cmd
			FE_REGISTER_CONSOLE_CMD (r_restart);

			FE_REGISTER_CONSOLE_CMD (snd_restart);
		
			// register keybinding console commands
			FE_REGISTER_CONSOLE_CMD (unbindall);
			FE_REGISTER_CONSOLE_CMD (bind);
			FE_REGISTER_CONSOLE_CMD (unbind);
			FE_REGISTER_CONSOLE_CMD (toggleconsole);
			FE_REGISTER_CONSOLE_CMD (echo);
			getConsole ()->command ("bind ` toggleconsole");

			// resmgr is not affected by app configurator
			sys_printf ("init resMgr\n");
			initResourceMgr ();
			sys_printf ("configure app\n");
			mpApp->configure ();

			// parse command line commands
			sys_printf ("parse/exec command line\n");
			if (argv)
			{
				char cmd[1024] = "";
				for (int i = 1; i < argc; i++)
				{
					if (argv[i][0] == '+')
					{
						if (cmd[0])
							getConsole ()->command (cmd);
						strcpy (cmd, &argv[i][1]);
					}
					else
					{
						strcat (cmd, " \"");
						strcat (cmd, argv[i]);
						strcat (cmd, "\"");
					}
				}
				if (cmd[0])
					getConsole ()->command (cmd);
			}
		
			fprintf (stderr, "INFO: sys_profile=%s\n", sys_profile->string);
			restartVideo ();
	
			if (mpRenderer)
			{
				// font
				mpSysFont = getResourceMgr ()->createFontFT ("ter-116b.pcf");
				// metashader
				mpMetaShader = new metaShader (r_metashader->string);
				// create drawutil
				mpDrawUtil = new drawUtil ();
				// create renderqueue
				mpRBackend = new rBackend ();
				// create scene manager
				mpSceneManager = new sceneManager ();
			}

#if !SOUND_DISABLED
			fprintf (stderr, "INFO: audio_profile=%s\n", sys_audio_profile->string);
			restartAudio ();
#endif
	
			mpEntManager = new entityManager ();
	
			// shared init
			sharedInit ();

			// anim/bone stuff
			mpAnimSequencer = new animSequencer;
		
#if !PHYSX_DISABLED
			// physx
			fprintf (stderr, "physx_init\n");
			physx_init ();
#endif
	
			// script
			fprintf (stderr, "loading script\n");
			mpScript = new script ();
			mpScript->load (sys_mainscript->string);
	
			// app may use it's own config file and change cvars, so we do this here
			// init application
			mpApp->init ();
		}
		catch (genericError e)
		{
	
			fprintf (stderr, "engine init error\n");
			initError = true;
			quit ();
		}
	
		if (0 != sys_int_mainloop->ivalue)
		{
			fprintf (stderr, "mainloop called!\n");
			mpViewport->mainLoop ();
			fprintf (stderr, "mainloop finished!\n");
		}
	}
	
	void	engine::quit (void)
	{
		getViewport ()->close (0);
	}
	
	void engine::tick (void)
	{
		mpTimer->update ();
#if !PHYSX_DISABLED
		physx_tick (mpTimer->getTimePassed ());
#endif
		mpApp->update ();
#ifndef SOUND_DISABLED
		if (getSoundDriver ())
			getSoundDriver ()->update ();
#endif
		if (mpAnimSequencer)
			mpAnimSequencer->update ();
		if (!getRenderer()->isLost ())
		{
//			beginScene ();
			mpApp->render ();
			mpConsole->render ();
//			endScene ();
		}
		else
		{
			sleep (100);
		}
		getRenderer()->present ();
#if !PHYSX_DISABLED
		physx_fetch_results ();
#endif
	}
	
	void engine::keyDown (int code)
	{
		if (mpConsole->isVisible ())
			mpConsole->keyDown (code);
		else
			mpApp->keyDown (code);
	}
	
	void engine::keyUp (int code)
	{
		if (!mpConsole->isVisible ())
			mpApp->keyUp (code);
	}
	
	application*	engine::getApplication (void) const
	{
		return mpApp;
	}
	
	baseRenderer*		engine::getRenderer (void) const
	{
		return mpRenderer;
	}

#ifndef SOUND_DISABLED
	smartPtr <baseSoundDriver>	engine::getSoundDriver (void) const
	{
		return mpSoundDriver;
	}
#endif
	
	fileSystem*	engine::getFileSystem (void) const
	{
		return mpFileSystem;
	}
	
	baseViewport*		engine::getViewport (void) const
	{
		return mpViewport;
	}
	
	void			engine::setViewport (baseViewport* vp)
	{
		mpViewport = vp;
	}
	
	inputDevice*	engine::getInputDevice (void) const
	{
		return mpInputDevice;
	}
	
	ubyte*					engine::getTmpBuffer (int sz) const
	{
		assert (sz <= default_tmpbuffer_size);
		if (sz > default_tmpbuffer_size)
			return NULL;
		return mpTmpBuffer;
	}
	
	logger*	engine::getLogger (void)
	{
		if (!mpLogger)
			mpLogger = new logger (NULL);
		return mpLogger;
	}
	
	timer*	engine::getTimer (void) const
	{
		return mpTimer;
	}
	
	console*	engine::getConsole (void)
	{
		if (!mpConsole)
		{
			mpConsole = new console ();
		}
		return mpConsole;
	}
	
	effectParms*	engine::getEffectParms (void) const
	{
		return mpEffectParms;
	}
	
	parmMgr*		engine::getParmMgr (void) const
	{
		return mpParmMgr;
	}
	
	void	engine::initResourceMgr (void)
	{
		mpResourceMgr = new resourceMgr;
	}
	
	resourceMgr*	engine::getResourceMgr (void) const
	{
		return mpResourceMgr;
	}
	
	drawUtil*		engine::getDrawUtil (void) const
	{
		return mpDrawUtil;
	}
	
	rBackend*	engine::getRBackend (void) const
	{
		return mpRBackend;
	}
	
	sceneManager*	engine::getSceneManager (void) const
	{
		return mpSceneManager;
	}
	
	keyBinder*	engine::getKeyBinder (void) const
	{
		return mpKeyBinder;
	}
	
	entityManager*	engine::getEntManager (void) const
	{
		return mpEntManager;
	}

	metaShader* engine::getMetaShader (void) const
	{
		return mpMetaShader;
	}
	
	fontFT* engine::getSysFont (void) const
	{
		return mpSysFont;
	}

	animSequencer*		engine::getAnimSequencer (void) const
	{
		return mpAnimSequencer;
	}

	stackAllocator *engine::getStackAllocator (void) const
	{
		return mpStackAllocator;
	}
	
	cvarManager* engine::getCVarManager (void) const
	{
		return mpCVarManager;
	}
}
