/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_TYPES_H
#define __F_TYPES_H

#include <stdint.h>

// win32 dllexport
#if defined(_WIN32)
#	if	defined(__MINGW32__)
#		define FE_API
#	else
#		pragma warning( disable : 4251 )
#		if defined (FEGDKDLL_EXPORTS)
#			define FE_API	__declspec(dllexport)
#		else
#			define FE_API	__declspec(dllexport)
#		endif
#	endif
#else
#	define FE_API
#endif

#if 0
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#endif

namespace fe
{

	template <typename A, typename B> inline const A& min (const A& a, const B& b)
	{
		return a < b ? a : b;
	}
	
	template <typename A, typename B> inline const A& max (const A& a, const B& b)
	{
		return a > b ? a : b;
	}
	
	//-----------------------------------
	// misc typedefs
	//-----------------------------------
	
	typedef unsigned char		uchar;
	typedef unsigned char		ubyte;
	typedef unsigned long		ulong;
	typedef unsigned int		uint;
	typedef unsigned short		ushort;
	typedef void*				handle;
	typedef unsigned short		uint16;
	typedef short				int16;
	
	typedef uint32_t		uint32;
	typedef int32_t			int32;
	typedef uint64_t		uint64;
	typedef int64_t			int64;
	
	typedef intptr_t int_ptr;
	typedef uintptr_t uint_ptr;
	
	struct float4 {
		float4 (void) {}
		float4 (float f) { x = y = z = w = f; }
		float4 (float xx, float yy, float zz, float ww) { x = xx; y = yy; z = zz; w = ww; }
		float4 (const float4 &f) { x = f.x; y = f.y; z = f.z; w = f.w; }
		float4 (ulong f)
		{
			uchar *pval = (uchar *)&f;
			x = (float)pval[0]/0xff; y = (float)pval[1]/0xff; z = (float)pval[2]/0xff; w = (float)pval[3]/0xff;
		}
		float4 (float v[4]) { x = v[0]; y = v[1]; z = v[2]; w = v[3]; }
	
		ulong rgba (void) const
		{
			ulong ret;
			uchar *pret = (uchar*)&ret;
	
			pret[0] = (uchar)(r*255);
			pret[1] = (uchar)(g*255);
			pret[2] = (uchar)(b*255);
			pret[3] = (uchar)(a*255);
	
			return ret;
		}
	
		ulong bgra (void) const
		{
			ulong ret;
			uchar *pret = (uchar*)&ret;
	
			pret[2] = (uchar)(r*255);
			pret[1] = (uchar)(g*255);
			pret[0] = (uchar)(b*255);
			pret[3] = (uchar)(a*255);
	
			return ret;
		}
	
		operator ulong ()
		{
			ulong ret;
			uchar *pret = (uchar *)&ret;
			pret[0] = min (0xff, max (0, (int)(x*0xff)));
			pret[1] = min (0xff, max (0, (int)(y*0xff)));
			pret[2] = min (0xff, max (0, (int)(z*0xff)));
			pret[3] = min (0xff, max (0, (int)(w*0xff)));
			return ret;
		}
	
		float operator[] (int idx) const
		{
			return (&x)[idx];
		}

		float& operator[] (int idx)
		{
			return (&x)[idx];
		}

		union {
			struct { float x, y, z, w; };
			struct { float r, g, b, a; };
		};
	};
	
	struct rect
	{
		int left, top, right, bottom;
	};
	
	struct point
	{
		int x, y;
	};
	
#if 0	
	//----------------------
	// some utility macros
	//----------------------
	// setting cvar values
	#define FE_SET_CVAR_INT(name, val) (fe::g_engine)->getConsole ()->getVar (name, true)->setInt (val)
	#define FE_SET_CVAR_FLOAT(name, val) (fe::g_engine)->getConsole ()->getVar (name, true)->setFloat (val)
	#define FE_SET_CVAR_STR(name, val) (fe::g_engine)->getConsole ()->getVar (name, true)->setStr (val)
	
	#define FE_SET_CVAR_DEFAULT_INT(name, val) if (!(fe::g_engine)->getConsole ()->getVar (name, false)) (fe::g_engine)->getConsole ()->getVar (name, true)->setInt (val)
	#define FE_SET_CVAR_DEFAULT_FLOAT(name, val) if (!(fe::g_engine)->getConsole ()->getVar (name, false)) (fe::g_engine)->getConsole ()->getVar (name, true)->setFloat (val)
	#define FE_SET_CVAR_DEFAULT_STR(name, val) if (!(fe::g_engine)->getConsole ()->getVar (name, false)) (fe::g_engine)->getConsole ()->getVar (name, true)->setStr (val)
	
	// getting cvar values
	#define FE_GET_CVAR_INT(name) (fe::g_engine)->getConsole ()->getVar (name, true)->getInt ()
	#define FE_GET_CVAR_FLOAT(name) (fe::g_engine)->getConsole ()->getVar (name, true)->getFloat ()
	#define FE_GET_CVAR_STR(name) (fe::g_engine)->getConsole ()->getVar (name, true)->getStr ()
#endif	

void
sys_error (const char *fmt, ...);

void
sys_exit (int errcode);

}

#define sys_printf printf


#endif // __F_TYPES_H

