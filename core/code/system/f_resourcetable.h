/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#if 0
#ifndef __F_RESOURCETABLE_H
#define __F_RESOURCETABLE_H

#include "f_string.h"
#include "f_helpers.h"
#include "f_baseobject.h"
#include "f_resourcemgr.h"
#include "f_error.h"
#include "f_util.h"

namespace fe
{
	const int RES_HASHSIZE = 256;
		
	template <class T> class resource
	{
	
	private:
	
		T* mResourceHash[RES_HASHSIZE];
		int mNumResources;
	
	public:
	
		resource (void)	// this will be the owner of all created objects
		{
			memset (mResourceHash, 0, sizeof (mResourceHash));
		}
	
		~resource (void)
		{
			if (mNumResources) {
				T **lst = new T*[mNumResources];
				int i = 0;
				for (int h = 0; h < RES_HASHSIZE; h++) {
					T *o = mResourceHash[h];
					while (o) {
						lst[i++] = o;
						o = checked_cast<T*>(o->mpResHashNext);
					}
				}
				while (i>0) {
					lst[--i]->release ();
				}
				delete[] lst;
			}
		}
	
		T* create (const char *name)
		{
			T *o = getResource (name);
			if (!o)
			{
				o = new T (name);
				if (!o->isValid ())
				{
					o->release ();
					return NULL;
				}
				addResource (name, o);
			}
			return o;
		}
		
		T* create (charParser &parser)
		{
			parser.getToken ();
			cStr name = parser.token ();
			T *o = getResource (name);
			if (o)
			{
				fprintf (stderr, "WARNING: object with name %s is already defined\n", name.c_str ());
				return o;
			}
			T *obj = new T (parser, name);
			if (!obj->isValid ())
			{
				o->release ();
				return NULL;
			}
			addResource (name, obj);
			return obj;
		}
	
		void destroy (const char *name)
		{
			removeResource (name);
		}
		
		void flushUnused (void)
		{
			if (mNumResources) {
				T **lst = new T*[mNumResources];
				int i = 0;
				for (int h = 0; h < RES_HASHSIZE; h++) {
					T *o = mResourceHash[h];
					while (o) {
						lst[i++] = o;
						o = checked_cast<T*>(o->mpResHashNext);
					}
				}
				i--;
				while (i>=0) {
					if (!lst[i]->isUsed ())
						lst[i]->release (); // this SHOULD remove it from resource manager
				}
				delete[] lst;
			}
		}
		
		T* getResource (const char *name)
		{
			int h = hashValue (name, RES_HASHSIZE);
			T* o = mResourceHash[h];
			while (o) {
				if (!strcmp (o->name(), name))
					return o;
				o = checked_cast<T*>(o->mpResHashNext);
			}
			return NULL;
		}
	
		int getCount (void) const
		{
			return mNumResources;
		}
		
//		T* getObject (int idx) const
//		{
//			typename resourceMap::const_iterator it;
//			int i = 0;
//			for (it = mResourceList.begin (); it != mResourceList.end (); it++, i++)
//			{
//				if (i == idx)
//				{
//					return (*it).second;
//				}
//			}
//			return NULL;
//		}
		
		void addResource (const char *name, T *r)
		{
			int h = hashValue (name, RES_HASHSIZE);
			r->mpResHashNext = mResourceHash[h];
			mResourceHash[h] = r;
			r->addRef ();
			mNumResources++;
		}
		
		void removeResource (const char *name)
		{
			int h = hashValue (name, RES_HASHSIZE);
			T* o = mResourceHash[h];
			T* prev = NULL;
			while (o) {
				if (!strcmp (o->name(), name)) {
					if (prev) {
						prev->mpResHashNext = checked_cast<T*>(o->mpResHashNext);
					}
					else {
						mResourceHash[h] = checked_cast<T*>(o->mpResHashNext);
					}
					o->release ();
					return;
				}
				prev = o;
				o = checked_cast<T*>(o->mpResHashNext);
			}
			sys_printf ("WARNING: cannot delete '%s' - no such resource\n", name);
		}
		
	};
}

#endif // __F_RESOURCETABLE_H
#endif
