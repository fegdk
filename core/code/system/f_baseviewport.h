/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_BASEVIEWPORT_H
#define __F_BASEVIEWPORT_H

#include "f_string.h"
#include "f_baseobject.h"

namespace fe
{

	class baseViewport : public baseObject
	{
	
	public:
	
		baseViewport (void);
		virtual ~baseViewport (void);
	
		// common window access
		virtual void	setPosition (int x, int y) = 0;
		virtual void	setSize (int w, int h) = 0;
		virtual int		getLeft (void) const = 0;
		virtual int		getTop (void) const = 0;
		virtual int		getWidth (void) const = 0;
		virtual int		getHeight (void) const = 0;
		virtual void	close (ulong flags) = 0;
		virtual void mainLoop (void) = 0;
	};
	
}

#endif // __F_BASEVIEWPORT_H


