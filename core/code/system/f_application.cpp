/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_application.h"
#include "f_types.h"
#include "f_engine.h"
#include "f_baserenderer.h"
#include "f_parser.h"
#include "f_console.h"

namespace fe
{

	application::application(void)
	{
	}
	
	application::~application(void)
	{
	}
	
	void		application::configure (void)
	{
		// read config
		charParser p (g_engine->getFileSystem (), "autoexec.cfg");
		p.errMode (0);
		char s[1024];
		for (;;)
		{
			if (p.getToken ())
				break;;
			strcpy (s, p.token());
			while (!p.isEOL ())
			{
				if (p.getToken ())
					p.error ();
				strcat (s, " ");
				strcat (s, p.token ());
			}
			fprintf (stderr, "%s\n", s);
			g_engine->getConsole ()->command (s);
		}
	}
	
	void		application::init( void ) {}
	
	void		application::free( void ) {}
	
	const char*	application::getTitle( void ) const { return "FE application"; }
	
	const char*	application::dataPath( void ) const { return "data"; }
	
	void		application::keyDown( int keycode ) {}
	
	void		application::keyUp( int keycode ) {}
	
	void		application::mouseMove( int x, int y ) {}
	
	void		application::mouseDown( int keycode ) {}
	
	void		application::mouseUp( int keycode ) {}
	
	void		application::mouseWheel(int delta) {}
	
	void		application::update( void )
	{
	}
	
	void		application::render( void ) const
	{
	}
	
}
