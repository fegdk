/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_string.h"
#include "f_mathexpression.h"
#include "f_error.h"
#include "f_engine.h"
#include "f_parmmgr.h"
#include "f_helpers.h"
#include <assert.h>
#ifdef _DEBUG
#include <stdio.h>
#endif
#include "f_console.h"
#include "cvars.h"

namespace fe
{

// TODO: implement table lookups
/*
	table lookup details:
	
	new op:
	
		op_table1d dst, src0, src1
		dst -- destination register
		src0 -- pointer to a table of floats
		src1.x -- index into table
		src1.yzw -- undefined
	
	tree semantics:
			
			op_table1d
			/       \
		 src0		src1

	script syntax example:
			
			sintable[time*0.1]
*/

// TODO: intrinsic sin/cos (purposes???)
// TODO: 2d/3d/4d lookups (purposes???)

/*
op semantics:

add dst, src0, src1
sub dst, src0, src1
mul dst, src0, src1
div dst, src0, src1
dot dst, src0, src1
mov dst, src
*/

/*
bytecode format
	char	nconsts;				// number of constants

	float	constants[nconsts];		// array of constants

	char	ncmds;					// number of commands

	struct {
		 ((uchar)operator_t)		op;
		uchar					destreg;
		ushort					parm1;
		ushort					parm2;
	} commands[ncmds];				// array of commands

parm format:
bits 0..13	-	index of parameter (can be constant, register or variable)
bit 14..15	-	00 - constant; 01 - register; 10 - variable; 11 - reserved
*/

/*
app-defined variables are accessed through parmManager
the way it works should be as follows:

float *fval = engine->varManager ()->getFloat4ByName ("name");

or

ushort	index = engine->varManager ()->indexForName ("name");
float *fval = engine->varManager ()->getFloat4ByIndex (index);

parm manager should (?) provide overloads for type-casting. e.g.:

float*	getFloat4ByIndex (ushort index);
float4* getFloatByIndex (ushort index);
float4* getFloat3ByIndex (ushort index);
... etc ...

*/

/*
registers are shared between programs
registers are not guaranteed to keep meaningfull values between programs
*/

	void mathExpression::getwords (const cStr &in, cStr expression[], int &exprsize)
	{
		exprsize = 0;
	
		const char *s = in.c_str ();
		for (;;)
		{
			if (*s == 0)
				break;
			else if (*s == '*'
				|| *s == '/'
				|| *s == '+'
				|| *s == '-'
				|| *s == '|'
				|| *s == '['
				|| *s == ']'
				|| *s == '('
				|| *s == ')')
			{
				if (exprsize == MATH_MAX_EXPRESSION)
					sys_error ("exceeded max expression limit in mathexpr '%s'", name());
				cStr ss;
				ss += *s;
				expression[exprsize++] = ss;
				s++;
			}
			else if (*s <= 32)
				s++;
			else
			{
				const char *e = s;
				while (*e && *e > 32
					&& *e != '*'
					&& *e != '/'
					&& *e != '+'
					&& *e != '-'
					&& *e != '|'
					&& *e != '['
					&& *e != ']'
					&& *e != '('
					&& *e != ')')
				{
					e++;
				}
				if (e == s)
					break;
				cStr ss;
				for (; s != e; s++)
					ss += *s;
				if (exprsize == MATH_MAX_EXPRESSION)
					sys_error ("exceeded max expression limit in mathexpr '%s'", name());
				expression[exprsize++] = ss;
			}
		}
	}
	
	bool mathExpression::validate (const cStr expr[], const int exprsize)
	{
		// +001: no imparity of brackets, e.g. "(()"
		// +002: no double-operator cases, e.g. "a+*b"
		// +003: no double-value cases, e.g. "a b"
		// 004: no operator-only branches, e.g. "-"
		// +005: no empty branches, e.g. "()"
		// 006: no branches starting with operators other than "+" or "-"
	
		int cnt = 0;
		int bcnt = 0;
		int scnt = 0;
		int sbcnt = 0;
		size_t i;
	
		for (i = 0; i < exprsize; i++)
		{
			// 001
			if (expr[i] == "(")
			{
				cnt++;
				bcnt++;
			}
			else if (expr[i] == ")")
			{
				cnt--;
				bcnt++;
			}
	
			// 002
			else if (expr[i] == "*"
				|| expr[i] == "/"
				|| expr[i] == "+"
				|| expr[i] == "-"
				|| expr[i] == "|")
			{
				if (i < exprsize - 1)
				{
					if (expr[i+1] == "*"
						|| expr[i+1] == "/"
						|| expr[i+1] == "+"
						|| expr[i+1] == "-"
						|| expr[i+1] == "|")
					{
						return false;
					}
				}
			}
	
			else if (expr[i] == "[")
			{
				scnt++;
				sbcnt++;
			}
	
			else if (expr[i] == "]")
			{
				scnt--;
				sbcnt++;
			}
	
			// 003
			else	// normal value
			{
				if (i < exprsize - 1)
				{
					if (expr[i+1] != "*"
						&& expr[i+1] != "/"
						&& expr[i+1] != "+"
						&& expr[i+1] != "-"
						&& expr[i+1] != "|"
						&& expr[i+1] != "["
						&& expr[i+1] != "]"
						&& expr[i+1] != "("
						&& expr[i+1] != ")")
					{
						return false;
					}
				}
			}
		}
		if (cnt)	// 001
			return false;
	
		if (scnt)
			return false;
	
		// 004
		if (exprsize == 1
			&& (expr[0] == "*"
			|| expr[0] == "/"
			|| expr[0] == "+"
			|| expr[0] == "-"
			|| expr[0] == "|"))
		{
			return false;
		}
	
		// 005
		if (bcnt == exprsize)
			return false;
	
		if (sbcnt == exprsize)
			return false;
	
		return true;
	}
	
	cStr mathExpression::opstring (int op)
	{
		cStr s[] = { "", "+", "-", "*", "/" };
		return s[op];
	}
	
	mathExpression::operator_t mathExpression::getoperator (const cStr expression[], size_t curword)
	{
		cStr s = expression[curword];
		if (s == "*")
			return op_mul;
		if (s == "/")
			return op_div;
		if (s == "+")
			return op_add;
		if (s == "-")
			return op_sub;
		if (s == "|")
			return op_dot;
		return op_no;
	}
	
	bool mathExpression::iscomplex (const cStr &s)
	{
		if (s == "(")
			return true;
		return false;
	}
	
	mathExpression::node* mathExpression::buildtree (cStr expr[], int &exprsize)
	{
		// handle branch
		while (expr[0] == "(" && expr[exprsize-1] == ")")
		{
			// handle patalogical cases like "((3.2)+ (1))"
			size_t i;
			int cnt = 1;
			for (i = 1; i < exprsize; i++)
			{
				if (expr[i] == "(")
					cnt++;
				else if (expr[i] == ")")
					cnt--;
				if (cnt == 0 && i != exprsize - 1)
					break;
			}
			// remove brackets
			if (i == exprsize)
			{
				for (int k = 0; k < exprsize-1; k++)
					expr[k] = expr[k+1];
				exprsize-=2;
				continue;
			}
			break;
		}
	
		// handle table lookups
		// this check ensures that we don't have a case like "a[x] + b[y]"
		if (exprsize > 1
			&& getoperator (expr, 0) == op_no
			&& expr[1] == "["
			&& expr[exprsize-1] == "]")
		{
			bool lookup = true;
			for (size_t i = 2; i < exprsize; i++)
			{
				if (expr[i] == "]"
					&& i != exprsize - 1)
				{
					lookup = false;
					break;
				}
			}
	
			if (lookup)
			{
				cStr l[MATH_MAX_EXPRESSION];
				int ls = 0;
				cStr r[MATH_MAX_EXPRESSION];
				int rs = 0;
				l[ls++] = expr[0];
				for (size_t i = 2; i < exprsize - 1; i++)
				{
					r[rs++] = expr[i];
				}
				node *n = new node;
				n->op = op_lookup;
				n->left = buildtree (l, ls);
				n->right = buildtree (r, rs);
				return n;
			}
		}
	
		// handle expressions started w/ "+" or "-" signs
		if (expr[0] == "+") {
			for (int k = 0; k < exprsize-1; k++)
				expr[k] = expr[k+1];
			exprsize--;
		}
		else if (expr[0] == "-")
		{
			for (int k = 0; k < exprsize-1; k++)
				expr[k] = expr[k+1];
			exprsize--;
			expr[0] = "-" + expr[0];
		}
	
		// handle single value
		if (exprsize == 1)
		{
			node *n = new node;
			n->leaf = true;
	//		n->value = (float)atof (expr[0]);
			n->strvalue = expr[0];
			return n;
		}
	
		// find lowest-priority op
		// FIXME: should select op nearest to a center of expression. dunno if it matters..
		size_t index = (unsigned)-1;
		operator_t minop = op_max;
		size_t i;
		for (i = 0; i < exprsize; i++)
		{
			if (expr[i] == "(")
			{
				// skip to next ")"
				int cnt = 1;
				size_t k;
				for (k = i+1; k < exprsize; k++)
				{
					if (expr[k] == ")")
					{
						cnt--;
						if (!cnt)
							break;
					}
					else if (expr[k] == "(")
						cnt++;
				}
				i = k;
				continue;
			}
	
			if (expr[i] == "[")
			{
				// skip to next "]"
				int cnt = 1;
				size_t k;
				for (k = i+1; k < exprsize; k++)
				{
					if (expr[k] == "]")
					{
						cnt--;
						if (!cnt)
							break;
					}
					else if (expr[k] == "[")
						cnt++;
				}
				i = k;
				continue;
			}
	
			operator_t op = getoperator (expr, i);
			if (op == op_no)
				continue;
	
			if (op < minop)
			{
				minop = op;
				index = i;
			}
		}
	
		// dbg
	#ifdef _DEBUG
		cStr l;
		cStr r;
	#endif
	
		// split
		cStr leftexpr[MATH_MAX_EXPRESSION];
		int ls = 0;
		cStr rightexpr[MATH_MAX_EXPRESSION];
		int rs = 0;
		for (i = 0; i < exprsize; i++)
		{
			if (i < index)
			{
				leftexpr[ls++] = expr[i];
	#ifdef _DEBUG
				l += expr[i];
	#endif
			}
			else if (i > index)
			{
				rightexpr[rs++] = expr[i];
	#ifdef _DEBUG
				r += expr[i];
	#endif
			}
		}
	
		node *n = new node;
		n->op = minop;
		if (rs)
			n->left = buildtree (leftexpr, ls);
		if (rs)
			n->right = buildtree (rightexpr, rs);
	
		return n;
	}
	
	cStr mathExpression::strop (operator_t op) const
	{
		cStr s[] = { "", "add", "sub", "mul", "div", "dot" };
		return s[op];
	}
	
	bool mathExpression::isconst (const char *s)
	{
		if (*s == '-' || *s == '+')
			s++;
		while (*s)
		{
			if (!isdigit (*s) && *s != '.')
				return false;
			s++;
		}
	
		return true;
	}
		
	int mathExpression::getparamidx (const char *n) const
	{
		int parmIdx = g_engine->getParmMgr ()->indexForName (n);
		if (parmIdx >= (1<<13))
			Con_Printf ("param index is too big: %s(%d)\n", n, parmIdx);
		if (parmIdx == -1)
		{
			parmIdx = g_engine->getCVarManager ()->getCVarIdx (n);
			if (parmIdx == -1)
				Con_Printf ("cvar not found: %s\n", n);
			if (parmIdx >= (1<<13))
				Con_Printf ("param index is too big: %s(%d)\n", n, parmIdx);
			parmIdx |= (1<<13);
		}
		return parmIdx | varmask;
	}
	
	float* mathExpression::getvarvalueptr (int v) const
	{
		int i = v &~ parmtypemask;
		if (i & (1<<13))
			return &g_engine->getCVarManager ()->cvarForIdx (i &~ (1<<13))->fvalue;
		return g_engine->getParmMgr ()->getData (i);
	}
	
	void mathExpression::traverse (node *n, int reg)
	{
		assert (reg < maxregisters);
		if (n->root && n->leaf)
		{
			if (mNumCommands == MATH_MAX_COMMANDS)
				sys_error ("cmd limit exceeded for mathexpr '%s'", name());
			command_t c;
			c.opcode = op_mov;
			c.dst = reg;
	
			if (isconst (n->strvalue))
			{
				if (mNumConstants == MATH_MAX_CONSTANTS)
					sys_error ("const limit exceeded for mathexpr '%s'", name());
				c.src0 = (ushort)mNumConstants;
				c.src0 |= constmask;
				mConstants[mNumConstants++] = (float)atof (n->strvalue);
			}
			else	// var
			{
				c.src0 = getparamidx (n->strvalue);
			}
	
	//		printf ("mov r%d, %f\n", reg, n->value);
	
			mCommands[mNumCommands++] = c;
	
			return;
		}
		else
		{
			if (!n->left->leaf)
				traverse (n->left, reg);
			if (!n->right->leaf)
				traverse (n->right, n->left->leaf ? reg : reg+1);
			assert (!n->leaf); // do nothing to leaf nodes, they already have correct values
	//		n->strvalue.printf ("r%d", reg);
			n->reg = reg;
			if (mNumCommands == MATH_MAX_COMMANDS)
				sys_error ("cmd limit exceeded for mathexpr '%s'", name());
	
			command_t c;
			c.opcode = n->op;
			c.dst = reg;
	
			if (!n->left->leaf)	// reg
			{
				c.src0 = n->left->reg | regmask;
			}
			else if (isconst (n->left->strvalue))
			{
				if (mNumConstants == MATH_MAX_CONSTANTS)
					sys_error ("const limit exceeded for mathexpr '%s'", name());
				c.src0 = (ushort)mNumConstants;
				c.src0 |= constmask;
				mConstants[mNumConstants++] = (float)atof (n->left->strvalue);
			}
			else	// var
			{
				c.src0 = getparamidx (n->left->strvalue);
			}
	
			if (!n->right->leaf)	// reg
			{
				c.src1 = n->right->reg | regmask;
			}
			else if (isconst (n->right->strvalue))
			{
				if (mNumConstants == MATH_MAX_CONSTANTS)
					sys_error ("const limit exceeded for mathexpr '%s'", name());
				c.src1 = (ushort)mNumConstants;
				c.src1 |= constmask;
				mConstants[mNumConstants++] = (float)atof (n->right->strvalue);
			}
			else	// var
			{
				c.src1 = getparamidx (n->right->strvalue);
			}
			mCommands[mNumCommands++] = c;
	
	//		printf ("%s r%d, %s, %s\n", strop (n->op).c_str (), reg, n->left->strvalue.c_str (), n->right->strvalue.c_str ());
		}
	}
	
	#ifdef _DEBUG
	void mathExpression::printProgram (const std::vector< mathExpression::command_t > &commands, const std::vector< float > &constants) const
	{
		FE_EPTR_FROM_OWNER;
		for (size_t i = 0; i < commands.size (); i++)
		{
			command_t c = commands[i];
	
			switch (c.opcode)
			{
			case op_mov:
				printf ("mov r%d, ", c.dst);
				if (constmask == (c.src0 & parmtypemask))
					printf ("%f\n", constants[c.src0 &~ parmtypemask]);
				else
					printf ("%f\n", engine->parmMgr ()->getFloat (c.src0 &~ parmtypemask));
				break;
			case op_mul:
			case op_div:
			case op_add:
			case op_sub:
			case op_dot:
				printf ("%s r%d, ", strop ((operator_t)c.opcode).c_str (), c.dst);
	
				if (constmask == (c.src0 & parmtypemask))
					printf ("%f, ", constants[c.src0 &~ parmtypemask]);
				else if (varmask == (c.src0 & parmtypemask))
					printf ("%f, ", getvarvalue (c.src0));
				else if (regmask == (c.src0 & parmtypemask))
					printf ("r%d, ", c.src0 &~ parmtypemask);
	
				if (constmask == (c.src1 & parmtypemask))
					printf ("%f\n", constants[c.src1 &~ parmtypemask]);
				else if (varmask == (c.src1 & parmtypemask))
					printf ("%f\n", getvarvalue (c.src1));
				else if (regmask == (c.src1 & parmtypemask))
					printf ("r%d\n", c.src1 &~ parmtypemask);
				break;
			}
		}
	}
	#endif
	
	void mathExpression::deltree (node *n)
	{
		if (n->left)
			deltree (n->left);
		if (n->right)
			deltree (n->right);
		delete n;
	}
	
	mathExpression::mathExpression (void)
	{
		mathExpression("");
	}
	
	mathExpression::mathExpression (const char *expr)
	{
		mName = expr;
		mNumConstants = mNumCommands = 0;
		cStr expression[MATH_MAX_EXPRESSION];
		int exprsize = 0;
		getwords (expr, expression, exprsize);
		bool valid = validate (expression, exprsize);
		if (!valid)
			sys_error ("error in expression '%s'", expr);
	
		node *n = buildtree (expression, exprsize);
		assert (n);
		if (!n)
			sys_error ("failed to build binary tree for expression \"%s\".\n", expr);
		n->root = true;
	
		traverse (n);
		deltree (n);
	
	#ifdef _DEBUG
		printProgram (mCommands, mConstants);
	#endif
	}
	
	mathExpression::~mathExpression (void)
	{
	}
	
	float mathExpression::evaluate (void)
	{
		float registers[maxregisters];
		float *p1, *p2;
	
		for (size_t i = 0; i < mNumCommands; i++)
		{
			command_t c = mCommands[i];
	
			switch (c.opcode)
			{
			case op_mov:
				if (constmask == (c.src0 & parmtypemask))
					registers[c.dst] = mConstants[c.src0 &~ parmtypemask];
				else
					registers[c.dst] = *g_engine->getParmMgr ()->getData (c.src0 &~ parmtypemask);
				break;
			default:
				if (constmask == (c.src0 & parmtypemask))
					p1 = &mConstants[c.src0 &~ parmtypemask];
				else if (varmask == (c.src0 & parmtypemask))
					p1 = getvarvalueptr (c.src0);
				else if (regmask == (c.src0 & parmtypemask))
					p1 = &registers[c.src0 &~ parmtypemask];
				else
					sys_error ("[mathExpression::evaluate] something wrong with mathexpression: '%s'", mName.c_str ());
	
				if (constmask == (c.src1 & parmtypemask))
					p2 = &mConstants[c.src1 &~ parmtypemask];
				else if (varmask == (c.src1 & parmtypemask))
					p2 = getvarvalueptr (c.src1);
				else if (regmask == (c.src1 & parmtypemask))
					p2 = &registers[c.src1 &~ parmtypemask];
				else
					sys_error ("[mathExpression::evaluate] something wrong with mathexpression: '%s'", mName.c_str ());
	
				switch (c.opcode)
				{
				case op_lookup:
					{
						int low = (int)floor (*p2);
						int hi = (int)ceil (*p2);
						float frac = *p2 - low;
	
						int sz = g_engine->getParmMgr ()->getParmSize (c.src0 &~ parmtypemask);
	
						low = low % sz;
						hi = hi % sz;
	
						float l = p1[low];
						float r = p1[hi];
						registers[c.dst] = l * (1-frac) + r * (frac);
					}
					break;
				case op_mul:
					registers[c.dst] = (*p1) * (*p2);
					break;
				case op_div:
					registers[c.dst] = (*p1) / (*p2);
					break;
				case op_add:
					registers[c.dst] = (*p1) + (*p2);
					break;
				case op_sub:
					registers[c.dst] = (*p1) - (*p2);
					break;
				case op_dot:
					{
	//					float d;
	//					d = p1->x * p2->x + p1->y * p2->y + p1->z * p2->z + p1->w * p2->w;
	//					registers[c.dst].x = registers[c.dst].y = registers[c.dst].z = registers[c.dst].w = d;
						registers[c.dst] = 1;
					}
					break;
				}
				break;
			}
		}
		return registers[0];
	}
	
}
