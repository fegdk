/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_HELPERS_H
#define __F_HELPERS_H

#include <assert.h>

namespace fe {

	class genericCallback
	{
	public:
		virtual void call (void *data) = 0;
	};
	
	// this class is used to store/access objects of different types in one list
	class virtualDtorObject
	{
	public:
		virtual ~virtualDtorObject (void) {}
	};
	
	template <class T1, class T2>
	inline const T1 checked_cast (T2 src)
	{
		assert (src);
		assert (dynamic_cast <T1> (src));
		return static_cast <T1> (src);
	}
	
	template <class T> class heapBuffer
	{
	
		// first buffer byte is a refc
	
	private:
		
		unsigned char*		mpBuffer;
	
	public:
	
		heapBuffer (int cnt)
		{
			assert (cnt);
			mpBuffer = new unsigned char[cnt * sizeof (T) + 1];
			*mpBuffer = 1;
		}
	
		heapBuffer (const heapBuffer &b)
		{
			mpBuffer = b.mpBuffer;
			mpBuffer[0]++;
		}
	
		~heapBuffer ()
		{
			if (--mpBuffer[0] == 0)
				delete[] mpBuffer;
		}
	
		operator T* ()
		{
			return (T*) (mpBuffer + 1);
		}
	
	};
	
	template <class T> class noAddrefReleaseOnSmartRefcPtr : public T
	{
	private:
		noAddrefReleaseOnSmartRefcPtr (void) : T (NULL) {}
		virtual void addRef () = 0;
		virtual void release () = 0;
	};
	
	template <class T> class smartPtr
	{
	
	private:
	
		T*		p;
	
	public:
	
		smartPtr (void)
		{
			p = NULL;
		}
	
		smartPtr (T *lp)
		{
			if ( (p = lp) != NULL)
				p->addRef ();
		}
		
		smartPtr (const smartPtr<T>& lp)
		{
			if ( (p = lp.p) != NULL)
				p->addRef ();
		}
	
		~smartPtr ()
		{
			if (p)
				p->release ();
		}
		
		operator T* () const
		{
			return p;
		}
	
	/*	T& operator* () const
		{
			assert (p != NULL);
			return *p;
		}
	
		T** operator& ()
		{
			assert (p == NULL);
			return &p;
		}*/
	
		noAddrefReleaseOnSmartRefcPtr<T>* operator-> () const
		{
//			assert (p!=NULL);
			return (noAddrefReleaseOnSmartRefcPtr<T>*)p;
		}
	
		T* operator= (T* lp)
		{
			T* pp = p;
			if ( (p = lp) != NULL)
				p->addRef ();
			if (pp)
				pp->release ();
			return p;
		}
		
		T* operator= (const smartPtr<T>& lp)
		{
			T* pp = p;
			if ( (p = lp.p) != NULL)
				p->addRef ();
			if (pp)
				pp->release ();
			return p;
		}
	
		template <class N> smartPtr<N> dynamicCast (void) const
		{
			return p ? smartPtr<N> (checked_cast <N*> (p)) : smartPtr<N> (NULL);
		}
	
	};
	
	template <class T> class autoPtr
	{
	
	private:
	
		T*		mpObject;
	
	public:
	
		autoPtr (T *o)
		{
			assert (o);
			mpObject = o;
		}
		
		~autoPtr ()
		{
			delete mpObject;
		}
		
		operator T* ()
		{
			return mpObject;
		}
	
		operator const T* () const
		{
			return mpObject;
		}
	
	};

}


#endif // __F_HELPERS_H

