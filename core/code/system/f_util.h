/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

// $Id$

#ifndef __UTIL_H
#define __UTIL_H

#include "f_math.h"
#include "f_types.h"

namespace fe
{

	ray3
	rayFromMousePos (int x, int y, const matrix4 &viewMatrix, int width, int height, float fov, float nearplane, float farplane);
	
	void
	buildOcclusionPyramid (float n, float f, float a, float fov, const matrix4 &view, int vpw, int vph, const vector4 &L, plane planes[5], vector3 RW[5]);

	uint32
	getUnicodeFromUtf8 (const ubyte *utf8, uint32 *unicode);
	
	uint32
	hashValue (const char *s, int hashSize = 256);
}

#endif // __UTIL_H
