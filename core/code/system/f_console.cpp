/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_console.h"
#include "f_types.h"
#include "f_keycodes.h"
#include "f_timer.h"
#include "f_baseviewport.h"
#include "f_fontft.h"
#include "f_parser.h"
#include "f_error.h"
#include "f_engine.h"
#include "f_drawutil.h"
#include "f_baserenderer.h"
#include "f_resourcemgr.h"
#include "utf8.h"
#include "cvars.h"
#include "f_util.h"

#define CON_MODE_TIME	.5f // time in seconds req. to show/hide console

namespace fe
{
	enum
	{
		CMD_MAX_ARGS = 10,
		CMD_LINE_MAX = 1024
	};

	static int cmd_argc;
	static char *cmd_argv[CMD_MAX_ARGS];
	static char cmd_line[CMD_LINE_MAX];

	int Cmd_Argc (void)
	{
		return cmd_argc;
	}

	const char *Cmd_Argv(int arg)
	{
		if (arg >= CMD_MAX_ARGS)
		{
			fprintf (stderr, "ERROR: cmd arg %d requested, max=%d\n", arg, CMD_MAX_ARGS);
			return NULL;
		}
		return cmd_argv[arg];
	}

	void Con_Printf (const char *fmt, ...)
	{
		char p[1024];
		va_list ap;
		va_start(ap, fmt);
		vsnprintf (p, 1024, fmt, ap);
		va_end(ap);
		g_engine->getConsole ()->msg (p);
		fprintf (stderr, p);
	}

	void Cmd_alias_f (void)
	{
		if (Cmd_Argc () <= 2)
		{
			Con_Printf ("usage: alias name action");
			return;
		}
		smartPtr <console> con = g_engine->getConsole ();
		con->setAlias (Cmd_Argv(1), Cmd_Argv(2));
	}
	
	void Cmd_exec_f (void)
	{
		if (Cmd_Argc () <= 1)
		{
			Con_Printf ("usage: exec filename");
			return;
		}
		charParser p (g_engine->getFileSystem (), Cmd_Argv (1));
		char s[1024];
		for (;;)
		{
			if (p.getToken ())
				break;
			strcpy (s, p.token ());
			while (!p.isEOL ())
			{
				if (p.getToken ())
					p.error ();
				strcat (s, " \"");
				strcat (s, p.token ());
				strcat (s, "\"");
			}
			g_engine->getConsole ()->command (s);
		}
	}
	
	void Cmd_sys_exec_f (void)
	{
		if (Cmd_Argc () <= 1)
		{
			Con_Printf ("usage: sys_exec filename");
			return;
		}
		charParser p (NULL, Cmd_Argv (1), true);
		char s[1024];
		for (;;)
		{
			if (p.getToken ())
				break;
			strcpy (s, p.token ());
			while (!p.isEOL ())
			{
				if (p.getToken ())
					p.error ();
				strcat (s, " \"");
				strcat (s, p.token ());
				strcat (s, "\"");
			}
			g_engine->getConsole ()->command (s);
		}
	}
	
	
	console::console (const char *logFName)
	{
		setName ("console");
		mToggleTime = -10000;
		memset (&mInput, 0, sizeof (mInput));
		mVisible = false;

		mCursor = -1;
		mCurrentCmd = 0;

		mNumAliases = 0;
		mNumCommands = 0;

		// init std commands
		addCommand ("alias", Cmd_alias_f);
		addCommand ("exec", Cmd_exec_f);
		addCommand ("sys_exec", Cmd_sys_exec_f);
	}
	
	console::~console (void)
	{
		fprintf (stderr, "console clean!\n");
	}
	
	void console::render (void)
	{
		smartPtr <fontFT> font = g_engine->getSysFont ();
		float timeDelta = g_engine->getTimer ()->getSysTime () - mToggleTime;
		if (timeDelta > CON_MODE_TIME)
			timeDelta = CON_MODE_TIME;
		float vis = timeDelta / CON_MODE_TIME;
		vis = sin (vis * M_PI / 2);
	
		if (!mVisible)
			vis = 1 - vis;
	
		int width = g_engine->getViewport ()->getWidth ();
		int height = g_engine->getViewport ()->getHeight ();
	
		float conHeight = height / 2.f;
		float h = conHeight * vis;
	
		if (h <= 0)
			return;
		
		if (mCursor == -1)
		{
			int nl = maxNumLines ();
			if (nl != -1)
			{
				mCursor = mMessageHistory.cnt - nl;
				if (mCursor < 0)
					mCursor = 0;
			}
			else
				return;
		}

//		if (mCursor > mMessageHistorySize - maxNumLines ())
//			mCursor = std::max (0, mMessageHistorySize - maxNumLines ());
	
		g_engine->getDrawUtil ()->unbindTexture (0);
		g_engine->getDrawUtil ()->drawPic (0, h-conHeight, width, conHeight, 0, 0, 0, 0, float4 (.0f, .0f, .0f, .69f), true);
		g_engine->getDrawUtil ()->drawPic (0, h-font->getTextHeight ()-font->getLineGap (), width, 1, 0, 0, 0, 0, float4 (1.f, 1.f, 1.f, .37f), true);
	
		int cx, cy;
		font->getTextExtent (mInput.buffer, mInput.cursor, cx, cy);
	
		ulong t = (ulong) (g_engine->getTimer ()->getSysTime () * 1000);
		if ( (t % 500) > 250)
			g_engine->getDrawUtil ()->drawPic (cx, h-font->getTextHeight () - font->getLineGap (), 2, font->getTextHeight () + font->getLineGap (), 0, 0, 0, 0, float4 (0.f, 1.f, 0.f, 1.f), true);
	
	
		// calculate number of lines to be painted
		int numLines = min (maxNumLines (), mMessageHistory.cnt - mCursor);
	
	    // paint msgbuffer
		for (int i = 0; i < numLines; i++)
		{
			int y = (int) ((i) * cy + h - g_engine->getViewport ()->getHeight () / 2);
			font->drawTextStringFormatted (
				mMessageHistory.get (i+mCursor),
				0, y, 1000, 1000,
				0, y, 1000, 1000,
				float4 (1,1,1,1), 0);
		}
	
		// draw user input
		font->drawTextString (mInput.buffer, 0, (int) (h - font->getTextHeight () - font->getLineGap ()), float4 (1,1,1,1));
	}
	
	void console::show (bool show)
	{
		if (mVisible != show)
			mToggleTime = g_engine->getTimer ()->getSysTime ();
		mVisible = show;
	}
	
	void console::toggle (void)
	{
		show (!mVisible);
	}
	
	void console::msg (const char *str)
	{
		// get msgrec from pool
		mMessageHistory.add (str);
		// update mBuffer cursor
		int nl = maxNumLines ();
		if (nl != -1)
		{
			mCursor = mMessageHistory.cnt - nl;
			if (mCursor < 0)
				mCursor = 0;
		}
	}
	
	void console::command (const char *str)
	{
		// parse line
		cStr charcmd = str;
		
		strncpy (cmd_line, str, CMD_LINE_MAX);
		cmd_argc = 0;
	
		int alias = mNumAliases;

		char *p = cmd_line;
		// skip leading spaces
		while (*p <= ' ')
			p++;
		// split
		while (*p)
		{
			if (*p == '\"')
			{
				p++;
				cmd_argv[cmd_argc++] = p;
				while (*p && *p != '\"')
					p++;
				if (*p == 0)
				{
					msg ("WARNING: bad string\n");
				}
			}
			else
			{
				cmd_argv[cmd_argc++] = p;
				while (*p > ' ')
					p++;
				if (*p == 0)
					break;
			}
			*p = 0;
			// trailing spaces
			do {
				p++;
			} while (*p && *p <= ' ');
		}
		if (cmd_argc)
		{
			command_t *cmd = commandFind (cmd_argv[0]);
			if (cmd)
			{
				cmd->exec ();
			}
			else
			{
				cvarManager *cvars = g_engine->getCVarManager ();
				if (cmd_argc == 1)
				{
					// cvar
					const char *val = cvars->stringValue (cmd_argv[0]);
					if (val)
						Con_Printf ("    %s = \"%s\"", cmd_argv[0], val);
				}
				else
				{
					cvars->set (cmd_argv[0], cmd_argv[1], false);
				}
			}
		}
	}

	void field_t::findMatches (const char *name, field_t *obj)
	{
		const char *input = obj->buffer;
		if (strncmp (input, name, strlen (input)))
		{
			return;
		}

		obj->matchCount++;
		if (obj->matchCount == 1)
		{
			strncpy (obj->shortestMatch, name, sizeof (obj->shortestMatch));
			return;
		}

		for (int i = 0; obj->shortestMatch[i]; i++)
		{
			if (i >= strlen (name))
			{
				obj->shortestMatch[i] = 0;
				break;
			}
			if (tolower (obj->shortestMatch[i]) != tolower (name[i]))
				obj->shortestMatch[i] = 0;
		}
	}
	
	void field_t::printMatches (const char *name, field_t *obj)
	{
		const char *input = obj->buffer;
		if (!strncmp (input, name, strlen (input)))
			Con_Printf ("    %s", name);
	}

	void field_t::printCVarMatches (const char *name, field_t *obj)
	{
		const char *input = obj->buffer;
		if (!strncmp (input, name, strlen (input)))
			Con_Printf ("    %s = \"%s\"", name, g_engine->getCVarManager ()->stringValue (name));
	}

	void console::commandCompletion (void (*matchfunc)(const char *cmd, field_t *data), field_t *data)
	{
		for (int i = 0; i < mNumCommands; i++)
		{
			matchfunc (mCommands[i].name, data);
		}
	}

	void field_t::completeCommand (bool use_commands, bool use_cvars)
	{
		matchCount = 0;
		if (use_commands)
			g_engine->getConsole ()->commandCompletion (findMatches, this);
		if (use_cvars)
			g_engine->getCVarManager ()->commandCompletion (findMatches, this);
		if (!matchCount)
			return;
		if (matchCount == 1)
		{
			strncpy (buffer, shortestMatch, MAX_FIELD_SIZE);
			cursor = strlen (buffer);
			return;
		}
		if (use_commands)
			g_engine->getConsole ()->commandCompletion (printMatches, this);
		if (use_cvars)
			g_engine->getCVarManager ()->commandCompletion (printCVarMatches, this);
	}

	bool console::keyDown (int code)
	{
		if (code == '`')
		{
			show (!mVisible);
			return true;
		}

		if (code & Key_CharFlag)
		{
			code &= ~Key_CharFlag;
			if (code > 127 || code == '`') // do not allow non-ascii and ` to be typed
				return true;
			if (mVisible)
			{
				// convert to utf8 and insert
				char str[7];
				int sz = u8_wc_toutf8 (str, code);
				str[sz] = 0;
				memmove (&mInput.buffer[mInput.cursor+sz], &mInput.buffer[mInput.cursor], strlen (mInput.buffer) - mInput.cursor);
				memcpy (&mInput.buffer[mInput.cursor], str, sz);
				mInput.cursor += sz;
				return true;
			}
			
			return false;
		}
		else if (mVisible)
		{
			int incr;
			switch (code)
			{
			case Key_Del:
				if (mInput.buffer[mInput.cursor])
				{
					incr = 0;
					u8_inc (&mInput.buffer[mInput.cursor], &incr);
					memmove (&mInput.buffer[mInput.cursor], &mInput.buffer[mInput.cursor+incr], strlen (mInput.buffer) - mInput.cursor - incr+1);
				}
				break;
			case Key_BackSpace:
				if (mInput.cursor > 0)
				{
					incr = 0;
					u8_dec (&mInput.buffer[mInput.cursor], &incr);
					incr = -incr;
					mInput.cursor-=incr;
					memmove (&mInput.buffer[mInput.cursor], &mInput.buffer[mInput.cursor+incr], strlen (mInput.buffer) - mInput.cursor - incr+1);
				}
				break;
			case Key_Enter:
				command (mInput.buffer);
				mCmdHistory.add (mInput.buffer);
				mCurrentCmd = mCmdHistory.cnt;
				mInput.cursor = 0;
				memset (mInput.buffer, 0, sizeof (mInput.buffer));
				break;
			case Key_RightArrow:
				incr = 0;
				u8_inc (&mInput.buffer[mInput.cursor], &incr);
				mInput.cursor = min (mInput.cursor + incr, (int)strlen (mInput.buffer));
				break;
			case Key_LeftArrow:
				incr = 0;
				u8_dec (&mInput.buffer[mInput.cursor], &incr);
				incr = -incr;
				mInput.cursor = max (mInput.cursor - incr, 0);
				break;
			case Key_Home:
				mInput.cursor = 0;
				break;
			case Key_End:
				mInput.cursor = strlen (mInput.buffer);
				break;
			case Key_PgDn:
				mCursor += maxNumLines () - 1;
				mCursor = min (mCursor, (int)mMessageHistory.cnt - maxNumLines ());
				mCursor = max (0, mCursor);
				break;
			case Key_PgUp:
				mCursor -= maxNumLines () - 1;
				mCursor = min (mCursor, (int)mMessageHistory.cnt - maxNumLines ());
				mCursor = max (0, mCursor);
				break;
			case Key_UpArrow:
				if (mCurrentCmd)
				{
					mCurrentCmd--;
					strcpy (mInput.buffer, mCmdHistory.get (mCurrentCmd));
					mInput.cursor = strlen (mInput.buffer);
				}
				break;
			case Key_DownArrow:
				if (mCurrentCmd < mCmdHistory.cnt)
				{
					mCurrentCmd++;
					if (mCurrentCmd == mCmdHistory.cnt)
					{
						mInput.buffer[0] = 0;
						mInput.cursor = 0;
					}
					else
					{
						strcpy (mInput.buffer, mCmdHistory.get (mCurrentCmd));
						mInput.cursor = strlen (mInput.buffer);
					}
				}
				break;
			case Key_Tab:
				mInput.completeCommand (true, true);
				break;
			default:
				return false;
			}
			return true;
		}
	
		return false;
	}
	
	int console::maxNumLines (void) const
	{
		smartPtr <fontFT> font = g_engine->getSysFont ();
		if (font && g_engine->getViewport ())
			return (g_engine->getViewport ()->getHeight () / 2) / font->getTextHeight () - 2;
		return -1;
	}
	
	void console::addCommand (const char *name, void (*exec)(void))
	{
		if (mNumCommands == MAX_COMMANDS)
		{
			fprintf (stderr, "ERROR: console commands overflow\n");
			return;
		}
		command_t *cmd = &mCommands[mNumCommands++];
		cmd->name = name;
		cmd->exec = exec;
		uint32 hash = hashValue (name, CMD_HASH_SIZE);
		cmd->hashNext = mCommandHash[hash];
		mCommandHash[hash] = cmd;
	}
	
	void console::setAlias (const char *name, const char *value)
	{
		if (mNumAliases == MAX_ALIASES)
		{
			fprintf (stderr, "ERROR: console alias overflow (%s->%s)\n", name, value);
			return;
		}
		strcpy (mAliases[mNumAliases].left, name);
		strcpy (mAliases[mNumAliases].right, value);
	}
	
	console::command_t* console::commandFind (const char *name)
	{
		uint32 hash = hashValue (name, CMD_HASH_SIZE);
		for (command_t *cmd = mCommandHash[hash]; cmd; cmd = cmd->hashNext)
		{
			if (!strcmp (cmd->name, name))
				return cmd;
		}
		return NULL;
	}
	
}

