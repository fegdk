/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_timer.h"
#ifndef _WIN32
#include <sys/time.h>
#endif

namespace fe
{

	static float processTime;

	void
	initProcessTime (void)
	{
	#ifdef _WIN32
		processTime = timeGetTime ()*0.001f;
	#else
		struct timeval tv;
		gettimeofday (&tv, NULL);
		processTime = tv.tv_sec + tv.tv_usec * 0.000001;
	#endif
	}

	#ifdef _WIN32
	float
	getProcessTime (void)
	{
		return timeGetTime ()*0.001f - processTime;
	}
	#else
	float getProcessTime (void)
	{
		struct timeval tv;
		gettimeofday (&tv, NULL);
		return tv.tv_sec + tv.tv_usec * 0.000001- processTime;
	}
	#endif
	
	void sleep (int ms)
	{
	}
	
	timer::timer (void)
	{
		mbPaused = false;
		mPrevTime = 0;
		mCurrentTime = 0;
		mSysPrevTime = 0;
		mSysCurrentTime = 0;
		mStartTime = getProcessTime ();
	}
	
	timer::~timer ()
	{
	}
	
	void		timer::update (void)
	{
		// update timer
		mSysPrevTime = mSysCurrentTime;
		mSysCurrentTime = getProcessTime () - mStartTime;

		mPrevTime = mCurrentTime;
		if (!mbPaused)
			mCurrentTime += mSysCurrentTime - mSysPrevTime;
	}
	
	float		timer::getTime (void) const
	{
		return mCurrentTime;
	}
	
	float		timer::getTimePassed (void) const
	{
		return mCurrentTime - mPrevTime;
	}
	
	float		timer::getSysTime (void) const
	{
		return mSysCurrentTime;
	}
	
	float		timer::getSysTimePassed () const
	{
		return mSysCurrentTime - mSysPrevTime;
	}
	
	void		timer::pause (bool onoff)
	{
		mbPaused = onoff;
		update ();
	}
	
	bool		timer::isPaused (void) const
	{
		return mbPaused;
	}
	
}

