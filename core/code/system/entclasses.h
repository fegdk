#ifndef __ENTCLASSES_H
#define __ENTCLASSES_H

#include "entity.h"
#include "f_math.h"
#include "f_model.h"

namespace fe
{

	enum		// ent classes
	{
		ECLASS_WORLD,
		ECLASS_SPAWNSPOT,
		ECLASS_PHYSXOBJ, // generic object, affected by physics
		ECLASS_MAX = 256,
	};

#define ECLASS_BEGIN(classname, classid) class FE_API classname : public entity {\
	public:\
		classname (uint32 id) : entity (id) {init ();}\
		void init (void);\
		uint32 getClass (void) const {return classid;}
#define ECLASS_END() };

	const int MAX_WORLD_ENTS = 1024;
	class FE_API entWorld : public entity {
	private:
		smartPtr <model> mpModel;
		entityPtr mEnts[MAX_WORLD_ENTS];
		int mNumEnts;
	public:
		void init (void);
		uint32 getClass (void) const {return ECLASS_WORLD;}
		entWorld (uint32 id);
		~entWorld (void);
		void load (const char *fname);
		smartPtr <model> getModel (void) const;
	};
	
	ECLASS_BEGIN (entSpawnSpot, ECLASS_SPAWNSPOT)
	ECLASS_END ()

	ECLASS_BEGIN (entPhysXObj, ECLASS_PHYSXOBJ)
	private:
		smartPtr <sceneObject> mpModel;	// takes transformation from physx
		matrix4 origMtx;
		vector3 initPos;
	public:
		sceneObject *getModel () {
			return mpModel;
		}
		void setModel (sceneObject *mdl) {
			mpModel = mdl;
			origMtx = mdl->getWorldMatrix ();
		}
		const matrix4& getOrigMtx (void) {
			return origMtx;
		}
		void setInitPos (const vector3 &pos) {
			initPos = pos;
		}
		const vector3& getInitPos (void) const {
			return initPos;
		}
	ECLASS_END ()

#undef ECLASS_BEGIN
#undef ECLASS_END

}

#endif // __ENTCLASSES_H
