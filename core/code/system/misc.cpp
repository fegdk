#include "f_types.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

namespace fe {

void sys_error (const char *fmt, ...) {
	int n;
	const int size = 1024;
	char p[size];
	va_list ap;

	va_start(ap, fmt);
	n = vsnprintf(p, size, fmt, ap);
	va_end(ap);
	if (n > -1 && n < size)
		sys_printf ("ERROR: %s\n", p);
	else
		sys_printf ("ERROR: [err string is too large to format]\n");
	sys_exit (-1);
}

void
sys_exit (int errcode) {
	exit (-1);
}

}
