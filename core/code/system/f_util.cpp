/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_util.h"

namespace fe
{

	ray3 rayFromMousePos (int x, int y, const matrix4 &viewMatrix, int width, int height, float fov, float nearplane, float farplane)
	{
		matrix4 invview = viewMatrix.inverse();
	
		float size = ( float )( 2.0f * nearplane * tan( fov / 2.0f ) );
		vector3 dir;
		dir.x = ( x - width / 2.f ) * size / height;
		dir.y = -( y - height / 2.f ) * size / height;
		dir.z = nearplane;
	
		dir = dir * invview;
	
		vector3 origin = (vector3&)invview._41;
		dir -= origin;
		dir.normalize();
		
		ray3 ray;
		ray.origin() = origin;
		ray.direction() = dir;
		return ray;
	}
	
	void buildOcclusionPyramid (float n, float f, float a, float fov, const matrix4 &view, int vpw, int vph, const vector4 &L, plane planes[5], vector3 RW[5])
	{
		// equations were taken from Eric Lengyel gamasutra article "The Mechanics of Robust Stencil Shadows"
		float e = 1.f / tan (fov/2);
		float d = (L * view).dot (vector4 (0, 0, -1, -n));
		const float eps = 0.001f;
	
		matrix4 invview = view.inverse ();
		
		vector3 R[4], N[4];
	
		if (fabs (d) > eps)
		{
			R[0] = vector3 (n/e, a*n/e, -n);
			R[1] = vector3 (-n/e, a*n/e, -n);
			R[2] = vector3 (-n/e, -a*n/e, -n);
			R[3] = vector3 (n/e, -a*n/e, -n);
	
			// translate to world space
			RW[0] = R[0] * invview;
			RW[1] = R[1] * invview;
			RW[2] = R[2] * invview;
			RW[3] = R[3] * invview;
			RW[4] = (vector3 &)invview._41;
	
			// calc normals
			for (int i = 0; i < 4; i++)
			{
				N[i] = (RW[i] - RW[(i-1)%4]).cross (vector3 (L.x, L.y, L.z) - L.w * RW[i]);
				if (d < -eps)
					N[i].negate ();
			}
	
			// calc side planes
			for (int i = 0; i < 4; i++)
			{
				planes[i] = vector4 (N[i].x, N[i].y, N[i].z, -N[i].dot (RW[i])) * (1.f/N[i].length ());
			}
	
			// calc near plane
			planes[4] = plane (0, 0, -1, -n) * invview;
			if (d < -eps)
				planes[4].negate ();
		}
	}

	uint32 getUnicodeFromUtf8 (const ubyte *utf8, uint32 *unicode)
	{
		uint32 nsymbols = 0;
		for (;; ++nsymbols)
		{
			uint32 c;
			int noOctets = 0;
			int firstOctetMask = 0;
			unsigned char firstOctet = utf8[0];
			if (firstOctet < 0x80)
			{
				noOctets = 1;
				firstOctetMask = 0x7F;
			}
			else if ((firstOctet & 0xE0) == 0xC0)
			{
				noOctets = 2;
				firstOctetMask = 0x1F;
			}
			else if ((firstOctet & 0xF0) == 0xE0)
			{
				noOctets = 3;
				firstOctetMask = 0x0F;
			}
			else if ((firstOctet & 0xF8) == 0xF0)
			{
				noOctets = 4;
				firstOctetMask = 0x07;
			}
			else if ((firstOctet & 0xFC) == 0xF8)
			{
				noOctets = 5;
				firstOctetMask = 0x03;
			}
			else if ((firstOctet & 0xFE) == 0xFC)
			{
				noOctets = 6;
				firstOctetMask = 0x01;
			}
			else
			{
				unicode[nsymbols] = 0;	// invalid utf8 char
				return nsymbols;
			}

			c = firstOctet & firstOctetMask;
			for (int i = 1; i != noOctets; ++i)
			{
				if ((utf8[i] & 0xC0) != 0x80)
				{
					unicode[nsymbols] = 0;
					return nsymbols;
				}
				c <<= 6;
				c |= utf8[i] & 0x3F;
			}

			unicode[nsymbols] = c;
			utf8 += noOctets;
		}

		return 0;	// never actually used
	}
	
	uint32
	hashValue (const char *s, int hashSize)
	{
		uint32 hash = 0;
		int i = 0;
		while (s[i])
		{
			hash += tolower (s[i]) * (i+100);
			i++;
		}
		hash &= (hashSize-1);
		return hash;
	}
}
