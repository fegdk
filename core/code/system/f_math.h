/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_MATH_H
#define __F_MATH_H

#include <math.h>
#include <assert.h>
#include <string.h>
#include "f_types.h"

namespace fe
{

	class FE_API vector3
	{
	public:
	
	    static const vector3 zero;
	    static const vector3 unitX;
	    static const vector3 unitY;
	    static const vector3 unitZ;
	
		float x, y, z;
			
		vector3 (void) {}
		
		vector3 (float xx, float yy, float zz)
		{
			x = xx;
			y = yy;
			z = zz;		
		}
	
		vector3 (const float *xyz)
		{
			x = xyz[0];
			y = xyz[1];
			z = xyz[2];
		}
		
		float &operator [] (int i)
		{
			assert (i < 3);
			return (&x)[i];
		}
		
		const float &operator [] (int i) const
		{
			assert (i < 3);
			return (&x)[i];
		}
	
		operator float * (void)
		{
			return &x;
		}
		
		vector3 cross (const vector3 &v) const
		{
			return vector3 (y * v.z - z * v.y, z * v.x- x * v.z, x * v.y - y * v.x);
		}
	
		float dot (const vector3 &v) const
		{
			return x * v.x + y * v.y + z * v.z;
		}
		
		// rotates a vector around an arbitrary axis
		// should apply to point rotation too,
		// since the routine doesn't need normalized values
		vector3  rotateAroundAxis (const vector3 &axis, float angle)
		{
			vector3 up = cross (axis);
			vector3 ret;
			float sint = (float)sin (angle);
			float cost = (float)cos (angle);
			ret.x = x * cost + up.x * sint;
			ret.y = y * cost + up.y * sint;
			ret.z = z * cost + up.z * sint;
			return ret;
		}
	
		bool operator == (const vector3 &v) const
		{
			return x == v.x && y == v.y && z == v.z;
		}
		
		vector3 operator / (float f) const
		{
			return vector3 (x / f, y / f, z / f);
		}
		
		vector3& operator /= (float f)
		{
			x /= f;
			y /= f;
			z /= f;
			return *this;
		}
		
		vector3& operator += (const vector3 &v)
		{
			x += v.x;
			y += v.y;
			z += v.z;
			return *this;
		}
		
		vector3& operator -= (const vector3 &v)
		{
			x -= v.x;
			y -= v.y;
			z -= v.z;
			return *this;
		}
		
		vector3 operator - (const vector3 &v) const
		{
			return vector3 (x - v.x, y - v.y, z - v.z);
		}
		
		vector3 operator - (void) const
		{
			return vector3 (-x, -y, -z);
		}
	
		void negate (void)
		{
			x = -x;
			y = -y;
			z = -z;
		}
		
		vector3 operator + (const vector3 &v) const
		{
			return vector3 (x + v.x, y + v.y, z + v.z);
		}
	
		float length (void) const
		{
			return sqrt (dot (*this));
		}
		
		float squaredLength (void) const
		{
			return dot (*this);
		}
		
		float normalize (float tolerance = 1.0e-6f)
		{
			float d = length ();
			if (d > tolerance)
			{
				float id = 1.f / d;
				x *= id;
				y *= id;
				z *= id;
			}
			else
				d = 0;
	
			return d;
		}
	
		friend vector3 operator * (const vector3 &v, float f)
		{
			return vector3 (f*v.x, f*v.y, f*v.z);
		}
	
		friend vector3 operator * (float f, const vector3 &v)
		{
			return vector3 (f*v.x, f*v.y, f*v.z);
		}
	
	};
	
	class vector2
	{
	public:
		float x, y;
	
		vector2 (void)
		{
		}
		
		vector2 (float xx, float yy)
		{
			x = xx;
			y = yy;
		}
	
		bool operator == (const vector2 &v) const
		{
			return x == v.x && y == v.y;
		}
		
		float &operator [] (int i)
		{
			assert (i < 3);
			return (&x)[i];
		}
		
		const float &operator [] (int i) const
		{
			assert (i < 3);
			return (&x)[i];
		}
	};
	
	#ifndef M_PI
	#define M_PI 3.14159f
	#endif
	
	inline void MakeNormalVectors (const vector3 &front, vector3 &right, vector3 &up)
	{
		float		d;
	
		// this rotate and negate guarantees a vector
		// not colinear with the original
		right.y = -front.x;
		right.z = front.y;
		right.x = front.z;
	
		d = right.dot (front);
		right = right - d * front;
		right.normalize ();
		up = right.cross (front);
	}
	
	class ray3
	{
			
	private:
			
		vector3 mOrigin;
		vector3 mDirection;
		
	public:
		
		const vector3& origin (void) const
		{
			return mOrigin;
		}
		
		vector3& origin (void)
		{
			return mOrigin;
		}
		
		const vector3& direction (void) const
		{
			return mDirection;
		}
		
		vector3& direction (void)
		{
			return mDirection;
		}
		
	};
	
	
	#if 0
	inline vector3 VectorMA (const vector3 &va, const double &scale, const vector3 &vb)
	{
		vector3 vc;
		vc.x = va.x + scale * vb.x;
		vc.y = va.y + scale * vb.y;
		vc.z = va.z + scale * vb.z;
		return vc;
	}
	
	inline void MakeNormalVectors (const vector3 &forward, vector3 &right, vector3 &up)
	{
		float		d;
	
		// this rotate and negate guarantees a vector
		// not colinear with the original
		right.y = -forward.x;
		right.z = forward.y;
		right.x = forward.z;
	
		d = right | forward;
		right = VectorMA (right, -d, forward);
		right.normalize ();
		up = right ^ forward;
	}
	#endif
	
	class FE_API vector4
	{
	public:
		
		float x, y, z, w;
		
		vector4 ()
		{
		}
	
		vector4 (const vector4 &v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}
	
		vector4 (float _x, float _y, float _z, float _w)
		{
			x=_x;
			y=_y;
			z=_z;
			w=_w;
		}
	
		float &operator [] (int i)
		{
			assert (i < 4);
			return (&x)[i];
		}
	
		const float &operator [] (int i) const 
		{
			assert (i < 4);
			return (&x)[i];
		}
	
		void operator = (const vector3 &vec)
		{
			x = vec.x;
			y = vec.y;
			z = vec.z;
			w = 1.f;
		}
	
		vector4 operator * (float d) const
		{
			return vector4 (x * d, y * d, z * d, w * d);
		}
	
		vector4 operator - (void) const
		{
			return vector4 (-x, -y, -z, -w);
		}
	
		void negate (void)
		{
			x = -x;
			y = -y;
			z = -z;
			w = -w;
		}
	
		float dot (const vector4 &v) const
		{
			return x * v.x + y * v.y + z * v.z + w * v.w;
		}
		
	};
	
	class matrix4 {
	public:
		matrix4 () {}
	    matrix4 (const float *)
		{
		}
	    matrix4 (bool init)
		{
			if (init)
			{
				_11 = 1;
				_12 = 0;
				_13 = 0;
				_14 = 0;
				_21 = 0;
				_22 = 1;
				_23 = 0;
				_24 = 0;
				_31 = 0;
				_32 = 0;
				_33 = 1;
				_34 = 0;
				_41 = 0;
				_42 = 0;
				_43 = 0;
				_44 = 1;
			}
		}
	    matrix4 (const matrix4& m)
		{
			_11 = m._11;
			_12 = m._12;
			_13 = m._13;
			_14 = m._14;
			_21 = m._21;
			_22 = m._22;
			_23 = m._23;
			_24 = m._24;
			_31 = m._31;
			_32 = m._32;
			_33 = m._33;
			_34 = m._34;
			_41 = m._41;
			_42 = m._42;
			_43 = m._43;
			_44 = m._44;
		}
	    matrix4 (float m00, float m01, float m02, float m03,
	                float m10, float m11, float m12, float m13,
	                float m20, float m21, float m22, float m23,
	                float m30, float m31, float m32, float m33)
		{
			_11 = m00;
			_12 = m01;
			_13 = m02;
			_14 = m03;
			_21 = m10;
			_22 = m11;
			_23 = m12;
			_24 = m13;
			_31 = m20;
			_32 = m21;
			_33 = m22;
			_34 = m23;
			_41 = m30;
			_42 = m31;
			_43 = m32;
			_44 = m33;
		}
		union {
			
			struct {
				float _11, _12, _13, _14;
				float _21, _22, _23, _24;
				float _31, _32, _33, _34;
				float _41, _42, _43, _44;
			};
			
			float m[4][4];
	
		};
	
		float &operator [] (int i) {
			return (&_11)[i];
		}
	
		void translate (const vector3& where) {
			identity ();
			_41 = where.x;
			_42 = where.y;
			_43 = where.z;
		}
		
		void translate (float x, float y, float z) {
			identity ();
			_41 = x;
			_42 = y;
			_43 = z;
		}
	
		void rotateX (const float rads) {
			identity ();
			_22= (float)cos (rads);
			_23= (float)sin (rads);
			_32=- (float)sin (rads);
			_33= (float)cos (rads);
		}
	    
		void rotateY (const float rads) {
	
			identity ();
			_11= (float)cos (rads);
			_13=- (float)sin (rads);
			_31= (float)sin (rads);
			_33= (float)cos (rads);
		}
	
		void rotateZ (const float rads) {
			identity ();
			_11= (float)cos (rads);
			_12= (float)sin (rads);
			_21=- (float)sin (rads);
			_22= (float)cos (rads);
		}
	
		void rotateAroundAxis (const vector3 &axis, const float angle)
		{		
			identity ();
			
			float		cost;
			float		sint;
			vector3	v;
	
			cost = (float)cos (angle);
			sint = (float)sin (angle);
			v = axis;
			v.normalize ();
	
			_11 = (v[0] * v[0]) * (1.0f - cost) + cost;
			_12 = (v[0] * v[1]) * (1.0f - cost) - (v[2] * sint);
			_13 = (v[0] * v[2]) * (1.0f - cost) + (v[1] * sint);
	
			_21 = (v[1] * v[0]) * (1.0f - cost) + (v[2] * sint);
			_22 = (v[1] * v[1]) * (1.0f - cost) + cost ;
			_23 = (v[1] * v[2]) * (1.0f - cost) - (v[0] * sint);
	
			_31 = (v[2] * v[0]) * (1.0f - cost) - (v[1] * sint);
			_32 = (v[2] * v[1]) * (1.0f - cost) + (v[0] * sint);
			_33 = (v[2] * v[2]) * (1.0f - cost) + cost;
	
			_14 = _24 = _34 = 0.0f;
			_41 = _42 = _43 = 0.0f;
			_44 = 1.0f;
		}
	
		void fromEulerAnglesXYZ (float ax, float ay, float az);
	
		friend vector3 operator * (const matrix4& m, const vector3& v) {
			vector3 r;
	
			float                       x;
			float                       y;
			float                       z;
			float                       w;
	    
			w = v.x * m._14 + v.y * m._24 + v.z * m._34 + m._44;
			
			if (fabs (w) < 1.0e-5f)
				return vector3 (0, 0, 0);
	
			x = v.x * m._11 + v.y * m._21 + v.z * m._31 + m._41;
			y = v.x * m._12 + v.y * m._22 + v.z * m._32 + m._42;
			z = v.x * m._13 + v.y * m._23 + v.z * m._33 + m._43;
	
			r.x = x/w;
			r.y = y/w;
			r.z = z/w;
	
			return r;
		}
	
		friend vector3 operator * (const vector3& v, const matrix4& m) {
			vector3 r;
	
			float                       x;
			float                       y;
			float                       z;
			float                       w;
	    
			w = v.x * m._14 + v.y * m._24 + v.z * m._34 + m._44;
			
			if (fabs (w) < 1.0e-5f)
				return vector3 (0, 0, 0);
	
			x = v.x * m._11 + v.y * m._21 + v.z * m._31 + m._41;
			y = v.x * m._12 + v.y * m._22 + v.z * m._32 + m._42;
			z = v.x * m._13 + v.y * m._23 + v.z * m._33 + m._43;
	
			r.x = x/w;
			r.y = y/w;
			r.z = z/w;
	
			return r;
		}
		
		friend vector4 operator * (const vector4& v, const matrix4& m) {
			vector4 r;
	
	//		float                       x;
	//		float                       y;
	//		float                       z;
	//		float                       w;
	    
			
	//		if (fabs (w) < 1.0e-5f)
	//			return vector3 (0, 0, 0);
	
			r.x = v.x * m._11 + v.y * m._21 + v.z * m._31 + v.w * m._41;
			r.y = v.x * m._12 + v.y * m._22 + v.z * m._32 + v.w * m._42;
			r.z = v.x * m._13 + v.y * m._23 + v.z * m._33 + v.w * m._43;
			r.w = v.x * m._14 + v.y * m._24 + v.z * m._34 + v.w * m._44;
	
	/*		r.x = x/w;
			r.y = y/w;
			r.z = z/w;
			r.w = w/w;*/
	
			return r;
		}
	
		matrix4 operator * (const matrix4 &b) const {
			matrix4 ret;
			for (int i=0; i<4; i++) {
				for (int j=0; j<4; j++) {
					ret.m[i][j] = 0.0f;
					for (int k=0; k<4; k++) {
						ret.m[i][j] += m[i][k] * b.m[k][j];
					}
				}
			}
			return ret;
		}
	
		void identity () {
			_11=_22=_33=_44=1;
			_12=_13=_14=_21=_23=_24=_31=_32=_34=_41=_42=_43=0;
		}
	
		matrix4 transpose () const {
			matrix4 ret;
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					ret.m[i][j] = m[j][i];
				}
			}
			return ret;
		}
	
		vector3 transform (const vector3& v) const {
			vector3 r;
	
			r.x = v.x * _11 + v.y * _21 + v.z * _31;
			r.y = v.x * _12 + v.y * _22 + v.z * _32;
			r.z = v.x * _13 + v.y * _23 + v.z * _33;
	
			return r;
		}
	
		int projection (const float fFOV, const float fAspect, const float fNearPlane, const float fFarPlane) {
	
			float w, h, Q;
			if (fabs (fFarPlane-fNearPlane) < 0.01f)
				return -1;
			if (fabs (sin (fFOV/2)) < 0.01f)
				return -1;
	
			w = fAspect * ( (float)cos (fFOV/2)/ (float)sin (fFOV/2));
			h = 1.0f  * ( (float)cos (fFOV/2)/ (float)sin (fFOV/2));
			Q = fFarPlane / (fFarPlane - fNearPlane);
	
			memset (this, 0, sizeof (matrix4));
			_11 = w;
			_22 = h;
			_33 = Q;
			_34 = 1.0f;
			_43 = -Q*fNearPlane;
	
			return 0;
	
		}
	
		int camera (const vector3 &from, const vector3 &at, const vector3 &worldUp) {
	
			vector3	vView;
			vector3	vUp;
			vector3	vRight;
			float	flength;
			float	fdotProduct;
	
			//Get the z basis vector, which points straight ahead. This is the
			//difference from the eyepoint to the lookat point.
			vView = at - from;
			flength = vView.length ();
	
			//TODO: error handling
			if (flength < 1.0e-5f)
				return -1;
	
			//normalize the z basis vector
			vView /= flength;
	
			//Get the dot product, and calculate the projection of the z basis
			//vector onto the up vector. The projection is the y basis vector.
			fdotProduct = worldUp.dot (vView);
	
			vUp = worldUp - fdotProduct * vView;
	    
			//If this vector has near-zero length because the input specified a
			//bogus up vector, let's try a default up vector
			flength = vUp.length ();
	
			if (flength < 1.0e-5f) {
				vUp = vector3 (0.0f, 1.0f, 0.0f) - vView.y * vView;
	        
				//If we still have near-zero length, resort to a different axis
				flength = vUp.length ();
	
				if (flength < 1.0e-5f) {
					vUp = vector3 (0.0f, 0.0f, 1.0f) - vView.z * vView;
	            
					flength = vUp.length ();
					if (flength < 1.0e-5f)
						return -1;
				}
			}
	
			//normalize the y basis vector
			vUp/=flength;
	
			//The x basis vector is found simply with the cross product of the y
			//and z basis vectors
			vRight = vUp.cross (vView);
	
			//Start building the matrix. The first three rows contains the basis
			//vectors used to rotate the view to point at the lookat point
			identity ();
	
			_11 = vRight.x;
			_12 = vUp.x;
			_13 = vView.x;
	
			_21 = vRight.y;
			_22 = vUp.y;
			_23 = vView.y;
	    
			_31 = vRight.z;    
			_32 = vUp.z;    
			_33 = vView.z;
	    
			//Do the translation values (rotations are still about the eyepoint)
			_41 = - from.dot (vRight);
			_42 = - from.dot (vUp);
			_43 = - from.dot (vView);
	
			return 0;
		}
	
		void orthoOffCenterLH (float l, float r, float b, float t, float zn, float zf)
		{
			_11 = 2/ (r-l);      _12 = 0;            _13 = 0;           _14 = 0;
			_21 = 0;            _22 = 2/ (t-b);      _23 = 0;           _24 = 0;
			_31 = 0;            _32 = 0;            _33 = 1/ (zf-zn);   _34 = 0;
			_41 = (l+r)/ (l-r);  _42 = (t+b)/ (b-t);  _43 = zn/ (zn-zf);  _44 = 1;
		}
	
		void orthoLH (float w, float h, float zn, float zf)
		{
			_11 = 2/w;      _12 = 0;		_13 = 0;			_14 = 0;
			_21 = 0;        _22 = 2/h;		_23 = 0;			_24 = 0;
			_31 = 0;        _32 = 0;		_33 = 1/ (zf-zn);	_34 = 0;
			_41 = 0;		_42 = 0;		_43 = zn/ (zn-zf);	_44 = 1;
		}
	
		void ortho (float w, float h)
		{
			identity ();
			_11 =  2.0f / w;
			_22 = -2.0f / h;
			_33 =  1.0f;
			_44 =  1.0f;
			_41 = -1;
			_42 =  1;
		}
	
		matrix4 inverse (void) const
		{
			matrix4 r;
	
			float                       fDetInv;
	
			if (fabs (_44 - 1.0f) > 1.0e-3f)
			{
				r.identity ();
				return r;
			}
	
			if (fabs (_14) > 1.0e-3f || 
				fabs (_24) > 1.0e-3f || 
				fabs (_34) > 1.0e-3f)
			{
				r.identity ();
				return r;
			}
	
			fDetInv = 1.0f / (_11 * (_22 * _33 - _23 * _32) -
							   _12 * (_21 * _33 - _23 * _31) +
							   _13 * (_21 * _32 - _22 * _31));
	
			r._11 =  fDetInv * (_22 * _33 - _23 * _32);
			r._12 = -fDetInv * (_12 * _33 - _13 * _32);
			r._13 =  fDetInv * (_12 * _23 - _13 * _22);
			r._14 = 0.0f;
	
			r._21 = -fDetInv * (_21 * _33 - _23 * _31);
			r._22 =  fDetInv * (_11 * _33 - _13 * _31);
			r._23 = -fDetInv * (_11 * _23 - _13 * _21);
			r._24 = 0.0f;
	
			r._31 =  fDetInv * (_21 * _32 - _22 * _31);
			r._32 = -fDetInv * (_11 * _32 - _12 * _31);
			r._33 =  fDetInv * (_11 * _22 - _12 * _21);
			r._34 = 0.0f;
	
			r._41 = - (_41 * r._11 + _42 * r._21 + _43 * r._31);
			r._42 = - (_41 * r._12 + _42 * r._22 + _43 * r._32);
			r._43 = - (_41 * r._13 + _42 * r._23 + _43 * r._33);
			r._44 = 1.0f;
	
			return r;
		}	
	};
	
	class plane : public vector4 {
			
	public:
	
		plane (void) {}
	
		plane (float x, float y, float z, float w)
			: vector4 (x, y, z, w)
		{
		}
	
		plane (const vector4 &v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}
	
		void fromPoints (const vector3& a, const vector3& b, const vector3& c) {
			normal () = (b - a).cross (c - b);
			normal ().normalize ();
			constant () = normal ().dot (a);
		}
	
		float distanceTo (const vector3& pt) const
		{
			return pt.dot (normal ()) - constant ();
		}
	
		vector3 projectPoint (const vector3& pt) const
		{
			return pt - normal () * distanceTo (pt);
		}
	
		const vector3& normal (void) const
		{
			return (const vector3 &)x;
		}
	
		float constant (void) const
		{
			return w;
		}
		
		vector3& normal (void)
		{
			return (vector3 &)x;
		}
	
		float& constant (void)
		{
			return w;
		}
	};
	
	class box3
	{
	public:
	    box3 (void)
		{
		}
	    box3 (bool zero)
		{
			if (zero)
			{
			    mCenter = vector3::zero;
			    mAxis[0] = vector3::unitX;
			    mAxis[1] = vector3::unitY;
			    mAxis[2] = vector3::unitZ;
			    mExtent[0] = mExtent[1] = mExtent[2] = 0;
			}
		}
	
	    vector3& center ()
		{
			return mCenter;
		}
	    const vector3& center () const
		{
			return mCenter;
		}
	
	    vector3& axis (int i)
		{
			return mAxis[i];
		}
	
	    const vector3& axis (int i) const
		{
			return mAxis[i];
		}
	
	    vector3* axes ()
		{
			return mAxis;
		}
	
	
	    const vector3* axes () const
		{
			return mAxis;
		}
	
	    float& extent (int i)
		{
			return mExtent[i];
		}
	
	    const float& extent (int i) const
		{
			return mExtent[i];
		}
	
	    float* extents ()
		{
			return mExtent;
		}
	
	    const float* extents () const
		{
			return mExtent;
		}
	
	    void computeVertices (vector3 akVertex[8]) const
		{
			// TODO: implement this
		}
	
	protected:
	    vector3 mCenter;
	    vector3 mAxis[3];
	    float mExtent[3];
	};


	struct aab3 {
		vector3 mins;
		vector3 maxs;
	};
	
	void FE_API calcTriangleBasis (const vector3& E, const vector3& F, const vector3& G,
			float sE, float tE, float sF, float tF, float sG, float tG,
			vector3 &tangentX, vector3 &tangentY);
	
	vector3 FE_API orthogonalize (const vector3 &v1, const vector3 &v2);
	
	vector3 FE_API closestPointOnLine (const vector3 &a, const vector3 &b, const vector3 &p);
	
}

#endif // __F_MATH_H

