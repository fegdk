#ifndef __F_FILESYSTEM_H
#define __F_FILESYSTEM_H

#include "f_string.h"
#include "f_baseobject.h"

struct z_stream_s;
namespace fe {

	class file;
	class matrix4;
	
	enum { MAX_FILE_NAME = 64 };
	
	enum fileMode_t
	{
		FS_MODE_READ,
	};
	
	enum fsBlockType
	{
		fs_binaryCompressed	=	0x00000001,
		fs_binaryRawData	=	0x00000002,
		fs_directory		=	0x00000003,
	};
	
	struct fsFileEntry;
	const int FS_MAX_PAKNAME = 128;
	const int FS_MAX_BASEPATHNAME = 128;
	const int FS_MAX_PAKS = 32;
	const int FS_MAX_BASEPATHES = 5;
	
	class fileSystem : public baseObject
	{
	
	protected:
	
		fsFileEntry* mpRootFolder;
		
		char mPakFiles[FS_MAX_PAKS][FS_MAX_PAKNAME];
		int mNumPaks;
		char mBasePathes[FS_MAX_BASEPATHES][FS_MAX_BASEPATHNAME];
		int mNumBasePathes;
	
		void			initNormal (fsFileEntry *, const char *path, int basepath);
		void			initPaks (fsFileEntry *, const char *path, int basepath);
		void			loadPakFile (int pakfile, int basepath);
		void			loadPakFileStructure (int pakfile, FILE *fp, fsFileEntry *parent, int basepath);
	
		~fileSystem ();
	
	public:
	
		fileSystem (void);
	
		void init (const char *pathlist);
		void reset (const char *pathlist);
		void cleanUp (void);
		file* openFile (const char *fname, fileMode_t openMode = FS_MODE_READ);
//		int getFileList (const char *path, const char *extension, std::vector <cStr> &names);
		const char *getBasePath (int idx) const;
		const char*		getPakFile (int i) const;
	};
	
	class file
	{
		friend class fileSystem;
	
	protected:
	
		fsFileEntry*				mpEntry;
		cStr						mName;
		FILE*						mpFile;
		z_stream_s*					mpZStream;
		uchar*						mpZBuffer;
		ulong						mOffset;	// seek position
		ulong						mZOffset;	// z seek position
	
		void open (fileMode_t openMode);
		file (fsFileEntry *, const char *);
		~file ();
	
	public:
	
		long			tell (void) const;
		int				seek (int offset, int whence);
		int				read (void *buffer, ulong size);
		int				readUINT16 (uint16 *buffer, int num);
		int				readUINT32 (uint32 *buffer, int num);
		int				readFloat (float *buffer, int num);
		int				readMatrix4 (matrix4 *buffer, int num);
		int				getSize (void) const;
		void			close (void);
		const char*		name (void) const;
	};
	
}

#endif // __F_FILESYSTEM_H

