/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_ENGINE_H
#define __F_ENGINE_H

#include "config.h"
#include "f_baseobject.h"
#include "f_helpers.h"

namespace fe
{

	class fileSystem;
	class baseViewport;
	class inputDevice;
	class application;
	class effect;
	class timer;
	class console;
	class effectParms;
	class parmMgr;
	class baseRenderer;
#ifndef SOUND_DISABLED
	class baseSoundDriver;
#endif
	class resourceMgr;
	class texture;
	class drawUtil;
	class rBackend;
	class logger;
	class sceneManager;
	class keyBinder;
	class script;
	class entityManager;
	class metaShader;
	class fontFT;
	class animSequencer;
	class stackAllocator;
	class cvarManager;
	
	struct cvar_t;
	extern cvar_t *sys_int_mainloop;
	extern cvar_t *sys_profile;
	extern cvar_t *sys_mainscript;
	extern cvar_t *sys_debug_freetype;
	extern cvar_t *sys_stack_size;

	class FE_API engine : public baseObject
	{
	
	private:
	
		/**
		 * Specifies size of buffer returned by tmpBuffer
		 * FIXME: should be set by configure script
		 */
		enum { default_tmpbuffer_size = 65536 };
		ubyte* mpTmpBuffer;
	
		smartPtr <baseRenderer>			mpRenderer;
		smartPtr <baseViewport>			mpViewport;
		smartPtr <fileSystem>			mpFileSystem;
		smartPtr <inputDevice>			mpInputDevice;
		smartPtr <application>			mpApp;
		smartPtr <logger>				mpLogger;
		smartPtr <timer>				mpTimer;
		smartPtr <console>				mpConsole;
		smartPtr <effectParms>			mpEffectParms;
		smartPtr <parmMgr>				mpParmMgr;
#ifndef SOUND_DISABLED
		smartPtr <baseSoundDriver>		mpSoundDriver;
#endif
		smartPtr <resourceMgr>			mpResourceMgr;
		smartPtr <drawUtil>				mpDrawUtil;
		smartPtr <rBackend>				mpRBackend;
		smartPtr <sceneManager>			mpSceneManager;
		smartPtr <keyBinder>			mpKeyBinder;
		smartPtr <script>				mpScript;
		smartPtr <entityManager>		mpEntManager;
		smartPtr <metaShader>			mpMetaShader;
		smartPtr <fontFT>				mpSysFont;
		smartPtr <animSequencer>		mpAnimSequencer;
		smartPtr <stackAllocator>		mpStackAllocator;
		smartPtr <cvarManager>			mpCVarManager;
	
		// initializes subsystems which are *shared* between all engine usage patterns
		// i mean different 'run' methods
		void sharedInit (void);
		void initResourceMgr (void);
	
		// platform-specific stuff
		void mainLoop (void);
		void beginScene (void);
		void endScene (void);
	
	public:
		
		engine (void);
		~engine (void);
		
		/**
		 * Use this one if your application has own renderer/mainloop (e.g. FEditor uses this scheme).
		 * Be aware that this method is gonna be abandoned someday, cause it causes compatibility problems.
		 * @param renderer pointer to a renderer object
		 * @param dataPath VFS root
		 */
		void								run (const smartPtr <baseRenderer> &renderer, const char *dataPath = "./");
		
		/**
		 * Use this if u wanna use internal renderer/mainloop (see feApplication for reference and usage tutorial)
		 * @param app pointer to application object
		 */
		void								run (const smartPtr <application> &app, int argc, char *argv[]);
	
		// TODO: do 2 more 'run' methods -- internal renderer vs own mainloop; internal mainloop vs own renderer
	
		/**
		 * This can be used to close application by user choice
		 * console 'quit' command uses this
		 */
		void								quit (void);
	
		/**
		 * Normal engine tick, performs usual stuff -- update states, render stuff, etc
		 * Shouldn't it be private? :-/
		 */
		void								tick (void);
	
		/**
		 * Use that if u need to emulate keydown event
		 */
		void								keyDown (int code);
	
		/**
		 * Use that when ur gonna emulate keyup event
		 */
		void								keyUp (int code);
		
		/**
		 * @return application pointer.
		 * \deprecated Some subsystems used this to check renderer requirements, etc. It is now controlled through cvars.
		 */
		application*			getApplication (void) const;
	
		/**
		 * Renderer access.
		 * You don't gonna need one in usual portable application.
		 * You may use it to query supported vidmodes, renderer info, etc.
		 * @return renderer pointer.
		 */
		baseRenderer*			getRenderer (void) const;
	
#ifndef SOUND_DISABLED
		/**
		 * @return sound mixer object pointer
		 */
		smartPtr <baseSoundDriver>			getSoundDriver (void) const;
#endif
	
		/**
		 * @return the main filesystem object used by the engine to load resources
		 */
		fileSystem*			getFileSystem (void) const;
		
		/**
		 * @return current viewport. can be NULL if u want it that way.
		 */
		baseViewport*			getViewport (void) const;
	
		/**
		 * Changes current viewport. Used mostly in FEditor.
		 * @param vp new current viewport object pointer
		 */
		void							setViewport (baseViewport* vp);
	
		/**
		 * @return input system object
		 */
		inputDevice*			getInputDevice (void) const;
		
		/**
		 * Faster than any malloc, but must be used carefully, since many internal routines use it as a temporary storage.
		 * It is constant storage, so it is NOT THREAD-SAFE and risky even in single-threaded environment.
		 * @param sz size of buffer required
		 * @return temporary sysmem buffer of requested size if it is less than default_tmpbuffer_size, or NULL otherwise
		 */
		ubyte*							getTmpBuffer (int sz) const;
	
		/**
		 * @return logger object ptr
		 */
		logger*				getLogger (void);
	
		/**
		 * @return main system timer, used to sync everything in the game
		 */
		timer*				getTimer (void) const;
	
		/**
		 * Should be available always, even if not visible/renderable (even in non-graphical apps, such as 'fgp').
		 * @return console object pointer.
		 */
		console*				getConsole (void);
	
		/**
		 * @return effect file parameter manager
		 */
		effectParms*			getEffectParms (void) const;
	
		/**
		 * @return global parameter manager, used in feMathExpression class and maybe somewhere else
		 */
		parmMgr*				getParmMgr (void) const;
	
		/**
		 * @return global resource manager
		 */
		resourceMgr*			getResourceMgr (void) const;
	
		/**
		 * @return feDrawUtil object
		 */
		drawUtil*				getDrawUtil (void) const;
	
		/**
		 * @return render backend object
		 */
		rBackend*			getRBackend (void) const;
	
		/**
		 * @return scene manager object
		 */
		sceneManager*			getSceneManager (void) const;
	
		/**
		 * @return keybinder object
		 */
		keyBinder*			getKeyBinder (void) const;
	
		/**
		 * @return entity manager object
		 */
		entityManager*		getEntManager (void) const;

		/**
		 * @return metashader object
		 */
		metaShader*			getMetaShader (void) const;

		/**
		 * @return system font used to draw console, etc
		 */
		fontFT*				getSysFont (void) const;

		/**
		 * @return animation sequencer object
		 */
		animSequencer*		getAnimSequencer (void) const;

		/**
		 * @return global stack allocator
		 */
		stackAllocator *getStackAllocator (void) const;

		/**
		 * [re]starts video subsystem
		 */
		void restartVideo (void);

		/**
		 * [re]starts audio subsystem
		 */
		void restartAudio (void);

		/**
		 * @return console variable manager
		 */
		cvarManager* getCVarManager (void) const;
	};
	
	extern FE_API engine *g_engine;

}

#endif // __F_ENGINE_H

