#include "pch.h"
#include "entclasses.h"
#include "f_engine.h"
#include "entitymanager.h"
#include "f_resourcemgr.h"
#include "f_material.h"

namespace fe
{
	
	/**
	 * editorname: worldspawn
	 */

	entWorld::~entWorld (void)
	{
	}

	void entWorld::init (void)
	{
	}

	struct vertex_t
	{
		float x, y, z;
		float u, v;
	};
	
	const int POLY_MAX_VERTS = 16;

	struct polygon_t
	{
		int mtl;
		vertex_t verts[POLY_MAX_VERTS];
		int nverts;
		vector3 normal;
	};

	int cmpPolys (const void* a, const void *b)
	{
		return ((polygon_t*)a)->mtl < ((polygon_t*)b)->mtl;
	}

	entWorld::entWorld (uint32 id)
		: entity (id)
	{
		mNumEnts = 0;
	}

	void entWorld::load (const char *fname)
	{
		const int MAX_POLYGONS = 1024;
		const int MAX_MATERIALS = 256;
		const int MAX_MTLNAME = 64;
		char mtlnames[MAX_MTLNAME][MAX_MATERIALS];
		int nmtlnames = 0;
		materialPtr mtls[MAX_MATERIALS];
		int nmtls = 0;
		polygon_t polygons[MAX_POLYGONS];
		int npolygons = 0;

		try
		{
			charParser p (g_engine->getFileSystem (), fname);
			for (;;)
			{
				p.getToken ();
				if (!p.cmptoken ("polygons"))
				{
					p.matchToken ("{");
					for (;;)
					{
						p.getToken ();
						if (!p.cmptoken ("}"))
							break;
						else if (!p.cmptoken ("{"))
						{
							// load polygon
							polygon_t *poly = &polygons[npolygons++];
							memset (poly, 0, sizeof (polygon_t));
							p.getToken ();

							int m;
							for (m = 0; m < nmtlnames; m++) {
								if (!strcmp (mtlnames[m], p.token ()))
									break;
							}
							if (m == nmtlnames)
								strcpy (mtlnames[m], p.token ());
							poly->mtl = m;

							p.getToken ();
							int nverts = atoi (p.token ());

							for (int v = 0; v < nverts; v++)
							{
								vertex_t *vx = &poly->verts[poly->nverts++];
								p.getToken ();
								vx->x = atof (p.token ());
								p.getToken ();
								vx->y = atof (p.token ());
								p.getToken ();
								vx->z = atof (p.token ());
								p.getToken ();
								vx->u = atof (p.token ());
								p.getToken ();
								vx->v = atof (p.token ());
							}

							p.matchToken ("}");
							// calc normal
							vector3 a = (vector3&)(poly->verts[0].x) - (vector3&)(poly->verts[1].x);
							vector3 b = (vector3&)(poly->verts[2].x) - (vector3&)(poly->verts[1].x);
							poly->normal = a.cross (b);
							poly->normal.normalize ();
						}
						else
							p.generalSyntaxError ();
					}
				}
				else if (!p.cmptoken ("entity"))
				{
					p.matchToken ("{");
					for (;;)
					{
						const int PARM_LENGTH = 32;
						const int MAX_PARMS = 16;
						char parms[MAX_PARMS][PARM_LENGTH][2];
						int nparms;
						char *classname = NULL;
						p.getToken ();
						if (!p.cmptoken ("}"))
						{
							if (!classname)
							{
								fprintf (stderr, "WARNING: entity without classname in mapfile\n");
								break;
							}
							uint32 id = g_engine->getEntManager ()->getEClassForClassName (classname);
							entityPtr ent = g_engine->getEntManager ()->createEntity (id);
//							ent->setParms (parms);
							mEnts[mNumEnts++] = ent;
							break;
						}
						else
						{
							p.getToken ();
							strcpy (parms[nparms][0], p.token ());
							p.getToken ();
							strcpy (parms[nparms][1], p.token ());
							if (!strcmp(parms[nparms][0], "classname")) {
								classname = parms[nparms][1];
							}
							nparms++;
						}
					}
				}
				else
					p.generalSyntaxError ();
			}
		}
		catch (parserException e)
		{
		}

		// sort polygons
		qsort (polygons, sizeof (polygon_t), npolygons, cmpPolys);

		// load materials
		for (size_t m = 0; m < nmtlnames; m++)
		{
			mtls[nmtls++] = g_engine->getResourceMgr ()->createMaterial (mtlnames[m]);
		}
#if 0 // !!FIXME
		// create vertex&index buffers, and create model
		std::vector <drawVertex_t> verts;
		std::vector <ushort> inds;
		std::vector <modelSubset_t> subs;
		
		sz = polygons.size ();
		int mtl = -1;
		modelSubset_t *sub = NULL;
		for (size_t pp = 0; pp < sz; pp++)
		{
			polygon_t &p = polygons[pp];
			if (p.mtl != mtl)
			{
				// create new subset
				mtl = p.mtl;
				subs.resize (subs.size () + 1);
				sub = &subs.back ();
				sub->mtl = mtlList[mtl];
				sub->firstVertex = verts.size ();
				sub->firstFace = inds.size ()/3;
				sub->numVerts = 0;
				sub->numFaces = 0;
				sub->tris = NULL;
			}
			// add polygon as a set of verts and indexes
			size_t nverts = p.verts.size ();
			for (size_t v = 0; v < nverts; v++)
			{
				verts.resize (verts.size () + 1);
				drawVertex_t &vx = verts.back ();
				vx.pos.x = p.verts[v].x;
				vx.pos.y = p.verts[v].y;
				vx.pos.z = p.verts[v].z;
				vx.norm = p.normal;
				vx.uv.x = p.verts[v].u;
				vx.uv.y = p.verts[v].v;
				vx.uvLightmap.x = p.verts[v].u;
				vx.uvLightmap.y = p.verts[v].v;
				vx.color[0] = 0xff;
				vx.color[1] = 0xff;
				vx.color[2] = 0xff;
				vx.color[3] = 0xff;
			}
			// poly is a triangle fan, convert it to indexed trilist
			size_t nfaces = nverts-2;
			for (size_t f = 1; f < nfaces-1; f++)
			{
				inds.push_back (0+sub->numVerts);
				inds.push_back (f+sub->numVerts);
				inds.push_back (f+1+sub->numVerts);
			}
			sub->numVerts += nverts;
			sub->numFaces += nfaces;
		}
		// now create model
		matrix4 mtx (true);
//		mpModel = new model (fname, NULL, mtx, verts, inds, subs, mtlList);
#endif
		mpModel = NULL;
	}
	
	smartPtr <model> entWorld::getModel (void) const
	{
		return mpModel;
	}

	/**
	 * editorname: info_playerstart
	 */

	void entSpawnSpot::init (void)
	{
	}

	/**
	 * editorname: info_physx
	 */
	void entPhysXObj::init (void)
	{
	}

}

