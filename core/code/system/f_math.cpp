/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_math.h"

namespace fe
{

	const vector3 vector3::zero (0, 0, 0);
	const vector3 vector3::unitX (1, 0, 0);
	const vector3 vector3::unitY (0, 1, 0);
	const vector3 vector3::unitZ (0, 0, 1);
	
	void FE_API calcTriangleBasis (const vector3& E, const vector3& F, const vector3& G,
			float sE, float tE, float sF,
			float tF, float sG, float tG,
			vector3 &tangentX, vector3 &tangentY)
	{
		vector3 P = F - E;
		vector3 Q = G - E;
		float s1 = sF - sE;
		float t1 = tF - tE;
		float s2 = sG - sE;
		float t2 = tG - tE;
		float pqMatrix[2][3];
		pqMatrix[0][0] = P[0];
		pqMatrix[0][1] = P[1];
		pqMatrix[0][2] = P[2];
		pqMatrix[1][0] = Q[0];
		pqMatrix[1][1] = Q[1];
		pqMatrix[1][2] = Q[2];
		float temp = 1 / (s1*t2 - s2*t1);
		float stMatrix[2][2];
		stMatrix[0][0] = t2 * temp;
		stMatrix[0][1] = -t1 * temp;
		stMatrix[1][0] = -s2 * temp;
		stMatrix[1][1] = s1 * temp;
		float tbMatrix[2][3];
		// stMatrix * pqMatrix
		tbMatrix[0][0] = stMatrix[0][0]*pqMatrix[0][0] + stMatrix[0][1]*pqMatrix[1][0];
		tbMatrix[0][1] = stMatrix[0][0]*pqMatrix[0][1] + stMatrix[0][1]*pqMatrix[1][1];
		tbMatrix[0][2] = stMatrix[0][0]*pqMatrix[0][2] + stMatrix[0][1]*pqMatrix[1][2];
		tbMatrix[1][0] = stMatrix[1][0]*pqMatrix[0][0] + stMatrix[1][1]*pqMatrix[1][0];
		tbMatrix[1][1] = stMatrix[1][0]*pqMatrix[0][1] + stMatrix[1][1]*pqMatrix[1][1];
		tbMatrix[1][2] = stMatrix[1][0]*pqMatrix[0][2] + stMatrix[1][1]*pqMatrix[1][2];
		tangentX = vector3 (tbMatrix[0][0], tbMatrix[0][1], tbMatrix[0][2]);
		tangentY = vector3 (tbMatrix[1][0], tbMatrix[1][1], tbMatrix[1][2]);
		tangentX.normalize ();
		tangentY.normalize ();	
	}
	
	vector3 FE_API orthogonalize(const vector3 &v1, const vector3 &v2)
	{
		vector3 v2ProjV1 = closestPointOnLine (v1, -v1, v2);
		vector3 res = v2 - v2ProjV1;
		res.normalize ();
		return res;
	}
	
	vector3 FE_API closestPointOnLine(const vector3 &a, const vector3 &b, const vector3 &p)
	{
		// a-b is the line, p the point in question
		vector3 c = p-a;
		vector3 V = b-a;
		float d = V.length();
		V.normalize (); // normalize V
		float t = V.dot (c); // V dot c
		// Check to see if t is beyond the extents of the line segment
		if (t < 0.0f)
		{
			vector3 res = a;
			return res;
		}
		if (t > d)
		{
			vector3 res = b;
			return res;
		}
		// Return the point between a and b
		V = V * t; //set length of V to t.
		vector3 res = a + V;
		return res;
	}
	
	void
	matrix4::fromEulerAnglesXYZ (float ax, float ay, float az)
	{
		double ci, cj, ch, si, sj, sh, cc, cs, sc, ss;
	
		ci = cos(ax);
		cj = cos(ay);
		ch = cos(az);
		si = sin(ax);
		sj = sin(ay);
		sh = sin(az);
		cc = ci*ch;
		cs = ci*sh;
		sc = si*ch;
		ss = si*sh;
	
		m[0][0] = (float)(cj*ch);
		m[1][0] = (float)(sj*sc-cs);
		m[2][0] = (float)(sj*cc+ss);
		m[0][1] = (float)(cj*sh);
		m[1][1] = (float)(sj*ss+cc);
		m[2][1] = (float)(sj*cs-sc);
		m[0][2] = (float)-sj;
		m[1][2] = (float)(cj*si);
		m[2][2] = (float)(cj*ci);
		m[0][3] = 0;
		m[1][3] = 0;
		m[2][3] = 0;
		m[3][0] = 0;
		m[3][1] = 0;
		m[3][2] = 0;
		m[3][3] = 1;
	}
	
}
