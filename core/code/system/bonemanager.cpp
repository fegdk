/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "bonemanager.h"

namespace fe {

	boneManager::boneManager (void)
	{
		memset (mBones, 0, sizeof (mBones));
		mNumBones = 0;
	}

	boneManager::~boneManager (void)
	{
	}

	int boneManager::getBoneIndexByName (const char *name, bool createnew)
	{
		for (int i = 0; i < mNumBones; i++)
		{
			int l = min (strlen (name), MAX_REGISTERED_BONE_NAME-1);
			if (l >= MAX_REGISTERED_BONE_NAME)
				fprintf (stderr, "WARNING: bone name is too long (%s)\n", name);				
			if (!strncmp (name, mBones[i], l))
				return i;
		}

		if (!createnew)
			return -1;

		if (mNumBones == MAX_REGISTERED_BONES)
		{
			fprintf (stderr, "ERROR: cannot register bone %s - limit exceeded\n", name);
			return -1;
		}
		int l = min (strlen (name), MAX_REGISTERED_BONE_NAME-1);
		strncpy (mBones[mNumBones], name, l);
		mBones[mNumBones][l] = 0;
		return mNumBones++;
	}

}
