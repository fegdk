/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_animation.h"
#include "f_filesystem.h"
#include "f_parser.h"
#include "f_error.h"
#include "f_timer.h"
#include "f_engine.h"
#include "f_spline.h"

namespace fe
{
	
#if 0
	//---------------------------------------------
	// animationSequencer implementation
	//---------------------------------------------
	animationSequencer::animationSequencer (const char *name)
	{
		mName = name;
		mCurrentSequence = -1;
		mStartTime = 0.f;
		mCurrentTime = 0.f;
		try {
			charParser p (g_engine->getFileSystem (), name);
			p.matchToken ("animation_sequencer");
			p.getToken ();
			mName = p.token ();
			load (p);
		}
		catch (parserException e) {
			if (e != parserException::END_OF_FILE)
				throw (genericError ("failed to load animation sequencer %s", name));
		}
	}
	
	void	animationSequencer::load (charParser &p)
	{
		p.matchToken ("{");
		bool expEof = true;
		int idx = 0;
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
				break;
			else if (!p.cmptoken ("count"))
			{
				p.getToken ();
				mSequences.resize (atoi (p.token ()));
			}
			else if (!p.cmptoken ("sequence"))
			{
				p.getToken ();
				mSequenceNames[p.token ()] = idx;
				p.matchToken ("{");
				p.getToken ();
				mSequences[idx].startTime = atof (p.token ());
				p.getToken ();
				mSequences[idx].duration = atof (p.token ());
				p.getToken ();
				mSequences[idx].temp = atof (p.token ());
				p.getToken ();
				mSequences[idx].backwards = atoi (p.token ()) ? true : false;
				p.matchToken ("}");
			}
			else
				p.generalSyntaxError ();
		}
	}
	
	animationSequencer::animationSequencer (charParser &p, const char *name)
	{
		mName = name;
		load (p);
	}
	
	int	animationSequencer::addSequence (float startTime, float duration, float temp, bool backwards)
	{
		sequence_t s;
		s.startTime = startTime;
		s.duration = duration;
		s.temp = temp;
		s.backwards = backwards;
		mSequences.push_back (s);
		return (int)mSequences.size () - 1;
	}
	
	void	animationSequencer::setSequence (int i, float time)
	{
		mCurrentSequence = i;
		mStartTime = mCurrentTime = time;
	}
	
	int		animationSequencer::getSequence (void) const
	{
		return mCurrentSequence;
	}
	
	void	animationSequencer::update (void)
	{
		mCurrentTime = g_engine->getTimer ()->getTime ();
		
		if (mCurrentSequence != -1)
		{
			while ( (mCurrentTime - mStartTime) * mSequences[mCurrentSequence].temp > mSequences[mCurrentSequence].duration)
				mStartTime += mSequences[mCurrentSequence].duration / mSequences[mCurrentSequence].temp;
		}
	}
	
	float	animationSequencer::currentTime () const
	{
		if (mCurrentSequence == -1)
			return 0;
	
		const sequence_t &s = mSequences[mCurrentSequence];
		float timeDelta = (mCurrentTime - mStartTime) * s.temp;
	//	while (timeDelta > s.duration)
	//		timeDelta -= s.duration;
		return s.startTime + timeDelta;
	}
#endif
	
	//---------------------------------------------
	// animationController implementation
	//---------------------------------------------
	
#if 0	
	animationController::animationController (baseKeyFrameContainer *cont)
	{
		mpContainer = cont;
	}
	void animationController::setStartTime (float time)
	{
		mStartTime = time;
	}
	
	float animationController::getStartTime (float time) const
	{
		return mStartTime;
	}
	
	void animationController::update (void)
	{
		float time = g_engine->getTimer ()->getTime () - mStartTime;
		mCachedFrame = mpContainer->getFrameMtx (time);
	}
	
	const animationFrame& animationController::getMatrix (void) const
	{
		return mCachedFrame;
	}
	
	animationFrame animationController::getMatrix (float time) const
	{
		return mpContainer->getFrameMtx (time);
	}
#endif
	
	//---------------------------------------------
	// baseKeyFrameContainer implementation
	//---------------------------------------------
	baseKeyFrameContainer::baseKeyFrameContainer (void)
	{
	}
	
	//---------------------------------------------
	// matrixKeyFrameContainer implementation
	//---------------------------------------------
	matrixKeyFrameContainer::matrixKeyFrameContainer (const char *name)
	{
		mName = name;
		mFrameRate = 0.f;
		mpFrames = NULL;
		mNumFrames = 0;
		charParser p (g_engine->getFileSystem (), name);
		p.matchToken ("matrix_keyframes");
		p.getToken ();
		mName = p.token ();
		load (p);
	}
	
	matrixKeyFrameContainer::~matrixKeyFrameContainer (void)
	{
		if (mpFrames) {
			delete[] mpFrames;
			mpFrames = NULL;
		}
	}
	void matrixKeyFrameContainer::load (charParser &p)
	{
		int frm = 0;
		p.matchToken ("{");
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
				break;
			else if (!p.cmptoken ("count"))
			{
				p.getToken ();
				mNumFrames = atoi (p.token ());
				mpFrames = new animationFrame_t[mNumFrames];
			}
			else if (!p.cmptoken ("matrix"))
			{
				assert (mNumFrames);
				p.matchToken ("{");
				for (int i = 0; i < 4; i++)
				{
					for (int j = 0; j < 4; j < 3)
					{
						p.getToken ();
						mpFrames[frm].rows[i][j] = atof (p.token ());
					}
				}
				p.matchToken ("}");
				frm++;
			}
			else
				p.generalSyntaxError ();
		}
	}
		
	matrixKeyFrameContainer::matrixKeyFrameContainer (charParser &p, const char *name)
	{
		mName = name;
		load (p);
	}
	
	const animationFrame_t&	matrixKeyFrameContainer::getFrameMtx (float time) const
	{
		int frm = (int) (time * getFrameRate ());
		return mpFrames[frm];
	}
	
	//---------------------------------------------
	// ipoQuatKeyFrameContainer implementation
	//---------------------------------------------
	
	ipoQuatKeyFrameContainer::ipoQuatKeyFrameContainer (const char *name)
	{
		mName = name;
		mpFrames = NULL;
		mNumFrames = 0;
		charParser p (g_engine->getFileSystem (), name);
		p.matchToken ("ipo_quat_keyframes");
		p.getToken ();
		mName = p.token ();
		load (p);
	}
	
	ipoQuatKeyFrameContainer::ipoQuatKeyFrameContainer (charParser &p, const char *name)
	{
		mName = name;
		mpFrames = NULL;
		mNumFrames = 0;
		load (p);
	}

	ipoQuatKeyFrameContainer::~ipoQuatKeyFrameContainer (void)
	{
		if (mpFrames) {
			delete[] mpFrames;
			mpFrames = NULL;
		}
	}
	
	#define SMALL -1.0e-10
	
	double Sqrt3d(double d)
	{
	    if(d==0.0) return 0;
	    if(d<0) return -exp(log(-d)/3);
	    else return exp(log(d)/3);
	}
	
	int findzero(float x, float q0, float q1, float q2, float q3, float *o)
	{
		double c0, c1, c2, c3, a, b, c, p, q, d, t, phi;
		int nr= 0;
	
		c0= q0-x;
		c1= 3*(q1-q0);
		c2= 3*(q0-2*q1+q2);
		c3= q3-q0+3*(q1-q2);
		
		if(c3!=0.0) {
			a= c2/c3;
			b= c1/c3;
			c= c0/c3;
			a= a/3;
	
			p= b/3-a*a;
			q= (2*a*a*a-a*b+c)/2;
			d= q*q+p*p*p;
	
			if(d>0.0) {
				t= sqrt(d);
				o[0]= (float)(Sqrt3d(-q+t)+Sqrt3d(-q-t)-a);
				if(o[0]>= SMALL && o[0]<=1.000001) return 1;
				else return 0;
			}
			else if(d==0.0) {
				t= Sqrt3d(-q);
				o[0]= (float)(2*t-a);
				if(o[0]>=SMALL && o[0]<=1.000001) nr++;
				o[nr]= (float)(-t-a);
				if(o[nr]>=SMALL && o[nr]<=1.000001) return nr+1;
				else return nr;
			}
			else {
				phi= acos(-q/sqrt(-(p*p*p)));
				t= sqrt(-p);
				p= cos(phi/3);
				q= sqrt(3-3*p*p);
				o[0]= (float)(2*t*p-a);
				if(o[0]>=SMALL && o[0]<=1.000001) nr++;
				o[nr]= (float)(-t*(p+q)-a);
				if(o[nr]>=SMALL && o[nr]<=1.000001) nr++;
				o[nr]= (float)(-t*(p-q)-a);
				if(o[nr]>=SMALL && o[nr]<=1.000001) return nr+1;
				else return nr;
			}
		}
		else {
			a=c2;
			b=c1;
			c=c0;
			
			if(a!=0.0) {
				p=b*b-4*a*c;
				if(p>0) {
					p= sqrt(p);
					o[0]= (float)((-b-p)/(2*a));
					if(o[0]>=SMALL && o[0]<=1.000001) nr++;
					o[nr]= (float)((-b+p)/(2*a));
					if(o[nr]>=SMALL && o[nr]<=1.000001) return nr+1;
					else return nr;
				}
				else if(p==0) {
					o[0]= (float)(-b/(2*a));
					if(o[0]>=SMALL && o[0]<=1.000001) return 1;
					else return 0;
				}
			}
			else if(b!=0.0) {
				o[0]= (float)(-c/b);
				if(o[0]>=SMALL && o[0]<=1.000001) return 1;
				else return 0;
			}
			else if(c==0.0) {
				o[0]= 0.0;
				return 1;
			}
			return 0;	
		}
	}
	
	void berekeny(float f1, float f2, float f3, float f4, float *o, int b)
	{
		float t, c0, c1, c2, c3;
		int a;
	
		c0= f1;
		c1= 3.0f*(f2 - f1);
		c2= 3.0f*(f1 - 2.0f*f2 + f3);
		c3= f4 - f1 + 3.0f*(f2-f3);
		
		for(a=0; a<b; a++) {
			t= o[a];
			o[a]= c0+t*c1+t*t*c2+t*t*t*c3;
		}
	}
	
	void berekenx(float *f, float *o, int b)
	{
		float t, c0, c1, c2, c3;
		int a;
	
		c0= f[0];
		c1= 3*(f[3]-f[0]);
		c2= 3*(f[0]-2*f[3]+f[6]);
		c3= f[9]-f[0]+3*(f[3]-f[6]);
		for(a=0; a<b; a++) {
			t= o[a];
			o[a]= c0+t*c1+t*t*c2+t*t*t*c3;
		}
	}
	
	void correct_bezpart(float *v1, float *v2, float *v3, float *v4)
	{
		/* the total length of the handles is not allowed to be more
		 * than the horizontal distance between (v1-v4)
	         * this to prevent curve loops
		 */
		float h1[2], h2[2], len1, len2, len, fac;
		
		h1[0]= v1[0]-v2[0];
		h1[1]= v1[1]-v2[1];
		h2[0]= v4[0]-v3[0];
		h2[1]= v4[1]-v3[1];
		
		len= v4[0]- v1[0];
		len1= (float)fabs(h1[0]);
		len2= (float)fabs(h2[0]);
		
		if(len1+len2==0.0) return;
		if(len1+len2 > len) {
			fac= len/(len1+len2);
			
			v2[0]= (v1[0]-fac*h1[0]);
			v2[1]= (v1[1]-fac*h1[1]);
			
			v3[0]= (v4[0]-fac*h2[0]);
			v3[1]= (v4[1]-fac*h2[1]);
		}
	}
	
	void QuatCopy (float *result, const float *quat)
	{
		memcpy (result, quat, sizeof (float) * 4);
	}
	
	void QuatInterpolW(float *result, const float *quat1, const float *quat2, float t)
	{
	    float omega, cosom, sinom, sc1, sc2;
	
	    cosom = quat1[0]*quat2[0] + quat1[1]*quat2[1] + quat1[2]*quat2[2] + quat1[3]*quat2[3] ;
	
	    /* rotate around shortest angle */
	    if ((1.0 + cosom) > 0.0001) {
	
	        if ((1.0 - cosom) > 0.0001) {
	            omega = acos(cosom);
	            sinom = sin(omega);
	            sc1 = sin((1.0 - t) * omega) / sinom;
	            sc2 = sin(t * omega) / sinom;
	        }
	        else {
	            sc1 = 1.0 - t;
	            sc2 = t;
	        }
	        result[0] = sc1*quat1[0] + sc2*quat2[0];
	        result[1] = sc1*quat1[1] + sc2*quat2[1];
	        result[2] = sc1*quat1[2] + sc2*quat2[2];
	        result[3] = sc1*quat1[3] + sc2*quat2[3];
	    }
	    else {
	        result[0] = quat2[3];
	        result[1] = -quat2[2];
	        result[2] = quat2[1];
	        result[3] = -quat2[0];
	
	        sc1 = sin((1.0 - t)*M_PI_2);
	        sc2 = sin(t*M_PI_2);
	
	        result[0] = sc1*quat1[0] + sc2*result[0];
	        result[1] = sc1*quat1[1] + sc2*result[1];
	        result[2] = sc1*quat1[2] + sc2*result[2];
	        result[3] = sc1*quat1[3] + sc2*result[3];
	    }
	}
	
	void QuatToMat3( float *q, float m[4][4])
	{
	    double q0, q1, q2, q3, qda,qdb,qdc,qaa,qab,qac,qbb,qbc,qcc;
	
	    q0= M_SQRT2 * q[0];
	    q1= M_SQRT2 * q[1];
	    q2= M_SQRT2 * q[2];
	    q3= M_SQRT2 * q[3];
	
	    qda= q0*q1;
	    qdb= q0*q2;
	    qdc= q0*q3;
	    qaa= q1*q1;
	    qab= q1*q2;
	    qac= q1*q3;
	    qbb= q2*q2;
	    qbc= q2*q3;
	    qcc= q3*q3;
	
	    m[0][0]= (float)(1.0-qbb-qcc);
	    m[0][1]= (float)(qdc+qab);
	    m[0][2]= (float)(-qdb+qac);
	
	    m[1][0]= (float)(-qdc+qab);
	    m[1][1]= (float)(1.0-qaa-qcc);
	    m[1][2]= (float)(qda+qbc);
	
	    m[2][0]= (float)(qdb+qac);
	    m[2][1]= (float)(-qda+qbc);
	    m[2][2]= (float)(1.0-qaa-qbb);
	}
	
	void QuatInterpol(float *result, const float *quat1, const float *quat2, float t)
	{
	    float quat[4], omega, cosom, sinom, sc1, sc2;
	
	    cosom = quat1[0]*quat2[0] + quat1[1]*quat2[1] + quat1[2]*quat2[2] + quat1[3]*quat2[3] ;
	
	    /* rotate around shortest angle */
	    if (cosom < 0.0) {
	        cosom = -cosom;
	        quat[0]= -quat1[0];
	        quat[1]= -quat1[1];
	        quat[2]= -quat1[2];
	        quat[3]= -quat1[3];
	    }
	    else {
	        quat[0]= quat1[0];
	        quat[1]= quat1[1];
	        quat[2]= quat1[2];
	        quat[3]= quat1[3];
	    }
	
	    if ((1.0 - cosom) > 0.0001) {
	        omega = acos(cosom);
	        sinom = sin(omega);
	        sc1 = sin((1 - t) * omega) / sinom;
	        sc2 = sin(t * omega) / sinom;
	    } else {
	        sc1= 1.0 - t;
	        sc2= t;
	    }
	
	    result[0] = sc1 * quat[0] + sc2 * quat2[0];
	    result[1] = sc1 * quat[1] + sc2 * quat2[1];
	    result[2] = sc1 * quat[2] + sc2 * quat2[2];
	    result[3] = sc1 * quat[3] + sc2 * quat2[3];
	}
	
	const animationFrame_t& ipoQuatKeyFrameContainer::getFrameMtx (float time) const
	{
		int sz;
		sz = mNumFrames;
		float values[] = {0, 0, 0, 1, 1, 1};
		float rotation[4] = {0, 1, 0, 0};
		for (int tp = kf_x; tp < kf_max; tp++)
		{
			const keyFrame *kf = NULL, *next_kf = NULL;
			// find keyframe for specified component
			int frm;
			for (frm = 0; frm < sz; frm++)
			{
				const keyFrame *k = &mpFrames[frm];
				if (k->type != tp)
					continue;
				if (!kf) 	// 1st keyframe
				{
					kf = k;
					continue;
				}
				else if (!next_kf)	// 2nd keyframe
				{
					next_kf = k;
				}
				else if (next_kf)	// 3rd or more
				{
					kf = next_kf;
					next_kf = k;
				}
				// check if current time is between 2 keyframes
				if (tp == kf_rot)
				{
					if (kf->data.rotation[0] <= time && next_kf->data.rotation[0] >= time)
						break;
				}
				else if (kf->data.points.p[0] <= time && next_kf->data.points.p[0] >= time)
					break;
			}
			if (tp == kf_rot)
			{
				if (kf && time <= kf->data.rotation[0]+0.0001f)
					QuatCopy (rotation, &kf->data.rotation[1]);
				else if (next_kf && time >= next_kf->data.rotation[0]-0.0001f)
					QuatCopy (rotation, &next_kf->data.rotation[1]);
				else if (kf && next_kf && sz != frm)
				{
					float t = (time - kf->data.rotation[0]) / (next_kf->data.rotation[0] - kf->data.rotation[0]);
					QuatInterpol (rotation, &kf->data.rotation[1], &next_kf->data.rotation[1], t);
				}
				else
				{
					printf ("no quat for %s[%f]!!\n", name (), time);
				}
			}
			else
			{
				if (kf && time <= kf->data.points.p[0]+0.0001f)
				{
					values[tp] = kf->data.points.p[1];
				}
				else if (next_kf && time >= next_kf->data.points.p[0]-0.0001f)
				{
					values[tp] = next_kf->data.points.p[1];
				}
				else if (kf && next_kf && sz != frm)
				{
					if (fabs (kf->data.points.p[1] - next_kf->data.points.p[1]) < 0.0001f)
						values[tp] = kf->data.points.p[1];
					else
					{
						int a, b;
						float opl[32];
						// get y for x
						float v1[2], v2[2], v3[2], v4[2];
						v1[0]= kf->data.points.p[0];
						v1[1]= kf->data.points.p[1];
						v2[0]= kf->data.points.h2[0];
						v2[1]= kf->data.points.h2[1];
						v3[0]= next_kf->data.points.h1[0];
						v3[1]= next_kf->data.points.h1[1];
						v4[0]= next_kf->data.points.p[0];
						v4[1]= next_kf->data.points.p[1];
						correct_bezpart(v1, v2, v3, v4);
						/*				if (tp >= kf_rx && tp <= kf_rz)
										{
										if (fabs (v2[1] - v3[1]) > 180.f)
										{
										if (v2[1] > v3[1])
										{
										v3[1] += 360.f;
										v4[1] += 360.f;
										}
										else
										{
										v3[1] -= 360.f;
										v4[1] -= 360.f;
										}
										}
										}*/
	
						b = findzero (time, v1[0], v2[0], v3[0], v4[0], opl);
						if (b)
						{
							berekeny(v1[1], v2[1], v3[1], v4[1], opl, 1);
							values[tp] = opl[0];
						}
						else
							fprintf (stderr, "berekeny error!\n");
					}
				}
		/*		else if (kf && !next_kf)
				{
					values[tp] = kf->data.points.p[1];
				}
				else if (next_kf && !kf)
				{
					values[tp] = next_kf->data.points.p[1];
				}*/
			}
		}
		// create matrix (temporary hack)
		matrix4 rot, pos, final;
	//	rot.fromEulerAnglesXYZ (values[kf_rx]*M_PI/180.f, values[kf_ry]*M_PI/180.f, values[kf_rz]*M_PI/180.f);
	//	rot.fromQuat (rotation);
		QuatToMat3 (rotation, rot.m);
		rot.m[0][3] = 0;
		rot.m[1][3] = 0;
		rot.m[2][3] = 0;
		rot.m[3][3] = 1;
		final = rot;
		final.m[3][0] = values[kf_x];
		final.m[3][1] = values[kf_y];
		final.m[3][2] = values[kf_z];
		static animationFrame_t frm;
		memset (&frm, 0, sizeof (&frm));
		frm.rows[0][0] = final.m[0][0];
		frm.rows[0][1] = final.m[1][0];
		frm.rows[0][2] = final.m[2][0];
		frm.rows[0][3] = final.m[3][0];
		frm.rows[1][0] = final.m[0][1];
		frm.rows[1][1] = final.m[1][1];
		frm.rows[1][2] = final.m[2][1];
		frm.rows[1][3] = final.m[3][1];
		frm.rows[2][0] = final.m[0][2];
		frm.rows[2][1] = final.m[1][2];
		frm.rows[2][2] = final.m[2][2];
		frm.rows[2][3] = final.m[3][2];
		
		return frm;
		
	}
	
	void ipoQuatKeyFrameContainer::load (charParser &p)
	{
		int frm = 0;
		p.matchToken ("{");
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
			{
				break;
			}
			else if (!p.cmptoken ("count")) {
				assert (!mNumFrames);
				p.getToken ();
				mNumFrames = atoi (p.token ());
				mpFrames = new keyFrame[mNumFrames];
			}
			else {
				assert (mNumFrames);
				if (!p.cmptoken ("rot"))
					mpFrames[frm].type = kf_rot;
				else if (!p.cmptoken ("x"))
					mpFrames[frm].type = kf_x;
				else if (!p.cmptoken ("y"))
					mpFrames[frm].type = kf_y;
				else if (!p.cmptoken ("z"))
					mpFrames[frm].type = kf_z;
		/*		else if (!p.cmptoken ("rx"))
					mFrames[frm].type = kf_rx;
				else if (!p.cmptoken ("ry"))
					mFrames[frm].type = kf_ry;
				else if (!p.cmptoken ("rz"))
					mFrames[frm].type = kf_rz;*/
				else if (!p.cmptoken ("sx"))
					mpFrames[frm].type = kf_sx;
				else if (!p.cmptoken ("sy"))
					mpFrames[frm].type = kf_sy;
				else if (!p.cmptoken ("sz"))
					mpFrames[frm].type = kf_sz;
				else
					p.generalSyntaxError ();
				p.matchToken ("{");
				if (mpFrames[frm].type == kf_rot)
				{
					p.getToken ();
					mpFrames[frm].data.rotation[0] = (float)atof (p.token ());
					p.getToken ();
					mpFrames[frm].data.rotation[1] = (float)atof (p.token ());
					p.getToken ();
					mpFrames[frm].data.rotation[2] = (float)atof (p.token ());
					p.getToken ();
					mpFrames[frm].data.rotation[3] = (float)atof (p.token ());
					p.getToken ();
					mpFrames[frm].data.rotation[4] = (float)atof (p.token ());
				}
				else
				{
					for (int i = 0; i < 3; i++)
					{
						for (int j = 0; j < 2; j++)
						{
							p.getToken ();
							mpFrames[frm].data.rawpoints[i][j] = (float)atof (p.token ());
						}
					}
				}
				p.matchToken ("}");
				frm++;
			}
		}
	}
	
	//---------------------------------------------
	// animation implementation
	//---------------------------------------------
	
	animation::animation (const char *name)
	{
		mName = name;
		mpBoneKeyFrames = NULL;
		charParser p (g_engine->getFileSystem (), name);
		p.matchToken ("animation");
		p.getToken ();
		mName = p.token ();
		load (p);
	}
	
	animation::animation (charParser &p, const char *name)
	{
		mName = name;
		mpBoneKeyFrames = NULL;
		load (p);
	}

	animation::~animation (void)
	{
		if (mpBoneKeyFrames)
		{
//			for (int i = 0; i < mNumBones; i++)
//				mpBoneKeyFrames[i] = NULL;
			delete[] mpBoneKeyFrames;
			mpBoneKeyFrames = NULL;
		}
	}
	
	int animation::getNumBones (void) const
	{
		return mNumBones;
	}

	baseKeyFrameContainer* animation::getKeyFramesForBone (int bone) const
	{
		return mpBoneKeyFrames[bone];
	}
	
	void animation::load (charParser &p)
	{
		mDuration = 0;
		p.matchToken ("{");
		int curbone = 0;
		for (;;)
		{
			p.getToken ();
			if (!p.cmptoken ("}"))
				break;
			else if (!p.cmptoken ("duration"))
			{
				p.getToken ();
				mDuration = (float)atof (p.token ());
			}
			else if (!p.cmptoken ("numbones"))
			{
				p.getToken ();
				mNumBones = atoi (p.token ());
				// alloc
				mpBoneKeyFrames = new smartPtr<baseKeyFrameContainer>[mNumBones];
			}
			else if (!p.cmptoken ("ipo_quat_keyframes"))
			{
				p.getToken ();
				cStr name = p.token ();
				baseKeyFrameContainer *ctl = new ipoQuatKeyFrameContainer (p, name);
				mpBoneKeyFrames[curbone] = ctl;
				curbone++;
			}
			else
				p.generalSyntaxError ();
		}
	}
	
	float animation::getDuration (void) const
	{
		return mDuration;
	}
	
	void animation::sortBones (animContainer *cont)
	{
		int nbones = cont->getNumBones ();
		smartPtr <baseKeyFrameContainer> *kf = new smartPtr <baseKeyFrameContainer>[nbones];
		for (int i = 0; i < mNumBones; i++)
		{
			int idx = cont->getBoneIndexForName (mpBoneKeyFrames[i]->name ());
			if (idx == -1)
				fprintf (stderr, "WARNING: bone %s is not in container!\n", mpBoneKeyFrames[i]->name ());
			else
				kf[idx] = mpBoneKeyFrames[i];
		}
		for (int i = 0; i < mNumBones; i++)
			mpBoneKeyFrames[i] = NULL;
		delete[] mpBoneKeyFrames;
		mpBoneKeyFrames = kf;
	}
	
	animContainer::animContainer (void)
	{
		mNumAnims = 0;
		// each bone is registered here to get ID
		memset (mBones, 0, sizeof (mBones));
		mNumBones = 0;
	}

	animContainer::~animContainer (void)
	{
	}

	void animContainer::addBone (const char *name)
	{
		for (int i = 0; i < mNumBones; i++)
		{
			int l = (int)strlen (name);
			l = min (MAX_BONE_NAME-1, l);
			if (!strncmp (name, mBones[i], l))
				return;
		}
		if (mNumBones == MAX_BONES)
		{
			fprintf (stderr, "ERROR: cannot add new bone (%s) to animContainer\n", name);
			return;
		}
		int l = (int)strlen (name);
		l = min (l, MAX_BONE_NAME-1);
		strncpy (mBones[mNumBones], name, l);
		mBones[mNumBones][l] = 0;
		mNumBones++;
	}

	void animContainer::addAnim (animation *anim)
	{
		//fprintf (stderr, "INFO: adding anim %s to container\n", anim->name ());
		for (int i = 0; i < mNumAnims; i++)
		{
			if (mpAnims[i] == anim)
			{
				fprintf (stderr, "WARNING: anim %s is already registered in container\n", anim->name ());
				return;
			}
		}
		if (mNumAnims == MAX_ANIMATIONS_FOR_SINGLE_SKELETON)
		{
			fprintf (stderr, "ERROR: cannot add anim %s to container (limit exceeded)\n", anim->name ());
			return;
		}
		mpAnims[mNumAnims++] = anim;
		int nbones = anim->getNumBones ();
		//fprintf (stderr, "INFO: numbones: %d\n", nbones);
		for (int i = 0; i < nbones; i++)
		{
			baseKeyFrameContainer* bone = anim->getKeyFramesForBone (i);
			//fprintf (stderr, "INFO: adding bone %s to container\n", bone->name ());
			addBone (bone->name ());
		}
		anim->sortBones (this);
	}

	void animContainer::removeAnim (animation *anim)
	{
		if (mNumAnims == 0)
		{
			fprintf (stderr, "WARNING: attempt to remove anim %s from empty container\n", anim->name ());
			return;
		}
		int i;
		for (i = 0; i < mNumAnims; i++)
		{
			if (mpAnims[i] == anim)
			{
				if (i != mNumAnims-1)
				{
					mpAnims[i] = mpAnims[mNumAnims-1];
				}
				mNumAnims--;
				return;
			}
		}
	}
	
	int animContainer::getNumBones (void) const
	{
		return mNumBones;
	}

	const char *animContainer::getBoneName (int i) const
	{
		return mBones[i];
	}

	int animContainer::getBoneIndexForName (const char *name) const
	{
		for (int i = 0; i < mNumBones; i++)
		{
			int l = (int)strlen (name);
			l = min (MAX_BONE_NAME-1, l);
			if (!strncmp (name, mBones[i], l))
				return i;
		}
		return -1;
	}

}

