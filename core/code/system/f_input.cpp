/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_input.h"
#include "f_keycodes.h"
#include "f_types.h"
#include "f_engine.h"
#include "f_baseviewport.h"
#include "f_input.h"
#include "f_helpers.h"
#include "f_console.h"
#include "f_keybinder.h"
#include "cvars.h"

namespace fe
{
	cvar_t *in_keyb_autorepeat;
	cvar_t *in_debugkeys;

	inputDevice::inputDevice (void)
	{
		cvarManager *cvars = g_engine->getCVarManager ();
		in_keyb_autorepeat = cvars->get ("in_keyb_autorepeat", "0", CVAR_READONLY);
		in_debugkeys = cvars->get ("in_debugkeys", "0", 0);
	}

	void inputDevice::keyDown (int vk)
	{
		int c = tolower (vk);
		bool stop = false;
		if (!mState.isPressed (c) || in_keyb_autorepeat->ivalue) // avoid autorepeat
		{
			mState.setState (c, true);
			if (!g_engine->getConsole ()->isVisible ())
				stop = g_engine->getKeyBinder ()->keyDown (c);
		}
		if (!stop)
			g_engine->keyDown (c);
	}
	
	void inputDevice::keyUp (int vk)
	{
		int c = tolower (vk);
		mState.setState (c, false);
		bool stop = false;
		if (!g_engine->getConsole ()->isVisible ())
			stop = g_engine->getKeyBinder ()->keyUp (c);
		if (!stop)
			g_engine->keyUp (c);
	}
	
	void inputDevice::printableChar (int vk)
	{
		g_engine->keyDown (vk | Key_CharFlag); // printable characters
	}
	
	inputState*	inputDevice::getState (void)
	{
		return &mState;
	}
	
	void inputDevice::lButtonDown ()
	{
		mState.setState (Key_Mouse1, true);
	}
	
	void inputDevice::lButtonUp ()
	{
		mState.setState (Key_Mouse1, false);
	}
	
	void inputDevice::rButtonDown ()
	{
		mState.setState (Key_Mouse2, true);
	}
	
	void inputDevice::rButtonUp ()
	{
		mState.setState (Key_Mouse2, false);
	}
	
	void inputDevice::mButtonDown ()
	{
		mState.setState (Key_Mouse3, true);
	}
	
	void inputDevice::mButtonUp ()
	{
		mState.setState (Key_Mouse3, false);
	}
	
	void inputDevice::mouseMove (int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	
	void inputDevice::mWheel ()
	{
	}
	
	void inputDevice::setCursorPos (int _x, int _y)
	{
		x = _x;
		y = _y;
	}
	
	point inputDevice::getCursorPos ()
	{
		point pt = { x, y };
		return pt;
	}
	
	struct keyname
	{
		int code;
	    const char *name;
	};
	
	static keyname keynames[] = {
		{ Key_Mouse1, "mouse1" },
		{ Key_Mouse2, "mouse2" },
		{ Key_Mouse3, "mouse3" },
		{ Key_BackSpace, "backspace" },
		{ Key_Tab, "tab" },
		{ Key_Enter, "enter" },
		{ Key_Shift, "shift" },
		{ Key_Ctrl, "ctrl" },
		{ Key_Alt, "alt" },
		{ Key_Pause, "pause" },
		{ Key_Escape, "escape" },
		{ Key_Space, "space" },
		{ Key_PgUp, "pgup" },
		{ Key_PgDn, "pgdn" },
		{ Key_End, "end" },
		{ Key_Home, "home" },
		{ Key_LeftArrow, "leftarrow" },
		{ Key_UpArrow, "uparrow" },
		{ Key_RightArrow, "rightarrow" },
		{ Key_DownArrow, "downarrow" },
		{ Key_Ins, "ins" },
		{ Key_Del, "del" },
		{ Key_Kp_Ins, "kp_ins" },
		{ Key_Kp_End, "kp_end" },
		{ Key_Kp_DownArrow, "kp_downarrow" },
		{ Key_Kp_PgDn, "kp_pgdn" },
		{ Key_Kp_LeftArrow, "kp_leftarrow" },
		{ Key_Kp_5, "kp_5" },
		{ Key_Kp_RightArrow, "kp_rightarrow" },
		{ Key_Kp_Home, "kp_home" },
		{ Key_Kp_UpArrow, "kp_uparrow" },
		{ Key_Kp_PgUp, "kp_pgup" },
		{ Key_Kp_Star, "kp_star" },
		{ Key_Kp_Plus, "kp_plus" },
		{ Key_Kp_Minus, "kp_minus" },
		{ Key_Kp_Del, "kp_del" },
		{ Key_Kp_Slash, "kp_slash" },
		{ Key_F1, "f1" },
		{ Key_F2, "f2" },
		{ Key_F3, "f3" },
		{ Key_F4, "f4" },
		{ Key_F5, "f5" },
		{ Key_F6, "f6" },
		{ Key_F7, "f7" },
		{ Key_F8, "f8" },
		{ Key_F9, "f9" },
		{ Key_F10, "f10" },
		{ Key_F11, "f11" },
		{ Key_F12, "f12" },
		{ Key_F13, "f13" },
		{ Key_F14, "f14" },
		{ Key_F15, "f15" },
		{ Key_Kp_NumLock, "kp_numlock" },
		{ Key_Scroll, "scroll" },
		{ Key_Kp_Equals, "kp_equals" },
		{ 0, NULL }
	};
	
	int inputManager::keyCodeByName (const cStr &code)
	{
		if (code.size () == 1)	// it's a one char code - simple
			return tolower (code[0]);
	
	
		// it's an alias
		// convert to lowercase
		cStr lc_code = code;
		lc_code.tolower ();
		for (keyname *kn = keynames; kn->name; kn++)
		{
			if (lc_code == kn->name) 
				return kn->code;
		}
	
		return 0;
	}
	
}


