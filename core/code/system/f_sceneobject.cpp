/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_sceneobject.h"
#include "f_animation.h"
#include "f_engine.h"
#include "f_timer.h"

namespace fe
{
	
	sceneObject::sceneObject (sceneObject *parent)
		: mRelativeMatrix (true)
		, mWorldMatrix (true)
		, mInitPosInverseMatrix (true)
	{
		mpSceneNext = mpScenePrev = NULL;
		mpParentObject = parent;
		mFlags = recalc_aab;
		mpAnimatedChildren = NULL;
	}
	
	sceneObject::~sceneObject ()
	{
		if (mpAnimatedChildren)
			delete[] mpAnimatedChildren;
	}
	
	matrix4	sceneObject::getWorldMatrix (void) const
	{
		return mWorldMatrix;
	}
	
	matrix4	sceneObject::getRelativeMatrix (void) const
	{
		return mRelativeMatrix;
	}
	
	matrix4	sceneObject::getSkinnedBoneRelativeMatrix (void) const
	{
		return mInitPosInverseMatrix * mRelativeMatrix;
	}
	
	matrix4	sceneObject::getSkinnedBoneWorldMatrix (void) const
	{
		return mInitPosInverseMatrix * mWorldMatrix;
	}
	
	void	sceneObject::setWorldMatrix (const matrix4 &m)
	{
		mFlags |= (invalidate_children_tm | recalc_aab);
		mWorldMatrix = m;
	}
	
	void	sceneObject::setRelativeMatrix (const matrix4 &m)
	{
		mFlags |= invalidate_children_tm;
		mRelativeMatrix = m;
		if (mpParentObject)
			mWorldMatrix = mRelativeMatrix * mpParentObject->getWorldMatrix ();
		else
			mWorldMatrix = mRelativeMatrix;
	}
	
	void	sceneObject::update (ulong updateFlags)
	{
		if (updateFlags & F_RECALC_MATRIX)
		{
			if (mpParentObject)
				mWorldMatrix = mRelativeMatrix * mpParentObject->getWorldMatrix ();
			else
				mWorldMatrix = mRelativeMatrix;
		}

		if (mFlags & invalidate_children_tm)
		{
			updateFlags |= F_RECALC_MATRIX;
			mFlags &= ~invalidate_children_tm;
		}
	
		for (sceneObject *c = mpChildren; c; c = c->mpNextChild)
			c->update (updateFlags);
	}
	
	sceneObject *sceneObject::getNewInstance (sceneObject *parent)
	{
		sceneObject *obj = new sceneObject (parent);
		if (mpRenderable)
		{
			obj->mpRenderable = mpRenderable->copy ();
			// set new sceneobject to all drawsurfs
			int sz = obj->mpRenderable->numDrawSurfs ();
			for (int i = 0; i < sz; i++)
				obj->mpRenderable->getDrawSurf (i)->obj = obj;
		}
		obj->mName = mName;
		obj->mWorldMatrix = mWorldMatrix;
		obj->mRelativeMatrix = mRelativeMatrix;
		obj->mInitPosInverseMatrix = mInitPosInverseMatrix;
		for (sceneObject *c = mpChildren; c; c = c->mpNextChild)
		{
			obj->addChild (c->getNewInstance (obj));
		}
		return obj;
	}
	
	sceneObject* sceneObject::getChildren (void) const
	{
		return mpChildren;
	}

	void sceneObject::addChild (sceneObject *child)
	{
		child->mpNextChild = mpChildren;
		mpChildren = child;
	}
	
	void sceneObject::removeChild (sceneObject *child)
	{
		assert (mpChildren);
		if (!mpChildren)
			return;

		if (mpChildren == child)
		{
			mpChildren = child->mpNextChild;
			child->mpNextChild = NULL;
		}
		else
		{
			sceneObject *c = mpChildren;

			while (c)
			{
				if (c->mpNextChild == child)
				{
					c->mpNextChild = child->mpNextChild;
					child->mpNextChild = NULL;
					break;
				}
			}
		}
	}
	
	sceneObject* sceneObject::getNextChild (void) const
	{
		return mpNextChild;
	}
	
	static void fillAnimData (sceneObject *obj, sceneObject **lst, animContainer *cont)
	{
		int nbones = cont->getNumBones ();
		for (int i = 0; i < nbones; i++)
		{
			const char *n = cont->getBoneName (i);
			if (!strcmp (obj->name (), n))
			{
				lst[i] = obj;
				break;
			}
		}
		for (sceneObject *c = obj->getChildren (); c; c = c->getNextChild ())
		{
			fillAnimData (c, lst, cont);
		}
	}

	void sceneObject::initAnimData (animContainer *cont)
	{
		if (mpAnimatedChildren)
			delete[] mpAnimatedChildren;
		int nbones = cont->getNumBones ();
		mpAnimatedChildren = new sceneObject*[nbones];
		fillAnimData (this, mpAnimatedChildren, cont);
	}
	
	sceneObject **sceneObject::getAnimatedChildren (void)
	{
		return mpAnimatedChildren;
	}

	void sceneObject::calcAAB (void)
	{
		// for now it's just AAB, which is suitable to init physx obj
		vector3 mins(1.0e+5f, 1.0e+5f, 1.0e+5f);
		vector3 maxs(-1.0e+5f, -1.0e+5f, -1.0e+5f);
		if (mpRenderable) {
			const matrix4& mtx = getWorldMatrix ();
			fprintf (stderr, "mtx: %f %f %f\n", mtx.m[3][0], mtx.m[3][1], mtx.m[3][2]);
			int nds = mpRenderable->numDrawSurfs ();
			for (int i = 0; i < nds; i++) {
				drawSurf_t *ds = mpRenderable->getDrawSurf (i);
				meshData_t *md = ds->mesh;
				uchar *vx = md->verts;
				for (int v = 0; v < md->numVerts; v++, vx += md->vertexSize) {
					const vector3 pos = *((vector3 *)vx) * mtx;
					for (int k = 0; k < 3; k++) {
						if (pos[k] < mins[k])
							mins[k] = pos[k];
						if (pos[k] > maxs[k])
							maxs[k] = pos[k];
					}
				}
			}
		}
		mBBox.mins = mins;
		mBBox.maxs = maxs;
		for (sceneObject *c = mpChildren; c; c = c->mpNextChild) {
			const aab3 &b = c->getBoundingBox (true);
			for (int k = 0; k < 3; k++) {
				if (b.mins[k] < mins[k])
					mins[k] = b.mins[k];
				if (b.maxs[k] > maxs[k])
					maxs[k] = b.maxs[k];
			}
		}
		mBBoxWithChildren.mins = mins;
		mBBoxWithChildren.maxs = maxs;
	}
	
	const aab3 &sceneObject::getBoundingBox (bool withChildren)
	{
		if (mFlags & recalc_aab) {
			mFlags &= ~recalc_aab;
			calcAAB ();
		}
		return withChildren ? mBBoxWithChildren : mBBox;
	}

}
