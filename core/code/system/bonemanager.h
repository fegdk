/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __BONEMANAGER_H
#define __BONEMANAGER_H

#include "f_baseobject.h"

namespace fe {

	enum {
		MAX_REGISTERED_BONES = 200,
		MAX_REGISTERED_BONE_NAME = 32
	};

	class FE_API boneManager : public baseObject
	{
	private:
		char mBones[MAX_REGISTERED_BONES][MAX_REGISTERED_BONE_NAME];
		int mNumBones;
	public:
		boneManager (void);
		~boneManager (void);

		int getBoneIndexByName (const char *name, bool createnew = false);

	};

}

#endif // __BONEMANAGER_H


