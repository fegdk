/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_ANIMATION_H
#define __F_ANIMATION_H

#include "f_math.h"
#include "f_baseobject.h"
#include "f_helpers.h"
#include "f_parser.h"

namespace fe
{

	struct FE_API animationFrame_t
	{
		float4 rows[3];	// 3x4 matrix, in vertex shader format
		void getMatrix4 (matrix4 &m) const
		{
			m._11 = rows[0][0]; m._12 = rows[1][0]; m._13 = rows[2][0]; m._14 = 0;
			m._21 = rows[0][1]; m._22 = rows[1][1]; m._23 = rows[2][1]; m._24 = 0;
			m._31 = rows[0][2]; m._32 = rows[1][2]; m._33 = rows[2][2]; m._34 = 0;
			m._41 = rows[0][3]; m._42 = rows[1][3]; m._43 = rows[2][3]; m._44 = 1;
		}
	};

	class baseKeyFrameContainer;
	
	
	class FE_API baseKeyFrameContainer : public baseObject
	{
	public:
	
		baseKeyFrameContainer (void);
		
		/**
		 * @param time time value relative to animation start time
		 * @return transformation matrix for specified time
		 */
		virtual const animationFrame_t& getFrameMtx (float time) const = 0;
	};
	
	/**
	 * Fastest animation controller.
	 * No interpolation, just a raw set of transforms sampled from animation package.
	 * Can be used with arbitrary software, all exporters support that.
	 */
	class FE_API matrixKeyFrameContainer : public baseKeyFrameContainer
	{
	private:
	
		float mFrameRate;
		animationFrame_t* mpFrames;
		int mNumFrames;
	
		void load (charParser &p);
		
		/**
		 * @return frames per second value
		 */
		virtual float getFrameRate (void) const = 0;
	
	
	public:
		matrixKeyFrameContainer (const char *name);
		matrixKeyFrameContainer (charParser &p, const char *name);
		~matrixKeyFrameContainer (void);
	
		const animationFrame_t& getFrameMtx (float time) const;
	};
	
	/**
	 * Implements key-frame animation based on ipo curves exported from Blender.
	 */
	class FE_API ipoQuatKeyFrameContainer : public baseKeyFrameContainer
	{
	public:
		enum { kf_x, kf_y, kf_z, /*kf_rx, kf_ry, kf_rz, */kf_sx, kf_sy, kf_sz, kf_rot, kf_max };
		struct keyFrame
		{
			int type;
			union {
				struct {
					float h1[2];
					float p[2];
					float h2[2];
				} points;
				float rawpoints[3][2];
				float rotation[5];
			} data;
		};
	private:
		keyFrame*	mpFrames;
		int mNumFrames;
		void load (charParser &p);
	
	public:
		ipoQuatKeyFrameContainer (const char *name);
		ipoQuatKeyFrameContainer (charParser &p, const char *name);
		~ipoQuatKeyFrameContainer (void);
	
		const animationFrame_t& getFrameMtx (float time) const;
	};
	
	class animContainer;
	/**
	 * contains list of animation controllers for specific hierarchy
	 * animation can be bound to matching node hierarchy
	 */
	class FE_API animation : public baseObject
	{
	private:

		int mNumBones;
		smartPtr <baseKeyFrameContainer>	*mpBoneKeyFrames;

		float mDuration;		// duration of animation in seconds
		void load (charParser &p);
	public:
		
		animation (const char *name);
		animation (charParser &p, const char *name);
		~animation (void);
		int getNumBones (void) const;
		baseKeyFrameContainer* getKeyFramesForBone (int bone) const;
		void sortBones (animContainer *cont);
		float getDuration (void) const;
		
	};
	
	/**
	 * contains all animations for single skeleton type
	 */
	enum
	{
		MAX_ANIMATIONS_FOR_SINGLE_SKELETON = 100,
		MAX_BONES = 200,
		MAX_BONE_NAME = 32
	};

	class animContainer : public baseObject
	{
	private:
		smartPtr <animation>	mpAnims[MAX_ANIMATIONS_FOR_SINGLE_SKELETON];
		int mNumAnims;
		// each bone is registered here to get ID
		char mBones[MAX_BONES][MAX_BONE_NAME];
		int mNumBones;

		void addBone (const char *name);
	public:
		animContainer (void);
		~animContainer (void);

		void addAnim (animation *anim);
		void removeAnim (animation *anim);

		int getNumBones (void) const;
		const char *getBoneName (int i) const;
		int getBoneIndexForName (const char *name) const;
		
	};

	
}

#endif // __F_ANIMATION_H
