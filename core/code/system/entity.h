#ifndef __ENTITY_H
#define __ENTITY_H

#include "f_baseobject.h"

namespace fe
{

	class FE_API entity : public baseObject
	{
		private:

			uint32 mId;

		public:

			smartPtr<entity> next;
			entity *nextInClass;

			enum flags { mover = 1, trigger = 2, target = 4 };
			entity (uint32 id);
			~entity (void);

			uint32 getId (void) const;
			virtual uint32 getClass (void) const = 0;

			// this methods should not be called from general game code
			virtual void fire (uint32 sourceEnt);

	};

	typedef smartPtr <entity> entityPtr;

}

#endif // __ENTITY_H

