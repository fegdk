/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_RESOURCEMGR_H
#define __F_RESOURCEMGR_H

#include "f_baseobject.h"
#include "f_parser.h"
#include "f_helpers.h"

namespace fe
{

	class texture;
	class material;
	class fontFT;
	class model;
	class animation;
	class cgShader;
	class ps14Shader;
	class effect;
	class sceneObject;
	class animContainer;

	enum {
		RES_TEXTURE,
		RES_MATERIAL,
		RES_FONT,
		RES_SCENEOBJECT,
		RES_ANIMATION,
		RES_CGSHADER,
		RES_PS14SHADER,
		RES_EFFECT,
		RES_MAX
	};

	const int RES_HASHSIZE = 256;
	const int MAX_RESOURCES = 1024;

	class resourceMgr : public baseObject
	{
	private:
		baseObject* mResourceHash[RES_HASHSIZE];
		int mNumResources;
		baseObject *create (const char *name);
		baseObject* create (charParser &parser);
		void destroy (const char *name);
		void flushUnused (void);
		baseObject* getResource (const char *name);
		void addResource (const char *name, baseObject *r);
		void removeResource (const char *name);

		baseObject* mResources[RES_MAX][MAX_RESOURCES];
		int mResCounts[RES_MAX];

//		resource<texture> mTextureMgr;
//		resource<material> mMaterialMgr;
//		resource<fontFT> mFontMgr;
//		resource<sceneObject> mModelMgr;
//		resource<animation> mAnimationMgr;
//		resource<cgShader> mCGShaderMgr;
//		resource<ps14Shader> mPS14ShaderMgr;
//		resource<effect> mEffectMgr;

	public:
		resourceMgr (void);
		~resourceMgr (void);

//		resource <texture> *getTextureMgr (void);
//		resource <material> *getMaterialMgr (void);
//		resource <fontFT> *getFontFTMgr (void);
//		resource <sceneObject> *getModelMgr (void);
//		resource <animation> *getAnimationMgr (void);
//		resource <cgShader> *getCGShaderMgr (void);
//		resource <ps14Shader> *getPS14ShaderMgr (void);
//		resource <effect> *getEffectMgr (void);

		texture* createTexture (const char *name);
		texture* createTexture (charParser &parser);
		material* createMaterial (const char *name);
		material* createMaterial (charParser &parser);
		fontFT* createFontFT (const char *name);
		fontFT* createFontFT (charParser &parser);
		sceneObject* createModel (const char *name, animContainer *anim = NULL);
		sceneObject* createModel (charParser &parser);
		animation* createAnimation (const char *name);
		animation* createAnimation (charParser &parser);
		cgShader* createCGShader (const char *name);
		cgShader* createCGShader (charParser &parser);
		ps14Shader* createPS14Shader (const char *name);
		ps14Shader* createPS14Shader (charParser &parser);
		effect* createEffect (const char *name);
		effect* createEffect (charParser &parser);
		int getCount (int type) const;
		baseObject* getObject (int type, int idx);
	};
	
}

#endif // __F_RESOURCEMGR_H

