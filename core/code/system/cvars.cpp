#include "pch.h"
#include "cvars.h"
#include "config.h"
#include "f_console.h"

namespace fe
{

	// entire console idea comes from Id's quake engine
	// this is just an implementation of similar cvar system
	
	cvarManager::cvarManager (void)
	{
		mNumCVars = 0;
		memset (mCVars, 0, sizeof (mCVars));

		mpCVarList = NULL;
		memset (mpCVarsHash, 0, sizeof (mpCVarsHash));

		mModifiedFlags = 0;
	}
	
	cvarManager::~cvarManager (void)
	{
		for (int i = 0; i < mNumCVars; i++)
		{
			cvar_t *var = &mCVars[i];
			if (var->name)
				delete[] var->name;
			if (var->string)
				delete[] var->string;
			if (var->resetString)
				delete[] var->resetString;
			if (var->latchedString)
				delete[] var->latchedString;
		}
	}

	cvar_t*
	cvarManager::find (const char *s)
	{
		uint32 hash = hashValue (s);
		for (cvar_t *var = mpCVarsHash[hash]; var; var = var->hashNext)
		{
			if (!strcmp (s, var->name))
				return var;
		}
		return NULL;
	}

	uint32
	cvarManager::hashValue (const char *s)
	{
		uint32 hash = 0;
		int i = 0;
		while (s[i])
		{
			hash += tolower (s[i]) * (i+100);
			i++;
		}
		hash &= (CVARS_HASH_SIZE-1);
		return hash;
	}
		
	char*
	cvarManager::copyString (const char *s)
	{
		// TODO: better allocator, special case for 1-digits, etc
		size_t l = strlen (s)+1;
		char *ss = new char[l];
		memcpy (ss, s, l);
		return ss;
	}
		
	bool
	cvarManager::validateString (const char *s)
	{
		if (!s)
			return false;
		if (strchr (s, '\\'))
			return false;
		if (strchr (s, '\"'))
			return false;
		if (strchr (s, ';'))
			return false;
		return true;
	}
	
	cvar_t*
	cvarManager::get (const char *name, const char *value, uint32 flags)
	{
		cvar_t *var;

		if (!name || !value)
		{
			fprintf (stderr, "ERROR: cvarMgr::get NULL parameter\n");
			return NULL;
		}

		if (!validateString (name))
		{
			fprintf (stderr, "invalid cvar name: %s\n", name);
			name = "BAD_CVAR";
		}

		var = find (name);
		if (var)
		{
			if ((var->flags & CVAR_USERCREATED) && !(flags & CVAR_USERCREATED) && value[0])
			{
				// take the new value as reset value (will be set after cvar_reset)
				var->flags &= ~CVAR_USERCREATED;
				delete[] var->resetString;
				var->resetString = copyString (value);
				mModifiedFlags |= flags;
			}
			// set new values
			var->flags |= flags;

			if (!var->resetString[0])
			{
				delete[] var->resetString;
				var->resetString = copyString (value);
			}
			else if (value[0] && strcmp (var->resetString, value))
			{
				fprintf (stderr, "WARNING: cvar %s given init values: %s and %s\n", name, var->resetString, value);
			}
			if (var->latchedString)
			{
				char *s = var->latchedString;
				var->latchedString = NULL;
				set (name, s, true);
				delete[] s;
			}
			return var;
		}

		// create new
		
		if (mNumCVars >= CVARS_MAX)
		{
			fprintf (stderr, "ERROR: too many cvars\n");
			return NULL;
		}

		var = &mCVars[mNumCVars++];
		var->name = copyString (name);
		var->flags = flags;
		var->string = copyString (value);
		var->resetString = copyString (value);
		var->latchedString = NULL;
		var->modified = true;
		var->modificationCount = 1;
		var->fvalue = atof (value);
		var->ivalue = atoi (value);

		var->next = mpCVarList;
		mpCVarList = var;

		uint32 hash = hashValue (name);
		var->hashNext = mpCVarsHash[hash];
		mpCVarsHash[hash] = var;

		return var;
	}
	
	cvar_t*
	cvarManager::set (const char *name, const char *value, bool force)
	{
		if (!validateString (name))
		{
			fprintf (stderr, "invalid cvar name: %s\n", name);
			name = "BAD_CVAR";
		}

		cvar_t *var = find (name);
		if (!var)
		{
			if (!value)
				return NULL;

			if (!force)
				return get (name, value, CVAR_USERCREATED);
			else
				return get (name, value, 0);
		}

		if (!value)
			value = var->resetString;

		if (!strcmp (value, var->string))
			return var;

		mModifiedFlags |= var->flags;

		if (!force)
		{
			if (var->flags & CVAR_READONLY)
			{
				Con_Printf ("%s is readonly\n", name);
				return var;
			}

			if (var->flags & CVAR_INIT)
			{
				Con_Printf ("%s is write-protected\n", name);
				return var;
			}

			if (var->flags & CVAR_LATCH)
			{
				if (var->latchedString)
				{
					if (!strcmp (var->latchedString, value))
						return var;
					delete[] var->latchedString;
				}
				else
				{
					if (!strcmp (value, var->string))
						return var;
				}
				Con_Printf ("%s will be changed upon restarting\n", name);
				var->latchedString = copyString (value);
				var->modified = true;
				var->modificationCount++;
				return var;
			}
		}
		else
		{
			if (var->latchedString)
			{
				delete[] var->latchedString;
				var->latchedString = NULL;
			}
		}
		if (!strcmp (value, var->string))
			return var;

		var->modified = true;
		var->modificationCount++;
		delete[] var->string;
		var->string = copyString (value);
		var->ivalue = atoi (value);
		var->fvalue = atof (value);
		return var;

	}

	cvar_t*
	cvarManager::setInt (const char *name, int value, bool force)
	{
		char s[20];
		snprintf (s, 20, "%d", value);
		return set (name, s, force);
	}

	cvar_t*
	cvarManager::setFloat (const char *name, float value, bool force)
	{
		char s[20];
		snprintf (s, 20, "%f", value);
		return set (name, s, force);
	}

	const char *
	cvarManager::stringValue (const char *name)
	{
		cvar_t *var = find (name);
		return var ? var->string : NULL;
	}

	int
	cvarManager::intValue (const char *name)
	{
		cvar_t *var = find (name);
		return var ? var->ivalue : 0;
	}

	float
	cvarManager::floatValue (const char *name)
	{
		cvar_t *var = find (name);
		return var ? var->fvalue : 0;
	}
		
	void
	cvarManager::commandCompletion (void (*matchfunc)(const char *cmd, field_t *data), field_t *data)
	{
		for (int i = 0; i < mNumCVars; i++)
		{
			matchfunc (mCVars[i].name, data);
		}
	}

	int
	cvarManager::getCVarIdx (const char *n)
	{
		cvar_t *var = find (n);
		return var ? var - mCVars : -1;
	}

	cvar_t*
	cvarManager::cvarForIdx (int idx)
	{
		return &mCVars[idx];
	}

}
