/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "animsequencer.h"
#include "f_engine.h"
#include "f_timer.h"
#include "f_animation.h"
#include "f_sceneobject.h"

namespace fe
{

	animSequencer::animSequencer (void)
	{
		mNumAnims = 0;
		memset (mAnims, 0, sizeof (mAnims));
	}

	animSequencer::~animSequencer (void)
	{
	}

	int animSequencer::startAnimation (sceneObject *obj, animation *anim)
	{
		if (mNumAnims >= MAX_PLAYING_ANIMS)
		{
			fprintf (stderr, "ERROR: too many animations!\n");
			return -1;
		}
		mAnims[mNumAnims].anim = anim;
		mAnims[mNumAnims].obj = obj;
		mAnims[mNumAnims].startTime = g_engine->getTimer ()->getTime ();

		// FIXME: do we need to update immediately, or wait for animSequencer::update ?

		return mNumAnims++;
	}

	void animSequencer::stopAnimation (int anim)
	{
		if (anim != mNumAnims-1)
			memcpy (&mAnims[anim], &mAnims[mNumAnims-1], sizeof (animState_t));
		mNumAnims--;
	}

	void animSequencer::update (void)
	{
		float t = g_engine->getTimer ()->getTime ();
		for (int i = 0; i < mNumAnims; i++)
		{
			float dt = t - mAnims[i].startTime;
			// get matrix for each bone, and set to appropriate sceneobject
			int nBones = mAnims[i].anim->getNumBones ();
			sceneObject **ch = mAnims[i].obj->getAnimatedChildren ();
			int b;
			for (b = 0; b < nBones-1; b++)
			{
				// find sceneobject representing this bone
				baseKeyFrameContainer *bone = mAnims[i].anim->getKeyFramesForBone (b);
				sceneObject *o = ch[b];
				const animationFrame_t& mtx = bone->getFrameMtx (dt);
				matrix4 m;
				mtx.getMatrix4 (m);
				o->setRelativeMatrix (m);
			}
		}
	}

	animState_t *animSequencer::getAnim (int anim)
	{
		return &mAnims[anim];
	}

}
