#ifndef __ENTITYMANAGER_H
#define __ENTITYMANAGER_H

#include "f_baseobject.h"
#include "entity.h"
#include "entclasses.h"

namespace fe
{

	const int MAX_ENTS = 2048;

	class FE_API entityManager : public baseObject
	{
		private:
			uint32 mLastId;
			smartPtr <entity> mEntsList; // linked list
			entity* mEnts[MAX_ENTS]; // array for fast lookups
			entity* mEntsByClass[ECLASS_MAX];

		public:
			entityManager (void);
			~entityManager (void);
			void clean (void);
			uint32 getEClassForClassName (const char *classname) const;
			entity* createEntity (uint32 classId);
			void destroyEntity (uint32 id);
			void destroyEntity (entity* ent);
			entity* getEntityForId (uint32 id);
			entity* getEntListForClass (uint32 classId);
	};

}

#endif // __ENTITYMANAGER_H
