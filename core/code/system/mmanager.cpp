/*
	fegdk: FE Game Development Kit
	Copyright (C) 2001-2008 Alexey "waker" Yakovenko

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Library General Public
	License as published by the Free Software Foundation; either
	version 2 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Library General Public License for more details.

	You should have received a copy of the GNU Library General Public
	License along with this library; if not, write to the Free
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

	Alexander Maltsev
	keltar@users.sourceforge.net
*/

#include "pch.h"
#include "mmanager.h"
#include <malloc.h>

namespace fe
{

pool::pool (uint32 elem_size, uint32 num_elems) : mElemSize(elem_size), mNumElems(num_elems), mpBitfields (NULL), mLastBit (0)
{
	mNumBitfields = num_elems / 32;
	if (num_elems % 32)
	{
		++mNumBitfields;
	}

	mpBitfields = (uint32*)new ubyte[mNumBitfields * 4 + elem_size * num_elems];
	mpMemory = (ubyte*)(mpBitfields + mNumBitfields);
	mpLastAllocated = mpBitfields;
}

pool::~pool ()
{
	delete[] mpBitfields;
}

void *pool::alloc ()
{
	uint32 *cptr = mpLastAllocated;
	for (uint32 i = 0; i != mNumBitfields; ++i)
	{
		if (*cptr != -1)
		{
			for (ubyte b = mLastBit; b != 32; ++b)
			{
				const uint32 mask = 1 << b;
				if (!(*cptr & mask))
				{
					*cptr |= mask;
					mpLastAllocated = cptr;
					mLastBit = b;
					return mpMemory + mElemSize * (((ubyte*)cptr - (ubyte*)mpBitfields) * 8 + b);
				}
			}
		}

		if ((int)(++cptr - mpBitfields) > mNumElems)
		{
			cptr = mpBitfields;
		} else
		{
			++cptr;
		}
		mLastBit = 0;
	}

	return NULL;
}

void pool::free (void *p)
{
	uint32 offset = (ubyte*)p - mpMemory;
	offset /= mElemSize;
	uint32 word = offset / 32;
	ubyte bit = offset % 32;
	uint32 mask = 1 << bit;
	mpBitfields[word] ^= mask;
	mpLastAllocated = mpBitfields + word;
	mLastBit = bit;
}


stackAllocator::stackAllocator (uint32 size) : mpPtr (NULL), mSize (size), mMark (0)
{
	if (!size)
		fprintf (stderr, "ERROR: sys_stack_size = 0\n");
	mpPtr = new ubyte[size];
}

stackAllocator::~stackAllocator ()
{
	delete[] mpPtr;
}

uint32 stackAllocator::getMark () const
{
	return mMark;
}

void stackAllocator::setMark (uint32 mark)
{
	mMark = mark;
}

void *stackAllocator::alloc (uint32 size)
{
	void *r = (void*)(mpPtr + mMark);
	mMark += size;
	return r;
}

uint32 getUsedMemorySize()
{
	struct mallinfo mi = mallinfo();
	return mi.arena;
}

}
