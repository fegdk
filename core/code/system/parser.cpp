#include "f_parser.h"

namespace fe {

void charParser::errMode (int mode) {
	mErrMode = mode;
}

charParser::charParser (
#ifndef FE_NO_FILESYSTEM
fileSystem *fs, const char *buffer, bool stdio, int size
#else
const char *buffer, int size
#endif
)
{
#ifndef FE_NO_FILESYSTEM
	mpFileSystem = fs;
#else
	bool stdio = true;
#endif
	mErrMode = 1;
	mpBuffer = NULL;
	mpScript = NULL;
	mBufferSize = 0;
	mbTokenReady = false;
	mbSTDIO = stdio;
	mpToken = mToken;
	if (size == -1)
	{
		strncpy (mFileName, buffer, PARSER_MAXFNAME);
	}
	else
	{
		mpBuffer = mpScript = (char*)buffer;
		mBufferSize = size;
		mpEnd = mpBuffer + size;
		mLine = 0;
	}
}

charParser::~charParser ()
{		
	if (mbSTDIO && mpFileSTDIO) {
		fclose (mpFileSTDIO);
		mpFileSTDIO = NULL;
	}
#ifndef FE_NO_FILESYSTEM
	else if (!mbSTDIO && mpFile) {
		mpFile->close ();
		mpFile = NULL;
	}
#endif
}

int charParser::readNextChunk (void) {
	if (!mpBuffer) {
		mpBuffer = mBuffer;
		if (mbSTDIO) {
			mpFileSTDIO = fopen (mFileName, "rb");
			if (!mpFileSTDIO) {
				sys_printf ("ERROR: failed to open file %s\n", mFileName);
				if (mErrMode) error ();
				return FILE_NOT_FOUND;
			}
			fseek (mpFileSTDIO, 0, SEEK_END);
			mFileLeft = ftell (mpFileSTDIO);
			rewind (mpFileSTDIO);
			mpScript = mpBuffer;
			mpEnd = mpBuffer + mBufferSize;
			mLine = 0;
		}
		else {
#ifndef FE_NO_FILESYSTEM
			mpFile = mpFileSystem->openFile (mFileName);
			if (!mpFile) {
				sys_printf ("ERROR: failed to open file %s\n", mFileName);
				if (mErrMode) error ();
				return FILE_NOT_FOUND;
			}
			mFileLeft = mpFile->getSize ();
			mpScript = mpBuffer;
			mpEnd = mpBuffer + mBufferSize;
			mLine = 0;
#else
			sys_printf ("ERROR: fe::filesystem is not available in this build\n");
			if (mErrMode) error ();
			return FEATURE_NOT_INCLUDED;
#endif
		}
	}
	if (mpScript != mpBuffer) {
		mpScript = mpBuffer;
	}
	int sz = min (mFileLeft, PARSER_BUFSIZE);
	if (mbSTDIO) {
		if (fread (mpScript, sz, 1, mpFileSTDIO) != 1) {
			sys_printf ("ERROR: failed to read from file %s\n", mFileName);
			if (mErrMode) error ();
			return READ_ERROR;
		}
	}
	else {
		if (mpFile->read (mpScript, sz) != sz) {
			sys_printf ("ERROR: failed to read from file %s\n", mFileName);
			if (mErrMode) error ();
			return READ_ERROR;
		}
	}
	mpEnd = mpScript + sz;
	mFileLeft -= sz;
	return 0;
}

int charParser::checkEOF (void) {
	if (mpScript == mpEnd) {
		if (mFileLeft) {
			// read next chunk
			int res = readNextChunk ();
			if (res)
				return res;
		}
		else {
			if (mErrMode) error ();
			return END_OF_FILE;
		}
	}
	return 0;
}

int charParser::getToken (void)
{
	int res;
	if (!mpScript) {
		// load data
		res = readNextChunk ();
		if (res)
			return res;
	}
	if (mbTokenReady)
	{
		mbTokenReady = false;
		return 0;
	}

	if (checkEOF ()) return END_OF_FILE;

	// skip spaces
	for (;;)
	{
		while (mpScript < mpEnd && *mpScript <= 32)
		{
			if (*mpScript++ == '\n') {
				mLine++;
			}
			if (checkEOF ()) return END_OF_FILE;
		}

		// # comments
		if (*mpScript == '#')
		{
			while (mpScript < mpEnd && *mpScript++ != '\n') {
				if (checkEOF ()) return END_OF_FILE;
			}
			mLine++;
			continue;
		}
		break;
	}

	mpToken = mToken;

	if (*mpScript == '"')
	{
		// quoted token
		mpScript++;
		if (checkEOF ()) return END_OF_FILE;
		while (*mpScript != '"')
		{
			if (mpToken - mToken == PARSER_MAXTOKEN)
			{
				*mpToken = 0;
				sys_printf ("ERROR: token too large in file %s line %i, token: %s\n", mFileName, mLine, mToken);
				if (mErrMode) exit(-1);
				return TOKEN_TOO_LARGE;
			}
			*mpToken++ = *mpScript++;
			if (checkEOF ()) return END_OF_FILE;
		}
		mpScript++;
	}
	else
	{
		// separator
		if (*mpScript == '{'
			|| *mpScript == '}'
			|| *mpScript == '['
			|| *mpScript == ']'
			|| *mpScript == '('
			|| *mpScript == ')'
			|| *mpScript == '<'
			|| *mpScript == '>'
			|| *mpScript == ';'
			|| *mpScript == '+'
			)
		{
			*mpToken++ = *mpScript++;
		}
		else
			// regular mToken
			while (*mpScript > 32
				&& *mpScript != '{'
				&& *mpScript != '}'
				&& *mpScript != '['
				&& *mpScript != ']'
				&& *mpScript != '('
				&& *mpScript != ')'
				&& *mpScript != '<'
				&& *mpScript != '>'
				&& *mpScript != ';'
				&& *mpScript != '+'
				)
			{
				if (mpToken - mToken == PARSER_MAXTOKEN)
				{
					*mpToken = 0;
					sys_printf ("ERROR: token too large in file %s line %i\n", mFileName, mLine);
					if (mErrMode) exit(-1);
					return TOKEN_TOO_LARGE;
				}
				*mpToken++ = *mpScript++;
				if (checkEOF ())
					break; // reached EOF while reading token, this is not an error
			}
	}
	*mpToken = 0;
	return 0;
}

void charParser::unGetToken ()
{
	mbTokenReady = true;
}

bool charParser::hasToken ()
{
	int		oldLine;

	bool	r;

	oldLine = mLine;

	r = getToken ();

	if (!r)
		return false;

	unGetToken ();

	if (oldLine == mLine)
		return true;

	return false;
}

int charParser::matchToken (const char *match)
{
	int res;
	res = getToken ();
	if (res)
		return res;

	const char *p1, *p2;
	p1 = mToken;
	p2 = match;
	while (*p1 && *p2) {
		if (*p1 != *p2)
		{
			sys_printf ("WARNING: unmatched token %s (expected %s) in file %s line %d\n", mToken, match, mFileName, mLine);
			if (mErrMode) exit(-1);
			return TOKEN_MISMATCH;
		}
		p1++;
		p2++;
	}
	return 0;
}

void charParser::nextLine (void)
{
	int line = mLine;
	while (line == mLine)
		getToken ();
	unGetToken ();
}

void charParser::ignoreBlock (const cStr &opentag, const cStr &closetag)
{
	int opens = 1;
	while (opens)
	{
		getToken ();
		if (!cmptoken (opentag))
			opens++;
		else if (!cmptoken (closetag))
			opens--;
	}
}

void charParser::getBlockContents (const cStr &opentag, const cStr &closetag, cStr &block)
{
	int opens = 1;
	int ln = -1;
	block = "";
	while (opens)
	{
		bool add = true;
		getToken ();
		if (!cmptoken (opentag))
		{
			opens++;
		}
		else if (!cmptoken (closetag))
		{
			if (--opens == 0)
				add = false;
		}
		if (mLine != ln)
		{
			ln = mLine;
			block += "\r\n";
		}
		else
			block += " ";
		if (add)
			block += token ();
	}
}

const char *charParser::token (void) const
{
	return mToken;
}

bool charParser::cmptoken (const char *s) const
{
	return strcmp (s, mToken) ? true : false;
}

bool charParser::isEOL (void)
{
	if (checkEOF ()) return true;
	while (*mpScript <= 32)
	{
		if (*mpScript == '\n')
			return true;
		mpScript++;
		if (checkEOF ()) return true;
	}
	return false;
}

void charParser::unexpectedEof (void)
{
	sys_printf ("ERROR: unexpected end of file %s, line %d\n", mFileName, mLine);
}

void charParser::generalSyntaxError (void)
{
	sys_printf ("ERROR: syntax error in file %s, line %d\n", mFileName, mLine);
}

void charParser::error (void) {
	sys_printf ("ERROR: error loading file %s at line %d\n", mFileName, mLine);
	sys_exit (-1);
}

}
