/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

// FIXME: CHECK: multiline comment w/ preprocessor may behave wrong
#ifndef __F_PARSER_H
#define __F_PARSER_H

#include <stdio.h>
#include "f_helpers.h"
#include "f_types.h"

#ifndef FE_NO_FILESYSTEM
#include "f_filesystem.h"
#endif

namespace fe {
	
	const int PARSER_BUFSIZE = 1024;
	const int PARSER_MAXFNAME = 256;
	const int PARSER_MAXTOKEN = 256;

	class charParser
	{
	private:
		
	#ifndef FE_NO_FILESYSTEM
		smartPtr<fileSystem> mpFileSystem;
	#endif

		// parser reads file in blocks of PARSER_BUFSIZE sized chunks
		// file is kept opened 'till the dtor is called
		char mBuffer[PARSER_BUFSIZE];
		int mBufferSize;
		char* mpBuffer; // points to either mBuffer, or to passed buffer

		bool mbSTDIO;
		char mFileName[PARSER_MAXFNAME];
		union {
			FILE *mpFileSTDIO;
#ifndef FE_NO_FILESYSTEM
			file *mpFile;
#endif
		};
		int mFileSize;
		int mFileLeft; // how much left in the file

		char* mpScript;
		char* mpEnd;
		int mLine;
		char* mpToken;
		bool mbTokenReady;
		char mToken[PARSER_MAXTOKEN];

		int mErrMode; // will call exit(-1) on any error if mErrMode==1, default is 1
	
	public:
		enum {
			FILE_NOT_FOUND = -1,
			READ_ERROR = -2,
			END_OF_FILE = -3,
			TOKEN_TOO_LARGE = -4,
			TOKEN_MISMATCH = -5,
			FILE_NOT_UNICODE = -6,
			UNKNOWN_ERROR = -7,
			LEX_ERROR = -8,
			FEATURE_NOT_INCLUDED = -9,
		};

		void errMode (int mode);

		charParser (
		#ifndef FE_NO_FILESYSTEM
		fileSystem *fs, const char *buffer, bool stdio = false, int size = -1
		#else
		const char *buffer, int size = -1
		#endif
		);
	
		~charParser (void);
	
		int readNextChunk (void);

		int checkEOF (void);

		int getToken (void);
	
		void unGetToken (void);
	
		bool hasToken (void);
	
		int matchToken (const char *match);
	
		void nextLine (void);
	
		void ignoreBlock (const cStr &opentag, const cStr &closetag);
	
		void getBlockContents (const cStr &opentag, const cStr &closetag, cStr &block);
	
		const char *token (void) const;
	
		bool cmptoken (const char *s) const;
	
		bool isEOL (void);
	
		void unexpectedEof (void);
	
		void generalSyntaxError (void);

		void error (void);
	};
	
}

#endif // __F_PARSER_H

