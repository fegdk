/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_SCENEOBJECT_H
#define __F_SCENEOBJECT_H

#include "f_math.h"
#include "f_types.h"
#include "f_fragileobject.h"
#include "f_objectid.h"
#include "f_helpers.h"
#include "f_animation.h"
#include "backend.h"

namespace fe
{
	
	// models, particle systems and other mesh providers are derived from this
	class geomSource : public baseObject
	{
	protected:
		meshData_t *mpMeshData;
		int mMeshDataCount;
	public:
		geomSource (void)
		{
			mpMeshData = NULL;
			mMeshDataCount = 0;
		}
		~geomSource (void)
		{
			if (mpMeshData)
			{
				delete[] mpMeshData;
				mpMeshData = NULL;
			}
			mMeshDataCount = 0;
		}
		int getMeshDataCount (void) const
		{
			return mMeshDataCount;
		}

		meshData_t* getMeshData (int n) const
		{
			return mpMeshData+n;
		}
	};

	class FE_API renderable : public baseObject
	{
	private:
		smartPtr <geomSource> mpGeomSource;

	// rendering backend stuff
	protected:
		drawSurf_t *mpDrawSurfs;
		int mNumDrawSurfs;
		
	public:
		renderable (geomSource *src)
		{
			mpGeomSource = src;
			mpDrawSurfs = NULL;
			mNumDrawSurfs = 0;
		}
		~renderable (void)
		{
			if (mpDrawSurfs)
			{
				delete[] mpDrawSurfs;
				mpDrawSurfs = NULL;
			}
			mNumDrawSurfs = 0;
		}

		int numDrawSurfs (void) const
		{
			return mNumDrawSurfs;
		}

		drawSurf_t *getDrawSurf (int n) const
		{
			return mpDrawSurfs+n;
		}

		void setDrawSurfs (drawSurf_t *ds, int numds)
		{
			mpDrawSurfs = ds;
			mNumDrawSurfs = numds;
		}

		renderable* copy (void)
		{
			renderable *rend = new renderable (mpGeomSource);
			rend->mpDrawSurfs = new drawSurf_t[mNumDrawSurfs];
			rend->mNumDrawSurfs = mNumDrawSurfs;
			memcpy (rend->mpDrawSurfs, mpDrawSurfs, sizeof (drawSurf_t) * mNumDrawSurfs);
			return rend;
		}
	
	};

	class FE_API sceneObject : public baseObject
	{
	friend class sceneManager;
	protected:
		smartPtr<sceneObject>	mpSceneNext;
		smartPtr<sceneObject>	mpScenePrev;
		sceneObject*	mpParentObject;
		smartPtr <sceneObject>	mpChildren;	// head of children list
		smartPtr <sceneObject>	mpNextChild;	// next object in children list
		smartPtr <renderable> mpRenderable;

		// mpAnimatedChildren contains list of all nodes in this object's hierarchy;
		// helps animation to find bone using single array lookup;
		// list is built at load time
		sceneObject **mpAnimatedChildren;

		matrix4		mRelativeMatrix;
		matrix4		mWorldMatrix;
		matrix4		mInitPosInverseMatrix;	// required for skinned mesh

		uint32	mFlags;
	
		// axis-aligned bounding boxes in world space
		aab3 mBBox;
		aab3 mBBoxWithChildren;
	
		void calcAAB (void);
		
	public:
	
		enum {F_RECALC_MATRIX = 1};
		enum {invalidate_children_tm = 1, recalc_aab = 2};
	
		sceneObject (sceneObject *parent);
		virtual ~sceneObject ();

		sceneObject *getParent (void) const
		{
			return mpParentObject;
		}
	
		sceneObject* getChildren (void) const;
		void addChild (sceneObject *child);
		void removeChild (sceneObject *child);
		sceneObject* getNextChild (void) const;

		void		setRelativeMatrix (const matrix4 &m);
		void		setWorldMatrix (const matrix4 &m);
	
		matrix4	getRelativeMatrix (void) const;
		matrix4	getWorldMatrix (void) const;
		matrix4	getSkinnedBoneRelativeMatrix (void) const;
		matrix4	getSkinnedBoneWorldMatrix (void) const;

		virtual void update (ulong updateFlags = 0);

		void attachRenderable (renderable *rend)
		{
			mpRenderable = rend;
		}

		renderable *getRenderable (void) const
		{
			return mpRenderable;
		}

		sceneObject *getNewInstance (sceneObject *parent = NULL);
		void initAnimData (animContainer *cont);
		sceneObject **getAnimatedChildren (void);
		
		const aab3 &getBoundingBox (bool withChildren);
	};
	
	typedef smartPtr <sceneObject> sceneObjectPtr;
	
}

#endif // __F_SCENEOBJECT_H
