/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_logger.h"

namespace fe
{

	void logger::init (const char *fname)
	{
		free ();
		if (!fname)
		{
			mpFile = stdout;
		}
	}
	
	logger::logger (const char *fname, int dbglevel)
	{
		mpFile = NULL;
		mDbgLevel = dbglevel;
		init (fname);
	}
	
	void logger::free (void)
	{
		if (mpFile && mpFile != stdout)
		{
			fclose (mpFile);
		}
		mpFile = NULL;
	}
	
	logger::~logger (void)
	{
		free ();
	}
	
	void logger::setFilename (const char *fname)
	{
		init (fname);	
	}
	
	void logger::setDbgLevel (int dbglevel)
	{
		mDbgLevel = dbglevel;	
	}
	
	int logger::getDbgLevel (void) const
	{
		return mDbgLevel;
	}
	
	void logger::dbgPrint (int dbglevel, const char *fmt, ...)
	{
		if (mpFile && mDbgLevel >= dbglevel)
		{
			va_list ap;
			va_start (ap, fmt);
			vfprintf (mpFile, fmt, ap);
			va_end (ap);
		}
	}
	
}
