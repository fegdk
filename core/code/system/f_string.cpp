/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_string.h"

namespace fe
{

	void cStr::grow (size_t s)
		{
			_Res = s;
			char *buf = new char[ _Res + 1 ];
			assert (buf);
			if (_Ptr)
			{
				strcpy (buf, _Ptr);
				delete[] _Ptr;
			}
			else
				memset (buf, 0, sizeof (char) * (_Res + 1) );
			_Ptr = buf;
		}
	
	cStr::~cStr ()
	{
		if (_Ptr)
			delete[] _Ptr;
	}
	
}
