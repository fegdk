/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_KEYBINDER_H
#define __F_KEYBINDER_H

#include "f_baseobject.h"

namespace fe
{
	
	/** The keyBinderClass is used to bind key combinations to some user-specified actions.
	 * Only one action may be bound to one keycombo.
	 */
	class FE_API keyBinder : public baseObject
	{
	private:
		// the whole system may be a bit slow due to std::map lookup
		// --> may be replaced with ushort array w/ 64k cost
		
		// maps keycode to action
		typedef std::map <int, cStr> handlersMap;
		handlersMap mHandlers;
	
	public:
	
		enum { lalt = (1<<16), lctrl = (1<<17), lshift = (1<<18), ralt = (1<<19), rctrl = (1<<20), rshift = (1<<21), alt = (1<<22), shift = (1<<23), ctrl = (1<<24)};
	
		keyBinder (void);
		virtual ~keyBinder (void);
	
		/** Binds keycombo to a console command (string value).
		 * @param keycombo Same as for bindToUserAction
		 * @param cmd Pointer to a string, containint console command
		 */
		void bind (int keycombo, const char *cmd);
	
		/** Unbinds specified combo.
		* @param keycombo Key combination
		 */
		void unbind (int keycombo);
	
		/** Unbinds all keys, i.e. resets entire system.
		 */
		void unbindAll (void);
	
		/**
		 * @return keycode for string value
		 */
		int keycodeForString (const char *str) const;
	
		/**
		 * Handles key press.
		 * @param keycode specifies keycombo to handle
		 * @return true if keycombo was accepted
		 */
		bool keyDown (int keycombo);
	
		/**
		 * Handles key release.
		 * @param keycombo specifies keycombo to handle
		 * @return true if keycombo was accepted
		 */
		bool keyUp (int keycombo);
	
	};
	
}

#endif // __F_KEYBINDER_H
