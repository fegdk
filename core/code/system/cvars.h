#ifndef __CVARS_H
#define __CVARS_H

#include "f_baseobject.h"

namespace fe
{
	enum
	{
		CVAR_INT,
		CVAR_FLOAT,
		CVAR_STRING
	};

	enum
	{
		CVAR_SVALUE_SIZE = 64
	};

	enum
	{
		CVAR_STORE = 0x00000001,    // save to config
		CVAR_READONLY = 0x00000002, // can't be set by user
		CVAR_SERVER = 0x00000004,   // propagate to all clients
		CVAR_CLIENT = 0x00000008,   // sent to server on user state changes
		CVAR_USERCREATED = 0x00000010, // created using set
		CVAR_INIT = 0x00000020, // set from cmdline only
		CVAR_LATCH = 0x00000040, // will be changed upon restart (cvar_restart or next cvars->get)
	};

	struct cvar_t
	{
		char *string;
		char *name;
		char *resetString;
		char *latchedString;
		uint32 flags;
		bool modified;
		int modificationCount;
		float fvalue;
		int ivalue;
		cvar_t *next;
		cvar_t *hashNext;
	};

	struct field_t;

	class cvarManager : public baseObject
	{
	private:
		
		enum { CVARS_MAX = 1024, CVARS_HASH_SIZE = 256 };

		cvar_t mCVars[CVARS_MAX];
		int mNumCVars;

		cvar_t *mpCVarList;
		cvar_t *mpCVarsHash[CVARS_HASH_SIZE];

		uint32 mModifiedFlags;

		uint32
		hashValue (const char *s);
		
		char*
		copyString (const char *s);

		cvar_t*
		find (const char *s);

		bool
		validateString (const char *s);

	public:
		
		cvarManager (void);
		~cvarManager (void);

		cvar_t*
		get (const char *name, const char *value, uint32 flags);

		cvar_t*
		set (const char *name, const char *value, bool force);
		
		cvar_t*
		setInt (const char *name, int value, bool force);
		
		cvar_t*
		setFloat (const char *name, float value, bool force);

		const char *
		stringValue (const char *name);

		void
		stringValueBuffer (const char *name, char *buffer, int len);

		int
		intValue (const char *name);

		float
		floatValue (const char *name);

		void
		commandCompletion (void (*matchfunc)(const char *cmd, field_t *data), field_t *data);

		int
		getCVarIdx (const char *n);
		
		cvar_t*
		cvarForIdx (int idx);
	};
}

#endif // __CVARS_H

