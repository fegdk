/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_CONSOLE_H
#define __F_CONSOLE_H

#include "f_string.h"
#include "f_math.h"
#include "f_helpers.h"
#include "f_baseobject.h"
//#include "f_fontft.h"

namespace fe
{
	
	class console;
	/**
	 * Helper macros for quick console command creation
	 */
	#define FE_REGISTER_CONSOLE_CMD(name) \
		fe::g_engine->getConsole ()->addCommand (#name, Cmd_##name##_f);

	int Cmd_Argc (void);
	const char *Cmd_Argv(int arg);
	void Con_Printf (const char *fmt, ...);
	
	template <int num, int strsize> class fixedStrDeque
	{
	public:
		char pool[num][strsize];
		int cnt;
		int start;

		fixedStrDeque (void)
		{
			cnt = 0;
			start = 0;
		}

		void add (const char *str)
		{
			if (strlen (str) >= strsize)
			{
				fprintf (stderr, "ERROR: string is too long for fixedStrDeque<%d,%d>\n", num, strsize);
				return;
			}
			if (cnt == num)
			{
				strcpy (pool[start], str);
				start++;
				if (start >= num)
					start -= num;
			}
			else
			{
				strcpy (pool[start+cnt], str);
				cnt++;
			}
		}

		const char *get (int idx)
		{
			idx += start;
			while (idx >= cnt)
				idx -= cnt;
			return pool[idx];
		}
	};

	enum
	{
		MAX_FIELD_SIZE = 256
	};

	struct field_t
	{
		char buffer[MAX_FIELD_SIZE];
		int cursor;
		int matchCount;
		char shortestMatch[MAX_FIELD_SIZE];

		void completeCommand (bool use_commands, bool use_cvars);
		static void findMatches (const char *s, field_t *obj);
		static void printMatches (const char *s, field_t *obj);
		static void printCVarMatches (const char *s, field_t *obj);
	};

	/**
	 * Implementation of developer console, which can be used to create/run commands, query/set console variables, etc.
	 */
	class FE_API console : public baseObject
	{
	protected:
	
		enum {
			MAX_CMD_HISTORY = 100,
			MAX_MESSAGE_HISTORY = 100,
			MAX_MESSAGE_LEN = 100,
			MAX_ALIASES = 100,
			MAX_ALIAS_LEN = 100,
			MAX_COMMANDS = 1000,
			MAX_ALIAS_NAME = 32
		};

		fixedStrDeque<MAX_CMD_HISTORY, MAX_MESSAGE_LEN> mCmdHistory;
		fixedStrDeque<MAX_MESSAGE_HISTORY, MAX_MESSAGE_LEN> mMessageHistory;

		int mCursor;
		int mCurrentCmd;

		struct command_t
		{
			const char *name;
			void (*exec)(void);
			command_t *hashNext;
		};

		struct alias_t
		{
			char left[MAX_ALIAS_NAME];
			char right[MAX_ALIAS_LEN];
		};

		enum
		{
			CMD_HASH_SIZE = 256
		};
		command_t mCommands[MAX_COMMANDS];
		int mNumCommands;
		command_t* mCommandHash[CMD_HASH_SIZE];
		
		alias_t mAliases[MAX_ALIASES];
		int mNumAliases;
		
		bool			mVisible;
	
		field_t mInput;
//		char mInput[MAX_MESSAGE_LEN];
//		int				mInputCursor;
	
		float			mToggleTime;
	
		~console (void);
	
		int maxNumLines (void) const;

		command_t* commandFind (const char *name);

	public:
	
		console (const char *logFName = NULL);
	
		void render (void);
		void show (bool);
		void toggle (void);
		void msg (const char *message);
		void command (const char *cmd);
		bool keyDown (int);				// returns true if accepted
		bool isVisible (void) const { return mVisible; }
		void addCommand (const char *name, void (*exec)(void));
		void setAlias (const char *name, const char *value);
		void commandCompletion (void (*matchfunc)(const char *cmd, field_t *data), field_t *data);
	
	};
	
}

#endif // __F_CONSOLE_H

