/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __ANIMSEQUENCER_H
#define __ANIMSEQUENCER_H

#include "f_baseobject.h"

namespace fe
{

	enum
	{
		MAX_PLAYING_ANIMS	= 1000
	};

	class animation;
	class sceneObject;

	struct animState_t
	{
		float startTime;
		animation* anim;
		sceneObject* obj;
	};

	/**
	 * @brief holds state for each playing animation
	 */
	class FE_API animSequencer : public baseObject
	{
	private:

		animState_t mAnims[MAX_PLAYING_ANIMS];
		int mNumAnims;
		
	public:

		animSequencer (void);
		~animSequencer (void);

		int startAnimation (sceneObject *obj, animation *anim);
		void stopAnimation (int anim);
		void update (void);
		animState_t *getAnim (int anim);
	};

}

#endif // __ANIMSEQUENCER_H

