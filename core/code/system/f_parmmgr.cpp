/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_parmmgr.h"
#include "f_engine.h"
#include "f_timer.h"
//#include "f_math.h"
//#include "f_helpers.h"
//#include "f_console.h"
#include "f_util.h"

namespace fe
{
	
	// some common parms
	
	class parmTime : public parmInterface
	{
	private:
		float time;
	public:
		float*	get (void)
		{
			time = g_engine->getTimer ()->getTime ();	// NOTE: can cause precision problems on large values
			return &time;
		}
		int		size (void)
		{
			return 1;
		}
	};
	
	class parmSin : public parmInterface
	{
	private:
		float sint[360];
	public:
		parmSin (void)
		{
			for (int i = 0; i < 360; i++)
				sint[i] = (float)sin (i*M_PI/180.f);
		}
		float*	get (void)
		{
			return sint;
		}
		int		size (void)
		{
			return 360;
		}
	};
	
	parmMgr::parmMgr (void)
	{
		smartPtr <parmInterface> i;
		for (int k = 0; k < PARMS_HASHSIZE; k++)
			mParmHash[k] = -1;
		mNumParms = 0;
	
		i = new parmTime;
		regParm ("time", i);
	
		i = new parmSin;
		regParm ("sin", i);
	}
	
	parmMgr::~parmMgr ()
	{
		sys_printf ("parmmgr clean!\n");
	}
	
	char	parmMgr::regParm (const char *name, parmInterface *intr)
	{
		if (mNumParms == MAX_PARMS)
			sys_error ("too many parms while trying to register %s\n", name);
		strncpy (intr->name, name, PARM_MAXNAME);
		uint32 hash = hashValue (name, PARMS_HASHSIZE);
		if (mParmHash[hash] == -1) {
			intr->hashNext = -1;
		}
		else {
			intr->hashNext = mParmHash[hash];
		}
		mParmHash[hash] = mNumParms;
		mParmArray[mNumParms++] = intr;
		return mNumParms-1;
	}
	
	void	parmMgr::unregParm (const char *name)
	{
		uint32 hash = hashValue (name, PARMS_HASHSIZE);
		if (mParmHash[hash] == -1) {
			sys_error ("unregParm: parm '%s' not registered", name);
		}
		char i = mParmHash[hash];
		parmInterface *prev = NULL;
		while (i != -1) {
			if (!strcmp (name, mParmArray[i]->name)) {
				if (!prev) {
					mParmHash[hash] = -1;
				}
				else {
					prev->hashNext = i;
				}
				break;
			}
			i = mParmArray[i]->hashNext;
		}
	}
	
	char	parmMgr::indexForName (const char *name)
	{
		uint32 hash = hashValue (name, PARMS_HASHSIZE);
		if (mParmHash[hash] == -1) {
			return -1;
		}
		char i = mParmHash[hash];
		while (i != -1) {
			if (!strcmp (name, mParmArray[i]->name))
				return i;
			i = mParmArray[i]->hashNext;
		}
		return -1;
	}
	
	float*	parmMgr::getData (char index)
	{
		return mParmArray[index]->get ();
	}

	float	parmMgr::getValue (char index, float pos)
	{
		parmInterface *i = mParmArray[index];
		float f = floor (pos);
		int pos1 = int (f) % i->size ();
		int pos2 = int (ceil (pos)) % i->size ();
		float v1 = i->get ()[pos1];
		float v2 = i->get ()[pos2];
		float alpha = pos - f;
		return v1 * (1-alpha) + v2 * alpha;
	}
	
	int		parmMgr::getParmSize (char index)
	{
		return mParmArray[index]->size ();
	}
	
}
