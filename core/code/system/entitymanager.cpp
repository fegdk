#include "pch.h"
#include "entitymanager.h"
#include "entclasses.h"

namespace fe
{

	entityManager::entityManager (void)
	{
		mLastId = 0;
	}

	entityManager::~entityManager (void)
	{
		clean ();
	}

	void entityManager::clean (void)
	{
		while (mEntsList)
			mEntsList = mEntsList->next;
		memset (mEnts, 0, sizeof (mEnts));
		memset (mEntsByClass, 0, sizeof (mEntsByClass));
	}

	uint32 entityManager::getEClassForClassName (const char *classname) const
	{
		if (!strcmp (classname, "info_player_start"))
			return ECLASS_SPAWNSPOT;
		return ECLASS_MAX;
	}

	entity* entityManager::createEntity (uint32 classId)
	{
		smartPtr <entity> ent;
		switch (classId)
		{
		case ECLASS_WORLD:
			ent = new entWorld (mLastId);
			break;
		case ECLASS_SPAWNSPOT:
			ent = new entSpawnSpot (mLastId);
			break;
		case ECLASS_PHYSXOBJ:
			fprintf (stderr, "creating PHYSXOBJ\n");
			ent = new entPhysXObj(mLastId);
			break;
		default:
			return NULL;
		}
		mEnts[mLastId] = ent;
		ent->nextInClass = mEntsByClass[classId];
		mEntsByClass[classId] = ent;
		mLastId++;
		return ent;
	}

	void entityManager::destroyEntity (uint32 id)
	{
		if (!mEnts[id]) {
			sys_printf ("WARNING: attempt to destroy non-existing entity (%d)\n", id);
			return;
		}
		// remove from mEntsList and mEntsByClass and set NULL in mEnts
		entity *e = mEntsList, *prev = NULL;
		while (e && e != mEnts[id]) {
			prev = e;
			e = e->next;
		}
		if (e) {
			if (prev)
				prev->next = e->next;
			else
				mEntsList = e->next;
		}

		prev = NULL;
		e = mEntsByClass[mEnts[id]->getClass()];
		while (e && e != mEnts[id]) {
			prev = e;
			e = e->nextInClass;
		}
		if (e) {
			if (prev)
				prev->next = e->next;
			else
				mEntsByClass[mEnts[id]->getClass()] = e->next;
		}
		mEnts[id] = NULL;
	}

	void entityManager::destroyEntity (entity *ent)
	{
		destroyEntity (ent->getId ());
	}

	entity* entityManager::getEntityForId (uint32 id)
	{
		assert (id < mLastId);
		return mEnts[id];
	}
	
	entity *entityManager::getEntListForClass (uint32 classId)
	{
		return mEntsByClass[classId];
	}

}

