#include "pch.h"
#include "entity.h"

namespace fe
{

	entity::entity (uint32 id)
	{
		mId = id;
	}

	entity::~entity (void)
	{
	}

	uint32 entity::getId (void) const
	{
		return mId;
	}

	void entity::fire (uint32 sourceEnt)
	{
	}
}
