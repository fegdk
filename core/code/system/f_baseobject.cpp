/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_baseobject.h"
#include "f_helpers.h"

namespace fe
{

	int baseObject::mLastId = 0;
	baseObject *baseObject::mpObjList = NULL;
	
	class leakManager
	{
	public:
		leakManager ();
		~leakManager ();
	};
	
	leakManager lm;
	
	leakManager::leakManager ()
	{
	}
	
	leakManager::~leakManager ()
	{
		baseObject *o = baseObject::mpObjList;
		while (o) {
			fprintf (stderr, "memory leak: id=%Xh, refc=%d ptr=%Xh, name='%s', data='", o->mId, o->mRefc, o, o->name());
			for (int i = 0; i < 20; i++)
			{
				try {
					uchar c = ((char*)o)[i];
					if (c >= 32)
						fprintf (stderr, "%c", c);
					else
						fprintf (stderr, ".");
				}
				catch (...) {
					fprintf (stderr, ".");
				}
			}
			fprintf (stderr, "'\n");
			o = o->mpNextObj;
		}
	}
	
	baseObject::~baseObject (void)
	{
		if (mpPrevObj) {
			mpPrevObj->mpNextObj = mpNextObj;
		}
		else {
			mpObjList = mpNextObj;
		}
		if (mpNextObj) {
			mpNextObj->mpPrevObj = mpPrevObj;
		}
	}
	
	baseObject::baseObject (void)
	{
		mpPrevObj = NULL;
		mpNextObj = mpObjList;
		mpObjList = this;
		mpResHashNext = NULL;
		mRefc = 0;
		mId = ++mLastId;
		mbValid = true;
		mUsedRefc = -1;
	}
	
	void baseObject::addRef (void)
	{
	//	fprintf (stderr, "object %s addreffed to %d\n", mName.c_str (), mRefc);
		mRefc++;
	}
	
	void baseObject::release (void)
	{
		// <= is intentional -- client may prefer to not use smartpointers and refcounting
		if (--mRefc <= 0)
		{
	//		fprintf (stderr, "object %s released to %d\n", mName.c_str (), mRefc);
			delete this;
		}
	}
	
	const char* baseObject::name (void) const
	{
		return mName;
	}
	
	void baseObject::setName (const char *nm)
	{
		mName = nm;
	}
	
	bool baseObject::isUsed (void) const
	{
		// mbUsed flag must be set by resource mgr when non-parser creation method is used
		// reference counter is remembered at the moment
		// when refc reaches the same value "used" flag is being reset to 0
		return mRefc > 1 ? true : false;
	}
		
	void baseObject::loadData (void)
	{
	}
	
	bool baseObject::isValid (void) const
	{
		return mbValid;
	}
	
	
	bool baseObject::isTypeOf (int type) const
	{
		return false;
	}
	
}

