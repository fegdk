/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_LOGGER_H
#define __F_LOGGER_H

#include "f_baseobject.h"

namespace fe
{
	
	// debug levels are meant to [un]block some or all
	// of debug messages spammed by the engine and the program. 
	// the more the value of debug level is - the more spam u'll recieve
	//
	// 0 is engine critical 
	// 10 is application-side messages
	// 20 is engine non-critical error messages
	// 30 verbose engine information
	//
	// you can use something in between too. just set dbg level accordingly
	
	class FE_API logger : public baseObject
	{
	private:
	
		FILE *mpFile;
		int mDbgLevel;
		
		void init (const char *fname);
		void free (void);
	
	public:
		logger (const char *fname, int dbglevel = 0);
		~logger (void);
	
		// changes output of logger to different file
		void setFilename (const char *fname);
	
		void setDbgLevel (int dbglevel);
		int getDbgLevel (void) const;
		void dbgPrint (int dbglevel, const char *fmt, ...);
	};
	
}

#endif // __F_LOGGER_H

