/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_PARMMGR_H
#define __F_PARMMGR_H

#include "f_types.h"
#include "f_baseobject.h"

namespace fe
{
	const int MAX_PARMS = 128;
	const int PARMS_HASHSIZE = 16;
	const int PARM_MAXNAME = 16;
	class parmInterface : public baseObject
	{
	friend class parmMgr;
	protected:
		char hashNext;
		char name[PARM_MAXNAME];
	public:
		
		virtual float*	get (void) = 0;
		virtual int		size (void) = 0;
	};
	
	class parmMgr : public baseObject
	{
	
	private:
	
		// this is used for name-based lookups
		char mParmHash[PARMS_HASHSIZE];
		// this is used for index-based lookups
		smartPtr <parmInterface> mParmArray[MAX_PARMS];
		int mNumParms;
	
	public:
		
		parmMgr (void);
		~parmMgr (void);
	    char	regParm (const char *name, parmInterface *intr);
	    // this function is unsafe, because index references will be left.
	    // therefore, mParmArray is left unchanged after this call, so that index-references are valid.
	    // it is better to reinitialize parmMgr entirely when parms need to be reset.
		void	unregParm (const char *name);
	
		char	indexForName (const char *name);
		float*	getData (char index);
		float	getValue (char index, float pos);
		int		getParmSize (char index);
	
	};
	
}

#endif // __F_PARMMGR_H

