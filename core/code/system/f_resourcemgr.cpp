/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "config.h"
#include "f_texture.h"
#include "f_fontft.h"
#include "f_material.h"
#include "f_animation.h"
#include "f_model.h"
#if HAVE_LIBCG
#include "f_cgshader.h"
#endif
#include "f_ps14shader.h"
#include "f_effect.h"
#include "f_resourcemgr.h"
#include "f_error.h"
#include "f_engine.h"
#include "f_util.h"

namespace fe
{
	
	resourceMgr::resourceMgr (void)
	{
		setName ("resource mgr");
		memset (mResourceHash, 0, sizeof (mResourceHash));
		mNumResources = 0;
		memset (mResCounts, 0, sizeof (mResCounts));
	}
	
	resourceMgr::~resourceMgr (void)
	{
		if (mNumResources) {
			baseObject **lst = new baseObject*[mNumResources];
			int i = 0;
			for (int h = 0; h < RES_HASHSIZE; h++) {
				baseObject *o = mResourceHash[h];
				while (o) {
					lst[i++] = o;
					o = o->mpResHashNext;
				}
			}
			while (i>0) {
				lst[--i]->release ();
			}
			delete[] lst;
		}
	}
	

//	baseObject* resourceMgr::create (const char *name)
//	{
//		baseObject *o = getResource (name);
//		if (!o)
//		{
//			o = new baseObject (name);
//			if (!o->isValid ())
//			{
//				o->release ();
//				return NULL;
//			}
//			addResource (name, o);
//		}
//		return o;
//	}
//	
//	baseObject* resourceMgr::create (charParser &parser)
//	{
//		parser.getToken ();
//		cStr name = parser.token ();
//		baseObject *o = getResource (name);
//		if (o)
//		{
//			fprintf (stderr, "WARNING: object with name %s is already defined\n", name.c_str ());
//			return o;
//		}
//		baseObject *obj = new baseObject (parser, name);
//		if (!obj->isValid ())
//		{
//			o->release ();
//			return NULL;
//		}
//		addResource (name, obj);
//		return obj;
//	}

	void resourceMgr::destroy (const char *name)
	{
		removeResource (name);
	}
	
	void resourceMgr::flushUnused (void)
	{
		if (mNumResources) {
			baseObject **lst = new baseObject*[mNumResources];
			int i = 0;
			for (int h = 0; h < RES_HASHSIZE; h++) {
				baseObject *o = mResourceHash[h];
				while (o) {
					lst[i++] = o;
					o = o->mpResHashNext;
				}
			}
			i--;
			while (i>=0) {
				if (!lst[i]->isUsed ())
					lst[i]->release (); // this SHOULD remove it from resource manager
			}
			delete[] lst;
		}
	}
	
	baseObject* resourceMgr::getResource (const char *name)
	{
		int h = hashValue (name, RES_HASHSIZE);
		baseObject* o = mResourceHash[h];
		while (o) {
			if (!strcmp (o->name(), name))
				return o;
			o = o->mpResHashNext;
		}
		return NULL;
	}

	void resourceMgr::addResource (const char *name, baseObject *r)
	{
		int h = hashValue (name, RES_HASHSIZE);
		r->mpResHashNext = mResourceHash[h];
		mResourceHash[h] = r;
		r->addRef ();
		mNumResources++;
	}
	
	void resourceMgr::removeResource (const char *name)
	{
		int h = hashValue (name, RES_HASHSIZE);
		baseObject* o = mResourceHash[h];
		baseObject* prev = NULL;
		while (o) {
			if (!strcmp (o->name(), name)) {
				if (prev) {
					prev->mpResHashNext = o->mpResHashNext;
				}
				else {
					mResourceHash[h] = o->mpResHashNext;
				}
				o->release ();
				return;
			}
			prev = o;
			o = o->mpResHashNext;
		}
		sys_printf ("WARNING: cannot delete '%s' - no such resource\n", name);
	}


//	resource <texture> *resourceMgr::getTextureMgr (void)
//	{
//		return &mTextureMgr;
//	}
//
//	resource <material> *resourceMgr::getMaterialMgr (void)
//	{
//		return &mMaterialMgr;
//	}
//
//	resource <fontFT> *resourceMgr::getFontFTMgr (void)
//	{
//		return &mFontMgr;
//	}
//
//	resource <sceneObject> *resourceMgr::getModelMgr (void)
//	{
//		return &mModelMgr;
//	}
//
//	resource <animation> *resourceMgr::getAnimationMgr (void)
//	{
//		return &mAnimationMgr;
//	}
//
//	resource <cgShader> *resourceMgr::getCGShaderMgr (void)
//	{
//#if HAVE_LIBCG
//		return &mCGShaderMgr;
//#else
//		return NULL;
//#endif
//	}
//
//	resource <ps14Shader> *resourceMgr::getPS14ShaderMgr (void)
//	{
//		return &mPS14ShaderMgr;
//	}
//
//	resource <effect> *resourceMgr::getEffectMgr (void)
//	{
//		return &mEffectMgr;
//	}

	const char tex_notfound[] = "textures/common/notfound.png";
	texture* resourceMgr::createTexture (const char *name)
	{
		baseObject *o = getResource (name);
		if (o)
			return checked_cast<texture*>(o);
		texture *t = new texture (name);
		if (!t->isValid ())
		{
			t->release ();
			if (!strcmp (name, tex_notfound))
				return createTexture (tex_notfound);
			sys_error ("createTexture: failed to load default texture\n");
		}
		addResource (name, t);
		return t;
	}

	texture* resourceMgr::createTexture (charParser &parser)
	{
		parser.getToken ();
		cStr name = parser.token ();
		baseObject *o = getResource (name);
		if (o)
		{
			sys_printf ("WARNING: object with name %s is already defined\n", name.c_str ());
			return checked_cast<texture*>(o);
		}
		texture *t = new texture (parser, name);
		if (!t->isValid ())
		{
			t->release ();
			if (!strcmp (name, tex_notfound))
				return createTexture (tex_notfound);
			sys_error ("createTexture: failed to load default texture\n");
		}
		addResource (name, t);
		return t;
	}

	material* resourceMgr::createMaterial (const char *name)
	{
		baseObject *o = getResource (name);
		if (o)
			return checked_cast<material*>(o);
		material *t = new material (name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createMaterial failed for %s\n", name);
		}
		addResource (name, t);
		mResources[RES_MATERIAL][mResCounts[RES_MATERIAL]++] = t;
		return t;
	}

	material* resourceMgr::createMaterial (charParser &parser)
	{
		parser.getToken ();
		cStr name = parser.token ();
		baseObject *o = getResource (name);
		if (o)
		{
			sys_printf ("WARNING: object with name %s is already defined\n", name.c_str ());
			return checked_cast<material*>(o);
		}
		material *t = new material (parser, name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createMaterial failed for %s\n", name.c_str ());
		}
		addResource (name, t);
		mResources[RES_MATERIAL][mResCounts[RES_MATERIAL]++] = t;
		return t;
	}

	fontFT* resourceMgr::createFontFT (const char *name)
	{
		baseObject *o = getResource (name);
		if (o)
			return checked_cast<fontFT*>(o);
		fontFT *t = new fontFT (name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createFontFT failed for %s\n", name);
		}
		addResource (name, t);
		return t;
	}

	fontFT* resourceMgr::createFontFT (charParser &parser)
	{
		parser.getToken ();
		cStr name = parser.token ();
		baseObject *o = getResource (name);
		if (o)
		{
			sys_printf ("WARNING: object with name %s is already defined\n", name.c_str ());
			return checked_cast<fontFT*>(o);
		}
		fontFT *t = new fontFT (parser, name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createFontFT failed for %s\n", name.c_str ());
		}
		addResource (name, t);
		return t;
	}

	static void loadModelFromFile (file *f, int majorver, int minorver, sceneObject *node, modelMtlList &mtls)
	{
		if (minorver >= 2)
		{
			char nsz;
			f->read (&nsz, sizeof (char));
			if (nsz)
			{
				char nm[100];
				f->read (nm, nsz);
				nm[nsz] = 0;
				node->setName (nm);
			}
		}

		uint32 sp;
	
		// matrix
		matrix4 mtx;
		f->readMatrix4 (&mtx, 1);
		matrix4 worldMtx = node->getParent () ? mtx * node->getParent ()->getWorldMatrix() : mtx;
		node->setRelativeMatrix (mtx);
		node->setWorldMatrix (worldMtx);

		model *mdl = NULL;
		// ok, try mesh subsets
		f->readUINT32 (&sp, 1);
		if (sp)
		{
			mdl = new model ();
			renderable *rend = new renderable (mdl);
			node->attachRenderable (rend);
			mdl->loadMesh (f, majorver, minorver, mtls, sp, rend);
		}
		else
		{
			// skip mesh flags
			
			// tangents
			uchar tangents;
			f->read (&tangents, sizeof (uchar));

			// lightmaps
			uchar lightmaps = 0;
			if (minorver >= 1)
				f->read (&lightmaps, sizeof(uchar));

			// numverts
			f->readUINT32 (&sp, 1);

			// numfaces
			f->readUINT32 (&sp, 1);
			
			// old version animation flag
			if (minorver < 2)
			{
				uchar b;
				f->read (&b, sizeof (b));
			}
		}

		// load children
		f->readUINT32 (&sp, 1);
		int nChildren = sp;
		
		for (uint32 i = 0; i < nChildren; i++)
		{
			sceneObject *c = new sceneObject (node);
			loadModelFromFile (f, majorver, minorver, c, mtls);
			node->addChild (c);
		}
	}

	static sceneObject *loadModelFromFile (const char *fname)
	{
		// sceneobject must be created for each node
		// model must be created only where mesh exists
		file *f = g_engine->getFileSystem ()->openFile (fname, FS_MODE_READ);

		int majorver = 1;
		int minorver = 0;
		sceneObject *node = new sceneObject (NULL);
		// read magic
		char mgc[10];
		f->read( mgc, 9 );
		mgc[9] = 0;
		// for now i support only 1.1
		if (!strcmp (mgc, "FEMDL0101"))
		{
			majorver = 1;
			minorver = 1;
		}
		else if (!strcmp (mgc, "FEMDL0102"))
		{
			majorver = 1;
			minorver = 2;
		}
		else
			sys_error("Invalid model file signature (%s).\n", f->name());

		// load materials
		modelMtlList mtls;
		mtls.load (f);

		loadModelFromFile (f, majorver, minorver, node, mtls);
		f->close ();

		return node;
	}

	sceneObject* resourceMgr::createModel (const char *name, animContainer *anim)
	{
		// model is created specially, cause we need instancing
		baseObject *o = getResource (name);
		sceneObject *obj = NULL;
		if (o)
			obj = checked_cast<sceneObject*>(o);
		else {
			// load template
			obj = loadModelFromFile (name);
			if (!obj)
				sys_error ("createModel failed on %s", name);
			addResource (name, obj);
		}

		// create from template
		sceneObject *newModel = obj->getNewInstance ();
		if (anim)
			newModel->initAnimData (anim);
		return newModel;
	}

	sceneObject* resourceMgr::createModel (charParser &parser)
	{
		return NULL;
	}

	animation* resourceMgr::createAnimation (const char *name)
	{
		baseObject *o = getResource (name);
		if (o)
			return checked_cast<animation*>(o);
		animation *t = new animation (name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createAnimation failed for %s\n", name);
		}
		addResource (name, t);
		return t;
	}

	animation* resourceMgr::createAnimation (charParser &parser)
	{
		parser.getToken ();
		cStr name = parser.token ();
		baseObject *o = getResource (name);
		if (o)
		{
			sys_printf ("WARNING: object with name %s is already defined\n", name.c_str ());
			return checked_cast<animation*>(o);
		}
		animation *t = new animation (parser, name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createAnimation failed for %s\n", name.c_str ());
		}
		addResource (name, t);
		return t;
	}
	
#ifdef HAVE_LIBCG
	cgShader* resourceMgr::createCGShader (const char *name)
	{
		baseObject *o = getResource (name);
		if (o)
			return checked_cast<cgShader*>(o);
		cgShader *t = new cgShader (name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createCGShader failed for %s\n", name);
		}
		addResource (name, t);
		return t;
	}

	cgShader* resourceMgr::createCGShader (charParser &parser)
	{
		parser.getToken ();
		cStr name = parser.token ();
		baseObject *o = getResource (name);
		if (o)
		{
			sys_printf ("WARNING: object with name %s is already defined\n", name.c_str ());
			return checked_cast<cgShader*>(o);
		}
		cgShader *t = new cgShader (parser, name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createCGShader failed for %s\n", name.c_str ());
		}
		addResource (name, t);
		return t;
	}
#endif
	
	ps14Shader* resourceMgr::createPS14Shader (const char *name)
	{
		baseObject *o = getResource (name);
		if (o)
			return checked_cast<ps14Shader*>(o);
		ps14Shader *t = new ps14Shader (name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createPS14Shader failed for %s\n", name);
		}
		addResource (name, t);
		return t;
	}
	ps14Shader* resourceMgr::createPS14Shader (charParser &parser)
	{
		parser.getToken ();
		cStr name = parser.token ();
		baseObject *o = getResource (name);
		if (o)
		{
			sys_printf ("WARNING: object with name %s is already defined\n", name.c_str ());
			return checked_cast<ps14Shader*>(o);
		}
		ps14Shader *t = new ps14Shader (parser, name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createPS14Shader failed for %s\n", name.c_str ());
		}
		addResource (name, t);
		return t;
	}
	
	effect* resourceMgr::createEffect (const char *name)
	{
		baseObject *o = getResource (name);
		if (o)
			return checked_cast<effect*>(o);
		effect *t = new effect (name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createEffect failed for %s\n", name);
		}
		addResource (name, t);
		return t;
	}

	effect* resourceMgr::createEffect (charParser &parser)
	{
		parser.getToken ();
		cStr name = parser.token ();
		baseObject *o = getResource (name);
		if (o)
		{
			sys_printf ("WARNING: object with name %s is already defined\n", name.c_str ());
			return checked_cast<effect*>(o);
		}
		effect *t = new effect (parser, name);
		if (!t->isValid ())
		{
			t->release ();
			sys_error ("createEffect failed for %s\n", name.c_str ());
		}
		addResource (name, t);
		return t;
	}
	int resourceMgr::getCount (int type) const
	{
		return mResCounts[type];
	}
	baseObject* resourceMgr::getObject (int type, int idx)
	{
		return mResources[type][idx];
	}
}
