/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#include "pch.h"
#include "f_keybinder.h"
#include "f_keycodes.h"
#include "f_engine.h"
#include "f_console.h"
#include "f_input.h"

namespace fe
{
	
	keyBinder::keyBinder (void)
	{
	}
	
	keyBinder::~keyBinder (void)
	{
	}
	
	void keyBinder::bind (int keycombo, const char *cmd)
	{
		mHandlers[keycombo] = cmd;
	}
	
	void keyBinder::unbind (int keycombo)
	{
		handlersMap::iterator it = mHandlers.find (keycombo);
		if (it != mHandlers.end ())
			mHandlers.erase (it);
	}
	
	void keyBinder::unbindAll (void)
	{
		mHandlers.clear ();
	}
	
	int keyBinder::keycodeForString (const char *str) const
	{
		const char *ptr = str;
		int i;
		int keycombo = 0;
		struct { const char* name; int value; } modifiers[] = {
			{"lalt",lalt},
			{"lctrl",lctrl},
			{"lshift",lshift},
			{"ralt",ralt},
			{"rctrl",rctrl},
			{"rshift",rshift},
			{"alt",alt},
			{"shift",shift},
			{"ctrl",ctrl},
			{NULL,0}
		};
		char symbols[] = "[]\\-=`;'/,.";
		// tab capslock backspace esc ins del home end pgup pgdown
		// arrow keys
		// all numpad keys
		// function keys (f1-f15)
		// mouse1-mouse5,
		// wheelup, wheeldown
		struct { const char* name; int value; } syskeys[] = {
			{"tab", Key_Tab},
			{"capslock", Key_CapsLock},
			{"backspace", Key_BackSpace},
			{"esc", Key_Escape},
			{"escape", Key_Escape},
			{"enter", Key_Enter},
			{"return", Key_Enter},
			{"ins", Key_Ins},
			{"del", Key_Del},
			{"home", Key_Home},
			{"end", Key_End},
			{"pgup", Key_PgUp},
			{"pgdn", Key_PgDn},
			{"left", Key_LeftArrow},
			{"right", Key_RightArrow},
			{"up", Key_UpArrow},
			{"down", Key_DownArrow},
			{"kphome", Key_Kp_Home},
			{"kpup", Key_Kp_UpArrow},
			{"kppgup", Key_Kp_PgUp},
			{"kpleft", Key_Kp_LeftArrow},
			{"kp5", Key_Kp_5},
			{"kpright", Key_Kp_RightArrow},
			{"kpend", Key_Kp_End},
			{"kpdown", Key_Kp_DownArrow},
			{"kpenter", Key_Kp_Enter},
			{"kpins", Key_Kp_Ins},
			{"kpdel", Key_Kp_Del},
			{"kpslash", Key_Kp_Slash},
			{"kpminus", Key_Kp_Minus},
			{"kpplus", Key_Kp_Plus},
			{"kpnumlock", Key_Kp_NumLock},
			{"kpstar", Key_Kp_Star},
			{"kpequals", Key_Kp_Equals},
			{"f1", Key_F1},
			{"f2", Key_F2},
			{"f3", Key_F3},
			{"f4", Key_F4},
			{"f5", Key_F5},
			{"f6", Key_F6},
			{"f7", Key_F7},
			{"f8", Key_F8},
			{"f9", Key_F9},
			{"f10", Key_F10},
			{"f11", Key_F11},
			{"f12", Key_F12},
			{"f13", Key_F13},
			{"f14", Key_F14},
			{"f15", Key_F15},
			{"mouse1", Key_Mouse1},
			{"mouse2", Key_Mouse2},
			{"mouse3", Key_Mouse3},
			{"mouse4", Key_Mouse4},
			{"mouse5", Key_Mouse5},
			{"wheelup", Key_MWheelUp},
			{"wheeldown", Key_MWheelDown},
			{NULL, 0}
		};
	
		for (;;)
		{
			if (*ptr == 0)
				break;
			else if (*ptr == '+' || *ptr == ' ')
			{
				ptr++;
				continue;
			}
	
			// check if we have a modifier key
			for (i = 0; modifiers[i].name; i++)
			{
				size_t l = strlen (modifiers[i].name);
				if (!strncmp (modifiers[i].name, ptr, l)
					&& (*(ptr+l) == 0 || *(ptr+l) == '+' || *(ptr+l) == ' '))
				{
					keycombo |= modifiers[i].value;
					ptr += l;
					break;
				}
			}
			if (modifiers[i].name)
				continue;
		
			// lowercase letters
			if ((*ptr >= 0x61 && *ptr <= 0x7a)
				&& (*(ptr+1)=='+' || *(ptr+1)==' ' || *(ptr+1)==0))
			{
				keycombo |= *ptr;
				ptr++;
				continue;
			}
	
			// digits
			if ((*ptr >= 0x31 && *ptr <= 0x38)
				&& (*(ptr+1)=='+' || *(ptr+1)==' ' || *(ptr+1)==0))
			{
				keycombo |= *ptr;
				ptr++;
				continue;
			}
	
			// []\-=`;'/,. symbols
			if (strchr (symbols, *ptr))
			{
				keycombo |= *ptr;
				ptr++;
				continue;
			}
	
			// tab capslock backspace esc ins del home end pgup pgdown
			// arrow keys
			// all numpad keys
			// function keys (f1-f15)
			// mouse1-mouse5,
			// wheelup, wheeldown
			// joy1-joy32,
			for (i = 0; syskeys[i].name; i++)
			{
				size_t l = strlen (syskeys[i].name);
				if (!strncmp (syskeys[i].name, ptr, l)
					&& (*(ptr+l) == 0 || *(ptr+l) == '+' || *(ptr+l) == ' '))
				{
					keycombo |= syskeys[i].value;
					ptr += l;
					break;
				}
			}
			if (syskeys[i].name)
				continue;
	
			// if we got there -- error
			return -1;
		}
		return keycombo;
	}
	
	bool keyBinder::keyDown (int keycombo)
	{
		handlersMap::iterator it;
		inputState *st = g_engine->getInputDevice ()->getState ();
	
		// we gonna check all possible mod-combos
		// we allow only one modifier + key at a time
	/*	if (st->isPressed (Key_Shift)
			|| st->isPressed (Key_LShift)
			|| st->isPressed (Key_RShift))
		{
			it = mHandlers.find (keycombo | shift);
		}
		else*/ if (st->isPressed (Key_LShift))
		{
			it = mHandlers.find (keycombo | lshift);
		}
		else if (st->isPressed (Key_RShift))
		{
			it = mHandlers.find (keycombo | rshift);
		}
		/*else if (st->isPressed (Key_Alt)
			|| st->isPressed (Key_LAlt)
			|| st->isPressed (Key_RAlt))
		{
			it = mHandlers.find (keycombo | alt);
		}*/
		else if (st->isPressed (Key_LAlt))
		{
			it = mHandlers.find (keycombo | lalt);
		}
		else if (st->isPressed (Key_RAlt))
		{
			it = mHandlers.find (keycombo | ralt);
		}
		/*else if (st->isPressed (Key_Ctrl)
			|| st->isPressed (Key_LCtrl)
			|| st->isPressed (Key_RCtrl))
		{
			it = mHandlers.find (keycombo | ctrl);
		}*/
		else if (st->isPressed (Key_LCtrl))
		{
			it = mHandlers.find (keycombo | lctrl);
		}
		else if (st->isPressed (Key_RCtrl))
		{
			it = mHandlers.find (keycombo | rctrl);
		}
		else
		{
			it = mHandlers.find (keycombo);
		}
	
		if (it != mHandlers.end ())
		{
			g_engine->getConsole ()->command ((*it).second.c_str ());
			return true;
		}
		return false;
	}
	
	bool keyBinder::keyUp (int keycombo)
	{
		handlersMap::iterator it;
		it = mHandlers.find (keycombo);
		if (it != mHandlers.end ())
		{
			if ((*it).second[0] == '+')
			{
				const char *p = (*it).second.c_str ();
				g_engine->getConsole ()->command (cStr("-") + p + 1);
				return true;
			}
		}
		return false;
	}
	
}
