// $Id$

// linux implementation of filesystem

#include "pch.h"
#include <algorithm>
#include "f_filesystem.h"
#include "f_baseobject.h"
#include "f_error.h"
#include <zlib.h>
#include "f_helpers.h"

#include <sys/types.h>
#ifdef __linux__
#include <dirent.h>
#include <fnmatch.h>
#endif

#include "f_engine.h"

namespace fe
{

	const int FS_MAX_FILE_NAME = 32;	// includes terminating zero
	typedef struct fsStream_s {
		char					name[FS_MAX_FILE_NAME];
		uint32					size;		// uncompressed blocksize, 0 for directories
		uint32					comprsize;	// compressed block size; 0 for uncompressed
		uint64					offset;		// offset from file start. used only for pak files
	} fsStream_t;
	
	struct fsFileEntry
	{
		fsStream_t stream;

		// non-shared data
		char pak;	// index of pak file or -1
		char basepath;
		fsFileEntry *children;
		fsFileEntry *next;
	
		// some operations
		fsFileEntry (void)
		{
			memset (stream.name, 0, FS_MAX_FILE_NAME);
			stream.size = 0;
			stream.comprsize = 0;
			stream.offset = 0;
			pak = -1;
			basepath = -1;
			children = NULL;
			next = NULL;
		}
	
		~fsFileEntry (void)
		{
			fsFileEntry *c = children;
			while (c) {
				children = c->next;
				delete c;
				c = children;
			}
		}
	
		fsFileEntry*		find (const char *fname, cStr &fullName)
		{
			size_t sz = strlen (stream.name);
			// compare beginning
			if (strncmp (stream.name, fname, sz))
			{
				return NULL;
			}
			
			const char *ptr = fname + sz;
			
			if (sz)
			{
				if (*ptr == 0)
				{
					fullName += stream.name;
					return this;
				}
				
				if (*ptr != '/')
					return NULL;
				fullName += stream.name;
				fullName += "/";
				
				ptr++;
			}
			
			fsFileEntry *c = children;
			while (c) {
				fsFileEntry *f = c->find (ptr, fullName);
				if (f)
					return f;
				c = c->next;
			}
			
			return NULL;
		}
	};
	
	//-----------------------------------
	// fileSystem implementation
	//-----------------------------------
	
	fileSystem::fileSystem (void)
	{
		mpRootFolder = NULL;
		mNumBasePathes = 0;
		mNumPaks = 0;
		cleanUp ();
	}
	
	fileSystem::~fileSystem ()
	{
		fprintf (stderr, "filesystem dtor...\n");
		cleanUp ();
		fprintf (stderr, "filesystem clean!\n");
	}
	
	void	fileSystem::cleanUp (void)
	{
		mNumBasePathes = 0;
		mNumPaks = 0;
		if (mpRootFolder)
		{
			delete mpRootFolder;
			mpRootFolder = NULL;
		}
	}
	
	void	fileSystem::reset (const char *pathlist)
	{
		cleanUp ();
		init (pathlist);
	}
	
	void	fileSystem::init (const char *pathlist)
	{
		// pathlist is list of space-separated pathnames which should be used to init vfs
		fprintf (stderr, "mounting vfs directories...\n");
		fprintf (stderr, "pathlist is '%s'\n", pathlist);
		if (!mpRootFolder)
		{
			mpRootFolder = new fsFileEntry;
		}
		char path[1000];
		const char *curr = pathlist;
		const char *endl = curr;
		while (*curr)
		{
			while (*endl && *endl != ':')
				endl++;
			strncpy (path, curr, endl-curr);
			path[endl-curr] = 0;
			strcpy (mBasePathes[mNumBasePathes++], path);
			fprintf (stderr, "parsing paks in %s...\n", path);
			initPaks (mpRootFolder, path, mNumBasePathes-1);
			fprintf (stderr, "parsing regular files in %s...\n", path);
			initNormal (mpRootFolder, path, mNumBasePathes - 1);	// has higher priority than paks
			if (*endl == 0)
				break;
			endl++;
			curr = endl;
		}
	}
	
	void	fileSystem::initNormal (fsFileEntry *f, const char *path, int basepath)
	{
		cStr dirname = cStr (path);
	
	#ifdef __linux__
		struct dirent **namelist = NULL;
		int n;
	
		n = scandir (dirname, &namelist, 0, alphasort);
		if (n < 0)
		{
			if (namelist)
				free (namelist);
			return;	// not a dir or no read access
		}
		else
		{
			while (n--)
			{
				// no hidden files
				if (namelist[n]->d_name[0] != '.')
				{
					if (fnmatch (namelist[n]->d_name, "*.pak", 0))
					{
						fsFileEntry *nf = new fsFileEntry ();
						if (strlen (namelist[n]->d_name) > FS_MAX_FILE_NAME - 1)
						{
							fprintf (stderr, "WARNING: [fileSystem::initOS] filename is too big (>%d characters): '%s'\n", FS_MAX_FILE_NAME-1, namelist[n]->d_name);
							continue;
						}
						cStr fn = namelist[n]->d_name;
						strcpy (nf->stream.name, fn.c_str ());
	
						cStr fullpath = (cStr (path)+"/"+fn).c_str ();
						
						FILE *fp = fopen (fullpath.c_str (), "rb");
						if (fp) {
							// get size
							size_t filesize;
							fseek (fp, 0, SEEK_END);
							filesize = ftell (fp);
							fclose (fp);
							
							// FIXME: how do i know it's a directory?!
							nf->basepath = basepath;
							nf->stream.size = filesize;
							nf->stream.offset = 0;
		
							// find that file in the children of 'parent'
							size_t i;
							fsFileEntry *curr = NULL;
							
							fsFileEntry *c = f->children;
							while (c) {
								if (!strcmp (c->stream.name, nf->stream.name))
								{
									// directorys are the same! do not add
									curr = c;
									break;
								}
								c = c->next;
							}
		
							if (!curr)
							{
								curr = nf;
								curr->pak = -1;
								curr->next = f->children;
								f->children = curr;
							}
							else
							{
								// replace
								curr->basepath = basepath;
								curr->stream.size = nf->stream.size;
								curr->stream.comprsize = nf->stream.comprsize;
								curr->stream.offset = nf->stream.offset;
								curr->pak = -1;
								delete nf;
							}
						}
					}
				}
				
				free (namelist[n]);
			}
			free (namelist);
		}
	
		fsFileEntry *c = f->children;
		while (c) {
			cStr dir = cStr (path) + "/" + c->stream.name;
			initNormal (c, dir, basepath);
			c = c->next;
		}
	#endif
		
	#ifdef _WIN32
		// here goes tested and working win32 implementation (just for your reference)
		WIN32_FIND_DATAA fd;
		HANDLE hSearch = FindFirstFileA (cStr (path) + "/*.*", &fd);
		if (hSearch != INVALID_HANDLE_VALUE)
		{
			do
			{
				if (strcmp (fd.cFileName, "..") && strcmp (fd.cFileName, "."))
				{
					// skip paks
					char ext[_MAX_EXT];
					_splitpath (fd.cFileName, NULL, NULL, NULL, ext);
					if (strcmp (ext, ".pak"))
					{
						fsFileEntry *nf = new fsFileEntry (this);
						cStr fn = fd.cFileName;
	//					fn.tolower ();
						strcpy (nf->name, fn.c_str ());
						nf->blocktype = (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ? fs_directory : fs_BinaryRawData;
						nf->fstype = fsFileEntry::os;
						nf->basepath = basepath;
						nf->size = fd.nFileSizeLow;
						nf->comprsize = nf->size;
						nf->offset = 0;
	
						// find that file in the children of 'parent'
						size_t i;
						fsFileEntry *curr = NULL;
	
						for (i = 0; i < f->children.size (); i++)
						{
							if (!strcmp (f->children[i]->name, nf->name))
							{
								// directorys are the same! do not add
								curr = f->children[i];
								break;
							}
						}
	
						if (!curr)
						{
							curr = nf;
							curr->fstype = fsFileEntry::os;
							curr->pakfile = -1;
							f->children.push_back (curr);
						}
						else
						{
							// replace
							curr->basepath = basepath;
							curr->blocktype = nf->blocktype;
							curr->size = nf->size;
							curr->comprsize = nf->comprsize;
							curr->offset = nf->offset;
							curr->pakfile = -1;
							curr->fstype = fsFileEntry::os;
							delete nf;
						}
	//					f->children.push_back (nf);
					}
				}
			} while (FindNextFileA (hSearch, &fd));
			FindClose (hSearch);
		}
		
		for (size_t i = 0; i < f->children.size (); i++)
			initNormal (f->children[i], cStr (path) + "/" + f->children[i]->name, basepath);
	#endif
	}
	
	void	fileSystem::initPaks (fsFileEntry *f, const char *path, int basepath)
	{
	#ifdef __linux__
		struct dirent **namelist = NULL;
		int n;
		n = scandir (cStr (path)/* + "/."*/, &namelist, 0, alphasort);
		if (n < 0)
		{
			if (namelist)
				free (namelist);
			return;
//			throw genericError ("[fileSystem::initFE] scandir failed (%s), retval = %d", path, n);
		}
		else
		{
			while (n--)
			{
				if (!fnmatch (namelist[n]->d_name, "*.pak", 0))
				{
					// .pak extension
					
					// try fopen
					FILE *fp = fopen (namelist[n]->d_name, "rb");
					if (!fp)
					{
						// doesn't open, can be a dir, or something wrong, or whatever..
					}
					else
					{
						fclose (fp);
						strcpy (mPakFiles[mNumPaks], path);
						strcat (mPakFiles[mNumPaks], "/");
						strcat (mPakFiles[mNumPaks], namelist[n]->d_name);
						mNumPaks++;
					}
				}
				free (namelist[n]);
			}
			free (namelist);
		}
		
		// sort alphabetically
		if (mNumPaks)
			qsort (mPakFiles, FS_MAX_PAKNAME, mNumPaks, alphasort);
	
		// load
		for (int i = 0; i < mNumPaks; i++)
			loadPakFile (i, basepath);
	
	#endif
	
	#ifdef _WIN32
		WIN32_FIND_DATAA fd;
		HANDLE hSearch = FindFirstFileA (cStr (path) + "/*.pak", &fd);
		if (hSearch != INVALID_HANDLE_VALUE)
		{
			do
			{
				if (0 == (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					mPakFiles.push_back (cStr (path) + "/" + fd.cFileName);
				}
			} while (FindNextFileA (hSearch, &fd));
			FindClose (hSearch);
		}
	
		// sort alphabetically
		std::sort (mPakFiles.begin (), mPakFiles.end ());
	
		// load
		for (int i = 0; i < mNumPaks; i++)
			loadPakFile (i);
	#endif
	}
	
	void	fileSystem::loadPakFile (int pakfile, int basepath)
	{
		FILE *fp = fopen (mPakFiles[pakfile], "rb");
	
		char magic[8];
		magic[7] = 0;
		fread (magic, 5, 1, fp);
		if (strcmp (magic, "FEPAKV2"))
		{
			fclose (fp);
			sys_error ("invalid pak file '%s'", mPakFiles[pakfile]);
		}
	
		// read table offset
		ulong table;
		fread (&table, sizeof (table), 1, fp);
	
		// seek there
		fseek (fp, table, SEEK_SET);
	
		// load
		loadPakFileStructure (pakfile, fp, NULL, basepath);
	
		fclose (fp);
	}
	
	void	fileSystem::loadPakFileStructure (int pakfile, FILE *fp, fsFileEntry *parent, int basepath)
	{
		fsFileEntry *curr = NULL;
		fsFileEntry *currFile = new fsFileEntry ();
		bool	del = false;
		fread (currFile, sizeof (fsStream_t), 1, fp);
		if (!parent)
		{
			// reading root directory
			curr = mpRootFolder;
			del = true;
		}
		else
		{
			// find that file in the children of 'parent'
			size_t i;
	
			fsFileEntry *c = parent->children;
			while (c) {
				if (!strcmp (c->stream.name, currFile->stream.name))
				{
					// directorys are the same! do not add
					curr = c;
					break;
				}
				c = c->next;
			}
	
			if (!curr)
			{
				curr = currFile;
				curr->basepath = basepath;
				curr->pak = pakfile;
				curr->next = parent->children;
				parent->children = curr;
			}
			else
			{
				// replace
				curr->basepath = basepath;
				curr->stream.size = currFile->stream.size;
				curr->stream.comprsize = currFile->stream.comprsize;
				curr->stream.offset = currFile->stream.offset;
				curr->pak = pakfile;
				del = true;
			}
		}
	
		if (!currFile->stream.size) // is it a directory?
		{
			// load children
			ulong nc;
			fread (&nc, sizeof (nc), 1, fp);
			for (ulong i = 0; i < nc; i++)
			{
				loadPakFileStructure (pakfile, fp, curr, basepath);
			}
		}
	
		if (del)
			delete currFile;
	}
	
	file*	fileSystem::openFile (const char *fname, fileMode_t openMode)
	{
		assert (mpRootFolder);
		cStr lcname = fname;
	//	lcname.tolower ();
		cStr n;
		fsFileEntry *entry = mpRootFolder->find (lcname, n);
		if (!entry)
			return NULL;
	
		file *f = new file (entry, n);
		f->open (openMode);
	
		return f;
	}
	
#if 0
	int		fileSystem::getFileList (const char *path, const char *extension, std::vector< cStr > &names)
	{
		cStr n;
		fsFileEntry *entry = mpRootFolder->find (path, n);
		if (!entry)
			return 0;
		for (size_t i = 0; i < entry->children.size (); i++)
		{
			const char *fname = entry->children[i]->name;
	#ifdef __linux__
			if (!fnmatch (entry->name, cStr ("*.") + extension, 0))
	#endif
	#ifdef _WIN32
			char *p = strstr (entry->name, cStr (".") + extension);
			if (p && * (p+strlen (extension)+1) == 0)
	#endif
			{
	#ifdef __linux__
				char ret[NAME_MAX];
	#else
				// FIXME: do it right way
				char ret[200];
	#endif
				sprintf (ret, "%s/%s", n.c_str (), fname);
				names.push_back (ret);
			}
		}
		return 0;
	}
#endif
	#if 0 // removed
	int		fileSystem::deleteFile (const str &fname)
	{
		return 0;
	}
	
	int		fileSystem::createPath (const str &_path)
	{
		TCHAR *ptr = (TCHAR *)_path.c_str ();
		TCHAR *path = ptr;
	
		for (;;)
		{
			while (*ptr && *ptr != _T ('/')) ptr++;
			if (*ptr != _T ('/')) break;
	
			// create if not exists
			*ptr = _T ('\0');
			_tmkdir (path);
			*ptr = _T ('/');
			ptr++;
		}
		return 0;
	}
	#endif
	
	const char* fileSystem::getPakFile (int i) const
	{
		return mPakFiles[i];
	}

	const char* fileSystem::getBasePath (int idx) const
	{
		return mBasePathes[idx];
	}
	
	//-----------------------------------
	// file implementation
	//-----------------------------------
	
	file::file (fsFileEntry *entry, const char *name)
	{
		mName = name;
		mpEntry = entry;
		mpFile = NULL;
		mpZStream = NULL;
		mOffset = 0;
		mpZBuffer = NULL;
	}
	
	file::~file ()
	{
	}
	
	void file::open (fileMode_t openMode)
	{
		assert (NULL == mpFile);
		smartPtr <fileSystem> fs = g_engine->getFileSystem ();
		if (mpEntry->pak == -1) // not in pak?
		{
			cStr fullName = fs->getBasePath (mpEntry->basepath);
			fullName += "/";
			fullName += mName;
			mpFile = fopen (fullName, "rb");
			fseek (mpFile, 0, SEEK_END);
			mpEntry->stream.size = ftell (mpFile);
			mpEntry->stream.comprsize = mpEntry->stream.size;
			rewind (mpFile);
		}
		else
		{
			mpFile = fopen (fs->getPakFile (mpEntry->pak), "rb");
			fseek (mpFile, mpEntry->stream.offset, SEEK_SET);
			mOffset = 0;
		}
	}
	
	int		file::readUINT16 (uint16 *buffer, int num)
	{
		int r = read (buffer, num * sizeof (uint16));
#if WORDS_BIGENDIAN
		{
			for (int i = 0; i < num; i++)
			{
				uchar *p = (uchar *)(buffer + i);
				uchar tmp;
				tmp = p[0];
				p[0] = p[1];
				p[1] = tmp;
			}			
		}
#endif
		return r;
	}
	
	int		file::readUINT32 (uint32 *buffer, int num)
	{
		int r = read (buffer, num * sizeof (uint32));
#if (WORDS_BIGENDIAN)
		{
			for (int i = 0; i < num; i++)
			{
				uchar *p = (uchar *)(buffer + i);
				uchar tmp;
				tmp = p[0];
				p[0] = p[3];
				p[3] = tmp;
				tmp = p[1];
				p[1] = p[2];
				p[2] = tmp;
			}			
		}
#endif
		return r;
	}

	int		file::readFloat (float *buffer, int num)
	{
		int r = read (buffer, num * sizeof (float));
#if (WORDS_BIGENDIAN)
		{
			for (int i = 0; i < num; i++)
			{
				uchar *p = (uchar *)(buffer + i);
				uchar tmp;
				tmp = p[0];
				p[0] = p[3];
				p[3] = tmp;
				tmp = p[1];
				p[1] = p[2];
				p[2] = tmp;
			}			
		}
#endif
		return r;
	}
	
	int		file::readMatrix4 (matrix4 *buffer, int num)
	{
		int r = read (buffer, num * sizeof (float)*16);
#if (WORDS_BIGENDIAN)
		{
			for (int i = 0; i < num*16; i++)
			{
				uchar *p = (uchar *)(((float*)buffer) + i);
				uchar tmp;
				tmp = p[0];
				p[0] = p[3];
				p[3] = tmp;
				tmp = p[1];
				p[1] = p[2];
				p[2] = tmp;
			}			
		}
#endif
		return r;
		
	}

	int		file::read (void *buffer, ulong size)
	{
		assert (mpFile);
		assert (mpEntry);
		if (mpEntry->pak == -1) // not in pak?
		{
			return fread (buffer, 1, size, mpFile);
		}
		else
		{
			if (mpEntry->stream.comprsize) // deflated
			{
				// optimized entire-buffer uncompress
				if (size == mpEntry->stream.size)
				{
					mOffset += size;
					if (mOffset > mpEntry->stream.size)
						sys_error ("reading outside of file bounds '%s'", mName.c_str ());
					ulong dstsz = (ulong)size;
					uchar *srcbuf = new uchar[mpEntry->stream.comprsize];
					fread (srcbuf, 1, mpEntry->stream.comprsize, mpFile);
					uncompress ((uchar*)buffer, &dstsz, srcbuf, mpEntry->stream.comprsize);
					delete[] srcbuf;
					return mpEntry->stream.size;
				}
				else
				{
					// partial uncompress using inflate directly
					if (mOffset != 0)
						assert (mpZStream);
	
	//				mOffset += size;
	//				if (mOffset > mpEntry->size)
	//					throw genericError (_T ("reading outside of file bounds '%s'"), mName.c_str ());
	
					// allocate zstream if needed
	
	//#define zbuffer_sz		 (10 * 1024)
	#define zbuffer_sz		 (20)
	
					int err;
					ulong read_sz = min ((int)mpEntry->stream.comprsize, zbuffer_sz);
					if (!mpZStream)
					{
						mpZStream = new z_stream_s;
						mpZBuffer = new uchar[zbuffer_sz];
						memset (mpZStream, 0, sizeof (z_stream_s));
						mpZStream->next_in = (Bytef*)mpZBuffer;
						mpZStream->avail_in = read_sz;
						mpZStream->zalloc = (alloc_func)0;
						mpZStream->zfree = (free_func)0;
	
						// read some
						fread (mpZBuffer, read_sz, 1, mpFile);
	
						err = inflateInit (mpZStream);
						if (err != Z_OK)
							sys_error ("inflateInit error reading file '%s'", mName.c_str ());
					}
					mpZStream->next_out = (Bytef*)buffer;
					mpZStream->avail_out = size;
	
					ulong decompr_bytes = 0;
	
					// read some bytes
					for (;;)
					{
						err = inflate (mpZStream, Z_SYNC_FLUSH);
						decompr_bytes = (uchar*)mpZStream->next_out - (uchar*)buffer;
						if (err == Z_STREAM_END)
						{
							inflateEnd (mpZStream);
							break;
						}
						else if (err == Z_OK && decompr_bytes == size)
						{
							mOffset += size;
							break;
						}
						else
						{
							// move remaining input if possible
							ulong read_amnt = (uchar*)mpZStream->next_in - (uchar*)mpZBuffer;
							int rd_offs = 0;
							if (read_amnt != read_sz)	// i.e. > 0
							{
								rd_offs = read_sz - read_amnt;
								memmove (mpZBuffer, mpZStream->next_in, rd_offs);
							}
							// add more input
							int add_amnt = min ((ulong)zbuffer_sz - (ulong)rd_offs, mpEntry->stream.comprsize - mOffset);
							fread (mpZBuffer + rd_offs, add_amnt, 1, mpFile);
							mpZStream->next_in = (Bytef*)mpZBuffer;
							mpZStream->avail_in = rd_offs + add_amnt;
						}
	//					else
	//						throw genericError ("inflate error reading file '%s'; zerr = %d", mName.c_str (), err);
					}
					return decompr_bytes;
				}
			}
			else
			{
				int res = fread (buffer, 1, size, mpFile);
				mOffset += res;
				return res;
			}
		}
	}
	
	#if 0 // removed
	int		file::write (void *buffer, int size)
	{
		assert (false);
		return -1;
	}
	
	void	file::flush (void)
	{
		assert (false);
	}
	#endif
	
	int		file::getSize (void) const
	{
		assert (mpEntry);
		return mpEntry->stream.size;
	}
	
	void	file::close (void)
	{
		assert (mpFile);
		fclose (mpFile);
		mpFile = NULL;
	
		if (mpZStream)
		{
			inflateEnd (mpZStream);
			delete mpZStream;
			mpZStream = NULL;
		}
	
		if (mpZBuffer)
		{
			delete[] mpZBuffer;
			mpZBuffer = NULL;
		}
	
		delete this;
	}
	
	const char *	file::name (void) const
	{
		assert (mpEntry);
		return mpEntry->stream.name;
	}
	
	long	file::tell (void) const
	{
		if (mpEntry->pak == -1) // not in pak?
			return ftell (mpFile);
		else
			return mOffset;
	}
	
	int		file::seek (int offset, int whence)
	{
		if (mpEntry->pak == -1) // not in pak?
			return fseek (mpFile, offset, whence);
		else if (mpEntry->stream.comprsize) // compressed?
		{
			if (whence == SEEK_CUR)
				offset += mOffset;
			else if (whence == SEEK_END)
				offset = mpEntry->stream.size - offset - 1;
	
			// FIXME: pak file binary uncompressed data can be handled in a better way
	
			if (offset == 0)	// rewind special case
			{
				mOffset = 0;
				if (mpZStream)
				{
					inflateEnd (mpZStream);
					delete mpZStream;
					mpZStream = NULL;
				}
	
				if (mpZBuffer)
				{
					delete[] mpZBuffer;
					mpZBuffer = NULL;
				}
				fseek (mpFile, mpEntry->stream.offset, SEEK_SET);
				mOffset = 0;
			}
			else if (offset < (long)mOffset)
			{
				mOffset = 0;
				if (mpZStream)
				{
					inflateEnd (mpZStream);
					delete mpZStream;
					mpZStream = NULL;
				}
	
				if (mpZBuffer)
				{
					delete[] mpZBuffer;
					mpZBuffer = NULL;
				}
				heapBuffer<uchar> buf (offset);
				read (buf, offset);
			}
			else if ( (long)mOffset != offset)
			{
				heapBuffer<uchar> buf (offset - mOffset);
				read (buf, offset - mOffset);
			}
			return 0;
		}
		else if (!mpEntry->stream.comprsize)
		{
			if (whence == SEEK_CUR)
				offset += mOffset;
			else if (whence == SEEK_END)
				offset = mpEntry->stream.size - offset - 1;
	
			// FIXME: pak file binary uncompressed data can be handled in a better way
	
			if (offset == 0)	// rewind special case
			{
				mOffset = 0;
				return fseek (mpFile, mpEntry->stream.offset, SEEK_SET);
			}
			else
			{
				mOffset = offset;
				return fseek (mpFile, mpEntry->stream.offset + offset, SEEK_SET);
			}
		}
		return 0;
	}
	
}
