#ifndef __PCH_H
#define __PCH_H

#ifdef _WIN32
#define _WIN32_WINNT 0x0400
#endif

#if defined _DEBUG && defined _WIN32
#define CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#else
#include <stdlib.h>
#endif

//-----------------------------------
// misc msvc warning disables
//-----------------------------------

#ifdef _MSC_VER
#pragma warning (disable:4244)
#pragma warning (disable:4786)
#endif

//-----------------------------------
// win32 includes
//-----------------------------------

#ifdef _WIN32
#include <windows.h>
#endif

#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <ctype.h>

#endif // __PCH_H

