#ifndef __VECMATH_H
#define __VECMATH_H

#ifdef _MSC_VER
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#include <assert.h>
#include <string.h>
#include "types.h"

#define SMALL_NUM 1.0e-5f

typedef float float2[2];
typedef float float3[3];
typedef float float4[4];
typedef float4 plane_t;
typedef float4 float4x4[4];

typedef struct {
	float3 center;
	float3 axis[3];
	float extent[3];
} box3_t;

typedef struct {
	float3 origin;
	float3 direction;
} ray3_t;

// FIXME add float4 maths (including quats)

#define vec3_init(a, x, y, z) { (a)[0] = (x); (a)[1] = (y); (a)[2] = (z); }
#define vec3_zero(a) { (a)[0] = (a)[1] = (a)[2] = 0; }
#define vec3_unitx(a) { (a)[0] = 1; (a)[1] = 0; (a)[2] = 0; }
#define vec3_unity(a) { (a)[0] = 0; (a)[1] = 1; (a)[2] = 0; }
#define vec3_unitz(a) { (a)[0] = 0; (a)[1] = 0; (a)[2] = 1; }
#define vec3_copy(a, b) { ((a))[0] = ((b))[0]; (a)[1] = (b)[1]; (a)[2] = (b)[2]; }
#define vec3_dot(a, b) ((a)[0]*(b)[0] + (a)[1]*(b)[1] + (a)[2]*(b)[2])
#define vec3_mag(a) ((float)(sqrt(vec3_dot((a), (a)))))
#define vec3_sqrmag(a) vec3_dot((a), (a))
#define vec3_add(a, b, c) { (a)[0] = (b)[0] + (c)[0];  (a)[1] = (b)[1] + (c)[1]; (a)[2] = (b)[2] + (c)[2]; }
#define vec3_sub(a, b, c) { (a)[0] = (b)[0] - (c)[0];  (a)[1] = (b)[1] - (c)[1]; (a)[2] = (b)[2] - (c)[2]; }
#define vec3_scale(a, b, c) { (a)[0] = (b)[0]*(c);  (a)[1] = (b)[1]*(c); (a)[2] = (b)[2]*(c); }
#define vec3_invscale(a, b, c) { (a)[0] = (b)[0]/(c);  (a)[1] = (b)[1]/(c); (a)[2] = (b)[2]/(c); }
#define vec3_mul(a, b, c) { (a)[0] = (b)[0]*(c)[0];  (a)[1] = (b)[1]*(c)[1]; (a)[2] = (b)[2]*(c)[2]; }
#define vec3_cross(a, b, c) { (a)[0] = (b)[1]*(c)[2] - (b)[2]*(c)[1];  (a)[1] = (b)[2]*(c)[0] - (b)[0]*(c)[2]; (a)[2] = (b)[0]*(c)[1] - (b)[1]*(c)[0]; }
#define vec3_mad(a, b, c, d)  { (a)[0] = (b)[0] + (c)[0]*(d); (a)[1] = (b)[1] + (c)[1]*(d); (a)[2] = (b)[2] + (c)[2]*(d); }
#define vec3_negate(a, b) { (a)[0] = -(b)[0]; (a)[1] = -(b)[1]; (a)[2] = -(b)[2]; }
//#define vec3_norm(a, b) { float d = vec3_mag (b); if (d == 0.f) { vec3_zero (a); } else {d = 1.f/d; vec3_scale ((a), (b), (d));} }
#define vec3_equal(a, b) (fabs((a)[0]-(b)[0])<SMALL_NUM && fabs((a)[1]-(b)[1])<SMALL_NUM && fabs((a)[2]-(b)[2])<SMALL_NUM)

#define vec2_init(a, x, y) { (a)[0] = (x); (a)[1] = (y); }
#define vec2_zero(a) { (a)[0] = (a)[1] = 0; }
#define vec2_unitx(a) { (a)[0] = 1; (a)[1] = 0; }
#define vec2_unity(a) { (a)[0] = 0; (a)[1] = 1; }
#define vec2_copy(a, b) { ((a))[0] = ((b))[0]; (a)[1] = (b)[1]; }
#define vec2_dot(a, b) ((a)[0]*(b)[0] + (a)[1]*(b)[1])
#define vec2_mag(a) ((float)(sqrt(vec2_dot((a), (a)))))
#define vec2_add(a, b, c) { (a)[0] = (b)[0] + (c)[0];  (a)[1] = (b)[1] + (c)[1]; }
#define vec2_sub(a, b, c) { (a)[0] = (b)[0] - (c)[0];  (a)[1] = (b)[1] - (c)[1]; }
#define vec2_scale(a, b, c) { (a)[0] = (b)[0]*(c);  (a)[1] = (b)[1]*(c); }
#define vec2_mul(a, b, c) { (a)[0] = (b)[0]*(c)[0];  (a)[1] = (b)[1]*(c)[1]; }
#define vec2_cross(a, b, c) { (a)[0] = (b)[1];  (a)[1] = -(c)[2]; }
#define vec2_mad(a, b, c, d)  { (a)[0] = (b)[0] + (c)[0]*(d); (a)[1] = (b)[1] + (c)[1]*(d); }
#define vec2_negate(a, b) { (a)[0] = -(b)[0]; (a)[1] = -(b)[1]; }
#define vec2_norm(a, b) { float d = vec2_mag (b); if (d == 0.f) { vec2_zero (a); } else {d = 1.f/d; vec2_scale ((a), (b), (d));} }
#define vec2_perp(a, b) ((a)[0]+(b)[1]-(a)[1]*(b)[0])
#define vec2_equal(a, b) (fabs((a)[0]-(b)[0])<SMALL_NUM && fabs((a)[1]-(b)[1])<SMALL_NUM)

/**
 * rotates a vector around an arbitrary axis
 * @param out output value
 * @param in input value
 * @param axis an axis
 * @param angle an angle
 */
void
vec3_rotate_around_axis (float3 out, float3 in, float3 axis, float angle);
	
float
vec3_norm (float3 out, float3 in);

void
vec3_make_normals (float3 front, float3 right, float3 up);

void
m4x4_init (float4x4 m);

void
m4x4_copy (float4x4 out, float4x4 in);

void
m4x4_translation (float4x4 m, float x, float y, float z);

void
m4x4_rotation_x (float4x4 m, float angle);

void
m4x4_rotation_y (float4x4 m, float angle);

void
m4x4_rotation_z (float4x4 m, float angle);

void
m4x4_rotate_around_axis (float4x4 m, float3 axis, float angle);

void
m4x4_from_euler_angles_xyz (float4x4 m, float ax, float ay, float az);

void
m4x4_vec3_mul (float3 r, float4x4 m, float3 v);

void
m4x4_vec4_mul (float3 r, float4x4 m, float4 v);

void
m4x4_mul (float4x4 r, float4x4 m, float4x4 b);

void
m4x4_vec3_transform (float3 r, float4x4 m, float3 v);

int
m4x4_projection (float4x4 r, float fov, float aspect, float np, float fp);

int
m4x4_lookat (float4x4 r, float3 from, float3 at, float3 up);

void
m4x4_ortho_offcenter (float4x4 res, float l, float r, float b, float t, float zn, float zf);

void
m4x4_ortho (float4x4 r, float w, float h, float zn, float zf);

void
m4x4_inverse (float4x4 r, float4x4 m);

void
plane_from_points (plane_t r, float3 a, float3 b, float3 c);

float
plane_distance_to_point (plane_t p, float3 a);

void
plane_project_point (float3 r, plane_t p, float3 a);

void
box3_zero (box3_t *b);

void
box3_compute_vertices (box3_t *b, float3 vx[8]);

void
calcTriangleBasis (float3 E, float3 F, float3 G,
		float sE, float tE, float sF, float tF, float sG, float tG,
		float3 tangentX, float3 tangentY);

void
orthogonalize (float3 res, float3 v1, float3 v2);

void closestPointOnLine (float3 res, float3 a, float3 b, float3 p);
	
#endif // __VECMATH_H

