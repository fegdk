/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_EXPMODEL_H
#define __F_EXPMODEL_H

#include "f_string.h"
#include "f_parser.h"
#include "f_math.h"
#include "f_sceneobject.h"
#include "f_animation.h"
#include "f_model.h"
#include "f_helpers.h"

namespace fe
{

	enum objClassID
	{
		UNKNOWN_OBJECT_CLASS_ID,
		TRI_OBJECT_CLASS_ID,
		ENTITY_OBJECT_CLASS_ID,
		REF_OBJECT_CLASS_ID,
		DUMMY_OBJECT_CLASS_ID
	};
	
	struct expFace
	{
		int v[3];
	};
	
	class expScene;
	
	class expObject : public baseObject
	{
	
	public:
		typedef std::vector < smartPtr <expObject> >	childrenList;
	
	protected:
	
		float						mFrameRate;
		expObject*	mpParent;
		matrix4		mMatrix;
		matrix4		mWorldMatrix;
	
		childrenList mChildren;
	
		objClassID	mClassID;
	
		bool baseLoad (charParser &p, const char *fname);
		void recalcWorldTransforms (void);
	
		virtual ~expObject (void)
		{
			mChildren.clear ();
		}
	
	public:
	
		// constructor & destructor
		expObject (expObject *parent);
	
		objClassID getClassID (void) const { return mClassID; }
	
		const matrix4 &getMatrix (void) const
		{
			return mMatrix;
		}
	
		const matrix4 &getWorldMatrix (void) const
		{
			return mWorldMatrix;
		}
	
		void setMatrix (const matrix4 &m);
		void setWorldMatrix (const matrix4 &m);
		virtual void load (charParser &p, const char *fname) = 0;
		virtual void render (const smartPtr <expScene> &scene) const;
		const childrenList&	children (void) const { return mChildren; }
		void addChild (const smartPtr <expObject> &child);
		void removeChild (const smartPtr <expObject> &child);
		void setParent (expObject *parent);
		smartPtr <expObject> getParent (void) { return mpParent; }
	
		float	frameRate (void) const { return mFrameRate; }
	};
	typedef smartPtr <expObject> expObjectPtr;
	
	class expScene : public baseObject
	{
	
	public:
	
		typedef std::vector <materialPtr>	materialList;
	
	protected:
	
		expObjectPtr	mpSceneRoot;
		std::vector <cStr>	mMtlNameList;
		std::vector <cStr>	mBoneList;
		materialList mMtlList;
		
		void			load (charParser &p);
		expObjectPtr	loadObject (charParser &p);
	
		~expScene (void);
	
	public:
	
		expScene (void);
	
		int load (const char *fname);
	
		expObjectPtr	getRootNode (void) { return mpSceneRoot; }
	
		std::vector <cStr>& mtlNameList (void) { return mMtlNameList; }
		materialList& mtlList (void) { return mMtlList; }
	
		virtual void render (void);
	};
	
	typedef smartPtr <expScene> expScenePtr;
	
	class expTriObject : public expObject
	{
	
	protected:
	
		std::vector <vector3>	mVerts;
		std::vector <vector3>	mColors;
		std::vector <vector3>	mNormals;
		std::vector <vector2>	mTexCoords;
		std::vector <vector2>	mLightmapTexCoords;
		std::vector <expFace>	mFaces;
		std::vector <expFace>	mColorFaces;
		std::vector <expFace>	mNormalFaces;
		std::vector <expFace>	mTexCoordFaces;
		std::vector <expFace>	mLightmapTexCoordFaces;
		std::vector <int>	   	mMtlIndex;
		std::vector <vector3> mTangents;
		std::vector <vector3> mBinormals;
		std::vector <expFace> mTSFaces;
	
		struct boneInfl {
			int bone;
			float weight;
		};
		typedef std::vector <boneInfl>	vertexInfl;
		std::vector <vertexInfl>	mVertexInfluences;
	
		void						flush (expScene *scene, int mtl, int firstFace, int numFaces);
	
		~expTriObject (void) {}
	
	public:
		
		expTriObject (expObject *parent);
	
		virtual void load (charParser &p, const char *fname);
		void transformIntoWorldSpace (void);
	
		std::vector<vector3>&	verts () { return mVerts; }
		std::vector<vector3>&	colors () { return mColors; }
		std::vector<vector3>&	normals () { return mNormals; }
		std::vector<vector2>&	texCoords () { return mTexCoords; }
		std::vector<vector2>&	lightmapTexCoords () { return mLightmapTexCoords; }
		std::vector<expFace>&	faces () { return mFaces; }
		std::vector<expFace>&	colorFaces () { return mColorFaces; }
		std::vector<expFace>&	normalFaces () { return mNormalFaces; }
		std::vector<expFace>&	texCoordFaces () { return mTexCoordFaces; }
		std::vector<expFace>&	lightmapTexCoordFaces () { return mLightmapTexCoordFaces; }
		std::vector<int>&	mtlIndex () { return mMtlIndex; }
		std::vector<vector3>&	tangents () { return mTangents; }
		std::vector<vector3>&	binormals () { return mBinormals; }
		std::vector<expFace>&	tsFaces () { return mTSFaces; }
	
		const std::vector<vector3>&	verts () const { return mVerts; }
		const std::vector<vector3>&	colors () const { return mColors; }
		const std::vector<vector3>&	normals () const { return mNormals; }
		const std::vector<vector2>&	texCoords () const { return mTexCoords; }
		const std::vector<vector2>&	lightmapTexCoords () const { return mLightmapTexCoords; }
		const std::vector<expFace>&	faces () const { return mFaces; }
		const std::vector<expFace>&	colorFaces () const { return mColorFaces; }
		const std::vector<expFace>&	normalFaces () const { return mNormalFaces; }
		const std::vector<expFace>&	texCoordFaces () const { return mTexCoordFaces; }
		const std::vector<expFace>&	lightmapTexCoordFaces () const { return mLightmapTexCoordFaces; }
		const std::vector<int>&	mtlIndex () const { return mMtlIndex; }
		const std::vector<vector3>&	tangents () const { return mTangents; }
		const std::vector<vector3>&	binormals () const { return mBinormals; }
		const std::vector<expFace>&	tsFaces () const { return mTSFaces; }
		
		virtual void render (expScene *scene);
	};
	
	typedef smartPtr <expTriObject> expTriObjectPtr;
	
	class expModel : public sceneObject
	{
	
	protected:
	
		struct face
		{
			ushort v[3];
		};
	
		struct subset
		{
			uint32 firstVertex;
			uint32 firstFace;
			uint32 numVerts;
			uint32 numFaces;
			uint32 mtl;
		};
	
		bool mbLightmaps, mbTangents;

		std::vector< subset >	mSubSets;
		std::vector< drawVertex_t >	mVerts;
		std::vector< face >		mFaces;
		std::vector< int >		mMtlIndex;
		cStr					mLightmapName;
	
		~expModel (void);
		void	saveGeometry (FILE *fp) const;
	
	public:
	
		expModel (expModel *parent);
		
		void	convertFrom (const expScenePtr &s, const expObjectPtr &o);
		void	save (const char *fname, const expScenePtr &s) const;
	};
	
	class expEntityObject : public expObject
	{
	private:
	
	public:
		// data
		std::map<cStr, cStr>	mProps;
	
	public:
	
		virtual void load (charParser &p, const char *fname);
	
		// constructor & destructor
		expEntityObject (expObject *parent);
		virtual void render (expScene *scene);
	
	protected:
		virtual ~expEntityObject () {};
	};
	
	class expRefObject : public expObject
	{
	
		// data
		cStr		mRefObjFileName;	// imported .mdl filename
		expScene*	mpScene;			// imported scene ptr
	
	public:
	
		virtual void load (charParser &p, const char *fname);
	
		// constructor & destructor
		expRefObject (expObject *parent);
		virtual void render (expScene *scene);
	
	protected:
	
		virtual ~expRefObject () {};
	
	};
	
	class expDummyObject : public expObject
	{
	
	public:
	
		expDummyObject (expObject *parent);
		virtual void load (charParser &p, const char *fname);
		virtual void render (expScene *scene);
	};
	
}

#endif // __F_EXPMODEL_H

