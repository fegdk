#ifndef __TYPES_H
#define __TYPES_H

#include <stdint.h>

// win32 dllexport
#if defined(_WIN32)
#	if	defined(__MINGW32__)
#		define FE_API
#	else
#		pragma warning( disable : 4251 )
#		if defined (FEGDKDLL_EXPORTS)
#			define FE_API	__declspec(dllexport)
#		else
#			define FE_API	__declspec(dllexport)
#		endif
#	endif
#else
#	define FE_API
#endif

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))

//-----------------------------------
// misc typedefs
//-----------------------------------

typedef unsigned char		uchar;
typedef unsigned char		ubyte;
typedef unsigned long		ulong;
typedef unsigned int		uint;
typedef unsigned short		ushort;
typedef void*				handle;
typedef unsigned short		uint16;
typedef short				int16;
typedef int					boolean;

typedef uint32_t		uint32;
typedef int32_t			int32;
typedef uint64_t		uint64;
typedef int64_t			int64;

typedef intptr_t int_ptr;
typedef uintptr_t uint_ptr;

typedef struct
{
	int left, top, right, bottom;
} rect2d_t;

typedef struct 
{
	int x, y;
} point2d_t;
	
#endif // __TYPES_H
