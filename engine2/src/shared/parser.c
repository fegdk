#include <stdio.h>
#include <string.h>
#include "parser.h"
#ifndef NO_VFS
#include "filesystem.h"
#endif

char p_error_string[P_MAX_ERROR_STRING];
char p_buffer[P_MAX_BUFFER];
const char *p_script;
const char *p_end;
int p_size;
int p_line;
char *p_tokenptr;
int p_tokenready;
char p_token[P_MAX_TOKEN];
char p_fname[P_MAX_FNAME];

#ifndef NO_VFS
int
p_open_vfs_file (const char *fname)
{
	return p_init (fname, 0, -1);
}
#endif

int
p_open_file (const char *fname)
{
	return p_init (fname, 1, -1);
}

int
p_open_buffer (const char *buffer, int sz)
{
	return p_init (buffer, 0, sz);
}

int
p_init (const char *script, int stdio, int sz)
{
	int err;
	p_tokenready = 0;
	p_line = 0;
	if (sz == -1)
	{
		if (err = p_load_file (script, stdio)) return err;
		p_script = p_buffer;
		p_end = p_buffer + p_size;
		strcpy (p_fname, script);
	}
	else {
		p_script = script;
		p_size = sz;
		p_end = script + sz;
		strcpy (p_fname, "membuffer");
	}
	return P_OK;
}

void
p_close (void)
{
}

int
p_load_file (const char *fname, int stdio)
{
	if (stdio) {
		FILE *fp = fopen (fname, "rb");
		if (!fp) {
			sprintf (p_error_string, "file not found: %s\n", fname);
			return P_FILE_NOT_FOUND;
		}

		fseek (fp, 0, SEEK_END);
		p_size = ftell (fp);
		rewind (fp);

		fread (p_buffer, p_size, sizeof (char), fp);
		fclose (fp);
		p_buffer[p_size] = 0;
	}
	else {
		return -1;
	}
	return 0;
}

int
p_gettoken (void)
{
	if (p_tokenready)                         /* is a token already waiting? */
	{
		p_tokenready = 0;
		return P_OK;
	}

	if (p_script >= p_end) {
		sprintf (p_error_string, "end of file\n");
		return P_EOF;
	}

	/* skip spaces */
	for (;;)
	{
		while (p_script < p_end && *p_script <= 32)
		{
			if (*p_script++ == '\n')
				p_line++;
		}

		if (p_script >= p_end) {
			sprintf (p_error_string, "end of file\n");
			return P_EOF;
		}

		/* ; # // comments */

		/* this condition is correct, since it is at allowed to read at least one more byte (terminating zero) */
		if (*p_script == '#'
			|| (p_script[0] == '/' && p_script[1] == '/'))
		{
			while (p_script < p_end && *p_script++ != '\n');

			if (p_script >= p_end) {
				sprintf (p_error_string, "end of file\n");
				return P_EOF;
			}

			p_line++;
			continue;
		}

		/*  / * * / comments */

		if (p_script[0] == '/' && p_script[1] == '*')
		{
			p_script+=2;
			while (p_script[0] != '*' || p_script[1] != '/')
			{
				if (*p_script == '\n')
					p_line++;
				p_script++;
				if (p_script >= p_end) {
					sprintf (p_error_string, "end of file\n");
					return P_EOF;
				}
			}
			p_script += 2;
			continue;
		}

		break;
	}

	p_tokenptr = p_token;

	if (*p_script == '"')
	{
		/* quoted mToken */
		p_script++;
		while (*p_script != '"')
		{
			*p_tokenptr++ = *p_script++;
			if (p_script == p_end)
				break;
			if (p_tokenptr == &p_token[P_MAX_TOKEN])
			{
				sprintf (p_error_string, "token too large: line %i\n", p_line);
				return P_TOKEN_TOO_LARGE;
			}
		}
		p_script++;
	}
	else
	{
		/* separator */
		if (*p_script == '{'
			|| *p_script == '}'
			|| *p_script == '['
			|| *p_script == ']'
			|| *p_script == '('
			|| *p_script == ')'
			|| *p_script == '<'
			|| *p_script == '>'
			|| *p_script == ';'
			|| *p_script == '+'
			)
		{
			*p_tokenptr++ = *p_script++;
		}
		else
			/* regular mToken */
			while (*p_script > 32
				&& *p_script != '{'
				&& *p_script != '}'
				&& *p_script != '['
				&& *p_script != ']'
				&& *p_script != '('
				&& *p_script != ')'
				&& *p_script != '<'
				&& *p_script != '>'
				&& *p_script != ';'
				&& *p_script != '+'
				)
			{
				*p_tokenptr++ = *p_script++;
				if (p_script == p_end)
					break;
				if (p_tokenptr == &p_token[P_MAX_TOKEN])
				{
					sprintf (p_error_string, "token too large: line %i\n", p_line);
					return P_TOKEN_TOO_LARGE;
				}
			}
	}

	*p_tokenptr = 0;
	return P_OK;
}

void
p_ungettoken (void)
{
	p_tokenready = 1;
}

int
p_matchtoken (const char *match)
{
	int err;
	if (err = p_gettoken ()) return err;

	const char *p1, *p2;
	p1 = p_token;
	p2 = match;
	while (*p1 && *p2)
	{
		if (*p1 != *p2)
			break;
		p1++;
		p2++;
	}
	if (*p1 != *p2)
	{
		sprintf (p_error_string, "expected '%s': file '%s', line %d, expression '%s'\n"
			, match, p_fname[0], p_line + 1, p_token);
		return P_TOKEN_MISMATCH;
	}
	return P_OK;
}

int
p_nextline (void)
{
	int err;
	int line = p_line;
	while (line == p_line)
	{
		if (err = p_gettoken ()) return err;
	}
	p_ungettoken ();
	return P_OK;
}

int
p_ignoreblock (const char *opentag, const char *closetag)
{
	int opens = 1;
	int err;
	while (opens)
	{
		if (err = p_gettoken ()) return err;
		if (!p_cmptoken (opentag))
			opens++;
		else if (!p_cmptoken (closetag))
			opens--;
	}
	return P_OK;
}

int
p_getblockcontents (const char *opentag, const char *closetag, char *block, int sz)
{
	int err;
	int opens = 1;
	int ln = -1;
	char *end = block + sz;
	block = "";
	while (opens)
	{
		int add = 1;
		if (err = p_gettoken ()) return err;
		if (!p_cmptoken (opentag))
		{
			opens++;
		}
		else if (!p_cmptoken (closetag))
		{
			if (--opens == 0)
				add = 0;
		}
		if (block + 1 >= end)
			return P_BUFFER_TOO_SMALL;
		if (p_line != ln)
		{
			ln = p_line;
			strcat (block, "\n");
		}
		else
			strcat (block, " ");
		block++;
		if (add) {
			int l = strlen (p_token);
			if (block + l >= end)
				return P_BUFFER_TOO_SMALL;
			strcat (block, p_token);
			block += l;
		}
	}
	return P_OK;
}

int
p_cmptoken (const char *s)
{
	return strcmp (s, p_token);
}

int
p_tokenavailable (void)
{
	const char *search_p = p_script;
	while (*search_p <= 32)
	{
		if (*search_p == '\n')
			return 0;
		if (*search_p == 0)
			return 0;
		search_p++;
	}
	return 1;
}

int
p_iseol (void)
{
	const char *search_p = p_script;
	while (*search_p <= 32)
	{
		if (*search_p == '\n')
			return 1;
		if (*search_p == 0)
			return 1;
		search_p++;
	}
	return 0;
}

int
p_unexpectedeof (void)
{
	sprintf ("unexpected end of file: file '%s', line %d\n", p_fname, p_line + 1);
	return P_UNEXPECTED_EOF;
}

int
p_syntaxerror (void)
{
	sprintf ("error in expression: file '%s', line %d, expression '%s'\n", p_fname, p_line + 1, p_token);
	return P_SYNTAX_ERROR;
}


