/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_TIMER_H
#define __F_TIMER_H

#include "f_types.h"
#include "f_baseobject.h"

namespace fe
{

	/**
	 * initializes process time
	 */
	void
	initProcessTime (void);

	/**
	 * sleeps for specified number of milliseconds
	 * @param ms number of milliseconds to sleep
	 */
	void
	sleep (int ms);

	/**
	 * @return time elapsed from process start in seconds
	 */
	float getProcessTime (void);

	class FE_API timer : public baseObject
	{
	
	protected:
	
		float		mStartTime;

		float		mSysPrevTime;
		float		mSysCurrentTime;

		float		mPrevTime;
		float		mCurrentTime;

		bool		mbPaused;
	
	
	public:
	
		timer (void);
		~timer (void);
	
		/**
		 * @return time elapsed from start of timer, pause-aware
		 */
		float		getTime (void) const;

		/**
		 * @return time elapsed from previous timer update, pause-aware
		 */
		float		getTimePassed (void) const;

		/**
		 * @return time elapsed from start of timer
		 */
		float		getSysTime (void) const;
		
		/**
		 * @return time elapsed from previous timer update
		 */
		float		getSysTimePassed (void) const;

		/**
		 * pauses/unpauses the timer
		 * @param onoff true to pause, false to unpause
		 */
		void		pause (bool onoff);

		/**
		 * @return pause state, true if paused, false otherwise
		 */
		bool		isPaused (void) const;
	
		/**
		 * updates the timer state
		 */
		void		update (void);

	
	};
	
}

#endif // __F_TIMER_H


