// syntax:
// I(name,default_value)
// F(name,default_value)
// S(name,default_value)
I(sys_int_mainloop, 1)
S(sys_profile, "sdl_opengl")
I(sys_keyb_autorepeat, 0)
S(sys_mainscript, "scripts/main.txt")

I(r_fullscreen, 0)
I(r_vidmode, -1)
I(r_showtris, 0)
I(r_loadsurfaceparms,0)
I(r_use_vbo, 1)
I(r_texture_filtering, 1)	// 0 nearest, 1 linear, 2 trilinear, 3 anisotropic
I(r_usemipmaps, 1)
S(r_metashader, "materials/metainfo.txt")
F(gl_polygon_offset_factor_line, -0.5)
F(gl_polygon_offset_units_line, 0)

I(snd_maxnumbuffers, 1000)
I(snd_numstreamchunks, 16)
I(snd_streamchunksize, 10000)
I(snd_maxdupsources, 3)

I(cl_loglevel, 0)

I(ft_debug, 0)

I(i_debugkeys, 0)

S(audio_profile, "openal")

I(sys_stack_size, 8)
