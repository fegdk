/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexander Maltsev
    keltar@users.sourceforge.net
*/

#ifndef __F_MMANAGER_H
#define __F_MMANAGER_H

#include "f_types.h"
#include "f_baseobject.h"
#include "f_engine.h"

namespace fe
{

class pool : public baseObject
{
public:
	explicit pool (uint32 elem_size, uint32 num_elems);
	~pool ();

	void *alloc ();
	void free (void *p);

protected:
	uint32 mElemSize;
	uint32 mNumElems;
	uint32 *mpLastAllocated;
	ubyte mLastBit;
	uint32 mNumBitfields;
	ubyte *mpMemory;
	uint32 *mpBitfields;
};

class stackAllocator : public baseObject
{
public:
	explicit stackAllocator (uint32 size);
	~stackAllocator ();

	uint32 getMark () const;
	void setMark (uint32 mark);

	void *alloc (uint32 size);

protected:
	ubyte *mpPtr;
	uint32 mSize;
	uint32 mMark;
};

class stackAllocatorHandle
{
public:
	stackAllocatorHandle () : mpAllocator (g_engine->getStackAllocator ())
	{
		mMark = mpAllocator->getMark ();
	}
	stackAllocatorHandle (stackAllocator &a) : mpAllocator (&a)
	{
		mMark = a.getMark ();
	}
	~stackAllocatorHandle ()
	{
		mpAllocator->setMark (mMark);
	}

	void *alloc (uint32 size)
	{
		return mpAllocator->alloc (size);
	}

protected:
	uint32 mMark;
	stackAllocator *mpAllocator;
};


uint32 getUsedMemorySize ();

}

#endif
