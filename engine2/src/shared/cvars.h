#ifndef __CVARS_H
#define __CVARS_H

#include "types.h"

enum
{
	CVAR_INT,
	CVAR_FLOAT,
	CVAR_STRING
};

enum
{
	CVAR_SVALUE_SIZE = 64
};

enum
{
	CVAR_STORE = 0x00000001,    // save to config
	CVAR_READONLY = 0x00000002, // can't be set by user
	CVAR_SERVER = 0x00000004,   // propagate to all clients
	CVAR_CLIENT = 0x00000008,   // sent to server on user state changes
	CVAR_USERCREATED = 0x00000010, // created using set
	CVAR_INIT = 0x00000020, // set from cmdline only
	CVAR_LATCH = 0x00000040, // will be changed upon restart (cvar_restart or next cvars->get)
};

typedef struct cvar_s
{
	char *string;
	char *name;
	char *resetString;
	char *latchedString;
	uint32 flags;
	int modificationCount;
	float fvalue;
	int ivalue;
	struct cvar_s *next;
	struct cvar_s *hashNext;
	unsigned modified : 1;
} cvar_t;

struct field_s;

enum { CVARS_MAX = 1024, CVARS_HASH_SIZE = 256 };

cvar_t cvars[CVARS_MAX];
int num_cvars;

cvar_t *cvar_list;
cvar_t *cvars_hash[CVARS_HASH_SIZE];

uint32 cvar_modified_flags;
	
uint32
cvar_hash_value (const char *s);

char*
cvar_copy_string (const char *s);

cvar_t*
cvar_find (const char *s);

boolean
cvar_validate_string (const char *s);

void
cvar_init (void);

void
cvar_free (void);

cvar_t*
cvar_get (const char *name, const char *value, uint32 flags);

cvar_t*
cvar_set (const char *name, const char *value, boolean force);

cvar_t*
cvar_set_int (const char *name, int value, boolean force);

cvar_t*
cvar_set_float (const char *name, float value, boolean force);

const char *
cvar_string_value (const char *name);

void
cvar_string_value_buffer (const char *name, char *buffer, int len);

int
cvar_int_value (const char *name);

float
cvar_float_value (const char *name);

void
cvar_command_completion (void (*matchfunc)(const char *cmd, struct field_s *data), struct field_s *data);

int
cvar_get_idx (const char *n);

cvar_t*
cvar_for_idx (int idx);

#endif // __CVARS_H

