#ifndef __FILESYSTEM_H
#define __FILESYSTEM_H

#include <stdio.h>
#include "types.h"

enum {
	FS_MAX_FILE_NAME = 64,
	FS_MAX_PAKS = 128,
	FS_MAX_PATH_DIRS = 8,
	FS_MAX_PATH_LENGTH = 1024,
	FS_MAX_OPENED_FILES = 8,
};

/**
 * init vfs
 * @param path list of directories separated with ":", can be NULL
 * @return 0 on success
 */
int
fs_init (void);

void
fs_free (void);

/**
 * open file
 * @param fname a file name
 * @param mode file access mode
 */
int
fs_fopen (const char *fname);

// DON'T access this directly
typedef struct fs_file_s
{
	int stream;
	FILE* file;
	struct z_stream_s* zstream;
	uchar* zbuffer;
	ulong offset;	// seek position
	ulong zoffset;	// z seek position
} fs_file_t;

uint32
fs_ftell (fs_file_t *f);

int
fs_fseek (fs_file_t *f, int32 offset, int whence);

int
fs_fread (fs_file_t *f, void *buffer, int32 size);

int
fs_fread_int16 (fs_file_t *f, int16 *buffer, int32 num);

int
fs_fread_int32 (fs_file_t *f, int32 *buffer, int32 num);

int
fs_fread_float (fs_file_t *f, float *buffer, int32 num);

uint32
fs_fgetsize (fs_file_t *f);

void
fs_fclose (fs_file_t *f);

#endif // __FILESYSTEM_H
