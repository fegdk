/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_INPUT_H
#define __F_INPUT_H

#include "f_keycodes.h"
#include "f_baseobject.h"

namespace fe
{
	
	class engine;
	struct cvar_t;
	extern cvar_t *in_keyb_autorepeat;
	extern cvar_t *in_debugkeys;
	
	// NOTE: acts like singleton, shared by all input devices
	class FE_API inputState
	{
	private:
		bool	mKeyState[Key_LastKey];
	public:
		inputState (void) {reset ();}
		void reset (void) { memset( mKeyState, 0, sizeof( bool ) * Key_LastKey ); }
		bool isPressed (int code) { return mKeyState[code]; }
		void setState (int code, bool state) { mKeyState[code] = state; }
	};
	
	class inputManager
	{
	public:
		static int keyCodeByName( const cStr &code );
	};
	
	class FE_API inputDevice : public baseObject
	{
	private:
		inputState mState;
		int x, y;
	public:
	
		inputDevice (void);
		virtual inputState*	getState (void);
		void keyDown (int vk);
		void keyUp (int vk);
		void printableChar (int vk);
		void lButtonDown (void);
		void lButtonUp (void);
		void rButtonDown (void);
		void rButtonUp (void);
		void mButtonDown (void);
		void mButtonUp (void);
		void mouseMove (int x, int y);
		void mWheel (void);
		void setCursorPos( int _x, int _y );
		point getCursorPos();
	};
	
}

#endif // __F_INPUT_H

