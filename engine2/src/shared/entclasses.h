#ifndef __ENTCLASSES_H
#define __ENTCLASSES_H

#include "entity.h"
#include "f_math.h"
#include "f_model.h"

namespace fe
{

	enum		// ent classes
	{
		ECLASS_WORLD,
		ECLASS_SPAWNSPOT,
		ECLASS_MAX
	};

#define ECLASS_BEGIN(classname, classid) class FE_API classname : public entity {\
	public:\
		classname (uint32 id) : entity (id) {init ();}\
		void init (void);\
		uint32 getClass (void) const {return classid;}
#define ECLASS_END() };

	ECLASS_BEGIN (entWorld, ECLASS_WORLD)
	private:
		smartPtr <model> mpModel;
		std::vector <entityPtr> mEnts;
	public:
		~entWorld (void);
		void load (const char *fname);
		smartPtr <model> getModel (void) const;
	ECLASS_END ()
	
	ECLASS_BEGIN (entSpawnSpot, ECLASS_SPAWNSPOT)
	ECLASS_END ()

#undef ECLASS_BEGIN
#undef ECLASS_END

}

#endif // __ENTCLASSES_H
