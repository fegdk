/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_RESOURCETABLE_H
#define __F_RESOURCETABLE_H

#include "f_string.h"
#include "f_helpers.h"
#include "f_baseobject.h"
#include "f_resourcemgr.h"
#include "f_error.h"

namespace fe
{
		
	template <typename T> class resource
	{
	
	private:
	
		// FIXME: do it without stl.. some hashtable or whatever
		typedef std::map <cStr, T*> resourceMap;
		resourceMap		mResourceList;
	
	public:
	
		resource (void)	// this will be the owner of all created objects
		{
		}
	
		~resource (void)
		{
			typename resourceMap::iterator it;
			std::vector <baseObject*> lst;
			for (it = mResourceList.begin (); it != mResourceList.end (); it++)
			{
				lst.push_back ((baseObject*)(*it).second);
			}
			size_t sz = lst.size ();
			for (size_t i = 0; i < sz; i++)
				lst[i]->release ();
		}
	
		T* create (const char *name)
		{
			T *o = getResource (name);
			if (!o)
			{
				o = new T (name);
				if (!o->isValid ())
				{
					o->release ();
					return NULL;
				}
				addResource (name, o);
			}
			return o;
		}
		
		T* create (charParser &parser)
		{
			parser.getToken ();
			cStr name = parser.token ();
			T *o = getResource (name);
			if (o)
			{
				fprintf (stderr, "WARNING: object with name %s is already defined\n", name.c_str ());
				return o;
			}
			T *obj = new T (parser, name);
			if (!obj->isValid ())
			{
				o->release ();
				return NULL;
			}
			addResource (name, obj);
			return obj;
		}
	
		void destroy (const char *name)
		{
			removeResource (name);
		}
		
		void flushUnused (void)
		{
			typename resourceMap::iterator it;
			std::vector <T*> lst;
			for (it = mResourceList.begin (); it != mResourceList.end (); it++)
				lst.push_back ( (*it).second);
			size_t sz = lst.size ();
			for (size_t i = 0; i < sz; i++)
			{
				if (!lst[i]->isUsed ())
					lst[i]->release (); // this SHOULD remove it from resource manager
			}
		}
		
		T* getResource (const char *name)
		{
			typename resourceMap::iterator it;
			it = mResourceList.find (name);
			if (it != mResourceList.end ())
				return (*it).second;
			return NULL;
		}
	
		int getCount (void) const
		{
			return (int)mResourceList.size ();
		}
		
		T* getObject (int idx) const
		{
			typename resourceMap::const_iterator it;
			int i = 0;
			for (it = mResourceList.begin (); it != mResourceList.end (); it++, i++)
			{
				if (i == idx)
				{
					return (*it).second;
				}
			}
			return NULL;
		}
		
		void addResource (const char *name, T *r)
		{
			mResourceList[name] = r;
			r->addRef ();
		}
		
		void removeResource (const char *name)
		{
			typename resourceMap::iterator it;
			it = mResourceList.find (name);
			if (it != mResourceList.end ())
			{
				 (*it).second->release ();
				mResourceList.erase (it);
			}
			else
			{
				fprintf (stderr, "WARNING: cannot delete '%s' - no such resource\n", name);
			}
		}
		
	};
}

#endif // __F_RESOURCETABLE_H

