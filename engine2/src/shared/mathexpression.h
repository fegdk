/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_MATHEXPRESSION_H
#define __F_MATHEXPRESSION_H

#include "f_string.h"
#include "f_types.h"
#include "f_baseobject.h"
#include <vector>
#include "f_helpers.h"

namespace fe
{

	class mathExpression : public baseObject
	{
	private:
		// FIXME: do we need more than 2?
		// FIXME: hi-level expressions never use more than 2
		// FIXME: but if we allow user to write something more complex (and optimized) in assembly - 2 regs are insufficient
		// FIXME: increment maxregisters to whatever required when assembly programs support is available
		enum { maxregisters = 2 };
	
		enum operator_t { op_no, op_add, op_sub, op_mul, op_div, op_dot, op_mov, op_lookup, op_max };
	
		enum { parmtypemask = (0x0003 << 14), constmask = (0x0000 << 14), regmask = (0x0001 << 14), varmask = (0x0002 << 14), resmask = (0x0003 << 14) };
	
		struct command_t {
			ubyte	opcode;
			uchar	dst;
			ushort	src0;
			ushort	src1;
		};
	
		struct node {
			node () {
				root = false;
				leaf = false;
				op = op_no;
				reg = -1;
				left = NULL;
				right = NULL;
			}
			bool root;
			bool leaf;
			operator_t op;
			int reg;
			cStr strvalue;
	
			node *left;
			node *right;
		};
	
		std::vector< float >	mConstants;
		std::vector< command_t >	mCommands;
	
		void		getwords (const cStr &in, std::vector< cStr > &expression);
		bool		validate (const std::vector<cStr> &expr);
		cStr		opstring (int op);
		operator_t	getoperator (const std::vector<cStr> &expression, size_t curword);
		int			getparamidx (const char *name) const;
		float*		getvarvalueptr (int v) const;
		bool		iscomplex (const cStr &s);
		node*		buildtree (std::vector<cStr> &expr);
		cStr		strop (operator_t op) const;
		bool		isconst (const char *s);
		void		traverse (node *n, std::vector< mathExpression::command_t > &commands, std::vector< float > &constants, int reg = 0);
		void		deltree (node *n);
	#ifdef _DEBUG
		void		printProgram (const std::vector< mathExpression::command_t > &commands, const std::vector< float > &constants) const;
	#endif
	
	public:
	
		mathExpression (void);
		mathExpression (const char *expr);
	
		~mathExpression (void);
	
		float		evaluate (void);
	};
	
	typedef smartPtr <mathExpression> mathExpressionPtr;
	
}

#endif // __F_MATHEXPRESSION_H

