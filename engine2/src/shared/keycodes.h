/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_KEYCODES_H
#define __F_KEYCODES_H

// the code and entire idea was taken from quake code
// hope johnc will forgive my lazyness :)

//
// these are the key numbers that should be passed to KeyEvent
//

// normal keys should be passed as lowercased ascii

namespace fe
{

	enum keyCode {
		Key_None = 0,
		Key_CtrlC = 3,
		Key_CtrlV = 22,
		Key_BackSpace = 8,
		Key_Tab = 9,
		Key_Enter = 13,
		Key_Return = 13,
		Key_CR = 13,
		Key_Escape = 27,
		Key_Esc = 27,
		Key_Space = 32,


		Key_Del = 127,
		Key_Command = 128,
		Key_CapsLock,
		Key_ScrollLock,
		Key_Power,
		Key_Pause,

		Key_UpArrow,
		Key_DownArrow,
		Key_LeftArrow,
		Key_RightArrow,

		Key_LAlt,
		Key_RAlt,
		Key_Alt,
		Key_LCtrl,
		Key_RCtrl,
		Key_Ctrl,
		Key_LShift,
		Key_RShift,
		Key_Shift,
		Key_LMeta,
		Key_RMeta,
		Key_Meta,
		Key_LSuper,
		Key_RSuper,
		Key_Super,
		Key_Ins,
		Key_PgDn,
		Key_PgUp,
		Key_Home,
		Key_End,
		Key_Scroll,

		Key_F1,
		Key_F2,
		Key_F3,
		Key_F4,
		Key_F5,
		Key_F6,
		Key_F7,
		Key_F8,
		Key_F9,
		Key_F10,
		Key_F11,
		Key_F12,
		Key_F13,
		Key_F14,
		Key_F15,

		Key_Kp_Home,
		Key_Kp_UpArrow,
		Key_Kp_PgUp,
		Key_Kp_LeftArrow,
		Key_Kp_5,
		Key_Kp_RightArrow,
		Key_Kp_End,
		Key_Kp_DownArrow,
		Key_Kp_PgDn,
		Key_Kp_Enter,
		Key_Kp_Ins,
		Key_Kp_Del,
		Key_Kp_Slash,
		Key_Kp_Minus,
		Key_Kp_Plus,
		Key_Kp_NumLock,
		Key_Kp_Star,
		Key_Kp_Equals,

		Key_Mouse1,
		Key_Mouse2,
		Key_Mouse3,
		Key_Mouse4,
		Key_Mouse5,
		Key_MWheelDown,
		Key_MWheelUp,

		Key_Joy1,
		Key_Joy2,
		Key_Joy3,
		Key_Joy4,
		Key_Joy5,
		Key_Joy6,
		Key_Joy7,
		Key_Joy8,
		Key_Joy9,
		Key_Joy10,
		Key_Joy11,
		Key_Joy12,
		Key_Joy13,
		Key_Joy14,
		Key_Joy15,
		Key_Joy16,
		Key_Joy17,
		Key_Joy18,
		Key_Joy19,
		Key_Joy20,
		Key_Joy21,
		Key_Joy22,
		Key_Joy23,
		Key_Joy24,
		Key_Joy25,
		Key_Joy26,
		Key_Joy27,
		Key_Joy28,
		Key_Joy29,
		Key_Joy30,
		Key_Joy31,
		Key_Joy32,

		Key_Aux1,
		Key_Aux2,
		Key_Aux3,
		Key_Aux4,
		Key_Aux5,
		Key_Aux6,
		Key_Aux7,
		Key_Aux8,
		Key_Aux9,
		Key_Aux10,
		Key_Aux11,
		Key_Aux12,
		Key_Aux13,
		Key_Aux14,
		Key_Aux15,
		Key_Aux16,

		// this code is used to define size of keystate array
		Key_LastKey	= 0x100,

		// this flag is used to determine what we've got
		// if it is set - we got printable character
		// if not - we got some key which should not be printed
		Key_CharFlag = 0x80000000
	};

}

#endif // __F_KEYCODES_H
