#ifndef __ENTITYMANAGER_H
#define __ENTITYMANAGER_H

#include "f_baseobject.h"
#include "entity.h"

namespace fe
{

	class FE_API entityManager : public baseObject
	{
		private:
			uint32 mLastId;
			typedef std::map < uint32, smartPtr <entity> > entmap_t;
			typedef std::map < uint32, std::vector <uint32> > entclass_map_t;
			entmap_t mEntities;
			entclass_map_t mClassifiedEnts;
		public:
			entityManager (void);
			~entityManager (void);
			void clean (void);
			uint32 getEClassForClassName (const char *classname) const;
			smartPtr <entity> createEntity (uint32 classId);
			void destroyEntity (uint32 id);
			void destroyEntity (smartPtr <entity> ent);
			smartPtr <entity> getEntityForId (uint32 id);
			void getEntListForClass (uint32 classId, std::vector <smartPtr <entity> > &ents);
	};

}

#endif // __ENTITYMANAGER_H
