/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_APPLICATION_H
#define __F_APPLICATION_H

#include "f_types.h"
#include "f_baseobject.h"

namespace fe
{

	/**
	 * This class is used to create typical application using FEGDK.
	 * Usage is simple. Derive own class off of application,
	 * implement all methods that are needed by your app.
	 * Then create instance of an object of your new application class.
	 * Create feEngine instance. Pass pointer to your app to feEngine::run.
	 * You're done.
	 * This class may be used even for full-scale big projects,
	 * but it is not suitable for some types of applications.
	 * E.g. see how FEditor uses the feEngine providing own renderer class and own mainloop.
	 */
	class FE_API application : public baseObject
	{
	
	protected:
	
		application (void);
		virtual ~application (void);
	
	public:
	
	/**
	 * This method is called just after console is created. All other systems are down yet.
	 * Application is responsible for loading all it's stuff into cvars.
	 * Default implementation just loads autoexec.cfg (it's enough for most needs).
	 */
		virtual void		configure (void);
	
	/**
	 * This method is called by the engine just before mainloop starts.
	 * You can load required resources here and initialize some stuff.
	 * Most of the engine stuff is accessible at this point,
	 * but mainloop was not started yet
	 */
		virtual void		init( void );
	
	/**
	 * This method is called by the engine right after mainloop exits.
	 * You can stop playing sounds/music here, and do similar stuff.
	 */
		virtual void		free( void );
	
	/**
	 * Sets application name or window title or whatever.
	 * Please, notice feStr usage - it is a char/wchar mapping type, so use TCHARS instead of char or wchar_t. It is required for unicode version of the engine.
	 * @return value, which is usually will be set as the WM window title.
	 */
		virtual const TCHAR*		getTitle( void ) const;
	
	/**
	 * @return relative path to directory which will be set as virtual filesystem root.
	 */
		virtual const char*	dataPath( void ) const;
	/**
	 * You can handle keyboard keypresses here. Note: although all keycodes are listed in a single enumeration, different devices can get different codes.
	 * @param keycode contains key code (from f_keycodes.h). If OR'ed with Key_CharFlag - it is a printable character.
	 */
		virtual void		keyDown( int keycode );
	
	/**
	 * You can handle key releases here
	 * @param keycode code of the released key (see application::keyDown for details)
	 */
		virtual void		keyUp( int keycode );
	
	/**
	 * Mouse movement handler.
	 * Recieves absolute window-relative position of mouse pointer.
	 * @param x new horizontal position of mouse pointer
	 * @param y new vertical position of mouse pointer
	 */
		virtual void		mouseMove( int x, int y );
	
	/**
	 * Mouse button press handler.
	 * @param keycode code of mouse button, one of Key_Mouse1...Key_Mouse5.
	 */
		virtual void		mouseDown( int keycode );
	
	/**
	 * Mouse pbutton release handler.
	 * @param keycode code of mouse button, one of Key_Mouse1...Key_Mouse5.
	 */
		virtual void		mouseUp( int keycode );
	/**
	 * Mouse wheel rotation handler.
	 * @param delta amount of rotation (signed).
	 */
		virtual void		mouseWheel(int delta);
	
	/**
	 * Method used to update the application's behavior.
	 * Move tanks and planes, fire weapons, play sounds, etc here.
	 * No, you cannot do it in render method (it was made 'const' intentionally).
	 */
		virtual void		update( void );
	
	/**
	 * Method used to render the visuals.
	 * You cannot really update anything serious here, cause the method is 'const'.
	 * Use 'update' for that kind of stuff.
	 * You don't need to do funky stuff like 'SwapBuffers' or 'Present'.
	 * It is done by the engine automatically.
	 * If different behavior needed - please contact waker@users.sf.net directly.
	 */
		virtual void		render( void ) const;
	
	};
	
}

#endif // __F_APPLICATION_H
