/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_STRING_H
#define __F_STRING_H

#include "f_types.h"

namespace fe {
	
	struct DOUBLE_S  { uchar doubleBits[sizeof(double)]; };
	#define DOUBLE_ARG  DOUBLE_S
	
	struct FLOAT_S  { uchar floatBits[sizeof(float)]; };
	#define FLOAT_ARG  FLOAT_S
	
	#define FORCE_ANSI      0x10000
	#define FORCE_UNICODE   0x20000
	#define FORCE_INT64     0x40000
	
	#define TCHAR_ARG   TCHAR
	#define WCHAR_ARG   wchar_t
	#define CHAR_ARG    char
	
}

#include "f_cstr.h"
#include "f_wstr.h"

namespace fe {

	#ifdef UNICODE
	typedef wStr feStr;
	#else
	typedef cStr feStr;
	#endif

}

#endif // __F_STRING_H

