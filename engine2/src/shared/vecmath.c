#include <math.h>
#include "vecmath.h"
#include "types.h"

void
vec3_rotate_around_axis (float3 out, float3 in, float3 axis, float angle)
{
	float3 up;
	float sint = (float)sin (angle);
	float cost = (float)cos (angle);
	vec3_cross (up, in, axis);
	out[0] = in[0] * cost + in[0] * sint;
	out[1] = in[1] * cost + in[1] * sint;
	out[2] = in[2] * cost + in[2] * sint;
}

float
vec3_norm (float3 out, float3 in)
{
	const float tolerance = 1.0e-6f;
	float d = vec3_mag (in);
	if (d > tolerance)
	{
		float id = 1.f / d;
		out[0] *= id;
		out[1] *= id;
		out[2] *= id;
	}
	else
		d = 0;

	return d;
}

void
vec3_make_normals (float3 front, float3 right, float3 up)
{
	float d;

	// this rotate and negate guarantees a vector
	// not colinear with the original
	right[1] = -front[0];
	right[2] = front[1];
	right[0] = front[2];

	d = vec3_dot (right, front);
	vec3_mad (right, right, front, -d);
	vec3_norm (right, right);
	vec3_cross (up, right, front);
}

void
m4x4_identity (float4x4 m)
{
	vec4_init (m[0], 1, 0, 0, 0);
	vec4_init (m[1], 0, 1, 0, 0);
	vec4_init (m[2], 0, 0, 1, 0);
	vec4_init (m[3], 0, 0, 0, 1);
}

void
m4x4_copy (float4x4 out, float4x4 in)
{
	vec4_copy (out[0], in[0]);
	vec4_copy (out[1], in[1]);
	vec4_copy (out[2], in[2]);
	vec4_copy (out[3], in[3]);
}

void
m4x4_translation (float4x4 m, float x, float y, float z)
{
	vec4_init (m[0], 1, 0, 0, 0);
	vec4_init (m[1], 0, 1, 0, 0);
	vec4_init (m[2], 0, 0, 1, 0);
	vec4_init (m[3], x, y, z, 1);
}

void
m4x4_rotation_x (float4x4 m, float angle)
{
	float sint = sin (angle);
	float cost = cos (angle);
	vec4_init (m[0], 1, 0, 0, 0);
	vec4_init (m[1], 0, cost, sint, 0);
	vec4_init (m[2], 0, -sint, cost, 0);
	vec4_init (m[3], 0, 0, 0, 1);
}

void
m4x4_rotation_y (float4x4 m, float angle)
{
	float sint = sin (angle);
	float cost = cos (angle);
	vec4_init (m[0], cost, 0, -sint, 0);
	vec4_init (m[1], 0, 1, 0, 0);
	vec4_init (m[2], sint, 0, cost, 0);
	vec4_init (m[3], 0, 0, 0, 1);
}

void
m4x4_rotation_z (float4x4 m, float angle)
{
	float sint = sin (angle);
	float cost = cos (angle);
	vec4_init (m[0], cost, sint, 0, 0);
	vec4_init (m[1], -sint, cost, 0, 0);
	vec4_init (m[2], 0, 0, 1, 0);
	vec4_init (m[3], 0, 0, 0, 1);
}

void
m4x4_rotate_around_axis (float4x4 m, float3 axis, float angle)
{
	float cost;
	float sint;
	float3 v;

	cost = (float)cos (angle);
	sint = (float)sin (angle);
	vec3_norm (v, axis);

	m[0][0] = (v[0] * v[0]) * (1.0f - cost) + cost;
	m[0][1] = (v[0] * v[1]) * (1.0f - cost) - (v[2] * sint);
	m[0][2] = (v[0] * v[2]) * (1.0f - cost) + (v[1] * sint);

	m[1][0] = (v[1] * v[0]) * (1.0f - cost) + (v[2] * sint);
	m[1][1] = (v[1] * v[1]) * (1.0f - cost) + cost ;
	m[1][2] = (v[1] * v[2]) * (1.0f - cost) - (v[0] * sint);

	m[2][0] = (v[2] * v[0]) * (1.0f - cost) - (v[1] * sint);
	m[2][1] = (v[2] * v[1]) * (1.0f - cost) + (v[0] * sint);
	m[2][2] = (v[2] * v[2]) * (1.0f - cost) + cost;

	m[0][3] = 0.0f;
	m[1][3] = 0.0f;
	m[2][3] = 0.0f;
	m[3][3] = 1.0f;
}
	
void
m4x4_from_euler_angles_xyz (float4x4 m, float ax, float ay, float az)
{
	double ci, cj, ch, si, sj, sh, cc, cs, sc, ss;

	ci = cos(ax);
	cj = cos(ay);
	ch = cos(az);
	si = sin(ax);
	sj = sin(ay);
	sh = sin(az);
	cc = ci*ch;
	cs = ci*sh;
	sc = si*ch;
	ss = si*sh;

	m[0][0] = (float)(cj*ch);
	m[1][0] = (float)(sj*sc-cs);
	m[2][0] = (float)(sj*cc+ss);
	m[0][1] = (float)(cj*sh);
	m[1][1] = (float)(sj*ss+cc);
	m[2][1] = (float)(sj*cs-sc);
	m[0][2] = (float)-sj;
	m[1][2] = (float)(cj*si);
	m[2][2] = (float)(cj*ci);
	m[0][3] = 0;
	m[1][3] = 0;
	m[2][3] = 0;
	m[3][0] = 0;
	m[3][1] = 0;
	m[3][2] = 0;
	m[3][3] = 1;
}

void
m4x4_vec3_mul (float3 r, float4x4 m, float3 v)
{
	float                       x;
	float                       y;
	float                       z;
	float                       w;

	w = v[0] * m[0][3] + v[1] * m[1][3] + v[2] * m[2][3] + m[3][3];
	
	if (fabs (w) < SMALL_NUM) {
		vec3_init (r, 0, 0, 0);
		return;
	}

	x = v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0] + m[3][0];
	y = v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1] + m[3][1];
	z = v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2] + m[3][2];

	r[0] = x/w;
	r[1] = y/w;
	r[2] = z/w;
}

void
m4x4_vec4_mul (float4 r, float4x4 m, float4 v)
{
	r[0] = v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0] + v[3] * m[3][0];
	r[1] = v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1] + v[3] * m[3][1];
	r[2] = v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2] + v[3] * m[3][2];
	r[3] = v[0] * m[0][3] + v[1] * m[1][3] + v[2] * m[2][3] + v[3] * m[3][3];
}

void
m4x4_mul (float4x4 r, float4x4 m, float4x4 b)
{
	int i, j, k;
	for (i=0; i<4; i++) {
		for (j=0; j<4; j++) {
			r[i][j] = 0;
			for (k=0; k<4; k++) {
				r[i][j] += m[i][k] * b[k][j];
			}
		}
	}
}

void
m4x4_transpose (float4x4 r, float4x4 m)
{
	int i, j;
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 4; j++)
		{
			r[i][j] = m[j][i];
		}
	}
}

void
m4x4_vec3_transform (float3 r, float4x4 m, float3 v)
{
	r[0] = v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0];
	r[1] = v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1];
	r[2] = v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2];
}

int
m4x4_projection (float4x4 r, float fov, float aspect, float np, float fp)
{
	float w, h, Q;
	float d = fp-np;
	float cost, sint, cot;
	fov *= 0.5f;
	cost = cos(fov);
	sint = sin(fov);
	cot = cost/sint;

	if (fabs (d) < 0.01f)
		return -1;
	if (fabs (sint) < 0.01f)
		return -1;

	w = aspect * cot;
	h = 1.0f * cot;
	Q = fp / d;

	r[0][0] = w;
	r[1][1] = h;
	r[2][2] = Q;
	r[2][3] = 1.0f;
	r[3][2] = -Q*np;

	return 0;
}

int
m4x4_lookat (float4x4 r, float3 from, float3 at, float3 up)
{
	float3	vView;
	float3	vUp;
	float3	vRight;
	float	flength;
	float	fdotProduct;

	//Get the z basis vector, which points straight ahead. This is the
	//difference from the eyepoint to the lookat point.
	vec3_sub (vView, at, from);
	flength = vec3_mag (vView);

	//TODO: error handling
	if (flength < SMALL_NUM)
		return -1;

	//normalize the z basis vector
	vec3_invscale (vView, vView, flength);

	//Get the dot product, and calculate the projection of the z basis
	//vector onto the up vector. The projection is the y basis vector.
	fdotProduct = vec3_dot (up, vView);

	vec3_mad (vUp, up, vView, -fdotProduct);

	//If this vector has near-zero length because the input specified a
	//bogus up vector, let's try a default up vector
	flength = vec3_mag (vUp);

	if (flength < SMALL_NUM) {
		float3 defUp = {0,1,0};
		vec_mad (vUp, defUp, vView, -vView[1]);
	
		//If we still have near-zero length, resort to a different axis
		flength = vec3_mag (vUp);

		if (flength < SMALL_NUM) {
			float3 defUp = {0,0,1};
			vec3_mad (vUp, defUp, vView, -vView[2]);
		
			flength = vec3_mag (vUp);
			if (flength < SMALL_NUM)
				return -1;
		}
	}

	//normalize the y basis vector
	vec3_invscale (vUp, vUp, flength);

	//The x basis vector is found simply with the cross product of the y
	//and z basis vectors
	vec3_cross (vRight, vUp, vView);

	//Start building the matrix. The first three rows contains the basis
	//vectors used to rotate the view to point at the lookat point
	identity ();

	r[0][0] = vRight[0];
	r[0][1] = vUp[0];
	r[0][2] = vView[0];

	r[1][0] = vRight[1];
	r[1][1] = vUp[1];
	r[1][2] = vView[1];

	r[2][0] = vRight[2];    
	r[2][1] = vUp[2];    
	r[2][2] = vView[2];

	//Do the translation values (rotations are still about the eyepoint)
	r[3][0] = -vec3_dot (from, vRight);
	r[3][1] = -vec3_dot (from, vUp);
	r[3][2] = -vec3_dot (from, vView);

	r[0][3] = 0;
	r[1][3] = 0;
	r[2][3] = 0;
	r[3][3] = 1;

	return 0;
}

void
m4x4_ortho_offcenter (float4x4 res, float l, float r, float b, float t, float zn, float zf)
{
	res[0][0] = 2/ (r-l);     res[0][1] = 0;            res[0][2] = 0;           res[0][3] = 0;
	res[1][0] = 0;            res[1][1] = 2/ (t-b);     res[1][2] = 0;           res[1][3] = 0;
	res[2][0] = 0;            res[2][1] = 0;            res[2][2] = 1/ (zf-zn);  res[2][3] = 0;
	res[3][0] = (l+r)/ (l-r); res[3][1] = (t+b)/ (b-t); res[3][2] = zn/ (zn-zf); res[3][3] = 1;
}

void
m4x4_ortho (float4x4 r, float w, float h, float zn, float zf)
{
	r[0][0] = 2/w; r[0][1] = 0;   r[0][2] = 0;           r[0][3] = 0;
	r[1][0] = 0;   r[1][1] = 2/h; r[1][2] = 0;           r[1][3] = 0;
	r[2][0] = 0;   r[2][1] = 0;   r[2][2] = 1/ (zf-zn);  r[2][3] = 0;
	r[3][0] = 0;   r[3][1] = 0;   r[3][2] = zn/ (zn-zf); r[3][3] = 1;
}

void
m4x4_inverse (float4x4 r, float4x4 m)
{
	float                       fDetInv;

	if (fabs (m[3][3] - 1.0f) > 1.0e-3f)
	{
		m4x4_identity (r);
		return;
	}

	if (fabs (m[0][3]) > 1.0e-3f || 
		fabs (m[1][3]) > 1.0e-3f || 
		fabs (m[2][3]) > 1.0e-3f)
	{
		m4x4_identity (r);
		return;
	}

	fDetInv = 1.0f / (m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) -
					   m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
					   m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]));

	r[0][0] =  fDetInv * (m[1][1] * m[2][2] - m[1][2] * m[2][1]);
	r[0][1] = -fDetInv * (m[0][1] * m[2][2] - m[0][2] * m[2][1]);
	r[0][2] =  fDetInv * (m[0][1] * m[1][2] - m[0][2] * m[1][1]);
	r[0][3] = 0.0f;

	r[1][0] = -fDetInv * (m[1][0] * m[2][2] - m[1][2] * m[2][0]);
	r[1][1] =  fDetInv * (m[0][0] * m[2][2] - m[0][2] * m[2][0]);
	r[1][2] = -fDetInv * (m[0][0] * m[1][2] - m[0][2] * m[1][0]);
	r[1][3] = 0.0f;

	r[2][0] =  fDetInv * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);
	r[2][1] = -fDetInv * (m[0][0] * m[2][1] - m[0][1] * m[2][0]);
	r[2][2] =  fDetInv * (m[0][0] * m[1][1] - m[0][1] * m[1][0]);
	r[2][3] = 0.0f;

	r[3][0] = - (m[3][0] * r[0][0] + m[3][1] * r[1][0] + m[3][2] * r[2][0]);
	r[3][1] = - (m[3][0] * r[0][1] + m[3][1] * r[1][1] + m[3][2] * r[2][1]);
	r[3][2] = - (m[3][0] * r[0][2] + m[3][1] * r[1][2] + m[3][2] * r[2][2]);
	r[3][3] = 1.0f;
}

void
plane_from_points (plane_t r, float3 a, float3 b, float3 c)
{
	float3 ba, cb;
	vec3_sub (ba, b, a);
	vec3_sub (cb, c, b);
	vec3_cross (r, ba, cb);
	vec3_norm (r, r);
	r[3] = vec3_dot (r, a);
}

float
plane_distance_to_point (plane_t p, float3 a)
{
	return vec3_dot (a, p) - p[3];
}
	
void
plane_project_point (float3 r, plane_t p, float3 a)
{
	vec_mad (r, a, p, -plane_distance (p, a));
}

void
box3_zero (box3_t *b) {
	vec3_zero (b->center);
	vec3_unitx (b->axis[0]);
	vec3_unity (b->axis[1]);
	vec3_unitz (b->axis[2]);
	b->extent[0] = b->extent[1] = b->extent[2];
}
