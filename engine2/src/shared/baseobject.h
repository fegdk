/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_BASEOBJECT_H
#define __F_BASEOBJECT_H

#include "f_string.h"
#include "f_helpers.h"

namespace fe {

	/** Base class for most of the system classes. */
	/**
	 * The class is used for almost all classes in FEGDK.
	 * It provides basic functionality, such as refcounting, naming and checking for memory leaks.
	 */
	class FE_API baseObject
	{
		friend class leakManager;
	
	protected:
	
	/**
	 * internal counter used to assign IDs to newly created objects
	 */
		static int mLastId;
	/**
	 * pointer to list of all created objects.
	 * gets allocated on first object creation.
	 * gets freed when the last object is destroyed.
	 * used by leakManager to determine which objects was not released before program termination.
	 */
		static std::map< int, baseObject* >* mpObjects;
	/**
	 * name of the object
	 */
		cStr mName;
	/**
	 * object's reference counter.
	 * can be accessed by derived classes for debug purposes.
	 */
		int mRefc;
	/**
	 * identifier of the object. assigned automatically. should not be changed during session.
	 */
		int mId;
	
	/**
	 * value of refcounter for the moment when object was marked as used
	 */
		int mUsedRefc;
	
		bool mbValid;
	
	/** virtual dtor */
	/**
	 * 'release' method calls 'delete this', so virtual is required
	 */
		virtual ~baseObject (void);
	
	/** main ctor */
	/**
	 * this ctor is most used in derived classes.
	 */
		baseObject (void);
	
	public:
	
	// don't use those addref/release to refcount object
	// smartptr should take care of that
	
	/**
	 * increases reference counter by 1
	 */
		void addRef(void);
	/**
	 * decreases reference counter by 1.
	 * calls 'delete this' when counter reaches zero.
	 */
		void release(void);
	/**
	 * returns name of an object.
	 * in case the name is NULL returns empty string ("")
	 */
		const char* name( void ) const;
	/**
	 * sets the name for an object.
	 * @param nm new name
	 */
		void setName (const char *nm);
	
	/**
	 * tells object that it is used from now and can proceed with loading it's data using prefetch
	 */
		void use (void);
	
	/**
	 * returns 'true' if object refcount is more then 1, 'false' otherwise.
	 */
		bool isUsed (void) const;
	
	/**
	 * object data loading method
	 * should be called by feScript::prefetch or by the object itself when used
	 */
		virtual void loadData (void);
	
	/**
	 * returns 'true' if object creation was successful
	 */
		bool isValid (void) const;
	
	/**
	 * implements base for Run-Time Type Information.
	 * returns true if an object belongs to a specified type-id.
	 * returns false otherwise.
	 * @param type type-id of an object we're testing against
	 */
		virtual bool isTypeOf (int type) const;
	
	};
	
	typedef smartPtr<baseObject> baseObjectPtr;
	
}

#endif // __F_BASEOBJECT_H

