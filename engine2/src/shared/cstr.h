/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_CSTR_H
#define __F_CSTR_H

#include <stdarg.h>
#include <wchar.h>
#include <stdio.h>
#include <ctype.h>
#include <assert.h>
#include <string.h>
#include "f_string.h"
#include "f_types.h"

namespace fe {

	class FE_API cStr
	{
	protected:
		char*	_Ptr;
		size_t	_Len;
		size_t	_Res;
	
	public:
	
		static const size_t npos = 0xffffffff;
	
		cStr( const char *s )
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
			assign( s );
		}
	
		cStr( const wchar_t *s )
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
			assign( s );
		}
	
		cStr()
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
		}
	
		cStr( const cStr& _X )
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
			assign(_X, 0, npos);
		}
	
		cStr (const char *s, size_t sz)
		{
			if (!s)
			{
				_Ptr = NULL;
				_Len = 0;
				_Res = 0;
			}
			else
			{
				_Res = sz;
				_Len = sz;
				_Ptr = new char [sz+1];
				strncpy (_Ptr, s, sz);
				_Ptr[sz] = 0;
			}
		}
	
		cStr( const cStr& _X, size_t _P, size_t _M )
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
			assign(_X, _P, _M);
		}
	
		~cStr();
		void grow( size_t s );
	
		void clear( void )
		{
			if ( _Len )
			{
				_Len = 0;
				*_Ptr = 0;
			}
		}
	
		cStr &assign( const char *s )
		{
			resize( strlen( s ) );
			if ( _Len )
				strcpy( _Ptr, s );
			else if ( _Ptr )
				*_Ptr = 0;
			return *this;
		}
	
		cStr &assign( const wchar_t *s )
		{
			resize( wcslen( s ) );
			size_t i;
			for ( i = 0; i < _Len; i++ )
				_Ptr[i] = (char)*s++;
	 		if ( _Len )
	 			_Ptr[i] = 0;
			return *this;
		}
	
		void printf( char *fmt, ... )
		{
	//		try {
				int n;
				char *p, *np;
				va_list ap;
	
				if (_Len == 0)
					resize (strlen (fmt));
	
				while (1) {
					/* Try to print in the allocated space. */
					va_start(ap, fmt);
					n = vsnprintf (_Ptr, _Len, fmt, ap);
					va_end(ap);
					/* If that worked, return the string. */
					if (n > -1 && n < _Len)
						break;
					/* Else try again with more space. */
					if (n > -1)    /* glibc 2.1 */
						resize (_Len + 1); /* precisely what is needed */
					else           /* glibc 2.0 */
						resize (_Len * 2);  /* twice the old size */
				}
	
	//		}
	//		catch ( ... )
	//		{
				// seems like too big line. terminate.
	//		}
		}	
	
		void trim_left( char *targets )
		{
			char *c = _Ptr;
	
			while ( *c != '\0' )
			{
				if ( strchr( targets, *c ) == NULL )
					break;
				c++;
			}
	
			if ( c != _Ptr )
			{
				// fix up data and length
				size_t l = _Len - ( c - _Ptr );
				memmove( _Ptr, c, ( l + 1 ) * sizeof( char ) );
				_Len = l;
			}
		}
	
		void trim_left( char target )
		{
			char *c = _Ptr;
	
			while ( target == *c )
				c++;
	
			if ( c != _Ptr )
			{
				// fix up data and length
				size_t l = _Len - ( c - _Ptr );
				memmove( _Ptr, c, ( l + 1 ) * sizeof( char ) );
				_Len = l;
			}
		}
	
		void trim_left( void )
		{
			char *c = _Ptr;
	
			while ( _istspace( *c ) )
				c++;
	
			if ( c != _Ptr )
			{
				// fix up data and length
				size_t l = _Len - ( c - _Ptr );
				memmove( _Ptr, c, ( l + 1 ) * sizeof( char ) );
				_Len = l;
			}
		}
	
		void trim_right( char *targets )
		{
			// find beginning of trailing matches
			// by starting at beginning (DBCS aware)
	
			char *c = _Ptr;
			char *last = NULL;
	
			while ( *c != '\0' )
			{
				if ( strchr( targets, *c ) != NULL )
				{
					if ( last == NULL)
						last = c;
				}
				else
					last = NULL;
				c++;
			}
	
			if ( last != NULL )
			{
				// truncate at left-most matching character
				*last = '\0';
				_Len = last - _Ptr;
			}
		}
	
		void trim_right( char target )
		{
			char *c = _Ptr;
			char *last = NULL;
	
			while ( *c != '\0' )
			{
				if ( *c == target )
				{
					if ( last == NULL )
						last = c;
				}
				else
					last = NULL;
				c++;
			}
	
			if ( last != NULL )
			{
				// truncate at left-most matching character
				*last = '\0';
				_Len = last - c;
			}
		}
	
		void trim_right( void )
		{
			char *c = _Ptr;
			char *last = NULL;
	
			while ( *c != '\0' )
			{
				if ( _istspace( *c ) )
				{
					if ( last == NULL )
						last = c;
				}
				else
					last = NULL;
				c++;
			}
	
			if ( last != NULL )
			{
				// truncate at trailing space start
				*last = '\0';
				_Len = last - _Ptr;
			}
		}
	
		void reserve( size_t s )
		{
			if ( _Res < s )
				grow( s );
		}
	
		void resize( size_t s )
		{
			if ( s <= _Len )
				erase( s );
			else
				append( s - _Len, char( 0 ) );
		}
	
		void erase( size_t s )
		{
			if ( s )
				memset( _Ptr + s, 0, sizeof( char ) * ( _Len - s ) );
			_Len = s;
		}
	
		size_t size( void ) const
		{
			return _Len;
		}
	
		size_t length( void ) const
		{
			return _Len;
		}
	
		size_t capacity( void ) const
		{
			return _Res;
		}
	
		const char *c_str( void ) const
		{
			return _Ptr ? _Ptr : "";
		}
	
		const char *data( void ) const
		{
			return c_str();
		}
	
		cStr& assign( const cStr &s )
		{
			resize( s.size() );
			if ( _Len )
				strcpy( _Ptr, s.c_str() );
			return *this;
		}
	
		cStr& assign( size_t s, char c )
		{
			resize( s );
	
			char *p;
			for ( p = _Ptr; p < _Ptr + s; p++ )
				*p = c;
			*p = char( 0 );
	
			return *this;
		}
	
		cStr& assign( const cStr& _X, size_t _P, size_t _M )
		{
			size_t _N = _X.size() - _P;
			if (_M < _N)
				_N = _M;
	
	
			resize( _N );
			if ( _Len )
				strncpy( _Ptr, &_X.c_str()[_P], _N );
			if ( _Ptr )
				_Ptr[_N] = char( 0 );
	
			return *this;
		}
	
		cStr& append( size_t s, char c )
		{
			if ( _Len + s > _Res )
				grow( _Len + s );
			for ( char *p = _Ptr + _Len; p < _Ptr + _Len + s; p++ )
				*p = c;
			_Len += s;
			*(_Ptr+_Len) = 0;
			return *this;
		}
	
		cStr& append( const char *s )
		{
			size_t l = _Len;
			resize( l + strlen( s ) );
			if ( _Ptr )
				strcpy( _Ptr + l, s );
			return *this;
		}
	
		cStr& append( const cStr &s )
		{
			size_t l = _Len;
			resize( l + s.size() );
			if ( _Ptr )
				strcpy( _Ptr + l, s.c_str() );
			return *this;
		}
	
		size_t find( char s ) const
		{
			for ( char *p = _Ptr; p < _Ptr + _Len; p++ )
			{
				if ( *p == s )
					return p - _Ptr;
			}
			return npos;
		}
	
		cStr substr( size_t p, size_t m ) const
		{
			return cStr( *this, p, m );
		}
	
		int compare( const cStr &s ) const
		{
			const char *p1, *p2;
	
			p1 = empty() ? "" : _Ptr;
			p2 = s.empty() ? "" : s.c_str();
	
			return strcmp( p1, p2 );
		}
	
		bool empty( void ) const
		{
			return _Len == 0;
		}
	
		void insert( size_t offs, const cStr& val )
		{
			assert( offs <= size() );
			if ( val.empty() )
				return;
			size_t sz = size();
			resize( size() + val.size() );
			if ( offs < sz )
				memmove( _Ptr + offs + val.size(), _Ptr + offs, ( sz - offs + 1 ) * sizeof( char ) );
			memcpy( _Ptr + offs, val.c_str(), val.size() * sizeof( char ) );
			*(_Ptr + _Len) = 0;
		}
	
		void erase( size_t offs, size_t amount )
		{
			assert( offs < size() );
			assert( offs + amount <= size() );
	
			memmove( _Ptr + offs, _Ptr + offs + amount, ( size() - offs ) * sizeof( char ) );
			resize( size() - amount );
		}
	
		operator const char *() const
		{
			return c_str();
		}
	
		cStr& operator = ( const cStr& _X )
		{
			return assign( _X );
		}
	
		cStr& operator = ( const char* _X )
		{
			return assign( _X );
		}
	
		cStr& operator = ( char _X )
		{
			return assign( 1, _X );
		}
	
		cStr& operator += ( const cStr& _X )
		{
			return append( _X );
		}
	
		cStr& operator += ( const char* _X )
		{
			return append( _X );
		}
	
		cStr& operator += ( char _X )
		{
			return append( 1, _X );
		}
	
		void tolower( void )
		{
			for ( size_t i = 0; i < size(); i++ )
			{
				*(_Ptr + i) = ::tolower( *( _Ptr + i ) );
			}
		}
	};
	
	inline cStr operator + ( const cStr &s1, const cStr &s2 )
	{
		return cStr( s1 ) += s2;
	}
	
	inline bool operator < ( const cStr &_L, const cStr &_R )
	{
		return _L.compare(_R) < 0;
	}
	
	inline bool operator > ( const cStr &_L, const cStr &_R )
	{
		return _R < _L;
	}
	
	inline bool operator <= ( const cStr &_L, const cStr &_R )
	{
		return !( _R < _L );
	}
	
	inline bool operator >= ( const cStr &_L, const cStr &_R )
	{
		return !( _L < _R );
	}
	
	inline bool operator == ( const cStr &_L, const cStr &_R )
	{
		return _L.compare( _R ) ? false : true;
	}
	
	inline bool operator == ( const cStr &_L, const char *_R )
	{
		return _L.compare( _R ) ? false : true;
	}
	
	inline bool operator == ( const char *_L, const cStr &_R )
	{
		return _R.compare( _L ) ? false : true;
	}
	
	inline bool operator != ( const cStr &_L, const cStr &_R )
	{
		return _L.compare( _R ) ? true : false;
	}
	
	inline bool operator != ( const cStr &_L, const char *_R )
	{
		return _L.compare( _R ) ? true : false;
	}
	
	inline bool operator != ( const char *_L, const cStr &_R )
	{
		return _R.compare( _L ) ? true : false;
	}

}

#endif
