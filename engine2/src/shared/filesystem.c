#include "filesystem.h"
#include <zlib.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#ifdef __linux__
#include <dirent.h>
#include <fnmatch.h>
#endif
#include "cvars.h"

// should handle 32-bit sized streams inside of 64-bit sized storage
typedef struct fsStream_s {
	char					name[FS_MAX_FILE_NAME];
	uint32					size;		// uncompressed blocksize, 0 for directories
	uint32					comprsize;	// compressed block size; 0 for uncompressed
	uint64					offset;		// offset from file start. used only for pak files
} fsStream_t;

typedef struct fsStreamInfo_s
{
	fsStream_t stream;

	int16 idx;
	char pak;	// index of pakfile or -1
	char path;	// index of pathdir

	struct fsStreamInfo_s *next;
	struct fsStreamInfo_s *children;
} fsStreamInfo_t;

cvar_t *fs_maxfiles;
cvar_t *fs_path;

fsStreamInfo_t *fs_streams; // fs_init allocates this using fs_maxfiles cvar
int fs_stream_count;

char fs_path_dirs[FS_MAX_PATH_DIRS][FS_MAX_PATH_LENGTH];
int fs_path_count;

char fs_pakfiles[FS_MAX_PAKS][FS_MAX_FILE_NAME];
int fs_pak_count;

FILE* fs_opened_files[FS_MAX_OPENED_FILES];

int
fs_init_paks (fsStreamInfo_t *si, const char *path, int basepath);

int
fs_load_pak_file (int pakfile, int basepath);

int
fs_load_pak_file_structure (int pakfile, FILE *fp, fsStreamInfo_t *parent, int basepath);

fsStreamInfo_t*
fs_alloc_stream (void)
{
	fsStreamInfo_t *si;
	if (fs_stream_count == fs_maxfiles->ivalue) {
		sys_printf ("ERROR: too many files in vfs\n");
		return NULL;
	}
	si = &fs_streams[fs_stream_count];
	si->idx = fs_stream_count;
	memset (si->stream.name, 0, FS_MAX_FILE_NAME);
	si->stream.size = 0;
	si->stream.comprsize = 0;
	si->stream.offset = 0;
	si->pak = -1;
	fs_stream_count++;
	return si;
}

void
fs_streaminfo_copy (fsStreamInfo_t *d, fsStreamInfo_t *s)
{
	strcpy (d->stream.name, s->stream.name);
	d->stream.size = s->stream.size;
	d->stream.comprsize = s->stream.comprsize;
	d->stream.offset = s->stream.offset;
	d->idx = s->idx;
	d->pak = s->pak;
	d->path = s->path;
}

int
fs_find_stream (fsStreamInfo_t *si, const char *fname)
{
	const char *ptr;
	size_t sz = strlen (si->stream.name);
	fsStreamInfo_t *c;

	// compare beginning
	if (strncmp (si->stream.name, fname, sz))
	{
		return -1;
	}
	
	ptr = fname + sz;
	
	if (sz)
	{
		if (*ptr == 0)
		{
			return si->idx;
		}
		
		if (*ptr != '/')
			return -1;
		
		ptr++;
	}
	
	for (c = si->children; c; c = c->next)
	{
		int i = fs_find_stream (c, ptr);
		if (i != -1)
			return i;
	}
	
	return -1;
}

int
fs_init (void)
{
	char *dir;
	fs_maxfiles = cvar_get ("fs_maxfiles", "1024", CVAR_READONLY);
	fs_path = cvar_get ("fs_path", ".", CVAR_READONLY);
	fs_streams = malloc (sizeof (fsStreamInfo_t) * fs_maxfiles->ivalue);
	sys_printf ("mounting vfs directories...\n");
	sys_printf ("PATH is '%s'\n", fs_path->string);

	// traverse fs_path and add files
	dir = fs_path->string;
	while (*dir) {
		if (fs_path_count >= FS_MAX_PATH_DIRS)
		{
			sys_printf ("WARNING: PATH contains more than %d dirs, stopping\n", FS_MAX_PATH_DIRS);
			return 0;
		}
		char *e = dir;
		char *p = fs_path_dirs[fs_path_count++];
		while (*e && *e != ':') {
			e++;
		}
		strncpy (p, dir, e-dir);
		p[e-dir] = 0;
		if (!fs_stream_count) {
			fs_alloc_stream ();
		}
		fs_init_files (fs_streams, p, fs_path_count - 1);
		fs_init_paks (fs_streams, p, fs_path_count - 1);
		if (*e == 0) {
			break;
		}
		dir = e+1;
		if (*dir == 0) {
			break;
		}
	}
}

void
fs_free (void)
{
	if (fs_streams) {
		free (fs_streams);
		fs_streams = NULL;
	}
}

int
fs_init_files (fsStreamInfo_t *si, const char *path, int basepath)
{
#ifndef _WIN32
	struct dirent **namelist = NULL;
	int n;
	fsStreamInfo_t *c;

	n = scandir (path, &namelist, 0, alphasort);
	if (n < 0)
	{
		if (namelist)
			free (namelist);
		return 0;	// not a dir or no read access
	}
	else
	{
		si->stream.size = 0; // dir
		while (n--)
		{
			// no hidden files
			if (namelist[n]->d_name[0] != '.')
			{
				if (fnmatch (namelist[n]->d_name, "*.pak", 0))
				{
					fsStreamInfo_t nf;
					char *fullpath;
					FILE *fp;
					if (strlen (namelist[n]->d_name) > FS_MAX_FILE_NAME - 1)
					{
						sys_printf ("WARNING: fs_init_files: filename is too long (>%d characters): '%s' (skipped)\n", FS_MAX_FILE_NAME-1, namelist[n]->d_name);
						continue;
					}
					strcpy (nf.stream.name, namelist[n]->d_name);
					fullpath = malloc (strlen(path)+strlen(namelist[n]->d_name)+2);
					strcpy (fullpath, path);
					strcat (fullpath, "/");
					strcat (fullpath, namelist[n]->d_name);
					nf.stream.comprsize = 0; // uncompressed
					fp = fopen (fullpath, "rb");
					free (fullpath);
					if (!fp)
						nf.stream.size = 0; // directory
					else
					{
						// get size
						size_t filesize;
						size_t i;
						fsStreamInfo_t *curr;
						fseek (fp, 0, SEEK_END);
						filesize = ftell (fp);
						fclose (fp);
						
						// FIXME: how do i know it's a directory?!
//						nf->basepath = basepath;
						nf.stream.size = filesize;
						nf.stream.offset = 0;
						nf.path = basepath;
	
						// find that file in the children of 'parent'
						for (c = si->children; c; c = c->next)
						{
							if (!strcmp (c->stream.name, nf.stream.name))
							{
								// directories are the same! do not add
								curr = c;
								break;
							}
						}
	
						if (!curr)
						{
							// insert new
							fsStreamInfo_t *ff = fs_alloc_stream ();
							fs_streaminfo_copy (ff, &nf);
							ff->next = si->children;
							si->children = ff;
						}
						else
						{
							// replace old
							fs_streaminfo_copy (curr, &nf);
						}
					}
				}
			}
			
			free (namelist[n]);
		}
		free (namelist);
	}

	for (c = si->children; c; c = c->next)
	{
		char *dir = malloc (strlen (path)+strlen (c->stream.name)+2);
		strcpy (dir, path);
		strcat (dir, "/");
		strcat (dir, c->stream.name);
		fs_init_files (c, dir, basepath);
		free (dir);
	}
#else
	// here goes tested and working win32 implementation (just for your reference)
	WIN32_FIND_DATAA fd;
	HANDLE hSearch = FindFirstFileA (cStr (path) + "/*.*", &fd);
	if (hSearch != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (strcmp (fd.cFileName, "..") && strcmp (fd.cFileName, "."))
			{
				// skip paks
				char ext[_MAX_EXT];
				_splitpath (fd.cFileName, NULL, NULL, NULL, ext);
				if (strcmp (ext, ".pak"))
				{
					fsFileEntry *nf = new fsFileEntry (this);
					cStr fn = fd.cFileName;
//					fn.tolower ();
					strcpy (nf->name, fn.c_str ());
					nf->blocktype = (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) ? fs_directory : fs_BinaryRawData;
					nf->fstype = fsFileEntry::os;
					nf->basepath = basepath;
					nf->size = fd.nFileSizeLow;
					nf->comprsize = nf->size;
					nf->offset = 0;

					// find that file in the children of 'parent'
					size_t i;
					fsFileEntry *curr = NULL;

					for (i = 0; i < f->children.size (); i++)
					{
						if (!strcmp (f->children[i]->name, nf->name))
						{
							// directorys are the same! do not add
							curr = f->children[i];
							break;
						}
					}

					if (!curr)
					{
						curr = nf;
						curr->fstype = fsFileEntry::os;
						curr->pakfile = -1;
						f->children.push_back (curr);
					}
					else
					{
						// replace
						curr->basepath = basepath;
						curr->blocktype = nf->blocktype;
						curr->size = nf->size;
						curr->comprsize = nf->comprsize;
						curr->offset = nf->offset;
						curr->pakfile = -1;
						curr->fstype = fsFileEntry::os;
						delete nf;
					}
//					f->children.push_back (nf);
				}
			}
		} while (FindNextFileA (hSearch, &fd));
		FindClose (hSearch);
	}
	
	for (size_t i = 0; i < f->children.size (); i++)
		initNormal (f->children[i], cStr (path) + "/" + f->children[i]->name, basepath);
#endif
}

int
fs_init_paks (fsStreamInfo_t *si, const char *path, int basepath)
{
	#if defined(__linux__)
		struct dirent **namelist = NULL;
		int n;
		n = scandir (path, &namelist, 0, alphasort);
		if (n < 0)
		{
			if (namelist)
				free (namelist);
			sys_printf ("ERROR: fs_init_paks: scandir failed (%s), retval=%d", path, n);
			return -1;
		}
		else
		{
			while (n--)
			{
				if (!fnmatch (namelist[n]->d_name, "*.pak", 0))
				{
					// .pak extension
					
					// try fopen
					FILE *fp = fopen (namelist[n]->d_name, "rb");
					if (!fp)
					{
						// doesn't open, can be a dir, or something wrong, or whatever..
					}
					else
					{
						fclose (fp);
						if (fs_path_count == FS_MAX_PAKS) {
							sys_printf ("ERROR: fs_init_paks: too many paks");
							return -1;
						}
						strcpy (fs_pakfiles[fs_pak_count], path);
						strcat (fs_pakfiles[fs_pak_count], "/");
						strcat (fs_pakfiles[fs_pak_count], namelist[n]->d_name);
						fs_pak_count++;
					}
				}
				free (namelist[n]);
			}
			free (namelist);
		}
		
		// sort alphabetically
		qsort (fs_pakfiles, fs_pak_count, sizeof (char *), alphasort);
	
		// load
		for (n = 0; n < fs_pak_count; n++)
			fs_load_pak_file (n, basepath);
	
	#endif
	return 0;
}

int
fs_load_pak_file (int pakfile, int basepath)
{
	FILE *fp;
	char magic[6];
	int res;
	fp = fopen (fs_pakfiles[pakfile], "rb");
	if (!fp) {
		sys_printf ("ERROR: fs_load_pak_file: failed to open file %s for reading", fs_pakfiles[pakfile]);
		return -1;
	}
	magic[5] = 0;
	fread (magic, 5, 1, fp);
	if (strcmp (magic, "FEPAK"))
	{
		fclose (fp);
		sys_printf ("ERROR: fs_load_pak_file: invalid pak signature in file %s", fs_pakfiles[pakfile]);
		return -1;
	}
	// read table offset
	ulong table;
	fread (&table, sizeof (table), 1, fp);

	// seek there
	fseek (fp, table, SEEK_SET);

	// load
	res = fs_load_pak_file_structure (pakfile, fp, NULL, basepath);

	fclose (fp);
	return res;
}

int
fs_load_pak_file_structure (int pakfile, FILE *fp, fsStreamInfo_t *parent, int basepath)
{
	fsStreamInfo_t *curr = NULL;
	fsStreamInfo_t currFile;
	fread (&currFile, sizeof (fsStream_t), 1, fp);
	if (!parent)
	{
		// reading root directory
		curr = fs_streams;
	}
	else
	{
		// find that file in the children of 'parent'
		fsStreamInfo_t *c;

		for (c = parent->children; c; c=c->next)
		{
			if (!strcmp (c->stream.name, currFile.stream.name))
			{
				// directories are the same! do not add
				curr = c;
				break;
			}
		}

		if (!curr)
		{
			curr = fs_alloc_stream ();
		}
		fs_streaminfo_copy (curr, &currFile);
		curr->path = basepath;
		curr->pak = pakfile;
		curr->next = parent->children;
		parent->children = curr;
	}

	if (!currFile.stream.size) // directory
	{
		// load children
		uint16 nc;
		fread (&nc, sizeof (nc), 1, fp);
		while (nc--)
		{
			fs_load_pak_file_structure (pakfile, fp, curr, basepath);
		}
	}
	return 0;
}

int
fs_open_file (const char *fname)
{
	int k;
	int e = fs_find_stream (fs_streams, fname);
	if (e == -1)
		return -1;

	for (k = 0; k < FS_MAX_OPENED_FILES; k++) {
		if (!fs_opened_files[k])
			break;
	}
	if (k == FS_MAX_OPENED_FILES)
		return -1;
	
	return k;
}

