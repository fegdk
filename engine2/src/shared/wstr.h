/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_WSTR_H
#define __F_WSTR_H

#include "wchar.h"
#ifndef _WIN32
#include "wctype.h"
#endif

#ifndef _wcsinc
#define _wcsinc(_pc)    ((_pc)+1)
#endif

#ifndef _wclen
#define _wclen(lpsz) (1)
#endif

namespace fe
{

	class wStr
	{
	protected:
		wchar_t*	_Ptr;
		size_t		_Len;
		size_t		_Res;
	
	public:
	
		static const size_t npos = 0xffffffff;
	
		wStr( const wchar_t *s )
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
			assign( s );
		}
		wStr( const char *s )
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
			assign( s );
		}
	
		wStr()
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
		}
	
		wStr( const wStr& _X )
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
			assign(_X, 0, npos);
		}
	
		wStr( const wStr& _X, size_t _P, size_t _M )
		{
			_Ptr = NULL;
			_Len = 0;
			_Res = 0;
			assign(_X, _P, _M);
		}
	
		~wStr()
		{
			if ( _Ptr )
				delete[] _Ptr;
		}
	
		void clear( void )
		{
			if ( _Len )
			{
				_Len = 0;
				*_Ptr = 0;
			}
		}
	
		wStr &assign( const wchar_t *s )
		{
			resize( wcslen( s ) );
			if ( _Len )
				wcscpy( _Ptr, s );
			else if ( _Ptr )
				*_Ptr = 0;
			return *this;
		}
		wStr& assign( const char *s )
		{
			resize( strlen( s ) );
			if ( _Ptr )
			{
				size_t i;
				for ( i = 0; i < _Len; i++ )
					_Ptr[i] = *s++;
				if ( _Len )
					_Ptr[i] = 0;
			}
			return *this;
		}
	
		void printf( wchar_t *fmt, ... )
		{
	//		try {
				int n;
				char *p, *np;
				va_list ap;
	
				if (_Len == 0)
					resize (wcslen (fmt));
	
				while (1) {
					/* Try to print in the allocated space. */
					va_start(ap, fmt);
					n = vswprintf (_Ptr, _Len, fmt, ap);
					va_end(ap);
					/* If that worked, return the string. */
					if (n > -1 && n < _Len)
						break;
					/* Else try again with more space. */
					if (n > -1)    /* glibc 2.1 */
						resize (_Len + 1); /* precisely what is needed */
					else           /* glibc 2.0 */
						resize (_Len * 2);  /* twice the old size */
				}
	//		}
	//		catch ( ... )
	//		{
				// seems like too big line. terminate.
	//			throw genericError ("cStr::printf failed!\n");
	//		}
		}	
	
		void trim_left( wchar_t *targets )
		{
			wchar_t *c = _Ptr;
	
			while ( *c != '\0' )
			{
				if ( wcschr( targets, *c ) == NULL )
					break;
				c++;
			}
	
			if ( c != _Ptr )
			{
				// fix up data and length
				size_t l = _Len - ( c - _Ptr );
				memmove( _Ptr, c, ( l + 1 ) * sizeof( wchar_t ) );
				_Len = l;
			}
		}
	
		void trim_left( wchar_t target )
		{
			wchar_t *c = _Ptr;
	
			while ( target == *c )
				c++;
	
			if ( c != _Ptr )
			{
				// fix up data and length
				size_t l = _Len - ( c - _Ptr );
				memmove( _Ptr, c, ( l + 1 ) * sizeof( wchar_t ) );
				_Len = l;
			}
		}
	
		void trim_left( void )
		{
			wchar_t *c = _Ptr;
	
			while ( _istspace( *c ) )
				c++;
	
			if ( c != _Ptr )
			{
				// fix up data and length
				size_t l = _Len - ( c - _Ptr );
				memmove( _Ptr, c, ( l + 1 ) * sizeof( wchar_t ) );
				_Len = l;
			}
		}
	
		void trim_right( wchar_t *targets )
		{
			// find beginning of trailing matches
			// by starting at beginning (DBCS aware)
	
			wchar_t *c = _Ptr;
			wchar_t *last = NULL;
	
			while ( *c != '\0' )
			{
				if ( wcschr( targets, *c ) != NULL )
				{
					if ( last == NULL)
						last = c;
				}
				else
					last = NULL;
				c++;
			}
	
			if ( last != NULL )
			{
				// truncate at left-most matching character
				*last = '\0';
				_Len = last - _Ptr;
			}
		}
	
		void trim_right( wchar_t target )
		{
			wchar_t *c = _Ptr;
			wchar_t *last = NULL;
	
			while ( *c != '\0' )
			{
				if ( *c == target )
				{
					if ( last == NULL )
						last = c;
				}
				else
					last = NULL;
				c++;
			}
	
			if ( last != NULL )
			{
				// truncate at left-most matching character
				*last = '\0';
				_Len = last - c;
			}
		}
	
		void trim_right( void )
		{
			wchar_t *c = _Ptr;
			wchar_t *last = NULL;
	
			while ( *c != '\0' )
			{
				if ( _istspace( *c ) )
				{
					if ( last == NULL )
						last = c;
				}
				else
					last = NULL;
				c++;
			}
	
			if ( last != NULL )
			{
				// truncate at trailing space start
				*last = '\0';
				_Len = last - _Ptr;
			}
		}
	
		void reserve( size_t s )
		{
			if ( _Res < s )
				grow( s );
		}
	
		void resize( size_t s )
		{
			if ( s <= _Len )
				erase( s );
			else
				append( s - _Len, wchar_t( 0 ) );
		}
	
		void erase( size_t s )
		{
			if ( s )
				memset( _Ptr + s, 0, sizeof( wchar_t ) * ( _Len - s ) );
			_Len = s;
		}
	
		size_t size( void ) const
		{
			return _Len;
		}
	
		size_t length( void ) const
		{
			return _Len;
		}
	
		size_t capacity( void ) const
		{
			return _Res;
		}
	
		const wchar_t *c_str( void ) const
		{
			return _Ptr ? _Ptr : L"";
		}
	
		const wchar_t *data( void ) const
		{
			return c_str();
		}
	
		void grow( size_t s )
		{
			_Res = s;
			wchar_t *buf = new wchar_t[ _Res + 1 ];
			assert( buf );
			if ( _Ptr )
			{
				wcscpy( buf, _Ptr );
				delete[] _Ptr;
			}
			else
				memset( buf, 0, sizeof( wchar_t ) * ( _Res + 1 )  );
			_Ptr = buf;
		}
	
		wStr& assign( const wStr &s )
		{
			resize( s.size() );
			if ( _Len )
				wcscpy( _Ptr, s.c_str() );
			return *this;
		}
	
		wStr& assign( size_t s, wchar_t c )
		{
			resize( s );
	
			wchar_t *p;
			for ( p = _Ptr; p < _Ptr + s; p++ )
				*p = c;
			*p = wchar_t( 0 );
	
			return *this;
		}
	
		wStr& assign( const wStr& _X, size_t _P, size_t _M )
		{
			size_t _N = _X.size() - _P;
			if (_M < _N)
				_N = _M;
	
	
			resize( _N );
			if ( _Len )
				wcsncpy( _Ptr, &_X.c_str()[_P], _N );
			if ( _Ptr )
				_Ptr[_N] = wchar_t( 0 );
	
			return *this;
		}
	
		wStr& append( size_t s, wchar_t c )
		{
			if ( _Len + s > _Res )
				grow( _Len + s );
			for ( wchar_t *p = _Ptr + _Len; p < _Ptr + _Len + s; p++ )
				*p = c;
			_Len += s;
			return *this;
		}
	
		wStr& append( const wchar_t *s )
		{
			size_t l = _Len;
			resize( l + wcslen( s ) );
			wcscpy( _Ptr + l, s );
			return *this;
		}
	
		wStr& append( const wStr &s )
		{
			size_t l = _Len;
			resize( l + s.size() );
			wcscpy( _Ptr + l, s.c_str() );
			return *this;
		}
	
		size_t find( wchar_t s ) const
		{
			for ( wchar_t *p = _Ptr; p < _Ptr + _Len; p++ )
			{
				if ( *p == s )
					return p - _Ptr;
			}
			return npos;
		}
	
		wStr substr( size_t p, size_t m ) const
		{
			return wStr( *this, p, m );
		}
	
		int compare( const wStr &s ) const
		{
			const wchar_t *p1, *p2;
	
			p1 = empty() ? L"" : _Ptr;
			p2 = s.empty() ? L"" : s.c_str();
	
			return wcscmp( p1, p2 );
		}
	
		bool empty( void ) const
		{
			return _Len == 0;
		}
	
		void insert( size_t offs, const wStr& val )
		{
			assert( offs <= size() );
			if ( val.empty() )
				return;
			size_t sz = size();
			resize( size() + val.size() );
			if ( offs < sz )
				memmove( _Ptr + offs + val.size(), _Ptr + offs, ( sz - offs + 1 ) * sizeof( wchar_t ) );
			memcpy( _Ptr + offs, val.c_str(), val.size() * sizeof( wchar_t ) );
			*(_Ptr + _Len) = 0;
		}
	
		void erase( size_t offs, size_t amount )
		{
			assert( offs < size() );
			assert( offs + amount <= size() );
	
			memmove( _Ptr + offs, _Ptr + offs + amount, ( size() - offs - amount ) * sizeof( wchar_t ) );
			resize( size() - amount );
		}
	
		operator const wchar_t *() const
		{
			return c_str();
		}
	
		wStr& operator = ( const wStr& _X )
		{
			return assign( _X );
		}
	
		wStr& operator = ( const wchar_t* _X )
		{
			return assign( _X );
		}
	
		wStr& operator = ( wchar_t _X )
		{
			return assign( 1, _X );
		}
	
		wStr& operator += ( const wStr& _X )
		{
			return append( _X );
		}
	
		wStr& operator += ( const wchar_t* _X )
		{
			return append( _X );
		}
	
		wStr& operator += ( wchar_t _X )
		{
			return append( 1, _X );
		}
	
		void tolower( void )
		{
			for ( size_t i = 0; i < size(); i++ )
			{
				#ifdef _WIN32
				*(_Ptr + i) = towlower( *( _Ptr + i ) );
				#else
				*(_Ptr + i) = towlower( *( _Ptr + i ) );
				#endif
			}
		}
	
	};
	
	inline wStr operator + ( const wStr &s1, const wStr &s2 )
	{
		return wStr( s1 ) += s2;
	}
	
	inline bool operator < ( const wStr &_L, const wStr &_R )
	{
		return _L.compare(_R) < 0;
	}
	
	inline bool operator > ( const wStr &_L, const wStr &_R )
	{
		return _R < _L;
	}
	
	inline bool operator <= ( const wStr &_L, const wStr &_R )
	{
		return !( _R < _L );
	}
	
	inline bool operator >= ( const wStr &_L, const wStr &_R )
	{
		return !( _L < _R );
	}
	
	inline bool operator == ( const wStr &_L, const wStr &_R )
	{
		return _L.compare( _R ) ? false : true;
	}
	
	inline bool operator == ( const wStr &_L, const wchar_t *_R )
	{
		return _L.compare( _R ) ? false : true;
	}
	
	inline bool operator == ( const wchar_t *_L, const wStr &_R )
	{
		return _R.compare( _L ) ? false : true;
	}
	
	inline bool operator != ( const wStr &_L, const wStr &_R )
	{
		return _L.compare( _R ) ? true : false;
	}
	
	inline bool operator != ( const wStr &_L, const wchar_t *_R )
	{
		return _L.compare( _R ) ? true : false;
	}
	
	inline bool operator != ( const wchar_t *_L, const wStr &_R )
	{
		return _R.compare( _L ) ? true : false;
	}

}

#endif // __F_WSTR_H
