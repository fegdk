#ifndef __CONSOLE_H
#define __CONSOLE_H

#include "types.h"

/**
 * Implementation of developer console, which can be used to create/run commands, query/set console variables, etc.
 */
int
cmd_argc (void);

const char *
cmd_argv(int arg);

void
sys_printf (const char *fmt, ...);
	
enum
{
	MAX_FIELD_SIZE = 256
};

typedef struct field_s
{
	char buffer[MAX_FIELD_SIZE];
	int cursor;
	int matchCount;
	char shortestMatch[MAX_FIELD_SIZE];
} field_t;

void
cmd_complete (field_t *f, boolean use_commands, boolean use_cvars);

void
cmd_find_matches (field_t *f, const char *s);

void
cmd_print_matches (field_t *f, const char *s);

void
cmd_cvar_matches (field_t *f, const char *s);

enum {
	
	CMD_MAX_HISTORY = 100,
	MAX_ALIASES = 100,
	MAX_ALIAS_LEN = 100,
	MAX_COMMANDS = 1000,
	MAX_ALIAS_NAME = 32
};

extern int cmd_cursor;
extern int cmd_current;

typedef struct
{
	const char *name;
	void (*exec)(void);
	command_t *hashNext;
} command_t;

typedef struct
{
	char left[MAX_ALIAS_NAME];
	char right[MAX_ALIAS_LEN];
} alias_t;

typedef struct {
	int start;
	int num;
	char lines[CMD_MAX_HISTORY][MAX_FIELD_SIZE];
} cmd_history_t;

void
cmd_history_append (cmd_history_t *hist, const char *ln);

enum
{
	CMD_HASH_SIZE = 256
};

extern command_t cmd_ommands[MAX_COMMANDS];
extern int cmd_num_commands;
extern command_t* cmd_command_hash[CMD_HASH_SIZE];

extern alias_t cmd_aliases[MAX_ALIASES];
extern int cmd_num_aliases;

extern boolean cmd_visible;
extern float cmd_vistimer;
extern cmd_history_t cmd_history;
extern cmd_history_t cmd_msg_buffer;

int
cmd_get_num_rows (void);

int
cmd_find_command (const char *name);

int
cmd_init (const char *log_fname);

void
cmd_free (void);

void
cmd_render (void);

void
cmd_show (boolean show);

void
cmd_toggle (void);

void
cmd_echo (const char *s);

void
cmd_exec (const char *s);

void
cmd_register_command (const char *name, void (*exec)(void));

void
cmd_set_alias (const char *name, const char *value);

void
cmd_command_completion (void (*matchfunc)(const char *cmd, field_t *date), field_t *data);
	
#endif // __F_CONSOLE_H

