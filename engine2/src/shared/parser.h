/* FIXME: multiline comment w/ preprocessor may behave wrong */
#ifndef __PARSER_H
#define __PARSER_H

enum {
	P_OK = 0,
	P_FILE_NOT_FOUND = -1,
	P_EOF = -2,
	P_TOKEN_TOO_LARGE = -3,
	P_TOKEN_MISMATCH = -4,
	P_SYNTAX_ERROR = -5,
	P_BUFFER_TOO_SMALL = -6,
	P_UNEXPECTED_EOF = -7,
};

enum {
	P_MAX_ERROR_STRING = 2048,
	P_MAX_TOKEN = 1024,
	P_MAX_INCLUDE_DEPTH = 8,
	P_MAX_FNAME = 1024,
	P_MAX_BUFFER = 16384,
};

extern char p_error_string[P_MAX_ERROR_STRING];
extern char p_buffer[P_MAX_BUFFER];
extern const char *p_script;
extern const char *p_end;
extern int p_size;
extern int p_line;
extern char *p_tokenptr;
extern int p_tokenready;
extern char p_token[P_MAX_TOKEN];
extern char p_fname[P_MAX_FNAME];

int
p_open_vfs_file (const char *fname);

int
p_open_file (const char *fname);

int
p_open_buffer (const char *buffer, int sz);
	
int
p_init (const char *script, int stdio, int sz);

void
p_close (void);

int
p_load_file (const char *fname, int stdio);

int
p_gettoken (void);

void
p_ungettoken (void);

int
p_matchtoken (const char *match);

int
p_nextline (void);

int
p_ignoreblock (const char *opentag, const char *closetag);

int
p_getblockcontents (const char *opentag, const char *closetag, char *block, int sz);

int
p_cmptoken (const char *s);

int
p_tokenavailable (void);

int
p_iseol (void);

int
p_unexpectedeof (void);

int
p_syntaxerror (void);

#endif /* __PARSER_H */
