/*
    fegdk: FE Game Development Kit
    Copyright (C) 2001-2008 Alexey "waker" Yakovenko

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    Alexey Yakovenko
    waker@users.sourceforge.net
*/

#ifndef __F_SCRIPT_H
#define __F_SCRIPT_H

#include "f_baseobject.h"
#include "f_parser.h"

namespace fe
{

	// this class is responsible for loading and managing all engine scripts (materials, models, anims, etc)
	//
	// usage pattern:
	//  1. create script object
	//  2. run script::load method to load specified script file
	//     which is usually going to contain "load" calls to load other scripts
	//     it will load object definitions into memory (which will allow to access them)
	//  3. load (reference) your application's objects (materials, models, etc) using resourceMgr
	//  4. if u want all objects to be prefetched -- call script::prefetch method
	//  5. if u want all unused definitions to be unloaded -- call script::flushUnused methos
	
	class FE_API script : public baseObject
	{
	private:
		typedef std::vector <smartPtr <baseObject> > objList;
		objList	mObjects; // list of objects specified in script file
	public:
		script (void);
		~script (void);
		void load (const char *fname);
		void load (charParser &p);
		void prefetch (void);
		void flushUnused (void);
	};

}

#endif // __F_SCRIPT_H

