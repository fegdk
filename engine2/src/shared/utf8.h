#include <stdarg.h>
#include "f_types.h"

namespace fe
{

/* is c the start of a utf8 sequence? */
#define isutf(c) (((c)&0xC0)!=0x80)

/* convert UTF-8 data to wide character */
int u8_toucs(uint32 *dest, int32 sz, const char *src, int32 srcsz);

/* the opposite conversion */
int u8_toutf8(char *dest, int32 sz, uint32 *src, int32 srcsz);

/* single character to UTF-8 */
int u8_wc_toutf8(char *dest, wchar_t ch);

/* character number to byte offset */
int u8_offset(char *str, int32 charnum);

/* byte offset to character number */
int u8_charnum(char *s, int32 offset);

/* return next character, updating an index variable */
uint32 u8_nextchar(const char *s, int32 *i);

/* move to next character */
void u8_inc(const char *s, int32 *i);

/* move to previous character */
void u8_dec(const char *s, int32 *i);

/* assuming src points to the character after a backslash, read an
   escape sequence, storing the result in dest and returning the number of
   input characters processed */
int u8_read_escape_sequence(const char *src, uint32 *dest);

/* given a wide character, convert it to an ASCII escape sequence stored in
   buf, where buf is "sz" bytes. returns the number of characters output. */
int u8_escape_wchar(char *buf, int32 sz, uint32 ch);

/* convert a string "src" containing escape sequences to UTF-8 */
int u8_unescape(char *buf, int32 sz, const char *src);

/* convert UTF-8 "src" to ASCII with escape sequences.
   if escape_quotes is nonzero, quote characters will be preceded by
   backslashes as well. */
int u8_escape(char *buf, int32 sz, const char *src, int32 escape_quotes);

/* utility predicates used by the above */
int octal_digit(char c);
int hex_digit(char c);

/* return a pointer to the first occurrence of ch in s, or NULL if not
   found. character index of found character returned in *charn. */
char *u8_strchr(char *s, uint32 ch, int32 *charn);

/* same as the above, but searches a buffer of a given size instead of
   a NUL-terminated string. */
char *u8_memchr(char *s, uint32 ch, size_t sz, int32 *charn);

/* count the number of characters in a UTF-8 string */
int u8_strlen(char *s);

int u8_is_locale_utf8(char *locale);

/* printf where the format string and arguments may be in UTF-8.
   you can avoid this function and just use ordinary printf() if the current
   locale is UTF-8. */
int u8_vprintf(char *fmt, va_list ap);
int u8_printf(char *fmt, ...);

}

